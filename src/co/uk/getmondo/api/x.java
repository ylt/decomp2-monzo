package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class x implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!x.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public x(c var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new x(var, var, var, var);
   }

   public TaxResidencyApi a() {
      return (TaxResidencyApi)b.a.d.a(this.b.a((OkHttpClient)this.c.b(), (com.squareup.moshi.v)this.d.b(), (String)this.e.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
