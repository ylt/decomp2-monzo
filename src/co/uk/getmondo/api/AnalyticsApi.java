package co.uk.getmondo.api;

import co.uk.getmondo.api.model.tracking.Impression;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AnalyticsApi {
   @Headers({"Content-Type: application/json"})
   @POST("analytics/track")
   io.reactivex.b trackEvent(@Body Impression var);
}
