package co.uk.getmondo.api.b;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class i implements Callable {
   private final a a;

   private i(a var) {
      this.a = var;
   }

   public static Callable a(a var) {
      return new i(var);
   }

   public Object call() {
      return a.a(this.a);
   }
}
