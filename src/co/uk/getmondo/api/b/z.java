package co.uk.getmondo.api.b;

import co.uk.getmondo.d.ai;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class z implements Callable {
   private final a a;
   private final ai b;
   private final String c;
   private final String d;

   private z(a var, ai var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public static Callable a(a var, ai var, String var, String var) {
      return new z(var, var, var, var);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c, this.d);
   }
}
