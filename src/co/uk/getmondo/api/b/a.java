package co.uk.getmondo.api.b;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import co.uk.getmondo.api.authentication.OAuthException;
import co.uk.getmondo.api.model.ApiAccount;
import co.uk.getmondo.api.model.ApiToken;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

public class a {
   private final co.uk.getmondo.common.s a;
   private final co.uk.getmondo.common.accounts.d b;
   private final MonzoApi c;
   private final MonzoOAuthApi d;
   private final MonzoProfileApi e;
   private final co.uk.getmondo.card.c f;
   private final co.uk.getmondo.common.k.f g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.q i;
   private final co.uk.getmondo.common.g j;
   private final co.uk.getmondo.common.accounts.m k;
   private co.uk.getmondo.payments.send.data.h l;

   public a(co.uk.getmondo.common.s var, co.uk.getmondo.common.accounts.d var, MonzoApi var, MonzoOAuthApi var, MonzoProfileApi var, co.uk.getmondo.card.c var, co.uk.getmondo.common.k.f var, co.uk.getmondo.common.a var, co.uk.getmondo.common.q var, co.uk.getmondo.common.g var, co.uk.getmondo.common.accounts.m var, co.uk.getmondo.payments.send.data.h var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(ApiToken var) throws Exception {
      return android.support.v4.g.j.a((new co.uk.getmondo.d.a.s()).a(var), var.d());
   }

   // $FF: synthetic method
   static ai a(a var, ai var, String var, String var) throws Exception {
      var.b.e();
      var.b.a(var, var, var);
      return var;
   }

   // $FF: synthetic method
   static ak a(a var, co.uk.getmondo.common.accounts.l var) throws Exception {
      return var.b.b();
   }

   // $FF: synthetic method
   static io.reactivex.v a(a var, ApiAccount var) {
      return var.a(var);
   }

   private io.reactivex.v a(ApiAccount var) {
      io.reactivex.v var;
      if(co.uk.getmondo.d.a.b.a(var.f()) == co.uk.getmondo.d.a.b.RETAIL) {
         var = this.f.a(var.a()).d(j.a());
      } else {
         var = io.reactivex.v.a(this.c(var.a()), this.f.a(var.a()), k.a());
      }

      return var.d(l.a(var)).a(co.uk.getmondo.d.a.class);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var) throws Exception {
      return io.reactivex.v.a((Object)var.l.d());
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, ai var) throws Exception {
      io.reactivex.v var = var.c();
      io.reactivex.v var = var.e();
      io.reactivex.v var = var.d();
      co.uk.getmondo.common.accounts.m var = var.k;
      var.getClass();
      return io.reactivex.v.a(var, var, var, v.a(var));
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, String var, android.support.v4.g.j var) throws Exception {
      return var.a((ai)var.a, var, (String)var.b);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, String var, String var) throws Exception {
      String var = var.a.a();
      io.reactivex.v var;
      if(!var.equals(var.b(var, var.a.b()))) {
         var = io.reactivex.v.a((Throwable)(new OAuthException(var)));
      } else {
         var = var.d.requestUserToken(MonzoOAuthApi.a, "authorization_code", var, "https://monzo.com/-magic-auth").d(w.a()).a(y.a(var, var));
      }

      return var;
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, List var) throws Exception {
      return io.reactivex.n.fromIterable(var).flatMapSingle(u.a(var)).toList();
   }

   // $FF: synthetic method
   static io.reactivex.z a(Throwable var) throws Exception {
      io.reactivex.v var;
      if(var instanceof ApiException && ((ApiException)var).a()) {
         var = io.reactivex.v.a((Object)com.c.b.b.c());
      } else {
         var = io.reactivex.v.a(var);
      }

      return var;
   }

   // $FF: synthetic method
   static Object a(ApiAccount var, co.uk.getmondo.d.a.a var) throws Exception {
      return var.a(var);
   }

   // $FF: synthetic method
   static void a(a var, ak var) throws Exception {
      var.i.a(var.d(), var.c());
   }

   // $FF: synthetic method
   static void a(a var, co.uk.getmondo.signup_old.a.a.a var, String var, an var) throws Exception {
      co.uk.getmondo.d.ac var = new co.uk.getmondo.d.ac(var.c(), var.a(), (String)null, var, var.b(), (String)null, (Integer)null, var.c(), false);
      var.b.a(var, var);
   }

   // $FF: synthetic method
   static void a(a var, Throwable var) throws Exception {
      if(var instanceof ApiException) {
         ApiException var = (ApiException)var;
         int var = var.b();
         String var = var.c();
         String var = var.d();
         co.uk.getmondo.api.model.b var = var.e();
         var.h.a(Impression.b(var, var, var, var));
      }

   }

   // $FF: synthetic method
   static ak b(a var, co.uk.getmondo.common.accounts.l var) throws Exception {
      return var.b.b();
   }

   private io.reactivex.b b(co.uk.getmondo.signup_old.a.a.a var) {
      String var = this.a.a();
      io.reactivex.v var = this.c.waitlistSignUp((new co.uk.getmondo.api.a.a(var, var.a(), var.c().j(), var.b(), var.c().c())).a());
      co.uk.getmondo.d.a.v var = new co.uk.getmondo.d.a.v();
      var.getClass();
      return var.d(b.a(var)).c(m.a(this, var, var)).c();
   }

   private String b(String var, String var) {
      try {
         co.uk.getmondo.common.k.f var = this.g;
         StringBuilder var = new StringBuilder();
         var = var.a(var.append(var).append(var).toString());
      } catch (NoSuchAlgorithmException var) {
         var = var;
      }

      return var;
   }

   // $FF: synthetic method
   static void b(a var, ak var) throws Exception {
      var.i.a(var.b());
      var.j.a(var.b());
   }

   // $FF: synthetic method
   static void b(a var, Throwable var) throws Exception {
      var.b.e();
   }

   private io.reactivex.v c() {
      return this.c.accounts().d(g.a()).a(h.a(this));
   }

   private io.reactivex.v c(String var) {
      return this.c.getInitialTopupStatus(var).d(n.a());
   }

   private io.reactivex.v d() {
      return this.l.a().a((io.reactivex.z)io.reactivex.v.a(i.a(this)));
   }

   private io.reactivex.v e() {
      io.reactivex.v var = this.e.profile();
      co.uk.getmondo.d.a.p var = new co.uk.getmondo.d.a.p();
      var.getClass();
      return var.d(o.a(var)).d(p.a()).f(q.a());
   }

   public io.reactivex.b a(co.uk.getmondo.create_account.a.a.a var) {
      return this.c.createAccount(SignupSource.LEGACY_PREPAID, var.a(), var.b().h()).a((io.reactivex.z)this.a()).c();
   }

   public io.reactivex.b a(co.uk.getmondo.signup_old.a.a.a var) {
      return this.b(var);
   }

   public io.reactivex.b a(String var) {
      String var = UUID.randomUUID().toString();
      this.a.b(var);
      this.a.a(var);
      return this.d.authorize(MonzoOAuthApi.a, var, "oauthclient_000097JsUCy1aF4Hud2iJN", "code", "https://monzo.com/-magic-auth", this.b(var, var));
   }

   public io.reactivex.v a() {
      io.reactivex.v var = this.c();
      io.reactivex.v var = this.e();
      io.reactivex.v var = this.d();
      co.uk.getmondo.common.accounts.m var = this.k;
      var.getClass();
      var = io.reactivex.v.a(var, var, var, c.a(var));
      co.uk.getmondo.common.accounts.d var = this.b;
      var.getClass();
      return var.c(d.a(var)).d(e.a(this)).c(f.a(this));
   }

   public io.reactivex.v a(ai var, String var, String var) {
      io.reactivex.v var = io.reactivex.v.c(z.a(this, var, var, var)).a(aa.a(this));
      co.uk.getmondo.common.accounts.d var = this.b;
      var.getClass();
      return var.c(ab.a(var)).d(ac.a(this)).d(ad.a(this)).c(ae.a(this));
   }

   public io.reactivex.v a(String var, String var) {
      return io.reactivex.v.a(x.a(this, var, var));
   }

   public io.reactivex.b b(String var) {
      return this.d.logOut("Bearer " + var).a(t.a(this));
   }

   public io.reactivex.v b() {
      io.reactivex.v var = this.c.waitlist();
      co.uk.getmondo.d.a.v var = new co.uk.getmondo.d.a.v();
      var.getClass();
      io.reactivex.v var = var.d(r.a(var));
      co.uk.getmondo.common.accounts.d var = this.b;
      var.getClass();
      return var.c(s.a(var));
   }
}
