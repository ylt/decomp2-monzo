package co.uk.getmondo.api.b;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import co.uk.getmondo.profile.data.MonzoProfileApi;

public final class af implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;

   static {
      boolean var;
      if(!af.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public af(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                                 if(!a && var == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var;
                                    if(!a && var == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var;
                                       if(!a && var == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var;
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new af(var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public a a() {
      return new a((co.uk.getmondo.common.s)this.b.b(), (co.uk.getmondo.common.accounts.d)this.c.b(), (MonzoApi)this.d.b(), (MonzoOAuthApi)this.e.b(), (MonzoProfileApi)this.f.b(), (co.uk.getmondo.card.c)this.g.b(), (co.uk.getmondo.common.k.f)this.h.b(), (co.uk.getmondo.common.a)this.i.b(), (co.uk.getmondo.common.q)this.j.b(), (co.uk.getmondo.common.g)this.k.b(), (co.uk.getmondo.common.accounts.m)this.l.b(), (co.uk.getmondo.payments.send.data.h)this.m.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
