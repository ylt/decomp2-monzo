package co.uk.getmondo.api.b;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class x implements Callable {
   private final a a;
   private final String b;
   private final String c;

   private x(a var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static Callable a(a var, String var, String var) {
      return new x(var, var, var);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c);
   }
}
