package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class ag implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!ag.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public ag(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new ag(var);
   }

   public ae a() {
      return new ae((OkHttpClient)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
