package co.uk.getmondo.api;

import kotlin.Metadata;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H'J\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00032\b\b\u0001\u0010\t\u001a\u00020\u0006H'J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H'¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/api/HelpApi;", "", "categories", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/help/Content;", "id", "", "search", "Lco/uk/getmondo/api/model/help/SearchQuery;", "query", "trending", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface HelpApi {
   @GET("help/content/categories/{id}")
   io.reactivex.v categories(@Path("id") String var);

   @GET("help/content/search")
   io.reactivex.v search(@Query("q") String var);

   @GET("help/content/trending")
   io.reactivex.v trending();
}
