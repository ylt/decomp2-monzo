package co.uk.getmondo.api;

import java.util.List;
import kotlin.Metadata;
import org.threeten.bp.LocalDate;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0007\bf\u0018\u00002\u00020\u0001JD\u0010\u0002\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u00052\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\u00052\b\b\u0001\u0010\t\u001a\u00020\u00072\b\b\u0001\u0010\n\u001a\u00020\u00052\b\b\u0001\u0010\u000b\u001a\u00020\u0007H'J\u0012\u0010\f\u001a\u00020\u00032\b\b\u0001\u0010\r\u001a\u00020\u0007H'J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH'J\b\u0010\u0011\u001a\u00020\u0003H'J&\u0010\u0012\u001a\u00020\u00032\b\b\u0001\u0010\u0013\u001a\u00020\u00072\b\b\u0001\u0010\u0014\u001a\u00020\u00072\b\b\u0001\u0010\u0015\u001a\u00020\u0007H'J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\u000fH'J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u000fH'J\"\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u000f2\b\b\u0001\u0010\u001c\u001a\u00020\u00072\b\b\u0001\u0010\u001d\u001a\u00020\u0007H'J\u0012\u0010\u001e\u001a\u00020\u00032\b\b\u0001\u0010\u001f\u001a\u00020\u0007H'J\u000e\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u000fH'J\b\u0010\"\u001a\u00020\u0003H'J\b\u0010#\u001a\u00020\u0003H'J(\u0010$\u001a\u00020\u00032\b\b\u0001\u0010%\u001a\u00020\u00072\n\b\u0003\u0010&\u001a\u0004\u0018\u00010\u00072\b\b\u0001\u0010'\u001a\u00020(H'JB\u0010)\u001a\u00020\u00032\n\b\u0003\u0010*\u001a\u0004\u0018\u00010\u00072\u000e\b\u0001\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00070,2\b\b\u0001\u0010-\u001a\u00020\u00072\b\b\u0001\u0010\u001d\u001a\u00020\u00072\b\b\u0001\u0010\u001c\u001a\u00020\u0007H'J\u001c\u0010.\u001a\u00020\u00032\b\b\u0001\u0010/\u001a\u00020\u00052\b\b\u0001\u00100\u001a\u00020\u0005H'J\u001c\u00101\u001a\u00020\u00032\b\b\u0001\u0010\u001f\u001a\u00020\u00072\b\b\u0001\u00102\u001a\u00020\u0007H'¨\u00063"},
   d2 = {"Lco/uk/getmondo/api/SignupApi;", "", "acceptLegalDocuments", "Lio/reactivex/Completable;", "termsAndConditionsAccepted", "", "termsAndConditionsVersion", "", "privacyPolicyAccepted", "privacyPolicyVersion", "fscsInformationSheetAccepted", "fscsInformationSheetVersion", "activateCard", "pan", "cardOrderOptions", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;", "commitProfile", "createCardOrder", "nameType", "pinType", "pin", "legalDocuments", "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "profile", "Lco/uk/getmondo/api/model/signup/SignUpProfile;", "searchAddress", "Lco/uk/getmondo/api/model/AddressesReponse;", "countryCode", "postalCode", "sendPhoneNumber", "phoneNumber", "signupStatus", "Lco/uk/getmondo/api/model/signup/SignupInfo;", "skipFingerprintEnrolment", "startSignUp", "submitProfile", "legalName", "preferredName", "dateOfBirth", "Lorg/threeten/bp/LocalDate;", "submitProfileAddress", "addressId", "addressLines", "", "administrativeArea", "subscribeToMarketing", "optIn", "optOut", "verifyPhoneNumber", "code", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface SignupApi {
   @FormUrlEncoded
   @POST("signup/legal-documents/accept")
   io.reactivex.b acceptLegalDocuments(@Field("terms_and_conditions_accepted") boolean var, @Field("terms_and_conditions_version") String var, @Field("privacy_policy_accepted") boolean var, @Field("privacy_policy_version") String var, @Field("fscs_information_sheet_accepted") boolean var, @Field("fscs_information_sheet_version") String var);

   @FormUrlEncoded
   @POST("signup/personal-account/card/activate")
   io.reactivex.b activateCard(@Field("pan") String var);

   @GET("signup/personal-account/card-order/options")
   io.reactivex.v cardOrderOptions();

   @POST("signup/profile/commit")
   io.reactivex.b commitProfile();

   @FormUrlEncoded
   @POST("signup/personal-account/card-order/create")
   io.reactivex.b createCardOrder(@Field("name_type") String var, @Field("pin_type") String var, @Field("pin") String var);

   @GET("signup/legal-documents/documents")
   io.reactivex.v legalDocuments();

   @GET("signup/profile")
   io.reactivex.v profile();

   @GET("signup/profile/address/search")
   io.reactivex.v searchAddress(@Query("country_code") String var, @Query("postal_code") String var);

   @FormUrlEncoded
   @POST("signup/profile/phone/send")
   io.reactivex.b sendPhoneNumber(@Field("phone_number") String var);

   @GET("signup/personal-account/status")
   io.reactivex.v signupStatus();

   @POST("signup/secure-token/skip")
   io.reactivex.b skipFingerprintEnrolment();

   @POST("signup/personal-account/start")
   io.reactivex.b startSignUp();

   @FormUrlEncoded
   @PUT("signup/profile")
   io.reactivex.b submitProfile(@Field("legal_name") String var, @Field("preferred_name") String var, @Field("date_of_birth") LocalDate var);

   @FormUrlEncoded
   @PUT("signup/profile/address")
   io.reactivex.b submitProfileAddress(@Field("address_id") String var, @Field("address_lines[]") List var, @Field("administrative_area") String var, @Field("postal_code") String var, @Field("country_code") String var);

   @FormUrlEncoded
   @POST("signup/marketing/subscribe")
   io.reactivex.b subscribeToMarketing(@Field("opt_in") boolean var, @Field("opt_out") boolean var);

   @FormUrlEncoded
   @POST("signup/profile/phone/verify")
   io.reactivex.b verifyPhoneNumber(@Field("phone_number") String var, @Field("code") String var);

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class DefaultImpls {
      // $FF: synthetic method
      @FormUrlEncoded
      @PUT("signup/profile")
      public static io.reactivex.b submitProfile$default(SignupApi var, String var, String var, LocalDate var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: submitProfile");
         } else {
            if((var & 2) != 0) {
               var = (String)null;
            }

            return var.submitProfile(var, var, var);
         }
      }

      // $FF: synthetic method
      @FormUrlEncoded
      @PUT("signup/profile/address")
      public static io.reactivex.b submitProfileAddress$default(SignupApi var, String var, List var, String var, String var, String var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: submitProfileAddress");
         } else {
            if((var & 1) != 0) {
               var = (String)null;
            }

            return var.submitProfileAddress(var, var, var, var, var);
         }
      }
   }
}
