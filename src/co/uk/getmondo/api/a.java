package co.uk.getmondo.api;

import android.content.Context;
import android.provider.Settings.Secure;
import java.io.IOException;
import java.util.UUID;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class a implements Interceptor {
   private final Context a;
   private final co.uk.getmondo.common.s b;

   public a(Context var, co.uk.getmondo.common.s var) {
      this.a = var;
      this.b = var;
   }

   private String a() {
      String var = Secure.getString(this.a.getContentResolver(), "android_id");
      return UUID.nameUUIDFromBytes(var.getBytes()) + ";1";
   }

   private String b() {
      String var = this.b.c();
      String var = var;
      if(var == null) {
         var = UUID.randomUUID().toString();
         this.b.c(var);
      }

      return var;
   }

   private String c() {
      String var = this.b.d();
      String var = var;
      if(var == null) {
         var = UUID.randomUUID().toString();
         this.b.d(var);
      }

      return var;
   }

   public Response intercept(Interceptor.Chain var) throws IOException {
      Request var = var.request();
      Headers.Builder var = var.headers().newBuilder().add("Mondo-Build", String.valueOf(1140150)).add("Mondo-Version", "1.14.1").add("Prod", "1").add("Mondo-Idempotence", this.a()).add("User-Agent", co.uk.getmondo.common.k.p.g(System.getProperty("http.agent"))).add("Mondo-Session", this.b()).add("Mondo-Fingerprint", this.c()).add("__auth_v2", "true");
      return var.proceed(var.newBuilder().headers(var.build()).build());
   }
}
