package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(c var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(c var) {
      return new e(var);
   }

   public OkHttpClient a() {
      return (OkHttpClient)b.a.d.a(this.b.c(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
