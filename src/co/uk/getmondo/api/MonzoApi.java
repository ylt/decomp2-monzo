package co.uk.getmondo.api;

import co.uk.getmondo.api.model.signup.SignupSource;
import java.util.List;
import java.util.Map;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MonzoApi {
   @GET("account-settings")
   io.reactivex.v accountSettings(@Query("account_id") String var);

   @GET("accounts")
   io.reactivex.v accounts();

   @FormUrlEncoded
   @POST("card-activation/activate")
   io.reactivex.v activateCardBank(@Field("account_id") String var, @Field("card_pan") String var);

   @FormUrlEncoded
   @POST("card-activation/activate")
   io.reactivex.v activateCardPrepaid(@Field("account_id") String var, @Field("card_token") String var);

   @FormUrlEncoded
   @POST("card-replacement/activate")
   io.reactivex.v activateReplacementCard(@Field("account_id") String var, @Field("old_card_id") String var, @Field("new_card_token") String var);

   @FormUrlEncoded
   @POST("stripe/cards")
   io.reactivex.v addStripeCard(@Field("account_id") String var, @Field("stripe_token") String var, @Field("iin") String var, @Field("save") boolean var, @Field("idempotency_key") String var);

   @FormUrlEncoded
   @POST("signup/apply")
   io.reactivex.b applyForAccount(@Query("source") SignupSource var, @Field("v2_enabled") boolean var, @FieldMap Map var, @Field("address[street_address][]") String... var);

   @GET("balance")
   io.reactivex.v balance(@Query("account_id") String var);

   @GET("balance/limits")
   io.reactivex.v balanceLimits(@Query("account_id") String var);

   @GET("card/list")
   io.reactivex.v cards(@Query("account_id") String var);

   @FormUrlEncoded
   @POST("phone/check-code")
   io.reactivex.v checkVerificationCode(@Field("phone_number") String var, @Field("code") String var);

   @GET("config")
   io.reactivex.v config();

   @FormUrlEncoded
   @POST("signup/create")
   io.reactivex.b createAccount(@Query("source") SignupSource var, @FieldMap Map var, @Field("address[street_address][]") String... var);

   @FormUrlEncoded
   @POST("attachment/upload")
   io.reactivex.v createAttachmentUploadUrl(@Field("account_id") String var, @Field("file_name") String var, @Field("file_type") String var);

   @DELETE("feed/{id}")
   io.reactivex.b deleteFeedItem(@Path("id") String var);

   @FormUrlEncoded
   @POST("attachment/deregister")
   io.reactivex.b deregisterAttachment(@Field("id") String var);

   @GET("feed")
   io.reactivex.v feed(@Query("account_id") String var, @Query("start_time") String var);

   @GET("/card-dispatch/status")
   io.reactivex.v getCardDispatchStatus(@Query("account_id") String var);

   @GET("topup/status")
   io.reactivex.v getInitialTopupStatus(@Query("account_id") String var);

   @GET("golden-ticket/{id}")
   io.reactivex.v goldenTicketStatus(@Path("id") String var);

   @GET("intercom/tokens/{intercom_app_id}")
   io.reactivex.v intercomUserHash(@Path("intercom_app_id") String var);

   @GET("geocode/postal-code-lookup")
   io.reactivex.v lookupPostcode(@Query("postal_code") String var, @Query("country") String var);

   @GET("news")
   io.reactivex.v news();

   @FormUrlEncoded
   @PUT("user-settings")
   io.reactivex.v optInToPeerToPeer(@Field("p2p_enabled") boolean var);

   @FormUrlEncoded
   @POST("card-replacement/order")
   io.reactivex.b orderReplacementCardBank(@Field("card_id") String var, @Field("address[street_address][]") String[] var, @Field("address[locality]") String var, @Field("address[administrative_area]") String var, @Field("address[postal_code]") String var, @Field("address[country]") String var);

   @FormUrlEncoded
   @POST("card-replacement/order")
   io.reactivex.v orderReplacementCardPrepaid(@Field("card_id") String var, @Field("address[street_address][]") String[] var, @Field("address[locality]") String var, @Field("address[administrative_area]") String var, @Field("address[postal_code]") String var, @Field("address[country]") String var);

   @FormUrlEncoded
   @POST("pin/sms")
   io.reactivex.b pinViaSms(@Field("account_id") String var, @Field("date_of_birth") String var);

   @GET("contact-discovery/query")
   io.reactivex.v queryContacts(@Query("hash[][phone]") List var);

   @FormUrlEncoded
   @POST("attachment/register")
   io.reactivex.v registerAttachment(@Field("account_id") String var, @Field("external_id") String var, @Field("file_url") String var);

   @FormUrlEncoded
   @POST("fcm/register")
   io.reactivex.b registerForFcm(@Field("device_token") String var, @Field("app_id") String var);

   @FormUrlEncoded
   @POST("phone/send-code")
   io.reactivex.b sendVerificationCode(@Field("phone_number") String var);

   @FormUrlEncoded
   @POST("signup")
   io.reactivex.v signup(@Query("source") SignupSource var, @Field("v2_enabled") boolean var);

   @GET("pin/sms_blocked")
   io.reactivex.v smsPinBlocked(@Query("account_id") String var);

   @GET("stripe/cards")
   io.reactivex.v stripeCards(@Query("account_id") String var);

   @FormUrlEncoded
   @POST("stripe/three_d_secure")
   io.reactivex.v threeDSecure(@Field("account_id") String var, @Field("currency") String var, @Field("amount") long var, @Field("is_initial") boolean var, @Field("stripe_card_id") String var, @Field("return_url") String var, @Field("idempotency_key") String var);

   @FormUrlEncoded
   @PUT("card/toggle")
   io.reactivex.b toggleCard(@Field("card_id") String var, @Field("status") co.uk.getmondo.api.model.a var);

   @GET("topup/limits")
   io.reactivex.v topUpLimits(@Query("account_id") String var);

   @FormUrlEncoded
   @POST("stripe/top_up")
   io.reactivex.b topUpThreeDSecure(@Field("account_id") String var, @Field("stripe_source") String var, @Field("currency") String var, @Field("amount") long var, @Field("is_initial") boolean var, @Field("three_d_secure_decision") String var, @Field("idempotency_key") String var);

   @FormUrlEncoded
   @POST("transactions/update-metadata")
   io.reactivex.v updateNotes(@Field("transaction_id") String var, @Field("metadata[notes]") String var);

   @FormUrlEncoded
   @PUT("account-settings")
   io.reactivex.v updateSettings(@Field("account_id") String var, @Field("magstripe_atm_enabled") boolean var);

   @FormUrlEncoded
   @PATCH("transactions/{transaction_id}")
   io.reactivex.b updateTransactionCategory(@Path("transaction_id") String var, @Field("category") String var);

   @GET("waitlist")
   io.reactivex.v waitlist();

   @FormUrlEncoded
   @POST("waitlist/signup")
   io.reactivex.v waitlistSignUp(@FieldMap Map var);
}
