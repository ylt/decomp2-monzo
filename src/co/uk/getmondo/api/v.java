package co.uk.getmondo.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public final class v implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!v.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public v(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new v(var, var, var, var, var);
   }

   public ServiceStatusApi a() {
      return (ServiceStatusApi)b.a.d.a(this.b.a((OkHttpClient)this.c.b(), (HttpLoggingInterceptor)this.d.b(), (com.google.gson.f)this.e.b(), (String)this.f.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
