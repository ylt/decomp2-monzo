package co.uk.getmondo.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ae {
   private final OkHttpClient a;

   public ae(OkHttpClient var) {
      this.a = var.newBuilder().writeTimeout(1L, TimeUnit.MINUTES).readTimeout(1L, TimeUnit.MINUTES).build();
   }

   // $FF: synthetic method
   static void a(ae var, InputStream var, long var, String var, String var) throws Exception {
      ah var = new ah(var, var, var);
      Request var = (new Request.Builder()).url(var).put(var).build();
      Response var = var.a.newCall(var).execute();
      if(!var.isSuccessful()) {
         throw new IOException("Error uploading file: " + var);
      }
   }

   public io.reactivex.b a(String var, InputStream var, long var, String var) {
      return io.reactivex.b.a(af.a(this, var, var, var, var));
   }
}
