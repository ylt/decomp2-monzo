package co.uk.getmondo.api;

import retrofit2.Retrofit;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!l.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public l(c var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var) {
      return new l(var, var);
   }

   public IdentityVerificationApi a() {
      return (IdentityVerificationApi)b.a.d.a(this.b.b((Retrofit)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
