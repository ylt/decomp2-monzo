package co.uk.getmondo.api;

import co.uk.getmondo.api.model.identity_verification.ContentType;
import co.uk.getmondo.api.model.identity_verification.FileType;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import kotlin.Metadata;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H'J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0003H'JH\u0010\n\u001a\u00020\u000b2\b\b\u0001\u0010\f\u001a\u00020\r2\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\b\b\u0001\u0010\u0010\u001a\u00020\u00062\b\b\u0001\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\n\b\u0003\u0010\u0014\u001a\u0004\u0018\u00010\u0006H'J&\u0010\u0015\u001a\u00020\u000b2\b\b\u0001\u0010\u0016\u001a\u00020\u00062\b\b\u0001\u0010\u0017\u001a\u00020\u00062\b\b\u0001\u0010\u0018\u001a\u00020\u0006H'J&\u0010\u0019\u001a\u00020\u000b2\b\b\u0001\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u001a\u001a\u00020\u00062\b\b\u0001\u0010\u0011\u001a\u00020\u0012H'J@\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00032\b\b\u0001\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u000e\u001a\u00020\u001d2\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u001e\u001a\u00020\u001f2\b\b\u0001\u0010 \u001a\u00020!H'J\u0018\u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\u00032\b\b\u0001\u0010\f\u001a\u00020\rH'J\u0012\u0010$\u001a\u00020\u000b2\b\b\u0001\u0010\f\u001a\u00020\rH'¨\u0006%"},
   d2 = {"Lco/uk/getmondo/api/IdentityVerificationApi;", "", "createKYCUploadUrl", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;", "fileName", "", "fileType", "kycStatus", "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;", "registerIdentityDocument", "Lio/reactivex/Completable;", "source", "Lco/uk/getmondo/api/model/signup/SignupSource;", "type", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "country", "systemCameraUsed", "", "primaryFileId", "secondaryFileId", "registerKYCUrl", "fileUrl", "groupId", "evidenceType", "registerSelfieVideo", "fileId", "requestFileUpload", "Lco/uk/getmondo/api/model/identity_verification/FileUpload;", "Lco/uk/getmondo/api/model/identity_verification/FileType;", "contentType", "Lco/uk/getmondo/api/model/identity_verification/ContentType;", "contentLength", "", "status", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "submit", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface IdentityVerificationApi {
   @FormUrlEncoded
   @POST("kyc/upload")
   io.reactivex.v createKYCUploadUrl(@Field("file_name") String var, @Field("file_type") String var);

   @GET("kyc/status")
   io.reactivex.v kycStatus();

   @FormUrlEncoded
   @POST("identity-verification/register-identity-document")
   io.reactivex.b registerIdentityDocument(@Query("source") SignupSource var, @Field("type") IdentityDocumentType var, @Field("issuing_country") String var, @Field("system_camera_used") boolean var, @Field("primary_image_file_id") String var, @Field("secondary_image_file_id") String var);

   @FormUrlEncoded
   @POST("kyc/register")
   io.reactivex.b registerKYCUrl(@Field("file_url") String var, @Field("group_id") String var, @Field("evidence_type") String var);

   @FormUrlEncoded
   @POST("identity-verification/register-selfie-video")
   io.reactivex.b registerSelfieVideo(@Query("source") SignupSource var, @Field("file_id") String var, @Field("system_camera_used") boolean var);

   @FormUrlEncoded
   @POST("identity-verification/request-file-upload")
   io.reactivex.v requestFileUpload(@Query("source") SignupSource var, @Field("type") FileType var, @Field("file_name") String var, @Field("content_type") ContentType var, @Field("content_length") long var);

   @GET("identity-verification/status")
   io.reactivex.v status(@Query("source") SignupSource var);

   @POST("identity-verification/submit")
   io.reactivex.b submit(@Query("source") SignupSource var);

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class DefaultImpls {
      // $FF: synthetic method
      @FormUrlEncoded
      @POST("identity-verification/register-identity-document")
      public static io.reactivex.b registerIdentityDocument$default(IdentityVerificationApi var, SignupSource var, IdentityDocumentType var, String var, boolean var, String var, String var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: registerIdentityDocument");
         } else {
            if((var & 32) != 0) {
               var = (String)null;
            }

            return var.registerIdentityDocument(var, var, var, var, var, var);
         }
      }
   }
}
