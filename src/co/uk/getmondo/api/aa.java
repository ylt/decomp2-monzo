package co.uk.getmondo.api;

public final class aa implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final y b;

   static {
      boolean var;
      if(!aa.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public aa(y var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(y var) {
      return new aa(var);
   }

   public String a() {
      return (String)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
