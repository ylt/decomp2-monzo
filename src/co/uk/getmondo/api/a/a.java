package co.uk.getmondo.api.a;

import co.uk.getmondo.common.k.p;
import co.uk.getmondo.create_account.b;
import java.util.HashMap;
import java.util.Map;

public class a {
   private final String a;
   private final String b;
   private final String c;
   private final String d;
   private final String e;

   public a(String var, String var, String var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public Map a() {
      HashMap var = new HashMap();
      var.put("email", this.a);
      var.put("name", this.b);
      var.put("country", this.c);
      var.put("date_of_birth", b.c(this.d));
      if(p.c(this.e)) {
         var.put("postal_code", this.e);
      }

      var.put("current_account", Boolean.TRUE);
      return var;
   }
}
