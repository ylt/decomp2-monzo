package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class n implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;

   static {
      boolean var;
      if(!n.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public n(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new n(var, var, var, var, var, var, var);
   }

   public AnalyticsApi a() {
      return (AnalyticsApi)b.a.d.a(this.b.a((OkHttpClient)this.c.b(), (com.google.gson.f)this.d.b(), (co.uk.getmondo.api.authentication.b)this.e.b(), (ac)this.f.b(), (co.uk.getmondo.api.authentication.k)this.g.b(), (String)this.h.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
