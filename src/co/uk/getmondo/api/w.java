package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class w implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!w.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public w(c var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new w(var, var, var, var);
   }

   public SignupApi a() {
      return (SignupApi)b.a.d.a(this.b.d((OkHttpClient)this.c.b(), (com.squareup.moshi.v)this.d.b(), (String)this.e.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
