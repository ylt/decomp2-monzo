package co.uk.getmondo.api;

import retrofit2.http.GET;

public interface ServiceStatusApi {
   @GET("api/v2/incidents/unresolved.json")
   io.reactivex.v getUnresolvedIncidents();
}
