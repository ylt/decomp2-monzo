package co.uk.getmondo.api;

import android.content.Context;

public final class ad implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!ad.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public ad(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new ad(var, var);
   }

   public ac a() {
      return new ac((Context)this.b.b(), (com.google.gson.f)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
