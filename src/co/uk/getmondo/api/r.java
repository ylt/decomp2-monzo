package co.uk.getmondo.api;

import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import okhttp3.OkHttpClient;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!r.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public r(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new r(var, var, var, var, var);
   }

   public MonzoOAuthApi a() {
      return (MonzoOAuthApi)b.a.d.a(this.b.a((OkHttpClient)this.c.b(), (com.google.gson.f)this.d.b(), (ac)this.e.b(), (String)this.f.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
