package co.uk.getmondo.api;

import okhttp3.logging.HttpLoggingInterceptor;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var;
      if(!k.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public k(c var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(c var) {
      return new k(var);
   }

   public HttpLoggingInterceptor a() {
      return (HttpLoggingInterceptor)b.a.d.a(this.b.d(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
