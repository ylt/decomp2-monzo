package co.uk.getmondo.api;

import kotlin.Metadata;
import retrofit2.http.GET;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H'¨\u0006\u0005"},
   d2 = {"Lco/uk/getmondo/api/MigrationApi;", "", "migrationInfo", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface MigrationApi {
   @GET("prepaid-migration-whitelist")
   io.reactivex.v migrationInfo();
}
