package co.uk.getmondo.api;

import java.io.IOException;
import java.io.InputStream;
import okhttp3.MediaType;
import okhttp3.RequestBody;

class ah extends RequestBody {
   private final InputStream a;
   private final long b;
   private final String c;

   ah(InputStream var, long var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public long contentLength() {
      return this.b;
   }

   public MediaType contentType() {
      return MediaType.parse(this.c);
   }

   public void writeTo(c.d param1) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
