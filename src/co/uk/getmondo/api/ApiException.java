package co.uk.getmondo.api;

import java.io.IOException;
import java.util.Locale;

public class ApiException extends IOException {
   private final int a;
   private final String b;
   private final String c;
   private final co.uk.getmondo.api.model.b d;

   public ApiException(int var, String var, String var, co.uk.getmondo.api.model.b var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public boolean a() {
      boolean var;
      if(this.a == 404) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public int b() {
      return this.a;
   }

   public String c() {
      return this.b;
   }

   public String d() {
      return this.c;
   }

   public co.uk.getmondo.api.model.b e() {
      return this.d;
   }

   public String getMessage() {
      String var;
      if(this.d != null) {
         var = this.d.toString();
      } else {
         var = "";
      }

      return String.format(Locale.ENGLISH, "Request to %s failed with %d due to: %s", new Object[]{this.b, Integer.valueOf(this.a), var});
   }
}
