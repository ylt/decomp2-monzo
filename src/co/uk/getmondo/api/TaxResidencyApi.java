package co.uk.getmondo.api;

import kotlin.Metadata;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u0005H'J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H'J\b\u0010\t\u001a\u00020\u0003H'J1\u0010\n\u001a\u00020\u00032\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\r\u001a\u00020\f2\u000e\b\u0001\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u000fH'¢\u0006\u0002\u0010\u0010J&\u0010\u0011\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u00052\b\b\u0001\u0010\u0012\u001a\u00020\u00052\b\b\u0001\u0010\u0013\u001a\u00020\u0005H'¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/api/TaxResidencyApi;", "", "noTin", "Lio/reactivex/Completable;", "countryCode", "", "status", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;", "submit", "updateSelfCertification", "summaryScreen", "", "usCitizen", "countryCodes", "", "(ZZ[Ljava/lang/String;)Lio/reactivex/Completable;", "updateTin", "type", "value", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface TaxResidencyApi {
   @FormUrlEncoded
   @POST("tax-residency/no-tin")
   io.reactivex.b noTin(@Field("country_code") String var);

   @GET("tax-residency/status")
   io.reactivex.v status();

   @POST("tax-residency/submit")
   io.reactivex.b submit();

   @FormUrlEncoded
   @POST("tax-residency/update-self-certification")
   io.reactivex.b updateSelfCertification(@Field("from_summary_screen") boolean var, @Field("us_citizen") boolean var, @Field("country_codes[]") String[] var);

   @FormUrlEncoded
   @POST("tax-residency/update-tin")
   io.reactivex.b updateTin(@Field("country_code") String var, @Field("type") String var, @Field("value") String var);
}
