package co.uk.getmondo.api;

import kotlin.Metadata;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H'J,\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\b\u001a\u00020\t2\b\b\u0001\u0010\n\u001a\u00020\u0006H'¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/api/OverdraftApi;", "", "overdraftStatus", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;", "accountId", "", "setOverdraftLimit", "limitAmount", "", "currency", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface OverdraftApi {
   @GET("/overdraft/status")
   io.reactivex.v overdraftStatus(@Query("account_id") String var);

   @FormUrlEncoded
   @PUT("/overdraft/limit")
   io.reactivex.v setOverdraftLimit(@Field("account_id") String var, @Field("limit_amount") long var, @Field("currency") String var);
}
