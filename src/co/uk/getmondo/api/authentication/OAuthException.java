package co.uk.getmondo.api.authentication;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/api/authentication/OAuthException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "email", "", "(Ljava/lang/String;)V", "getEmail", "()Ljava/lang/String;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class OAuthException extends RuntimeException {
   private final String a;

   public OAuthException(String var) {
      kotlin.d.b.l.b(var, "email");
      super();
      this.a = var;
   }

   public final String a() {
      return this.a;
   }
}
