package co.uk.getmondo.api.authentication;

import io.reactivex.v;
import okhttp3.Credentials;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MonzoOAuthApi {
   String a = Credentials.basic("oauthclient_000097JsUCy1aF4Hud2iJN", "mxWdehPCSLZCqzGFeNnB06mS3PKF+8mO32v8tElCaj9MhW5trl7LmnmZmaxFkfCsDuEuBJR5tOcJmvYKwQaX");

   @FormUrlEncoded
   @POST("oauth2/authorize")
   io.reactivex.b authorize(@Header("Authorization") String var, @Field("email") String var, @Field("client_id") String var, @Field("response_type") String var, @Field("redirect_uri") String var, @Field("state") String var);

   @POST("oauth2/logout")
   io.reactivex.b logOut(@Header("Authorization") String var);

   @FormUrlEncoded
   @POST("oauth2/token")
   v refreshToken(@Header("Authorization") String var, @Field("grant_type") String var, @Field("refresh_token") String var, @Field("refresh_attempt") String var);

   @FormUrlEncoded
   @POST("oauth2/token")
   v requestClientToken(@Header("Authorization") String var, @Field("grant_type") String var);

   @FormUrlEncoded
   @POST("oauth2/token")
   v requestUserToken(@Header("Authorization") String var, @Field("grant_type") String var, @Field("code") String var, @Field("redirect_uri") String var);
}
