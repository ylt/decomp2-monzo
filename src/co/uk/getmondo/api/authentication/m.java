package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.a.s;
import io.reactivex.v;
import java.util.UUID;

public class m {
   private final MonzoOAuthApi a;
   private final co.uk.getmondo.common.accounts.o b;

   m(MonzoOAuthApi var, co.uk.getmondo.common.accounts.o var) {
      this.a = var;
      this.b = var;
   }

   public static String a() {
      return UUID.randomUUID().toString();
   }

   // $FF: synthetic method
   static boolean a(Integer var, Throwable var) throws Exception {
      boolean var = true;
      boolean var;
      if(var.intValue() >= 3) {
         var = false;
      } else {
         var = var;
         if(var instanceof ApiException) {
            int var = ((ApiException)var).b();
            if(var != 401) {
               var = var;
               if(var != 403) {
                  return var;
               }
            }

            var = false;
         }
      }

      return var;
   }

   v a(ai var) {
      v var = this.a.refreshToken(MonzoOAuthApi.a, "refresh_token", var.c().a(), var.c().b()).a(n.a());
      s var = new s();
      var.getClass();
      var = var.d(o.a(var));
      co.uk.getmondo.common.accounts.o var = this.b;
      var.getClass();
      return var.c(p.a(var));
   }
}
