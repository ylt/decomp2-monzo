package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class b implements Interceptor {
   private final d a;
   private final co.uk.getmondo.common.accounts.o b;

   b(d var, co.uk.getmondo.common.accounts.o var) {
      this.a = var;
      this.b = var;
   }

   private String a() {
      String var = this.b.a();
      if(var == null) {
         var = ((ApiToken)this.a.a().b()).a();
      }

      return var;
   }

   public Response intercept(Interceptor.Chain var) throws IOException {
      Request var = var.request();
      String var = this.a();
      Response var;
      if(var == null) {
         var = var.proceed(var);
      } else {
         var = var.proceed(var.newBuilder().header("Authorization", "Bearer " + var).build());
      }

      return var;
   }
}
