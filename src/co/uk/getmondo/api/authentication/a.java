package co.uk.getmondo.api.authentication;

public enum a implements co.uk.getmondo.common.e.f {
   a("unauthorized.bad_access_token.evicted"),
   b("unauthorized.bad_access_token.expired"),
   c("unauthorized.bad_access_token"),
   d("unauthorized.bad_refresh_token"),
   e("unauthorized.bad_authorization_code"),
   f("unauthorized"),
   g("forbidden");

   private final String h;

   private a(String var) {
      this.h = var;
   }

   public String a() {
      return this.h;
   }

   public boolean b() {
      boolean var;
      if(this != a && this != d) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }
}
