package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import co.uk.getmondo.d.ai;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class k implements Authenticator {
   static final long a;
   private final co.uk.getmondo.common.accounts.o b;
   private final d c;
   private final com.google.gson.f d;
   private final m e;

   static {
      a = TimeUnit.SECONDS.toMillis(30L);
   }

   k(co.uk.getmondo.common.accounts.o var, d var, com.google.gson.f var, m var) {
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   private String a(Request var) {
      String var = var.header("Authorization");
      if(var != null && var.contains("Bearer ")) {
         var = var.replace("Bearer ", "").trim();
      } else {
         var = null;
      }

      return var;
   }

   private Request a(Response var, String var) {
      ApiToken var = (ApiToken)this.c.a().b();
      String var = var.a();
      if(var.a().equals(var)) {
         var = ((ApiToken)this.c.b().b()).a();
      } else {
         var = var;
      }

      d.a.a.a("Proceeding with new client token %s", new Object[]{var});
      return this.b(var, var);
   }

   private Request a(Response var, String var, ai var) {
      Object var = null;
      Request var;
      if(var == null) {
         d.a.a.d("Attempted to refresh user token, but there isn't an existing user token saved", new Object[0]);
         var = (Request)var;
      } else {
         if(!var.b().equals(var)) {
            var = var.b();
         } else {
            if(Long.valueOf(var.a()).longValue() < a) {
               d.a.a.d("Illegal refresh. User token has attempted to be refreshed within less than %d seconds of its creation", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(a))});
               var = (Request)var;
               return var;
            }

            var = ((ai)this.e.a(var).b()).b();
         }

         d.a.a.a("Proceeding with new user token %s", new Object[]{var});
         var = this.b(var, var);
      }

      return var;
   }

   private Request b(Response var, String var) {
      return var.request().newBuilder().header("Authorization", "Bearer " + var).build();
   }

   public Request authenticate(Route param1, Response param2) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
