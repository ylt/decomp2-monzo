package co.uk.getmondo.api.authentication;

public final class q implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!q.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public q(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new q(var, var);
   }

   public m a() {
      return new m((MonzoOAuthApi)this.b.b(), (co.uk.getmondo.common.accounts.o)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
