package co.uk.getmondo.api.authentication;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!h.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public h(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new h(var, var);
   }

   public d a() {
      return new d((i)this.b.b(), (MonzoOAuthApi)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
