package co.uk.getmondo.api.authentication;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class e implements Callable {
   private final d a;

   private e(d var) {
      this.a = var;
   }

   public static Callable a(d var) {
      return new e(var);
   }

   public Object call() {
      return d.a(this.a);
   }
}
