package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import io.reactivex.v;
import io.reactivex.z;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class d {
   private final i a;
   private final MonzoOAuthApi b;
   private final Set c = Collections.synchronizedSet(new HashSet());

   public d(i var, MonzoOAuthApi var) {
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   static z a(d var) throws Exception {
      ApiToken var = var.a.a();
      v var;
      if(var == null) {
         var = var.b();
      } else {
         var = v.a((Object)var);
      }

      return var;
   }

   // $FF: synthetic method
   static void a(d var, ApiToken var) throws Exception {
      var.a.a(var);
      d.a.a.b("New client token created %s", new Object[]{var.a()});
   }

   // $FF: synthetic method
   static void b(d var, ApiToken var) throws Exception {
      var.c.add(var.a());
   }

   v a() {
      return v.a(e.a(this)).c(f.a(this));
   }

   boolean a(String var) {
      return this.c.contains(var);
   }

   v b() {
      return this.b.requestClientToken(MonzoOAuthApi.a, "client_credentials").c(g.a(this));
   }
}
