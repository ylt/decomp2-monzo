package co.uk.getmondo.api.authentication;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.api.model.ApiToken;

public class i {
   private final com.google.gson.f a;
   private SharedPreferences b;

   i(Context var, com.google.gson.f var) {
      this.a = var;
      this.b = var.getSharedPreferences("client-storage", 0);
   }

   ApiToken a() {
      ApiToken var = null;
      String var = this.b.getString("KEY_TOKEN", (String)null);
      if(var != null) {
         var = (ApiToken)this.a.a(var, ApiToken.class);
      }

      return var;
   }

   void a(ApiToken var) {
      this.b.edit().putString("KEY_TOKEN", this.a.a(var)).apply();
   }
}
