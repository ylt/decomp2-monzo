package co.uk.getmondo.api;

import android.content.Context;

public final class b implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!b.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public b(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new b(var, var);
   }

   public a a() {
      return new a((Context)this.b.b(), (co.uk.getmondo.common.s)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
