package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001d\b\u0086\b\u0018\u00002\u00020\u0001B[\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0001\u0010\t\u001a\u00020\n\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\bHÆ\u0003J\t\u0010\"\u001a\u00020\nHÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\u000eHÆ\u0003Je\u0010&\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0003\u0010\t\u001a\u00020\n2\b\b\u0003\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\u00032\b\b\u0003\u0010\r\u001a\u00020\u000eHÆ\u0001J\u0013\u0010'\u001a\u00020\n2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010)\u001a\u00020\u000eHÖ\u0001J\t\u0010*\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0015R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0015R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001c¨\u0006+"},
   d2 = {"Lco/uk/getmondo/api/model/ApiProfile;", "", "userId", "", "name", "preferredName", "email", "address", "Lco/uk/getmondo/api/model/LegacyApiAddress;", "addressUpdatable", "", "dateOfBirth", "phoneNumber", "userNumber", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/LegacyApiAddress;ZLjava/lang/String;Ljava/lang/String;I)V", "getAddress", "()Lco/uk/getmondo/api/model/LegacyApiAddress;", "getAddressUpdatable", "()Z", "getDateOfBirth", "()Ljava/lang/String;", "getEmail", "getName", "getPhoneNumber", "getPreferredName", "getUserId", "getUserNumber", "()I", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiProfile {
   private final LegacyApiAddress address;
   private final boolean addressUpdatable;
   private final String dateOfBirth;
   private final String email;
   private final String name;
   private final String phoneNumber;
   private final String preferredName;
   private final String userId;
   private final int userNumber;

   public ApiProfile(@h(a = "user_id") String var, String var, @h(a = "preferred_name") String var, String var, LegacyApiAddress var, @h(a = "address_updatable") boolean var, @h(a = "date_of_birth") String var, @h(a = "phone_number") String var, @h(a = "user_number") int var) {
      l.b(var, "userId");
      l.b(var, "name");
      l.b(var, "email");
      l.b(var, "address");
      l.b(var, "dateOfBirth");
      l.b(var, "phoneNumber");
      super();
      this.userId = var;
      this.name = var;
      this.preferredName = var;
      this.email = var;
      this.address = var;
      this.addressUpdatable = var;
      this.dateOfBirth = var;
      this.phoneNumber = var;
      this.userNumber = var;
   }

   public final String a() {
      return this.userId;
   }

   public final String b() {
      return this.name;
   }

   public final String c() {
      return this.preferredName;
   }

   public final String d() {
      return this.email;
   }

   public final LegacyApiAddress e() {
      return this.address;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiProfile)) {
            return var;
         }

         ApiProfile var = (ApiProfile)var;
         var = var;
         if(!l.a(this.userId, var.userId)) {
            return var;
         }

         var = var;
         if(!l.a(this.name, var.name)) {
            return var;
         }

         var = var;
         if(!l.a(this.preferredName, var.preferredName)) {
            return var;
         }

         var = var;
         if(!l.a(this.email, var.email)) {
            return var;
         }

         var = var;
         if(!l.a(this.address, var.address)) {
            return var;
         }

         boolean var;
         if(this.addressUpdatable == var.addressUpdatable) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.dateOfBirth, var.dateOfBirth)) {
            return var;
         }

         var = var;
         if(!l.a(this.phoneNumber, var.phoneNumber)) {
            return var;
         }

         if(this.userNumber == var.userNumber) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final boolean f() {
      return this.addressUpdatable;
   }

   public final String g() {
      return this.dateOfBirth;
   }

   public final String h() {
      return this.phoneNumber;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final int i() {
      return this.userNumber;
   }

   public String toString() {
      return "ApiProfile(userId=" + this.userId + ", name=" + this.name + ", preferredName=" + this.preferredName + ", email=" + this.email + ", address=" + this.address + ", addressUpdatable=" + this.addressUpdatable + ", dateOfBirth=" + this.dateOfBirth + ", phoneNumber=" + this.phoneNumber + ", userNumber=" + this.userNumber + ")";
   }
}
