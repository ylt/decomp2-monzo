package co.uk.getmondo.api.model;

import java.util.Map;
import kotlin.Metadata;
import kotlin.a.ab;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u001c\b\u0002\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0005\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006J\u001d\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0005\u0018\u00010\u0003HÆ\u0003J'\u0010\n\u001a\u00020\u00002\u001c\b\u0002\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0005\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0004HÖ\u0001R%\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0005\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/api/model/ApiContactDiscovery;", "", "phone", "", "", "", "(Ljava/util/Map;)V", "getPhone", "()Ljava/util/Map;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiContactDiscovery {
   private final Map phone;

   public ApiContactDiscovery() {
      this((Map)null, 1, (i)null);
   }

   public ApiContactDiscovery(Map var) {
      this.phone = var;
   }

   // $FF: synthetic method
   public ApiContactDiscovery(Map var, int var, i var) {
      if((var & 1) != 0) {
         var = ab.a();
      }

      this(var);
   }

   public final Map a() {
      return this.phone;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label26: {
            if(var instanceof ApiContactDiscovery) {
               ApiContactDiscovery var = (ApiContactDiscovery)var;
               if(l.a(this.phone, var.phone)) {
                  break label26;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      Map var = this.phone;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var;
   }

   public String toString() {
      return "ApiContactDiscovery(phone=" + this.phone + ")";
   }
}
