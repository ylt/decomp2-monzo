package co.uk.getmondo.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.squareup.moshi.h;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\u0012\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B?\u0012\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0001\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\b\u0012\b\b\u0001\u0010\t\u001a\u00020\u0006\u0012\b\b\u0001\u0010\n\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0006¢\u0006\u0002\u0010\fJ\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00060\bHÆ\u0003J\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003JC\u0010\u0019\u001a\u00020\u00002\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0003\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\b2\b\b\u0003\u0010\t\u001a\u00020\u00062\b\b\u0003\u0010\n\u001a\u00020\u00062\b\b\u0003\u0010\u000b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0013\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fHÖ\u0003J\t\u0010 \u001a\u00020\u001bHÖ\u0001J\t\u0010!\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u001bH\u0016R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\t\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0014\u0010\u000b\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0014\u0010\n\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000e¨\u0006'"},
   d2 = {"Lco/uk/getmondo/api/model/ApiAddress;", "Lco/uk/getmondo/api/model/Address;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "addressId", "", "addressLines", "", "administrativeArea", "postalCode", "countryCode", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddressId", "()Ljava/lang/String;", "getAddressLines", "()Ljava/util/List;", "getAdministrativeArea", "getCountryCode", "getPostalCode", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAddress implements Address {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public ApiAddress a(Parcel var) {
         l.b(var, "source");
         return new ApiAddress(var);
      }

      public ApiAddress[] a(int var) {
         return new ApiAddress[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final ApiAddress.Companion Companion = new ApiAddress.Companion((i)null);
   private final String addressId;
   private final List addressLines;
   private final String administrativeArea;
   private final String countryCode;
   private final String postalCode;

   public ApiAddress(Parcel var) {
      l.b(var, "source");
      String var = var.readString();
      ArrayList var = var.createStringArrayList();
      l.a(var, "source.createStringArrayList()");
      List var = (List)var;
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      this(var, var, var, var, var);
   }

   public ApiAddress(@h(a = "address_id") String var, @h(a = "address_lines") List var, @h(a = "administrative_area") String var, @h(a = "postal_code") String var, @h(a = "country_code") String var) {
      l.b(var, "addressLines");
      l.b(var, "administrativeArea");
      l.b(var, "postalCode");
      l.b(var, "countryCode");
      super();
      this.addressId = var;
      this.addressLines = var;
      this.administrativeArea = var;
      this.postalCode = var;
      this.countryCode = var;
   }

   // $FF: synthetic method
   public ApiAddress(String var, List var, String var, String var, String var, int var, i var) {
      if((var & 1) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var);
   }

   public List a() {
      return this.addressLines;
   }

   public String b() {
      return this.administrativeArea;
   }

   public String c() {
      return this.postalCode;
   }

   public String d() {
      return this.countryCode;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return Address.DefaultImpls.a(this);
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof ApiAddress) {
               ApiAddress var = (ApiAddress)var;
               if(l.a(this.addressId, var.addressId) && l.a(this.a(), var.a()) && l.a(this.b(), var.b()) && l.a(this.c(), var.c()) && l.a(this.d(), var.d())) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.addressId;
   }

   public int hashCode() {
      int var = 0;
      String var = this.addressId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      List var = this.a();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d();
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiAddress(addressId=" + this.addressId + ", addressLines=" + this.a() + ", administrativeArea=" + this.b() + ", postalCode=" + this.c() + ", countryCode=" + this.d() + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeString(this.addressId);
      var.writeStringList(this.a());
      var.writeString(this.b());
      var.writeString(this.c());
      var.writeString(this.d());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/ApiAddress$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/ApiAddress;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }
   }
}
