package co.uk.getmondo.api.model.pin;

import kotlin.Metadata;
import kotlin.d.b.i;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/api/model/pin/ApiPinSms;", "", "smsBlocked", "", "(Ljava/lang/Boolean;)V", "getSmsBlocked", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPinSms {
   private final Boolean smsBlocked;

   public ApiPinSms() {
      this((Boolean)null, 1, (i)null);
   }

   public ApiPinSms(Boolean var) {
      this.smsBlocked = var;
   }

   // $FF: synthetic method
   public ApiPinSms(Boolean var, int var, i var) {
      if((var & 1) != 0) {
         var = (Boolean)null;
      }

      this(var);
   }

   public final Boolean a() {
      return this.smsBlocked;
   }
}
