package co.uk.getmondo.api.model.pin;

import co.uk.getmondo.common.e.f;

public enum a implements f {
   CARD_NOT_FOUND("not_found.card_not_found"),
   DOB_CHECK_FAILED("forbidden.dob_check_failed"),
   INCORRECT_DOB("bad_request.bad_param.date_of_birth"),
   NUMBER_CHANGE("forbidden.recent_phone_number_change"),
   NUMBER_NOT_FOUND("not_found.phone_number_not_found"),
   PROFILE_NOT_FOUND("not_found.profile_not_found");

   private final String prefix;

   private a(String var) {
      this.prefix = var;
   }

   public String a() {
      return this.prefix;
   }
}
