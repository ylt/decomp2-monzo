package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\b\b\u0001\u0010\b\u001a\u00020\u0004¢\u0006\u0002\u0010\tJ\u0011\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0004HÆ\u0003JC\u0010\u0016\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0003\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00042\b\b\u0003\u0010\b\u001a\u00020\u0004HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0004HÖ\u0001R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/api/model/LegacyApiAddress;", "", "streetAddress", "", "", "locality", "postalCode", "country", "administrativeArea", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAdministrativeArea", "()Ljava/lang/String;", "getCountry", "getLocality", "getPostalCode", "getStreetAddress", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LegacyApiAddress {
   private final String administrativeArea;
   private final String country;
   private final String locality;
   private final String postalCode;
   private final List streetAddress;

   public LegacyApiAddress(@h(a = "street_address") List var, String var, @h(a = "postal_code") String var, String var, @h(a = "administrative_area") String var) {
      l.b(var, "locality");
      l.b(var, "postalCode");
      l.b(var, "country");
      l.b(var, "administrativeArea");
      super();
      this.streetAddress = var;
      this.locality = var;
      this.postalCode = var;
      this.country = var;
      this.administrativeArea = var;
   }

   // $FF: synthetic method
   public LegacyApiAddress(List var, String var, String var, String var, String var, int var, i var) {
      if((var & 1) != 0) {
         var = m.a();
      }

      this(var, var, var, var, var);
   }

   public final List a() {
      return this.streetAddress;
   }

   public final String b() {
      return this.locality;
   }

   public final String c() {
      return this.postalCode;
   }

   public final String d() {
      return this.country;
   }

   public final String e() {
      return this.administrativeArea;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof LegacyApiAddress) {
               LegacyApiAddress var = (LegacyApiAddress)var;
               if(l.a(this.streetAddress, var.streetAddress) && l.a(this.locality, var.locality) && l.a(this.postalCode, var.postalCode) && l.a(this.country, var.country) && l.a(this.administrativeArea, var.administrativeArea)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      List var = this.streetAddress;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.locality;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.postalCode;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.country;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.administrativeArea;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "LegacyApiAddress(streetAddress=" + this.streetAddress + ", locality=" + this.locality + ", postalCode=" + this.postalCode + ", country=" + this.country + ", administrativeArea=" + this.administrativeArea + ")";
   }
}
