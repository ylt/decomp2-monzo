package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b/\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u00ad\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u0012\u0006\u0010\u0015\u001a\u00020\u0003\u0012\u0006\u0010\u0016\u001a\u00020\u0003\u0012\u0006\u0010\u0017\u001a\u00020\u0018¢\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0003HÆ\u0003J\t\u00103\u001a\u00020\u0003HÆ\u0003J\t\u00104\u001a\u00020\u0003HÆ\u0003J\t\u00105\u001a\u00020\u0003HÆ\u0003J\t\u00106\u001a\u00020\u0003HÆ\u0003J\t\u00107\u001a\u00020\u0003HÆ\u0003J\t\u00108\u001a\u00020\u0003HÆ\u0003J\t\u00109\u001a\u00020\u0003HÆ\u0003J\t\u0010:\u001a\u00020\u0003HÆ\u0003J\t\u0010;\u001a\u00020\u0003HÆ\u0003J\t\u0010<\u001a\u00020\u0003HÆ\u0003J\t\u0010=\u001a\u00020\u0003HÆ\u0003J\t\u0010>\u001a\u00020\u0018HÆ\u0003J\t\u0010?\u001a\u00020\u0003HÆ\u0003J\t\u0010@\u001a\u00020\u0003HÆ\u0003J\t\u0010A\u001a\u00020\u0003HÆ\u0003J\t\u0010B\u001a\u00020\u0003HÆ\u0003J\t\u0010C\u001a\u00020\u0003HÆ\u0003J\t\u0010D\u001a\u00020\u0003HÆ\u0003J\t\u0010E\u001a\u00020\u0003HÆ\u0003JÛ\u0001\u0010F\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00032\b\b\u0002\u0010\u000f\u001a\u00020\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u00032\b\b\u0002\u0010\u0014\u001a\u00020\u00032\b\b\u0002\u0010\u0015\u001a\u00020\u00032\b\b\u0002\u0010\u0016\u001a\u00020\u00032\b\b\u0002\u0010\u0017\u001a\u00020\u0018HÆ\u0001J\u0013\u0010G\u001a\u00020H2\b\u0010I\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010J\u001a\u00020KHÖ\u0001J\t\u0010L\u001a\u00020MHÖ\u0001R\u0011\u0010\u0010\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001bR\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001bR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001bR\u0011\u0010\u0014\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001bR\u0011\u0010\u0011\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001bR\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001bR\u0011\u0010\u0016\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001bR\u0011\u0010\u0015\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001bR\u0011\u0010\u0012\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\b/\u00100¨\u0006N"},
   d2 = {"Lco/uk/getmondo/api/model/ApiBalanceLimits;", "", "maxBalance", "", "maxSinglePosLimit", "dailyLoadLimitTotal", "dailyLoadLimit", "monthlyLoadLimitTotal", "monthlyLoadLimit", "annualLoadLimitTotal", "annualLoadLimit", "dailyCashLimitTotal", "dailyCashLimit", "monthlyCashLimitTotal", "monthlyCashLimit", "annualCashLimitTotal", "annualCashLimit", "maxSingleP2pLimit", "monthlyP2pLimitTotal", "monthlyP2pLimit", "inboundP2pMax", "monthlyInboundP2pLimitTotal", "monthlyInboundP2pLimit", "verificationType", "Lco/uk/getmondo/api/model/VerificationType;", "(DDDDDDDDDDDDDDDDDDDDLco/uk/getmondo/api/model/VerificationType;)V", "getAnnualCashLimit", "()D", "getAnnualCashLimitTotal", "getAnnualLoadLimit", "getAnnualLoadLimitTotal", "getDailyCashLimit", "getDailyCashLimitTotal", "getDailyLoadLimit", "getDailyLoadLimitTotal", "getInboundP2pMax", "getMaxBalance", "getMaxSingleP2pLimit", "getMaxSinglePosLimit", "getMonthlyCashLimit", "getMonthlyCashLimitTotal", "getMonthlyInboundP2pLimit", "getMonthlyInboundP2pLimitTotal", "getMonthlyLoadLimit", "getMonthlyLoadLimitTotal", "getMonthlyP2pLimit", "getMonthlyP2pLimitTotal", "getVerificationType", "()Lco/uk/getmondo/api/model/VerificationType;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiBalanceLimits {
   private final double annualCashLimit;
   private final double annualCashLimitTotal;
   private final double annualLoadLimit;
   private final double annualLoadLimitTotal;
   private final double dailyCashLimit;
   private final double dailyCashLimitTotal;
   private final double dailyLoadLimit;
   private final double dailyLoadLimitTotal;
   private final double inboundP2pMax;
   private final double maxBalance;
   private final double maxSingleP2pLimit;
   private final double maxSinglePosLimit;
   private final double monthlyCashLimit;
   private final double monthlyCashLimitTotal;
   private final double monthlyInboundP2pLimit;
   private final double monthlyInboundP2pLimitTotal;
   private final double monthlyLoadLimit;
   private final double monthlyLoadLimitTotal;
   private final double monthlyP2pLimit;
   private final double monthlyP2pLimitTotal;
   private final VerificationType verificationType;

   public ApiBalanceLimits(double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, double var, VerificationType var) {
      l.b(var, "verificationType");
      super();
      this.maxBalance = var;
      this.maxSinglePosLimit = var;
      this.dailyLoadLimitTotal = var;
      this.dailyLoadLimit = var;
      this.monthlyLoadLimitTotal = var;
      this.monthlyLoadLimit = var;
      this.annualLoadLimitTotal = var;
      this.annualLoadLimit = var;
      this.dailyCashLimitTotal = var;
      this.dailyCashLimit = var;
      this.monthlyCashLimitTotal = var;
      this.monthlyCashLimit = var;
      this.annualCashLimitTotal = var;
      this.annualCashLimit = var;
      this.maxSingleP2pLimit = var;
      this.monthlyP2pLimitTotal = var;
      this.monthlyP2pLimit = var;
      this.inboundP2pMax = var;
      this.monthlyInboundP2pLimitTotal = var;
      this.monthlyInboundP2pLimit = var;
      this.verificationType = var;
   }

   public final double a() {
      return this.maxBalance;
   }

   public final double b() {
      return this.maxSinglePosLimit;
   }

   public final double c() {
      return this.dailyLoadLimitTotal;
   }

   public final double d() {
      return this.dailyLoadLimit;
   }

   public final double e() {
      return this.monthlyLoadLimitTotal;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label66: {
            if(var instanceof ApiBalanceLimits) {
               ApiBalanceLimits var = (ApiBalanceLimits)var;
               if(Double.compare(this.maxBalance, var.maxBalance) == 0 && Double.compare(this.maxSinglePosLimit, var.maxSinglePosLimit) == 0 && Double.compare(this.dailyLoadLimitTotal, var.dailyLoadLimitTotal) == 0 && Double.compare(this.dailyLoadLimit, var.dailyLoadLimit) == 0 && Double.compare(this.monthlyLoadLimitTotal, var.monthlyLoadLimitTotal) == 0 && Double.compare(this.monthlyLoadLimit, var.monthlyLoadLimit) == 0 && Double.compare(this.annualLoadLimitTotal, var.annualLoadLimitTotal) == 0 && Double.compare(this.annualLoadLimit, var.annualLoadLimit) == 0 && Double.compare(this.dailyCashLimitTotal, var.dailyCashLimitTotal) == 0 && Double.compare(this.dailyCashLimit, var.dailyCashLimit) == 0 && Double.compare(this.monthlyCashLimitTotal, var.monthlyCashLimitTotal) == 0 && Double.compare(this.monthlyCashLimit, var.monthlyCashLimit) == 0 && Double.compare(this.annualCashLimitTotal, var.annualCashLimitTotal) == 0 && Double.compare(this.annualCashLimit, var.annualCashLimit) == 0 && Double.compare(this.maxSingleP2pLimit, var.maxSingleP2pLimit) == 0 && Double.compare(this.monthlyP2pLimitTotal, var.monthlyP2pLimitTotal) == 0 && Double.compare(this.monthlyP2pLimit, var.monthlyP2pLimit) == 0 && Double.compare(this.inboundP2pMax, var.inboundP2pMax) == 0 && Double.compare(this.monthlyInboundP2pLimitTotal, var.monthlyInboundP2pLimitTotal) == 0 && Double.compare(this.monthlyInboundP2pLimit, var.monthlyInboundP2pLimit) == 0 && l.a(this.verificationType, var.verificationType)) {
                  break label66;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final double f() {
      return this.monthlyLoadLimit;
   }

   public final double g() {
      return this.annualLoadLimitTotal;
   }

   public final double h() {
      return this.annualLoadLimit;
   }

   public int hashCode() {
      long var = Double.doubleToLongBits(this.maxBalance);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.maxSinglePosLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.dailyLoadLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.dailyLoadLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyLoadLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyLoadLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.annualLoadLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.annualLoadLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.dailyCashLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.dailyCashLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyCashLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyCashLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.annualCashLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.annualCashLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.maxSingleP2pLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyP2pLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyP2pLimit);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.inboundP2pMax);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyInboundP2pLimitTotal);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.monthlyInboundP2pLimit);
      int var = (int)(var ^ var >>> 32);
      VerificationType var = this.verificationType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var + (((((((((((((((((((var * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31;
   }

   public final double i() {
      return this.dailyCashLimitTotal;
   }

   public final double j() {
      return this.dailyCashLimit;
   }

   public final double k() {
      return this.monthlyCashLimitTotal;
   }

   public final double l() {
      return this.monthlyCashLimit;
   }

   public final double m() {
      return this.annualCashLimitTotal;
   }

   public final double n() {
      return this.annualCashLimit;
   }

   public final double o() {
      return this.maxSingleP2pLimit;
   }

   public final double p() {
      return this.monthlyP2pLimitTotal;
   }

   public final double q() {
      return this.monthlyP2pLimit;
   }

   public final double r() {
      return this.inboundP2pMax;
   }

   public final double s() {
      return this.monthlyInboundP2pLimitTotal;
   }

   public final double t() {
      return this.monthlyInboundP2pLimit;
   }

   public String toString() {
      return "ApiBalanceLimits(maxBalance=" + this.maxBalance + ", maxSinglePosLimit=" + this.maxSinglePosLimit + ", dailyLoadLimitTotal=" + this.dailyLoadLimitTotal + ", dailyLoadLimit=" + this.dailyLoadLimit + ", monthlyLoadLimitTotal=" + this.monthlyLoadLimitTotal + ", monthlyLoadLimit=" + this.monthlyLoadLimit + ", annualLoadLimitTotal=" + this.annualLoadLimitTotal + ", annualLoadLimit=" + this.annualLoadLimit + ", dailyCashLimitTotal=" + this.dailyCashLimitTotal + ", dailyCashLimit=" + this.dailyCashLimit + ", monthlyCashLimitTotal=" + this.monthlyCashLimitTotal + ", monthlyCashLimit=" + this.monthlyCashLimit + ", annualCashLimitTotal=" + this.annualCashLimitTotal + ", annualCashLimit=" + this.annualCashLimit + ", maxSingleP2pLimit=" + this.maxSingleP2pLimit + ", monthlyP2pLimitTotal=" + this.monthlyP2pLimitTotal + ", monthlyP2pLimit=" + this.monthlyP2pLimit + ", inboundP2pMax=" + this.inboundP2pMax + ", monthlyInboundP2pLimitTotal=" + this.monthlyInboundP2pLimitTotal + ", monthlyInboundP2pLimit=" + this.monthlyInboundP2pLimit + ", verificationType=" + this.verificationType + ")";
   }

   public final VerificationType u() {
      return this.verificationType;
   }
}
