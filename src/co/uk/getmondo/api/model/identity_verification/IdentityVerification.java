package co.uk.getmondo.api.model.identity_verification;

import com.google.gson.a.c;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001%BO\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\b\u0012\b\b\u0002\u0010\n\u001a\u00020\b\u0012\b\b\u0002\u0010\u000b\u001a\u00020\b¢\u0006\u0002\u0010\fJ\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010\u001b\u001a\u00020\bHÆ\u0003J\t\u0010\u001c\u001a\u00020\bHÆ\u0003J\t\u0010\u001d\u001a\u00020\bHÆ\u0003J\t\u0010\u001e\u001a\u00020\bHÆ\u0003JU\u0010\u001f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\bHÆ\u0001J\u0013\u0010 \u001a\u00020\b2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\"\u001a\u00020#HÖ\u0001J\t\u0010$\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0011\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000eR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006&"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "", "rejectionReasonCode", "", "rejectionNote", "status", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;", "identityDocUploaded", "", "selfieUploaded", "allowPicker", "allowSystemCamera", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;ZZZZ)V", "getAllowPicker", "()Z", "getAllowSystemCamera", "getIdentityDocUploaded", "isVerified", "getRejectionNote", "()Ljava/lang/String;", "getRejectionReasonCode", "getSelfieUploaded", "getStatus", "()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "Status", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class IdentityVerification {
   private final boolean allowPicker;
   private final boolean allowSystemCamera;
   private final boolean identityDocUploaded;
   private final String rejectionNote;
   private final String rejectionReasonCode;
   private final boolean selfieUploaded;
   private final IdentityVerification.Status status;

   public IdentityVerification(String var, String var, IdentityVerification.Status var, boolean var, boolean var, boolean var, boolean var) {
      this.rejectionReasonCode = var;
      this.rejectionNote = var;
      this.status = var;
      this.identityDocUploaded = var;
      this.selfieUploaded = var;
      this.allowPicker = var;
      this.allowSystemCamera = var;
   }

   // $FF: synthetic method
   public IdentityVerification(String var, String var, IdentityVerification.Status var, boolean var, boolean var, boolean var, boolean var, int var, i var) {
      if((var & 1) != 0) {
         var = (String)null;
      }

      if((var & 2) != 0) {
         var = (String)null;
      }

      if((var & 8) != 0) {
         var = false;
      }

      if((var & 16) != 0) {
         var = false;
      }

      if((var & 32) != 0) {
         var = false;
      }

      if((var & 64) != 0) {
         var = false;
      }

      this(var, var, var, var, var, var, var);
   }

   public final String a() {
      return this.rejectionNote;
   }

   public final IdentityVerification.Status b() {
      return this.status;
   }

   public final boolean c() {
      return this.allowSystemCamera;
   }

   public final String d() {
      return this.rejectionNote;
   }

   public final IdentityVerification.Status e() {
      return this.status;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof IdentityVerification)) {
            return var;
         }

         IdentityVerification var = (IdentityVerification)var;
         var = var;
         if(!l.a(this.rejectionReasonCode, var.rejectionReasonCode)) {
            return var;
         }

         var = var;
         if(!l.a(this.rejectionNote, var.rejectionNote)) {
            return var;
         }

         var = var;
         if(!l.a(this.status, var.status)) {
            return var;
         }

         boolean var;
         if(this.identityDocUploaded == var.identityDocUploaded) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.selfieUploaded == var.selfieUploaded) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.allowPicker == var.allowPicker) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.allowSystemCamera == var.allowSystemCamera) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "IdentityVerification(rejectionReasonCode=" + this.rejectionReasonCode + ", rejectionNote=" + this.rejectionNote + ", status=" + this.status + ", identityDocUploaded=" + this.identityDocUploaded + ", selfieUploaded=" + this.selfieUploaded + ", allowPicker=" + this.allowPicker + ", allowSystemCamera=" + this.allowSystemCamera + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;", "", "(Ljava/lang/String;I)V", "PENDING_SUBMISSION", "NOT_STARTED", "PENDING_REVIEW", "APPROVED", "BLOCKED", "NOT_REQUIRED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Status {
      @c(
         a = "approved"
      )
      APPROVED,
      @c(
         a = "blocked"
      )
      BLOCKED,
      @c(
         a = "not-required"
      )
      NOT_REQUIRED,
      @c(
         a = "not-started"
      )
      NOT_STARTED,
      @c(
         a = "pending-review"
      )
      PENDING_REVIEW,
      @c(
         a = "pending-submission"
      )
      PENDING_SUBMISSION;

      static {
         IdentityVerification.Status var = new IdentityVerification.Status("PENDING_SUBMISSION", 0);
         PENDING_SUBMISSION = var;
         IdentityVerification.Status var = new IdentityVerification.Status("NOT_STARTED", 1);
         NOT_STARTED = var;
         IdentityVerification.Status var = new IdentityVerification.Status("PENDING_REVIEW", 2);
         PENDING_REVIEW = var;
         IdentityVerification.Status var = new IdentityVerification.Status("APPROVED", 3);
         APPROVED = var;
         IdentityVerification.Status var = new IdentityVerification.Status("BLOCKED", 4);
         BLOCKED = var;
         IdentityVerification.Status var = new IdentityVerification.Status("NOT_REQUIRED", 5);
         NOT_REQUIRED = var;
      }
   }
}
