package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/ContentType;", "", "mimeType", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getMimeType", "()Ljava/lang/String;", "toString", "IMAGE_PNG", "IMAGE_JPEG", "VIDEO_MP4", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum ContentType {
   public static final ContentType.Companion Companion;
   IMAGE_JPEG,
   IMAGE_PNG,
   VIDEO_MP4;

   private final String mimeType;

   static {
      ContentType var = new ContentType("IMAGE_PNG", 0, "image/png");
      IMAGE_PNG = var;
      ContentType var = new ContentType("IMAGE_JPEG", 1, "image/jpeg");
      IMAGE_JPEG = var;
      ContentType var = new ContentType("VIDEO_MP4", 2, "video/mp4");
      VIDEO_MP4 = var;
      Companion = new ContentType.Companion((i)null);
   }

   protected ContentType(String var) {
      l.b(var, "mimeType");
      super(var, var);
      this.mimeType = var;
   }

   public final String a() {
      return this.mimeType;
   }

   public String toString() {
      return this.mimeType;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/api/model/identity_verification/ContentType$Companion;", "", "()V", "fromFileExtension", "Lco/uk/getmondo/api/model/identity_verification/ContentType;", "extension", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }

      public final ContentType a(String var) {
         l.b(var, "extension");
         ContentType var;
         switch(var.hashCode()) {
         case 105441:
            if(!var.equals("jpg")) {
               throw (Throwable)(new IllegalArgumentException("Extension " + var + " cannot be matched to a content type"));
            }
            break;
         case 108273:
            if(!var.equals("mp4")) {
               throw (Throwable)(new IllegalArgumentException("Extension " + var + " cannot be matched to a content type"));
            }

            var = ContentType.VIDEO_MP4;
            return var;
         case 111145:
            if(!var.equals("png")) {
               throw (Throwable)(new IllegalArgumentException("Extension " + var + " cannot be matched to a content type"));
            }

            var = ContentType.IMAGE_PNG;
            return var;
         case 3268712:
            if(!var.equals("jpeg")) {
               throw (Throwable)(new IllegalArgumentException("Extension " + var + " cannot be matched to a content type"));
            }
            break;
         default:
            throw (Throwable)(new IllegalArgumentException("Extension " + var + " cannot be matched to a content type"));
         }

         var = ContentType.IMAGE_JPEG;
         return var;
      }
   }
}
