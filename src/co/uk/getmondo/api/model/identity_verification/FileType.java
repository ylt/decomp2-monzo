package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\t¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/FileType;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "toString", "IDENTITY_DOCUMENT", "SELFIE_VIDEO", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum FileType {
   IDENTITY_DOCUMENT,
   SELFIE_VIDEO;

   private final String apiValue;

   static {
      FileType var = new FileType("IDENTITY_DOCUMENT", 0, "identity_document");
      IDENTITY_DOCUMENT = var;
      FileType var = new FileType("SELFIE_VIDEO", 1, "selfie_video");
      SELFIE_VIDEO = var;
   }

   protected FileType(String var) {
      l.b(var, "apiValue");
      super(var, var);
      this.apiValue = var;
   }

   public String toString() {
      return this.apiValue;
   }
}
