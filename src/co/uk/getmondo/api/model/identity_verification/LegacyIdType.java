package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;", "", "type", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getType", "()Ljava/lang/String;", "PASSPORT", "DRIVING_LICENSE", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum LegacyIdType {
   public static final LegacyIdType.Companion Companion;
   DRIVING_LICENSE,
   PASSPORT;

   private final String type;

   static {
      LegacyIdType var = new LegacyIdType("PASSPORT", 0, "id_passport_image");
      PASSPORT = var;
      LegacyIdType var = new LegacyIdType("DRIVING_LICENSE", 1, "id_driver_licence_image");
      DRIVING_LICENSE = var;
      Companion = new LegacyIdType.Companion((i)null);
   }

   protected LegacyIdType(String var) {
      l.b(var, "type");
      super(var, var);
      this.type = var;
   }

   public final String a() {
      return this.type;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/api/model/identity_verification/LegacyIdType$Companion;", "", "()V", "from", "Lco/uk/getmondo/api/model/identity_verification/LegacyIdType;", "identityDocumentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "get", "ordinal", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }

      public final LegacyIdType a(IdentityDocumentType var) {
         l.b(var, "identityDocumentType");
         LegacyIdType var;
         switch(LegacyIdType$Companion$WhenMappings.$EnumSwitchMapping$0[var.ordinal()]) {
         case 1:
            var = LegacyIdType.PASSPORT;
            break;
         case 2:
            var = LegacyIdType.DRIVING_LICENSE;
            break;
         case 3:
            throw (Throwable)(new IllegalArgumentException("Cannot support national ID in legacy"));
         default:
            throw new NoWhenBranchMatchedException();
         }

         return var;
      }
   }
}
