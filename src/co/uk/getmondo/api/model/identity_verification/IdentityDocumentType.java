package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0001\u0018\u0000 \u00122\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B!\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u000e\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\rj\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "", "apiValue", "", "isBackRequired", "", "displayNameRes", "", "(Ljava/lang/String;ILjava/lang/String;ZI)V", "getApiValue", "()Ljava/lang/String;", "getDisplayNameRes", "()I", "()Z", "toString", "PASSPORT", "DRIVING_LICENSE", "NATIONAL_ID", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum IdentityDocumentType {
   public static final IdentityDocumentType.Companion Companion;
   DRIVING_LICENSE,
   NATIONAL_ID,
   PASSPORT;

   private final String apiValue;
   private final int displayNameRes;
   private final boolean isBackRequired;

   static {
      IdentityDocumentType var = new IdentityDocumentType("PASSPORT", 0, "passport", false, 2131362260);
      PASSPORT = var;
      IdentityDocumentType var = new IdentityDocumentType("DRIVING_LICENSE", 1, "driving_license", true, 2131362258);
      DRIVING_LICENSE = var;
      IdentityDocumentType var = new IdentityDocumentType("NATIONAL_ID", 2, "national_identity_card", true, 2131362259);
      NATIONAL_ID = var;
      Companion = new IdentityDocumentType.Companion((i)null);
   }

   protected IdentityDocumentType(String var, boolean var, int var) {
      l.b(var, "apiValue");
      super(var, var);
      this.apiValue = var;
      this.isBackRequired = var;
      this.displayNameRes = var;
   }

   public final String a() {
      return this.apiValue;
   }

   public final boolean b() {
      return this.isBackRequired;
   }

   public final int c() {
      return this.displayNameRes;
   }

   public String toString() {
      return this.apiValue;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType$Companion;", "", "()V", "from", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }

      public final IdentityDocumentType a(String var) {
         Object[] var = (Object[])IdentityDocumentType.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(l.a(((IdentityDocumentType)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         return (IdentityDocumentType)var;
      }
   }
}
