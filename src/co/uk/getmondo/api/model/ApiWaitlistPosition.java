package co.uk.getmondo.api.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b$\b\u0086\b\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\u0010J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0005HÆ\u0003J\t\u0010\"\u001a\u00020\u0005HÆ\u0003J\t\u0010#\u001a\u00020\bHÆ\u0003J\u000f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0005HÆ\u0003J\t\u0010'\u001a\u00020\bHÆ\u0003J\u0010\u0010(\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u0010\u0014Jp\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0002\u0010*J\u0013\u0010+\u001a\u00020\b2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020\u0005HÖ\u0001J\t\u0010.\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0015\u0010\u000f\u001a\u0004\u0018\u00010\b¢\u0006\n\n\u0002\u0010\u0015\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u000e\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0012R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001e¨\u0006/"},
   d2 = {"Lco/uk/getmondo/api/model/ApiWaitlistPosition;", "", "userId", "", "position", "", "after", "finished", "", "events", "", "Lco/uk/getmondo/api/model/ApiEvent;", "referralLink", "referralBump", "ineligible", "currentAccount", "(Ljava/lang/String;IIZLjava/util/List;Ljava/lang/String;IZLjava/lang/Boolean;)V", "getAfter", "()I", "getCurrentAccount", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getEvents", "()Ljava/util/List;", "getFinished", "()Z", "getIneligible", "getPosition", "getReferralBump", "getReferralLink", "()Ljava/lang/String;", "getUserId", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;IIZLjava/util/List;Ljava/lang/String;IZLjava/lang/Boolean;)Lco/uk/getmondo/api/model/ApiWaitlistPosition;", "equals", "other", "hashCode", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiWaitlistPosition {
   private final int after;
   private final Boolean currentAccount;
   private final List events;
   private final boolean finished;
   private final boolean ineligible;
   private final int position;
   private final int referralBump;
   private final String referralLink;
   private final String userId;

   public ApiWaitlistPosition(String var, int var, int var, boolean var, List var, String var, int var, boolean var, Boolean var) {
      l.b(var, "userId");
      l.b(var, "events");
      l.b(var, "referralLink");
      super();
      this.userId = var;
      this.position = var;
      this.after = var;
      this.finished = var;
      this.events = var;
      this.referralLink = var;
      this.referralBump = var;
      this.ineligible = var;
      this.currentAccount = var;
   }

   // $FF: synthetic method
   public ApiWaitlistPosition(String var, int var, int var, boolean var, List var, String var, int var, boolean var, Boolean var, int var, i var) {
      if((var & 16) != 0) {
         var = m.a();
      }

      this(var, var, var, var, var, var, var, var, var);
   }

   public final String a() {
      return this.userId;
   }

   public final int b() {
      return this.position;
   }

   public final int c() {
      return this.after;
   }

   public final boolean d() {
      return this.finished;
   }

   public final List e() {
      return this.events;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiWaitlistPosition)) {
            return var;
         }

         ApiWaitlistPosition var = (ApiWaitlistPosition)var;
         var = var;
         if(!l.a(this.userId, var.userId)) {
            return var;
         }

         boolean var;
         if(this.position == var.position) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.after == var.after) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.finished == var.finished) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.events, var.events)) {
            return var;
         }

         var = var;
         if(!l.a(this.referralLink, var.referralLink)) {
            return var;
         }

         if(this.referralBump == var.referralBump) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.ineligible == var.ineligible) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.currentAccount, var.currentAccount)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.referralLink;
   }

   public final int g() {
      return this.referralBump;
   }

   public final boolean h() {
      return this.ineligible;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final Boolean i() {
      return this.currentAccount;
   }

   public String toString() {
      return "ApiWaitlistPosition(userId=" + this.userId + ", position=" + this.position + ", after=" + this.after + ", finished=" + this.finished + ", events=" + this.events + ", referralLink=" + this.referralLink + ", referralBump=" + this.referralBump + ", ineligible=" + this.ineligible + ", currentAccount=" + this.currentAccount + ")";
   }
}
