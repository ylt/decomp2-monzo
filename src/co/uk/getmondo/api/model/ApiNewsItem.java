package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0007HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/ApiNewsItem;", "", "id", "", "uri", "title", "expires", "Lorg/threeten/bp/LocalDateTime;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;)V", "getExpires", "()Lorg/threeten/bp/LocalDateTime;", "getId", "()Ljava/lang/String;", "getTitle", "getUri", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiNewsItem {
   private final LocalDateTime expires;
   private final String id;
   private final String title;
   private final String uri;

   public ApiNewsItem(String var, String var, String var, LocalDateTime var) {
      l.b(var, "id");
      l.b(var, "uri");
      l.b(var, "title");
      l.b(var, "expires");
      super();
      this.id = var;
      this.uri = var;
      this.title = var;
      this.expires = var;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.uri;
   }

   public final String c() {
      return this.title;
   }

   public final LocalDateTime d() {
      return this.expires;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof ApiNewsItem) {
               ApiNewsItem var = (ApiNewsItem)var;
               if(l.a(this.id, var.id) && l.a(this.uri, var.uri) && l.a(this.title, var.title) && l.a(this.expires, var.expires)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.uri;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.title;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      LocalDateTime var = this.expires;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiNewsItem(id=" + this.id + ", uri=" + this.uri + ", title=" + this.title + ", expires=" + this.expires + ")";
   }
}
