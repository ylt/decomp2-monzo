package co.uk.getmondo.api.model.topup;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/api/model/topup/ApiStripeCard;", "", "stripeCardId", "", "lastFour", "endDate", "brand", "funding", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBrand", "()Ljava/lang/String;", "getEndDate", "getFunding", "getLastFour", "getStripeCardId", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiStripeCard {
   private final String brand;
   private final String endDate;
   private final String funding;
   private final String lastFour;
   private final String stripeCardId;

   public ApiStripeCard(String var, String var, String var, String var, String var) {
      l.b(var, "stripeCardId");
      l.b(var, "lastFour");
      l.b(var, "endDate");
      l.b(var, "brand");
      l.b(var, "funding");
      super();
      this.stripeCardId = var;
      this.lastFour = var;
      this.endDate = var;
      this.brand = var;
      this.funding = var;
   }

   public final String a() {
      return this.stripeCardId;
   }

   public final String b() {
      return this.lastFour;
   }

   public final String c() {
      return this.endDate;
   }

   public final String d() {
      return this.brand;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof ApiStripeCard) {
               ApiStripeCard var = (ApiStripeCard)var;
               if(l.a(this.stripeCardId, var.stripeCardId) && l.a(this.lastFour, var.lastFour) && l.a(this.endDate, var.endDate) && l.a(this.brand, var.brand) && l.a(this.funding, var.funding)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.stripeCardId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.lastFour;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.endDate;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.brand;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.funding;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiStripeCard(stripeCardId=" + this.stripeCardId + ", lastFour=" + this.lastFour + ", endDate=" + this.endDate + ", brand=" + this.brand + ", funding=" + this.funding + ")";
   }
}
