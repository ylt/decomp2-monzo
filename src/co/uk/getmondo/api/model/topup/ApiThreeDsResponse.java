package co.uk.getmondo.api.model.topup;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÆ\u0003J=\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\u0006\u0010\u001b\u001a\u00020\u0003J\t\u0010\u001c\u001a\u00020\u0005HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse;", "", "required", "", "decision", "", "stripeSource", "status", "redirectUrl", "(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getDecision", "()Ljava/lang/String;", "getRedirectUrl", "getRequired", "()Z", "getStatus", "getStripeSource", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "requiresWebResolution", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiThreeDsResponse {
   public static final ApiThreeDsResponse.Companion Companion = new ApiThreeDsResponse.Companion((i)null);
   private static final String STATUS_REDIRECT_PENDING = "redirect_pending";
   private static final String STATUS_SUCCEEDED = "succeeded";
   private final String decision;
   private final String redirectUrl;
   private final boolean required;
   private final String status;
   private final String stripeSource;

   public ApiThreeDsResponse(boolean var, String var, String var, String var, String var) {
      l.b(var, "stripeSource");
      l.b(var, "status");
      l.b(var, "redirectUrl");
      super();
      this.required = var;
      this.decision = var;
      this.stripeSource = var;
      this.status = var;
      this.redirectUrl = var;
   }

   public final boolean a() {
      boolean var = false;
      if(this.required && !j.a(Companion.b(), this.status, true)) {
         if(!j.a(Companion.a(), this.status, true)) {
            throw (Throwable)(new RuntimeException("Unsupported status found: status=" + this.status));
         }

         var = true;
      }

      return var;
   }

   public final boolean b() {
      return this.required;
   }

   public final String c() {
      return this.decision;
   }

   public final String d() {
      return this.stripeSource;
   }

   public final String e() {
      return this.redirectUrl;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiThreeDsResponse)) {
            return var;
         }

         ApiThreeDsResponse var = (ApiThreeDsResponse)var;
         boolean var;
         if(this.required == var.required) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.decision, var.decision)) {
            return var;
         }

         var = var;
         if(!l.a(this.stripeSource, var.stripeSource)) {
            return var;
         }

         var = var;
         if(!l.a(this.status, var.status)) {
            return var;
         }

         var = var;
         if(!l.a(this.redirectUrl, var.redirectUrl)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "ApiThreeDsResponse(required=" + this.required + ", decision=" + this.decision + ", stripeSource=" + this.stripeSource + ", status=" + this.status + ", redirectUrl=" + this.redirectUrl + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/api/model/topup/ApiThreeDsResponse$Companion;", "", "()V", "STATUS_REDIRECT_PENDING", "", "getSTATUS_REDIRECT_PENDING", "()Ljava/lang/String;", "STATUS_SUCCEEDED", "getSTATUS_SUCCEEDED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }

      private final String b() {
         return ApiThreeDsResponse.STATUS_SUCCEEDED;
      }

      public final String a() {
         return ApiThreeDsResponse.STATUS_REDIRECT_PENDING;
      }
   }
}
