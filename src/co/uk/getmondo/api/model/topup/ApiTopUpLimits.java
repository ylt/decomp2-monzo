package co.uk.getmondo.api.model.topup;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/api/model/topup/ApiTopUpLimits;", "", "minLoad", "", "maxLoad", "(FF)V", "getMaxLoad", "()F", "getMinLoad", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiTopUpLimits {
   private final float maxLoad;
   private final float minLoad;

   public ApiTopUpLimits(float var, float var) {
      this.minLoad = var;
      this.maxLoad = var;
   }

   public final float a() {
      return this.minLoad;
   }

   public final float b() {
      return this.maxLoad;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof ApiTopUpLimits) {
               ApiTopUpLimits var = (ApiTopUpLimits)var;
               if(Float.compare(this.minLoad, var.minLoad) == 0 && Float.compare(this.maxLoad, var.maxLoad) == 0) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      return Float.floatToIntBits(this.minLoad) * 31 + Float.floatToIntBits(this.maxLoad);
   }

   public String toString() {
      return "ApiTopUpLimits(minLoad=" + this.minLoad + ", maxLoad=" + this.maxLoad + ")";
   }
}
