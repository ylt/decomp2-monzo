package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001aB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/api/model/ApiGoldenTicket;", "", "ticketId", "", "status", "Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "uri", "shortUri", "(Ljava/lang/String;Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;Ljava/lang/String;Ljava/lang/String;)V", "getShortUri", "()Ljava/lang/String;", "getStatus", "()Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "getTicketId", "getUri", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "GoldenTicketStatus", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiGoldenTicket {
   private final String shortUri;
   private final ApiGoldenTicket.GoldenTicketStatus status;
   private final String ticketId;
   private final String uri;

   public ApiGoldenTicket(String var, ApiGoldenTicket.GoldenTicketStatus var, String var, String var) {
      l.b(var, "ticketId");
      l.b(var, "status");
      l.b(var, "uri");
      l.b(var, "shortUri");
      super();
      this.ticketId = var;
      this.status = var;
      this.uri = var;
      this.shortUri = var;
   }

   public final ApiGoldenTicket.GoldenTicketStatus a() {
      return this.status;
   }

   public final String b() {
      return this.shortUri;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof ApiGoldenTicket) {
               ApiGoldenTicket var = (ApiGoldenTicket)var;
               if(l.a(this.ticketId, var.ticketId) && l.a(this.status, var.status) && l.a(this.uri, var.uri) && l.a(this.shortUri, var.shortUri)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.ticketId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ApiGoldenTicket.GoldenTicketStatus var = this.status;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.uri;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.shortUri;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiGoldenTicket(ticketId=" + this.ticketId + ", status=" + this.status + ", uri=" + this.uri + ", shortUri=" + this.shortUri + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "ACTIVE", "INACTIVE", "CLAIMED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum GoldenTicketStatus {
      ACTIVE,
      CLAIMED,
      INACTIVE;

      private final String apiValue;

      static {
         ApiGoldenTicket.GoldenTicketStatus var = new ApiGoldenTicket.GoldenTicketStatus("ACTIVE", 0, "ACTIVE");
         ACTIVE = var;
         ApiGoldenTicket.GoldenTicketStatus var = new ApiGoldenTicket.GoldenTicketStatus("INACTIVE", 1, "INACTIVE");
         INACTIVE = var;
         ApiGoldenTicket.GoldenTicketStatus var = new ApiGoldenTicket.GoldenTicketStatus("CLAIMED", 2, "CLAIMED");
         CLAIMED = var;
      }

      protected GoldenTicketStatus(String var) {
         l.b(var, "apiValue");
         super(var, var);
         this.apiValue = var;
      }
   }
}
