package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003JK\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\t\u0010\u001f\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f¨\u0006 "},
   d2 = {"Lco/uk/getmondo/api/model/ApiAccount;", "", "id", "", "created", "Lorg/threeten/bp/LocalDateTime;", "description", "sortCode", "accountNumber", "type", "(Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getDescription", "getId", "getSortCode", "getType", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAccount {
   private final String accountNumber;
   private final LocalDateTime created;
   private final String description;
   private final String id;
   private final String sortCode;
   private final String type;

   public ApiAccount(String var, LocalDateTime var, String var, String var, String var, String var) {
      l.b(var, "id");
      l.b(var, "created");
      l.b(var, "description");
      super();
      this.id = var;
      this.created = var;
      this.description = var;
      this.sortCode = var;
      this.accountNumber = var;
      this.type = var;
   }

   // $FF: synthetic method
   public ApiAccount(String var, LocalDateTime var, String var, String var, String var, String var, int var, i var) {
      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      if((var & 32) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var, var);
   }

   public final String a() {
      return this.id;
   }

   public final LocalDateTime b() {
      return this.created;
   }

   public final String c() {
      return this.description;
   }

   public final String d() {
      return this.sortCode;
   }

   public final String e() {
      return this.accountNumber;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label36: {
            if(var instanceof ApiAccount) {
               ApiAccount var = (ApiAccount)var;
               if(l.a(this.id, var.id) && l.a(this.created, var.created) && l.a(this.description, var.description) && l.a(this.sortCode, var.sortCode) && l.a(this.accountNumber, var.accountNumber) && l.a(this.type, var.type)) {
                  break label36;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.type;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      LocalDateTime var = this.created;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.description;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.sortCode;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.accountNumber;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.type;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiAccount(id=" + this.id + ", created=" + this.created + ", description=" + this.description + ", sortCode=" + this.sortCode + ", accountNumber=" + this.accountNumber + ", type=" + this.type + ")";
   }
}
