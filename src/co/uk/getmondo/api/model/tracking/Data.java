package co.uk.getmondo.api.model.tracking;

import co.uk.getmondo.api.model.b;
import co.uk.getmondo.d.h;
import com.google.gson.a.c;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalUnit;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u001f\n\u0002\u0010\t\n\u0002\bU\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u0088\u00012\u00020\u0001:\u0002\u0088\u0001B½\u0005\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010.\u001a\u0004\u0018\u00010/\u0012\n\b\u0002\u00100\u001a\u0004\u0018\u00010/\u0012\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00103\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00105\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00106\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010;\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010=\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010@J\u0010\u0010D\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010F\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010G\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010I\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010J\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010K\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010L\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010O\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010P\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010Q\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010R\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010S\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010T\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010V\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010W\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010Y\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010Z\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010[\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010]\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010^\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010b\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010c\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010e\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010h\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010i\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010j\u001a\u0004\u0018\u00010/HÂ\u0003¢\u0006\u0002\u0010kJ\u0010\u0010l\u001a\u0004\u0018\u00010/HÂ\u0003¢\u0006\u0002\u0010kJ\u0010\u0010m\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010n\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010o\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010p\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010q\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010r\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010s\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010t\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010u\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010v\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010w\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010x\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010y\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010z\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010{\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010|\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010}\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010~\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010\u007f\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\f\u0010\u0080\u0001\u001a\u0004\u0018\u00010\u0005HÂ\u0003JÈ\u0005\u0010\u0081\u0001\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010/2\n\b\u0002\u00100\u001a\u0004\u0018\u00010/2\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00103\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00105\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00106\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010;\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010=\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0003\u0010\u0082\u0001J\u0016\u0010\u0083\u0001\u001a\u00020\u000f2\n\u0010\u0084\u0001\u001a\u0005\u0018\u00010\u0085\u0001HÖ\u0003J\n\u0010\u0086\u0001\u001a\u00020\u0003HÖ\u0001J\n\u0010\u0087\u0001\u001a\u00020\u0005HÖ\u0001R\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0014\u00107\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010(\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010)\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0014\u0010'\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010AR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010\f\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\"\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010,\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u00108\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u00105\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010?\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010.\u001a\u0004\u0018\u00010/X\u0082\u0004¢\u0006\u0004\n\u0002\u0010CR\u0012\u00104\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010#\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010*\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u00101\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010>\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010 \u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010:\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u00109\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010;\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010=\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010-\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u00100\u001a\u0004\u0018\u00010/X\u0082\u0004¢\u0006\u0004\n\u0002\u0010CR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u00103\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010<\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010$\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010+\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0089\u0001"},
   d2 = {"Lco/uk/getmondo/api/model/tracking/Data;", "Ljava/io/Serializable;", "tourPage", "", "openedFrom", "", "transactionId", "referrer", "redirectId", "url", "eligible", "amount", "cameFrom", "description", "docDone", "", "selfieDone", "docType", "kycRejected", "oldCategory", "newCategory", "category", "month", "granted", "contacts", "contactsOnMonzo", "from", "method", "authenticate", "appShortcutType", "fingerprintAuth", "activeNotifications", "nfcEnabled", "diligence", "enabled", "gpsAdid", "traceId", "type", "path", "apiStatusCode", "apiErrorCode", "apiErrorMessage", "logoutReason", "userId", "errorType", "sampleError", "firstSampleDelta", "", "secondSampleDelta", "memoryError", "videoErrorReason", "splitWith", "firstTime", "feedback", "reportMonth", "androidPayEnabled", "fallback", "notificationType", "notificationSubType", "optedIn", "topicId", "outcome", "nameOnCard", "fileType", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "Ljava/lang/Integer;", "Ljava/lang/Boolean;", "Ljava/lang/Long;", "component1", "()Ljava/lang/Integer;", "component10", "component11", "()Ljava/lang/Boolean;", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component35", "component36", "component37", "component38", "component39", "component4", "component40", "component41", "component42", "()Ljava/lang/Long;", "component43", "component44", "component45", "component46", "component47", "component48", "component49", "component5", "component50", "component51", "component52", "component53", "component54", "component55", "component56", "component57", "component58", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;", "equals", "other", "", "hashCode", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Data implements Serializable {
   public static final Data.Companion Companion = new Data.Companion((i)null);
   private final Integer activeNotifications;
   private final Integer amount;
   @c(
      a = "androidpay_enabled"
   )
   private final Boolean androidPayEnabled;
   @c(
      a = "api_error_code"
   )
   private final String apiErrorCode;
   @c(
      a = "api_error_message"
   )
   private final String apiErrorMessage;
   @c(
      a = "api_status_code"
   )
   private final Integer apiStatusCode;
   private final String appShortcutType;
   private final Boolean authenticate;
   @c(
      a = "came_from"
   )
   private final String cameFrom;
   private final String category;
   private final Integer contacts;
   private final Integer contactsOnMonzo;
   private final String description;
   private final String diligence;
   @c(
      a = "doc_done"
   )
   private final Boolean docDone;
   @c(
      a = "doc_type"
   )
   private final String docType;
   private final Integer eligible;
   private final Boolean enabled;
   private final String errorType;
   private final Boolean fallback;
   private final String feedback;
   @c(
      a = "file_type"
   )
   private final String fileType;
   private final String fingerprintAuth;
   private final Long firstSampleDelta;
   private final Boolean firstTime;
   private final String from;
   @c(
      a = "gps_adid"
   )
   private final String gpsAdid;
   private final Boolean granted;
   @c(
      a = "kyc_rejected"
   )
   private final Boolean kycRejected;
   @c(
      a = "logout_reason"
   )
   private final String logoutReason;
   private final Boolean memoryError;
   private final String method;
   private final Integer month;
   @c(
      a = "name_on_card"
   )
   private final String nameOnCard;
   @c(
      a = "new_category"
   )
   private final String newCategory;
   private final Boolean nfcEnabled;
   @c(
      a = "notification_sub_type"
   )
   private final String notificationSubType;
   @c(
      a = "notification_type"
   )
   private final String notificationType;
   @c(
      a = "old_category"
   )
   private final String oldCategory;
   @c(
      a = "opened_from"
   )
   private final String openedFrom;
   private final Boolean optedIn;
   private final String outcome;
   private final String path;
   @c(
      a = "play_store_redirect_event_id"
   )
   private final String redirectId;
   @c(
      a = "referrer"
   )
   private final String referrer;
   private final String reportMonth;
   private final Boolean sampleError;
   private final Long secondSampleDelta;
   @c(
      a = "selfie_done"
   )
   private final Boolean selfieDone;
   @c(
      a = "split_with"
   )
   private final String splitWith;
   @c(
      a = "topic_id"
   )
   private final String topicId;
   @c(
      a = "tour_page"
   )
   private final Integer tourPage;
   @c(
      a = "trace_id"
   )
   private final String traceId;
   @c(
      a = "transaction_id"
   )
   private final String transactionId;
   private final String type;
   private final String url;
   @c(
      a = "user_id"
   )
   private final String userId;
   private final String videoErrorReason;

   public Data() {
      this((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108863, (i)null);
   }

   public Data(Integer var, String var, String var, String var, String var, String var, Integer var, Integer var, String var, String var, Boolean var, Boolean var, String var, Boolean var, String var, String var, String var, Integer var, Boolean var, Integer var, Integer var, String var, String var, Boolean var, String var, String var, Integer var, Boolean var, String var, Boolean var, String var, String var, String var, String var, Integer var, String var, String var, String var, String var, String var, Boolean var, Long var, Long var, Boolean var, String var, String var, Boolean var, String var, String var, Boolean var, Boolean var, String var, String var, Boolean var, String var, String var, String var, String var) {
      this.tourPage = var;
      this.openedFrom = var;
      this.transactionId = var;
      this.referrer = var;
      this.redirectId = var;
      this.url = var;
      this.eligible = var;
      this.amount = var;
      this.cameFrom = var;
      this.description = var;
      this.docDone = var;
      this.selfieDone = var;
      this.docType = var;
      this.kycRejected = var;
      this.oldCategory = var;
      this.newCategory = var;
      this.category = var;
      this.month = var;
      this.granted = var;
      this.contacts = var;
      this.contactsOnMonzo = var;
      this.from = var;
      this.method = var;
      this.authenticate = var;
      this.appShortcutType = var;
      this.fingerprintAuth = var;
      this.activeNotifications = var;
      this.nfcEnabled = var;
      this.diligence = var;
      this.enabled = var;
      this.gpsAdid = var;
      this.traceId = var;
      this.type = var;
      this.path = var;
      this.apiStatusCode = var;
      this.apiErrorCode = var;
      this.apiErrorMessage = var;
      this.logoutReason = var;
      this.userId = var;
      this.errorType = var;
      this.sampleError = var;
      this.firstSampleDelta = var;
      this.secondSampleDelta = var;
      this.memoryError = var;
      this.videoErrorReason = var;
      this.splitWith = var;
      this.firstTime = var;
      this.feedback = var;
      this.reportMonth = var;
      this.androidPayEnabled = var;
      this.fallback = var;
      this.notificationType = var;
      this.notificationSubType = var;
      this.optedIn = var;
      this.topicId = var;
      this.outcome = var;
      this.nameOnCard = var;
      this.fileType = var;
   }

   // $FF: synthetic method
   public Data(Integer var, String var, String var, String var, String var, String var, Integer var, Integer var, String var, String var, Boolean var, Boolean var, String var, Boolean var, String var, String var, String var, Integer var, Boolean var, Integer var, Integer var, String var, String var, Boolean var, String var, String var, Integer var, Boolean var, String var, Boolean var, String var, String var, String var, String var, Integer var, String var, String var, String var, String var, String var, Boolean var, Long var, Long var, Boolean var, String var, String var, Boolean var, String var, String var, Boolean var, Boolean var, String var, String var, Boolean var, String var, String var, String var, String var, int var, int var, i var) {
      if((var & 1) != 0) {
         var = (Integer)null;
      }

      if((var & 2) != 0) {
         var = (String)null;
      }

      if((var & 4) != 0) {
         var = (String)null;
      }

      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      if((var & 32) != 0) {
         var = (String)null;
      }

      if((var & 64) != 0) {
         var = (Integer)null;
      }

      if((var & 128) != 0) {
         var = (Integer)null;
      }

      if((var & 256) != 0) {
         var = (String)null;
      }

      if((var & 512) != 0) {
         var = (String)null;
      }

      if((var & 1024) != 0) {
         var = (Boolean)null;
      }

      if((var & 2048) != 0) {
         var = (Boolean)null;
      }

      if((var & 4096) != 0) {
         var = (String)null;
      }

      if((var & 8192) != 0) {
         var = (Boolean)null;
      }

      if((var & 16384) != 0) {
         var = (String)null;
      }

      if(('耀' & var) != 0) {
         var = (String)null;
      }

      if((65536 & var) != 0) {
         var = (String)null;
      }

      if((131072 & var) != 0) {
         var = (Integer)null;
      }

      if((262144 & var) != 0) {
         var = (Boolean)null;
      }

      if((524288 & var) != 0) {
         var = (Integer)null;
      }

      if((1048576 & var) != 0) {
         var = (Integer)null;
      }

      if((2097152 & var) != 0) {
         var = (String)null;
      }

      if((4194304 & var) != 0) {
         var = (String)null;
      }

      if((8388608 & var) != 0) {
         var = (Boolean)null;
      }

      if((16777216 & var) != 0) {
         var = (String)null;
      }

      if((33554432 & var) != 0) {
         var = (String)null;
      }

      if((67108864 & var) != 0) {
         var = (Integer)null;
      }

      if((134217728 & var) != 0) {
         var = (Boolean)null;
      }

      if((268435456 & var) != 0) {
         var = (String)null;
      }

      if((536870912 & var) != 0) {
         var = (Boolean)null;
      }

      if((1073741824 & var) != 0) {
         var = (String)null;
      }

      if((Integer.MIN_VALUE & var) != 0) {
         var = (String)null;
      }

      if((var & 1) != 0) {
         var = (String)null;
      }

      if((var & 2) != 0) {
         var = (String)null;
      }

      if((var & 4) != 0) {
         var = (Integer)null;
      }

      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      if((var & 32) != 0) {
         var = (String)null;
      }

      if((var & 64) != 0) {
         var = (String)null;
      }

      if((var & 128) != 0) {
         var = (String)null;
      }

      if((var & 256) != 0) {
         var = (Boolean)null;
      }

      if((var & 512) != 0) {
         var = (Long)null;
      }

      if((var & 1024) != 0) {
         var = (Long)null;
      }

      if((var & 2048) != 0) {
         var = (Boolean)null;
      }

      if((var & 4096) != 0) {
         var = (String)null;
      }

      if((var & 8192) != 0) {
         var = (String)null;
      }

      if((var & 16384) != 0) {
         var = (Boolean)null;
      }

      if(('耀' & var) != 0) {
         var = (String)null;
      }

      if((65536 & var) != 0) {
         var = (String)null;
      }

      if((131072 & var) != 0) {
         var = (Boolean)null;
      }

      if((262144 & var) != 0) {
         var = (Boolean)null;
      }

      if((524288 & var) != 0) {
         var = (String)null;
      }

      if((1048576 & var) != 0) {
         var = (String)null;
      }

      if((2097152 & var) != 0) {
         var = (Boolean)null;
      }

      if((4194304 & var) != 0) {
         var = (String)null;
      }

      if((8388608 & var) != 0) {
         var = (String)null;
      }

      if((16777216 & var) != 0) {
         var = (String)null;
      }

      if((33554432 & var) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label140: {
            if(var instanceof Data) {
               Data var = (Data)var;
               if(l.a(this.tourPage, var.tourPage) && l.a(this.openedFrom, var.openedFrom) && l.a(this.transactionId, var.transactionId) && l.a(this.referrer, var.referrer) && l.a(this.redirectId, var.redirectId) && l.a(this.url, var.url) && l.a(this.eligible, var.eligible) && l.a(this.amount, var.amount) && l.a(this.cameFrom, var.cameFrom) && l.a(this.description, var.description) && l.a(this.docDone, var.docDone) && l.a(this.selfieDone, var.selfieDone) && l.a(this.docType, var.docType) && l.a(this.kycRejected, var.kycRejected) && l.a(this.oldCategory, var.oldCategory) && l.a(this.newCategory, var.newCategory) && l.a(this.category, var.category) && l.a(this.month, var.month) && l.a(this.granted, var.granted) && l.a(this.contacts, var.contacts) && l.a(this.contactsOnMonzo, var.contactsOnMonzo) && l.a(this.from, var.from) && l.a(this.method, var.method) && l.a(this.authenticate, var.authenticate) && l.a(this.appShortcutType, var.appShortcutType) && l.a(this.fingerprintAuth, var.fingerprintAuth) && l.a(this.activeNotifications, var.activeNotifications) && l.a(this.nfcEnabled, var.nfcEnabled) && l.a(this.diligence, var.diligence) && l.a(this.enabled, var.enabled) && l.a(this.gpsAdid, var.gpsAdid) && l.a(this.traceId, var.traceId) && l.a(this.type, var.type) && l.a(this.path, var.path) && l.a(this.apiStatusCode, var.apiStatusCode) && l.a(this.apiErrorCode, var.apiErrorCode) && l.a(this.apiErrorMessage, var.apiErrorMessage) && l.a(this.logoutReason, var.logoutReason) && l.a(this.userId, var.userId) && l.a(this.errorType, var.errorType) && l.a(this.sampleError, var.sampleError) && l.a(this.firstSampleDelta, var.firstSampleDelta) && l.a(this.secondSampleDelta, var.secondSampleDelta) && l.a(this.memoryError, var.memoryError) && l.a(this.videoErrorReason, var.videoErrorReason) && l.a(this.splitWith, var.splitWith) && l.a(this.firstTime, var.firstTime) && l.a(this.feedback, var.feedback) && l.a(this.reportMonth, var.reportMonth) && l.a(this.androidPayEnabled, var.androidPayEnabled) && l.a(this.fallback, var.fallback) && l.a(this.notificationType, var.notificationType) && l.a(this.notificationSubType, var.notificationSubType) && l.a(this.optedIn, var.optedIn) && l.a(this.topicId, var.topicId) && l.a(this.outcome, var.outcome) && l.a(this.nameOnCard, var.nameOnCard) && l.a(this.fileType, var.fileType)) {
                  break label140;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      Integer var = this.tourPage;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.openedFrom;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.transactionId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.referrer;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.redirectId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.url;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.eligible;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.amount;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.cameFrom;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.description;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Boolean var = this.docDone;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.selfieDone;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.docType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.kycRejected;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.oldCategory;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.newCategory;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.category;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.month;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.granted;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.contacts;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.contactsOnMonzo;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.from;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.method;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.authenticate;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.appShortcutType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fingerprintAuth;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.activeNotifications;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.nfcEnabled;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.diligence;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.enabled;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.gpsAdid;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.traceId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.type;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.path;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.apiStatusCode;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.apiErrorCode;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.apiErrorMessage;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.logoutReason;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.userId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.errorType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.sampleError;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Long var = this.firstSampleDelta;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.secondSampleDelta;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.memoryError;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.videoErrorReason;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.splitWith;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.firstTime;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.feedback;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.reportMonth;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.androidPayEnabled;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fallback;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.notificationType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.notificationSubType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.optedIn;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.topicId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.outcome;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.nameOnCard;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fileType;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "Data(tourPage=" + this.tourPage + ", openedFrom=" + this.openedFrom + ", transactionId=" + this.transactionId + ", referrer=" + this.referrer + ", redirectId=" + this.redirectId + ", url=" + this.url + ", eligible=" + this.eligible + ", amount=" + this.amount + ", cameFrom=" + this.cameFrom + ", description=" + this.description + ", docDone=" + this.docDone + ", selfieDone=" + this.selfieDone + ", docType=" + this.docType + ", kycRejected=" + this.kycRejected + ", oldCategory=" + this.oldCategory + ", newCategory=" + this.newCategory + ", category=" + this.category + ", month=" + this.month + ", granted=" + this.granted + ", contacts=" + this.contacts + ", contactsOnMonzo=" + this.contactsOnMonzo + ", from=" + this.from + ", method=" + this.method + ", authenticate=" + this.authenticate + ", appShortcutType=" + this.appShortcutType + ", fingerprintAuth=" + this.fingerprintAuth + ", activeNotifications=" + this.activeNotifications + ", nfcEnabled=" + this.nfcEnabled + ", diligence=" + this.diligence + ", enabled=" + this.enabled + ", gpsAdid=" + this.gpsAdid + ", traceId=" + this.traceId + ", type=" + this.type + ", path=" + this.path + ", apiStatusCode=" + this.apiStatusCode + ", apiErrorCode=" + this.apiErrorCode + ", apiErrorMessage=" + this.apiErrorMessage + ", logoutReason=" + this.logoutReason + ", userId=" + this.userId + ", errorType=" + this.errorType + ", sampleError=" + this.sampleError + ", firstSampleDelta=" + this.firstSampleDelta + ", secondSampleDelta=" + this.secondSampleDelta + ", memoryError=" + this.memoryError + ", videoErrorReason=" + this.videoErrorReason + ", splitWith=" + this.splitWith + ", firstTime=" + this.firstTime + ", feedback=" + this.feedback + ", reportMonth=" + this.reportMonth + ", androidPayEnabled=" + this.androidPayEnabled + ", fallback=" + this.fallback + ", notificationType=" + this.notificationType + ", notificationSubType=" + this.notificationSubType + ", optedIn=" + this.optedIn + ", topicId=" + this.topicId + ", outcome=" + this.outcome + ", nameOnCard=" + this.nameOnCard + ", fileType=" + this.fileType + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0005J*\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\u0006\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\tJ\u000e\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\tJ\u000e\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\tJ\u000e\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\tJ\u0015\u0010\u0013\u001a\u00020\u00042\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00042\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\u0002\u0010\u0017J\u000e\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0016J\u000e\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\tJ\u000e\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\tJ\u001e\u0010 \u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020\u00162\u0006\u0010#\u001a\u00020\u0016J\u0016\u0010$\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\t2\u0006\u0010%\u001a\u00020\tJ\u000e\u0010&\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\tJ\u0016\u0010'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\u0005J\u000e\u0010*\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\u0016J\u000e\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0016J\u000e\u0010-\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\tJ\u000e\u0010.\u001a\u00020\u00042\u0006\u0010.\u001a\u00020\tJ\u0016\u0010/\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u00100\u001a\u000201J\u000e\u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\tJ\u0015\u00104\u001a\u00020\u00042\b\u00104\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0014J\u000e\u00105\u001a\u00020\u00042\u0006\u00105\u001a\u00020\tJ\u000e\u00106\u001a\u00020\u00042\u0006\u00106\u001a\u00020\tJ\u0016\u00107\u001a\u00020\u00042\u0006\u00108\u001a\u00020\t2\u0006\u00109\u001a\u00020\tJ\u000e\u0010:\u001a\u00020\u00042\u0006\u0010:\u001a\u00020\tJ\u0016\u0010;\u001a\u00020\u00042\u0006\u0010<\u001a\u00020\t2\u0006\u0010=\u001a\u00020\u0016J'\u0010>\u001a\u00020\u00042\u0006\u0010?\u001a\u00020\u00162\b\u0010@\u001a\u0004\u0018\u00010A2\b\u0010B\u001a\u0004\u0018\u00010A¢\u0006\u0002\u0010C¨\u0006D"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Data$Companion;", "", "()V", "amount", "Lco/uk/getmondo/api/model/tracking/Data;", "", "apiError", "apiStatusCode", "path", "", "traceId", "Lco/uk/getmondo/api/model/ApiError;", "appShortcutType", "shortcutType", "cameFrom", "category", "Lco/uk/getmondo/model/Category;", "description", "diligence", "eligible", "(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;", "enabled", "", "(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Data;", "firstTime", "isFirstGoldenTicket", "from", "entryPoint", "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;", "value", "googlePlayServicesAdvertisingId", "gpsAdid", "identityVerificationSatus", "docDone", "selfieDone", "rejected", "logoutReason", "userId", "openedFrom", "p2pEnabled", "numberOfContactsOnMonzo", "numberOfContactsNotOnMonzo", "permissionGranted", "pinAuthentication", "isSuccess", "redirectId", "referrer", "spendingData", "month", "Lorg/threeten/bp/YearMonth;", "splitWith", "number", "tourPage", "transactionId", "type", "updateTransactionCategory", "oldCategory", "newCategory", "url", "videoError", "videoErrorReason", "memoryError", "videoMp4Info", "sampleError", "firstSampleDelta", "", "secondSampleDelta", "(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Data;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }

      public final Data a(int var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, Integer.valueOf(var), (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -129, 67108863, (i)null);
      }

      public final Data a(int var, int var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, Integer.valueOf(var), Integer.valueOf(var), (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1572865, 67108863, (i)null);
      }

      public final Data a(int var, String var, String var, b var) {
         l.b(var, "path");
         String var;
         if(var != null) {
            var = var.a();
         } else {
            var = null;
         }

         String var;
         if(var != null) {
            var = var.b();
         } else {
            var = null;
         }

         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, var, "error", var, Integer.valueOf(var), var, var, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, Integer.MAX_VALUE, 67108832, (i)null);
      }

      public final Data a(Impression.CustomiseMonzoMeLinkFrom var) {
         l.b(var, "entryPoint");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var.a(), (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 67108863, (i)null);
      }

      public final Data a(h var) {
         l.b(var, "category");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var.f(), (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -65537, 67108863, (i)null);
      }

      public final Data a(h var, YearMonth var) {
         l.b(var, "category");
         l.b(var, "month");
         int var = (int)var.c(1).a((Temporal)YearMonth.a().c(2), (TemporalUnit)ChronoUnit.j);
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var.f(), Integer.valueOf(0 - var), (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -196609, 67108863, (i)null);
      }

      public final Data a(Boolean var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, var, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -536870913, 67108863, (i)null);
      }

      public final Data a(Integer var) {
         return new Data(var, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2, 67108863, (i)null);
      }

      public final Data a(String var) {
         l.b(var, "url");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, var, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -33, 67108863, (i)null);
      }

      public final Data a(String var, String var) {
         l.b(var, "oldCategory");
         l.b(var, "newCategory");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, var, var, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -49153, 67108863, (i)null);
      }

      public final Data a(String var, boolean var) {
         l.b(var, "videoErrorReason");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, Boolean.valueOf(var), var, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67102719, (i)null);
      }

      public final Data a(boolean var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, Boolean.valueOf(var), (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -262145, 67108863, (i)null);
      }

      public final Data a(boolean var, Long var, Long var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, Boolean.valueOf(var), var, var, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67107071, (i)null);
      }

      public final Data a(boolean var, boolean var, boolean var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, Boolean.valueOf(var), Boolean.valueOf(var), (String)null, Boolean.valueOf(var), (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -11265, 67108863, (i)null);
      }

      public final Data b(Integer var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, var, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -65, 67108863, (i)null);
      }

      public final Data b(String var) {
         l.b(var, "openedFrom");
         return new Data((Integer)null, var, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -3, 67108863, (i)null);
      }

      public final Data b(String var, String var) {
         l.b(var, "logoutReason");
         l.b(var, "userId");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, var, var, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108767, (i)null);
      }

      public final Data b(boolean var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, "PIN", Boolean.valueOf(var), (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -12582913, 67108863, (i)null);
      }

      public final Data c(String var) {
         l.b(var, "transactionId");
         return new Data((Integer)null, (String)null, var, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -5, 67108863, (i)null);
      }

      public final Data c(boolean var) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, Boolean.valueOf(var), (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67092479, (i)null);
      }

      public final Data d(String var) {
         l.b(var, "referrer");
         return new Data((Integer)null, (String)null, (String)null, var, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -9, 67108863, (i)null);
      }

      public final Data e(String var) {
         l.b(var, "redirectId");
         return new Data((Integer)null, (String)null, (String)null, (String)null, var, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -17, 67108863, (i)null);
      }

      public final Data f(String var) {
         l.b(var, "cameFrom");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, var, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -257, 67108863, (i)null);
      }

      public final Data g(String var) {
         l.b(var, "type");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108862, (i)null);
      }

      public final Data h(String var) {
         l.b(var, "description");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, var, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -513, 67108863, (i)null);
      }

      public final Data i(String var) {
         l.b(var, "value");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 67108863, (i)null);
      }

      public final Data j(String var) {
         l.b(var, "gpsAdid");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, var, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1073741825, 67108863, (i)null);
      }

      public final Data k(String var) {
         l.b(var, "shortcutType");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, var, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -16777217, 67108863, (i)null);
      }

      public final Data l(String var) {
         l.b(var, "diligence");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, var, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -268435457, 67108863, (i)null);
      }

      public final Data m(String var) {
         l.b(var, "number");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, var, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67100671, (i)null);
      }
   }
}
