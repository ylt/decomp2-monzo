package co.uk.getmondo.api.model.tracking;

public enum a {
   GOLDEN_TICKET("golden-ticket.share-sheet.share"),
   INVITE_CONTACT_DISCOVERY("p2p.invite.share-sheet.share"),
   REQUEST_MONEY("request.share-sheet.share"),
   REQUEST_MONEY_CUSTOMISE("request.custom-share-sheet.share"),
   TOP_UP_SHARE("topup.share-sheet.share"),
   WAITING_LIST_SHARE("wl.share-sheet.share"),
   WAITING_LIST_WEB_EVENT("wl.webview-share-sheet.share");

   public final String value;

   private a(String var) {
      this.value = var;
   }
}
