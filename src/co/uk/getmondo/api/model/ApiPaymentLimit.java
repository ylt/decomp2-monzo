package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001)BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u001f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0002\u0010\u0012J\u0010\u0010 \u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0002\u0010\u0012J\u0010\u0010!\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0002\u0010\u0018J\\\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020\f2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010&\u001a\u00020'HÖ\u0001J\t\u0010(\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0015\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\n\n\u0002\u0010\u0019\u001a\u0004\b\u0017\u0010\u0018R\u0015\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u001a\u0010\u0012¨\u0006*"},
   d2 = {"Lco/uk/getmondo/api/model/ApiPaymentLimit;", "", "id", "", "name", "type", "Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "currency", "limit", "", "utilization", "unlimited", "", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V", "getCurrency", "()Ljava/lang/String;", "getId", "getLimit", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getName", "getType", "()Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "getUnlimited", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getUtilization", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/ApiPaymentLimit;", "equals", "other", "hashCode", "", "toString", "LimitType", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPaymentLimit {
   private final String currency;
   private final String id;
   private final Long limit;
   private final String name;
   private final ApiPaymentLimit.LimitType type;
   private final Boolean unlimited;
   private final Long utilization;

   public ApiPaymentLimit(String var, String var, ApiPaymentLimit.LimitType var, String var, Long var, Long var, Boolean var) {
      l.b(var, "id");
      l.b(var, "name");
      l.b(var, "type");
      super();
      this.id = var;
      this.name = var;
      this.type = var;
      this.currency = var;
      this.limit = var;
      this.utilization = var;
      this.unlimited = var;
   }

   // $FF: synthetic method
   public ApiPaymentLimit(String var, String var, ApiPaymentLimit.LimitType var, String var, Long var, Long var, Boolean var, int var, i var) {
      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (Long)null;
      }

      if((var & 32) != 0) {
         var = (Long)null;
      }

      if((var & 64) != 0) {
         var = Boolean.valueOf(false);
      }

      this(var, var, var, var, var, var, var);
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.name;
   }

   public final ApiPaymentLimit.LimitType c() {
      return this.type;
   }

   public final String d() {
      return this.currency;
   }

   public final Long e() {
      return this.limit;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label38: {
            if(var instanceof ApiPaymentLimit) {
               ApiPaymentLimit var = (ApiPaymentLimit)var;
               if(l.a(this.id, var.id) && l.a(this.name, var.name) && l.a(this.type, var.type) && l.a(this.currency, var.currency) && l.a(this.limit, var.limit) && l.a(this.utilization, var.utilization) && l.a(this.unlimited, var.unlimited)) {
                  break label38;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final Long f() {
      return this.utilization;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.name;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ApiPaymentLimit.LimitType var = this.type;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.currency;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Long var = this.limit;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.utilization;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Boolean var = this.unlimited;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiPaymentLimit(id=" + this.id + ", name=" + this.name + ", type=" + this.type + ", currency=" + this.currency + ", limit=" + this.limit + ", utilization=" + this.utilization + ", unlimited=" + this.unlimited + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "", "(Ljava/lang/String;I)V", "AMOUNT", "COUNT", "VIRTUAL", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum LimitType {
      @h(
         a = "amount"
      )
      AMOUNT,
      @h(
         a = "count"
      )
      COUNT,
      @h(
         a = "virtual"
      )
      VIRTUAL;

      static {
         ApiPaymentLimit.LimitType var = new ApiPaymentLimit.LimitType("AMOUNT", 0);
         AMOUNT = var;
         ApiPaymentLimit.LimitType var = new ApiPaymentLimit.LimitType("COUNT", 1);
         COUNT = var;
         ApiPaymentLimit.LimitType var = new ApiPaymentLimit.LimitType("VIRTUAL", 2);
         VIRTUAL = var;
      }
   }
}
