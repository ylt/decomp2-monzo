package co.uk.getmondo.api.model;

import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u000b\bf\u0018\u00002\u00020\u0001J\b\u0010\u000e\u001a\u00020\u0004H\u0016R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\tR\u0014\u0010\f\u001a\u0004\u0018\u00010\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\t¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/api/model/Address;", "Landroid/os/Parcelable;", "addressLines", "", "", "getAddressLines", "()Ljava/util/List;", "administrativeArea", "getAdministrativeArea", "()Ljava/lang/String;", "countryCode", "getCountryCode", "postalCode", "getPostalCode", "formattedSingleLine", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface Address extends Parcelable {
   List a();

   String b();

   String c();

   String d();

   String e();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class DefaultImpls {
      public static String a(Address var) {
         Iterable var = (Iterable)m.b((Collection)var.a(), (Iterable)m.c(new String[]{var.b(), var.d()}));
         Collection var = (Collection)(new ArrayList());
         Iterator var = var.iterator();

         while(var.hasNext()) {
            Object var = var.next();
            boolean var;
            if(!j.a((CharSequence)((String)var))) {
               var = true;
            } else {
               var = false;
            }

            if(var) {
               var.add(var);
            }
         }

         return m.a((Iterable)((List)var), (CharSequence)", ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null);
      }
   }
}
