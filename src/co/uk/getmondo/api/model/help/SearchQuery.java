package co.uk.getmondo.api.model.help;

import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003J-\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/api/model/help/SearchQuery;", "", "query", "", "model", "sections", "", "Lco/uk/getmondo/api/model/help/Section;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getModel", "()Ljava/lang/String;", "getQuery", "getSections", "()Ljava/util/List;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SearchQuery {
   private final String model;
   private final String query;
   private final List sections;

   public SearchQuery(String var, String var, List var) {
      l.b(var, "query");
      l.b(var, "model");
      l.b(var, "sections");
      super();
      this.query = var;
      this.model = var;
      this.sections = var;
   }

   public final List a() {
      return this.sections;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof SearchQuery) {
               SearchQuery var = (SearchQuery)var;
               if(l.a(this.query, var.query) && l.a(this.model, var.model) && l.a(this.sections, var.sections)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.query;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.model;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      List var = this.sections;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "SearchQuery(query=" + this.query + ", model=" + this.model + ", sections=" + this.sections + ")";
   }
}
