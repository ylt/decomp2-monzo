package co.uk.getmondo.api.model.help;

import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/api/model/help/Section;", "", "title", "", "topics", "", "Lco/uk/getmondo/api/model/help/Topic;", "(Ljava/lang/String;Ljava/util/List;)V", "getTitle", "()Ljava/lang/String;", "getTopics", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Section {
   private final String title;
   private final List topics;

   public Section(String var, List var) {
      l.b(var, "title");
      l.b(var, "topics");
      super();
      this.title = var;
      this.topics = var;
   }

   public final String a() {
      return this.title;
   }

   public final List b() {
      return this.topics;
   }

   public final String c() {
      return this.title;
   }

   public final List d() {
      return this.topics;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof Section) {
               Section var = (Section)var;
               if(l.a(this.title, var.title) && l.a(this.topics, var.topics)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.title;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      List var = this.topics;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "Section(title=" + this.title + ", topics=" + this.topics + ")";
   }
}
