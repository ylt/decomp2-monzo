package co.uk.getmondo.api.model;

import java.io.Serializable;

public class c implements Serializable {
   private int bump;
   private String id;
   private String text;
   private String type;

   public c(String var, int var) {
      this.text = var;
      this.bump = var;
   }

   public String a() {
      return this.id;
   }

   public String b() {
      return this.type;
   }

   public String c() {
      return this.text;
   }

   public int d() {
      return this.bump;
   }
}
