package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u000bJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\u001c\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0002\u0010\u0013JV\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\nHÆ\u0001¢\u0006\u0002\u0010\u001eJ\u0013\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\"\u001a\u00020#HÖ\u0001J\t\u0010$\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0015\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\r¨\u0006%"},
   d2 = {"Lco/uk/getmondo/api/model/ApiCard;", "", "id", "", "accountId", "processorToken", "expires", "lastDigits", "status", "replacementOrdered", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "getAccountId", "()Ljava/lang/String;", "getExpires", "getId", "getLastDigits", "getProcessorToken", "getReplacementOrdered", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getStatus", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lco/uk/getmondo/api/model/ApiCard;", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiCard {
   private final String accountId;
   private final String expires;
   private final String id;
   private final String lastDigits;
   private final String processorToken;
   private final Long replacementOrdered;
   private final String status;

   public ApiCard(String var, String var, String var, String var, String var, String var, Long var) {
      l.b(var, "id");
      l.b(var, "accountId");
      l.b(var, "processorToken");
      l.b(var, "expires");
      l.b(var, "lastDigits");
      l.b(var, "status");
      super();
      this.id = var;
      this.accountId = var;
      this.processorToken = var;
      this.expires = var;
      this.lastDigits = var;
      this.status = var;
      this.replacementOrdered = var;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final String c() {
      return this.processorToken;
   }

   public final String d() {
      return this.expires;
   }

   public final String e() {
      return this.lastDigits;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label38: {
            if(var instanceof ApiCard) {
               ApiCard var = (ApiCard)var;
               if(l.a(this.id, var.id) && l.a(this.accountId, var.accountId) && l.a(this.processorToken, var.processorToken) && l.a(this.expires, var.expires) && l.a(this.lastDigits, var.lastDigits) && l.a(this.status, var.status) && l.a(this.replacementOrdered, var.replacementOrdered)) {
                  break label38;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.status;
   }

   public final Long g() {
      return this.replacementOrdered;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.accountId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.processorToken;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.expires;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.lastDigits;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.status;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Long var = this.replacementOrdered;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiCard(id=" + this.id + ", accountId=" + this.accountId + ", processorToken=" + this.processorToken + ", expires=" + this.expires + ", lastDigits=" + this.lastDigits + ", status=" + this.status + ", replacementOrdered=" + this.replacementOrdered + ")";
   }
}
