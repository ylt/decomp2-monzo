package co.uk.getmondo.api.model.signup;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo;", "", "status", "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "stage", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V", "getStage", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "getStatus", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Stage", "Status", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignupInfo {
   private final SignupInfo.Stage stage;
   private final SignupInfo.Status status;

   public SignupInfo(SignupInfo.Status var, SignupInfo.Stage var) {
      l.b(var, "status");
      l.b(var, "stage");
      super();
      this.status = var;
      this.stage = var;
   }

   // $FF: synthetic method
   public SignupInfo(SignupInfo.Status var, SignupInfo.Stage var, int var, i var) {
      if((var & 2) != 0) {
         var = SignupInfo.Stage.NONE;
      }

      this(var, var);
   }

   public final SignupInfo.Status a() {
      return this.status;
   }

   public final SignupInfo a(SignupInfo.Status var, SignupInfo.Stage var) {
      l.b(var, "status");
      l.b(var, "stage");
      return new SignupInfo(var, var);
   }

   public final SignupInfo.Stage b() {
      return this.stage;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof SignupInfo) {
               SignupInfo var = (SignupInfo)var;
               if(l.a(this.status, var.status) && l.a(this.stage, var.stage)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      SignupInfo.Status var = this.status;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      SignupInfo.Stage var = this.stage;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "SignupInfo(status=" + this.status + ", stage=" + this.stage + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0010\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "", "(Ljava/lang/String;I)V", "PROFILE_DETAILS", "PHONE_VERIFICATION", "MARKETING_OPT_IN", "LEGAL_DOCUMENTS", "IDENTITY_VERIFICATION", "TAX_RESIDENCY", "CARD_ORDER", "DEVICE_AUTHENTICATION_ENROLMENT", "CARD_ACTIVATION", "PENDING", "DONE", "NONE", "WAIT_LIST_SIGNUP", "WAIT_LIST", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Stage {
      @h(
         a = "card-activation"
      )
      CARD_ACTIVATION,
      @h(
         a = "card-order"
      )
      CARD_ORDER,
      @h(
         a = "device-authentication-enrolment"
      )
      DEVICE_AUTHENTICATION_ENROLMENT,
      @h(
         a = "done"
      )
      DONE,
      @h(
         a = "identity-verification"
      )
      IDENTITY_VERIFICATION,
      @h(
         a = "legal-documents"
      )
      LEGAL_DOCUMENTS,
      @h(
         a = "marketing-opt-in"
      )
      MARKETING_OPT_IN,
      @h(
         a = ""
      )
      NONE,
      @h(
         a = "pending"
      )
      PENDING,
      @h(
         a = "phone-verification"
      )
      PHONE_VERIFICATION,
      @h(
         a = "profile-details"
      )
      PROFILE_DETAILS,
      @h(
         a = "tax-residency"
      )
      TAX_RESIDENCY,
      WAIT_LIST,
      WAIT_LIST_SIGNUP;

      static {
         SignupInfo.Stage var = new SignupInfo.Stage("PROFILE_DETAILS", 0);
         PROFILE_DETAILS = var;
         SignupInfo.Stage var = new SignupInfo.Stage("PHONE_VERIFICATION", 1);
         PHONE_VERIFICATION = var;
         SignupInfo.Stage var = new SignupInfo.Stage("MARKETING_OPT_IN", 2);
         MARKETING_OPT_IN = var;
         SignupInfo.Stage var = new SignupInfo.Stage("LEGAL_DOCUMENTS", 3);
         LEGAL_DOCUMENTS = var;
         SignupInfo.Stage var = new SignupInfo.Stage("IDENTITY_VERIFICATION", 4);
         IDENTITY_VERIFICATION = var;
         SignupInfo.Stage var = new SignupInfo.Stage("TAX_RESIDENCY", 5);
         TAX_RESIDENCY = var;
         SignupInfo.Stage var = new SignupInfo.Stage("CARD_ORDER", 6);
         CARD_ORDER = var;
         SignupInfo.Stage var = new SignupInfo.Stage("DEVICE_AUTHENTICATION_ENROLMENT", 7);
         DEVICE_AUTHENTICATION_ENROLMENT = var;
         SignupInfo.Stage var = new SignupInfo.Stage("CARD_ACTIVATION", 8);
         CARD_ACTIVATION = var;
         SignupInfo.Stage var = new SignupInfo.Stage("PENDING", 9);
         PENDING = var;
         SignupInfo.Stage var = new SignupInfo.Stage("DONE", 10);
         DONE = var;
         SignupInfo.Stage var = new SignupInfo.Stage("NONE", 11);
         NONE = var;
         SignupInfo.Stage var = new SignupInfo.Stage("WAIT_LIST_SIGNUP", 12);
         WAIT_LIST_SIGNUP = var;
         SignupInfo.Stage var = new SignupInfo.Stage("WAIT_LIST", 13);
         WAIT_LIST = var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "", "(Ljava/lang/String;I)V", "NOT_STARTED", "IN_PROGRESS", "APPROVED", "COMPLETED", "REJECTED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Status {
      @h(
         a = "approved"
      )
      APPROVED,
      @h(
         a = "completed"
      )
      COMPLETED,
      @h(
         a = "in-progress"
      )
      IN_PROGRESS,
      @h(
         a = "not-started"
      )
      NOT_STARTED,
      @h(
         a = "rejected"
      )
      REJECTED;

      static {
         SignupInfo.Status var = new SignupInfo.Status("NOT_STARTED", 0);
         NOT_STARTED = var;
         SignupInfo.Status var = new SignupInfo.Status("IN_PROGRESS", 1);
         IN_PROGRESS = var;
         SignupInfo.Status var = new SignupInfo.Status("APPROVED", 2);
         APPROVED = var;
         SignupInfo.Status var = new SignupInfo.Status("COMPLETED", 3);
         COMPLETED = var;
         SignupInfo.Status var = new SignupInfo.Status("REJECTED", 4);
         REJECTED = var;
      }
   }
}
