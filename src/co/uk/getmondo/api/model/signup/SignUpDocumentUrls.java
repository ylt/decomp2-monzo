package co.uk.getmondo.api.model.signup;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003JE\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "", "termsAndConditionsUrl", "", "termsAndConditionsVersion", "privacyPolicyUrl", "privacyPolicyVersion", "fscsInformationSheetUrl", "fscsInformationSheetVersion", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getFscsInformationSheetUrl", "()Ljava/lang/String;", "getFscsInformationSheetVersion", "getPrivacyPolicyUrl", "getPrivacyPolicyVersion", "getTermsAndConditionsUrl", "getTermsAndConditionsVersion", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignUpDocumentUrls {
   private final String fscsInformationSheetUrl;
   private final String fscsInformationSheetVersion;
   private final String privacyPolicyUrl;
   private final String privacyPolicyVersion;
   private final String termsAndConditionsUrl;
   private final String termsAndConditionsVersion;

   public SignUpDocumentUrls(@h(a = "terms_and_conditions_url") String var, @h(a = "terms_and_conditions_version") String var, @h(a = "privacy_policy_url") String var, @h(a = "privacy_policy_version") String var, @h(a = "fscs_information_sheet_url") String var, @h(a = "fscs_information_sheet_version") String var) {
      l.b(var, "termsAndConditionsUrl");
      l.b(var, "termsAndConditionsVersion");
      l.b(var, "privacyPolicyUrl");
      l.b(var, "privacyPolicyVersion");
      l.b(var, "fscsInformationSheetUrl");
      l.b(var, "fscsInformationSheetVersion");
      super();
      this.termsAndConditionsUrl = var;
      this.termsAndConditionsVersion = var;
      this.privacyPolicyUrl = var;
      this.privacyPolicyVersion = var;
      this.fscsInformationSheetUrl = var;
      this.fscsInformationSheetVersion = var;
   }

   public final String a() {
      return this.termsAndConditionsUrl;
   }

   public final String b() {
      return this.termsAndConditionsVersion;
   }

   public final String c() {
      return this.privacyPolicyUrl;
   }

   public final String d() {
      return this.privacyPolicyVersion;
   }

   public final String e() {
      return this.fscsInformationSheetUrl;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label36: {
            if(var instanceof SignUpDocumentUrls) {
               SignUpDocumentUrls var = (SignUpDocumentUrls)var;
               if(l.a(this.termsAndConditionsUrl, var.termsAndConditionsUrl) && l.a(this.termsAndConditionsVersion, var.termsAndConditionsVersion) && l.a(this.privacyPolicyUrl, var.privacyPolicyUrl) && l.a(this.privacyPolicyVersion, var.privacyPolicyVersion) && l.a(this.fscsInformationSheetUrl, var.fscsInformationSheetUrl) && l.a(this.fscsInformationSheetVersion, var.fscsInformationSheetVersion)) {
                  break label36;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.fscsInformationSheetVersion;
   }

   public int hashCode() {
      int var = 0;
      String var = this.termsAndConditionsUrl;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.termsAndConditionsVersion;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.privacyPolicyUrl;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.privacyPolicyVersion;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fscsInformationSheetUrl;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fscsInformationSheetVersion;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "SignUpDocumentUrls(termsAndConditionsUrl=" + this.termsAndConditionsUrl + ", termsAndConditionsVersion=" + this.termsAndConditionsVersion + ", privacyPolicyUrl=" + this.privacyPolicyUrl + ", privacyPolicyVersion=" + this.privacyPolicyVersion + ", fscsInformationSheetUrl=" + this.fscsInformationSheetUrl + ", fscsInformationSheetVersion=" + this.fscsInformationSheetVersion + ")";
   }
}
