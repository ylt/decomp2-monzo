package co.uk.getmondo.api.model;

public class b {
   private String code;
   private String message;

   public b(String var, String var) {
      this.code = var;
      this.message = var;
   }

   public String a() {
      return this.code;
   }

   public String b() {
      return this.message;
   }

   public boolean c() {
      return "bad_request.app_version_outdated".equals(this.code);
   }

   public String toString() {
      return "ApiError{code=" + this.code + ", message=" + this.message + "}";
   }
}
