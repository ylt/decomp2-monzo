package co.uk.getmondo.api.model.tax_residency;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.squareup.moshi.h;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000 12\u00020\u0001:\u000212B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B9\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\b\b\u0001\u0010\n\u001a\u00020\b\u0012\u000e\b\u0001\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0002\u0010\u000eJ\t\u0010 \u001a\u00020\u0006HÆ\u0003J\t\u0010!\u001a\u00020\bHÆ\u0003J\t\u0010\"\u001a\u00020\bHÆ\u0003J\t\u0010#\u001a\u00020\bHÆ\u0003J\u000f\u0010$\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003JA\u0010%\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0003\u0010\n\u001a\u00020\b2\u000e\b\u0003\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0001J\b\u0010&\u001a\u00020'H\u0016J\u0013\u0010(\u001a\u00020\b2\b\u0010)\u001a\u0004\u0018\u00010*HÖ\u0003J\t\u0010+\u001a\u00020'HÖ\u0001J\t\u0010,\u001a\u00020\u0006HÖ\u0001J\u0018\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020'H\u0016R\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013R\u0011\u0010\u0016\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0010R\u0011\u0010\u0018\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0010R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\r8F¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001aR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f¨\u00063"},
   d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "countryCode", "", "reportable", "", "completed", "allowNoTin", "tinTypes", "", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "(Ljava/lang/String;ZZZLjava/util/List;)V", "getAllowNoTin", "()Z", "alpha2CountryCode", "getAlpha2CountryCode", "()Ljava/lang/String;", "getCompleted", "getCountryCode", "hasMultipleTinTypes", "getHasMultipleTinTypes", "primaryTinType", "getPrimaryTinType", "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "getReportable", "secondaryTinType", "getSecondaryTinType", "getTinTypes", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "TinType", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Jurisdiction implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public Jurisdiction a(Parcel var) {
         l.b(var, "source");
         return new Jurisdiction(var);
      }

      public Jurisdiction[] a(int var) {
         return new Jurisdiction[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final Jurisdiction.Companion Companion = new Jurisdiction.Companion((i)null);
   private final boolean allowNoTin;
   private final boolean completed;
   private final String countryCode;
   private final boolean reportable;
   private final List tinTypes;

   public Jurisdiction(Parcel var) {
      boolean var = false;
      l.b(var, "source");
      String var = var.readString();
      l.a(var, "source.readString()");
      boolean var;
      if(1 == var.readInt()) {
         var = true;
      } else {
         var = false;
      }

      boolean var;
      if(1 == var.readInt()) {
         var = true;
      } else {
         var = false;
      }

      if(1 == var.readInt()) {
         var = true;
      }

      ArrayList var = var.createTypedArrayList(Jurisdiction.TinType.CREATOR);
      l.a(var, "source.createTypedArrayList(TinType.CREATOR)");
      this(var, var, var, var, (List)var);
   }

   public Jurisdiction(@h(a = "country_code") String var, boolean var, boolean var, @h(a = "allow_no_tin") boolean var, @h(a = "tin_types") List var) {
      l.b(var, "countryCode");
      l.b(var, "tinTypes");
      super();
      this.countryCode = var;
      this.reportable = var;
      this.completed = var;
      this.allowNoTin = var;
      this.tinTypes = var;
   }

   public final String a() {
      co.uk.getmondo.d.i var = co.uk.getmondo.d.i.Companion.a(this.countryCode);
      String var;
      if(var != null) {
         var = var.d();
      } else {
         var = null;
      }

      return var;
   }

   public final boolean b() {
      boolean var;
      if(this.tinTypes.size() >= 2) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final Jurisdiction.TinType c() {
      return (Jurisdiction.TinType)this.tinTypes.get(0);
   }

   public final Jurisdiction.TinType d() {
      return (Jurisdiction.TinType)m.c(this.tinTypes, 1);
   }

   public int describeContents() {
      return 0;
   }

   public final String e() {
      return this.countryCode;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof Jurisdiction)) {
            return var;
         }

         Jurisdiction var = (Jurisdiction)var;
         var = var;
         if(!l.a(this.countryCode, var.countryCode)) {
            return var;
         }

         boolean var;
         if(this.reportable == var.reportable) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.completed == var.completed) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.allowNoTin == var.allowNoTin) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.tinTypes, var.tinTypes)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final boolean f() {
      return this.reportable;
   }

   public final boolean g() {
      return this.completed;
   }

   public final boolean h() {
      return this.allowNoTin;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "Jurisdiction(countryCode=" + this.countryCode + ", reportable=" + this.reportable + ", completed=" + this.completed + ", allowNoTin=" + this.allowNoTin + ", tinTypes=" + this.tinTypes + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      byte var = 1;
      l.b(var, "dest");
      var.writeString(this.countryCode);
      byte var;
      if(this.reportable) {
         var = 1;
      } else {
         var = 0;
      }

      var.writeInt(var);
      if(this.completed) {
         var = 1;
      } else {
         var = 0;
      }

      var.writeInt(var);
      if(this.allowNoTin) {
         var = var;
      } else {
         var = 0;
      }

      var.writeInt(var);
      var.writeTypedList(this.tinTypes);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001f\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0006HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "friendlyName", "example", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getExample", "()Ljava/lang/String;", "getFriendlyName", "getId", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class TinType implements Parcelable {
      public static final Creator CREATOR = (Creator)(new Creator() {
         public Jurisdiction.TinType a(Parcel var) {
            l.b(var, "source");
            return new Jurisdiction.TinType(var);
         }

         public Jurisdiction.TinType[] a(int var) {
            return new Jurisdiction.TinType[var];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var) {
            return this.a(var);
         }

         // $FF: synthetic method
         public Object[] newArray(int var) {
            return (Object[])this.a(var);
         }
      });
      public static final Jurisdiction.Companion Companion = new Jurisdiction.Companion((i)null);
      private final String example;
      private final String friendlyName;
      private final String id;

      public TinType(Parcel var) {
         l.b(var, "source");
         String var = var.readString();
         l.a(var, "source.readString()");
         String var = var.readString();
         l.a(var, "source.readString()");
         String var = var.readString();
         l.a(var, "source.readString()");
         this(var, var, var);
      }

      public TinType(String var, @h(a = "friendly_name") String var, String var) {
         l.b(var, "id");
         l.b(var, "friendlyName");
         l.b(var, "example");
         super();
         this.id = var;
         this.friendlyName = var;
         this.example = var;
      }

      public final String a() {
         return this.id;
      }

      public final String b() {
         return this.friendlyName;
      }

      public final String c() {
         return this.example;
      }

      public int describeContents() {
         return 0;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label30: {
               if(var instanceof Jurisdiction.TinType) {
                  Jurisdiction.TinType var = (Jurisdiction.TinType)var;
                  if(l.a(this.id, var.id) && l.a(this.friendlyName, var.friendlyName) && l.a(this.example, var.example)) {
                     break label30;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.id;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.friendlyName;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.example;
         if(var != null) {
            var = var.hashCode();
         }

         return (var + var * 31) * 31 + var;
      }

      public String toString() {
         return "TinType(id=" + this.id + ", friendlyName=" + this.friendlyName + ", example=" + this.example + ")";
      }

      public void writeToParcel(Parcel var, int var) {
         l.b(var, "dest");
         var.writeString(this.id);
         var.writeString(this.friendlyName);
         var.writeString(this.example);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var) {
         this();
      }
   }
}
