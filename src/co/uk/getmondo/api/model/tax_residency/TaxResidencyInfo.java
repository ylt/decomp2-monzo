package co.uk.getmondo.api.model.tax_residency;

import com.squareup.moshi.h;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001dB9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0003\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007¢\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J\u0011\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007HÆ\u0003J?\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\u000e\b\u0003\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\bHÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000f¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;", "", "status", "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;", "allowSummaryScreen", "", "suggestedCountries", "", "", "jurisdictions", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "(Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;ZLjava/util/List;Ljava/util/List;)V", "getAllowSummaryScreen", "()Z", "getJurisdictions", "()Ljava/util/List;", "getStatus", "()Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;", "getSuggestedCountries", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "Status", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TaxResidencyInfo {
   private final boolean allowSummaryScreen;
   private final List jurisdictions;
   private final TaxResidencyInfo.Status status;
   private final List suggestedCountries;

   public TaxResidencyInfo(TaxResidencyInfo.Status var, @h(a = "allow_summary_screen") boolean var, @h(a = "suggested_countries") List var, List var) {
      l.b(var, "status");
      l.b(var, "suggestedCountries");
      super();
      this.status = var;
      this.allowSummaryScreen = var;
      this.suggestedCountries = var;
      this.jurisdictions = var;
   }

   // $FF: synthetic method
   public TaxResidencyInfo(TaxResidencyInfo.Status var, boolean var, List var, List var, int var, i var) {
      if((var & 4) != 0) {
         var = m.a();
      }

      if((var & 8) != 0) {
         var = m.a();
      }

      this(var, var, var, var);
   }

   public final TaxResidencyInfo.Status a() {
      return this.status;
   }

   public final boolean b() {
      return this.allowSummaryScreen;
   }

   public final List c() {
      return this.suggestedCountries;
   }

   public final List d() {
      return this.jurisdictions;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof TaxResidencyInfo)) {
            return var;
         }

         TaxResidencyInfo var = (TaxResidencyInfo)var;
         var = var;
         if(!l.a(this.status, var.status)) {
            return var;
         }

         boolean var;
         if(this.allowSummaryScreen == var.allowSummaryScreen) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.suggestedCountries, var.suggestedCountries)) {
            return var;
         }

         var = var;
         if(!l.a(this.jurisdictions, var.jurisdictions)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "TaxResidencyInfo(status=" + this.status + ", allowSummaryScreen=" + this.allowSummaryScreen + ", suggestedCountries=" + this.suggestedCountries + ", jurisdictions=" + this.jurisdictions + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo$Status;", "", "(Ljava/lang/String;I)V", "NOT_STARTED", "IN_PROGRESS", "SUBMITTED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Status {
      @h(
         a = "in-progress"
      )
      IN_PROGRESS,
      @h(
         a = "not-started"
      )
      NOT_STARTED,
      @h(
         a = "submitted"
      )
      SUBMITTED;

      static {
         TaxResidencyInfo.Status var = new TaxResidencyInfo.Status("NOT_STARTED", 0);
         NOT_STARTED = var;
         TaxResidencyInfo.Status var = new TaxResidencyInfo.Status("IN_PROGRESS", 1);
         IN_PROGRESS = var;
         TaxResidencyInfo.Status var = new TaxResidencyInfo.Status("SUBMITTED", 2);
         SUBMITTED = var;
      }
   }
}
