package co.uk.getmondo.api.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003J7\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\f¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/api/model/ApiBalance;", "", "balance", "", "spendToday", "currency", "", "localSpend", "", "Lco/uk/getmondo/api/model/ApiLocalSpend;", "(JJLjava/lang/String;Ljava/util/List;)V", "getBalance", "()J", "getCurrency", "()Ljava/lang/String;", "getLocalSpend", "()Ljava/util/List;", "getSpendToday", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiBalance {
   private final long balance;
   private final String currency;
   private final List localSpend;
   private final long spendToday;

   public ApiBalance(long var, long var, String var, List var) {
      l.b(var, "currency");
      l.b(var, "localSpend");
      super();
      this.balance = var;
      this.spendToday = var;
      this.currency = var;
      this.localSpend = var;
   }

   // $FF: synthetic method
   public ApiBalance(long var, long var, String var, List var, int var, i var) {
      if((var & 8) != 0) {
         var = m.a();
      }

      this(var, var, var, var);
   }

   public final long a() {
      return this.balance;
   }

   public final long b() {
      return this.spendToday;
   }

   public final String c() {
      return this.currency;
   }

   public final List d() {
      return this.localSpend;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiBalance)) {
            return var;
         }

         ApiBalance var = (ApiBalance)var;
         boolean var;
         if(this.balance == var.balance) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.spendToday == var.spendToday) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.currency, var.currency)) {
            return var;
         }

         var = var;
         if(!l.a(this.localSpend, var.localSpend)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      long var = this.balance;
      int var = (int)(var ^ var >>> 32);
      var = this.spendToday;
      int var = (int)(var ^ var >>> 32);
      String var = this.currency;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      List var = this.localSpend;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var * 31 + var) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiBalance(balance=" + this.balance + ", spendToday=" + this.spendToday + ", currency=" + this.currency + ", localSpend=" + this.localSpend + ")";
   }
}
