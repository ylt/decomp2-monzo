package co.uk.getmondo.api.model.sign_up;

import co.uk.getmondo.api.model.signup.SignupInfo;
import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0003\u0010\b\u001a\u00020\t\u0012\b\b\u0003\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001b\u001a\u00020\tHÆ\u0003J\t\u0010\u001c\u001a\u00020\u000bHÆ\u0003JE\u0010\u001d\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0003\u0010\b\u001a\u00020\t2\b\b\u0003\u0010\n\u001a\u00020\u000bHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u00032\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006#"},
   d2 = {"Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "", "bannerEnabled", "", "signupAllowed", "bannerTitle", "", "bannerSubtitle", "signupStatus", "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "signupStage", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V", "getBannerEnabled", "()Z", "getBannerSubtitle", "()Ljava/lang/String;", "getBannerTitle", "getSignupAllowed", "getSignupStage", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "getSignupStatus", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class MigrationInfo {
   private final boolean bannerEnabled;
   private final String bannerSubtitle;
   private final String bannerTitle;
   private final boolean signupAllowed;
   private final SignupInfo.Stage signupStage;
   private final SignupInfo.Status signupStatus;

   public MigrationInfo() {
      this(false, false, (String)null, (String)null, (SignupInfo.Status)null, (SignupInfo.Stage)null, 63, (i)null);
   }

   public MigrationInfo(@h(a = "banner_enabled") boolean var, @h(a = "signup_allowed") boolean var, @h(a = "banner_title") String var, @h(a = "banner_subtitle") String var, @h(a = "signup_status") SignupInfo.Status var, @h(a = "signup_stage") SignupInfo.Stage var) {
      l.b(var, "bannerTitle");
      l.b(var, "bannerSubtitle");
      l.b(var, "signupStatus");
      l.b(var, "signupStage");
      super();
      this.bannerEnabled = var;
      this.signupAllowed = var;
      this.bannerTitle = var;
      this.bannerSubtitle = var;
      this.signupStatus = var;
      this.signupStage = var;
   }

   // $FF: synthetic method
   public MigrationInfo(boolean var, boolean var, String var, String var, SignupInfo.Status var, SignupInfo.Stage var, int var, i var) {
      boolean var = false;
      if((var & 1) != 0) {
         var = false;
      }

      if((var & 2) != 0) {
         var = var;
      }

      if((var & 4) != 0) {
         var = "";
      }

      if((var & 8) != 0) {
         var = "";
      }

      if((var & 16) != 0) {
         var = SignupInfo.Status.NOT_STARTED;
      }

      if((var & 32) != 0) {
         var = SignupInfo.Stage.NONE;
      }

      this(var, var, var, var, var, var);
   }

   public final MigrationInfo a(@h(a = "banner_enabled") boolean var, @h(a = "signup_allowed") boolean var, @h(a = "banner_title") String var, @h(a = "banner_subtitle") String var, @h(a = "signup_status") SignupInfo.Status var, @h(a = "signup_stage") SignupInfo.Stage var) {
      l.b(var, "bannerTitle");
      l.b(var, "bannerSubtitle");
      l.b(var, "signupStatus");
      l.b(var, "signupStage");
      return new MigrationInfo(var, var, var, var, var, var);
   }

   public final boolean a() {
      return this.bannerEnabled;
   }

   public final boolean b() {
      return this.signupAllowed;
   }

   public final String c() {
      return this.bannerTitle;
   }

   public final String d() {
      return this.bannerSubtitle;
   }

   public final SignupInfo.Status e() {
      return this.signupStatus;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof MigrationInfo)) {
            return var;
         }

         MigrationInfo var = (MigrationInfo)var;
         boolean var;
         if(this.bannerEnabled == var.bannerEnabled) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.signupAllowed == var.signupAllowed) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.bannerTitle, var.bannerTitle)) {
            return var;
         }

         var = var;
         if(!l.a(this.bannerSubtitle, var.bannerSubtitle)) {
            return var;
         }

         var = var;
         if(!l.a(this.signupStatus, var.signupStatus)) {
            return var;
         }

         var = var;
         if(!l.a(this.signupStage, var.signupStage)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final SignupInfo.Stage f() {
      return this.signupStage;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "MigrationInfo(bannerEnabled=" + this.bannerEnabled + ", signupAllowed=" + this.signupAllowed + ", bannerTitle=" + this.bannerTitle + ", bannerSubtitle=" + this.bannerSubtitle + ", signupStatus=" + this.signupStatus + ", signupStage=" + this.signupStage + ")";
   }
}
