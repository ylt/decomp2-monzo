package co.uk.getmondo.api.model.payments;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/api/model/payments/SeriesResponse;", "", "series", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;)V", "getSeries", "()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SeriesResponse {
   private final ApiPaymentSeries series;

   public SeriesResponse(ApiPaymentSeries var) {
      l.b(var, "series");
      super();
      this.series = var;
   }

   public final ApiPaymentSeries a() {
      return this.series;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label26: {
            if(var instanceof SeriesResponse) {
               SeriesResponse var = (SeriesResponse)var;
               if(l.a(this.series, var.series)) {
                  break label26;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      ApiPaymentSeries var = this.series;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var;
   }

   public String toString() {
      return "SeriesResponse(series=" + this.series + ")";
   }
}
