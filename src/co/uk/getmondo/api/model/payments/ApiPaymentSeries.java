package co.uk.getmondo.api.model.payments;

import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u00010BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\f\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\fHÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0006HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\nHÆ\u0003J\t\u0010&\u001a\u00020\fHÆ\u0003J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\fHÆ\u0003Jo\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\f2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\fHÆ\u0001J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020.HÖ\u0001J\t\u0010/\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0012R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0017¨\u00061"},
   d2 = {"Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "", "id", "", "accountId", "amount", "", "currency", "scheme", "schemeData", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "startDate", "Lorg/threeten/bp/LocalDate;", "intervalType", "nextIterationDate", "endDate", "(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V", "getAccountId", "()Ljava/lang/String;", "getAmount", "()J", "getCurrency", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getId", "getIntervalType", "getNextIterationDate", "getScheme", "getSchemeData", "()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "getStartDate", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "FpsSchemeData", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPaymentSeries {
   private final String accountId;
   private final long amount;
   private final String currency;
   private final LocalDate endDate;
   private final String id;
   private final String intervalType;
   private final LocalDate nextIterationDate;
   private final String scheme;
   private final ApiPaymentSeries.FpsSchemeData schemeData;
   private final LocalDate startDate;

   public ApiPaymentSeries(String var, String var, long var, String var, String var, ApiPaymentSeries.FpsSchemeData var, LocalDate var, String var, LocalDate var, LocalDate var) {
      l.b(var, "id");
      l.b(var, "accountId");
      l.b(var, "currency");
      l.b(var, "scheme");
      l.b(var, "schemeData");
      l.b(var, "startDate");
      l.b(var, "intervalType");
      l.b(var, "nextIterationDate");
      super();
      this.id = var;
      this.accountId = var;
      this.amount = var;
      this.currency = var;
      this.scheme = var;
      this.schemeData = var;
      this.startDate = var;
      this.intervalType = var;
      this.nextIterationDate = var;
      this.endDate = var;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final long c() {
      return this.amount;
   }

   public final String d() {
      return this.currency;
   }

   public final String e() {
      return this.scheme;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiPaymentSeries)) {
            return var;
         }

         ApiPaymentSeries var = (ApiPaymentSeries)var;
         var = var;
         if(!l.a(this.id, var.id)) {
            return var;
         }

         var = var;
         if(!l.a(this.accountId, var.accountId)) {
            return var;
         }

         boolean var;
         if(this.amount == var.amount) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.currency, var.currency)) {
            return var;
         }

         var = var;
         if(!l.a(this.scheme, var.scheme)) {
            return var;
         }

         var = var;
         if(!l.a(this.schemeData, var.schemeData)) {
            return var;
         }

         var = var;
         if(!l.a(this.startDate, var.startDate)) {
            return var;
         }

         var = var;
         if(!l.a(this.intervalType, var.intervalType)) {
            return var;
         }

         var = var;
         if(!l.a(this.nextIterationDate, var.nextIterationDate)) {
            return var;
         }

         var = var;
         if(!l.a(this.endDate, var.endDate)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final ApiPaymentSeries.FpsSchemeData f() {
      return this.schemeData;
   }

   public final LocalDate g() {
      return this.startDate;
   }

   public final String h() {
      return this.intervalType;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.accountId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      long var = this.amount;
      int var = (int)(var ^ var >>> 32);
      var = this.currency;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.scheme;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ApiPaymentSeries.FpsSchemeData var = this.schemeData;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      LocalDate var = this.startDate;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.intervalType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.nextIterationDate;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.endDate;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + (var + ((var + var * 31) * 31 + var) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public final LocalDate i() {
      return this.nextIterationDate;
   }

   public final LocalDate j() {
      return this.endDate;
   }

   public String toString() {
      return "ApiPaymentSeries(id=" + this.id + ", accountId=" + this.accountId + ", amount=" + this.amount + ", currency=" + this.currency + ", scheme=" + this.scheme + ", schemeData=" + this.schemeData + ", startDate=" + this.startDate + ", intervalType=" + this.intervalType + ", nextIterationDate=" + this.nextIterationDate + ", endDate=" + this.endDate + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J1\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"},
      d2 = {"Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "", "reference", "", "beneficiaryAccountNumber", "beneficiarySortCode", "beneficiaryCustomerName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBeneficiaryAccountNumber", "()Ljava/lang/String;", "getBeneficiaryCustomerName", "getBeneficiarySortCode", "getReference", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class FpsSchemeData {
      private final String beneficiaryAccountNumber;
      private final String beneficiaryCustomerName;
      private final String beneficiarySortCode;
      private final String reference;

      public FpsSchemeData(String var, String var, String var, String var) {
         l.b(var, "reference");
         l.b(var, "beneficiaryAccountNumber");
         l.b(var, "beneficiarySortCode");
         l.b(var, "beneficiaryCustomerName");
         super();
         this.reference = var;
         this.beneficiaryAccountNumber = var;
         this.beneficiarySortCode = var;
         this.beneficiaryCustomerName = var;
      }

      public final String a() {
         return this.reference;
      }

      public final String b() {
         return this.beneficiaryAccountNumber;
      }

      public final String c() {
         return this.beneficiarySortCode;
      }

      public final String d() {
         return this.beneficiaryCustomerName;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label32: {
               if(var instanceof ApiPaymentSeries.FpsSchemeData) {
                  ApiPaymentSeries.FpsSchemeData var = (ApiPaymentSeries.FpsSchemeData)var;
                  if(l.a(this.reference, var.reference) && l.a(this.beneficiaryAccountNumber, var.beneficiaryAccountNumber) && l.a(this.beneficiarySortCode, var.beneficiarySortCode) && l.a(this.beneficiaryCustomerName, var.beneficiaryCustomerName)) {
                     break label32;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.reference;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.beneficiaryAccountNumber;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.beneficiarySortCode;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.beneficiaryCustomerName;
         if(var != null) {
            var = var.hashCode();
         }

         return (var + (var + var * 31) * 31) * 31 + var;
      }

      public String toString() {
         return "FpsSchemeData(reference=" + this.reference + ", beneficiaryAccountNumber=" + this.beneficiaryAccountNumber + ", beneficiarySortCode=" + this.beneficiarySortCode + ", beneficiaryCustomerName=" + this.beneficiaryCustomerName + ")";
      }
   }
}
