package co.uk.getmondo.api.model;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/api/model/ApiCardDispatchStatus;", "", "status", "Lco/uk/getmondo/api/model/ApiCardDispatchStatus$DispatchStatus;", "estimatedDeliveryDate", "Ljava/util/Date;", "(Lco/uk/getmondo/api/model/ApiCardDispatchStatus$DispatchStatus;Ljava/util/Date;)V", "getEstimatedDeliveryDate", "()Ljava/util/Date;", "getStatus", "()Lco/uk/getmondo/api/model/ApiCardDispatchStatus$DispatchStatus;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "DispatchStatus", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiCardDispatchStatus {
   private final Date estimatedDeliveryDate;
   private final ApiCardDispatchStatus.DispatchStatus status;

   public ApiCardDispatchStatus(ApiCardDispatchStatus.DispatchStatus var, Date var) {
      l.b(var, "status");
      l.b(var, "estimatedDeliveryDate");
      super();
      this.status = var;
      this.estimatedDeliveryDate = var;
   }

   public final Date a() {
      return this.estimatedDeliveryDate;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof ApiCardDispatchStatus) {
               ApiCardDispatchStatus var = (ApiCardDispatchStatus)var;
               if(l.a(this.status, var.status) && l.a(this.estimatedDeliveryDate, var.estimatedDeliveryDate)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      ApiCardDispatchStatus.DispatchStatus var = this.status;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Date var = this.estimatedDeliveryDate;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "ApiCardDispatchStatus(status=" + this.status + ", estimatedDeliveryDate=" + this.estimatedDeliveryDate + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/api/model/ApiCardDispatchStatus$DispatchStatus;", "", "(Ljava/lang/String;I)V", "UNKNOWN", "ORDERED", "DISPATCHED", "ACTIVATED", "DELIVERED", "CANCELLED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum DispatchStatus {
      ACTIVATED,
      CANCELLED,
      DELIVERED,
      DISPATCHED,
      ORDERED,
      UNKNOWN;

      static {
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("UNKNOWN", 0);
         UNKNOWN = var;
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("ORDERED", 1);
         ORDERED = var;
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("DISPATCHED", 2);
         DISPATCHED = var;
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("ACTIVATED", 3);
         ACTIVATED = var;
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("DELIVERED", 4);
         DELIVERED = var;
         ApiCardDispatchStatus.DispatchStatus var = new ApiCardDispatchStatus.DispatchStatus("CANCELLED", 5);
         CANCELLED = var;
      }
   }
}
