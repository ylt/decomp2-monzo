package co.uk.getmondo.api.model.feed;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\tJ\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\rJ\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003JJ\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010\u0018J\u0013\u0010\u0019\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010\u001b\u001a\u00020\u0005J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiMetadata;", "", "notes", "", "hideAmount", "", "inboundP2pId", "p2pTransferId", "bacsDirectDebitInstructionId", "(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBacsDirectDebitInstructionId", "()Ljava/lang/String;", "getHideAmount", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getInboundP2pId", "getNotes", "getP2pTransferId", "component1", "component2", "component3", "component4", "component5", "copy", "(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiMetadata;", "equals", "other", "hasNotes", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiMetadata {
   private final String bacsDirectDebitInstructionId;
   private final Boolean hideAmount;
   private final String inboundP2pId;
   private final String notes;
   private final String p2pTransferId;

   public ApiMetadata() {
      this((String)null, (Boolean)null, (String)null, (String)null, (String)null, 31, (i)null);
   }

   public ApiMetadata(String var, Boolean var, String var, String var, String var) {
      this.notes = var;
      this.hideAmount = var;
      this.inboundP2pId = var;
      this.p2pTransferId = var;
      this.bacsDirectDebitInstructionId = var;
   }

   // $FF: synthetic method
   public ApiMetadata(String var, Boolean var, String var, String var, String var, int var, i var) {
      if((var & 1) != 0) {
         var = (String)null;
      }

      if((var & 2) != 0) {
         var = (Boolean)null;
      }

      if((var & 4) != 0) {
         var = (String)null;
      }

      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var);
   }

   public final boolean a() {
      boolean var;
      if(this.notes != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final String b() {
      return this.notes;
   }

   public final Boolean c() {
      return this.hideAmount;
   }

   public final String d() {
      return this.inboundP2pId;
   }

   public final String e() {
      return this.p2pTransferId;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof ApiMetadata) {
               ApiMetadata var = (ApiMetadata)var;
               if(l.a(this.notes, var.notes) && l.a(this.hideAmount, var.hideAmount) && l.a(this.inboundP2pId, var.inboundP2pId) && l.a(this.p2pTransferId, var.p2pTransferId) && l.a(this.bacsDirectDebitInstructionId, var.bacsDirectDebitInstructionId)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.bacsDirectDebitInstructionId;
   }

   public int hashCode() {
      int var = 0;
      String var = this.notes;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Boolean var = this.hideAmount;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.inboundP2pId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.p2pTransferId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.bacsDirectDebitInstructionId;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiMetadata(notes=" + this.notes + ", hideAmount=" + this.hideAmount + ", inboundP2pId=" + this.inboundP2pId + ", p2pTransferId=" + this.p2pTransferId + ", bacsDirectDebitInstructionId=" + this.bacsDirectDebitInstructionId + ")";
   }
}
