package co.uk.getmondo.api.model.feed;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;", "", "shortFormatted", "", "formatted", "latitude", "", "longitude", "(Ljava/lang/String;Ljava/lang/String;DD)V", "getFormatted", "()Ljava/lang/String;", "getLatitude", "()D", "getLongitude", "getShortFormatted", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiMerchantAddress {
   private final String formatted;
   private final double latitude;
   private final double longitude;
   private final String shortFormatted;

   public ApiMerchantAddress(String var, String var, double var, double var) {
      l.b(var, "shortFormatted");
      l.b(var, "formatted");
      super();
      this.shortFormatted = var;
      this.formatted = var;
      this.latitude = var;
      this.longitude = var;
   }

   public final String a() {
      return this.shortFormatted;
   }

   public final double b() {
      return this.latitude;
   }

   public final double c() {
      return this.longitude;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof ApiMerchantAddress) {
               ApiMerchantAddress var = (ApiMerchantAddress)var;
               if(l.a(this.shortFormatted, var.shortFormatted) && l.a(this.formatted, var.formatted) && Double.compare(this.latitude, var.latitude) == 0 && Double.compare(this.longitude, var.longitude) == 0) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.shortFormatted;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.formatted;
      if(var != null) {
         var = var.hashCode();
      }

      long var = Double.doubleToLongBits(this.latitude);
      int var = (int)(var ^ var >>> 32);
      var = Double.doubleToLongBits(this.longitude);
      return ((var * 31 + var) * 31 + var) * 31 + (int)(var ^ var >>> 32);
   }

   public String toString() {
      return "ApiMerchantAddress(shortFormatted=" + this.shortFormatted + ", formatted=" + this.formatted + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
   }
}
