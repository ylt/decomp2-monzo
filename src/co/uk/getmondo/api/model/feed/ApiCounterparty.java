package co.uk.getmondo.api.model.feed;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003JE\u0010\u0014\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010\u0018\u001a\u00020\u0016J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiCounterparty;", "", "name", "", "number", "userId", "sortCode", "accountNumber", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "getName", "getNumber", "getSortCode", "getUserId", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hasName", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiCounterparty {
   private final String accountNumber;
   private final String name;
   private final String number;
   private final String sortCode;
   private final String userId;

   public ApiCounterparty(String var, String var, String var, String var, String var) {
      this.name = var;
      this.number = var;
      this.userId = var;
      this.sortCode = var;
      this.accountNumber = var;
   }

   // $FF: synthetic method
   public ApiCounterparty(String var, String var, String var, String var, String var, int var, i var) {
      if((var & 8) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var);
   }

   public final boolean a() {
      boolean var;
      if(this.name != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final String b() {
      return this.name;
   }

   public final String c() {
      return this.number;
   }

   public final String d() {
      return this.userId;
   }

   public final String e() {
      return this.sortCode;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof ApiCounterparty) {
               ApiCounterparty var = (ApiCounterparty)var;
               if(l.a(this.name, var.name) && l.a(this.number, var.number) && l.a(this.userId, var.userId) && l.a(this.sortCode, var.sortCode) && l.a(this.accountNumber, var.accountNumber)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.accountNumber;
   }

   public int hashCode() {
      int var = 0;
      String var = this.name;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.number;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.userId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.sortCode;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.accountNumber;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiCounterparty(name=" + this.name + ", number=" + this.number + ", userId=" + this.userId + ", sortCode=" + this.sortCode + ", accountNumber=" + this.accountNumber + ")";
   }
}
