package co.uk.getmondo.api.model.feed;

import java.util.Date;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b8\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B©\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019\u0012\u0006\u0010\u001b\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u001dJ\t\u00109\u001a\u00020\u0003HÆ\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0011HÆ\u0003J\t\u0010<\u001a\u00020\u0013HÆ\u0003J\t\u0010=\u001a\u00020\u0003HÆ\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\t\u0010?\u001a\u00020\u0003HÆ\u0003J\u0011\u0010@\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019HÆ\u0003J\t\u0010A\u001a\u00020\u0013HÆ\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010D\u001a\u00020\u0007HÆ\u0003J\t\u0010E\u001a\u00020\u0007HÆ\u0003J\t\u0010F\u001a\u00020\nHÆ\u0003J\t\u0010G\u001a\u00020\nHÆ\u0003J\t\u0010H\u001a\u00020\u0003HÆ\u0003J\t\u0010I\u001a\u00020\u0003HÆ\u0003J\t\u0010J\u001a\u00020\u0003HÆ\u0003JÏ\u0001\u0010K\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u00032\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u00032\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00192\b\b\u0002\u0010\u001b\u001a\u00020\u00132\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010L\u001a\u00020\u00132\b\u0010M\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010N\u001a\u00020\u0013J\u0006\u0010O\u001a\u00020\u0013J\u0006\u0010P\u001a\u00020\u0013J\u0006\u0010Q\u001a\u00020\u0013J\t\u0010R\u001a\u00020SHÖ\u0001J\t\u0010T\u001a\u00020\u0003HÖ\u0001J\u000e\u0010U\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0003R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0017\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010#R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010#R\u0011\u0010\u000e\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010#R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b+\u0010#R\u0011\u0010\u001b\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010-R\u0011\u0010.\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b.\u0010-R\u0011\u0010/\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b/\u0010-R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b0\u0010\u001fR\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b1\u0010#R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b6\u0010#R\u0011\u0010\u0014\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b7\u0010#R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b8\u0010'¨\u0006V"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiTransaction;", "", "id", "", "merchant", "Lco/uk/getmondo/api/model/feed/ApiMerchant;", "amount", "", "localAmount", "created", "Ljava/util/Date;", "updated", "currency", "localCurrency", "description", "declineReason", "counterparty", "Lco/uk/getmondo/api/model/feed/ApiCounterparty;", "isLoad", "", "settled", "metadata", "Lco/uk/getmondo/api/model/feed/ApiMetadata;", "category", "attachments", "", "Lco/uk/getmondo/api/model/feed/ApiAttachment;", "includeInSpending", "scheme", "(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V", "getAmount", "()J", "getAttachments", "()Ljava/util/List;", "getCategory", "()Ljava/lang/String;", "getCounterparty", "()Lco/uk/getmondo/api/model/feed/ApiCounterparty;", "getCreated", "()Ljava/util/Date;", "getCurrency", "getDeclineReason", "getDescription", "getId", "getIncludeInSpending", "()Z", "isPeerToPeer", "isTopup", "getLocalAmount", "getLocalCurrency", "getMerchant", "()Lco/uk/getmondo/api/model/feed/ApiMerchant;", "getMetadata", "()Lco/uk/getmondo/api/model/feed/ApiMetadata;", "getScheme", "getSettled", "getUpdated", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "fromAtm", "hasDeclineReason", "hasMerchant", "hasMetadata", "hashCode", "", "toString", "withCategory", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiTransaction {
   private final long amount;
   private final List attachments;
   private final String category;
   private final ApiCounterparty counterparty;
   private final Date created;
   private final String currency;
   private final String declineReason;
   private final String description;
   private final String id;
   private final boolean includeInSpending;
   private final boolean isLoad;
   private final long localAmount;
   private final String localCurrency;
   private final ApiMerchant merchant;
   private final ApiMetadata metadata;
   private final String scheme;
   private final String settled;
   private final Date updated;

   public ApiTransaction(String var, ApiMerchant var, long var, long var, Date var, Date var, String var, String var, String var, String var, ApiCounterparty var, boolean var, String var, ApiMetadata var, String var, List var, boolean var, String var) {
      l.b(var, "id");
      l.b(var, "created");
      l.b(var, "updated");
      l.b(var, "currency");
      l.b(var, "localCurrency");
      l.b(var, "description");
      l.b(var, "settled");
      l.b(var, "category");
      super();
      this.id = var;
      this.merchant = var;
      this.amount = var;
      this.localAmount = var;
      this.created = var;
      this.updated = var;
      this.currency = var;
      this.localCurrency = var;
      this.description = var;
      this.declineReason = var;
      this.counterparty = var;
      this.isLoad = var;
      this.settled = var;
      this.metadata = var;
      this.category = var;
      this.attachments = var;
      this.includeInSpending = var;
      this.scheme = var;
   }

   // $FF: synthetic method
   public ApiTransaction(String var, ApiMerchant var, long var, long var, Date var, Date var, String var, String var, String var, String var, ApiCounterparty var, boolean var, String var, ApiMetadata var, String var, List var, boolean var, String var, int var, i var) {
      if((131072 & var) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public final ApiTransaction a(String var) {
      l.b(var, "category");
      return a(this, (String)null, (ApiMerchant)null, 0L, 0L, (Date)null, (Date)null, (String)null, (String)null, (String)null, (String)null, (ApiCounterparty)null, false, (String)null, (ApiMetadata)null, var, (List)null, false, (String)null, 245759, (Object)null);
   }

   public final ApiTransaction a(String var, ApiMerchant var, long var, long var, Date var, Date var, String var, String var, String var, String var, ApiCounterparty var, boolean var, String var, ApiMetadata var, String var, List var, boolean var, String var) {
      l.b(var, "id");
      l.b(var, "created");
      l.b(var, "updated");
      l.b(var, "currency");
      l.b(var, "localCurrency");
      l.b(var, "description");
      l.b(var, "settled");
      l.b(var, "category");
      return new ApiTransaction(var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public final boolean a() {
      ApiMerchant var = this.merchant;
      boolean var;
      if(var != null) {
         var = var.f();
      } else {
         var = false;
      }

      return var;
   }

   public final boolean b() {
      boolean var;
      if(this.merchant != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final boolean c() {
      boolean var;
      if(!this.d() && this.isLoad) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final boolean d() {
      Object var = null;
      ApiMetadata var = this.metadata;
      String var;
      if(var != null) {
         var = var.e();
      } else {
         var = null;
      }

      boolean var;
      label23: {
         if(var == null) {
            ApiMetadata var = this.metadata;
            var = (String)var;
            if(var != null) {
               var = var.d();
            }

            if(var == null) {
               break label23;
            }
         }

         if(this.counterparty != null) {
            var = true;
            return var;
         }
      }

      var = false;
      return var;
   }

   public final boolean e() {
      boolean var;
      if(this.metadata != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiTransaction)) {
            return var;
         }

         ApiTransaction var = (ApiTransaction)var;
         var = var;
         if(!l.a(this.id, var.id)) {
            return var;
         }

         var = var;
         if(!l.a(this.merchant, var.merchant)) {
            return var;
         }

         boolean var;
         if(this.amount == var.amount) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.localAmount == var.localAmount) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.created, var.created)) {
            return var;
         }

         var = var;
         if(!l.a(this.updated, var.updated)) {
            return var;
         }

         var = var;
         if(!l.a(this.currency, var.currency)) {
            return var;
         }

         var = var;
         if(!l.a(this.localCurrency, var.localCurrency)) {
            return var;
         }

         var = var;
         if(!l.a(this.description, var.description)) {
            return var;
         }

         var = var;
         if(!l.a(this.declineReason, var.declineReason)) {
            return var;
         }

         var = var;
         if(!l.a(this.counterparty, var.counterparty)) {
            return var;
         }

         if(this.isLoad == var.isLoad) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.settled, var.settled)) {
            return var;
         }

         var = var;
         if(!l.a(this.metadata, var.metadata)) {
            return var;
         }

         var = var;
         if(!l.a(this.category, var.category)) {
            return var;
         }

         var = var;
         if(!l.a(this.attachments, var.attachments)) {
            return var;
         }

         if(this.includeInSpending == var.includeInSpending) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.scheme, var.scheme)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final boolean f() {
      boolean var;
      if(this.declineReason != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final String g() {
      return this.id;
   }

   public final ApiMerchant h() {
      return this.merchant;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final long i() {
      return this.amount;
   }

   public final long j() {
      return this.localAmount;
   }

   public final Date k() {
      return this.created;
   }

   public final Date l() {
      return this.updated;
   }

   public final String m() {
      return this.currency;
   }

   public final String n() {
      return this.localCurrency;
   }

   public final String o() {
      return this.description;
   }

   public final String p() {
      return this.declineReason;
   }

   public final ApiCounterparty q() {
      return this.counterparty;
   }

   public final String r() {
      return this.settled;
   }

   public final ApiMetadata s() {
      return this.metadata;
   }

   public final String t() {
      return this.category;
   }

   public String toString() {
      return "ApiTransaction(id=" + this.id + ", merchant=" + this.merchant + ", amount=" + this.amount + ", localAmount=" + this.localAmount + ", created=" + this.created + ", updated=" + this.updated + ", currency=" + this.currency + ", localCurrency=" + this.localCurrency + ", description=" + this.description + ", declineReason=" + this.declineReason + ", counterparty=" + this.counterparty + ", isLoad=" + this.isLoad + ", settled=" + this.settled + ", metadata=" + this.metadata + ", category=" + this.category + ", attachments=" + this.attachments + ", includeInSpending=" + this.includeInSpending + ", scheme=" + this.scheme + ")";
   }

   public final List u() {
      return this.attachments;
   }

   public final boolean v() {
      return this.includeInSpending;
   }

   public final String w() {
      return this.scheme;
   }
}
