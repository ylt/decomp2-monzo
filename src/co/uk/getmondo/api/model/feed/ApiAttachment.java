package co.uk.getmondo.api.model.feed;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiAttachment;", "", "id", "", "created", "Ljava/util/Date;", "externalId", "fileUrl", "(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)V", "getCreated", "()Ljava/util/Date;", "getExternalId", "()Ljava/lang/String;", "getFileUrl", "getId", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAttachment {
   private final Date created;
   private final String externalId;
   private final String fileUrl;
   private final String id;

   public ApiAttachment(String var, Date var, String var, String var) {
      l.b(var, "id");
      l.b(var, "created");
      l.b(var, "externalId");
      l.b(var, "fileUrl");
      super();
      this.id = var;
      this.created = var;
      this.externalId = var;
      this.fileUrl = var;
   }

   public final String a() {
      return this.id;
   }

   public final Date b() {
      return this.created;
   }

   public final String c() {
      return this.externalId;
   }

   public final String d() {
      return this.fileUrl;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof ApiAttachment) {
               ApiAttachment var = (ApiAttachment)var;
               if(l.a(this.id, var.id) && l.a(this.created, var.created) && l.a(this.externalId, var.externalId) && l.a(this.fileUrl, var.fileUrl)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      Date var = this.created;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.externalId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.fileUrl;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ApiAttachment(id=" + this.id + ", created=" + this.created + ", externalId=" + this.externalId + ", fileUrl=" + this.fileUrl + ")";
   }
}
