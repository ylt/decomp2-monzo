package co.uk.getmondo.api.model.feed;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001-BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\bHÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\nHÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\fHÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u000fHÆ\u0003Ji\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u000fHÆ\u0001J\u0013\u0010(\u001a\u00020\u000f2\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010*\u001a\u00020+HÖ\u0001J\t\u0010,\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0018R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0012¨\u0006."},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiFeedItem;", "", "id", "", "accountId", "externalId", "type", "created", "Ljava/util/Date;", "transaction", "Lco/uk/getmondo/api/model/feed/ApiTransaction;", "params", "Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "appUri", "isDismissable", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lco/uk/getmondo/api/model/feed/ApiTransaction;Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;Ljava/lang/String;Z)V", "getAccountId", "()Ljava/lang/String;", "getAppUri", "getCreated", "()Ljava/util/Date;", "getExternalId", "getId", "()Z", "getParams", "()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "getTransaction", "()Lco/uk/getmondo/api/model/feed/ApiTransaction;", "getType", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "Params", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiFeedItem {
   private final String accountId;
   private final String appUri;
   private final Date created;
   private final String externalId;
   private final String id;
   private final boolean isDismissable;
   private final ApiFeedItem.Params params;
   private final ApiTransaction transaction;
   private final String type;

   public ApiFeedItem(String var, String var, String var, String var, Date var, ApiTransaction var, ApiFeedItem.Params var, String var, boolean var) {
      l.b(var, "id");
      l.b(var, "accountId");
      l.b(var, "externalId");
      l.b(var, "type");
      l.b(var, "created");
      super();
      this.id = var;
      this.accountId = var;
      this.externalId = var;
      this.type = var;
      this.created = var;
      this.transaction = var;
      this.params = var;
      this.appUri = var;
      this.isDismissable = var;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final String c() {
      return this.externalId;
   }

   public final String d() {
      return this.type;
   }

   public final Date e() {
      return this.created;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiFeedItem)) {
            return var;
         }

         ApiFeedItem var = (ApiFeedItem)var;
         var = var;
         if(!l.a(this.id, var.id)) {
            return var;
         }

         var = var;
         if(!l.a(this.accountId, var.accountId)) {
            return var;
         }

         var = var;
         if(!l.a(this.externalId, var.externalId)) {
            return var;
         }

         var = var;
         if(!l.a(this.type, var.type)) {
            return var;
         }

         var = var;
         if(!l.a(this.created, var.created)) {
            return var;
         }

         var = var;
         if(!l.a(this.transaction, var.transaction)) {
            return var;
         }

         var = var;
         if(!l.a(this.params, var.params)) {
            return var;
         }

         var = var;
         if(!l.a(this.appUri, var.appUri)) {
            return var;
         }

         boolean var;
         if(this.isDismissable == var.isDismissable) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final ApiTransaction f() {
      return this.transaction;
   }

   public final ApiFeedItem.Params g() {
      return this.params;
   }

   public final String h() {
      return this.appUri;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final boolean i() {
      return this.isDismissable;
   }

   public String toString() {
      return "ApiFeedItem(id=" + this.id + ", accountId=" + this.accountId + ", externalId=" + this.externalId + ", type=" + this.type + ", created=" + this.created + ", transaction=" + this.transaction + ", params=" + this.params + ", appUri=" + this.appUri + ", isDismissable=" + this.isDismissable + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B}\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0011J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010'\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010\u0013J\u000b\u0010(\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u009e\u0001\u0010.\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010/J\u0013\u00100\u001a\u0002012\b\u00102\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00103\u001a\u000204HÖ\u0001J\t\u00105\u001a\u00020\u0003HÖ\u0001R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0013\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001aR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0016R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0016R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0016¨\u00066"},
      d2 = {"Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "", "ticketId", "", "currency", "amount", "", "intervalStart", "Lorg/threeten/bp/LocalDateTime;", "intervalEnd", "intervalType", "kycRejectedReason", "kycRejectedCustomerMessage", "sddMigrationRejectionNote", "title", "body", "imageUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAmount", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getBody", "()Ljava/lang/String;", "getCurrency", "getImageUrl", "getIntervalEnd", "()Lorg/threeten/bp/LocalDateTime;", "getIntervalStart", "getIntervalType", "getKycRejectedCustomerMessage", "getKycRejectedReason", "getSddMigrationRejectionNote", "getTicketId", "getTitle", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Params {
      private final Long amount;
      private final String body;
      private final String currency;
      private final String imageUrl;
      private final LocalDateTime intervalEnd;
      private final LocalDateTime intervalStart;
      private final String intervalType;
      private final String kycRejectedCustomerMessage;
      private final String kycRejectedReason;
      private final String sddMigrationRejectionNote;
      private final String ticketId;
      private final String title;

      public Params(String var, String var, Long var, LocalDateTime var, LocalDateTime var, String var, String var, String var, String var, String var, String var, String var) {
         this.ticketId = var;
         this.currency = var;
         this.amount = var;
         this.intervalStart = var;
         this.intervalEnd = var;
         this.intervalType = var;
         this.kycRejectedReason = var;
         this.kycRejectedCustomerMessage = var;
         this.sddMigrationRejectionNote = var;
         this.title = var;
         this.body = var;
         this.imageUrl = var;
      }

      public final String a() {
         return this.ticketId;
      }

      public final String b() {
         return this.currency;
      }

      public final Long c() {
         return this.amount;
      }

      public final LocalDateTime d() {
         return this.intervalStart;
      }

      public final LocalDateTime e() {
         return this.intervalEnd;
      }

      public boolean equals(Object var) {
         boolean var;
         label57: {
            if(this != var) {
               if(!(var instanceof ApiFeedItem.Params)) {
                  break label57;
               }

               ApiFeedItem.Params var = (ApiFeedItem.Params)var;
               if(!l.a(this.ticketId, var.ticketId) || !l.a(this.currency, var.currency) || !l.a(this.amount, var.amount) || !l.a(this.intervalStart, var.intervalStart) || !l.a(this.intervalEnd, var.intervalEnd) || !l.a(this.intervalType, var.intervalType) || !l.a(this.kycRejectedReason, var.kycRejectedReason) || !l.a(this.kycRejectedCustomerMessage, var.kycRejectedCustomerMessage) || !l.a(this.sddMigrationRejectionNote, var.sddMigrationRejectionNote) || !l.a(this.title, var.title) || !l.a(this.body, var.body) || !l.a(this.imageUrl, var.imageUrl)) {
                  break label57;
               }
            }

            var = true;
            return var;
         }

         var = false;
         return var;
      }

      public final String f() {
         return this.intervalType;
      }

      public final String g() {
         return this.kycRejectedReason;
      }

      public final String h() {
         return this.kycRejectedCustomerMessage;
      }

      public int hashCode() {
         int var = 0;
         String var = this.ticketId;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.currency;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         Long var = this.amount;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         LocalDateTime var = this.intervalStart;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.intervalEnd;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.intervalType;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.kycRejectedReason;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.kycRejectedCustomerMessage;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.sddMigrationRejectionNote;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.title;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.body;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.imageUrl;
         if(var != null) {
            var = var.hashCode();
         }

         return (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
      }

      public final String i() {
         return this.sddMigrationRejectionNote;
      }

      public final String j() {
         return this.title;
      }

      public final String k() {
         return this.body;
      }

      public final String l() {
         return this.imageUrl;
      }

      public String toString() {
         return "Params(ticketId=" + this.ticketId + ", currency=" + this.currency + ", amount=" + this.amount + ", intervalStart=" + this.intervalStart + ", intervalEnd=" + this.intervalEnd + ", intervalType=" + this.intervalType + ", kycRejectedReason=" + this.kycRejectedReason + ", kycRejectedCustomerMessage=" + this.kycRejectedCustomerMessage + ", sddMigrationRejectionNote=" + this.sddMigrationRejectionNote + ", title=" + this.title + ", body=" + this.body + ", imageUrl=" + this.imageUrl + ")";
      }
   }
}
