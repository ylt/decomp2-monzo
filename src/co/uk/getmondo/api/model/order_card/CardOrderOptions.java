package co.uk.getmondo.api.model.order_card;

import co.uk.getmondo.api.model.ApiAddress;
import com.squareup.moshi.h;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0006HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/api/model/order_card/CardOrderOptions;", "", "names", "", "Lco/uk/getmondo/api/model/order_card/CardOrderName;", "deliveryAddress", "Lco/uk/getmondo/api/model/ApiAddress;", "(Ljava/util/List;Lco/uk/getmondo/api/model/ApiAddress;)V", "getDeliveryAddress", "()Lco/uk/getmondo/api/model/ApiAddress;", "getNames", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CardOrderOptions {
   private final ApiAddress deliveryAddress;
   private final List names;

   public CardOrderOptions(List var, @h(a = "delivery_address") ApiAddress var) {
      l.b(var, "names");
      l.b(var, "deliveryAddress");
      super();
      this.names = var;
      this.deliveryAddress = var;
   }

   public final List a() {
      return this.names;
   }

   public final ApiAddress b() {
      return this.deliveryAddress;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof CardOrderOptions) {
               CardOrderOptions var = (CardOrderOptions)var;
               if(l.a(this.names, var.names) && l.a(this.deliveryAddress, var.deliveryAddress)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      List var = this.names;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ApiAddress var = this.deliveryAddress;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "CardOrderOptions(names=" + this.names + ", deliveryAddress=" + this.deliveryAddress + ")";
   }
}
