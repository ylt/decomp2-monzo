package co.uk.getmondo.api.model.order_card;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0014B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/api/model/order_card/CardOrderName;", "", "type", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "name", "", "(Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;Ljava/lang/String;)V", "getName", "()Ljava/lang/String;", "getType", "()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Type", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CardOrderName {
   private final String name;
   private final CardOrderName.Type type;

   public CardOrderName(CardOrderName.Type var, String var) {
      l.b(var, "type");
      l.b(var, "name");
      super();
      this.type = var;
      this.name = var;
   }

   public final CardOrderName.Type a() {
      return this.type;
   }

   public final String b() {
      return this.name;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof CardOrderName) {
               CardOrderName var = (CardOrderName)var;
               if(l.a(this.type, var.type) && l.a(this.name, var.name)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      CardOrderName.Type var = this.type;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.name;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "CardOrderName(type=" + this.type + ", name=" + this.name + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "PREFERRED", "LEGAL", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Type {
      @h(
         a = "legal"
      )
      LEGAL,
      @h(
         a = "preferred"
      )
      PREFERRED;

      private final String apiValue;

      static {
         CardOrderName.Type var = new CardOrderName.Type("PREFERRED", 0, "preferred");
         PREFERRED = var;
         CardOrderName.Type var = new CardOrderName.Type("LEGAL", 1, "legal");
         LEGAL = var;
      }

      protected Type(String var) {
         l.b(var, "apiValue");
         super(var, var);
         this.apiValue = var;
      }

      public final String a() {
         return this.apiValue;
      }
   }
}
