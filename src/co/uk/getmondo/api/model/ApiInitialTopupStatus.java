package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0007HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/api/model/ApiInitialTopupStatus;", "", "topupProcessed", "", "topupAmount", "", "topupCurrency", "", "(ZJLjava/lang/String;)V", "getTopupAmount", "()J", "getTopupCurrency", "()Ljava/lang/String;", "getTopupProcessed", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiInitialTopupStatus {
   private final long topupAmount;
   private final String topupCurrency;
   private final boolean topupProcessed;

   public ApiInitialTopupStatus(boolean var, long var, String var) {
      l.b(var, "topupCurrency");
      super();
      this.topupProcessed = var;
      this.topupAmount = var;
      this.topupCurrency = var;
   }

   // $FF: synthetic method
   public ApiInitialTopupStatus(boolean var, long var, String var, int var, i var) {
      if((var & 2) != 0) {
         var = 0L;
      }

      if((var & 4) != 0) {
         var = co.uk.getmondo.common.i.c.a.b();
         l.a(var, "Currency.GBP.currencyCode");
      }

      this(var, var, var);
   }

   public final boolean a() {
      return this.topupProcessed;
   }

   public final long b() {
      return this.topupAmount;
   }

   public final String c() {
      return this.topupCurrency;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ApiInitialTopupStatus)) {
            return var;
         }

         ApiInitialTopupStatus var = (ApiInitialTopupStatus)var;
         boolean var;
         if(this.topupProcessed == var.topupProcessed) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         if(this.topupAmount == var.topupAmount) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.topupCurrency, var.topupCurrency)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "ApiInitialTopupStatus(topupProcessed=" + this.topupProcessed + ", topupAmount=" + this.topupAmount + ", topupCurrency=" + this.topupCurrency + ")";
   }
}
