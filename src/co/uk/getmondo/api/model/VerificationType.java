package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\n\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/api/model/VerificationType;", "", "dueDiligenceLevel", "", "descriptionResId", "", "(Ljava/lang/String;ILjava/lang/String;I)V", "getDescriptionResId", "()I", "getDueDiligenceLevel", "()Ljava/lang/String;", "STANDARD", "FULL", "EXTENDED", "BLOCKED", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum VerificationType {
   @com.google.gson.a.c(
      a = "BLOCKED"
   )
   BLOCKED,
   @com.google.gson.a.c(
      a = "EDD"
   )
   EXTENDED,
   @com.google.gson.a.c(
      a = "FDD"
   )
   FULL,
   @com.google.gson.a.c(
      a = "SDD"
   )
   STANDARD;

   private final int descriptionResId;
   private final String dueDiligenceLevel;

   static {
      VerificationType var = new VerificationType("STANDARD", 0, "sdd", 2131362393);
      STANDARD = var;
      VerificationType var = new VerificationType("FULL", 1, "fdd", 2131362392);
      FULL = var;
      VerificationType var = new VerificationType("EXTENDED", 2, "edd", 2131362391);
      EXTENDED = var;
      VerificationType var = new VerificationType("BLOCKED", 3, "blocked", 2131362393);
      BLOCKED = var;
   }

   protected VerificationType(String var, int var) {
      l.b(var, "dueDiligenceLevel");
      super(var, var);
      this.dueDiligenceLevel = var;
      this.descriptionResId = var;
   }

   public final String a() {
      return this.dueDiligenceLevel;
   }

   public final int b() {
      return this.descriptionResId;
   }
}
