package co.uk.getmondo.api.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/api/model/AddressesReponse;", "", "addresses", "", "Lco/uk/getmondo/api/model/ApiAddress;", "(Ljava/util/List;)V", "getAddresses", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressesReponse {
   private final List addresses;

   public AddressesReponse(List var) {
      l.b(var, "addresses");
      super();
      this.addresses = var;
   }

   public final List a() {
      return this.addresses;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label26: {
            if(var instanceof AddressesReponse) {
               AddressesReponse var = (AddressesReponse)var;
               if(l.a(this.addresses, var.addresses)) {
                  break label26;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      List var = this.addresses;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var;
   }

   public String toString() {
      return "AddressesReponse(addresses=" + this.addresses + ")";
   }
}
