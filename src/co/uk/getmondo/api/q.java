package co.uk.getmondo.api;

public final class q implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var;
      if(!q.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public q(c var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(c var) {
      return new q(var);
   }

   public com.squareup.moshi.v a() {
      return (com.squareup.moshi.v)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
