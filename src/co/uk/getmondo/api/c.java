package co.uk.getmondo.api;

import android.content.Context;
import android.net.Uri;
import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import okhttp3.Authenticator;
import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZonedDateTime;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Ð\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001:\u0003[\\]B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002JA\u0010\u000b\u001a\u00020\b2\b\b\u0001\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015H\u0001¢\u0006\u0002\b\u0016J\r\u0010\u0017\u001a\u00020\bH\u0001¢\u0006\u0002\b\u0018J/\u0010\u0019\u001a\u00020\b2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b!J/\u0010\"\u001a\u00020\b2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b#J)\u0010$\u001a\u00020\u00042\b\b\u0001\u0010%\u001a\u00020\b2\b\b\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010&\u001a\u00020'H\u0001¢\u0006\u0002\b(J\r\u0010)\u001a\u00020'H\u0001¢\u0006\u0002\b*J)\u0010+\u001a\u00020,2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b0J\r\u00101\u001a\u00020\u0010H\u0001¢\u0006\u0002\b2J\u0015\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\b6J)\u00107\u001a\u0002082\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b9JA\u0010:\u001a\u00020;2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010&\u001a\u00020'2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b<J\u0015\u0010=\u001a\u00020>2\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\b?J\u0015\u0010@\u001a\u00020A2\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\bBJ\r\u0010C\u001a\u00020/H\u0001¢\u0006\u0002\bDJ1\u0010E\u001a\u00020F2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010&\u001a\u00020'2\u0006\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bGJ)\u0010H\u001a\u00020I2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bJJ)\u0010K\u001a\u00020L2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bMJ)\u0010N\u001a\u00020O2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bPJ/\u0010Q\u001a\u00020R2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010&\u001a\u00020'2\b\b\u0001\u0010S\u001a\u00020\u0006H\u0001¢\u0006\u0002\bTJ)\u0010U\u001a\u00020V2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bWJ)\u0010X\u001a\u00020Y2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bZ¨\u0006^"},
   d2 = {"Lco/uk/getmondo/api/ApiModule;", "", "()V", "buildRetrofit", "Lretrofit2/Retrofit;", "baseUrl", "", "client", "Lokhttp3/OkHttpClient;", "converterFactory", "Lretrofit2/Converter$Factory;", "provideBaseMonzoOkHttpClient", "context", "Landroid/content/Context;", "baseOkHttpClient", "loggingInterceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "additionalHeadersInterceptor", "Lco/uk/getmondo/api/AdditionalHeadersInterceptor;", "apiUrl", "preferences", "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;", "provideBaseMonzoOkHttpClient$app_monzoPrepaidRelease", "provideBaseOkHttpClient", "provideBaseOkHttpClient$app_monzoPrepaidRelease", "provideConfiguredForSignUpOkHttpClient", "baseMonzoHttpClient", "bearerAuthInterceptor", "Lco/uk/getmondo/api/authentication/BearerAuthInterceptor;", "errorHandlerInterceptor", "Lco/uk/getmondo/api/ErrorHandlerInterceptor;", "monzoAuthenticator", "Lco/uk/getmondo/api/authentication/MonzoAuthenticator;", "provideConfiguredForSignUpOkHttpClient$app_monzoPrepaidRelease", "provideConfiguredOkHttpClient", "provideConfiguredOkHttpClient$app_monzoPrepaidRelease", "provideDefaultMonzoRetrofit", "configuredMonzoHttpClient", "gson", "Lcom/google/gson/Gson;", "provideDefaultMonzoRetrofit$app_monzoPrepaidRelease", "provideGson", "provideGson$app_monzoPrepaidRelease", "provideHelpApi", "Lco/uk/getmondo/api/HelpApi;", "okHttpClient", "moshi", "Lcom/squareup/moshi/Moshi;", "provideHelpApi$app_monzoPrepaidRelease", "provideHttpLoggingInterceptor", "provideHttpLoggingInterceptor$app_monzoPrepaidRelease", "provideIdentityVerificationApi", "Lco/uk/getmondo/api/IdentityVerificationApi;", "retrofit", "provideIdentityVerificationApi$app_monzoPrepaidRelease", "provideMigrationApi", "Lco/uk/getmondo/api/MigrationApi;", "provideMigrationApi$app_monzoPrepaidRelease", "provideMonzoAnalyticsApi", "Lco/uk/getmondo/api/AnalyticsApi;", "provideMonzoAnalyticsApi$app_monzoPrepaidRelease", "provideMonzoApi", "Lco/uk/getmondo/api/MonzoApi;", "provideMonzoApi$app_monzoPrepaidRelease", "provideMonzoPaymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "provideMonzoPaymentsApi$app_monzoPrepaidRelease", "provideMoshi", "provideMoshi$app_monzoPrepaidRelease", "provideOAuthMonzoApi", "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;", "provideOAuthMonzoApi$app_monzoPrepaidRelease", "provideOverdraftApi", "Lco/uk/getmondo/api/OverdraftApi;", "provideOverdraftApi$app_monzoPrepaidRelease", "providePaymentLimitsApi", "Lco/uk/getmondo/api/PaymentLimitsApi;", "providePaymentLimitsApi$app_monzoPrepaidRelease", "provideProfileApi", "Lco/uk/getmondo/profile/data/MonzoProfileApi;", "provideProfileApi$app_monzoPrepaidRelease", "provideServiceStatusApi", "Lco/uk/getmondo/api/ServiceStatusApi;", "statusApiUrl", "provideServiceStatusApi$app_monzoPrepaidRelease", "provideSignUpApi", "Lco/uk/getmondo/api/SignupApi;", "provideSignUpApi$app_monzoPrepaidRelease", "provideTaxResidencyApi", "Lco/uk/getmondo/api/TaxResidencyApi;", "provideTaxResidencyApi$app_monzoPrepaidRelease", "ConfiguredForSignUpMonzoHttpClient", "ConfiguredMonzoHttpClient", "MonzoHttpClient", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final Retrofit a(String var, OkHttpClient var, Converter.Factory var) {
      Retrofit var = (new Retrofit.Builder()).baseUrl(var).client(var).addConverterFactory(var).addCallAdapterFactory((CallAdapter.Factory)RxJava2CallAdapterFactory.create()).build();
      kotlin.d.b.l.a(var, "Retrofit.Builder()\n     …\n                .build()");
      return var;
   }

   public final AnalyticsApi a(OkHttpClient var, com.google.gson.f var, co.uk.getmondo.api.authentication.b var, ac var, co.uk.getmondo.api.authentication.k var, String var) {
      kotlin.d.b.l.b(var, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var, "gson");
      kotlin.d.b.l.b(var, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var, "monzoAuthenticator");
      kotlin.d.b.l.b(var, "apiUrl");
      var.a(false);
      var = var.newBuilder().authenticator((Authenticator)var).addInterceptor((Interceptor)var).addInterceptor((Interceptor)var).build();
      kotlin.d.b.l.a(var, "okHttpClient");
      GsonConverterFactory var = GsonConverterFactory.create(var);
      kotlin.d.b.l.a(var, "GsonConverterFactory.create(gson)");
      Object var = this.a(var, var, (Converter.Factory)var).create(AnalyticsApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…AnalyticsApi::class.java)");
      return (AnalyticsApi)var;
   }

   public final MonzoApi a(Retrofit var) {
      kotlin.d.b.l.b(var, "retrofit");
      Object var = var.create(MonzoApi.class);
      kotlin.d.b.l.a(var, "retrofit.create(MonzoApi::class.java)");
      return (MonzoApi)var;
   }

   public final ServiceStatusApi a(OkHttpClient var, HttpLoggingInterceptor var, com.google.gson.f var, String var) {
      kotlin.d.b.l.b(var, "baseOkHttpClient");
      kotlin.d.b.l.b(var, "loggingInterceptor");
      kotlin.d.b.l.b(var, "gson");
      kotlin.d.b.l.b(var, "statusApiUrl");
      var = var.newBuilder().addInterceptor((Interceptor)var).build();
      Object var = (new Retrofit.Builder()).baseUrl(var).client(var).addConverterFactory((Converter.Factory)GsonConverterFactory.create(var)).addCallAdapterFactory((CallAdapter.Factory)RxJava2CallAdapterFactory.create()).build().create(ServiceStatusApi.class);
      kotlin.d.b.l.a(var, "Retrofit.Builder()\n     …iceStatusApi::class.java)");
      return (ServiceStatusApi)var;
   }

   public final TaxResidencyApi a(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(TaxResidencyApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…ResidencyApi::class.java)");
      return (TaxResidencyApi)var;
   }

   public final MonzoOAuthApi a(OkHttpClient var, com.google.gson.f var, ac var, String var) {
      kotlin.d.b.l.b(var, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var, "gson");
      kotlin.d.b.l.b(var, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var, "apiUrl");
      var.a(false);
      var = var.newBuilder().addInterceptor((Interceptor)var).build();
      kotlin.d.b.l.a(var, "okHttpClient");
      GsonConverterFactory var = GsonConverterFactory.create(var);
      kotlin.d.b.l.a(var, "GsonConverterFactory.create(gson)");
      Object var = this.a(var, var, (Converter.Factory)var).create(MonzoOAuthApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…onzoOAuthApi::class.java)");
      return (MonzoOAuthApi)var;
   }

   public final com.squareup.moshi.v a() {
      com.squareup.moshi.v var = (new com.squareup.moshi.v.a()).a((com.squareup.moshi.i.a)(new com.squareup.moshi.r())).a((Object)(new co.uk.getmondo.signup_old.ac())).a((Object)(new co.uk.getmondo.common.c.b())).a();
      kotlin.d.b.l.a(var, "Moshi.Builder()\n        …\n                .build()");
      return var;
   }

   public final OkHttpClient a(Context var, OkHttpClient var, HttpLoggingInterceptor var, a var, String var, co.uk.getmondo.developer_options.a var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "baseOkHttpClient");
      kotlin.d.b.l.b(var, "loggingInterceptor");
      kotlin.d.b.l.b(var, "additionalHeadersInterceptor");
      kotlin.d.b.l.b(var, "apiUrl");
      kotlin.d.b.l.b(var, "preferences");
      var = Uri.parse(var).getHost();
      CertificatePinner.Builder var = new CertificatePinner.Builder();
      String[] var = co.uk.getmondo.a.a;

      for(int var = 0; var < var.length; ++var) {
         var.add(var, new String[]{var[var]});
      }

      OkHttpClient.Builder var = var.newBuilder().addInterceptor((Interceptor)var).addInterceptor((Interceptor)var);
      var.certificatePinner(var.build());
      OkHttpClient var = var.build();
      kotlin.d.b.l.a(var, "builder.build()");
      return var;
   }

   public final OkHttpClient a(OkHttpClient var, co.uk.getmondo.api.authentication.b var, ac var, co.uk.getmondo.api.authentication.k var) {
      kotlin.d.b.l.b(var, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var, "monzoAuthenticator");
      var.a(true);
      var = var.newBuilder().authenticator((Authenticator)var).addInterceptor((Interceptor)var).addInterceptor((Interceptor)var).build();
      kotlin.d.b.l.a(var, "baseMonzoHttpClient.newB…\n                .build()");
      return var;
   }

   public final Retrofit a(OkHttpClient var, String var, com.google.gson.f var) {
      kotlin.d.b.l.b(var, "configuredMonzoHttpClient");
      kotlin.d.b.l.b(var, "apiUrl");
      kotlin.d.b.l.b(var, "gson");
      var = var.newBuilder().readTimeout(120L, TimeUnit.SECONDS).build();
      kotlin.d.b.l.a(var, "okHttpClient");
      GsonConverterFactory var = GsonConverterFactory.create(var);
      kotlin.d.b.l.a(var, "GsonConverterFactory.create(gson)");
      return this.a(var, var, (Converter.Factory)var);
   }

   public final IdentityVerificationApi b(Retrofit var) {
      kotlin.d.b.l.b(var, "retrofit");
      Object var = var.create(IdentityVerificationApi.class);
      kotlin.d.b.l.a(var, "retrofit.create(Identity…ificationApi::class.java)");
      return (IdentityVerificationApi)var;
   }

   public final MonzoProfileApi b(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(MonzoProfileApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…zoProfileApi::class.java)");
      return (MonzoProfileApi)var;
   }

   public final com.google.gson.f b() {
      com.google.gson.f var = (new com.google.gson.g()).a(com.google.gson.d.d).a((com.google.gson.t)(new co.uk.getmondo.common.g.a())).a((Type)LocalDateTime.class, null.a).a((Type)LocalDate.class, null.a).a((Type)ZonedDateTime.class, null.a).a("yyyy-MM-dd'T'HH:mm:ss.SSSZ").a();
      kotlin.d.b.l.a(var, "GsonBuilder()\n          …                .create()");
      return var;
   }

   public final OkHttpClient b(OkHttpClient var, co.uk.getmondo.api.authentication.b var, ac var, co.uk.getmondo.api.authentication.k var) {
      kotlin.d.b.l.b(var, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var, "monzoAuthenticator");
      var.a(true);
      OkHttpClient.Builder var = var.newBuilder().authenticator((Authenticator)var).addInterceptor((Interceptor)var).addInterceptor((Interceptor)var);
      var.interceptors().add(0, new ai(SignupSource.PERSONAL_ACCOUNT));
      var = var.build();
      kotlin.d.b.l.a(var, "builder.build()");
      return var;
   }

   public final HelpApi c(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(HelpApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…eate(HelpApi::class.java)");
      return (HelpApi)var;
   }

   public final PaymentsApi c(Retrofit var) {
      kotlin.d.b.l.b(var, "retrofit");
      Object var = var.create(PaymentsApi.class);
      kotlin.d.b.l.a(var, "retrofit.create(PaymentsApi::class.java)");
      return (PaymentsApi)var;
   }

   public final OkHttpClient c() {
      OkHttpClient var = (new OkHttpClient.Builder()).build();
      kotlin.d.b.l.a(var, "OkHttpClient.Builder().build()");
      return var;
   }

   public final SignupApi d(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(SignupApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…te(SignupApi::class.java)");
      return (SignupApi)var;
   }

   public final HttpLoggingInterceptor d() {
      HttpLoggingInterceptor var = new HttpLoggingInterceptor();
      var.setLevel(HttpLoggingInterceptor.Level.NONE);
      return var;
   }

   public final MigrationApi e(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(MigrationApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…MigrationApi::class.java)");
      return (MigrationApi)var;
   }

   public final OverdraftApi f(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(OverdraftApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…OverdraftApi::class.java)");
      return (OverdraftApi)var;
   }

   public final PaymentLimitsApi g(OkHttpClient var, com.squareup.moshi.v var, String var) {
      kotlin.d.b.l.b(var, "okHttpClient");
      kotlin.d.b.l.b(var, "moshi");
      kotlin.d.b.l.b(var, "apiUrl");
      MoshiConverterFactory var = MoshiConverterFactory.create(var);
      kotlin.d.b.l.a(var, "MoshiConverterFactory.create(moshi)");
      Object var = this.a(var, var, (Converter.Factory)var).create(PaymentLimitsApi.class);
      kotlin.d.b.l.a(var, "buildRetrofit(apiUrl, ok…entLimitsApi::class.java)");
      return (PaymentLimitsApi)var;
   }
}
