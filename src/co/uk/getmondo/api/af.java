package co.uk.getmondo.api;

import java.io.InputStream;

// $FF: synthetic class
final class af implements io.reactivex.c.a {
   private final ae a;
   private final InputStream b;
   private final long c;
   private final String d;
   private final String e;

   private af(ae var, InputStream var, long var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public static io.reactivex.c.a a(ae var, InputStream var, long var, String var, String var) {
      return new af(var, var, var, var, var);
   }

   public void a() {
      ae.a(this.a, this.b, this.c, this.d, this.e);
   }
}
