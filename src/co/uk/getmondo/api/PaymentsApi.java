package co.uk.getmondo.api;

import kotlin.Metadata;
import org.threeten.bp.LocalDate;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H'J:\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u00062\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u0006H'J:\u0010\r\u001a\u00020\b2\b\b\u0001\u0010\u000e\u001a\u00020\u00062\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u0006H'J\u0092\u0001\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\b\b\u0001\u0010\u0014\u001a\u00020\u00062\b\b\u0001\u0010\u0015\u001a\u00020\u00062\b\b\u0001\u0010\u0016\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\u0017\u001a\u00020\u00062\b\b\u0001\u0010\u0018\u001a\u00020\u00192\b\b\u0001\u0010\u001a\u001a\u00020\u00062\n\b\u0001\u0010\u001b\u001a\u0004\u0018\u00010\u0019H'J\u0018\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H'Jn\u0010\u001e\u001a\u00020\b2\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\b\b\u0001\u0010\u0014\u001a\u00020\u00062\b\b\u0001\u0010\u0015\u001a\u00020\u00062\b\b\u0001\u0010\u0016\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\n\b\u0001\u0010\u0017\u001a\u0004\u0018\u00010\u0006H'Jb\u0010\u001f\u001a\u00020\b2\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010 \u001a\u00020\u00062\b\b\u0001\u0010!\u001a\u00020\u00062\b\b\u0001\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\u00062\b\b\u0001\u0010\"\u001a\u00020\u0006H'J\u0018\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\u00032\b\b\u0001\u0010%\u001a\u00020\u0006H'J\u0018\u0010&\u001a\b\u0012\u0004\u0012\u00020$0\u00032\b\b\u0001\u0010'\u001a\u00020\u0006H'J\u001c\u0010(\u001a\u00020\b2\b\b\u0001\u0010)\u001a\u00020\u00062\b\b\u0001\u0010*\u001a\u00020\u0006H'¨\u0006+"},
   d2 = {"Lco/uk/getmondo/api/PaymentsApi;", "", "bacsDirectDebits", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/payments/DirectDebitListResponse;", "accountId", "", "cancelBacsDirectDebit", "Lio/reactivex/Completable;", "directDebitId", "challengeType", "challenge", "idempotencyKey", "cancelScheduledPaymentSeries", "seriesId", "scheduleBankPayment", "Lco/uk/getmondo/api/model/payments/SeriesResponse;", "amount", "", "currency", "recipientAccountNumber", "recipientSortCode", "recipientName", "reference", "startDate", "Lorg/threeten/bp/LocalDate;", "interval", "endDate", "scheduledPaymentsSeries", "Lco/uk/getmondo/api/model/payments/SeriesListResponse;", "transferMoneyToBank", "transferMoneyToPeer", "recipient", "recipientType", "notes", "userByPhoneNumber", "Lco/uk/getmondo/model/UserResponse;", "phoneNumber", "userByUsername", "username", "validatePayee", "accountNumber", "sortCode", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface PaymentsApi {
   @GET("bacs-direct-debits/instructions")
   io.reactivex.v bacsDirectDebits(@Query("account_id") String var);

   @FormUrlEncoded
   @PUT("/bacs-direct-debits/instructions/{direct_debit_id}/cancel")
   io.reactivex.b cancelBacsDirectDebit(@Path("direct_debit_id") String var, @Field("account_id") String var, @Field("challenge_type") String var, @Field("challenge") String var, @Field("idempotency_key") String var);

   @DELETE("scheduled-payments/series/{series_id}")
   io.reactivex.b cancelScheduledPaymentSeries(@Path("series_id") String var, @Query("account_id") String var, @Query("challenge_type") String var, @Query("challenge") String var, @Query("idempotency_key") String var);

   @FormUrlEncoded
   @POST("scheduled-payments/series/fps")
   io.reactivex.v scheduleBankPayment(@Field("account_id") String var, @Field("amount") long var, @Field("currency") String var, @Field("beneficiary_account_number") String var, @Field("beneficiary_sort_code") String var, @Field("beneficiary_customer_name") String var, @Field("idempotency_key") String var, @Field("challenge_type") String var, @Field("challenge") String var, @Field("reference") String var, @Field("start_date") LocalDate var, @Field("interval_type") String var, @Field("end_date") LocalDate var);

   @GET("scheduled-payments/series")
   io.reactivex.v scheduledPaymentsSeries(@Query("account_id") String var);

   @FormUrlEncoded
   @POST("faster-payments/create")
   io.reactivex.b transferMoneyToBank(@Field("account_id") String var, @Field("amount") long var, @Field("currency") String var, @Field("beneficiary_account_number") String var, @Field("beneficiary_sort_code") String var, @Field("beneficiary_customer_name") String var, @Field("idempotency_key") String var, @Field("challenge_type") String var, @Field("challenge") String var, @Field("reference") String var);

   @FormUrlEncoded
   @POST("p2p/transfer")
   io.reactivex.b transferMoneyToPeer(@Field("account_id") String var, @Field("recipient") String var, @Field("recipient_type") String var, @Field("amount") long var, @Field("currency") String var, @Field("challenge_type") String var, @Field("challenge") String var, @Field("dedupe_id") String var, @Field("notes") String var);

   @FormUrlEncoded
   @POST("p2p/recipients")
   io.reactivex.v userByPhoneNumber(@Field("candidate[0][phone]") String var);

   @FormUrlEncoded
   @POST("p2p/recipients")
   io.reactivex.v userByUsername(@Field("candidate[0][username]") String var);

   @GET("payee/validate")
   io.reactivex.b validatePayee(@Query("account_number") String var, @Query("sort_code") String var);
}
