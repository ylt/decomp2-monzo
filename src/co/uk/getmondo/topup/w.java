package co.uk.getmondo.topup;

// $FF: synthetic class
final class w implements io.reactivex.c.h {
   private final q a;
   private final String b;
   private final q.a c;

   private w(q var, String var, q.a var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(q var, String var, q.a var) {
      return new w(var, var, var);
   }

   public Object a(Object var) {
      return q.a(this.a, this.b, this.c, (String)var);
   }
}
