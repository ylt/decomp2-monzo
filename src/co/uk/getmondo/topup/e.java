package co.uk.getmondo.topup;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import java.util.HashMap;
import java.util.Map;

public class e {
   private final Handler a = new Handler();
   private e.a b;
   private View c;
   private View d;
   private Map e = new HashMap();
   private final Runnable f = new Runnable() {
      public void run() {
         e.this.e.put(this, Boolean.valueOf(true));
         if(e.this.b != null && !e.this.b.a()) {
            e.this.a.postDelayed(this, 200L);
         }

      }
   };
   private final Runnable g = new Runnable() {
      public void run() {
         e.this.e.put(this, Boolean.valueOf(true));
         if(e.this.b != null && !e.this.b.b()) {
            e.this.a.postDelayed(this, 200L);
         }

      }
   };

   private OnTouchListener a(Runnable var) {
      return f.a(this, var);
   }

   // $FF: synthetic method
   static boolean a(e var, Runnable var, View var, MotionEvent var) {
      boolean var = false;
      boolean var = false;
      boolean var = var;
      switch(var.getAction()) {
      case 0:
         var.setPressed(true);
         var.e.put(var, Boolean.valueOf(false));
         var.a.postDelayed(var, 600L);
         var = true;
         break;
      case 1:
         var.setPressed(false);
         var.a.removeCallbacks(var);
         var = var;
         if(var.e.containsKey(var)) {
            var = ((Boolean)var.e.get(var)).booleanValue();
         }

         if(!var) {
            var.performClick();
         }

         var = true;
      case 2:
         break;
      case 3:
         var.setPressed(false);
         var.a.removeCallbacks(var);
         var = true;
         break;
      default:
         var = var;
      }

      return var;
   }

   public void a() {
      if(this.c != null) {
         this.c.setOnTouchListener((OnTouchListener)null);
      }

      if(this.d != null) {
         this.d.setOnTouchListener((OnTouchListener)null);
      }

      this.a.removeCallbacks(this.g);
      this.a.removeCallbacks(this.f);
   }

   public void a(View var, View var) {
      this.c = var;
      this.d = var;
      this.c.setOnTouchListener(this.a(this.f));
      this.d.setOnTouchListener(this.a(this.g));
   }

   public void a(e.a var) {
      this.b = var;
   }

   public interface a {
      boolean a();

      boolean b();
   }
}
