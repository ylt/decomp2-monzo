package co.uk.getmondo.topup.bank;

import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.topup.ao;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new c(var, var, var, var, var);
   }

   public b a() {
      return (b)b.a.c.a(this.b, new b((d)this.c.b(), (co.uk.getmondo.common.a)this.d.b(), (co.uk.getmondo.card.c)this.e.b(), (ao)this.f.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
