package co.uk.getmondo.topup.bank;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.text.emoji.widget.EmojiTextView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.e;
import co.uk.getmondo.common.k.j;
import co.uk.getmondo.common.ui.CopyableLinearLayout;
import io.reactivex.n;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0001'B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0012\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u0006H\u0014J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001fH\u0016J\u0010\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020\u001fH\u0016J \u0010#\u001a\u00020\u00062\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020\u001fH\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR2\u0010\t\u001a&\u0012\f\u0012\n \u000b*\u0004\u0018\u00010\u00060\u0006 \u000b*\u0012\u0012\f\u0012\n \u000b*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\n0\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\f\u001a\u00020\r8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006("},
   d2 = {"Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter$TopUpInstructionsView;", "()V", "shareClicks", "Lio/reactivex/Observable;", "", "getShareClicks", "()Lio/reactivex/Observable;", "shareClicksRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "kotlin.jvm.PlatformType", "topUpInstructionsPresenter", "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;", "getTopUpInstructionsPresenter", "()Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;", "setTopUpInstructionsPresenter", "(Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;)V", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "share", "subject", "", "text", "showMonzoDetails", "cardNumber", "showRetailCardDetails", "beneficiary", "accountNumber", "sortCode", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TopUpInstructionsActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final TopUpInstructionsActivity.a b = new TopUpInstructionsActivity.a((i)null);
   public b a;
   private final com.b.b.c c = com.b.b.c.a();
   private final n e;
   private HashMap f;

   public TopUpInstructionsActivity() {
      com.b.b.c var = this.c;
      l.a(var, "shareClicksRelay");
      this.e = (n)var;
   }

   public static final Intent a(Context var) {
      l.b(var, "context");
      return b.a(var);
   }

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.f.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      return this.e;
   }

   public void a(String var) {
      l.b(var, "cardNumber");
      ((EmojiTextView)this.a(co.uk.getmondo.c.a.topUpInstructionsTextView)).setText((CharSequence)this.getString(2131362795));
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpBeneficiaryLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpBeneficiaryValue)).setText((CharSequence)this.getString(2131362789));
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpAccountNumberLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpAccountNumberValue)).setText((CharSequence)this.getString(2131362785));
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpSortCodeLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpSortCodeValue)).setText((CharSequence)this.getString(2131362814));
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpReferenceLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpReferenceValue)).setText((CharSequence)var);
   }

   public void a(String var, String var) {
      l.b(var, "subject");
      l.b(var, "text");
      this.startActivity(j.a((Context)this, var, var, co.uk.getmondo.api.model.tracking.a.TOP_UP_SHARE));
   }

   public void a(String var, String var, String var) {
      l.b(var, "beneficiary");
      l.b(var, "accountNumber");
      l.b(var, "sortCode");
      String var = e.b(127881);
      ((EmojiTextView)this.a(co.uk.getmondo.c.a.topUpInstructionsTextView)).setText((CharSequence)this.getString(2131362811, new Object[]{var}));
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpBeneficiaryLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpBeneficiaryValue)).setText((CharSequence)var);
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpAccountNumberLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpAccountNumberValue)).setText((CharSequence)var);
      ae.a((View)((CopyableLinearLayout)this.a(co.uk.getmondo.c.a.topUpSortCodeLayout)));
      ((TextView)this.a(co.uk.getmondo.c.a.topUpSortCodeValue)).setText((CharSequence)var);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034222);
      this.l().a(this);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(true);
      }

      b var = this.a;
      if(var == null) {
         l.b("topUpInstructionsPresenter");
      }

      var.a((b.a)this);
   }

   public boolean onCreateOptionsMenu(Menu var) {
      l.b(var, "menu");
      this.getMenuInflater().inflate(2131951625, var);
      return true;
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("topUpInstructionsPresenter");
      }

      var.b();
      super.onDestroy();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      l.b(var, "item");
      boolean var;
      if(var.getItemId() == 2131821784) {
         this.c.a((Object)kotlin.n.a);
         var = true;
      } else {
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/topup/bank/TopUpInstructionsActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, TopUpInstructionsActivity.class);
      }
   }
}
