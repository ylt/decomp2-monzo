package co.uk.getmondo.topup.card;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.d.ae;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public class TopUpWithSavedCardActivity extends co.uk.getmondo.common.activities.b implements h.a {
   co.uk.getmondo.common.a a;
   h b;
   ThreeDsResolver c;
   @BindView(2131820842)
   TextView descriptionView;
   private ProgressDialog e;
   private ae f;
   private co.uk.getmondo.d.c g;
   @BindView(2131821148)
   SavedCardView savedCardView;
   @BindView(2131821147)
   Button topUpButton;

   public static void a(Context var, co.uk.getmondo.d.c var, ae var) {
      var.startActivity(b(var, var, var));
   }

   public static Intent b(Context var, co.uk.getmondo.d.c var, ae var) {
      Intent var = new Intent(var, TopUpWithSavedCardActivity.class);
      var.putExtra("EXTRA_CARD", var);
      var.putExtra("EXTRA_AMOUNT", var);
      return var;
   }

   public void a() {
      if(this.e == null) {
         this.e = new ProgressDialog(this);
         this.e.setCancelable(false);
         this.e.setMessage(this.getString(2131362809));
      }

      if(!this.e.isShowing()) {
         this.e.show();
      }

   }

   public void a(boolean var) {
      this.topUpButton.setEnabled(var);
   }

   public void b() {
      if(this.e != null && this.e.isShowing()) {
         this.e.dismiss();
      }

   }

   public void c() {
      ConfirmationActivity.a(this, HomeActivity.b((Context)this));
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(!this.c.a(var, var, var)) {
         super.onActivityResult(var, var, var);
      }

   }

   @OnClick({2131821149})
   public void onAddNewCardClicked() {
      TopUpWithNewCardActivity.a(this, this.g);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.f = (ae)this.getIntent().getParcelableExtra("EXTRA_CARD");
      this.g = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("EXTRA_AMOUNT");
      if(this.f != null && this.g != null) {
         this.l().a(this);
         this.b.a((h.a)this);
         this.setContentView(2131034223);
         ButterKnife.bind((Activity)this);
         this.savedCardView.a(this.f);
         String var = this.g.toString();
         this.setTitle(this.getString(2131362820, new Object[]{var}));
         this.topUpButton.setText(this.getString(2131362790, new Object[]{var}));
         this.descriptionView.setText(this.getString(2131362812, new Object[]{var}));
      } else {
         throw new RuntimeException("Card and amount are required");
      }
   }

   protected void onDestroy() {
      this.b.b();
      super.onDestroy();
   }

   @OnClick({2131821147})
   public void topUpClicked() {
      this.b.a(this.g, this.f.b());
   }
}
