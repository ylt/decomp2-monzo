package co.uk.getmondo.topup.card;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopUpWithCardFragment_ViewBinding implements Unbinder {
   private TopUpWithCardFragment a;
   private View b;
   private TextWatcher c;
   private View d;

   public TopUpWithCardFragment_ViewBinding(final TopUpWithCardFragment var, View var) {
      this.a = var;
      var.descriptionView = (TextView)Utils.findRequiredViewAsType(var, 2131820842, "field 'descriptionView'", TextView.class);
      var.cardHolderNameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821388, "field 'cardHolderNameWrapper'", TextInputLayout.class);
      var.cardHolderName = (EditText)Utils.findRequiredViewAsType(var, 2131821389, "field 'cardHolderName'", EditText.class);
      var.cardNumberWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821390, "field 'cardNumberWrapper'", TextInputLayout.class);
      var.cardNumber = (EditText)Utils.findRequiredViewAsType(var, 2131821391, "field 'cardNumber'", EditText.class);
      var.expiryDateWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821392, "field 'expiryDateWrapper'", TextInputLayout.class);
      var.expiryDate = (EditText)Utils.findRequiredViewAsType(var, 2131821393, "field 'expiryDate'", EditText.class);
      var.cvvWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821394, "field 'cvvWrapper'", TextInputLayout.class);
      var.cvv = (EditText)Utils.findRequiredViewAsType(var, 2131821395, "field 'cvv'", EditText.class);
      var.billingPostcodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821396, "field 'billingPostcodeWrapper'", TextInputLayout.class);
      View var = Utils.findRequiredView(var, 2131821397, "field 'billingPostcode' and method 'onPostcodeTextChanged'");
      var.billingPostcode = (EditText)Utils.castView(var, 2131821397, "field 'billingPostcode'", EditText.class);
      this.b = var;
      this.c = new TextWatcher() {
         public void afterTextChanged(Editable varx) {
         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            var.onPostcodeTextChanged(varx);
         }
      };
      ((TextView)var).addTextChangedListener(this.c);
      var = Utils.findRequiredView(var, 2131821147, "field 'topUpButton' and method 'onTopUpClicked'");
      var.topUpButton = (Button)Utils.castView(var, 2131821147, "field 'topUpButton'", Button.class);
      this.d = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onTopUpClicked();
         }
      });
   }

   public void unbind() {
      TopUpWithCardFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.descriptionView = null;
         var.cardHolderNameWrapper = null;
         var.cardHolderName = null;
         var.cardNumberWrapper = null;
         var.cardNumber = null;
         var.expiryDateWrapper = null;
         var.expiryDate = null;
         var.cvvWrapper = null;
         var.cvv = null;
         var.billingPostcodeWrapper = null;
         var.billingPostcode = null;
         var.topUpButton = null;
         ((TextView)this.b).removeTextChangedListener(this.c);
         this.c = null;
         this.b = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
