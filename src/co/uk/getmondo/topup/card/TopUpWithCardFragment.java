package co.uk.getmondo.topup.card;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public class TopUpWithCardFragment extends co.uk.getmondo.common.f.a implements b.a {
   b a;
   @BindView(2131821397)
   EditText billingPostcode;
   @BindView(2131821396)
   TextInputLayout billingPostcodeWrapper;
   co.uk.getmondo.common.a c;
   @BindView(2131821389)
   EditText cardHolderName;
   @BindView(2131821388)
   TextInputLayout cardHolderNameWrapper;
   @BindView(2131821391)
   EditText cardNumber;
   @BindView(2131821390)
   TextInputLayout cardNumberWrapper;
   @BindView(2131821395)
   EditText cvv;
   @BindView(2131821394)
   TextInputLayout cvvWrapper;
   private co.uk.getmondo.d.c d;
   @BindView(2131820842)
   TextView descriptionView;
   private ProgressDialog e;
   @BindView(2131821393)
   EditText expiryDate;
   @BindView(2131821392)
   TextInputLayout expiryDateWrapper;
   private Boolean f;
   private ThreeDsResolver g;
   private Unbinder h;
   private TopUpWithCardFragment.a i;
   @BindView(2131821147)
   Button topUpButton;

   public static Fragment a(co.uk.getmondo.d.c var, boolean var) {
      TopUpWithCardFragment var = new TopUpWithCardFragment();
      Bundle var = new Bundle();
      var.putParcelable("ARG_AMOUNT", var);
      var.putBoolean("ARG_INITIAL_TOPUP", var);
      var.setArguments(var);
      return var;
   }

   private void h() {
      final co.uk.getmondo.create_account.c var = new co.uk.getmondo.create_account.c();
      this.expiryDate.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable varx) {
            if(TopUpWithCardFragment.this.expiryDate.getText().length() == 7) {
               TopUpWithCardFragment.this.cvv.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            TopUpWithCardFragment.this.expiryDateWrapper.setError((CharSequence)null);
            var.a(varx, var, var, this, TopUpWithCardFragment.this.expiryDateWrapper.getEditText());
         }
      });
   }

   private void i() {
      this.cardNumber.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var) {
            if(TopUpWithCardFragment.this.cardNumber.getText().length() == 19) {
               TopUpWithCardFragment.this.expiryDate.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence var, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence var, int var, int var, int var) {
            TopUpWithCardFragment.this.cardNumberWrapper.setError((CharSequence)null);
            co.uk.getmondo.create_account.a.a(var, var, TopUpWithCardFragment.this.cardNumber, this);
         }
      });
   }

   private void j() {
      this.cvv.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var) {
            if(TopUpWithCardFragment.this.cvv.getText().length() == 3) {
               TopUpWithCardFragment.this.billingPostcode.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence var, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence var, int var, int var, int var) {
            TopUpWithCardFragment.this.cvvWrapper.setError((CharSequence)null);
         }
      });
   }

   public void a() {
      this.cardNumberWrapper.setError(this.getContext().getString(2131362805));
   }

   public void a(String var) {
      this.cardHolderName.setText(var);
      this.cardNumber.requestFocus();
   }

   public void a(boolean var) {
      this.topUpButton.setEnabled(var);
   }

   public boolean a(Throwable var) {
      return this.i.a(var);
   }

   public void b() {
      this.expiryDateWrapper.setError(this.getContext().getString(2131362807));
   }

   public void c() {
      this.cvvWrapper.setError(this.getContext().getString(2131362806));
   }

   public void d() {
      this.billingPostcodeWrapper.setError(this.getContext().getString(2131362808));
   }

   public void e() {
      this.i.b();
   }

   public void f() {
      if(this.e == null) {
         this.e = new ProgressDialog(this.getContext(), 2131493134);
         this.e.setCancelable(false);
         this.e.setMessage(this.getContext().getString(2131362809));
      }

      if(!this.e.isShowing()) {
         this.e.show();
      }

   }

   public void g() {
      if(this.e != null && this.e.isShowing()) {
         this.e.dismiss();
      }

   }

   public void onActivityResult(int var, int var, Intent var) {
      if(!this.g.a(var, var, var)) {
         super.onActivityResult(var, var, var);
      }

   }

   public void onAttach(Context var) {
      super.onAttach(var);
      this.i = (TopUpWithCardFragment.a)var;
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
      this.g = new co.uk.getmondo.topup.three_d_secure.b(this, this.c);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034288, var, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.h.unbind();
      super.onDestroyView();
   }

   @OnTextChanged({2131821397})
   public void onPostcodeTextChanged(CharSequence var) {
      this.billingPostcodeWrapper.setError((CharSequence)null);
   }

   @OnClick({2131821147})
   public void onTopUpClicked() {
      this.cardNumberWrapper.setError((CharSequence)null);
      this.expiryDateWrapper.setError((CharSequence)null);
      this.cvvWrapper.setError((CharSequence)null);
      this.cardHolderNameWrapper.setError((CharSequence)null);
      this.billingPostcodeWrapper.setError((CharSequence)null);
      this.a.a(this.cardHolderName.getText().toString(), this.cardNumber.getText().toString(), this.expiryDate.getText().toString(), this.cvv.getText().toString(), this.billingPostcode.getText().toString(), this.d, this.f.booleanValue(), true, this.g);
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.h = ButterKnife.bind(this, (View)var);
      this.a.a((b.a)this);
      this.d = (co.uk.getmondo.d.c)this.getArguments().getParcelable("ARG_AMOUNT");
      this.f = Boolean.valueOf(this.getArguments().getBoolean("ARG_INITIAL_TOPUP"));
      String var = this.d.toString();
      this.descriptionView.setText(this.getContext().getString(2131362821, new Object[]{var}));
      this.topUpButton.setText(this.getContext().getString(2131362790, new Object[]{var}));
      this.h();
      this.i();
      this.j();
   }

   public interface a {
      boolean a(Throwable var);

      void b();
   }
}
