package co.uk.getmondo.topup.card;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SavedCardView_ViewBinding implements Unbinder {
   private SavedCardView a;

   public SavedCardView_ViewBinding(SavedCardView var, View var) {
      this.a = var;
      var.cardImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821755, "field 'cardImageView'", ImageView.class);
      var.cardHolderNameView = (TextView)Utils.findRequiredViewAsType(var, 2131821389, "field 'cardHolderNameView'", TextView.class);
      var.cardLastFourView = (TextView)Utils.findRequiredViewAsType(var, 2131821756, "field 'cardLastFourView'", TextView.class);
      var.cardExpiryView = (TextView)Utils.findRequiredViewAsType(var, 2131821757, "field 'cardExpiryView'", TextView.class);
   }

   public void unbind() {
      SavedCardView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.cardImageView = null;
         var.cardHolderNameView = null;
         var.cardLastFourView = null;
         var.cardExpiryView = null;
      }
   }
}
