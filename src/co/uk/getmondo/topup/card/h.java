package co.uk.getmondo.topup.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.topup.a.w;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import io.reactivex.u;

class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.topup.a.c g;
   private final w h;
   private final co.uk.getmondo.common.a i;
   private final ThreeDsResolver j;

   h(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.a var, co.uk.getmondo.topup.a.c var, w var, ThreeDsResolver var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(h var, String var, co.uk.getmondo.d.c var, String var) throws Exception {
      return var.g.a(var, var, var, false, var.j);
   }

   // $FF: synthetic method
   static void a(h var) throws Exception {
      var.i.a(Impression.a(Impression.TopUpSuccessType.SAVED_CARD));
      ((h.a)var.a).b();
      ((h.a)var.a).a(true);
      ((h.a)var.a).c();
   }

   // $FF: synthetic method
   static void a(h var, Throwable var) throws Exception {
      var.i.a(Impression.e(var.getMessage()));
      ((h.a)var.a).b();
      ((h.a)var.a).a(true);
      if(!var.h.a((co.uk.getmondo.common.e.e)var.a, var) && !var.e.a(var, (co.uk.getmondo.common.e.a.a)var.a)) {
         ((h.a)var.a).b(2131362802);
      }

   }

   void a(co.uk.getmondo.d.c var, String var) {
      this.i.a(Impression.a(Impression.TopUpTapFrom.FROM_SAVED_CARD));
      ((h.a)this.a).a();
      ((h.a)this.a).a(false);
      this.a((io.reactivex.b.b)this.f.d().c(i.a(this, var, var)).b(this.d).a(this.c).a(j.a(this), k.a(this)));
   }

   public void a(h.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.l());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(boolean var);

      void b();

      void c();
   }
}
