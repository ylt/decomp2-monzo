package co.uk.getmondo.topup.card;

import co.uk.getmondo.topup.a.w;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import io.reactivex.u;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;

   static {
      boolean var;
      if(!l.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public l(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new l(var, var, var, var, var, var, var, var, var);
   }

   public h a() {
      return (h)b.a.c.a(this.b, new h((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.common.e.a)this.e.b(), (co.uk.getmondo.common.accounts.d)this.f.b(), (co.uk.getmondo.common.a)this.g.b(), (co.uk.getmondo.topup.a.c)this.h.b(), (w)this.i.b(), (ThreeDsResolver)this.j.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
