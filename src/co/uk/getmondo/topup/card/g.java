package co.uk.getmondo.topup.card;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public final class g implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new g(var, var, var);
   }

   public void a(TopUpWithSavedCardActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (co.uk.getmondo.common.a)this.b.b();
         var.b = (h)this.c.b();
         var.c = (ThreeDsResolver)this.d.b();
      }
   }
}
