package co.uk.getmondo.topup.card;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.main.HomeActivity;

public class TopUpWithNewCardActivity extends co.uk.getmondo.common.activities.b implements TopUpWithCardFragment.a {
   public static void a(Context var, co.uk.getmondo.d.c var) {
      Intent var = new Intent(var, TopUpWithNewCardActivity.class);
      var.putExtra("EXTRA_TOPUP_AMOUNT", var);
      var.startActivity(var);
   }

   public boolean a(Throwable var) {
      return false;
   }

   public void b() {
      ConfirmationActivity.a(this, HomeActivity.b((Context)this));
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.l().a(this);
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("EXTRA_TOPUP_AMOUNT");
      if(var == null) {
         throw new RuntimeException("Top up amount required");
      } else {
         this.setTitle(this.getString(2131362820, new Object[]{var.toString()}));
         if(this.getSupportFragmentManager().a(16908290) == null) {
            this.getSupportFragmentManager().a().b(16908290, TopUpWithCardFragment.a(var, false)).c();
         }

      }
   }
}
