package co.uk.getmondo.topup.card;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopUpWithSavedCardActivity_ViewBinding implements Unbinder {
   private TopUpWithSavedCardActivity a;
   private View b;
   private View c;

   public TopUpWithSavedCardActivity_ViewBinding(final TopUpWithSavedCardActivity var, View var) {
      this.a = var;
      var.savedCardView = (SavedCardView)Utils.findRequiredViewAsType(var, 2131821148, "field 'savedCardView'", SavedCardView.class);
      var.descriptionView = (TextView)Utils.findRequiredViewAsType(var, 2131820842, "field 'descriptionView'", TextView.class);
      View var = Utils.findRequiredView(var, 2131821147, "field 'topUpButton' and method 'topUpClicked'");
      var.topUpButton = (Button)Utils.castView(var, 2131821147, "field 'topUpButton'", Button.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.topUpClicked();
         }
      });
      var = Utils.findRequiredView(var, 2131821149, "method 'onAddNewCardClicked'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onAddNewCardClicked();
         }
      });
   }

   public void unbind() {
      TopUpWithSavedCardActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.savedCardView = null;
         var.descriptionView = null;
         var.topUpButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
