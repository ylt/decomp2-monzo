package co.uk.getmondo.topup.card;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.ae;

public class SavedCardView extends FrameLayout {
   @BindView(2131821757)
   TextView cardExpiryView;
   @BindView(2131821389)
   TextView cardHolderNameView;
   @BindView(2131821755)
   ImageView cardImageView;
   @BindView(2131821756)
   TextView cardLastFourView;

   public SavedCardView(Context var) {
      this(var, (AttributeSet)null);
   }

   public SavedCardView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public SavedCardView(Context var, AttributeSet var, int var) {
      this(var, var, var, 0);
   }

   public SavedCardView(Context var, AttributeSet var, int var, int var) {
      super(var, var, var, var);
      ButterKnife.bind(LayoutInflater.from(this.getContext()).inflate(2131034436, this, true), (View)this);
   }

   private int a(String var) {
      int var;
      if("visa".equalsIgnoreCase(var)) {
         var = 2130837684;
      } else if("mastercard".equalsIgnoreCase(var)) {
         var = 2130837634;
      } else {
         var = 2130837632;
      }

      return var;
   }

   public void a(ae var) {
      this.cardImageView.setImageResource(this.a(var.d()));
      this.cardLastFourView.setText(var.c());
      this.cardExpiryView.setText(var.a());
   }
}
