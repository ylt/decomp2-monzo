package co.uk.getmondo.topup.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.topup.a.w;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.model.Card;
import io.reactivex.u;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class b extends co.uk.getmondo.common.ui.b {
   private static final SimpleDateFormat c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.topup.a.c h;
   private final w i;
   private final co.uk.getmondo.common.a j;

   static {
      c = new SimpleDateFormat("MM/yy", Locale.UK);
   }

   b(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.a var, co.uk.getmondo.topup.a.c var, w var) {
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(b var, Card var, boolean var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var, String var) throws Exception {
      return var.h.a(var, var, var, var, var, var);
   }

   private Calendar a(String var) {
      Calendar var;
      try {
         Date var = c.parse(var);
         var = Calendar.getInstance();
         var.setTime(var);
      } catch (ParseException var) {
         var = null;
      }

      return var;
   }

   private void a() {
      this.j.a(Impression.a(Impression.TopUpSuccessType.NEW_CARD));
      ((b.a)this.a).g();
      ((b.a)this.a).a(true);
      ((b.a)this.a).e();
   }

   // $FF: synthetic method
   static void a(b var) {
      var.a();
   }

   // $FF: synthetic method
   static void a(b var, Throwable var) {
      var.a(var);
   }

   private void a(Card var, b.a var) {
      if(!var.validateNumber()) {
         var.a();
      }

      if(!var.validateCVC()) {
         var.c();
      }

      if(!var.validateExpiryDate()) {
         var.b();
      }

   }

   private void a(Throwable var) {
      d.a.a.a(var, "Error topping up", new Object[0]);
      this.j.a(Impression.e(var.getMessage()));
      ((b.a)this.a).g();
      ((b.a)this.a).a(true);
      if(!((b.a)this.a).a(var) && !this.i.a((co.uk.getmondo.common.e.e)this.a, var) && !this.f.a(var, (co.uk.getmondo.common.e.a.a)this.a)) {
         ((b.a)this.a).b(2131362802);
      }

   }

   private String b(String var) {
      return var.trim().replaceAll("\\s+", "");
   }

   public void a(b.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.j.a(Impression.m());
      ac var = this.g.b().d();
      if(var != null && !p.d(var.c())) {
         var.a(var.c());
      }

   }

   void a(String var, String var, String var, String var, String var, co.uk.getmondo.d.c var, boolean var, boolean var, ThreeDsResolver var) {
      this.j.a(Impression.a(Impression.TopUpTapFrom.FROM_NEW_CARD));
      var = var.trim();
      Calendar var = this.a(this.b(var));
      int var;
      if(var == null) {
         var = 0;
      } else {
         var = var.get(2) + 1;
      }

      int var;
      if(var == null) {
         var = 0;
      } else {
         var = var.get(1);
      }

      Card var = (new Card.Builder(this.b(var), Integer.valueOf(var), Integer.valueOf(var), var)).addressZip(var).name(var).build();
      boolean var = var.validateCard();
      boolean var;
      if(!var.isEmpty()) {
         var = true;
      } else {
         var = false;
      }

      boolean var;
      if(var && var) {
         var = true;
      } else {
         var = false;
      }

      if(!var) {
         this.a(var, (b.a)this.a);
      }

      if(!var) {
         ((b.a)this.a).d();
      }

      if(var) {
         ((b.a)this.a).f();
         ((b.a)this.a).a(false);
         this.a((io.reactivex.b.b)this.g.d().c(c.a(this, var, var, var, var, var)).b(this.e).a(this.d).a(d.a(this), e.a(this)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var);

      void a(boolean var);

      boolean a(Throwable var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
