package co.uk.getmondo.topup.card;

// $FF: synthetic class
final class i implements io.reactivex.c.h {
   private final h a;
   private final String b;
   private final co.uk.getmondo.d.c c;

   private i(h var, String var, co.uk.getmondo.d.c var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(h var, String var, co.uk.getmondo.d.c var) {
      return new i(var, var, var);
   }

   public Object a(Object var) {
      return h.a(this.a, this.b, this.c, (String)var);
   }
}
