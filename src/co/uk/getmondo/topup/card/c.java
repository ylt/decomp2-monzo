package co.uk.getmondo.topup.card;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.model.Card;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final b a;
   private final Card b;
   private final boolean c;
   private final co.uk.getmondo.d.c d;
   private final boolean e;
   private final ThreeDsResolver f;

   private c(b var, Card var, boolean var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   public static io.reactivex.c.h a(b var, Card var, boolean var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      return new c(var, var, var, var, var, var);
   }

   public Object a(Object var) {
      return b.a(this.a, this.b, this.c, this.d, this.e, this.f, (String)var);
   }
}
