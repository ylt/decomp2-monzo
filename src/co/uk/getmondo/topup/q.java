package co.uk.getmondo.topup;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.topup.ApiTopUpLimits;
import co.uk.getmondo.api.model.tracking.Impression;
import java.util.List;
import org.json.JSONException;

public class q extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.a.a c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final MonzoApi h;
   private final ao i;
   private a j;
   private co.uk.getmondo.d.ae k;
   private co.uk.getmondo.common.a l;
   private final co.uk.getmondo.topup.a.c m;
   private final co.uk.getmondo.topup.a.w n;

   q(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var, MonzoApi var, ao var, co.uk.getmondo.a.a var, co.uk.getmondo.topup.a.c var, co.uk.getmondo.topup.a.w var) {
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.c = var;
      this.l = var;
      this.m = var;
      this.n = var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.c a(Integer var) throws Exception {
      return new co.uk.getmondo.d.c((long)(var.intValue() * 100), "GBP");
   }

   // $FF: synthetic method
   static io.reactivex.l a(q var, String var, q.a var, String var) throws Exception {
      co.uk.getmondo.d.c var = new co.uk.getmondo.d.c(var.j.g(), "GBP");
      return var.m.a(var, var, var).b(var.e).a(var.d).a(ab.a(var, var)).a((Object)co.uk.getmondo.common.b.a.a).e().a((io.reactivex.l)io.reactivex.h.a());
   }

   // $FF: synthetic method
   static io.reactivex.r a(q var, String var, a var) throws Exception {
      return io.reactivex.n.combineLatest(var.f().map(ad.a()), var.c.a(var), ae.a());
   }

   // $FF: synthetic method
   static io.reactivex.z a(co.uk.getmondo.d.a.r var, List var) throws Exception {
      io.reactivex.n var = io.reactivex.n.fromIterable(var);
      var.getClass();
      return var.map(af.a(var)).toList();
   }

   private void a(q.a var, co.uk.getmondo.d.b var) {
      if(var != null) {
         co.uk.getmondo.d.c var = var.a().a((long)(this.j.e() * 100));
         var.a(this.i.a(var));
      }

   }

   // $FF: synthetic method
   static void a(q.a var, Boolean var) throws Exception {
      var.g();
      if(!var.booleanValue()) {
         var.e();
      }

   }

   // $FF: synthetic method
   static void a(q var, q.a var, android.support.v4.g.j var) throws Exception {
      var.a((co.uk.getmondo.d.c)var.a);
      var.b(var);
      var.a(var, (co.uk.getmondo.d.b)var.b);
   }

   // $FF: synthetic method
   static void a(q var, q.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.l.a(Impression.a(Impression.TopUpSuccessType.ANDROID_PAY));
      var.d();
   }

   // $FF: synthetic method
   static void a(q var, q.a var, Object var) throws Exception {
      var.f();
      var.l.a(Impression.a(Impression.TopUpTapFrom.ANDROID_PAY));
   }

   // $FF: synthetic method
   static void a(q var, q.a var, Throwable var) throws Exception {
      var.g();
      if(var instanceof JSONException) {
         var.e();
      } else if(!var.n.a(var, var)) {
         var.g.a(var, var);
      }

   }

   // $FF: synthetic method
   static boolean a(q.a var, android.support.v4.g.j var) throws Exception {
      ApiTopUpLimits var = (ApiTopUpLimits)var.a;
      boolean var;
      if(var.b() >= 10.0F && var.b() >= var.a()) {
         var = true;
      } else {
         var.b();
         var.g();
         var = false;
      }

      return var;
   }

   // $FF: synthetic method
   static a b(q var, q.a var, android.support.v4.g.j var) throws Exception {
      ApiTopUpLimits var = (ApiTopUpLimits)var.a;
      List var = (List)var.b;
      co.uk.getmondo.d.ae var;
      if(var.size() > 0) {
         var = (co.uk.getmondo.d.ae)var.get(0);
      } else {
         var = null;
      }

      var.k = var;
      var.j = new a(var.a(), var.b(), 100);
      var.a();
      var.c(true);
      var.g();
      return var.j;
   }

   private void b(q.a var) {
      boolean var = true;
      boolean var;
      if(!this.j.d()) {
         var = true;
      } else {
         var = false;
      }

      var.a(var);
      if(!this.j.c()) {
         var = var;
      } else {
         var = false;
      }

      var.b(var);
   }

   // $FF: synthetic method
   static void b(q var, q.a var, Object var) throws Exception {
      co.uk.getmondo.d.c var = new co.uk.getmondo.d.c(var.j.g(), "GBP");
      if(var.k != null) {
         var.a(var, var.k);
      } else {
         var.b(var);
      }

   }

   // $FF: synthetic method
   static void b(q var, q.a var, Throwable var) throws Exception {
      var.g.a(var, var);
   }

   // $FF: synthetic method
   static void c(q var, q.a var, Object var) throws Exception {
      var.l.a(Impression.k());
      var.c();
   }

   // $FF: synthetic method
   static void c(q var, q.a var, Throwable var) throws Exception {
      var.g();
      if(!var.g.a(var, var)) {
         var.h();
      }

   }

   // $FF: synthetic method
   static void d() throws Exception {
      d.a.a.b("Balance refreshed", new Object[0]);
   }

   public void a(q.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(false);
      var.b(false);
      String var = this.f.b().c().a();
      co.uk.getmondo.d.a.r var = new co.uk.getmondo.d.a.r();
      io.reactivex.v var = this.h.stripeCards(var).d(r.a()).a(ac.a(var));
      var.f();
      this.a((io.reactivex.b.b)io.reactivex.v.a(this.h.topUpLimits(var), var, ag.a()).b(this.e).a(this.d).a(ah.a(var)).c(ai.a(this, var)).a(aj.a(this, var)).observeOn(this.d).subscribe(ak.a(this, var), al.a(this, var)));
      this.a((io.reactivex.b.b)this.c.b(var).b(this.e).a(this.d).a(am.b(), s.a(this, var)));
      this.a((io.reactivex.b.b)var.j().subscribe(t.a(this, var)));
      this.a((io.reactivex.b.b)var.i().subscribe(u.a(this, var)));
      this.a((io.reactivex.b.b)var.v().subscribe(v.a(this, var)));
      this.a((io.reactivex.b.b)var.k().flatMapMaybe(w.a(this, var, var)).subscribe(x.a(this, var), y.a()));
      this.a((io.reactivex.b.b)var.w().subscribe(z.a(var), aa.a()));
   }

   boolean a() {
      if(this.j == null) {
         throw new IllegalStateException("Tried to increase load without loading limits");
      } else {
         return this.j.a();
      }
   }

   boolean c() {
      if(this.j == null) {
         throw new IllegalStateException("Tried to decrease load without loading limits");
      } else {
         return this.j.b();
      }
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.c var);

      void a(co.uk.getmondo.d.c var, co.uk.getmondo.d.ae var);

      void a(String var);

      void a(boolean var);

      void b();

      void b(co.uk.getmondo.d.c var);

      void b(boolean var);

      void c();

      void c(boolean var);

      void d();

      void e();

      void f();

      void g();

      void h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();
   }
}
