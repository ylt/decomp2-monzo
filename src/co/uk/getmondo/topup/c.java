package co.uk.getmondo.topup;

import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;

// $FF: synthetic class
final class c implements ResultCallback {
   private final b a;
   private final b.a b;

   private c(b var, b.a var) {
      this.a = var;
      this.b = var;
   }

   public static ResultCallback a(b var, b.a var) {
      return new c(var, var);
   }

   public void onResult(Result var) {
      b.a(this.a, this.b, (BooleanResult)var);
   }
}
