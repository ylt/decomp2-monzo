package co.uk.getmondo.topup;

class a {
   private final int a;
   private final int b;
   private final int c;
   private int d;
   private com.b.b.b e;

   a(float var, float var, int var) {
      this.a = (int)Math.ceil((double)var);
      this.b = (int)Math.floor((double)var);
      this.c = var;
      this.d = this.a;
      this.e = com.b.b.b.b((Object)Integer.valueOf(this.d));
   }

   private void a(int var) {
      this.d = var;
      this.e.a((Object)Integer.valueOf(var));
   }

   private int h() {
      byte var;
      if(this.d < this.c) {
         var = 10;
      } else {
         var = 50;
      }

      return var;
   }

   private int i() {
      byte var;
      if(this.d <= this.c) {
         var = 10;
      } else {
         var = 50;
      }

      return var;
   }

   boolean a() {
      int var = this.d + this.h();
      if(var <= this.b) {
         this.a(var);
      }

      return this.c();
   }

   boolean b() {
      int var = this.d - this.i();
      if(var >= this.a) {
         this.a(var);
      }

      return this.d();
   }

   boolean c() {
      boolean var;
      if(this.d < this.b && this.d + this.h() <= this.b) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   boolean d() {
      boolean var;
      if(this.d > this.a && this.d - this.i() >= this.a) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   int e() {
      return this.d;
   }

   io.reactivex.n f() {
      return this.e;
   }

   long g() {
      return (long)(this.d * 100);
   }
}
