package co.uk.getmondo.topup;

import android.content.Intent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.Wallet.WalletOptions;
import com.google.android.gms.wallet.Wallet.WalletOptions.Builder;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;

class b implements OnConnectionFailedListener {
   private final co.uk.getmondo.common.i.b a = new co.uk.getmondo.common.i.b(true, false, true);
   private co.uk.getmondo.d.c b;
   private GoogleApiClient c;
   private b.d d;
   private b.b e;
   private b.c f;

   b() {
      this.b = new co.uk.getmondo.d.c(1000L, co.uk.getmondo.common.i.c.a);
   }

   private int a(Intent var) {
      int var = -1;
      if(var != null) {
         var = var.getIntExtra("com.google.android.gms.wallet.EXTRA_ERROR_CODE", -1);
      }

      return var;
   }

   private FullWalletRequest a(MaskedWallet var) {
      String var = this.a.a(this.b);
      LineItem var = LineItem.newBuilder().setCurrencyCode(this.b.l().b()).setQuantity("1").setTotalPrice(var).build();
      Cart var = Cart.newBuilder().setCurrencyCode(this.b.l().b()).setTotalPrice(var).addLineItem(var).build();
      return FullWalletRequest.newBuilder().setCart(var).setGoogleTransactionId(var.getGoogleTransactionId()).build();
   }

   private SupportWalletFragment a() {
      WalletFragmentStyle var = (new WalletFragmentStyle()).setBuyButtonText(6).setBuyButtonAppearance(4).setBuyButtonWidth(-1);
      SupportWalletFragment var = SupportWalletFragment.newInstance(WalletFragmentOptions.newBuilder().setEnvironment(1).setFragmentStyle(var).setTheme(1).setMode(1).build());
      var.initialize(this.b());
      return var;
   }

   // $FF: synthetic method
   static void a(b var, b.a var, BooleanResult var) {
      if(var.getStatus().isSuccess()) {
         if(var.getValue()) {
            var.a(var.a());
         } else {
            var.a();
         }
      } else {
         String var = var.getStatus().getStatusMessage();
         int var = var.getStatus().getStatusCode();
         d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, failed to determine if user ready to pay, message: " + var + " errorCode: " + var)));
      }

   }

   private WalletFragmentInitParams b() {
      PaymentMethodTokenizationParameters var = PaymentMethodTokenizationParameters.newBuilder().setPaymentMethodTokenizationType(1).addParameter("gateway", "stripe").addParameter("stripe:publishableKey", "pk_live_LW3b2bi1iXHicd88NnwlCkaA").addParameter("stripe:version", "4.1.3").build();
      MaskedWalletRequest var = MaskedWalletRequest.newBuilder().setMerchantName("Monzo").setPhoneNumberRequired(false).setShippingAddressRequired(false).setCurrencyCode(this.b.l().b()).setEstimatedTotalPrice(this.a.a(this.b)).setPaymentMethodTokenizationParameters(var).build();
      return WalletFragmentInitParams.newBuilder().setMaskedWalletRequest(var).setMaskedWalletRequestCode(1001).build();
   }

   void a(android.support.v4.app.j var, b.a var) {
      WalletOptions var = (new Builder()).setEnvironment(1).setTheme(0).build();
      this.c = (new com.google.android.gms.common.api.GoogleApiClient.Builder(var)).addApi(Wallet.API, var).enableAutoManage(var, 1000, this).build();
      IsReadyToPayRequest var = IsReadyToPayRequest.newBuilder().addAllowedCardNetwork(4).addAllowedCardNetwork(5).build();
      Wallet.Payments.isReadyToPay(this.c, var).setResultCallback(c.a(this, var));
   }

   void a(co.uk.getmondo.d.c var) {
      this.b = var;
   }

   void a(b.b var) {
      this.e = var;
   }

   void a(b.c var) {
      this.f = var;
   }

   void a(b.d var) {
      this.d = var;
   }

   boolean a(int var, int var, Intent var) {
      boolean var = false;
      switch(var) {
      case 1:
         this.f.a(false);
         d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, wallet constants error:" + this.a(var))));
         var = true;
         break;
      case 1001:
         switch(var) {
         case -1:
            this.e.a();
            FullWalletRequest var = this.a((MaskedWallet)var.getParcelableExtra("com.google.android.gms.wallet.EXTRA_MASKED_WALLET"));
            Wallet.Payments.loadFullWallet(this.c, var, 1002);
            break;
         case 0:
            this.f.a(true);
            break;
         default:
            this.f.a(false);
            d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, masked wallet request failed, error code:" + this.a(var))));
         }

         var = true;
         break;
      case 1002:
         switch(var) {
         case -1:
            FullWallet var = (FullWallet)var.getParcelableExtra("com.google.android.gms.wallet.EXTRA_FULL_WALLET");
            this.d.a(var.getPaymentMethodToken().getToken());
            break;
         case 0:
            this.f.a(true);
            break;
         default:
            this.f.a(false);
            d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, full wallet request failed, error code:" + this.a(var))));
         }

         var = true;
      }

      return var;
   }

   public void onConnectionFailed(ConnectionResult var) {
      d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, failed to connect. Message: " + var.getErrorMessage() + ", errorCode: " + var.getErrorCode())));
   }

   interface a {
      void a();

      void a(SupportWalletFragment var);
   }

   @FunctionalInterface
   interface b {
      void a();
   }

   @FunctionalInterface
   interface c {
      void a(boolean var);
   }

   @FunctionalInterface
   interface d {
      void a(String var);
   }
}
