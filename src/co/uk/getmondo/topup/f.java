package co.uk.getmondo.topup;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

// $FF: synthetic class
final class f implements OnTouchListener {
   private final e a;
   private final Runnable b;

   private f(e var, Runnable var) {
      this.a = var;
      this.b = var;
   }

   public static OnTouchListener a(e var, Runnable var) {
      return new f(var, var);
   }

   public boolean onTouch(View var, MotionEvent var) {
      return e.a(this.a, this.b, var, var);
   }
}
