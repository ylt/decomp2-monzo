package co.uk.getmondo.topup;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class TopUpActivity_ViewBinding implements Unbinder {
   private TopUpActivity a;
   private View b;
   private View c;
   private View d;

   public TopUpActivity_ViewBinding(final TopUpActivity var, View var) {
      this.a = var;
      var.toLoadAmountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821125, "field 'toLoadAmountView'", AmountView.class);
      View var = Utils.findRequiredView(var, 2131821124, "field 'decreaseButton' and method 'onDecreaseClicked'");
      var.decreaseButton = (ImageButton)Utils.castView(var, 2131821124, "field 'decreaseButton'", ImageButton.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onDecreaseClicked();
         }
      });
      var = Utils.findRequiredView(var, 2131821126, "field 'increaseButton' and method 'onIncreaseClicked'");
      var.increaseButton = (ImageButton)Utils.castView(var, 2131821126, "field 'increaseButton'", ImageButton.class);
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onIncreaseClicked();
         }
      });
      var.expectedBalanceView = (TextView)Utils.findRequiredViewAsType(var, 2131821127, "field 'expectedBalanceView'", TextView.class);
      var.topUpView = Utils.findRequiredView(var, 2131821122, "field 'topUpView'");
      var.topUpUnavailableView = Utils.findRequiredView(var, 2131821120, "field 'topUpUnavailableView'");
      var.topUpWithCardButton = (Button)Utils.findRequiredViewAsType(var, 2131821129, "field 'topUpWithCardButton'", Button.class);
      var.topUpWithBankButton = (Button)Utils.findRequiredViewAsType(var, 2131821128, "field 'topUpWithBankButton'", Button.class);
      var.overlayView = Utils.findRequiredView(var, 2131821131, "field 'overlayView'");
      var.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821132, "field 'progressBar'", ProgressBar.class);
      var = Utils.findRequiredView(var, 2131821121, "method 'onContactSupportClicked'");
      this.d = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onContactSupportClicked();
         }
      });
   }

   public void unbind() {
      TopUpActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.toLoadAmountView = null;
         var.decreaseButton = null;
         var.increaseButton = null;
         var.expectedBalanceView = null;
         var.topUpView = null;
         var.topUpUnavailableView = null;
         var.topUpWithCardButton = null;
         var.topUpWithBankButton = null;
         var.overlayView = null;
         var.progressBar = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
