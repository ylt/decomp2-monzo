package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.AddStripeCardRequest;

// $FF: synthetic class
final class l implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;

   private l(c var, String var, co.uk.getmondo.d.c var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(c var, String var, co.uk.getmondo.d.c var) {
      return new l(var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, (AddStripeCardRequest)var);
   }
}
