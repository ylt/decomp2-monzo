package co.uk.getmondo.topup.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class p implements Callable {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final boolean d;
   private final String e;

   private p(c var, String var, co.uk.getmondo.d.c var, boolean var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public static Callable a(c var, String var, co.uk.getmondo.d.c var, boolean var, String var) {
      return new p(var, var, var, var, var);
   }

   public Object call() {
      return c.a(this.a, this.b, this.c, this.d, this.e);
   }
}
