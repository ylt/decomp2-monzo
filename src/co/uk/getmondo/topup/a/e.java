package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final ApiThreeDsResponse a;

   private e(ApiThreeDsResponse var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(ApiThreeDsResponse var) {
      return new e(var);
   }

   public Object a(Object var) {
      return c.a(this.a, (ThreeDsResolver.a)var);
   }
}
