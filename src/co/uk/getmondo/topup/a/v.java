package co.uk.getmondo.topup.a;

public enum v implements co.uk.getmondo.common.e.f {
   a("bad_request.card_error", 2131362797),
   b("bad_request.card_error.incorrect_zip", 2131362803),
   c("bad_request.card_error.unsupported_card", 2131362799),
   d("bad_request.card_error.unsupported_card.credit_card", 2131362800),
   e("bad_request.card_error.card_declined", 2131362798),
   f("bad_request.card_error.card_declined.insufficient_funds", 2131362109),
   g("bad_request.duplicate_initial_topup", 2131362801);

   public final int h;
   private final String i;

   private v(String var, int var) {
      this.i = var;
      this.h = var;
   }

   public String a() {
      return this.i;
   }

   public int b() {
      return this.h;
   }
}
