package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.TopUpRequest;

// $FF: synthetic class
final class f implements io.reactivex.c.g {
   private final c a;
   private final TopUpRequest b;

   private f(c var, TopUpRequest var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, TopUpRequest var) {
      return new f(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (Throwable)var);
   }
}
