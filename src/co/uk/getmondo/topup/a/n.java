package co.uk.getmondo.topup.a;

import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final c a;
   private final Card b;
   private final String c;
   private final boolean d;

   private n(c var, Card var, String var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public static io.reactivex.c.h a(c var, Card var, String var, boolean var) {
      return new n(var, var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, this.d, (Token)var);
   }
}
