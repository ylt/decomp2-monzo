package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class s implements io.reactivex.c.g {
   private final c a;
   private final ThreeDSecureRequest b;

   private s(c var, ThreeDSecureRequest var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, ThreeDSecureRequest var) {
      return new s(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (ThreeDsResolver.a)var);
   }
}
