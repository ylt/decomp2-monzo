package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.exception.CardException;

public class w {
   public boolean a(co.uk.getmondo.common.e.e var, Throwable var) {
      boolean var;
      if(var instanceof CardException) {
         var.b(2131362797);
         var = true;
      } else {
         if(var instanceof ApiException) {
            co.uk.getmondo.api.model.b var = ((ApiException)var).e();
            if(var != null) {
               v var = (v)co.uk.getmondo.common.e.d.a(v.values(), var.a());
               if(var != null) {
                  var.b(var.b());
                  var = true;
                  return var;
               }
            }
         }

         if(var instanceof ThreeDsResolver.ThreeDsUnsuccessfulException) {
            switch(null.a[((ThreeDsResolver.ThreeDsUnsuccessfulException)var).a.ordinal()]) {
            case 1:
            case 2:
               var.b(2131362796);
            default:
               var = true;
            }
         } else {
            var = false;
         }
      }

      return var;
   }
}
