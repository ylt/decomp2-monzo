package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.AddStripeCardRequest;

// $FF: synthetic class
final class i implements io.reactivex.c.g {
   private final c a;
   private final AddStripeCardRequest b;

   private i(c var, AddStripeCardRequest var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, AddStripeCardRequest var) {
      return new i(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (Throwable)var);
   }
}
