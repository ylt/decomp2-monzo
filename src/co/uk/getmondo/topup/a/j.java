package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.AddStripeCardRequest;

// $FF: synthetic class
final class j implements io.reactivex.c.g {
   private final c a;
   private final AddStripeCardRequest b;

   private j(c var, AddStripeCardRequest var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, AddStripeCardRequest var) {
      return new j(var, var);
   }

   public void a(Object var) {
      c.b(this.a, this.b, (Throwable)var);
   }
}
