package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;

// $FF: synthetic class
final class r implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final String c;
   private final co.uk.getmondo.d.c d;
   private final boolean e;

   private r(c var, String var, String var, co.uk.getmondo.d.c var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public static io.reactivex.c.h a(c var, String var, String var, co.uk.getmondo.d.c var, boolean var) {
      return new r(var, var, var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, this.d, this.e, (ApiThreeDsResponse)var);
   }
}
