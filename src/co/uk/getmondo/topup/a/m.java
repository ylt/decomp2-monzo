package co.uk.getmondo.topup.a;

import com.stripe.android.model.Card;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class m implements Callable {
   private final c a;
   private final Card b;

   private m(c var, Card var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(c var, Card var) {
      return new m(var, var);
   }

   public Object call() {
      return c.a(this.a, this.b);
   }
}
