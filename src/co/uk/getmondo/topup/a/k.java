package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiStripeCard;

// $FF: synthetic class
final class k implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;

   private k(c var, String var, co.uk.getmondo.d.c var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(c var, String var, co.uk.getmondo.d.c var) {
      return new k(var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, (ApiStripeCard)var);
   }
}
