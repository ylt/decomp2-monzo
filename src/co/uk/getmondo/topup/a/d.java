package co.uk.getmondo.topup.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class d implements Callable {
   private final c a;
   private final String b;
   private final String c;

   private d(c var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static Callable a(c var, String var, String var) {
      return new d(var, var, var);
   }

   public Object call() {
      return c.a(this.a, this.b, this.c);
   }
}
