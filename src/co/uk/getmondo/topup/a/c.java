package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.topup.AddStripeCardRequest;
import co.uk.getmondo.api.model.topup.ApiStripeCard;
import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;
import co.uk.getmondo.api.model.topup.TopUpRequest;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.net.TokenParser;
import io.reactivex.z;
import org.json.JSONException;

public class c {
   private final MonzoApi a;
   private final io.reactivex.u b;
   private final a c;
   private final Stripe d;
   private final c.a e;

   public c(MonzoApi var, io.reactivex.u var, a var, Stripe var, c.a var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(ThreeDSecureRequest var, ApiThreeDsResponse var) throws Exception {
      return new android.support.v4.g.j(var, var);
   }

   // $FF: synthetic method
   static AddStripeCardRequest a(c var, String var, String var) throws Exception {
      return new AddStripeCardRequest(var, var.e.a(var).getId(), "", false);
   }

   // $FF: synthetic method
   static Token a(c var, Card var) throws Exception {
      return var.d.createTokenSynchronous(var);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var, String var, co.uk.getmondo.d.c var, AddStripeCardRequest var) throws Exception {
      return var.a.addStripeCard(var.a(), var.b(), var.c(), var.d(), var.c.a(var)).d(j.a(var, var)).c(k.a(var, var, var));
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var, String var, co.uk.getmondo.d.c var, ApiStripeCard var) throws Exception {
      return var.a((String)var, (String)var.a(), (co.uk.getmondo.d.c)var, false, (ThreeDsResolver)null);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var, String var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var, ApiStripeCard var) throws Exception {
      return var.a(var, var.a(), var, var, var);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var, String var, String var, co.uk.getmondo.d.c var, boolean var, ApiThreeDsResponse var) throws Exception {
      if(var.b()) {
         var = var.d();
      }

      TopUpRequest var = new TopUpRequest(var, var, var.l().b(), var.k(), var, var.c());
      return var.a.topUpThreeDSecure(var.a(), var.b(), var.c(), var.d(), var.e(), var.f(), var.c.a(var)).a(f.a(var, var)).b(var.b);
   }

   private io.reactivex.v a(ApiThreeDsResponse var, ThreeDSecureRequest var, ThreeDsResolver var, boolean var) {
      io.reactivex.v var;
      if(var.a()) {
         var = var.a(var.e(), "https://threeds.monzo.com", var).c(s.a(this, var)).a(e.a(var));
      } else {
         var = io.reactivex.v.a((Object)var);
      }

      return var;
   }

   // $FF: synthetic method
   static z a(ApiThreeDsResponse var, ThreeDsResolver.a var) throws Exception {
      io.reactivex.v var;
      if(var != ThreeDsResolver.a.a) {
         var = io.reactivex.v.a((Throwable)(new ThreeDsResolver.ThreeDsUnsuccessfulException(var)));
      } else {
         var = io.reactivex.v.a((Object)var);
      }

      return var;
   }

   // $FF: synthetic method
   static z a(c var, ThreeDsResolver var, boolean var, android.support.v4.g.j var) throws Exception {
      return var.a((ApiThreeDsResponse)var.a, (ThreeDSecureRequest)var.b, var, var);
   }

   // $FF: synthetic method
   static z a(c var, Card var, String var, boolean var, Token var) throws Exception {
      String var = var.getNumber().substring(0, 6);
      AddStripeCardRequest var = new AddStripeCardRequest(var, var.getId(), var, var);
      return var.a.addStripeCard(var.a(), var.b(), var.c(), var.d(), var.c.a(var)).d(i.a(var, var)).b(var.b);
   }

   // $FF: synthetic method
   static z a(c var, String var, co.uk.getmondo.d.c var, boolean var, String var) throws Exception {
      ThreeDSecureRequest var = new ThreeDSecureRequest(var, var.l().b(), var.k(), var, var, "https://threeds.monzo.com");
      return var.a.threeDSecure(var.a(), var.b(), var.c(), var.d(), var.e(), var.f(), var.c.a(var)).d(g.a(var)).d(h.a(var, var));
   }

   // $FF: synthetic method
   static void a(c var, AddStripeCardRequest var, Throwable var) throws Exception {
      var.a((Throwable)var, (Object)var);
   }

   // $FF: synthetic method
   static void a(c var, ThreeDSecureRequest var, ThreeDsResolver.a var) throws Exception {
      if(var == ThreeDsResolver.a.b) {
         var.c.b(var);
      }

   }

   // $FF: synthetic method
   static void a(c var, ThreeDSecureRequest var, Throwable var) throws Exception {
      var.a((Throwable)var, (Object)var);
   }

   // $FF: synthetic method
   static void a(c var, TopUpRequest var, Throwable var) throws Exception {
      var.a((Throwable)var, (Object)var);
   }

   private boolean a(co.uk.getmondo.api.model.b var) {
      String var;
      if(var != null) {
         var = var.a();
      } else {
         var = "";
      }

      boolean var;
      if(var != null && var.startsWith("bad_request.card_error")) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private boolean a(Throwable var, Object var) {
      boolean var;
      if(var instanceof ApiException && this.a(((ApiException)var).e())) {
         this.c.b(var);
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   // $FF: synthetic method
   static void b(c var, AddStripeCardRequest var, Throwable var) throws Exception {
      var.a((Throwable)var, (Object)var);
   }

   public io.reactivex.b a(String var, Card var, boolean var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      return io.reactivex.v.c(m.a(this, var)).a(n.a(this, var, var, var)).c(o.a(this, var, var, var, var));
   }

   public io.reactivex.b a(String var, String var, co.uk.getmondo.d.c var) {
      return io.reactivex.v.c(d.a(this, var, var)).c(l.a(this, var, var));
   }

   public io.reactivex.b a(String var, String var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      return io.reactivex.v.a(p.a(this, var, var, var, var)).a(q.a(this, var, var)).c(r.a(this, var, var, var, var));
   }

   public static class a {
      public Token a(String var) throws JSONException {
         return TokenParser.parseToken(var);
      }
   }
}
