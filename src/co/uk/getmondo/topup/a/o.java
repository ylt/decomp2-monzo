package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiStripeCard;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class o implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final boolean d;
   private final ThreeDsResolver e;

   private o(c var, String var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public static io.reactivex.c.h a(c var, String var, co.uk.getmondo.d.c var, boolean var, ThreeDsResolver var) {
      return new o(var, var, var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, this.d, this.e, (ApiStripeCard)var);
   }
}
