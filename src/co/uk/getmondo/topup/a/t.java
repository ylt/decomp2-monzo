package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.MonzoApi;
import com.stripe.android.Stripe;

public final class t implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!t.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public t(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new t(var, var, var, var, var);
   }

   public c a() {
      return new c((MonzoApi)this.b.b(), (io.reactivex.u)this.c.b(), (a)this.d.b(), (Stripe)this.e.b(), (c.a)this.f.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
