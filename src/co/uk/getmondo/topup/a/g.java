package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final ThreeDSecureRequest a;

   private g(ThreeDSecureRequest var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(ThreeDSecureRequest var) {
      return new g(var);
   }

   public Object a(Object var) {
      return c.a(this.a, (ApiThreeDsResponse)var);
   }
}
