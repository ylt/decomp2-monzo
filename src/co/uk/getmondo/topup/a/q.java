package co.uk.getmondo.topup.a;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class q implements io.reactivex.c.h {
   private final c a;
   private final ThreeDsResolver b;
   private final boolean c;

   private q(c var, ThreeDsResolver var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(c var, ThreeDsResolver var, boolean var) {
      return new q(var, var, var);
   }

   public Object a(Object var) {
      return c.a(this.a, this.b, this.c, (android.support.v4.g.j)var);
   }
}
