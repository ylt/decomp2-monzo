package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;

// $FF: synthetic class
final class h implements io.reactivex.c.g {
   private final c a;
   private final ThreeDSecureRequest b;

   private h(c var, ThreeDSecureRequest var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, ThreeDSecureRequest var) {
      return new h(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (Throwable)var);
   }
}
