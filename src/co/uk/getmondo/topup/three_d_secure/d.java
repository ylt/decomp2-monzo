package co.uk.getmondo.topup.three_d_secure;

import io.reactivex.c.g;

// $FF: synthetic class
final class d implements g {
   private final b a;
   private final boolean b;

   private d(b var, boolean var) {
      this.a = var;
      this.b = var;
   }

   public static g a(b var, boolean var) {
      return new d(var, var);
   }

   public void a(Object var) {
      b.a(this.a, this.b, (ThreeDsResolver.a)var);
   }
}
