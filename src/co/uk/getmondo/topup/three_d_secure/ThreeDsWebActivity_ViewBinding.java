package co.uk.getmondo.topup.three_d_secure;

import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ThreeDsWebActivity_ViewBinding implements Unbinder {
   private ThreeDsWebActivity a;

   public ThreeDsWebActivity_ViewBinding(ThreeDsWebActivity var, View var) {
      this.a = var;
      var.webView = (WebView)Utils.findRequiredViewAsType(var, 2131821117, "field 'webView'", WebView.class);
   }

   public void unbind() {
      ThreeDsWebActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.webView = null;
      }
   }
}
