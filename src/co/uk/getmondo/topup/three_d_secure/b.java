package co.uk.getmondo.topup.three_d_secure;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.v;
import io.reactivex.w;

public class b implements ThreeDsResolver {
   private Activity a;
   private Fragment b;
   private co.uk.getmondo.common.a c;
   private w d;

   public b(Activity var, co.uk.getmondo.common.a var) {
      this.a = var;
      this.c = var;
   }

   public b(Fragment var, co.uk.getmondo.common.a var) {
      this.b = var;
      this.c = var;
   }

   private void a(ThreeDsResolver.a var, boolean var) {
      if(var != null) {
         switch(null.a[var.ordinal()]) {
         case 1:
            this.c.a(Impression.d(var));
            break;
         case 2:
            this.c.a(Impression.c(var));
         }
      }

   }

   // $FF: synthetic method
   static void a(b var) throws Exception {
      var.d = null;
   }

   // $FF: synthetic method
   static void a(b var, boolean var, ThreeDsResolver.a var) throws Exception {
      var.a(var, var);
      var.c.a(Impression.e(var));
   }

   // $FF: synthetic method
   static void a(b var, boolean var, String var, String var, w var) throws Exception {
      var.c.a(Impression.b(var));
      Intent var;
      if(var.b != null) {
         var = ThreeDsWebActivity.a(var.b.getContext(), var, var, var);
         var.b.startActivityForResult(var, 501);
      } else {
         if(var.a == null) {
            throw new IllegalArgumentException("ThreeDsWebResolver requires a Fragment or Activity to launch ThreeDsWebActivity");
         }

         var = ThreeDsWebActivity.a(var.a, var, var, var);
         var.a.startActivityForResult(var, 501);
      }

      var.d = var;
      var.a(e.a(var));
   }

   public v a(String var, String var, boolean var) {
      return v.a(c.a(this, var, var, var)).c(d.a(this, var)).b(io.reactivex.a.b.a.a());
   }

   public boolean a(int param1, int param2, Intent param3) {
      // $FF: Couldn't be decompiled
   }
}
