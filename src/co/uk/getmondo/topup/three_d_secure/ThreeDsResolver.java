package co.uk.getmondo.topup.three_d_secure;

import android.content.Intent;
import io.reactivex.v;

public interface ThreeDsResolver {
   v a(String var, String var, boolean var);

   boolean a(int var, int var, Intent var);

   public static class ThreeDsUnsuccessfulException extends RuntimeException {
      public final ThreeDsResolver.a a;

      public ThreeDsUnsuccessfulException(ThreeDsResolver.a var) {
         super("Problem resolving 3DS redirection. Result=" + var);
         this.a = var;
      }
   }

   public static enum a {
      a,
      b,
      c,
      d;
   }
}
