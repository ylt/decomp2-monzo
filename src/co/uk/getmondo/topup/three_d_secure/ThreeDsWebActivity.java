package co.uk.getmondo.topup.three_d_secure;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;

public class ThreeDsWebActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   private Uri b;
   private String c;
   @BindView(2131821117)
   WebView webView;

   public static Intent a(Context var, String var, String var, boolean var) {
      Intent var = new Intent(var, ThreeDsWebActivity.class);
      var.putExtra("EXTRA_REDIRECT_URL", var);
      var.putExtra("EXTRA_RETURN_URL", var);
      var.putExtra("EXTRA_INITIAL_TOP_UP", var);
      return var;
   }

   public static ThreeDsResolver.a a(Intent var) {
      ThreeDsResolver.a var;
      if(!var.hasExtra("status")) {
         var = ThreeDsResolver.a.d;
      } else {
         String var = var.getStringExtra("status");
         byte var = -1;
         switch(var.hashCode()) {
         case -1281977283:
            if(var.equals("failed")) {
               var = 1;
            }
            break;
         case 945734241:
            if(var.equals("succeeded")) {
               var = 0;
            }
         }

         switch(var) {
         case 0:
            var = ThreeDsResolver.a.a;
            break;
         case 1:
            var = ThreeDsResolver.a.b;
            break;
         default:
            d.a.a.d("Unknown 3DS status found, status=%s", new Object[]{var});
            var = ThreeDsResolver.a.d;
         }
      }

      return var;
   }

   private void a(Uri var) {
      String var = var.getQueryParameter("status");
      Intent var = new Intent();
      var.putExtra("status", var);
      this.setResult(-1, var);
      this.finish();
   }

   protected void o() {
      this.finish();
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.c = this.getIntent().getStringExtra("EXTRA_REDIRECT_URL");
      boolean var = this.getIntent().getBooleanExtra("EXTRA_INITIAL_TOP_UP", false);
      this.b = Uri.parse(this.getIntent().getStringExtra("EXTRA_RETURN_URL"));
      if(this.c != null && this.b != null) {
         this.setContentView(2131034218);
         ButterKnife.bind((Activity)this);
         this.l().a(this);
         android.support.v7.app.a var = this.getSupportActionBar();
         if(var != null) {
            var.b(true);
            var.a(2130837836);
         }

         d.a.a.c("Redirecting to url %s", new Object[]{this.c});
         this.webView.getSettings().setJavaScriptEnabled(true);
         this.webView.setWebViewClient(new ThreeDsWebActivity.a());
         this.a.a(Impression.a(var, this.c));
         this.webView.postUrl(this.c, (byte[])null);
      } else {
         throw new RuntimeException("Redirect url and return url are required");
      }
   }

   private class a extends WebViewClient {
      private a() {
      }

      // $FF: synthetic method
      a(Object var) {
         this();
      }

      public void onReceivedError(WebView var, int var, String var, String var) {
         super.onReceivedError(var, var, var, var);
         String var = "generic_error_dep, code: " + var + ", description: " + var + ", failingUrl: " + var;
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var));
      }

      public void onReceivedError(WebView var, WebResourceRequest var, WebResourceError var) {
         super.onReceivedError(var, var, var);
         String var = "generic_error";
         if(VERSION.SDK_INT >= 23) {
            var = "generic_error" + ", code: " + var.getErrorCode() + ", description: " + var.getDescription();
         }

         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var));
      }

      public void onReceivedHttpError(WebView var, WebResourceRequest var, WebResourceResponse var) {
         super.onReceivedHttpError(var, var, var);
         String var = "http_error, code: " + var.getStatusCode() + " " + var.getReasonPhrase();
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var));
      }

      public void onReceivedSslError(WebView var, SslErrorHandler var, SslError var) {
         super.onReceivedSslError(var, var, var);
         String var = "ssl_error: " + var.toString();
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var));
      }

      public boolean shouldOverrideUrlLoading(WebView var, String var) {
         boolean var = true;
         Uri var = Uri.parse(var);
         if(var.getHost().equals(ThreeDsWebActivity.this.b.getHost())) {
            d.a.a.c("3DS return url intercepted: %s", new Object[]{var});
            ThreeDsWebActivity.this.a(var);
         } else {
            var = false;
         }

         return var;
      }
   }
}
