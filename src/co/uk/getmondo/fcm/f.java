package co.uk.getmondo.fcm;

import android.content.Context;
import io.intercom.android.sdk.push.IntercomPushClient;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new f(var, var);
   }

   public e a() {
      return new e((Context)this.b.b(), (IntercomPushClient)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
