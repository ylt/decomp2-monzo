package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.splash.SplashActivity;
import java.io.Serializable;

public abstract class d implements Serializable {
   static final Uri a = RingtoneManager.getDefaultUri(2);
   protected final int b;
   protected final NotificationMessage c;

   d(int var, NotificationMessage var) {
      this.b = var;
      this.c = var;
   }

   public PendingIntent a(Context var, ak.a var) {
      Intent var;
      if(var == ak.a.HAS_ACCOUNT) {
         var = HomeActivity.c(var);
      } else {
         var = SplashActivity.b(var);
      }

      return PendingIntent.getActivity(var, this.b(), var, 134217728);
   }

   public Uri a() {
      return a;
   }

   public int b() {
      return this.b;
   }

   public NotificationMessage c() {
      return this.c;
   }
}
