package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import co.uk.getmondo.signup.identity_verification.a.j;

public class c extends d {
   private final String d;

   public c(String var, NotificationMessage var, String var) {
      super(var.hashCode(), var);
      this.d = var;
   }

   public PendingIntent a(Context var, ak.a var) {
      PendingIntent var;
      if(var != ak.a.HAS_ACCOUNT) {
         var = super.a(var, var);
      } else {
         Intent var;
         if(this.d.equals("kyc_request")) {
            var = IdentityVerificationActivity.a(var, j.a, Impression.KycFrom.FEED, SignupSource.LEGACY_PREPAID);
         } else {
            if(!this.d.equals("kyc_passed")) {
               throw new IllegalArgumentException("Unexpected KYC subtype found: " + this.d);
            }

            var = IdentityApprovedActivity.b(var);
         }

         var = TaskStackBuilder.create(var).addNextIntentWithParentStack(var).getPendingIntent(this.b(), 134217728);
      }

      return var;
   }
}
