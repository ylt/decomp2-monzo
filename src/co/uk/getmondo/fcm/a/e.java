package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.net.Uri;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;

public class e extends d {
   private static final Uri d = Uri.parse("android.resource://co.uk.getmondo/2131296264");
   private static final Uri e = Uri.parse("android.resource://co.uk.getmondo/2131296263");
   private final String f;
   private final String g;

   public e(NotificationMessage var, String var, String var) {
      super(var.hashCode(), var);
      this.f = var;
      this.g = var;
   }

   public PendingIntent a(Context var, ak.a var) {
      PendingIntent var;
      if(var != ak.a.HAS_ACCOUNT) {
         var = super.a(var, var);
      } else {
         var = TaskStackBuilder.create(var).addNextIntentWithParentStack(TransactionDetailsActivity.a(var, this.f)).getPendingIntent(this.b(), 134217728);
      }

      return var;
   }

   public Uri a() {
      Uri var;
      if(!this.g.equals("transaction_topup") && !this.g.equals("transaction_credit")) {
         if(!this.g.equals("transaction_decline")) {
            var = d;
         } else {
            var = a;
         }
      } else {
         var = e;
      }

      return var;
   }
}
