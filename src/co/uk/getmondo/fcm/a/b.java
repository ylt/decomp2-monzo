package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;

public class b extends d {
   private final String d;

   public b(int var, NotificationMessage var, String var) {
      super(var, var);
      this.d = var;
   }

   public PendingIntent a(Context var, ak.a var) {
      Intent var = new Intent();
      var.setComponent(new ComponentName("co.uk.getmondo.ghost", "co.uk.getmondo.ghost.GhostLoginActivity"));
      var.putExtra("KEY_EXTRA_ACCESS_TOKEN", this.d);
      PendingIntent var;
      if(var.resolveActivity(var.getPackageManager()) == null) {
         var = super.a(var, var);
      } else {
         var = PendingIntent.getActivity(var, this.b(), var, 134217728);
      }

      return var;
   }
}
