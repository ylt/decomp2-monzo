package co.uk.getmondo.fcm;

import android.app.job.JobParameters;
import android.app.job.JobService;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import io.reactivex.u;
import java.io.IOException;

public class FcmRegistrationJobService extends JobService {
   u a;
   MonzoApi b;
   private io.reactivex.b.b c;

   // $FF: synthetic method
   static void a(FcmRegistrationJobService var, JobParameters var) throws Exception {
      d.a.a.a("Successfully registered for FCM messaging", new Object[0]);
      var.jobFinished(var, false);
   }

   // $FF: synthetic method
   static void a(FcmRegistrationJobService var, JobParameters var, Throwable var) throws Exception {
      boolean var;
      label21: {
         d.a.a.a((Throwable)(new RuntimeException("Failure in FCM registration", var)));
         if(var instanceof IOException) {
            if(!(var instanceof ApiException)) {
               var = true;
               break label21;
            }

            if(((ApiException)var).b() >= 500) {
               var = true;
               break label21;
            }
         }

         var = false;
      }

      if(var) {
         d.a.a.a("Error with FCM registration, rescheduling", new Object[0]);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Giving up trying to register for FCM", var)));
      }

      var.jobFinished(var, var);
   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
   }

   public void onDestroy() {
      if(this.c != null && !this.c.isDisposed()) {
         this.c.dispose();
      }

      super.onDestroy();
   }

   public boolean onStartJob(JobParameters var) {
      String var = var.getExtras().getString("KEY_TOKEN");
      this.c = this.b.registerForFcm(var, "co.uk.getmondo").b(this.a).a(a.a(this, var), b.a(this, var));
      return true;
   }

   public boolean onStopJob(JobParameters var) {
      if(this.c != null && !this.c.isDisposed()) {
         this.c.dispose();
      }

      return true;
   }
}
