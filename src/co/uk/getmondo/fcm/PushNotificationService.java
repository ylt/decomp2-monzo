package co.uk.getmondo.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.Build.VERSION;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.background_sync.SyncFeedAndBalanceJobService;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.settings.k;
import co.uk.getmondo.splash.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.l;
import com.google.gson.n;
import io.intercom.android.sdk.push.IntercomPushClient;
import java.util.Map;

public class PushNotificationService extends FirebaseMessagingService {
   com.google.gson.f a;
   co.uk.getmondo.common.accounts.d b;
   IntercomPushClient c;
   k d;
   co.uk.getmondo.common.a e;
   private NotificationManager f;
   private int g;

   private co.uk.getmondo.fcm.a.d a(n var, String var, NotificationMessage var) {
      byte var = -1;
      switch(var.hashCode()) {
      case -1170057799:
         if(var.equals("ghost_login")) {
            var = 2;
         }
         break;
      case 106677:
         if(var.equals("kyc")) {
            var = 1;
         }
         break;
      case 2141246174:
         if(var.equals("transaction")) {
            var = 0;
         }
      }

      Object var;
      switch(var) {
      case 0:
         var = new co.uk.getmondo.fcm.a.e(var, var.c("transaction_id").b(), var.c("sub_type").b());
         break;
      case 1:
         var = new co.uk.getmondo.fcm.a.c(var.c("feed_item_id").b(), var, var.c("sub_type").b());
         break;
      case 2:
         var = new co.uk.getmondo.fcm.a.b(2, var, var.c("access_token").b());
         break;
      default:
         int var = this.g;
         this.g = var + 1;
         var = new co.uk.getmondo.fcm.a.a(var, var);
      }

      return (co.uk.getmondo.fcm.a.d)var;
   }

   private void a(co.uk.getmondo.fcm.a.d var, ak.a var) {
      if(this.d.a()) {
         NotificationMessage var = var.c();
         android.support.v4.app.ab.c var = (new android.support.v4.app.ab.c(this)).a(2130837871).e(android.support.v4.content.a.c(this, 2131689526)).a(var.a(this, var)).a(true).a(var.a());
         if(p.d(var.a()) && p.d(var.b())) {
            if(VERSION.SDK_INT < 24) {
               var.a(this.getString(2131362012));
            }

            var.b(var.c()).a((new android.support.v4.app.ab.b()).a(var.c()));
         } else if(p.d(var.c())) {
            var.a(var.a()).b(var.b()).a((new android.support.v4.app.ab.b()).a(var.b()));
         } else {
            var.a(var.a()).b(var.b()).a((new android.support.v4.app.ab.e()).a(var.a()).b(var.b()).b(var.c()));
         }

         if(VERSION.SDK_INT >= 24) {
            var.b("summary_group");
            this.f.notify(var.b(), var.a());
            int var = this.f.getActiveNotifications().length;
            if(var > 1) {
               PendingIntent var = PendingIntent.getActivity(this, 1, SplashActivity.b(this), 134217728);
               android.support.v4.app.ab.c var = (new android.support.v4.app.ab.c(this)).a(2130837871).b("summary_group").b(var).a(true).a(var).e(android.support.v4.content.a.c(this, 2131689526)).c(true);
               this.f.notify(1, var.a());
            }
         } else {
            this.f.notify(var.b(), var.a());
         }
      }

   }

   private NotificationMessage b() {
      return new NotificationMessage("", "", this.getString(2131362500));
   }

   private NotificationMessage b(String param1) {
      // $FF: Couldn't be decompiled
   }

   private NotificationMessage c(String var) {
      NotificationMessage var;
      if(p.c(var) && var.contains("\n")) {
         String[] var = var.split("\\r?\\n");
         if(var.length > 1) {
            var = new NotificationMessage(d(var[0]), d(var[1]), "");
            return var;
         }
      }

      var = new NotificationMessage("", "", var);
      return var;
   }

   private static String d(String var) {
      String var = var;
      if(var.endsWith(".")) {
         var = var.substring(0, var.length() - 1);
      }

      return var;
   }

   public void a(com.google.firebase.messaging.a var) {
      d.a.a.b("Push notification received", new Object[0]);
      Map var = var.a();
      if(this.c.isIntercomPush(var)) {
         this.c.handlePush(this.getApplication(), var);
      } else if(var.containsKey("payload")) {
         n var = ((l)this.a.a((String)var.get("payload"), l.class)).k();
         String var = var.c("type").b();
         String var;
         if(var.c("sub_type") == null) {
            var = null;
         } else {
            var = var.c("sub_type").b();
         }

         this.e.a(Impression.c(var, var));
         co.uk.getmondo.fcm.a.d var = this.a(var, var, this.b((String)var.get("alert")));
         ak var = this.b.b();
         ak.a var;
         if(var != null) {
            var = var.a();
         } else {
            var = ak.a.NO_PROFILE;
         }

         if(var == ak.a.HAS_ACCOUNT) {
            SyncFeedAndBalanceJobService.a(this);
         }

         this.a(var, var);
      }

   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
      this.f = (NotificationManager)this.getSystemService("notification");
   }
}
