package co.uk.getmondo.fcm;

import android.app.job.JobParameters;

// $FF: synthetic class
final class a implements io.reactivex.c.a {
   private final FcmRegistrationJobService a;
   private final JobParameters b;

   private a(FcmRegistrationJobService var, JobParameters var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.a a(FcmRegistrationJobService var, JobParameters var) {
      return new a(var, var);
   }

   public void a() {
      FcmRegistrationJobService.a(this.a, this.b);
   }
}
