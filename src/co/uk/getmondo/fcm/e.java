package co.uk.getmondo.fcm;

import android.app.job.JobScheduler;
import android.app.job.JobInfo.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.k.p;
import com.google.firebase.iid.FirebaseInstanceId;
import io.intercom.android.sdk.push.IntercomPushClient;

public class e {
   private final Context a;
   private final IntercomPushClient b;

   e(Context var, IntercomPushClient var) {
      this.a = var;
      this.b = var;
   }

   public void a() {
      if(!co.uk.getmondo.a.b.booleanValue()) {
         String var = FirebaseInstanceId.a().c();
         if(p.c(var)) {
            this.b.sendTokenToIntercom(MonzoApplication.a(this.a), var);
            ComponentName var = new ComponentName(this.a, FcmRegistrationJobService.class);
            PersistableBundle var = new PersistableBundle();
            var.putString("KEY_TOKEN", var);
            ((JobScheduler)this.a.getSystemService("jobscheduler")).schedule((new Builder(0, var)).setPersisted(true).setRequiredNetworkType(1).setExtras(var).build());
         }
      }

   }
}
