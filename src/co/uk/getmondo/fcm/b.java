package co.uk.getmondo.fcm;

import android.app.job.JobParameters;

// $FF: synthetic class
final class b implements io.reactivex.c.g {
   private final FcmRegistrationJobService a;
   private final JobParameters b;

   private b(FcmRegistrationJobService var, JobParameters var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(FcmRegistrationJobService var, JobParameters var) {
      return new b(var, var);
   }

   public void a(Object var) {
      FcmRegistrationJobService.a(this.a, this.b, (Throwable)var);
   }
}
