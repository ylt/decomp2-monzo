package co.uk.getmondo.fcm;

import co.uk.getmondo.settings.k;
import io.intercom.android.sdk.push.IntercomPushClient;

public final class g implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new g(var, var, var, var, var);
   }

   public void a(PushNotificationService var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (com.google.gson.f)this.b.b();
         var.b = (co.uk.getmondo.common.accounts.d)this.c.b();
         var.c = (IntercomPushClient)this.d.b();
         var.d = (k)this.e.b();
         var.e = (co.uk.getmondo.common.a)this.f.b();
      }
   }
}
