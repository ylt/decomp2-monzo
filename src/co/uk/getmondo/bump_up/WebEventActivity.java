package co.uk.getmondo.bump_up;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;

public class WebEventActivity extends co.uk.getmondo.common.activities.h implements d.a {
   co.uk.getmondo.common.a a;
   d b;
   co.uk.getmondo.common.k c;
   private final com.b.b.c e = com.b.b.c.a();
   @BindView(2131821179)
   WebView eventWebView;
   private co.uk.getmondo.d.k f;

   public static void a(Activity var, co.uk.getmondo.d.k var) {
      Intent var = new Intent(var, WebEventActivity.class);
      var.putExtra(co.uk.getmondo.d.k.class.getSimpleName(), var);
      var.startActivity(var);
   }

   protected void a() {
      if(this.f != null && this.f.a()) {
         this.a.a(Impression.b(this.f.c()));
      }

      super.a();
   }

   public void a(b var) {
      this.a.a(Impression.d(var.b()));
      this.c.a((Context)this, (String)var.b());
   }

   public n b() {
      return this.e;
   }

   public void b(b var) {
      this.a.a(Impression.c(var.b()));
      this.startActivity(co.uk.getmondo.common.k.j.a(this, var.a(), var.b(), co.uk.getmondo.api.model.tracking.a.WAITING_LIST_WEB_EVENT));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034230);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      if(!this.getIntent().hasExtra(co.uk.getmondo.d.k.class.getSimpleName())) {
         this.finish();
      } else {
         this.b.a((d.a)this);
         this.f = (co.uk.getmondo.d.k)this.getIntent().getSerializableExtra(co.uk.getmondo.d.k.class.getSimpleName());
         this.eventWebView.getSettings().setJavaScriptEnabled(true);
         this.eventWebView.setWebViewClient(new WebViewClient());
         this.eventWebView.addJavascriptInterface(new WebEventActivity.a(), "mondo");
         this.eventWebView.loadUrl(this.f.c());
         this.eventWebView.setBackgroundColor(0);
         this.a.a(Impression.a(this.f.c()));
      }

   }

   protected void onDestroy() {
      this.b.b();
      super.onDestroy();
   }

   public class a {
      @JavascriptInterface
      public void postMessage(String var) {
         WebEventActivity.this.e.a((Object)var);
      }
   }
}
