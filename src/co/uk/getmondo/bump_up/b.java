package co.uk.getmondo.bump_up;

public class b {
   private String a;
   private String b;
   private String c;

   public String a() {
      return this.c;
   }

   public void a(String var) {
      this.b = var;
   }

   public String b() {
      return this.b;
   }

   public boolean c() {
      return "share".equals(this.a);
   }

   public boolean d() {
      return "safari".equals(this.a);
   }

   public boolean e() {
      return "USER_SHARE_URL".equalsIgnoreCase(this.b);
   }

   public String toString() {
      return "Message{action=" + this.a + ", text=" + this.c + ", url=" + this.b + "}";
   }
}
