package co.uk.getmondo.bump_up;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class BumpUpActivity extends co.uk.getmondo.common.activities.h {
   co.uk.getmondo.common.accounts.d a;
   private co.uk.getmondo.d.k b;
   @BindView(2131820829)
   AnimatedNumberView eventBumpText;
   @BindView(2131820828)
   TextView eventMessageText;

   public static int a(Intent var) {
      int var = 0;
      if(var != null) {
         int var;
         try {
            var = var.getIntExtra("BUMP_EXTRA", 0);
         } catch (Exception var) {
            return var;
         }

         var = var;
      }

      return var;
   }

   public static void a(Activity var, co.uk.getmondo.d.k var) {
      Intent var = new Intent(var, BumpUpActivity.class);
      var.putExtra(co.uk.getmondo.d.k.class.getSimpleName(), var);
      var.startActivityForResult(var, 11323);
   }

   public static boolean a(int var) {
      boolean var;
      if(11323 == var) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void finish() {
      if(this.b != null) {
         Intent var = new Intent();
         var.putExtra("BUMP_EXTRA", this.b.d());
         this.setResult(-1, var);
      }

      super.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034148);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      if(!this.getIntent().hasExtra(co.uk.getmondo.d.k.class.getSimpleName())) {
         this.finish();
      } else {
         this.b = (co.uk.getmondo.d.k)this.getIntent().getSerializableExtra(co.uk.getmondo.d.k.class.getSimpleName());
         this.eventBumpText.setNumber(this.b.d());
         this.eventMessageText.setText(this.b.c());
      }

   }

   @OnClick({2131820830})
   public void onShareMoreInvites() {
      String var = this.getString(2131362012);
      this.startActivity(Intent.createChooser(co.uk.getmondo.common.k.j.a(this.a.b().e().g()), var));
   }
}
