package co.uk.getmondo.bump_up;

public final class c implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new c(var, var, var);
   }

   public void a(WebEventActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (co.uk.getmondo.common.a)this.b.b();
         var.b = (d)this.c.b();
         var.c = (co.uk.getmondo.common.k)this.d.b();
      }
   }
}
