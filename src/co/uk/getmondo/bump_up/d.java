package co.uk.getmondo.bump_up;

import co.uk.getmondo.d.ak;
import io.reactivex.l;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.z;

class d extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.accounts.d c;
   private final com.google.gson.f d;

   d(co.uk.getmondo.common.accounts.d var, com.google.gson.f var) {
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   static b a(b var, ak var) throws Exception {
      if(var.e() && var.e() != null) {
         var.a(var.e().g());
      }

      return var;
   }

   // $FF: synthetic method
   static b a(d var, String var) throws Exception {
      return (b)var.d.a(var, b.class);
   }

   private v a(String var) {
      return v.c(i.a(this, var));
   }

   // $FF: synthetic method
   static z a(d var, b var) throws Exception {
      return var.c.c().d(j.a(var));
   }

   // $FF: synthetic method
   static void a(d.a var, b var) throws Exception {
      if(var.d()) {
         var.a(var);
      } else if(var.c()) {
         var.b(var);
      }

   }

   // $FF: synthetic method
   static l b(d var, String var) throws Exception {
      return var.a(var).e().a((l)io.reactivex.h.a());
   }

   public void a(d.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.a((io.reactivex.b.b)var.b().flatMapMaybe(e.a(this)).flatMapSingle(f.a(this)).subscribe(g.a(var), h.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(b var);

      n b();

      void b(b var);
   }
}
