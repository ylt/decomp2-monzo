package co.uk.getmondo.bump_up;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!k.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public k(b.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var) {
      return new k(var, var, var);
   }

   public d a() {
      return (d)b.a.c.a(this.b, new d((co.uk.getmondo.common.accounts.d)this.c.b(), (com.google.gson.f)this.d.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
