package co.uk.getmondo.bump_up;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class i implements Callable {
   private final d a;
   private final String b;

   private i(d var, String var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(d var, String var) {
      return new i(var, var);
   }

   public Object call() {
      return d.a(this.a, this.b);
   }
}
