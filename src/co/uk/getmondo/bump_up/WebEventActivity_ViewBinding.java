package co.uk.getmondo.bump_up;

import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class WebEventActivity_ViewBinding implements Unbinder {
   private WebEventActivity a;

   public WebEventActivity_ViewBinding(WebEventActivity var, View var) {
      this.a = var;
      var.eventWebView = (WebView)Utils.findRequiredViewAsType(var, 2131821179, "field 'eventWebView'", WebView.class);
   }

   public void unbind() {
      WebEventActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.eventWebView = null;
      }
   }
}
