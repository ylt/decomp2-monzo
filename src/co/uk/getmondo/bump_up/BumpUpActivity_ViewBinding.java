package co.uk.getmondo.bump_up;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class BumpUpActivity_ViewBinding implements Unbinder {
   private BumpUpActivity a;
   private View b;

   public BumpUpActivity_ViewBinding(final BumpUpActivity var, View var) {
      this.a = var;
      var.eventBumpText = (AnimatedNumberView)Utils.findRequiredViewAsType(var, 2131820829, "field 'eventBumpText'", AnimatedNumberView.class);
      var.eventMessageText = (TextView)Utils.findRequiredViewAsType(var, 2131820828, "field 'eventMessageText'", TextView.class);
      var = Utils.findRequiredView(var, 2131820830, "method 'onShareMoreInvites'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onShareMoreInvites();
         }
      });
   }

   public void unbind() {
      BumpUpActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.eventBumpText = null;
         var.eventMessageText = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
