package co.uk.getmondo.help;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001#B?\u0012\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007\u0012\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u001c\u0010\u001b\u001a\u00020\t2\n\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u001c\u0010\u001d\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0016H\u0016J\u0014\u0010!\u001a\u00020\t2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\"R\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R(\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006$"},
   d2 = {"Lco/uk/getmondo/help/HelpSectionAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/help/HelpSectionAdapter$ItemViewHolder;", "items", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "topicClickListener", "Lkotlin/Function1;", "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "", "helpClickListener", "Lkotlin/Function0;", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "getHelpClickListener", "()Lkotlin/jvm/functions/Function0;", "setHelpClickListener", "(Lkotlin/jvm/functions/Function0;)V", "getTopicClickListener", "()Lkotlin/jvm/functions/Function1;", "setTopicClickListener", "(Lkotlin/jvm/functions/Function1;)V", "getItemCount", "", "getItemId", "", "position", "getItemViewType", "onBindViewHolder", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setItems", "", "ItemViewHolder", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class m extends android.support.v7.widget.RecyclerView.a {
   private final List a;
   private kotlin.d.a.b b;
   private kotlin.d.a.a c;

   public m() {
      this((List)null, (kotlin.d.a.b)null, (kotlin.d.a.a)null, 7, (kotlin.d.b.i)null);
   }

   public m(List var, kotlin.d.a.b var, kotlin.d.a.a var) {
      kotlin.d.b.l.b(var, "items");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.setHasStableIds(true);
   }

   // $FF: synthetic method
   public m(List var, kotlin.d.a.b var, kotlin.d.a.a var, int var, kotlin.d.b.i var) {
      if((var & 1) != 0) {
         var = (List)(new ArrayList());
      }

      if((var & 2) != 0) {
         var = (kotlin.d.a.b)null;
      }

      if((var & 4) != 0) {
         var = (kotlin.d.a.a)null;
      }

      this(var, var, var);
   }

   public m.a a(ViewGroup var, int var) {
      kotlin.d.b.l.b(var, "parent");
      return new m.a(co.uk.getmondo.common.ui.m.a(var, var, false, 2, (Object)null));
   }

   public final kotlin.d.a.b a() {
      return this.b;
   }

   public void a(m.a var, int var) {
      kotlin.d.b.l.b(var, "holder");
      var.a((co.uk.getmondo.help.a.a.d)this.a.get(var));
   }

   public final void a(List var) {
      kotlin.d.b.l.b(var, "items");
      this.a.clear();
      this.a.addAll((Collection)var);
      this.notifyDataSetChanged();
   }

   public final void a(kotlin.d.a.a var) {
      this.c = var;
   }

   public final void a(kotlin.d.a.b var) {
      this.b = var;
   }

   public final kotlin.d.a.a b() {
      return this.c;
   }

   public int getItemCount() {
      return this.a.size();
   }

   public long getItemId(int var) {
      return ((co.uk.getmondo.help.a.a.d)this.a.get(var)).a();
   }

   public int getItemViewType(int var) {
      return ((co.uk.getmondo.help.a.a.d)this.a.get(var)).b();
   }

   // $FF: synthetic method
   public void onBindViewHolder(w var, int var) {
      this.a((m.a)var, var);
   }

   // $FF: synthetic method
   public w onCreateViewHolder(ViewGroup var, int var) {
      return (w)this.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/help/HelpSectionAdapter$ItemViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lco/uk/getmondo/help/HelpSectionAdapter;Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "sectionItem", "Lco/uk/getmondo/help/data/model/SectionItem;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class a extends w {
      private final View b;

      public a(View var) {
         kotlin.d.b.l.b(var, "view");
         super(var);
         this.b = var;
         this.b.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               co.uk.getmondo.help.a.a.d var = (co.uk.getmondo.help.a.a.d)m.this.a.get(a.this.getAdapterPosition());
               kotlin.n var;
               if(var instanceof co.uk.getmondo.help.a.a.d.c) {
                  kotlin.d.a.b var = m.this.a();
                  if(var != null) {
                     var = (kotlin.n)var.a(var);
                  }
               } else if(var instanceof co.uk.getmondo.help.a.a.d.a) {
                  kotlin.d.a.a var = m.this.b();
                  if(var != null) {
                     var = (kotlin.n)var.v_();
                  }
               }

            }
         }));
      }

      public final void a(co.uk.getmondo.help.a.a.d var) {
         kotlin.d.b.l.b(var, "sectionItem");
         if(var instanceof co.uk.getmondo.help.a.a.d.b) {
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.categorySectionTitle)).setText((CharSequence)((co.uk.getmondo.help.a.a.d.b)var).c());
         } else if(var instanceof co.uk.getmondo.help.a.a.d.c) {
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.categoryTopicTitle)).setText((CharSequence)((co.uk.getmondo.help.a.a.d.c)var).c().b());
            int var = this.getAdapterPosition() + 1;
            if(var <= m.this.a.size() && !(m.this.a.get(var) instanceof co.uk.getmondo.help.a.a.d.c)) {
               ae.b(this.b.findViewById(co.uk.getmondo.c.a.divider));
            } else {
               ae.a(this.b.findViewById(co.uk.getmondo.c.a.divider));
            }
         } else if(var instanceof co.uk.getmondo.help.a.a.d.a) {
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.categoryHelpTitle)).setText((CharSequence)((co.uk.getmondo.help.a.a.d.a)var).c());
         }

      }
   }
}
