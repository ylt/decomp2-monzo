package co.uk.getmondo.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.util.Linkify;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.api.model.help.Topic;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0001)B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u001e\u001a\u00020\u00062\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\b\u0010!\u001a\u00020\u0006H\u0016J\b\u0010\"\u001a\u00020\u0006H\u0016J\u0010\u0010#\u001a\u00020\u00062\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\u00062\u0006\u0010'\u001a\u00020(H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R#\u0010\u0017\u001a\n \u0019*\u0004\u0018\u00010\u00180\u00188BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001a\u0010\u001b¨\u0006*"},
   d2 = {"Lco/uk/getmondo/help/HelpTopicActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/help/HelpTopicPresenter$View;", "()V", "feedbackNegativeClicked", "Lio/reactivex/Observable;", "", "getFeedbackNegativeClicked", "()Lio/reactivex/Observable;", "feedbackPositiveClicked", "getFeedbackPositiveClicked", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "presenter", "Lco/uk/getmondo/help/HelpTopicPresenter;", "getPresenter", "()Lco/uk/getmondo/help/HelpTopicPresenter;", "setPresenter", "(Lco/uk/getmondo/help/HelpTopicPresenter;)V", "topicViewModel", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "kotlin.jvm.PlatformType", "getTopicViewModel", "()Lco/uk/getmondo/help/data/model/TopicViewModel;", "topicViewModel$delegate", "Lkotlin/Lazy;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "openChat", "openHelpHome", "setTitle", "title", "", "showTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HelpTopicActivity extends co.uk.getmondo.common.activities.b implements o.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(HelpTopicActivity.class), "topicViewModel", "getTopicViewModel()Lco/uk/getmondo/help/data/model/TopicViewModel;"))};
   public static final HelpTopicActivity.a e = new HelpTopicActivity.a((kotlin.d.b.i)null);
   public o b;
   public co.uk.getmondo.common.q c;
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.help.a.a.e b() {
         return (co.uk.getmondo.help.a.a.e)HelpTopicActivity.this.getIntent().getParcelableExtra("topic_vm");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap g;

   private final co.uk.getmondo.help.a.a.e e() {
      kotlin.c var = this.f;
      kotlin.reflect.l var = a[0];
      return (co.uk.getmondo.help.a.a.e)var.a();
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.a((LinearLayout)this.a(co.uk.getmondo.c.a.topicFeedbackPositive)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(Topic var) {
      kotlin.d.b.l.b(var, "topic");
      ((TextView)this.a(co.uk.getmondo.c.a.topicTitle)).setText((CharSequence)var.b());
      ((TextView)this.a(co.uk.getmondo.c.a.topicContent)).setText((CharSequence)var.c());
      Linkify.addLinks((TextView)this.a(co.uk.getmondo.c.a.topicContent), 15);
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "title");
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.a((CharSequence)var);
      }

   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((LinearLayout)this.a(co.uk.getmondo.c.a.topicFeedbackNegative)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void c() {
      String var = this.e().c();
      if(kotlin.h.j.a((CharSequence)var)) {
         co.uk.getmondo.common.q var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("intercomService");
         }

         var.a();
      } else {
         co.uk.getmondo.common.q var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("intercomService");
         }

         var.b(var);
      }

   }

   public void d() {
      this.startActivity(HelpActivity.c.a((Context)this).addFlags(67108864));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034176);
      ((TextView)this.a(co.uk.getmondo.c.a.negativeFeedbackEmojum)).setText((CharSequence)co.uk.getmondo.common.k.e.h());
      ((TextView)this.a(co.uk.getmondo.c.a.positiveFeedbackEmojum)).setText((CharSequence)co.uk.getmondo.common.k.e.g());
      co.uk.getmondo.common.h.b.b var = this.l();
      co.uk.getmondo.help.a.a.e var = this.e();
      kotlin.d.b.l.a(var, "topicViewModel");
      var.a(new co.uk.getmondo.help.b.f(var)).a(this);
      o var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((o.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/help/HelpTopicActivity$Companion;", "", "()V", "KEY_TOPIC_VM", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "topicViewModel", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, co.uk.getmondo.help.a.a.e var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "topicViewModel");
         Intent var = (new Intent(var, HelpTopicActivity.class)).putExtra("topic_vm", (Parcelable)var);
         kotlin.d.b.l.a(var, "Intent(context, HelpTopi…TOPIC_VM, topicViewModel)");
         return var;
      }
   }
}
