package co.uk.getmondo.help;

import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013BG\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0001\u0010\n\u001a\u00020\u000b\u0012\b\b\u0001\u0010\f\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/help/HelpCategoryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpCategoryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "helpManager", "Lco/uk/getmondo/help/data/HelpManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "categoryId", "", "categoryTitle", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.help.a.a e;
   private final co.uk.getmondo.common.e.a f;
   private final String g;
   private final String h;
   private final co.uk.getmondo.common.a i;

   public c(u var, u var, co.uk.getmondo.help.a.a var, co.uk.getmondo.common.e.a var, String var, String var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "helpManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "categoryId");
      kotlin.d.b.l.b(var, "categoryTitle");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   public void a(final c.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.Companion.o(this.g));
      co.uk.getmondo.help.a.a.c var = co.uk.getmondo.help.a.a.c.h.a(this.g);
      StringBuilder var = new StringBuilder();
      String var;
      if(var != null) {
         var = co.uk.getmondo.common.k.e.b(var.c());
      } else {
         var = null;
      }

      final String var = var.append(var).append(" ").append(this.h).toString();
      var.a(var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = io.reactivex.n.just(kotlin.n.a).mergeWith((io.reactivex.r)var.a()).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(c.this.e.b(c.this.g).b(c.this.c).a(c.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.a(true);
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(List varx, Throwable var) {
                  var.a(false);
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = c.this.f;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
                  var.d();
               }
            })));
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List varx) {
            c.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new d(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "Observable.just(Unit) //…Content(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.n var = var.b();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.help.a.a.d.c varx) {
            var.a(varx.c(), var);
         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new d(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.topicClicked\n      …pic, title) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.c();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            c.this.i.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.HELP, c.this.g, (String)null, 4, (Object)null));
            var.e();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new d(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.helpClicked\n       …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\r\u001a\u00020\u0005H&J\u0018\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0010\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0016\u0010\u0017\u001a\u00020\u00052\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H&J\b\u0010\u001b\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0007¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/help/HelpCategoryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "helpClicked", "Lio/reactivex/Observable;", "", "getHelpClicked", "()Lio/reactivex/Observable;", "retryClicked", "getRetryClicked", "topicClicked", "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "getTopicClicked", "openChat", "openTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "title", "", "setLoading", "isEnabled", "", "setTitle", "showContent", "sectionItems", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "showError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Topic var, String var);

      void a(String var);

      void a(List var);

      void a(boolean var);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();
   }
}
