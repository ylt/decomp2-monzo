package co.uk.getmondo.help;

import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/help/HelpTopicPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpTopicPresenter$View;", "topicViewModel", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/help/data/model/TopicViewModel;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.help.a.a.e c;
   private final co.uk.getmondo.common.a d;

   public o(co.uk.getmondo.help.a.a.e var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "topicViewModel");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
   }

   public void a(final o.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.c.a());
      var.a(this.c.b());
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.a();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            o.this.d.a(Impression.Companion.a(Impression.HelpOutcome.THUMBS_UP, o.this.c.b().a()));
            var.d();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new p(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.feedbackPositiveCli…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.b();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            o.this.d.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.HELP, o.this.c.b().a(), (String)null, 4, (Object)null));
            o.this.d.a(Impression.Companion.a(Impression.HelpOutcome.THUMBS_DOWN, o.this.c.b().a()));
            var.c();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new p(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.feedbackNegativeCli…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\t\u001a\u00020\u0004H&J\b\u0010\n\u001a\u00020\u0004H&J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/help/HelpTopicPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "feedbackNegativeClicked", "Lio/reactivex/Observable;", "", "getFeedbackNegativeClicked", "()Lio/reactivex/Observable;", "feedbackPositiveClicked", "getFeedbackPositiveClicked", "openChat", "openHelpHome", "setTitle", "title", "", "showTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Topic var);

      void a(String var);

      io.reactivex.n b();

      void c();

      void d();
   }
}
