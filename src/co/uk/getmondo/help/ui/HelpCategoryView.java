package co.uk.getmondo.help.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.e;
import co.uk.getmondo.help.a.a.a;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.n;
import kotlin.d.a.b;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\n\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010%\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u0010R$\u0010\n\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R*\u0010\u0015\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR$\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\t\u001a\u00020\u001c@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R$\u0010\"\u001a\u00020\u001c2\u0006\u0010\t\u001a\u00020\u001c@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001f\"\u0004\b$\u0010!¨\u0006&"},
   d2 = {"Lco/uk/getmondo/help/ui/HelpCategoryView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "value", "backgroundColorRes", "getBackgroundColorRes", "()I", "setBackgroundColorRes", "(I)V", "category", "Lco/uk/getmondo/help/data/model/Category;", "getCategory", "()Lco/uk/getmondo/help/data/model/Category;", "setCategory", "(Lco/uk/getmondo/help/data/model/Category;)V", "clickListener", "Lkotlin/Function1;", "", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "setClickListener", "(Lkotlin/jvm/functions/Function1;)V", "", "emojiText", "getEmojiText", "()Ljava/lang/String;", "setEmojiText", "(Ljava/lang/String;)V", "label", "getLabel", "setLabel", "bind", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HelpCategoryView extends ConstraintLayout {
   private a c;
   private String d;
   private String e;
   private int f;
   private b g;
   private HashMap h;

   public HelpCategoryView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public HelpCategoryView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public HelpCategoryView(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      this.d = "";
      this.e = "";
      View.inflate(var, 2131034431, (ViewGroup)this);
      if(var != null) {
         TypedArray var = var.obtainStyledAttributes(var, c.b.HelpCategoryView, 0, 0);
         String var = var.getString(0);
         if(var == null) {
            var = "";
         }

         this.setLabel(var);
         var = var.getString(2);
         if(var == null) {
            var = "";
         }

         this.setEmojiText(var);
         this.setBackgroundColorRes(var.getResourceId(1, 0));
         var.recycle();
      }

      this.setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            b var = HelpCategoryView.this.getClickListener();
            if(var != null) {
               n var = (n)var.a(HelpCategoryView.this.getCategory());
            }

         }
      }));
   }

   // $FF: synthetic method
   public HelpCategoryView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public final void a(a var) {
      l.b(var, "category");
      this.c = var;
      this.setLabel(ae.a(this, var.b()));
      String var = e.b(var.c());
      l.a(var, "EmojiHelper.getEmojum(category.emojum)");
      this.setEmojiText(var);
   }

   public View b(int var) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var = (View)this.h.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.h.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final int getBackgroundColorRes() {
      return this.f;
   }

   public final a getCategory() {
      return this.c;
   }

   public final b getClickListener() {
      return this.g;
   }

   public final String getEmojiText() {
      return this.e;
   }

   public final String getLabel() {
      return this.d;
   }

   public final void setBackgroundColorRes(int var) {
      if(var != 0) {
         Drawable var = ((ImageView)this.b(c.a.helpCategoryBackground)).getDrawable().mutate();
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
         }

         ((GradientDrawable)var).setColor(android.support.v4.content.a.c(this.getContext(), var));
      }

   }

   public final void setCategory(a var) {
      this.c = var;
   }

   public final void setClickListener(b var) {
      this.g = var;
   }

   public final void setEmojiText(String var) {
      l.b(var, "value");
      ((TextView)this.b(c.a.helpCategoryIcon)).setText((CharSequence)var);
   }

   public final void setLabel(String var) {
      l.b(var, "value");
      ((TextView)this.b(c.a.helpCategoryLabel)).setText((CharSequence)var);
   }
}
