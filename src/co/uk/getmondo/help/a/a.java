package co.uk.getmondo.help.a;

import co.uk.getmondo.api.HelpApi;
import co.uk.getmondo.api.model.help.Content;
import co.uk.getmondo.api.model.help.SearchQuery;
import co.uk.getmondo.api.model.help.Section;
import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.common.v;
import co.uk.getmondo.help.a.a.d;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\bJ\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u000f0\u000eJ\u001a\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0015\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/help/data/HelpManager;", "", "helpApi", "Lco/uk/getmondo/api/HelpApi;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "(Lco/uk/getmondo/api/HelpApi;Lco/uk/getmondo/common/ResourceProvider;)V", "trendingTitle", "", "getTrendingTitle", "()Ljava/lang/String;", "setTrendingTitle", "(Ljava/lang/String;)V", "loadCategoryItems", "Lio/reactivex/Single;", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "id", "loadTrendingTopics", "Lco/uk/getmondo/api/model/help/Topic;", "search", "query", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private String a;
   private final HelpApi b;
   private final v c;

   public a(HelpApi var, v var) {
      l.b(var, "helpApi");
      l.b(var, "resourceProvider");
      super();
      this.b = var;
      this.c = var;
      this.a = "";
   }

   public final String a() {
      return this.a;
   }

   public final void a(String var) {
      l.b(var, "<set-?>");
      this.a = var;
   }

   public final io.reactivex.v b() {
      io.reactivex.v var = this.b.trending().c((g)(new g() {
         public final void a(Content var) {
            a.this.a(((Section)m.e(var.a())).a());
         }
      })).d((h)null.a);
      l.a(var, "helpApi.trending()\n     ….first().topics.take(5) }");
      return var;
   }

   public final io.reactivex.v b(String var) {
      l.b(var, "id");
      io.reactivex.v var = this.b.categories(var).d((h)(new h() {
         public final List a(Content var) {
            l.b(var, "<name for destructuring parameter 0>");
            List var = var.b();
            List var = (List)(new ArrayList());

            Collection var;
            for(Iterator var = var.iterator(); var.hasNext(); var = (Collection)var) {
               Section var = (Section)var.next();
               String var = var.c();
               List var = var.d();
               var.add(new d.b(var));
               Iterator var = ((Iterable)var).iterator();

               while(var.hasNext()) {
                  Object var = var.next();
                  ((Collection)var).add(new d.c((Topic)var));
               }
            }

            ((Collection)var).add(new d.a(a.this.c.a(2131362239)));
            return var;
         }
      }));
      l.a(var, "helpApi.categories(id)\n …ryItems\n                }");
      return var;
   }

   public final io.reactivex.v c(String var) {
      l.b(var, "query");
      io.reactivex.v var = this.b.search(var).d((h)(new h() {
         public final List a(SearchQuery var) {
            l.b(var, "<name for destructuring parameter 0>");
            List var = var.a();
            List var = (List)(new ArrayList());
            ((Collection)var).add(new d.b(a.this.c.a(2131362237)));
            Iterator var = ((Iterable)((Section)m.e(var)).b()).iterator();

            while(var.hasNext()) {
               Object var = var.next();
               ((Collection)var).add(new d.c((Topic)var));
            }

            Collection var = (Collection)var;
            ((Collection)var).add(new d.a(a.this.c.a(2131362233)));
            return var;
         }
      }));
      l.a(var, "helpApi.search(query)\n  …icItems\n                }");
      return var;
   }
}
