package co.uk.getmondo.help.a.a;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\u0001\u0018\u0000 \u00192\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0019B!\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bR\u0014\u0010\u0007\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0014\u0010\u000e\u001a\u00020\u000fX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/help/data/model/HelpCategory;", "", "Lco/uk/getmondo/help/data/model/Category;", "id", "", "titleRes", "", "emojum", "(Ljava/lang/String;ILjava/lang/String;II)V", "getEmojum", "()I", "getId", "()Ljava/lang/String;", "getTitleRes", "type", "Lco/uk/getmondo/help/data/model/Category$Type;", "getType", "()Lco/uk/getmondo/help/data/model/Category$Type;", "TRANSACTIONS_BALANCE", "CARD_PIN_TOP_UP", "LIMITS", "GOING_ABROAD", "PRODUCT_FEATURES", "PROFILE", "CONTACTS_PAYMENTS", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum c implements a {
   a,
   b,
   c,
   d,
   e,
   f,
   g;

   public static final c.a h;
   private final a.a j;
   private final String k;
   private final int l;
   private final int m;

   static {
      c var = new c("TRANSACTIONS_BALANCE", 0, "transactions-and-balance", 2131362230, 128183);
      a = var;
      c var = new c("CARD_PIN_TOP_UP", 1, "card-pin-and-top-up", 2131362224, 128179);
      b = var;
      c var = new c("LIMITS", 2, "limits", 2131362228, 9995);
      c = var;
      c var = new c("GOING_ABROAD", 3, "going-abroad", 2131362222, 9992);
      d = var;
      c var = new c("PRODUCT_FEATURES", 4, "product-features", 2131362227, 128161);
      e = var;
      c var = new c("PROFILE", 5, "profile", 2131362223, 128100);
      f = var;
      c var = new c("CONTACTS_PAYMENTS", 6, "contacts-and-payments", 2131362225, 128140);
      g = var;
      h = new c.a((i)null);
   }

   protected c(String var, int var, int var) {
      l.b(var, "id");
      super(var, var);
      this.k = var;
      this.l = var;
      this.m = var;
      this.j = a.a.a;
   }

   public String a() {
      return this.k;
   }

   public int b() {
      return this.l;
   }

   public int c() {
      return this.m;
   }

   public a.a d() {
      return this.j;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/help/data/model/HelpCategory$Companion;", "", "()V", "from", "Lco/uk/getmondo/help/data/model/HelpCategory;", "id", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final c a(String var) {
         l.b(var, "id");
         Object[] var = (Object[])c.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(l.a(((c)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         return (c)var;
      }
   }
}
