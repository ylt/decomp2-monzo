package co.uk.getmondo.help.a.a;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0001\u0010R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0005R\u0012\u0010\f\u001a\u00020\rX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/help/data/model/Category;", "", "emojum", "", "getEmojum", "()I", "id", "", "getId", "()Ljava/lang/String;", "titleRes", "getTitleRes", "type", "Lco/uk/getmondo/help/data/model/Category$Type;", "getType", "()Lco/uk/getmondo/help/data/model/Category$Type;", "Type", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface a {
   String a();

   int b();

   int c();

   a.a d();

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/help/data/model/Category$Type;", "", "(Ljava/lang/String;I)V", "HELP", "COMMUNITY", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b;

      static {
         a.a var = new a.a("HELP", 0);
         a = var;
         a.a var = new a.a("COMMUNITY", 1);
         b = var;
      }
   }
}
