package co.uk.getmondo.help.a.a;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0001\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0017B)\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0004¢\u0006\u0002\u0010\tR\u0014\u0010\u0007\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0014\u0010\u000f\u001a\u00020\u0010X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rj\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/help/data/model/CommunityCategory;", "", "Lco/uk/getmondo/help/data/model/Category;", "id", "", "titleRes", "", "emojum", "url", "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V", "getEmojum", "()I", "getId", "()Ljava/lang/String;", "getTitleRes", "type", "Lco/uk/getmondo/help/data/model/Category$Type;", "getType", "()Lco/uk/getmondo/help/data/model/Category$Type;", "getUrl", "SNEAK_PEEKS", "FEATURE_IDEAS", "COMMUNITY_HOME", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum b implements a {
   a,
   b,
   c;

   public static final b.a d;
   private final a.a f;
   private final String g;
   private final int h;
   private final int i;
   private final String j;

   static {
      b var = new b("SNEAK_PEEKS", 0, "sneak-peeks", 2131362084, 128064, "https://community.monzo.com/c/sneak-peek");
      a = var;
      b var = new b("FEATURE_IDEAS", 1, "feature-ideas", 2131362083, 9889, "https://community.monzo.com/c/ideas");
      b = var;
      b var = new b("COMMUNITY_HOME", 2, "community-home", 2131362082, 127968, "https://community.monzo.com/");
      c = var;
      d = new b.a((i)null);
   }

   protected b(String var, int var, int var, String var) {
      l.b(var, "id");
      l.b(var, "url");
      super(var, var);
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.f = a.a.b;
   }

   public String a() {
      return this.g;
   }

   public int b() {
      return this.h;
   }

   public int c() {
      return this.i;
   }

   public a.a d() {
      return this.f;
   }

   public final String e() {
      return this.j;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/help/data/model/CommunityCategory$Companion;", "", "()V", "from", "Lco/uk/getmondo/help/data/model/CommunityCategory;", "id", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final b a(String var) {
         l.b(var, "id");
         Object[] var = (Object[])b.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(l.a(((b)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         return (b)var;
      }
   }
}
