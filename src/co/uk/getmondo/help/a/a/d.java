package co.uk.getmondo.help.a.a;

import co.uk.getmondo.api.model.help.Topic;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u000b\f\rB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n\u0082\u0001\u0003\u000e\u000f\u0010¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/help/data/model/SectionItem;", "", "()V", "id", "", "getId", "()J", "layout", "", "getLayout", "()I", "HelpAction", "SectionHeader", "TopicItem", "Lco/uk/getmondo/help/data/model/SectionItem$SectionHeader;", "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "Lco/uk/getmondo/help/data/model/SectionItem$HelpAction;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class d {
   private d() {
   }

   // $FF: synthetic method
   public d(i var) {
      this();
   }

   public abstract long a();

   public abstract int b();

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\nHÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/help/data/model/SectionItem$HelpAction;", "Lco/uk/getmondo/help/data/model/SectionItem;", "title", "", "(Ljava/lang/String;)V", "id", "", "getId", "()J", "layout", "", "getLayout", "()I", "getTitle", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends d {
      private final String a;

      public a(String var) {
         l.b(var, "title");
         super((i)null);
         this.a = var;
      }

      public long a() {
         return (long)this.a.hashCode();
      }

      public int b() {
         return 2131034364;
      }

      public final String c() {
         return this.a;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label26: {
               if(var instanceof d.a) {
                  d.a var = (d.a)var;
                  if(l.a(this.a, var.a)) {
                     break label26;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         String var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var;
      }

      public String toString() {
         return "HelpAction(title=" + this.a + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\nHÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/help/data/model/SectionItem$SectionHeader;", "Lco/uk/getmondo/help/data/model/SectionItem;", "title", "", "(Ljava/lang/String;)V", "id", "", "getId", "()J", "layout", "", "getLayout", "()I", "getTitle", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends d {
      private final String a;

      public b(String var) {
         l.b(var, "title");
         super((i)null);
         this.a = var;
      }

      public long a() {
         return (long)this.a.hashCode();
      }

      public int b() {
         return 2131034365;
      }

      public final String c() {
         return this.a;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label26: {
               if(var instanceof d.b) {
                  d.b var = (d.b)var;
                  if(l.a(this.a, var.a)) {
                     break label26;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         String var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var;
      }

      public String toString() {
         return "SectionHeader(title=" + this.a + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\nHÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"},
      d2 = {"Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "Lco/uk/getmondo/help/data/model/SectionItem;", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "(Lco/uk/getmondo/api/model/help/Topic;)V", "id", "", "getId", "()J", "layout", "", "getLayout", "()I", "getTopic", "()Lco/uk/getmondo/api/model/help/Topic;", "component1", "copy", "equals", "", "other", "", "hashCode", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends d {
      private final Topic a;

      public c(Topic var) {
         l.b(var, "topic");
         super((i)null);
         this.a = var;
      }

      public long a() {
         return (long)this.a.a().hashCode();
      }

      public int b() {
         return 2131034366;
      }

      public final Topic c() {
         return this.a;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label26: {
               if(var instanceof d.c) {
                  d.c var = (d.c)var;
                  if(l.a(this.a, var.a)) {
                     break label26;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         Topic var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var;
      }

      public String toString() {
         return "TopicItem(topic=" + this.a + ")";
      }
   }
}
