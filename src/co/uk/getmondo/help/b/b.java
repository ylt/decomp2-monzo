package co.uk.getmondo.help.b;

import android.content.Context;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001:\u0002\t\nB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0007J\u0012\u0010\u0006\u001a\u00020\u00032\b\b\u0001\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/help/di/HelpCategoryModule;", "", "categoryId", "", "(Ljava/lang/String;)V", "provideCategoryId", "provideCategoryTitle", "context", "Landroid/content/Context;", "Id", "Title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final String a;

   public b(String var) {
      l.b(var, "categoryId");
      super();
      this.a = var;
   }

   public final String a() {
      return this.a;
   }

   public final String a(Context var) {
      l.b(var, "context");
      co.uk.getmondo.help.a.a.c var = co.uk.getmondo.help.a.a.c.h.a(this.a);
      String var;
      if(var != null) {
         var = var.getString(var.b());
         if(var != null) {
            return var;
         }
      }

      var = "";
      return var;
   }
}
