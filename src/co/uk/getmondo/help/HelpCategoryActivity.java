package co.uk.getmondo.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 42\u00020\u00012\u00020\u0002:\u00014B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010#\u001a\u00020\r2\b\u0010$\u001a\u0004\u0018\u00010%H\u0014J\b\u0010&\u001a\u00020\rH\u0016J\u0018\u0010'\u001a\u00020\r2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u0005H\u0016J\u0010\u0010+\u001a\u00020\r2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020\r2\u0006\u0010*\u001a\u00020\u0005H\u0016J\u0016\u0010/\u001a\u00020\r2\f\u00100\u001a\b\u0012\u0004\u0012\u00020201H\u0016J\b\u00103\u001a\u00020\rH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0010\u001a\u00020\u00118\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\r0\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u000fR\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\"\u0010\u000f¨\u00065"},
   d2 = {"Lco/uk/getmondo/help/HelpCategoryActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/help/HelpCategoryPresenter$View;", "()V", "categoryId", "", "kotlin.jvm.PlatformType", "getCategoryId", "()Ljava/lang/String;", "categoryId$delegate", "Lkotlin/Lazy;", "helpClicked", "Lio/reactivex/Observable;", "", "getHelpClicked", "()Lio/reactivex/Observable;", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "presenter", "Lco/uk/getmondo/help/HelpCategoryPresenter;", "getPresenter", "()Lco/uk/getmondo/help/HelpCategoryPresenter;", "setPresenter", "(Lco/uk/getmondo/help/HelpCategoryPresenter;)V", "retryClicked", "getRetryClicked", "sectionAdapter", "Lco/uk/getmondo/help/HelpSectionAdapter;", "topicClicked", "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "getTopicClicked", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "openChat", "openTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "title", "setLoading", "isEnabled", "", "setTitle", "showContent", "sectionItems", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "showError", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HelpCategoryActivity extends co.uk.getmondo.common.activities.b implements c.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(HelpCategoryActivity.class), "categoryId", "getCategoryId()Ljava/lang/String;"))};
   public static final HelpCategoryActivity.a e = new HelpCategoryActivity.a((kotlin.d.b.i)null);
   public c b;
   public co.uk.getmondo.common.q c;
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return HelpCategoryActivity.this.getIntent().getStringExtra("categoryId");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final m g = new m((List)null, (kotlin.d.a.b)null, (kotlin.d.a.a)null, 7, (kotlin.d.b.i)null);
   private HashMap h;

   private final String f() {
      kotlin.c var = this.f;
      kotlin.reflect.l var = a[0];
      return (String)var.a();
   }

   public View a(int var) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var = (View)this.h.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.h.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)).c();
   }

   public void a(Topic var, String var) {
      kotlin.d.b.l.b(var, "topic");
      kotlin.d.b.l.b(var, "title");
      this.startActivity(HelpTopicActivity.e.a((Context)this, new co.uk.getmondo.help.a.a.e(var, var, (String)null, 4, (kotlin.d.b.i)null)));
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "title");
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.a((CharSequence)var);
      }

   }

   public void a(List var) {
      kotlin.d.b.l.b(var, "sectionItems");
      this.g.a(var);
   }

   public void a(boolean var) {
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)).a(var, this.getString(2131362229));
      if(var) {
         ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)));
      } else {
         ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView));
      }

   }

   public io.reactivex.n b() {
      io.reactivex.n var = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var) {
            kotlin.d.b.l.b(var, "emitter");
            HelpCategoryActivity.this.g.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(co.uk.getmondo.help.a.a.d.c varx) {
                  kotlin.d.b.l.b(varx, "it");
                  var.a(varx);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  HelpCategoryActivity.this.g.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…stener = null }\n        }");
      return var;
   }

   public io.reactivex.n c() {
      io.reactivex.n var = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var) {
            kotlin.d.b.l.b(var, "emitter");
            HelpCategoryActivity.this.g.a((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var.a(kotlin.n.a);
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  HelpCategoryActivity.this.g.a((kotlin.d.a.a)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…stener = null }\n        }");
      return var;
   }

   public void d() {
      LoadingErrorView var = (LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView);
      String var = this.getString(2131362226);
      kotlin.d.b.l.a(var, "getString(R.string.help_category_error)");
      LoadingErrorView.a(var, var, false, (String)null, 2130837839, 6, (Object)null);
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)));
   }

   public void e() {
      co.uk.getmondo.common.q var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("intercomService");
      }

      var.a();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034174);
      co.uk.getmondo.common.h.b.b var = this.l();
      String var = this.f();
      kotlin.d.b.l.a(var, "categoryId");
      var.a(new co.uk.getmondo.help.b.b(var)).a(this);
      LinearLayoutManager var = new LinearLayoutManager((Context)this);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.categoryRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.categoryRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.g);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.categoryRecyclerView)).setHasFixedSize(true);
      c var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((c.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/help/HelpCategoryActivity$Companion;", "", "()V", "KEY_CATEGORY_ID", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "categoryId", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, String var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "categoryId");
         Intent var = (new Intent(var, HelpCategoryActivity.class)).putExtra("categoryId", var);
         kotlin.d.b.l.a(var, "Intent(context, HelpCate…_CATEGORY_ID, categoryId)");
         return var;
      }
   }
}
