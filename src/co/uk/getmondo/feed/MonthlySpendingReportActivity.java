package co.uk.getmondo.feed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.v;
import co.uk.getmondo.main.HomeActivity;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

public class MonthlySpendingReportActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   @BindView(2131821100)
   AmountView amountView;
   private YearMonth b;
   @BindView(2131821103)
   Button goToSpendingButton;
   @BindView(2131821101)
   TextView monthTextView;

   public static Intent a(Context var, v var) {
      Intent var = new Intent(var, MonthlySpendingReportActivity.class);
      var.putExtra("KEY_MONTHLY_SPENDING_AMOUNT", var.a());
      var.putExtra("KEY_MONTHLY_SPENDING_MONTH", var.b());
      return var;
   }

   // $FF: synthetic method
   static void a(MonthlySpendingReportActivity var, SpendingReportFeedbackDialogFragment.a var) {
      Impression var = Impression.a(var, var.b);
      var.a.a(var);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034213);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("KEY_MONTHLY_SPENDING_AMOUNT");
      this.b = (YearMonth)this.getIntent().getSerializableExtra("KEY_MONTHLY_SPENDING_MONTH");
      this.a.a(Impression.a(this.b));
      this.amountView.setAmount(var);
      String var = this.b.c().a(TextStyle.a, Locale.ENGLISH);
      this.monthTextView.setText(this.getString(2131362451, new Object[]{var}));
      this.goToSpendingButton.setText(this.getString(2131362452, new Object[]{var}));
   }

   public boolean onCreateOptionsMenu(Menu var) {
      this.getMenuInflater().inflate(2131951624, var);
      return true;
   }

   @OnClick({2131821103})
   public void onGoToSpendingButtonClicked() {
      this.startActivity(HomeActivity.a((Context)this, (YearMonth)this.b));
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      boolean var;
      if(var.getItemId() == 2131821783) {
         SpendingReportFeedbackDialogFragment var = SpendingReportFeedbackDialogFragment.a();
         var.a(i.a(this));
         var.show(this.getFragmentManager(), "spending_report");
         var = true;
      } else {
         var = false;
      }

      return var;
   }
}
