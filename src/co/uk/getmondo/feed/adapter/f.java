package co.uk.getmondo.feed.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.o;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import co.uk.getmondo.d.u;
import com.bumptech.glide.g;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\fJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/feed/adapter/TransactionViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "listener", "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;", "(Landroid/view/View;Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;)V", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "feedItem", "Lco/uk/getmondo/model/FeedItem;", "bind", "", "bindP2pFeedItem", "photoUrl", "", "name", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends w {
   private final co.uk.getmondo.common.i.b a;
   private final co.uk.getmondo.common.ui.a b;
   private m c;
   private final a.a d;

   public f(View var, a.a var) {
      l.b(var, "view");
      super(var);
      this.d = var;
      this.a = new co.uk.getmondo.common.i.b(true, false, false, 6, (i)null);
      co.uk.getmondo.common.ui.a.a var = co.uk.getmondo.common.ui.a.a;
      Context var = var.getContext();
      l.a(var, "view.context");
      this.b = var.a(var);
      var.setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            if(f.this.d == null) {
               d.a.a.a("Need to set OnFeedItemActionListener", new Object[0]);
            }

            a.a var = f.this.d;
            if(var != null) {
               var.a(f.b(f.this));
            }

         }
      }));
      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            Context var = var.getContext();
            l.a(var, "anchorView.context");
            l.a(var, "anchorView");
            m var = f.b(f.this);
            a.a var = f.this.d;
            if(var == null) {
               l.a();
            }

            (new c(var, var, var, var)).show();
         }
      }));
   }

   private final void a(String var, String var) {
      CharSequence var = (CharSequence)var;
      boolean var;
      if(var != null && !j.a(var)) {
         var = false;
      } else {
         var = true;
      }

      if(!var) {
         g.b(((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).getContext()).a(Uri.parse(var)).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)))));
      } else {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(co.uk.getmondo.common.ui.a.b.a(this.b.a(var), 0, (Typeface)null, false, 7, (Object)null));
      }

      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(0);
      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(false);
      ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var);
   }

   // $FF: synthetic method
   public static final m b(f var) {
      m var = var.c;
      if(var == null) {
         l.b("feedItem");
      }

      return var;
   }

   public final void a(m var) {
      l.b(var, "feedItem");
      this.c = var;
      if(var.e().d()) {
         d.a.a.a((Throwable)(new IllegalArgumentException("Trying to bind a FeedItem in TransactionViewHolder, but there's no Transaction! Feed item type: " + var.j() + " id: " + var.a())));
      }

      aj var = (aj)var.e().a();
      co.uk.getmondo.payments.send.data.a.a var;
      String var;
      if(var.s()) {
         label104: {
            var = var.C();
            if(var != null) {
               var = var.a();
               if(var != null) {
                  break label104;
               }
            }

            var = var.x();
         }

         co.uk.getmondo.common.ui.a var = this.b;
         l.a(var, "title");
         Drawable var = co.uk.getmondo.common.ui.a.b.a(var.a(var), 0, (Typeface)null, true, 3, (Object)null);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(var);
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var);
      } else if(var.r()) {
         if(j.a((CharSequence)var.B().b())) {
            var = var.C();
            if(var != null) {
               var = var.b();
            } else {
               var = null;
            }

            if(var == null) {
               l.a();
            }

            var = o.a(var);
            StringBuilder var = (new StringBuilder()).append("").append(var).append(" • ");
            var = var.C();
            if(var != null) {
               var = var.c();
            } else {
               var = null;
            }

            var = var.append(var).toString();
         } else {
            var = var.B().b();
            l.a(var, "transaction.peer.name");
         }

         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(co.uk.getmondo.common.ui.a.b.a(this.b.a(var), 0, (Typeface)null, false, 7, (Object)null));
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var);
      } else if(var.t()) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var.x());
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(2130837982);
      } else if(var.q()) {
         String var = var.B().g();
         var = var.B().b();
         l.a(var, "transaction.peer.name");
         this.a(var, var);
      } else {
         if(var.n()) {
            com.bumptech.glide.j var = g.b(((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).getContext());
            u var = var.f();
            if(var == null) {
               l.a();
            }

            var.a(var.j()).a().a(var.c().c()).c().a((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView));
         } else if(var.z()) {
            ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(2130837850);
         } else {
            ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(var.c().c());
         }

         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var.x());
      }

      if(var.r()) {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(false);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(0);
      } else {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(true);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(2130838008);
      }

      AmountView var;
      co.uk.getmondo.d.c var;
      if(var.t()) {
         var = (AmountView)this.itemView.findViewById(co.uk.getmondo.c.a.amountView);
         var = var.g();
         l.a(var, "transaction.amount");
         var.a(2131493049, var);
      } else {
         var = (AmountView)this.itemView.findViewById(co.uk.getmondo.c.a.amountView);
         var = var.g();
         l.a(var, "transaction.amount");
         var.a(2131493048, var);
      }

      if(var.d().b()) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)).setText((CharSequence)var.d().a());
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView));
      }

      if(var.m() && ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)).getVisibility() != 0) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView)).setText((CharSequence)var.e());
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView)));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView));
      }

      if(var.y()) {
         ae.b((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer));
      } else {
         ae.a((View)((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)));
      }

      if(var.i()) {
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView)));
         TextView var = (TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView);
         co.uk.getmondo.common.i.b var = this.a;
         co.uk.getmondo.d.c var = var.h();
         l.a(var, "transaction.localAmount");
         var.setText((CharSequence)var.a(var));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView));
      }

      LayoutParams var;
      if(var.k()) {
         ae.a((View)((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton)));
         var = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getLayoutParams();
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
         }

         ((MarginLayoutParams)var).setMarginEnd(0);
      } else {
         ae.b((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton));
         int var = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getContext().getResources().getDimensionPixelSize(2131427603);
         var = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getLayoutParams();
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
         }

         ((MarginLayoutParams)var).setMarginEnd(var);
      }

   }
}
