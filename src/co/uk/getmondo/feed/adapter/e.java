package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final SimpleFeedItemViewHolder a;

   private e(SimpleFeedItemViewHolder var) {
      this.a = var;
   }

   public static OnClickListener a(SimpleFeedItemViewHolder var) {
      return new e(var);
   }

   public void onClick(View var) {
      SimpleFeedItemViewHolder.a(this.a, var);
   }
}
