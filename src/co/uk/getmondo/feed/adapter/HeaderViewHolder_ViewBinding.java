package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class HeaderViewHolder_ViewBinding implements Unbinder {
   private HeaderViewHolder a;

   public HeaderViewHolder_ViewBinding(HeaderViewHolder var, View var) {
      this.a = var;
      var.dateView = (TextView)Utils.findRequiredViewAsType(var, 2131821599, "field 'dateView'", TextView.class);
   }

   public void unbind() {
      HeaderViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.dateView = null;
      }
   }
}
