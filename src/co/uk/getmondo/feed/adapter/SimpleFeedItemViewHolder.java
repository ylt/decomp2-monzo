package co.uk.getmondo.feed.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.m;
import com.bumptech.glide.g;

class SimpleFeedItemViewHolder extends w {
   private final co.uk.getmondo.feed.a.a a;
   private final a.a b;
   private m c;
   @BindView(2131821598)
   TextView descriptionTextView;
   @BindView(2131821595)
   ImageView iconImageView;
   @BindView(2131821596)
   View overflowButton;
   @BindView(2131821597)
   TextView titleTextView;

   SimpleFeedItemViewHolder(View var, co.uk.getmondo.feed.a.a var, a.a var) {
      super(var);
      this.a = var;
      this.b = var;
      ButterKnife.bind(this, (View)var);
      var.setOnClickListener(d.a(this, var));
      this.overflowButton.setOnClickListener(e.a(this));
   }

   // $FF: synthetic method
   static void a(SimpleFeedItemViewHolder var, View var) {
      (new c(var.getContext(), var, var.c, var.b)).show();
   }

   // $FF: synthetic method
   static void a(SimpleFeedItemViewHolder var, a.a var, View var) {
      var.a(var.c);
   }

   void a(m var) {
      this.c = var;
      co.uk.getmondo.d.f var = var.i();
      int var = this.a.a(var.j());
      if(var != null && var.c() != null) {
         g.b(this.iconImageView.getContext()).a(var.c()).a().a(var).a(this.iconImageView);
      } else {
         this.iconImageView.setImageResource(var);
      }

      this.iconImageView.setClipToOutline(true);
      this.titleTextView.setText(this.a.a(var));
      this.descriptionTextView.setText(this.a.b(var));
      View var = this.overflowButton;
      byte var;
      if(var.k()) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }
}
