package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SimpleFeedItemViewHolder_ViewBinding implements Unbinder {
   private SimpleFeedItemViewHolder a;

   public SimpleFeedItemViewHolder_ViewBinding(SimpleFeedItemViewHolder var, View var) {
      this.a = var;
      var.iconImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821595, "field 'iconImageView'", ImageView.class);
      var.overflowButton = Utils.findRequiredView(var, 2131821596, "field 'overflowButton'");
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821597, "field 'titleTextView'", TextView.class);
      var.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821598, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      SimpleFeedItemViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.iconImageView = null;
         var.overflowButton = null;
         var.titleTextView = null;
         var.descriptionTextView = null;
      }
   }
}
