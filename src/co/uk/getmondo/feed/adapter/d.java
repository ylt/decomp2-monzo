package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final SimpleFeedItemViewHolder a;
   private final a.a b;

   private d(SimpleFeedItemViewHolder var, a.a var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(SimpleFeedItemViewHolder var, a.a var) {
      return new d(var, var);
   }

   public void onClick(View var) {
      SimpleFeedItemViewHolder.a(this.a, this.b, var);
   }
}
