package co.uk.getmondo.feed.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import co.uk.getmondo.d.m;
import com.crashlytics.android.Crashlytics;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class a extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
   protected List a = Collections.emptyList();
   private a.a b;
   private final co.uk.getmondo.feed.a.a c;

   public a(co.uk.getmondo.feed.a.a var) {
      this.c = var;
   }

   private void a(String var, Object... var) {
      d.a.a.a(var, var);
      Crashlytics.log(String.format(Locale.ENGLISH, var, var));
   }

   public long a(int var) {
      return (long)co.uk.getmondo.common.c.a.a(((m)this.a.get(var)).c().getTime());
   }

   public w a(ViewGroup var) {
      return new HeaderViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034371, var, false));
   }

   public void a(w var, int var) {
      ((HeaderViewHolder)var).dateView.setText(co.uk.getmondo.common.c.a.a(((m)this.a.get(var)).c().getTime(), true));
      ((HeaderViewHolder)var).dateView.setVisibility(0);
   }

   public void a(co.uk.getmondo.common.b.b var) {
      this.a("Setting feed items from query results - size: %s changes: %s", new Object[]{Integer.valueOf(var.a().size()), var.b()});
      List var;
      if(var.a().isEmpty()) {
         var = Collections.emptyList();
      } else {
         var = var.a();
      }

      this.a = var;
      if(var.b() == null) {
         this.a("notifyDataSetChanged called", new Object[0]);
         this.notifyDataSetChanged();
      } else {
         Iterator var = var.b().iterator();

         while(var.hasNext()) {
            co.uk.getmondo.common.b.b.a var = (co.uk.getmondo.common.b.b.a)var.next();
            switch(null.a[var.a().ordinal()]) {
            case 1:
               this.a("notifyItemRangeInserted called startIndex: %d length: %d", new Object[]{Integer.valueOf(var.b()), Integer.valueOf(var.c())});
               this.notifyItemRangeInserted(var.b(), var.c());
               break;
            case 2:
               this.a("notifyItemRangeRemoved called startIndex: %d length: %d", new Object[]{Integer.valueOf(var.b()), Integer.valueOf(var.c())});
               this.notifyItemRangeRemoved(var.b(), var.c());
               break;
            case 3:
               this.a("notifyItemRangeChanged called startIndex: %d length: %d", new Object[]{Integer.valueOf(var.b()), Integer.valueOf(var.c())});
               this.notifyItemRangeChanged(var.b(), var.c());
            }
         }
      }

   }

   public void a(a.a var) {
      this.b = var;
   }

   public int getItemCount() {
      return this.a.size();
   }

   public int getItemViewType(int var) {
      byte var;
      if(((m)this.a.get(var)).j() == co.uk.getmondo.feed.a.a.a.a) {
         var = 0;
      } else {
         var = 1;
      }

      return var;
   }

   public void onBindViewHolder(w var, int var) {
      m var = (m)this.a.get(var);
      if(var instanceof f) {
         ((f)var).a(var);
      } else if(var instanceof SimpleFeedItemViewHolder) {
         ((SimpleFeedItemViewHolder)var).a(var);
      }

   }

   public w onCreateViewHolder(ViewGroup var, int var) {
      LayoutInflater var = LayoutInflater.from(var.getContext());
      Object var;
      switch(var) {
      case 0:
         var = new f(var.inflate(2131034380, var, false), this.b);
         break;
      case 1:
         var = new SimpleFeedItemViewHolder(var.inflate(2131034370, var, false), this.c, this.b);
         break;
      default:
         throw new UnsupportedOperationException("Unknown feed item type: 16843169");
      }

      return (w)var;
   }

   public interface a {
      void a(m var);

      void b(m var);
   }
}
