package co.uk.getmondo.feed;

import co.uk.getmondo.a.k;
import co.uk.getmondo.d.m;
import co.uk.getmondo.d.t;
import io.reactivex.u;
import io.reactivex.c.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cBS\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/feed/HomeFeedPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "deleteFeedItemStorage", "Lco/uk/getmondo/common/DeleteFeedItemStorage;", "feedManager", "Lco/uk/getmondo/feed/data/FeedManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/feed/data/FeedManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountManager;)V", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "localSpendComparator", "Lco/uk/getmondo/account_balance/LocalSpendComparator;", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.i.b c;
   private final k d;
   private final u e;
   private final u f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.common.i h;
   private final co.uk.getmondo.feed.a.d i;
   private final co.uk.getmondo.background_sync.d j;
   private final co.uk.getmondo.card.c k;
   private final co.uk.getmondo.a.a l;
   private final co.uk.getmondo.common.accounts.b m;

   public g(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.i var, co.uk.getmondo.feed.a.d var, co.uk.getmondo.background_sync.d var, co.uk.getmondo.card.c var, co.uk.getmondo.a.a var, co.uk.getmondo.common.accounts.b var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "apiErrorHandler");
      l.b(var, "deleteFeedItemStorage");
      l.b(var, "feedManager");
      l.b(var, "syncManager");
      l.b(var, "cardManager");
      l.b(var, "balanceRepository");
      l.b(var, "accountManager");
      super();
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
      this.c = new co.uk.getmondo.common.i.b(true, false, false, 6, (kotlin.d.b.i)null);
      this.d = new k();
   }

   public void a(final g.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.i.a().observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.common.b.b varx) {
            if(varx.a().isEmpty() && g.this.i.c() == null) {
               var.e();
            } else {
               var.f();
            }

            g.a var = var;
            l.a(varx, "queryResults");
            var.a(varx);
         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "feedManager.feedItems()\n…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.i.b().filter((q)(new q() {
         public final boolean a(m var) {
            l.b(var, "<anonymous parameter 0>");
            return g.this.h.b();
         }
      })).flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(m var) {
            l.b(var, "kycFeedItem");
            return g.this.i.b(var.a()).b((io.reactivex.c.a)(new io.reactivex.c.a() {
               public final void a() {
                  g.this.h.c();
               }
            })).b(g.this.f).a((io.reactivex.c.g)null.a).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "feedManager.knowYourCust…ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.a().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n varx) {
            l.b(varx, "<anonymous parameter 0>");
            return g.this.j.a().b(g.this.f).a(g.this.e).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.a(true);
               }
            })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.a(false);
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = g.this.g;
                  l.a(varx, "throwable");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "view.onRefreshAction()\n …ce\") }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object varx) {
            var.d();
         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "view.onSearch()\n        …ch() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(m varx) {
            l.b(varx, "feedItem");
            return g.this.i.b(varx.a()).b(g.this.f).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = g.this.g;
                  l.a(varx, "throwable");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "view.onFeedItemDeleted()…ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.k.a().observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g varx) {
            if(varx.h()) {
               var.g();
            } else {
               var.h();
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "cardManager.card()\n     …led to get card state\") }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.m.e().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var) {
            l.b(var, "it");
            return g.this.l.a(var);
         }
      })).observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.b varx) {
            g.a var = var;
            co.uk.getmondo.d.c var = varx.a();
            l.a(var, "accountBalance.balance");
            var.a(var);
            var = var;
            var = varx.b();
            l.a(var, "accountBalance.spentToday");
            var.b(var);
            if(varx.d()) {
               ArrayList var = new ArrayList((Collection)varx.c());
               kotlin.a.m.a((List)var, (Comparator)g.this.d);
               Iterable var = (Iterable)var;
               Collection var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
               Iterator var = var.iterator();

               while(var.hasNext()) {
                  t var = (t)var.next();
                  co.uk.getmondo.common.i.b var = g.this.c;
                  co.uk.getmondo.d.c var = var.a();
                  l.a(var, "it.spendToday");
                  var.add(var.a(var));
               }

               List var = (List)var;
               var.a(var);
            } else {
               var.j();
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "accountManager.accountId…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.i().startWith((Object)n.a).flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n varx) {
            l.b(varx, "it");
            return g.this.m.e().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(String varx) {
                  l.b(varx, "it");
                  return g.this.l.b(varx);
               }
            })).b(g.this.f).a(g.this.e).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = g.this.g;
                  l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "view.onDateChanged()\n   …ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007H&J\b\u0010\r\u001a\u00020\u0004H&J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0016\u0010\u0011\u001a\u00020\u00042\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\t0\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0010H&J\b\u0010\u0019\u001a\u00020\u0004H&J\u0016\u0010\u001a\u001a\u00020\u00042\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001cH&J\b\u0010\u001e\u001a\u00020\u0004H&J\b\u0010\u001f\u001a\u00020\u0004H&¨\u0006 "},
      d2 = {"Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideForeignCurrencyAmounts", "", "hideProgressIndicator", "onDateChanged", "Lio/reactivex/Observable;", "onFeedItemDeleted", "Lco/uk/getmondo/model/FeedItem;", "onRefreshAction", "onSearch", "", "openSearch", "setBalance", "balance", "Lco/uk/getmondo/model/Amount;", "setFeedItems", "feedItems", "Lco/uk/getmondo/common/data/QueryResults;", "setRefreshing", "refreshing", "", "setSpentToday", "spentToday", "showCardFrozen", "showForeignCurrencyAmounts", "amounts", "", "", "showProgressIndicator", "showTitle", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.common.b.b var);

      void a(co.uk.getmondo.d.c var);

      void a(List var);

      void a(boolean var);

      io.reactivex.n b();

      void b(co.uk.getmondo.d.c var);

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();

      void h();

      io.reactivex.n i();

      void j();
   }
}
