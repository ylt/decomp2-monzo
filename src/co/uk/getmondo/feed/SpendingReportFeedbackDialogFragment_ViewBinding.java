package co.uk.getmondo.feed;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SpendingReportFeedbackDialogFragment_ViewBinding implements Unbinder {
   private SpendingReportFeedbackDialogFragment a;
   private View b;
   private View c;
   private View d;

   public SpendingReportFeedbackDialogFragment_ViewBinding(final SpendingReportFeedbackDialogFragment var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131821258, "field 'positiveFeedbackTextView' and method 'onCategoryClicked'");
      var.positiveFeedbackTextView = (TextView)Utils.castView(var, 2131821258, "field 'positiveFeedbackTextView'", TextView.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCategoryClicked((TextView)Utils.castParam(varx, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
      var = Utils.findRequiredView(var, 2131821259, "field 'neutralFeedbackTextView' and method 'onCategoryClicked'");
      var.neutralFeedbackTextView = (TextView)Utils.castView(var, 2131821259, "field 'neutralFeedbackTextView'", TextView.class);
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCategoryClicked((TextView)Utils.castParam(varx, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
      var = Utils.findRequiredView(var, 2131821260, "field 'negativeFeedbackTextView' and method 'onCategoryClicked'");
      var.negativeFeedbackTextView = (TextView)Utils.castView(var, 2131821260, "field 'negativeFeedbackTextView'", TextView.class);
      this.d = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCategoryClicked((TextView)Utils.castParam(varx, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
   }

   public void unbind() {
      SpendingReportFeedbackDialogFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.positiveFeedbackTextView = null;
         var.neutralFeedbackTextView = null;
         var.negativeFeedbackTextView = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
