package co.uk.getmondo.feed.welcome;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ak;
import io.reactivex.c.g;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter$View;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final d d;

   public b(co.uk.getmondo.common.a var, d var) {
      l.b(var, "analyticsService");
      l.b(var, "accountService");
      super();
      this.c = var;
      this.d = var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((f)var);
      ak var = this.d.b();
      co.uk.getmondo.d.a var;
      if(var != null) {
         var = var.c();
      } else {
         var = null;
      }

      if(var != null && var.e()) {
         ad var = (ad)var;
         var.a(var.g(), var.j());
      }

      this.c.a(Impression.Companion.aM());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((g)(new g() {
         public final void a(n varx) {
            var.c();
         }
      }));
      l.a(var, "view.onShowMeClicked()\n …cribe { view.openCard() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.b().subscribe((g)(new g() {
         public final void a(n varx) {
            var.finish();
         }
      }));
      l.a(var, "view.onNotNowClicked()\n …bscribe { view.finish() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005H&J\b\u0010\u0007\u001a\u00020\u0003H&J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH&¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "finish", "", "onNotNowClicked", "Lio/reactivex/Observable;", "onShowMeClicked", "openCard", "showSortCodeAndAccountNumber", "sortCode", "", "accountNumber", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends f {
      io.reactivex.n a();

      void a(String var, String var);

      io.reactivex.n b();

      void c();

      void finish();
   }
}
