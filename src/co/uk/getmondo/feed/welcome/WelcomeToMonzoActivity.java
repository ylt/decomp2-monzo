package co.uk.getmondo.feed.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.main.g;
import com.b.a.a.d;
import io.reactivex.n;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u00172\u00020\u00012\u00020\u0002:\u0001\u0017B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014J\b\u0010\u000e\u001a\u00020\u000bH\u0014J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\b\u0010\u0012\u001a\u00020\u000bH\u0016J\u0018\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;", "getPresenter", "()Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;", "setPresenter", "(Lco/uk/getmondo/feed/welcome/WelcomeToMonzoPresenter;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onNotNowClicked", "Lio/reactivex/Observable;", "onShowMeClicked", "openCard", "showSortCodeAndAccountNumber", "sortCode", "", "accountNumber", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class WelcomeToMonzoActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final WelcomeToMonzoActivity.a b = new WelcomeToMonzoActivity.a((i)null);
   public b a;
   private HashMap c;

   public static final Intent a(Context var) {
      l.b(var, "context");
      return b.a(var);
   }

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.welcomeShowMeButton)).map((h)d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(String var, String var) {
      l.b(var, "sortCode");
      l.b(var, "accountNumber");
      ((TextView)this.a(co.uk.getmondo.c.a.welcomeSortCodeTextView)).setText((CharSequence)var);
      ((TextView)this.a(co.uk.getmondo.c.a.welcomeAccountNumberTextView)).setText((CharSequence)var);
   }

   public n b() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.welcomeNotRightNowButton)).map((h)d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void c() {
      this.startActivity(HomeActivity.f.a((Context)this, g.c));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034231);
      this.l().a(this);
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/feed/welcome/WelcomeToMonzoActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, WelcomeToMonzoActivity.class);
      }
   }
}
