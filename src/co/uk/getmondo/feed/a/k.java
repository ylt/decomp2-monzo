package co.uk.getmondo.feed.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class k implements Callable {
   private final d a;
   private final String b;

   private k(d var, String var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(d var, String var) {
      return new k(var, var);
   }

   public Object call() {
      return d.b(this.a, this.b);
   }
}
