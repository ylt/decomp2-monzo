package co.uk.getmondo.feed.a.a;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0086\u0001\u0018\u0000 \u00152\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0015B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/feed/data/model/FeedItemType;", "", "type", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getType", "()Ljava/lang/String;", "TRANSACTION", "KYC_REQUEST", "KYC_PASSED", "KYC_REJECTED", "KYC_SDD_REQUEST", "KYC_SDD_APPROVED", "KYC_SDD_REJECTED", "GOLDEN_TICKET_AWARDED", "GOLDEN_TICKET_SUBSEQUENT", "MONTHLY_SPENDING_REPORT", "EDD_LIMITS", "WELCOME_FEED_ITEM", "BASIC_TITLE_AND_BODY", "UNKNOWN", "Find", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum a {
   a,
   b,
   c,
   d,
   e,
   f,
   g,
   h,
   i,
   j,
   k,
   l,
   m,
   n;

   public static final a.a o;
   private final String q;

   static {
      a var = new a("TRANSACTION", 0, "transaction");
      a = var;
      a var = new a("KYC_REQUEST", 1, "kyc_request");
      b = var;
      a var = new a("KYC_PASSED", 2, "kyc_passed");
      c = var;
      a var = new a("KYC_REJECTED", 3, "kyc_rejected");
      d = var;
      a var = new a("KYC_SDD_REQUEST", 4, "sdd_migration_request");
      e = var;
      a var = new a("KYC_SDD_APPROVED", 5, "sdd_migration_approved");
      f = var;
      a var = new a("KYC_SDD_REJECTED", 6, "sdd_migration_rejected");
      g = var;
      a var = new a("GOLDEN_TICKET_AWARDED", 7, "golden_ticket_awarded");
      h = var;
      a var = new a("GOLDEN_TICKET_SUBSEQUENT", 8, "golden_ticket_subsequent_award");
      i = var;
      a var = new a("MONTHLY_SPENDING_REPORT", 9, "spending_report");
      j = var;
      a var = new a("EDD_LIMITS", 10, "kyc_enhanced_limits");
      k = var;
      a var = new a("WELCOME_FEED_ITEM", 11, "onboarding_welcome");
      l = var;
      a var = new a("BASIC_TITLE_AND_BODY", 12, "basic_title_and_body");
      m = var;
      a var = new a("UNKNOWN", 13, "unknown");
      n = var;
      o = new a.a((i)null);
   }

   protected a(String var) {
      l.b(var, "type");
      super(var, var);
      this.q = var;
   }

   public final String a() {
      return this.q;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/feed/data/model/FeedItemType$Find;", "", "()V", "fromApiType", "Lco/uk/getmondo/feed/data/model/FeedItemType;", "type", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final a a(String var) {
         l.b(var, "type");
         Object[] var = (Object[])a.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(l.a(((a)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         a var = (a)var;
         if(var == null) {
            var = a.n;
         }

         return var;
      }
   }
}
