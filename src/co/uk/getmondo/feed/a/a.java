package co.uk.getmondo.feed.a;

import android.content.Context;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import kotlin.Metadata;
import kotlin.a.ab;
import kotlin.a.ah;
import kotlin.d.b.y;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0007H\u0007J\u000e\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0016J\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00070\u00192\u0006\u0010\u001a\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R/\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b0\u00068BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000bR/\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b0\u00068BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\r\u001a\u0004\b\u000f\u0010\u000b¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/feed/data/FeedItemResourceProvider;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "subtitlesMap", "", "Lco/uk/getmondo/feed/data/model/FeedItemType;", "", "kotlin.jvm.PlatformType", "getSubtitlesMap", "()Ljava/util/Map;", "subtitlesMap$delegate", "Lkotlin/Lazy;", "titlesMap", "getTitlesMap", "titlesMap$delegate", "getIconRes", "", "type", "getSubtitle", "feedItem", "Lco/uk/getmondo/model/FeedItem;", "getTitle", "searchInResources", "", "searchTerm", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new kotlin.d.b.w(y.a(a.class), "titlesMap", "getTitlesMap()Ljava/util/Map;")), (kotlin.reflect.l)y.a(new kotlin.d.b.w(y.a(a.class), "subtitlesMap", "getSubtitlesMap()Ljava/util/Map;"))};
   private final kotlin.c b;
   private final kotlin.c c;
   private final Context d;

   public a(Context var) {
      kotlin.d.b.l.b(var, "context");
      super();
      this.d = var;
      this.b = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final Map b() {
            return ab.a(new kotlin.h[]{kotlin.l.a(co.uk.getmondo.feed.a.a.a.b, a.this.d.getString(2131362323)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.c, a.this.d.getString(2131362318)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.h, a.this.d.getString(2131362204)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.i, a.this.d.getString(2131362210)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.k, a.this.d.getString(2131362167)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.d, a.this.d.getString(2131362354)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.e, a.this.d.getString(2131362372)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.g, a.this.d.getString(2131362370)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.f, a.this.d.getString(2131362368)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.j, a.this.d.getString(2131362454)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.l, a.this.d.getString(2131362503))});
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.c = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final Map b() {
            return ab.a(new kotlin.h[]{kotlin.l.a(co.uk.getmondo.feed.a.a.a.b, a.this.d.getString(2131362322)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.c, a.this.d.getString(2131362317)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.h, a.this.d.getString(2131362203)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.i, a.this.d.getString(2131362203)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.k, a.this.d.getString(2131362166)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.j, a.this.d.getString(2131362448)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.g, a.this.d.getString(2131362369)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.e, a.this.d.getString(2131362371)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.f, a.this.d.getString(2131362367)), kotlin.l.a(co.uk.getmondo.feed.a.a.a.l, a.this.d.getString(2131362502))});
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
   }

   private final Map a() {
      kotlin.c var = this.b;
      kotlin.reflect.l var = a[0];
      return (Map)var.a();
   }

   private final Map b() {
      kotlin.c var = this.c;
      kotlin.reflect.l var = a[1];
      return (Map)var.a();
   }

   public final int a(co.uk.getmondo.feed.a.a.a var) {
      kotlin.d.b.l.b(var, "type");
      int var;
      switch(b.a[var.ordinal()]) {
      case 1:
         var = 2130837884;
         break;
      default:
         var = 2130837982;
      }

      return var;
   }

   public final String a(co.uk.getmondo.d.m var) {
      String var;
      label38: {
         kotlin.d.b.l.b(var, "feedItem");
         co.uk.getmondo.feed.a.a.a var = var.j();
         if(var != null) {
            switch(b.b[var.ordinal()]) {
            case 1:
               co.uk.getmondo.d.v var = var.g();
               if(var == null) {
                  kotlin.d.b.l.a();
               }

               var = var.b().c().a(TextStyle.a, Locale.ENGLISH);
               var = this.d.getString(2131362454, new Object[]{var});
               break label38;
            case 2:
               co.uk.getmondo.d.f var = var.i();
               if(var != null) {
                  var = var.a();
                  if(var != null) {
                     break label38;
                  }
               }

               var = "";
               break label38;
            }
         }

         var = (String)this.a().get(var.j());
         if(var == null) {
            var = "";
         }
      }

      boolean var;
      if(((CharSequence)var).length() == 0) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         d.a.a.a((Throwable)(new IllegalArgumentException("There isn't title for feed item type " + var.j())));
      }

      kotlin.d.b.l.a(var, "title");
      return var;
   }

   public final Set a(String var) {
      kotlin.d.b.l.b(var, "searchTerm");
      Map var = this.a();
      Map var = (Map)(new LinkedHashMap());
      Iterator var = var.entrySet().iterator();

      Entry var;
      while(var.hasNext()) {
         var = (Entry)var.next();
         if(kotlin.h.j.b((CharSequence)var.getValue(), (CharSequence)var, true)) {
            var.put(var.getKey(), var.getValue());
         }
      }

      Set var = var.keySet();
      Map var = this.b();
      var = (Map)(new LinkedHashMap());
      Iterator var = var.entrySet().iterator();

      while(var.hasNext()) {
         var = (Entry)var.next();
         if(kotlin.h.j.b((CharSequence)var.getValue(), (CharSequence)var, true)) {
            var.put(var.getKey(), var.getValue());
         }
      }

      return ah.a(var, (Iterable)var.keySet());
   }

   public final String b(co.uk.getmondo.d.m var) {
      String var;
      label38: {
         kotlin.d.b.l.b(var, "feedItem");
         co.uk.getmondo.feed.a.a.a var = var.j();
         if(var != null) {
            switch(b.c[var.ordinal()]) {
            case 1:
               Context var = this.d;
               co.uk.getmondo.d.r var = var.h();
               if(var == null) {
                  kotlin.d.b.l.a();
               }

               var = var.getString(var.a().b());
               break label38;
            case 2:
               co.uk.getmondo.d.f var = var.i();
               if(var != null) {
                  var = var.b();
                  if(var != null) {
                     break label38;
                  }
               }

               var = "";
               break label38;
            }
         }

         var = (String)this.b().get(var.j());
         if(var == null) {
            var = "";
         }
      }

      boolean var;
      if(((CharSequence)var).length() == 0) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         d.a.a.a((Throwable)(new IllegalArgumentException("There isn't subtitle for feed item type " + var.j())));
      }

      kotlin.d.b.l.a(var, "subtitle");
      return var;
   }
}
