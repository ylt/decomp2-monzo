package co.uk.getmondo.feed.a;

import java.util.Map;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00010\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/feed/data/PeerManager;", "", "rxContacts", "Lco/uk/getmondo/payments/send/contacts/RxContacts;", "peerStorage", "Lco/uk/getmondo/feed/data/PeerStorage;", "(Lco/uk/getmondo/payments/send/contacts/RxContacts;Lco/uk/getmondo/feed/data/PeerStorage;)V", "keepPeersEnriched", "Lio/reactivex/Observable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class t {
   private final co.uk.getmondo.payments.send.a.e a;
   private final v b;

   public t(co.uk.getmondo.payments.send.a.e var, v var) {
      kotlin.d.b.l.b(var, "rxContacts");
      kotlin.d.b.l.b(var, "peerStorage");
      super();
      this.a = var;
      this.b = var;
   }

   public final io.reactivex.n a() {
      io.reactivex.n var = this.a.a().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(Map var) {
            kotlin.d.b.l.b(var, "it");
            return t.this.b.a(var).a((Object)Integer.valueOf(0)).f();
         }
      }));
      kotlin.d.b.l.a(var, "rxContacts.latestContact…fault(0).toObservable() }");
      return var;
   }
}
