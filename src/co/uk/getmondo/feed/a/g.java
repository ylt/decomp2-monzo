package co.uk.getmondo.feed.a;

import java.util.Date;
import java.util.List;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final String a;
   private final Date b;

   private g(String var, Date var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.h a(String var, Date var) {
      return new g(var, var);
   }

   public Object a(Object var) {
      return d.a(this.a, this.b, (List)var);
   }
}
