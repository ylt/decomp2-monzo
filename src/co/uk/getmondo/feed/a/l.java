package co.uk.getmondo.feed.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class l implements Callable {
   private final d a;
   private final String b;

   private l(d var, String var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(d var, String var) {
      return new l(var, var);
   }

   public Object call() {
      return d.a(this.a, this.b);
   }
}
