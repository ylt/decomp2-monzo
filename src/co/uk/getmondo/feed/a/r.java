package co.uk.getmondo.feed.a;

import io.realm.av;
import io.realm.bb;
import io.realm.bf;
import io.realm.bg;
import io.realm.bl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0001\u0018\u0000 %2\u00020\u0001:\u0001%B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fJ\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\fJ\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\nJ\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0014\u0010\u0013\u001a\u00020\b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u001aJ\u000e\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\u000eJ<\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f2\b\b\u0002\u0010\u001e\u001a\u00020\n2\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020!0 2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020#0 J \u0010$\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\nH\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006&"},
   d2 = {"Lco/uk/getmondo/feed/data/FeedStorage;", "", "()V", "feedMetadata", "Lco/uk/getmondo/model/FeedMetadata;", "getFeedMetadata", "()Lco/uk/getmondo/model/FeedMetadata;", "deleteFeedItem", "Lio/reactivex/Completable;", "feedItemId", "", "feedItems", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/data/QueryResults;", "Lco/uk/getmondo/model/FeedItem;", "knowYourCustomerRequestFeedItem", "mostRecentItemDateWithAccountId", "Ljava/util/Date;", "accountId", "saveFeed", "", "realm", "Lio/realm/Realm;", "feed", "Lco/uk/getmondo/feed/data/model/GetFeedResult;", "feedResults", "", "saveFeedItem", "feedItem", "searchFeedItems", "searchTerm", "types", "", "Lco/uk/getmondo/feed/data/model/FeedItemType;", "categories", "Lco/uk/getmondo/model/Category;", "updateIfDifferent", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class r {
   public static final r.a a = new r.a((kotlin.d.b.i)null);

   private final void a(av var, co.uk.getmondo.d.m var, String var) {
      co.uk.getmondo.d.m var = (co.uk.getmondo.d.m)var.a(co.uk.getmondo.d.m.class).a("id", var.a()).a("accountId", var).h();
      if(var == null || kotlin.d.b.l.a(var, (co.uk.getmondo.d.m)var.e((bb)var)) ^ true) {
         var.d((bb)var);
      }

   }

   private final void a(av var, co.uk.getmondo.feed.a.a.b var) {
      String[] var;
      String var;
      label36: {
         var = new String[var.b().size()];
         var = var.a();
         kotlin.f.a var = kotlin.f.d.a((kotlin.f.a)kotlin.a.m.a((Collection)var.b()));
         int var = var.a();
         int var = var.b();
         int var = var.c();
         if(var > 0) {
            if(var > var) {
               break label36;
            }
         } else if(var < var) {
            break label36;
         }

         while(true) {
            co.uk.getmondo.d.m var = (co.uk.getmondo.d.m)var.b().get(var);
            this.a(var, var, var);
            var[var] = var.a();
            if(var == var) {
               break;
            }

            var += var;
         }
      }

      if(var.c() != null) {
         bf var = var.a(co.uk.getmondo.d.m.class).a("accountId", var).a("created", var.c());
         boolean var;
         if(((Object[])var).length == 0) {
            var = true;
         } else {
            var = false;
         }

         if(!var) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            var.d().a("id", var);
         }

         var.b("id", "overdraft_charges_item_id");
         var.f().b();
      }

   }

   public final co.uk.getmondo.d.n a() {
      // $FF: Couldn't be decompiled
   }

   public final io.reactivex.b a(final co.uk.getmondo.d.m var) {
      kotlin.d.b.l.b(var, "feedItem");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            r var = r.this;
            kotlin.d.b.l.a(varx, "realm");
            co.uk.getmondo.d.m var = var;
            String var = var.b();
            kotlin.d.b.l.a(var, "feedItem.accountId");
            var.a(varx, var, var);
         }
      }));
   }

   public final io.reactivex.b a(final List var) {
      kotlin.d.b.l.b(var, "feedResults");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            Iterator var = var.iterator();

            while(var.hasNext()) {
               co.uk.getmondo.feed.a.a.b var = (co.uk.getmondo.feed.a.a.b)var.next();
               r var = r.this;
               kotlin.d.b.l.a(varx, "realm");
               var.a(varx, var);
            }

            varx.d((bb)(new co.uk.getmondo.d.n(new Date())));
         }
      }));
   }

   public final io.reactivex.n a(final String var, final Set var, final Set var) {
      kotlin.d.b.l.b(var, "searchTerm");
      kotlin.d.b.l.b(var, "types");
      kotlin.d.b.l.b(var, "categories");
      io.reactivex.n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final bg a(av varx) {
            kotlin.d.b.l.b(varx, "realm");
            bf var = varx.a(co.uk.getmondo.d.m.class);
            boolean varx;
            if(!kotlin.h.j.a((CharSequence)var)) {
               varx = true;
            } else {
               varx = false;
            }

            if(varx) {
               var.c("transaction.merchant.name", var, io.realm.l.b).c().c("transaction.merchant.formattedAddress", var, io.realm.l.b).c().c("transaction.merchant.emoji", var, io.realm.l.b).c().c("transaction.peer.peerName", var, io.realm.l.b).c().c("transaction.peer.contactName", var, io.realm.l.b).c().c("transaction.peer.phoneNumber", var, io.realm.l.b).c().c("transaction.declineReason", var, io.realm.l.b).c().c("transaction.notes", var, io.realm.l.b).c().c("transaction.description", var, io.realm.l.b).c().c("transaction.createdDateFormatted", var, io.realm.l.b).c().c("basicItemInfo.title", var, io.realm.l.b).c().c("basicItemInfo.subtitle", var, io.realm.l.b);
            }

            if(!((Collection)var).isEmpty()) {
               varx = true;
            } else {
               varx = false;
            }

            bf varx;
            Collection var;
            Iterable var;
            Object[] var;
            Iterator var;
            if(varx) {
               varx = var.c();
               var = (Iterable)var;
               var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
               var = var.iterator();

               while(var.hasNext()) {
                  var.add(((co.uk.getmondo.feed.a.a.a)var.next()).a());
               }

               var = (Collection)((List)var);
               var = var.toArray(new String[var.size()]);
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               varx.a("type", (String[])var);
            }

            if(!((Collection)var).isEmpty()) {
               varx = true;
            } else {
               varx = false;
            }

            if(varx) {
               varx = var.c();
               var = (Iterable)var;
               var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
               var = var.iterator();

               while(var.hasNext()) {
                  var.add(((co.uk.getmondo.d.h)var.next()).f());
               }

               var = (Collection)((List)var);
               var = var.toArray(new String[var.size()]);
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               varx.a("transaction.category", (String[])var);
            }

            return var.a("created", bl.b);
         }
      })).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "RxRealm.asObservable { r…}.map { it.queryResults }");
      return var;
   }

   public final Date a(String param1) {
      // $FF: Couldn't be decompiled
   }

   public final io.reactivex.b b(final String var) {
      kotlin.d.b.l.b(var, "feedItemId");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            varx.a(co.uk.getmondo.d.m.class).a("id", var).f().b();
         }
      }));
   }

   public final io.reactivex.n b() {
      io.reactivex.n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "RxRealm.asObservable { r…}.map { it.queryResults }");
      return var;
   }

   public final io.reactivex.n c() {
      io.reactivex.n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "RxRealm.asObservable { r….copyFirstFromRealm()!! }");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/feed/data/FeedStorage$Companion;", "", "()V", "FIELD_ACCOUNT_ID", "", "FIELD_CREATED", "FIELD_ID", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
