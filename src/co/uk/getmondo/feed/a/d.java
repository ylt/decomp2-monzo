package co.uk.getmondo.feed.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.feed.ApiFeedResponse;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.aj;
import io.reactivex.z;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class d {
   private final r a;
   private final MonzoApi b;
   private final co.uk.getmondo.d.a.f c;
   private final co.uk.getmondo.payments.send.payment_category.b d;
   private final a e;
   private final co.uk.getmondo.feed.search.a f;
   private final co.uk.getmondo.payments.send.a.e g;
   private final co.uk.getmondo.common.accounts.b h;
   private final io.reactivex.u i;

   public d(r var, MonzoApi var, co.uk.getmondo.d.a.f var, co.uk.getmondo.payments.send.payment_category.b var, a var, co.uk.getmondo.feed.search.a var, co.uk.getmondo.payments.send.a.e var, co.uk.getmondo.common.accounts.b var, io.reactivex.u var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.feed.a.a.b a(String var, Date var, List var) throws Exception {
      return new co.uk.getmondo.feed.a.a.b(var, var, var);
   }

   // $FF: synthetic method
   static io.reactivex.r a(ApiFeedResponse var) throws Exception {
      return io.reactivex.n.fromIterable(var.a());
   }

   // $FF: synthetic method
   static io.reactivex.r a(d var, String var) throws Exception {
      String var = var.trim();
      Set var = var.e.a(var);
      Set var = var.f.a(var);
      return var.a.a(var, var, var);
   }

   // $FF: synthetic method
   static List a(Map var, List var) throws Exception {
      Iterator var = var.iterator();

      while(var.hasNext()) {
         Iterator var = ((co.uk.getmondo.feed.a.a.b)var.next()).b().iterator();

         while(var.hasNext()) {
            ((co.uk.getmondo.d.m)var.next()).e().a(h.a(var));
         }
      }

      return var;
   }

   // $FF: synthetic method
   static List a(Object[] var) throws Exception {
      ArrayList var = new ArrayList(var.length);
      int var = var.length;

      for(int var = 0; var < var; ++var) {
         var.add((co.uk.getmondo.feed.a.a.b)var[var]);
      }

      return var;
   }

   // $FF: synthetic method
   static void a(Map var, aj var) {
      aa var = var.B();
      if(var != null && var.d() != null) {
         var.a((co.uk.getmondo.payments.send.a.b)var.get(var.d()));
      }

   }

   // $FF: synthetic method
   static boolean a(d var, co.uk.getmondo.d.m var) {
      return var.c(var);
   }

   // $FF: synthetic method
   static z b(d var, String var) throws Exception {
      String var = null;
      Date var = var.a.a(var);
      if(var != null) {
         var = co.uk.getmondo.common.c.a.a(co.uk.getmondo.common.c.a.b(var), co.uk.getmondo.common.c.a.a());
      } else {
         var = null;
      }

      if(var != null) {
         var = co.uk.getmondo.common.c.a.c(var);
      }

      io.reactivex.n var = var.b.feed(var, var).b(m.a());
      co.uk.getmondo.payments.send.payment_category.b var = var.d;
      var.getClass();
      var = var.flatMapSingle(n.a(var));
      co.uk.getmondo.d.a.f var = var.c;
      var.getClass();
      return var.map(o.a(var)).filter(p.a(var)).filter(f.a(var)).toList().d(g.a(var, var)).b(var.i);
   }

   private boolean b(co.uk.getmondo.d.m var) {
      boolean var;
      if(!this.h.b() && var.j() == co.uk.getmondo.feed.a.a.a.l) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   // $FF: synthetic method
   static boolean b(d var, co.uk.getmondo.d.m var) {
      return var.b(var);
   }

   private boolean c(co.uk.getmondo.d.m var) {
      boolean var;
      if(var.j() == co.uk.getmondo.feed.a.a.a.n || !var.e().b() && var.j() == co.uk.getmondo.feed.a.a.a.a) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   private io.reactivex.v d(String var) {
      return io.reactivex.v.a(k.a(this, var));
   }

   public io.reactivex.b a(co.uk.getmondo.d.m var) {
      return this.a.a(var);
   }

   public io.reactivex.b a(List var) {
      ArrayList var = new ArrayList();
      Iterator var = var.iterator();

      while(var.hasNext()) {
         var.add(this.d((String)var.next()));
      }

      io.reactivex.v var = io.reactivex.v.a(this.g.a().firstOrError(), io.reactivex.v.a((Iterable)var, (io.reactivex.c.h)e.a()), i.a());
      r var = this.a;
      var.getClass();
      return var.c(j.a(var));
   }

   public io.reactivex.n a() {
      return this.a.b();
   }

   public io.reactivex.n a(String var) {
      return io.reactivex.n.defer(l.a(this, var));
   }

   public io.reactivex.b b(String var) {
      return this.a.b(var).b((io.reactivex.d)this.b.deleteFeedItem(var));
   }

   public io.reactivex.n b() {
      return this.a.c();
   }

   public co.uk.getmondo.d.n c() {
      return this.a.a();
   }

   public io.reactivex.b c(String var) {
      return this.a.b(var);
   }
}
