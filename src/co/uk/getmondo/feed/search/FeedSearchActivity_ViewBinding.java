package co.uk.getmondo.feed.search;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class FeedSearchActivity_ViewBinding implements Unbinder {
   private FeedSearchActivity a;

   public FeedSearchActivity_ViewBinding(FeedSearchActivity var, View var) {
      this.a = var;
      var.searchView = (SearchView)Utils.findRequiredViewAsType(var, 2131820907, "field 'searchView'", SearchView.class);
      var.searchRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131820908, "field 'searchRecyclerView'", RecyclerView.class);
      var.noResults = Utils.findRequiredView(var, 2131820909, "field 'noResults'");
   }

   public void unbind() {
      FeedSearchActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.searchView = null;
         var.searchRecyclerView = null;
         var.noResults = null;
      }
   }
}
