package co.uk.getmondo.feed.search;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/feed/search/CategorySearcher;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "searchByName", "", "Lco/uk/getmondo/model/Category;", "searchTerm", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final Context a;

   public a(Context var) {
      kotlin.d.b.l.b(var, "context");
      super();
      this.a = var;
   }

   public final Set a(String var) {
      kotlin.d.b.l.b(var, "searchTerm");
      Object[] var = (Object[])co.uk.getmondo.d.h.values();
      Collection var = (Collection)(new ArrayList());

      for(int var = 0; var < var.length; ++var) {
         Object var = var[var];
         co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)var;
         if(kotlin.h.j.b((CharSequence)this.a.getString(var.a()), (CharSequence)var, true)) {
            var.add(var);
         }
      }

      return kotlin.a.m.m((Iterable)((List)var));
   }
}
