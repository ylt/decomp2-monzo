package co.uk.getmondo.feed.search;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;

class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.feed.a.d f;
   private final co.uk.getmondo.common.a g;

   g(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.feed.a.d var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(g var, g.a var, co.uk.getmondo.d.m var) throws Exception {
      return var.f.b(var.a()).b(var.d).a(m.a(var, var)).b();
   }

   // $FF: synthetic method
   static void a(g.a var, co.uk.getmondo.common.b.b var) throws Exception {
      if(var.a().isEmpty()) {
         var.c();
      } else {
         var.d();
         var.a(var);
      }

   }

   // $FF: synthetic method
   static void a(g var, g.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.W());
      io.reactivex.n var = var.a().observeOn(this.c);
      co.uk.getmondo.feed.a.d var = this.f;
      var.getClass();
      this.a((io.reactivex.b.b)var.switchMap(h.a(var)).subscribe(i.a(var), j.a()));
      this.a((io.reactivex.b.b)var.b().flatMapCompletable(k.a(this, var)).a(co.uk.getmondo.common.j.a.a(), l.a()));
   }

   public void b() {
      this.g.a(Impression.X());
      super.b();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.common.b.b var);

      io.reactivex.n b();

      void c();

      void d();
   }
}
