package co.uk.getmondo.feed.search;

public final class f implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new f(var, var, var);
   }

   public void a(FeedSearchActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (co.uk.getmondo.feed.adapter.a)this.b.b();
         var.b = (co.uk.getmondo.feed.c)this.c.b();
         var.c = (g)this.d.b();
      }
   }
}
