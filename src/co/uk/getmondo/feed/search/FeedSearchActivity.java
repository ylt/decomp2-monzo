package co.uk.getmondo.feed.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.r;
import java.util.concurrent.TimeUnit;

public class FeedSearchActivity extends co.uk.getmondo.common.activities.b implements co.uk.getmondo.feed.adapter.a.a, g.a {
   co.uk.getmondo.feed.adapter.a a;
   co.uk.getmondo.feed.c b;
   g c;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   @BindView(2131820909)
   View noResults;
   @BindView(2131820908)
   RecyclerView searchRecyclerView;
   @BindView(2131820907)
   SearchView searchView;

   public static Intent a(Context var, String var) {
      return (new Intent(var, FeedSearchActivity.class)).putExtra("EXTRA_QUERY", var).setFlags(67108864);
   }

   // $FF: synthetic method
   static r a(String var) throws Exception {
      io.reactivex.n var;
      if(TextUtils.isEmpty(var)) {
         var = io.reactivex.n.just(var);
      } else {
         var = io.reactivex.n.just(var).delay(400L, TimeUnit.MILLISECONDS);
      }

      return var;
   }

   // $FF: synthetic method
   static void a(FeedSearchActivity var) {
      var.searchRecyclerView.a(0);
   }

   // $FF: synthetic method
   static void b(FeedSearchActivity var) {
      var.searchView.clearFocus();
   }

   private void e() {
      this.searchView.setQueryHint(this.getString(2131362186));
      this.searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.c() {
         public boolean a(String var) {
            return false;
         }

         public boolean b(String var) {
            FeedSearchActivity.this.e.a((Object)var);
            return true;
         }
      });
   }

   public io.reactivex.n a() {
      return this.e.debounce(d.a());
   }

   public void a(co.uk.getmondo.common.b.b var) {
      this.a.a(var);
      this.searchRecyclerView.postDelayed(e.a(this), 50L);
   }

   public void a(co.uk.getmondo.d.m var) {
      this.b.a(var);
   }

   public io.reactivex.n b() {
      return this.f;
   }

   public void b(co.uk.getmondo.d.m var) {
      this.f.a((Object)var);
   }

   public void c() {
      this.noResults.setVisibility(0);
      this.noResults.animate().alpha(1.0F).setDuration(120L);
      this.searchRecyclerView.animate().alpha(0.0F).setDuration(120L);
   }

   public void d() {
      this.noResults.animate().cancel();
      this.noResults.setAlpha(0.0F);
      this.noResults.setVisibility(8);
      this.searchRecyclerView.animate().cancel();
      this.searchRecyclerView.setAlpha(1.0F);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034169);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.e();
      this.a.a((co.uk.getmondo.feed.adapter.a.a)this);
      LinearLayoutManager var = new LinearLayoutManager(this, 1, false);
      this.searchRecyclerView.setLayoutManager(var);
      this.searchRecyclerView.setAdapter(this.a);
      this.searchRecyclerView.setHasFixedSize(true);
      int var = this.getResources().getDimensionPixelSize(2131427609);
      this.searchRecyclerView.a(new co.uk.getmondo.common.ui.h(this, this.a, var));
      this.searchRecyclerView.a(new a.a.a.a.a.b(this.a));
      this.c.a((g.a)this);
      this.onNewIntent(this.getIntent());
   }

   protected void onDestroy() {
      this.c.b();
      super.onDestroy();
   }

   protected void onNewIntent(Intent var) {
      if(var.hasExtra("EXTRA_QUERY")) {
         String var = var.getStringExtra("EXTRA_QUERY");
         if(!TextUtils.isEmpty(var)) {
            this.searchView.a(var, false);
            this.searchView.post(c.a(this));
         }
      } else {
         this.e.a((Object)"");
      }

   }
}
