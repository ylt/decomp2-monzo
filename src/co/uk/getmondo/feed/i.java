package co.uk.getmondo.feed;

// $FF: synthetic class
final class i implements SpendingReportFeedbackDialogFragment.b {
   private final MonthlySpendingReportActivity a;

   private i(MonthlySpendingReportActivity var) {
      this.a = var;
   }

   public static SpendingReportFeedbackDialogFragment.b a(MonthlySpendingReportActivity var) {
      return new i(var);
   }

   public void a(SpendingReportFeedbackDialogFragment.a var) {
      MonthlySpendingReportActivity.a(this.a, var);
   }
}
