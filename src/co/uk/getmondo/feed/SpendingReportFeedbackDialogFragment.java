package co.uk.getmondo.feed;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SpendingReportFeedbackDialogFragment extends DialogFragment {
   private Unbinder a;
   private SpendingReportFeedbackDialogFragment.b b;
   @BindView(2131821260)
   TextView negativeFeedbackTextView;
   @BindView(2131821259)
   TextView neutralFeedbackTextView;
   @BindView(2131821258)
   TextView positiveFeedbackTextView;

   public static SpendingReportFeedbackDialogFragment a() {
      return new SpendingReportFeedbackDialogFragment();
   }

   public void a(SpendingReportFeedbackDialogFragment.b var) {
      this.b = var;
   }

   @OnClick({2131821258, 2131821259, 2131821260})
   void onCategoryClicked(TextView var) {
      if(this.b != null) {
         switch(var.getId()) {
         case 2131821258:
            this.b.a(SpendingReportFeedbackDialogFragment.a.a);
            break;
         case 2131821259:
            this.b.a(SpendingReportFeedbackDialogFragment.a.c);
            break;
         case 2131821260:
            this.b.a(SpendingReportFeedbackDialogFragment.a.b);
         }
      }

      this.getDialog().dismiss();
   }

   public Dialog onCreateDialog(Bundle var) {
      View var = LayoutInflater.from(this.getActivity()).inflate(2131034263, (ViewGroup)null, false);
      this.a = ButterKnife.bind(this, (View)var);
      this.positiveFeedbackTextView.setText(co.uk.getmondo.common.k.e.d());
      this.neutralFeedbackTextView.setText(co.uk.getmondo.common.k.e.e());
      this.negativeFeedbackTextView.setText(co.uk.getmondo.common.k.e.f());
      return (new Builder(this.getActivity())).setView(var).create();
   }

   public void onDestroyView() {
      this.a.unbind();
      super.onDestroyView();
   }

   public static enum a {
      a("positive"),
      b("negative"),
      c("neutral");

      private final String d;

      private a(String var) {
         this.d = var;
      }

      public String a() {
         return this.d;
      }
   }

   interface b {
      void a(SpendingReportFeedbackDialogFragment.a var);
   }
}
