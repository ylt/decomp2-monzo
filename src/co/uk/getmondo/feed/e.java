package co.uk.getmondo.feed;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.m;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.main.HomeActivity;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\u0012\u0010\"\u001a\u00020 2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u001a\u0010%\u001a\u00020 2\b\u0010&\u001a\u0004\u0018\u00010'2\u0006\u0010(\u001a\u00020)H\u0016J$\u0010*\u001a\u00020+2\u0006\u0010(\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010/\u001a\b\u0012\u0004\u0012\u00020 00H\u0016J\b\u00101\u001a\u00020 H\u0016J\u0010\u00102\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u0010\u00104\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u000e\u00105\u001a\b\u0012\u0004\u0012\u00020\u000f00H\u0016J\u0010\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J\u000e\u0010:\u001a\b\u0012\u0004\u0012\u00020 00H\u0016J\u0012\u0010;\u001a\u00020 2\b\u0010<\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010=\u001a\b\u0012\u0004\u0012\u00020\u001e00H\u0016J\u001a\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020+2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\b\u0010@\u001a\u00020 H\u0016J\u0010\u0010A\u001a\u00020 2\u0006\u0010B\u001a\u00020CH\u0016J\u0016\u0010D\u001a\u00020 2\f\u0010E\u001a\b\u0012\u0004\u0012\u00020\u000f0FH\u0016J\u0010\u0010G\u001a\u00020 2\u0006\u0010H\u001a\u000207H\u0016J\u0010\u0010I\u001a\u00020 2\u0006\u0010J\u001a\u00020CH\u0016J\b\u0010K\u001a\u00020 H\u0016J\u0016\u0010L\u001a\u00020 2\f\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00060NH\u0016J\b\u0010O\u001a\u00020 H\u0016J\b\u0010P\u001a\u00020 H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D¢\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR2\u0010\r\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR2\u0010\u001d\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e\u0018\u00010\u000e0\u000eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006Q"},
   d2 = {"Lco/uk/getmondo/feed/HomeFeedFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;", "()V", "FEED_STATE_KEY", "", "feedAdapter", "Lco/uk/getmondo/feed/adapter/FeedAdapter;", "getFeedAdapter", "()Lco/uk/getmondo/feed/adapter/FeedAdapter;", "setFeedAdapter", "(Lco/uk/getmondo/feed/adapter/FeedAdapter;)V", "feedItemDeletionRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lco/uk/getmondo/model/FeedItem;", "kotlin.jvm.PlatformType", "feedItemNavigator", "Lco/uk/getmondo/feed/FeedItemNavigator;", "getFeedItemNavigator", "()Lco/uk/getmondo/feed/FeedItemNavigator;", "setFeedItemNavigator", "(Lco/uk/getmondo/feed/FeedItemNavigator;)V", "homeFeedPresenter", "Lco/uk/getmondo/feed/HomeFeedPresenter;", "getHomeFeedPresenter", "()Lco/uk/getmondo/feed/HomeFeedPresenter;", "setHomeFeedPresenter", "(Lco/uk/getmondo/feed/HomeFeedPresenter;)V", "searchRelay", "", "hideForeignCurrencyAmounts", "", "hideProgressIndicator", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDateChanged", "Lio/reactivex/Observable;", "onDestroyView", "onFeedItemClicked", "feedItem", "onFeedItemDeleteRequested", "onFeedItemDeleted", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onRefreshAction", "onSaveInstanceState", "outState", "onSearch", "onViewCreated", "view", "openSearch", "setBalance", "balance", "Lco/uk/getmondo/model/Amount;", "setFeedItems", "feedItems", "Lco/uk/getmondo/common/data/QueryResults;", "setRefreshing", "refreshing", "setSpentToday", "spentToday", "showCardFrozen", "showForeignCurrencyAmounts", "amounts", "", "showProgressIndicator", "showTitle", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements co.uk.getmondo.feed.adapter.a.a, g.a {
   public co.uk.getmondo.feed.adapter.a a;
   public g c;
   public c d;
   private final String e = "FEED_STATE_KEY";
   private final com.b.b.c f = com.b.b.c.a();
   private final com.b.b.c g = com.b.b.c.a();
   private HashMap h;

   public View a(int var) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var = (View)this.h.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.h.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public n a() {
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            l.b(var, "emitter");
            ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)(new android.support.v4.widget.SwipeRefreshLayout.b() {
               public final void a() {
                  var.a(kotlin.n.a);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)null);
               }
            }));
         }
      }));
      l.a(var, "Observable.create { emit…istener(null) }\n        }");
      return var;
   }

   public void a(co.uk.getmondo.common.b.b var) {
      l.b(var, "feedItems");
      co.uk.getmondo.feed.adapter.a var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.a(var);
   }

   public void a(co.uk.getmondo.d.c var) {
      l.b(var, "balance");
      ((AmountView)this.a(co.uk.getmondo.c.a.balanceAmount)).setAmount(var);
   }

   public void a(m var) {
      l.b(var, "feedItem");
      c var = this.d;
      if(var == null) {
         l.b("feedItemNavigator");
      }

      var.a(var);
   }

   public void a(List var) {
      l.b(var, "amounts");
      ((TextView)this.a(co.uk.getmondo.c.a.spentTodayLabel)).setText((CharSequence)this.getString(2131362406, new Object[]{kotlin.a.m.a((Iterable)var, (CharSequence)" + ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null)}));
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendTodayBalanceView)).setVisibility(0);
   }

   public void a(final boolean var) {
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).post((Runnable)(new Runnable() {
         public final void run() {
            if((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout) != null) {
               ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setRefreshing(var);
            }

         }
      }));
   }

   public n b() {
      com.b.b.c var = this.f;
      l.a(var, "searchRelay");
      return (n)var;
   }

   public void b(co.uk.getmondo.d.c var) {
      l.b(var, "spentToday");
      ((AmountView)this.a(co.uk.getmondo.c.a.spentTodayAmount)).setAmount(var);
   }

   public void b(m var) {
      l.b(var, "feedItem");
      this.g.a((Object)var);
   }

   public n c() {
      com.b.b.c var = this.g;
      l.a(var, "feedItemDeletionRelay");
      return (n)var;
   }

   public void d() {
      this.startActivity(new Intent((Context)this.getActivity(), FeedSearchActivity.class));
   }

   public void e() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.progress)));
   }

   public void f() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.progress));
   }

   public void g() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362012);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689706));
   }

   public void h() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362481);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689593));
   }

   public n i() {
      n var = co.uk.getmondo.common.j.b.a(this.getContext(), new IntentFilter("android.intent.action.DATE_CHANGED")).map((io.reactivex.c.h)null.a);
      l.a(var, "RxBroadcastReceiver.crea…\n                .map { }");
      return var;
   }

   public void j() {
      ((TextView)this.a(co.uk.getmondo.c.a.spentTodayLabel)).setText((CharSequence)this.getString(2131362741));
   }

   public void k() {
      if(this.h != null) {
         this.h.clear();
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public void onCreateOptionsMenu(Menu var, MenuInflater var) {
      l.b(var, "inflater");
      super.onCreateOptionsMenu(var, var);
      var.inflate(2131951618, var);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      l.b(var, "inflater");
      View var = var.inflate(2131034270, var, false);
      l.a(var, "inflater.inflate(R.layou…t_feed, container, false)");
      return var;
   }

   public void onDestroyView() {
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)null);
      g var = this.c;
      if(var == null) {
         l.b("homeFeedPresenter");
      }

      var.b();
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setRefreshing(false);
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).destroyDrawingCache();
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).clearAnimation();
      super.onDestroyView();
      this.k();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      l.b(var, "item");
      boolean var;
      if(var.getItemId() == 2131821776) {
         this.f.a((Object)co.uk.getmondo.common.b.a.a);
         var = true;
      } else {
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   public void onSaveInstanceState(Bundle var) {
      super.onSaveInstanceState(var);
      if(var == null) {
         l.a();
      }

      var.putParcelable(this.e, ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).getLayoutManager().e());
   }

   public void onViewCreated(View var, Bundle var) {
      l.b(var, "view");
      super.onViewCreated(var, var);
      this.setHasOptionsMenu(true);
      final LinearLayoutManager var = new LinearLayoutManager(var.getContext(), 1, false);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var);
      co.uk.getmondo.feed.adapter.a var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.registerAdapterDataObserver((android.support.v7.widget.RecyclerView.c)(new android.support.v7.widget.RecyclerView.c() {
         public void b(int var, int var) {
            super.b(var, var);
            var.b(0, 0);
         }
      }));
      co.uk.getmondo.feed.adapter.a var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.a((co.uk.getmondo.feed.adapter.a.a)this);
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.setAdapter((android.support.v7.widget.RecyclerView.a)var);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setHasFixedSize(true);
      int var = this.getResources().getDimensionPixelSize(2131427609);
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      Context var = var.getContext();
      var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.h(var, (a.a.a.a.a.a)var, var)));
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      var = this.a;
      if(var == null) {
         l.b("feedAdapter");
      }

      var.a((android.support.v7.widget.RecyclerView.g)(new a.a.a.a.a.b((a.a.a.a.a.a)var)));
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setColorSchemeResources(new int[]{2131689558});
      if(var != null) {
         Parcelable var = var.getParcelable(this.e);
         ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).getLayoutManager().a(var);
      }

      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var = (HomeActivity)var;
         var.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         l.a(var, "toolbar");
         var.a(var);
         g var = this.c;
         if(var == null) {
            l.b("homeFeedPresenter");
         }

         var.a((g.a)this);
      }
   }
}
