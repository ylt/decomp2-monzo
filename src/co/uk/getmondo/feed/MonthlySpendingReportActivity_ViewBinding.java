package co.uk.getmondo.feed;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthlySpendingReportActivity_ViewBinding implements Unbinder {
   private MonthlySpendingReportActivity a;
   private View b;

   public MonthlySpendingReportActivity_ViewBinding(final MonthlySpendingReportActivity var, View var) {
      this.a = var;
      var.amountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821100, "field 'amountView'", AmountView.class);
      var.monthTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821101, "field 'monthTextView'", TextView.class);
      var = Utils.findRequiredView(var, 2131821103, "field 'goToSpendingButton' and method 'onGoToSpendingButtonClicked'");
      var.goToSpendingButton = (Button)Utils.castView(var, 2131821103, "field 'goToSpendingButton'", Button.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onGoToSpendingButtonClicked();
         }
      });
   }

   public void unbind() {
      MonthlySpendingReportActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountView = null;
         var.monthTextView = null;
         var.goToSpendingButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
