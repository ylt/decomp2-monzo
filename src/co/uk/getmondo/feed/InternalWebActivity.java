package co.uk.getmondo.feed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0012\u0010\t\u001a\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0015J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0002¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/feed/InternalWebActivity;", "Landroid/app/Activity;", "()V", "handleUri", "", "uri", "Landroid/net/Uri;", "onBackPressed", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "uriWithoutParam", "paramKey", "", "Companion", "CustomWebViewClient", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class InternalWebActivity extends Activity {
   public static final InternalWebActivity.a a = new InternalWebActivity.a((kotlin.d.b.i)null);
   private HashMap b;

   public static final Intent a(Context var, String var, String var) {
      l.b(var, "context");
      l.b(var, "url");
      return a.a(var, var, var);
   }

   private final Uri a(Uri var, String var) {
      Set var = var.getQueryParameterNames();
      Builder var = var.buildUpon().clearQuery();
      Iterable var = (Iterable)var;
      Collection var = (Collection)(new ArrayList());
      Iterator var = var.iterator();

      while(var.hasNext()) {
         Object var = var.next();
         if(l.a((String)var, var) ^ true) {
            var.add(var);
         }
      }

      Iterable var = (Iterable)((List)var);
      Iterator var = var.iterator();

      while(var.hasNext()) {
         String var = (String)var.next();
         var.appendQueryParameter(var, var.getQueryParameter(var));
      }

      var = var.build();
      l.a(var, "newUri.build()");
      return var;
   }

   private final boolean a(Uri var) {
      boolean var = true;
      if(MailTo.isMailTo(var.toString())) {
         this.startActivity(new Intent("android.intent.action.SENDTO", var));
      } else {
         if(var.isHierarchical()) {
            String var = var.getQueryParameter("monzo_action");
            if(var != null) {
               switch(var.hashCode()) {
               case -1820761141:
                  if(var.equals("external")) {
                     String var = this.a(var, "monzo_action").toString();
                     l.a(var, "uriWithoutParam(uri, MONZO_ACTION).toString()");
                     co.uk.getmondo.common.activities.a.a(this, var, 2131689487, true);
                     return var;
                  }
                  break;
               case 3015911:
                  if(var.equals("back")) {
                     this.finish();
                     return var;
                  }
               }
            }
         }

         var = false;
      }

      return var;
   }

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void onBackPressed() {
      if(((WebView)this.a(co.uk.getmondo.c.a.webView)).canGoBack()) {
         ((WebView)this.a(co.uk.getmondo.c.a.webView)).goBack();
      } else {
         super.onBackPressed();
      }

   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      int var;
      if(this.getIntent().getBooleanExtra("EXTRA_USE_DARK_THEME", false)) {
         var = 2131493156;
      } else {
         var = 2131493159;
      }

      this.setTheme(var);
      this.setContentView(2131034181);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).getSettings().setJavaScriptEnabled(true);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).getSettings().setAllowFileAccess(false);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).setWebViewClient((WebViewClient)(new InternalWebActivity.b()));
      ((Button)this.a(co.uk.getmondo.c.a.retryButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            ((WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView)).reload();
         }
      }));
      String var = this.getIntent().getStringExtra("EXTRA_URL");
      Uri var = Uri.parse(var);
      if(l.a(var.getQueryParameter("monzo_action"), "external")) {
         l.a(var, "uri");
         var = this.a(var, "monzo_action").toString();
         l.a(var, "uriWithoutParam(uri, MONZO_ACTION).toString()");
         co.uk.getmondo.common.activities.a.a(this, var, 2131689487, true);
         this.finish();
      } else {
         ((WebView)this.a(co.uk.getmondo.c.a.webView)).loadUrl(var);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/feed/InternalWebActivity$Companion;", "", "()V", "DARK_THEME_VALUE", "", "EXTRA_URL", "EXTRA_USE_DARK_THEME", "MONZO_ACTION", "MONZO_ACTION_BACK", "MONZO_ACTION_EXTERNAL", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "url", "style", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, String var, String var) {
         l.b(var, "context");
         l.b(var, "url");
         Intent var = new Intent(var, InternalWebActivity.class);
         var.putExtra("EXTRA_URL", var);
         var.putExtra("EXTRA_USE_DARK_THEME", l.a(var, "dark"));
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\"\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J(\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0018\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/feed/InternalWebActivity$CustomWebViewClient;", "Landroid/webkit/WebViewClient;", "(Lco/uk/getmondo/feed/InternalWebActivity;)V", "errorReceived", "", "onPageFinished", "", "view", "Landroid/webkit/WebView;", "url", "", "onPageStarted", "favicon", "Landroid/graphics/Bitmap;", "onReceivedError", "errorCode", "", "description", "failingUrl", "shouldOverrideUrlLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class b extends WebViewClient {
      private boolean b;

      public void onPageFinished(WebView var, String var) {
         byte var = 0;
         l.b(var, "view");
         l.b(var, "url");
         var = (WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView);
         byte var;
         if(this.b) {
            var = 8;
         } else {
            var = 0;
         }

         var.setVisibility(var);
         LinearLayout var = (LinearLayout)InternalWebActivity.this.a(co.uk.getmondo.c.a.errorLayout);
         if(this.b) {
            var = var;
         } else {
            var = 8;
         }

         var.setVisibility(var);
         ((ProgressBar)InternalWebActivity.this.a(co.uk.getmondo.c.a.progress)).setVisibility(8);
      }

      public void onPageStarted(WebView var, String var, Bitmap var) {
         l.b(var, "view");
         l.b(var, "url");
         this.b = false;
         ((ProgressBar)InternalWebActivity.this.a(co.uk.getmondo.c.a.progress)).setVisibility(0);
         ((WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView)).setVisibility(8);
         ((LinearLayout)InternalWebActivity.this.a(co.uk.getmondo.c.a.errorLayout)).setVisibility(8);
      }

      public void onReceivedError(WebView var, int var, String var, String var) {
         l.b(var, "view");
         l.b(var, "description");
         l.b(var, "failingUrl");
         this.b = true;
      }

      public boolean shouldOverrideUrlLoading(WebView var, String var) {
         l.b(var, "view");
         l.b(var, "url");
         InternalWebActivity var = InternalWebActivity.this;
         Uri var = Uri.parse(var);
         l.a(var, "Uri.parse(url)");
         return var.a(var);
      }
   }
}
