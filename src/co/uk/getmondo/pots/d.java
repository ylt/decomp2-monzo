package co.uk.getmondo.pots;

import co.uk.getmondo.common.v;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(b.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var) {
      return new d(var, var);
   }

   public b a() {
      return (b)b.a.c.a(this.b, new b((v)this.c.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
