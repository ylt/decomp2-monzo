package co.uk.getmondo.pots;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import io.reactivex.n;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0014\u001a\u00020\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\b\u0010\u0017\u001a\u00020\u0012H\u0014J\b\u0010\u0018\u001a\u00020\u0012H\u0016J\u0010\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u001eH\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/pots/CustomPotNameActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/pots/CustomPotNamePresenter$View;", "()V", "customNameEntered", "Lio/reactivex/Observable;", "", "getCustomNameEntered", "()Lio/reactivex/Observable;", "potNameChanges", "getPotNameChanges", "presenter", "Lco/uk/getmondo/pots/CustomPotNamePresenter;", "getPresenter", "()Lco/uk/getmondo/pots/CustomPotNamePresenter;", "setPresenter", "(Lco/uk/getmondo/pots/CustomPotNamePresenter;)V", "suggestionsClicks", "", "getSuggestionsClicks", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "openPotNameSuggestions", "openPotStyle", "potCreation", "Lco/uk/getmondo/pots/data/model/PotCreation;", "setNextButtonEnabled", "enabled", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CustomPotNameActivity extends co.uk.getmondo.common.activities.b implements f.a {
   public f a;
   private HashMap b;

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      com.b.a.a var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.potNameEditText));
      l.a(var, "RxTextView.textChanges(this)");
      n var = var.map((io.reactivex.c.h)null.a);
      l.a(var, "potNameEditText.textChan…s().map { it.toString() }");
      return var;
   }

   public void a(co.uk.getmondo.pots.a.a.a var) {
      l.b(var, "potCreation");
   }

   public void a(boolean var) {
      ((Button)this.a(co.uk.getmondo.c.a.nextButton)).setEnabled(var);
   }

   public n b() {
      n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.nameSuggestionsText)).map((io.reactivex.c.h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public n c() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.nextButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var) {
            l.b(var, "it");
            return ((TextInputEditText)CustomPotNameActivity.this.a(co.uk.getmondo.c.a.potNameEditText)).getText().toString();
         }
      }));
      l.a(var, "nextButton.clicks().map …ditText.text.toString() }");
      return var;
   }

   public void d() {
      Intent var = new Intent((Context)this, CreatePotActivity.class);
      var.addFlags(67108864);
      this.startActivity(var);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034159);
      this.l().a(this);
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((f.a)this);
   }

   protected void onDestroy() {
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }
}
