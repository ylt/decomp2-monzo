package co.uk.getmondo.pots;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(b.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b.a var) {
      return new g(var);
   }

   public f a() {
      return (f)b.a.c.a(this.b, new f());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
