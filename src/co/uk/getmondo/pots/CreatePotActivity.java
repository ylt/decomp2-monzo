package co.uk.getmondo.pots;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\b\u0010\u001b\u001a\u00020\u0018H\u0014J\u0010\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u0018H\u0016J\u0016\u0010 \u001a\u00020\u00182\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000e0\"H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006#"},
   d2 = {"Lco/uk/getmondo/pots/CreatePotActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/pots/CreatePotPresenter$View;", "()V", "gridLayoutManager", "Landroid/support/v7/widget/GridLayoutManager;", "getGridLayoutManager", "()Landroid/support/v7/widget/GridLayoutManager;", "gridLayoutManager$delegate", "Lkotlin/Lazy;", "potNameAdapter", "Lco/uk/getmondo/pots/PotNameAdapter;", "potNameClicks", "Lio/reactivex/Observable;", "Lco/uk/getmondo/pots/data/model/PotName;", "getPotNameClicks", "()Lio/reactivex/Observable;", "presenter", "Lco/uk/getmondo/pots/CreatePotPresenter;", "getPresenter", "()Lco/uk/getmondo/pots/CreatePotPresenter;", "setPresenter", "(Lco/uk/getmondo/pots/CreatePotPresenter;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "openPotStyle", "potCreation", "Lco/uk/getmondo/pots/data/model/PotCreation;", "showNameInput", "showPotNames", "potNames", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CreatePotActivity extends co.uk.getmondo.common.activities.b implements b.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(CreatePotActivity.class), "gridLayoutManager", "getGridLayoutManager()Landroid/support/v7/widget/GridLayoutManager;"))};
   public b b;
   private final h c = new h((List)null, (kotlin.d.a.b)null, 3, (i)null);
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final GridLayoutManager b() {
         return new GridLayoutManager((Context)CreatePotActivity.this, 2);
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap f;

   private final GridLayoutManager c() {
      kotlin.c var = this.e;
      l var = a[0];
      return (GridLayoutManager)var.a();
   }

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.f.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            kotlin.d.b.l.b(var, "emitter");
            CreatePotActivity.this.c.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(co.uk.getmondo.pots.a.a.b varx) {
                  kotlin.d.b.l.b(varx, "it");
                  var.a(varx);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  CreatePotActivity.this.c.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…stener = null }\n        }");
      return var;
   }

   public void a(co.uk.getmondo.pots.a.a.a var) {
      kotlin.d.b.l.b(var, "potCreation");
   }

   public void a(final List var) {
      kotlin.d.b.l.b(var, "potNames");
      this.c().a((android.support.v7.widget.GridLayoutManager.c)(new android.support.v7.widget.GridLayoutManager.c() {
         public int a(int varx) {
            if(varx == var.size() - 1 && varx % 2 == 0) {
               varx = CreatePotActivity.this.c().c();
            } else {
               varx = CreatePotActivity.this.c().c() / 2;
            }

            return varx;
         }
      }));
      this.c.a(var);
   }

   public void b() {
      this.startActivity(new Intent((Context)this, CustomPotNameActivity.class));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034157);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.potNameRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)this.c());
      ((RecyclerView)this.a(co.uk.getmondo.c.a.potNameRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.c);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.potNameRecyclerView)).setHasFixedSize(true);
      co.uk.getmondo.common.ui.d var = new co.uk.getmondo.common.ui.d(this.getResources().getDimensionPixelSize(2131427605), false, 2, (i)null);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.potNameRecyclerView)).a((android.support.v7.widget.RecyclerView.g)var);
      this.l().a(this);
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }
}
