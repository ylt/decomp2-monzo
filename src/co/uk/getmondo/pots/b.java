package co.uk.getmondo.pots;

import co.uk.getmondo.common.v;
import io.reactivex.n;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u000f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/pots/CreatePotPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/pots/CreatePotPresenter$View;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "(Lco/uk/getmondo/common/ResourceProvider;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final v c;

   public b(v var) {
      l.b(var, "resourceProvider");
      super();
      this.c = var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(co.uk.getmondo.pots.a.a.b.a.a(this.c));
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.pots.a.a.b varx) {
            co.uk.getmondo.pots.a.a.b.b var = varx.c();
            switch(c.a[var.ordinal()]) {
            case 1:
               var.a(new co.uk.getmondo.pots.a.a.a(varx.b()));
               break;
            case 2:
               var.b();
            }

         }
      }));
      l.a(var, "view.potNameClicks\n     …      }\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\bH&J\u0016\u0010\f\u001a\u00020\b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\u000eH&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/pots/CreatePotPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "potNameClicks", "Lio/reactivex/Observable;", "Lco/uk/getmondo/pots/data/model/PotName;", "getPotNameClicks", "()Lio/reactivex/Observable;", "openPotStyle", "", "potCreation", "Lco/uk/getmondo/pots/data/model/PotCreation;", "showNameInput", "showPotNames", "potNames", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      n a();

      void a(co.uk.getmondo.pots.a.a.a var);

      void a(List var);

      void b();
   }
}
