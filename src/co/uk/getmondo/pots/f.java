package co.uk.getmondo.pots;

import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0007\b\u0007¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/pots/CustomPotNamePresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/pots/CustomPotNamePresenter$View;", "()V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   public void a(final f.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            f.a var = var;
            l.a(varx, "it");
            var.a(varx.booleanValue());
         }
      }));
      l.a(var, "view.potNameChanges\n    …etNextButtonEnabled(it) }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            var.d();
         }
      }));
      l.a(var, "view.suggestionsClicks\n …penPotNameSuggestions() }");
      io.reactivex.rxkotlin.a.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            f.a var = var;
            l.a(varx, "it");
            var.a(new co.uk.getmondo.pots.a.a.a(varx));
         }
      }));
      l.a(var, "view.customNameEntered\n …tStyle(PotCreation(it)) }");
      io.reactivex.rxkotlin.a.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\f\u001a\u00020\nH&J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0010\u0010\u0010\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\u0012H&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006R\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0006¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/pots/CustomPotNamePresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "customNameEntered", "Lio/reactivex/Observable;", "", "getCustomNameEntered", "()Lio/reactivex/Observable;", "potNameChanges", "getPotNameChanges", "suggestionsClicks", "", "getSuggestionsClicks", "openPotNameSuggestions", "openPotStyle", "potCreation", "Lco/uk/getmondo/pots/data/model/PotCreation;", "setNextButtonEnabled", "enabled", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.pots.a.a.a var);

      void a(boolean var);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();
   }
}
