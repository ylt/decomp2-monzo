package co.uk.getmondo.pots.a.a;

import co.uk.getmondo.common.v;
import java.util.List;
import kotlin.Metadata;
import kotlin.c;
import kotlin.d;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.d.b.u;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/pots/data/model/PotName;", "", "emoji", "", "label", "type", "Lco/uk/getmondo/pots/data/model/PotName$Type;", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/data/model/PotName$Type;)V", "getEmoji", "()Ljava/lang/String;", "getLabel", "getType", "()Lco/uk/getmondo/pots/data/model/PotName$Type;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "Type", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   public static final b.a a = new b.a((i)null);
   private final String b;
   private final String c;
   private final b.b d;

   public b(String var, String var, b.b var) {
      l.b(var, "emoji");
      l.b(var, "label");
      l.b(var, "type");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   public b(String var, String var, b.b var, int var, i var) {
      if((var & 4) != 0) {
         var = b.b.a;
      }

      this(var, var, var);
   }

   public final String a() {
      return this.b;
   }

   public final String b() {
      return this.c;
   }

   public final b.b c() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof b) {
               b var = (b)var;
               if(l.a(this.b, var.b) && l.a(this.c, var.c) && l.a(this.d, var.d)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      b.b var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "PotName(emoji=" + this.b + ", label=" + this.c + ", type=" + this.d + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007¨\u0006\b²\u0006\u0013\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u008a\u0084\u0002¢\u0006\u0000"},
      d2 = {"Lco/uk/getmondo/pots/data/model/PotName$Companion;", "", "()V", "values", "", "Lco/uk/getmondo/pots/data/model/PotName;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      // $FF: synthetic field
      static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new u(y.a(b.a.class), "values", "<v#0>"))};

      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final List a(final v var) {
         l.b(var, "resourceProvider");
         c var = d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
            public final List b() {
               return m.b(new b[]{new b("\ud83d\udcb0", var.a(2131362586), (b.b)null, 4, (i)null), new b("✈️", var.a(2131362583), (b.b)null, 4, (i)null), new b("\ud83c\udf27", var.a(2131362585), (b.b)null, 4, (i)null), new b("\ud83c\udfe1", var.a(2131362584), (b.b)null, 4, (i)null), new b("\ud83c\udf84", var.a(2131362581), (b.b)null, 4, (i)null), new b("\ud83c\udfae", var.a(2131362582), (b.b)null, 4, (i)null), new b("\ud83d\udd8b️", var.a(2131362587), b.b.b)});
            }

            // $FF: synthetic method
            public Object v_() {
               return this.b();
            }
         }));
         kotlin.reflect.l var = a[0];
         return (List)var.a();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/pots/data/model/PotName$Type;", "", "(Ljava/lang/String;I)V", "SUGGESTED", "CUSTOM", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum b {
      a,
      b;

      static {
         b.b var = new b.b("SUGGESTED", 0);
         a = var;
         b.b var = new b.b("CUSTOM", 1);
         b = var;
      }
   }
}
