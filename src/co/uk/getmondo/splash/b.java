package co.uk.getmondo.splash;

import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.ak;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/splash/SplashPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/splash/SplashPresenter$View;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "signupStatusManager", "Lco/uk/getmondo/signup/status/SignupStatusManager;", "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/signup/status/SignupStatusManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final d c;
   private final co.uk.getmondo.signup.status.b d;

   public b(d var, co.uk.getmondo.signup.status.b var) {
      l.b(var, "accountService");
      l.b(var, "signupStatusManager");
      super();
      this.c = var;
      this.d = var;
   }

   public void a(b.a var) {
      l.b(var, "view");
      super.a((f)var);
      ak var = this.c.b();
      if(var == null) {
         var.a();
      } else {
         label19: {
            if(!co.uk.getmondo.a.c.booleanValue()) {
               co.uk.getmondo.d.a var = var.c();
               if(var != null && var.f()) {
                  break label19;
               }
            }

            if(!this.d.a()) {
               var.c();
               return;
            }
         }

         var.b();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/splash/SplashPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "openHome", "", "openOnboarding", "openSignupStatus", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends f {
      void a();

      void b();

      void c();
   }
}
