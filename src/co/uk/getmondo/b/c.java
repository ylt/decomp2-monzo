package co.uk.getmondo.b;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import co.uk.getmondo.api.model.tracking.Impression;

public class c implements ActivityLifecycleCallbacks {
   private final co.uk.getmondo.common.a a;
   private int b = 0;

   c(co.uk.getmondo.common.a var) {
      this.a = var;
   }

   private boolean a(Activity var) {
      return var.getClass().getName().startsWith("co.uk.getmondo");
   }

   public void onActivityCreated(Activity var, Bundle var) {
   }

   public void onActivityDestroyed(Activity var) {
   }

   public void onActivityPaused(Activity var) {
   }

   public void onActivityResumed(Activity var) {
   }

   public void onActivitySaveInstanceState(Activity var, Bundle var) {
   }

   public void onActivityStarted(Activity var) {
      ++this.b;
      if(this.b == 1 && this.a(var)) {
         this.a.a(Impression.b());
      }

   }

   public void onActivityStopped(Activity var) {
      if(this.b > 0) {
         --this.b;
      }

      if(this.b == 0 && this.a(var)) {
         this.a.a(Impression.c());
      }

   }
}
