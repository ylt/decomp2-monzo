package co.uk.getmondo.b;

import android.app.NotificationManager;
import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build.VERSION;
import co.uk.getmondo.api.model.tracking.Impression;

public class a {
   private final android.support.v4.b.b.a a;
   private final co.uk.getmondo.common.a b;

   a(android.support.v4.b.b.a var, co.uk.getmondo.common.a var) {
      this.a = var;
      this.b = var;
   }

   private Impression.FingerprintSupportType a() {
      Impression.FingerprintSupportType var;
      if(!this.a.b()) {
         var = Impression.FingerprintSupportType.NO_HARDWARE_DETECTED;
      } else if(!this.a.a()) {
         var = Impression.FingerprintSupportType.NO_ENROLLED_FINGERPRINTS;
      } else {
         var = Impression.FingerprintSupportType.READY_TO_USE;
      }

      return var;
   }

   private int b(Context var) {
      int var;
      if(VERSION.SDK_INT >= 23) {
         var = ((NotificationManager)var.getSystemService("notification")).getActiveNotifications().length;
      } else {
         var = -1;
      }

      return var;
   }

   private boolean c(Context var) {
      NfcAdapter var = ((NfcManager)var.getSystemService("nfc")).getDefaultAdapter();
      boolean var;
      if(var != null && var.isEnabled()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(Context var) {
      try {
         Impression var = Impression.a(this.a(), this.b(var), this.c(var));
         this.b.a(var);
      } catch (Throwable var) {
         d.a.a.a(var, "Failed to send analytic", new Object[0]);
      }

   }
}
