package co.uk.getmondo.common;

import android.app.Application;
import co.uk.getmondo.api.MonzoApi;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B!\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\nJ\u000e\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\rJ\u0018\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/IntercomService;", "", "application", "Landroid/app/Application;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "ioScheduler", "Lio/reactivex/Scheduler;", "(Landroid/app/Application;Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/Scheduler;)V", "clearRegisteredUser", "", "composeMessage", "message", "", "openIntercom", "registerUser", "userId", "updateUser", "profile", "Lco/uk/getmondo/model/Profile;", "account", "Lco/uk/getmondo/model/Account;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q {
   private final MonzoApi a;
   private final io.reactivex.u b;

   public q(Application var, MonzoApi var, io.reactivex.u var) {
      kotlin.d.b.l.b(var, "application");
      kotlin.d.b.l.b(var, "monzoApi");
      kotlin.d.b.l.b(var, "ioScheduler");
      super();
      this.a = var;
      this.b = var;
      Intercom.initialize(var, "android_sdk-33cefb6b165cfd2bc9843a6e33d8bb1201552534", "n3exvq9z");
   }

   public final void a() {
      Intercom.client().displayConversationsList();
   }

   public final void a(co.uk.getmondo.d.ac var, co.uk.getmondo.d.a var) {
      kotlin.d.b.l.b(var, "profile");
      Boolean var = co.uk.getmondo.a.b;
      kotlin.d.b.l.a(var, "BuildConfig.GHOST");
      if(!var.booleanValue()) {
         UserAttributes.Builder var = (new UserAttributes.Builder()).withName(var.c()).withEmail(var.d());
         if(var != null) {
            var.withCustomAttribute("account_id", var.a());
         }

         String var;
         String var;
         String var;
         label16: {
            Intercom.client().updateUser(var.build());
            var = var.c();
            var = var.d();
            if(var != null) {
               var = var.a();
               if(var != null) {
                  break label16;
               }
            }

            var = "";
         }

         d.a.a.b("Intercom user updated | name: %s email: %s accountID: %s", new Object[]{var, var, var});
      }

   }

   public final void a(String var) {
      kotlin.d.b.l.b(var, "userId");
      Boolean var = co.uk.getmondo.a.b;
      kotlin.d.b.l.a(var, "BuildConfig.GHOST");
      if(!var.booleanValue()) {
         Registration var = Registration.create().withUserId(var);
         Intercom.client().registerIdentifiedUser(var);
         d.a.a.b("User registered with Intercom | userId: %s", new Object[]{var.getUserId()});
         this.a.intercomUserHash("n3exvq9z").a(3L).d((io.reactivex.c.h)null.a).b(this.b).a((io.reactivex.c.g)null.a, (io.reactivex.c.g)null.a);
      }

   }

   public final void b() {
      Intercom.client().reset();
   }

   public final void b(String var) {
      kotlin.d.b.l.b(var, "message");
      Intercom.client().displayMessageComposer(var);
   }
}
