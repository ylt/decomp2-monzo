package co.uk.getmondo.common;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a(\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00020\u0001*\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¨\u0006\u0006"},
   d2 = {"changes", "Lio/reactivex/Observable;", "Lkotlin/Pair;", "Landroid/content/SharedPreferences;", "", "filterKey", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class ac {
   public static final io.reactivex.n a(final SharedPreferences var, final String var) {
      kotlin.d.b.l.b(var, "$receiver");
      io.reactivex.n var = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o varx) {
            kotlin.d.b.l.b(varx, "emitter");
            final OnSharedPreferenceChangeListener var = (OnSharedPreferenceChangeListener)(new OnSharedPreferenceChangeListener() {
               public final void onSharedPreferenceChanged(SharedPreferences varxx, String var) {
                  if(var == null || kotlin.d.b.l.a(var, var)) {
                     varx.a(new kotlin.h(varxx, var));
                  }

               }
            });
            var.registerOnSharedPreferenceChangeListener(var);
            varx.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var.unregisterOnSharedPreferenceChangeListener(var);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…istener(listener) }\n    }");
      return var;
   }
}
