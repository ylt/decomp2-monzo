package co.uk.getmondo.common.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final a a;
   private final Activity b;

   private b(a var, Activity var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(a var, Activity var) {
      return new b(var, var);
   }

   public void onClick(DialogInterface var, int var) {
      a.a(this.a, this.b, var, var);
   }
}
