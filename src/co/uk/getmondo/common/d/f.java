package co.uk.getmondo.common.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class f implements OnClickListener {
   private final Activity a;

   private f(Activity var) {
      this.a = var;
   }

   public static OnClickListener a(Activity var) {
      return new f(var);
   }

   public void onClick(DialogInterface var, int var) {
      e.a(this.a, var, var);
   }
}
