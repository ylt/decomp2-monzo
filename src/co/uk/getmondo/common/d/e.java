package co.uk.getmondo.common.d;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import co.uk.getmondo.MonzoApplication;

public class e extends DialogFragment {
   private boolean a;

   public static e a() {
      return a(false);
   }

   public static e a(boolean var) {
      e var = new e();
      Bundle var = new Bundle();
      var.putBoolean("ARGUMENT_FINISH_ACTIVITY", var);
      var.setArguments(var);
      return var;
   }

   // $FF: synthetic method
   static void a(Activity var, DialogInterface var, int var) {
      MonzoApplication.a(var).b().p().a();
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      var = this.getArguments();
      boolean var;
      if(var != null && var.getBoolean("ARGUMENT_FINISH_ACTIVITY")) {
         var = true;
      } else {
         var = false;
      }

      this.a = var;
   }

   public Dialog onCreateDialog(Bundle var) {
      Activity var = this.getActivity();
      return (new Builder(var)).setTitle(2131362567).setMessage(2131362562).setPositiveButton(this.getString(2131362085), f.a(var)).setCancelable(false).setNegativeButton(this.getString(2131362499), (OnClickListener)null).create();
   }

   public void onDismiss(DialogInterface var) {
      super.onDismiss(var);
      if(this.a) {
         this.getActivity().finish();
      }

   }
}
