package co.uk.getmondo.common.d;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.i;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.q;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\u0012\u0010\r\u001a\u00020\u000e2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/common/dialog/ForgotPinDialogFragment;", "Landroid/support/v4/app/DialogFragment;", "()V", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateDialog", "Landroid/app/Dialog;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends i {
   public q a;
   private HashMap b;

   public final q a() {
      q var = this.a;
      if(var == null) {
         l.b("intercomService");
      }

      return var;
   }

   public void b() {
      if(this.b != null) {
         this.b.clear();
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a((Context)this.getActivity()).b()).a(new co.uk.getmondo.common.h.b.c((Activity)this.getActivity())).a().a(this);
   }

   public Dialog onCreateDialog(Bundle var) {
      AlertDialog var = (new Builder((Context)this.getActivity())).setTitle(2131362193).setMessage(2131362194).setPositiveButton((CharSequence)this.getString(2131362091), (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            c.this.a().a();
         }
      })).setNegativeButton((CharSequence)this.getString(2131362499), (OnClickListener)null).create();
      l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.b();
   }
}
