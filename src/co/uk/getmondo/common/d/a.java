package co.uk.getmondo.common.d;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.i;
import android.support.v4.app.j;

public class a extends i {
   public static a a(String var) {
      return a("", var);
   }

   public static a a(String var, String var) {
      return a(var, var, true);
   }

   public static a a(String var, String var, int var, boolean var) {
      a var = new a();
      Bundle var = new Bundle();
      var.putString("ARGUMENT_TITLE", var);
      var.putString("ARGUMENT_MESSAGE", var);
      var.putInt("ARGUMENT_BUTTON_TEXT", var);
      var.putBoolean("ARGUMENT_FINISH_ACTIVITY", var);
      var.setArguments(var);
      return var;
   }

   public static a a(String var, String var, boolean var) {
      return a(var, var, 17039370, var);
   }

   // $FF: synthetic method
   static void a(a var, Activity var, DialogInterface var, int var) {
      if(var.getArguments().getBoolean("ARGUMENT_FINISH_ACTIVITY")) {
         var.finish();
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      j var = this.getActivity();
      return (new android.support.v7.app.d.a(var, 2131493138)).a(this.getArguments().getString("ARGUMENT_TITLE")).b(this.getArguments().getString("ARGUMENT_MESSAGE")).a(this.getArguments().getInt("ARGUMENT_BUTTON_TEXT"), b.a(this, var)).b();
   }
}
