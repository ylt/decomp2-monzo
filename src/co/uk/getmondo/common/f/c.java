package co.uk.getmondo.common.f;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import co.uk.getmondo.common.ui.i;

public class c {
   private final Context a;
   private final View b;
   private ProgressDialog c;
   private Snackbar d;

   public c(Context var, View var) {
      this.a = var;
      this.b = var;
      if(var == null) {
         throw new RuntimeException("The view passed into the UiMessageHelper cannot be null");
      }
   }

   // $FF: synthetic method
   static void a(c.a var, View var) {
      var.a();
   }

   // $FF: synthetic method
   static void b(c.a var, View var) {
      var.a();
   }

   public void a() {
      this.c(this.a.getString(2131362402));
   }

   public void a(int var, String var) {
      this.b(this.a.getString(var, new Object[]{var}), (String)null, (c.a)null);
   }

   public void a(String var) {
      i.a(this.b.getContext(), this.b, var, 0, false).c();
   }

   public void a(String var, String var, c.a var) {
      Snackbar var = i.a(this.b.getContext(), this.b, var, -2, false);
      var.a(var, d.a(var));
      var.c();
   }

   public Snackbar b(String var, String var, c.a var) {
      if(this.d == null) {
         this.d = i.a(this.b.getContext(), this.b, var, -2, false);
      } else {
         this.d.a(var);
      }

      if(var != null && var != null) {
         this.d.a(var, e.a(var));
      }

      if(!this.d.e()) {
         this.d.c();
      }

      return this.d;
   }

   public void b() {
      if(this.c != null) {
         this.c.dismiss();
      }

   }

   public void b(String var) {
      this.a(var);
   }

   public void c(String var) {
      try {
         if(this.c == null) {
            ContextThemeWrapper var = new ContextThemeWrapper(this.a, 2131493134);
            ProgressDialog var = new ProgressDialog(var);
            this.c = var;
         }

         this.c.setTitle("");
         this.c.setMessage(var);
         this.c.setIndeterminate(true);
         this.c.show();
      } catch (Exception var) {
         d.a.a.a(var, "Problem showing progress dialog", new Object[0]);
      }

   }

   public interface a {
      void a();
   }
}
