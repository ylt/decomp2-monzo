package co.uk.getmondo.common.f;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final c.a a;

   private d(c.a var) {
      this.a = var;
   }

   public static OnClickListener a(c.a var) {
      return new d(var);
   }

   public void onClick(View var) {
      c.b(this.a, var);
   }
}
