package co.uk.getmondo.common.f;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.splash.SplashActivity;

public class a extends Fragment implements co.uk.getmondo.common.e.a.a {
   private co.uk.getmondo.common.h.b.b a;
   protected com.c.b.b b = com.c.b.b.c();

   // $FF: synthetic method
   static void a(String var, c var) {
      var.b(var);
   }

   protected co.uk.getmondo.common.h.b.b B() {
      return this.a;
   }

   public void b(int var) {
      this.d(this.getString(var));
   }

   public void c(int var) {
      this.startActivity(SplashActivity.a(this.getActivity(), this.getString(var)));
   }

   public void d(String var) {
      this.b.a(b.a(var));
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.a = co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a(this.getActivity()).b()).a(new co.uk.getmondo.common.h.b.c(this.getActivity())).a();
   }

   public void onDestroyView() {
      this.b = com.c.b.b.c();
      super.onDestroyView();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.b = com.c.b.b.b(new c(var.getContext(), var));
   }

   public void q() {
      this.startActivity(ForceUpgradeActivity.a((Context)this.getActivity()));
   }

   public void u() {
      SplashActivity.a(this.getActivity());
   }
}
