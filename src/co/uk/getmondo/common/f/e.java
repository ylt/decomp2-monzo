package co.uk.getmondo.common.f;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final c.a a;

   private e(c.a var) {
      this.a = var;
   }

   public static OnClickListener a(c.a var) {
      return new e(var);
   }

   public void onClick(View var) {
      c.a(this.a, var);
   }
}
