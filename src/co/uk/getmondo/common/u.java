package co.uk.getmondo.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class u implements s {
   private final SharedPreferences a;

   public u(Context var) {
      this.a = var.getSharedPreferences("co.uk.getmondo", 0);
   }

   public String a() {
      return this.f("EMAIL_KEY");
   }

   public void a(String var) {
      this.a(var, "EMAIL_KEY");
   }

   protected void a(String var, String var) {
      Editor var = this.a.edit();
      if(var == null) {
         var.remove(var);
      } else {
         var.putString(var, var);
      }

      var.apply();
   }

   public void a(boolean var) {
      this.a.edit().putBoolean("FIRST_TIME_WAITING_LIST_KEY", var).apply();
   }

   public String b() {
      return this.f("OAUTH_STATE_KEY");
   }

   public void b(String var) {
      this.a(var, "OAUTH_STATE_KEY");
   }

   public void b(boolean var) {
      this.a.edit().putBoolean("NOTIFY_WHEN_READY_KEY", var).apply();
   }

   public String c() {
      return this.f("SESSION_KEY");
   }

   public void c(String var) {
      this.a(var, "SESSION_KEY");
   }

   public String d() {
      return this.f("FINGER_PRINT_KEY");
   }

   public void d(String var) {
      this.a(var, "FINGER_PRINT_KEY");
   }

   public void e(String var) {
      this.a(var, "READ_EVENTS_KEY");
   }

   public boolean e() {
      return this.a.getBoolean("FIRST_TIME_WAITING_LIST_KEY", true);
   }

   public String f() {
      String var = this.f("READ_EVENTS_KEY");
      String var = var;
      if(var == null) {
         var = "";
      }

      return var;
   }

   protected String f(String var) {
      return this.a.getString(var, (String)null);
   }

   public boolean g() {
      return this.a.getBoolean("NOTIFY_WHEN_READY_KEY", false);
   }
}
