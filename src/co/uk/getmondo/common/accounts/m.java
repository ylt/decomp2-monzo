package co.uk.getmondo.common.accounts;

import co.uk.getmondo.d.ac;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/accounts/ProfileAndAccountInfoZipper;", "", "()V", "zip", "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;", "accounts", "", "Lco/uk/getmondo/model/Account;", "profileOptional", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/model/Profile;", "isPrepaidAccountMigrated", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class m {
   public final l a(List var, com.c.b.b var, boolean var) {
      Object var = null;
      kotlin.d.b.l.b(var, "accounts");
      kotlin.d.b.l.b(var, "profileOptional");
      co.uk.getmondo.d.a var = (co.uk.getmondo.d.a)null;
      String var = (String)null;
      Boolean var = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var, "BuildConfig.BANK");
      Object var;
      co.uk.getmondo.d.a var;
      Iterator var;
      if(var.booleanValue()) {
         var = ((Iterable)var).iterator();

         do {
            if(!var.hasNext()) {
               var = null;
               break;
            }

            var = var.next();
         } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var).d(), co.uk.getmondo.d.a.b.RETAIL));

         var = (co.uk.getmondo.d.a)var;
      } else if(var.size() == 1) {
         var = (co.uk.getmondo.d.a)var.get(0);
      } else if(var.size() > 1) {
         if(var) {
            var = ((Iterable)var).iterator();

            Object var;
            do {
               if(!var.hasNext()) {
                  var = null;
                  break;
               }

               var = var.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var).d(), co.uk.getmondo.d.a.b.RETAIL));

            var = (co.uk.getmondo.d.a)var;
            Iterator var = ((Iterable)var).iterator();

            do {
               if(!var.hasNext()) {
                  var = null;
                  break;
               }

               var = var.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var).d(), co.uk.getmondo.d.a.b.PREPAID));

            var = (co.uk.getmondo.d.a)var;
            String var;
            if(var != null) {
               var = var.a();
            } else {
               var = null;
            }

            var = var;
            var = var;
         } else {
            var = ((Iterable)var).iterator();

            do {
               if(!var.hasNext()) {
                  var = null;
                  break;
               }

               var = var.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var).d(), co.uk.getmondo.d.a.b.PREPAID));

            var = (co.uk.getmondo.d.a)var;
         }
      } else {
         var = var;
      }

      ac var;
      if(var.d()) {
         var = (ac)var;
      } else {
         var = (ac)var.a();
      }

      return new l(var, var, var);
   }
}
