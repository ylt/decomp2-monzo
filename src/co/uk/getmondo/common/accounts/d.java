package co.uk.getmondo.common.accounts;

import co.uk.getmondo.common.q;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import io.reactivex.u;
import io.reactivex.v;
import io.realm.av;

public class d {
   private final k a;
   private final u b;
   private final q c;
   private final co.uk.getmondo.waitlist.i d;
   private final co.uk.getmondo.payments.send.payment_category.b.a e;
   private final co.uk.getmondo.settings.k f;
   private final co.uk.getmondo.signup.identity_verification.a.h g;
   private final co.uk.getmondo.signup.status.g h;
   private final co.uk.getmondo.migration.d i;

   public d(k var, u var, co.uk.getmondo.waitlist.i var, q var, co.uk.getmondo.payments.send.payment_category.b.a var, co.uk.getmondo.settings.k var, co.uk.getmondo.signup.identity_verification.a.h var, co.uk.getmondo.signup.status.g var, co.uk.getmondo.migration.d var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static void a(av var) {
      var.l();
   }

   // $FF: synthetic method
   static void f() throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void g() {
      io.reactivex.b.a(h.b()).b(this.b).c();
   }

   public void a(l var) {
      synchronized(this){}

      try {
         ak var = this.a.c();
         this.a.a(var.a(var));
      } finally {
         ;
      }

   }

   public void a(co.uk.getmondo.d.a var) {
      synchronized(this){}

      try {
         ak var = this.a.c();
         this.a.a(var.a(var));
      } finally {
         ;
      }

   }

   public void a(ac var) {
      synchronized(this){}

      try {
         ak var = this.a.c();
         this.a.a(var.a(var));
      } finally {
         ;
      }

   }

   public void a(ac var, an var) {
      synchronized(this){}

      try {
         ak var = this.a.c();
         this.a.a(var.a(var, var));
      } finally {
         ;
      }

   }

   public void a(ai var, String var, String var) {
      synchronized(this){}

      try {
         this.a.a(var, var);
         k var = this.a;
         ak var = new ak(var);
         var.a(var);
      } finally {
         ;
      }

   }

   public void a(an var) {
      synchronized(this){}

      try {
         ak var = this.a.c();
         this.a.a(var.a(var));
      } finally {
         ;
      }

   }

   public boolean a() {
      synchronized(this){}

      boolean var;
      try {
         var = this.a.e();
      } finally {
         ;
      }

      return var;
   }

   public ak b() {
      return this.a.c();
   }

   public v c() {
      k var = this.a;
      var.getClass();
      return v.c(e.a(var));
   }

   @Deprecated
   public v d() {
      return this.c().d(f.a()).d(g.a());
   }

   public void e() {
      synchronized(this){}

      try {
         this.a.d();
         this.d.c();
         this.f.e();
         this.g();
         this.c.b();
         this.e.b();
         this.g.i();
         this.h.c();
         this.i.c();
      } finally {
         ;
      }

   }
}
