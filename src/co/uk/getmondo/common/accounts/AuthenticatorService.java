package co.uk.getmondo.common.accounts;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AuthenticatorService extends Service {
   public IBinder onBind(Intent var) {
      return (new a(this.getApplicationContext())).getIBinder();
   }
}
