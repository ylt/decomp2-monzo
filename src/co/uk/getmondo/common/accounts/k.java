package co.uk.getmondo.common.accounts;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AuthenticatorException;
import android.accounts.OnAccountsUpdateListener;
import android.accounts.OperationCanceledException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import co.uk.getmondo.d.ab;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import java.io.IOException;

public class k {
   private final AccountManager a;
   private final com.google.gson.f b;
   private final Handler c = new Handler(Looper.getMainLooper());
   private ak d;

   public k(AccountManager var) {
      this.a = var;
      co.uk.getmondo.common.g.b var = co.uk.getmondo.common.g.b.a(co.uk.getmondo.d.a.class, "account").b(ab.class, "prepaid").b(ad.class, "retail");
      Class var;
      if(co.uk.getmondo.a.c.booleanValue()) {
         var = ad.class;
      } else {
         var = ab.class;
      }

      co.uk.getmondo.common.g.b var = var.a(var);
      this.b = (new com.google.gson.g()).a(var).a();
   }

   private Account f() {
      Account[] var = this.a.getAccountsByType("co.uk.getmondo");
      Account var;
      if(var.length <= 0) {
         var = null;
      } else {
         var = var[0];
      }

      return var;
   }

   public ai a() {
      Account var = this.f();
      ai var;
      if(var != null) {
         Object var;
         try {
            String var = this.a.blockingGetAuthToken(var, "co.uk.getmondo_authTokenType", true);
            String var = this.a.getUserData(var, "co.uk.getmondo_refreshData");
            ai.a var = (ai.a)this.b.a(var, ai.a.class);
            var = new ai(var, var);
            return var;
         } catch (OperationCanceledException var) {
            var = var;
         } catch (IOException var) {
            var = var;
         } catch (AuthenticatorException var) {
            var = var;
         }

         d.a.a.a((Throwable)(new RuntimeException("Failed to get auth token", (Throwable)var)));
      }

      var = null;
      return var;
   }

   public void a(OnAccountsUpdateListener var) {
      this.a.addOnAccountsUpdatedListener(var, this.c, true);
   }

   public void a(ai var) {
      Account var = this.f();
      this.a.setAuthToken(var, "co.uk.getmondo_authTokenType", var.b());
      this.a.setUserData(var, "co.uk.getmondo_refreshData", this.b.a(var.c()));
   }

   public void a(ai var, String var) {
      Account var = new Account(var, "co.uk.getmondo");
      this.a.addAccountExplicitly(var, (String)null, (Bundle)null);
      this.a(var);
   }

   public void a(ak param1) {
      // $FF: Couldn't be decompiled
   }

   public String b() {
      Account var = this.f();
      String var;
      if(var != null) {
         Object var;
         try {
            var = this.a.blockingGetAuthToken(var, "co.uk.getmondo_authTokenType", true);
            return var;
         } catch (OperationCanceledException var) {
            var = var;
         } catch (IOException var) {
            var = var;
         } catch (AuthenticatorException var) {
            var = var;
         }

         d.a.a.a((Throwable)(new RuntimeException("Failed to get auth token", (Throwable)var)));
      }

      var = null;
      return var;
   }

   public void b(OnAccountsUpdateListener var) {
      this.a.removeOnAccountsUpdatedListener(var);
   }

   public ak c() {
      // $FF: Couldn't be decompiled
   }

   public void d() {
      this.d = null;
      Account var = this.f();
      if(var != null) {
         if(VERSION.SDK_INT < 22) {
            this.a.removeAccount(var, (AccountManagerCallback)null, (Handler)null);
         } else {
            this.a.removeAccountExplicitly(var);
         }
      }

   }

   public boolean e() {
      boolean var;
      if(this.f() != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }
}
