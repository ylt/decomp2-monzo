package co.uk.getmondo.common.accounts;

import co.uk.getmondo.d.ak;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0012R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000fR\u0013\u0010\u0010\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\f¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/common/accounts/AccountManager;", "", "androidAccountManager", "Lco/uk/getmondo/common/accounts/AndroidAccountManager;", "(Lco/uk/getmondo/common/accounts/AndroidAccountManager;)V", "account", "Lco/uk/getmondo/model/Account;", "getAccount", "()Lco/uk/getmondo/model/Account;", "accountId", "", "getAccountId", "()Ljava/lang/String;", "isRetailAccount", "", "()Z", "secondaryAccountId", "getSecondaryAccountId", "Lio/reactivex/Observable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final k a;

   public b(k var) {
      kotlin.d.b.l.b(var, "androidAccountManager");
      super();
      this.a = var;
   }

   public final co.uk.getmondo.d.a a() {
      ak var = this.a.c();
      co.uk.getmondo.d.a var;
      if(var != null) {
         var = var.c();
      } else {
         var = null;
      }

      return var;
   }

   public final boolean b() {
      co.uk.getmondo.d.a var = this.a();
      boolean var;
      if(var != null) {
         var = var.e();
      } else {
         var = false;
      }

      return var;
   }

   public final String c() {
      co.uk.getmondo.d.a var = this.a();
      String var;
      if(var != null) {
         var = var.a();
      } else {
         var = null;
      }

      return var;
   }

   public final String d() {
      ak var = this.a.c();
      String var;
      if(var != null) {
         var = var.f();
      } else {
         var = null;
      }

      return var;
   }

   public final io.reactivex.n e() {
      io.reactivex.n var = io.reactivex.n.fromCallable((Callable)(new Callable() {
         public final String a() {
            return b.this.c();
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.fromCallable { accountId }");
      return var;
   }
}
