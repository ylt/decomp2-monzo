package co.uk.getmondo.common.accounts;

import co.uk.getmondo.common.q;
import io.reactivex.u;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new j(var, var, var, var, var, var, var, var, var);
   }

   public d a() {
      return new d((k)this.b.b(), (u)this.c.b(), (co.uk.getmondo.waitlist.i)this.d.b(), (q)this.e.b(), (co.uk.getmondo.payments.send.payment_category.b.a)this.f.b(), (co.uk.getmondo.settings.k)this.g.b(), (co.uk.getmondo.signup.identity_verification.a.h)this.h.b(), (co.uk.getmondo.signup.status.g)this.i.b(), (co.uk.getmondo.migration.d)this.j.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
