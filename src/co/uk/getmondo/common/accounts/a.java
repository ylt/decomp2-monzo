package co.uk.getmondo.common.accounts;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import co.uk.getmondo.splash.SplashActivity;

class a extends AbstractAccountAuthenticator {
   private final Context a;

   a(Context var) {
      super(var);
      this.a = var;
   }

   private Bundle a(AccountAuthenticatorResponse var, String var) {
      Intent var = new Intent(this.a, SplashActivity.class);
      var.putExtra("accountType", var);
      var.putExtra("accountAuthenticatorResponse", var);
      Bundle var = new Bundle();
      var.putParcelable("intent", var);
      return var;
   }

   public Bundle addAccount(AccountAuthenticatorResponse var, String var, String var, String[] var, Bundle var) throws NetworkErrorException {
      return this.a(var, var);
   }

   public Bundle confirmCredentials(AccountAuthenticatorResponse var, Account var, Bundle var) throws NetworkErrorException {
      return null;
   }

   public Bundle editProperties(AccountAuthenticatorResponse var, String var) {
      return null;
   }

   public Bundle getAuthToken(AccountAuthenticatorResponse var, Account var, String var, Bundle var) throws NetworkErrorException {
      var = AccountManager.get(this.a).peekAuthToken(var, var);
      Bundle var;
      if(!TextUtils.isEmpty(var)) {
         var = new Bundle();
         var.putString("authAccount", var.name);
         var.putString("accountType", var.type);
         var.putString("authtoken", var);
      } else {
         var = this.a(var, var.type);
      }

      return var;
   }

   public String getAuthTokenLabel(String var) {
      return this.a.getString(2131362012);
   }

   public Bundle hasFeatures(AccountAuthenticatorResponse var, Account var, String[] var) throws NetworkErrorException {
      return null;
   }

   public Bundle updateCredentials(AccountAuthenticatorResponse var, Account var, String var, Bundle var) throws NetworkErrorException {
      return null;
   }
}
