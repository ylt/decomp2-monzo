package co.uk.getmondo.common.accounts;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class e implements Callable {
   private final k a;

   private e(k var) {
      this.a = var;
   }

   public static Callable a(k var) {
      return new e(var);
   }

   public Object call() {
      return this.a.c();
   }
}
