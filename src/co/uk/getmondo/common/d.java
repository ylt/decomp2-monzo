package co.uk.getmondo.common;

import co.uk.getmondo.api.AnalyticsApi;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new d(var, var);
   }

   public a a() {
      return new a((AnalyticsApi)this.b.b(), (io.reactivex.u)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
