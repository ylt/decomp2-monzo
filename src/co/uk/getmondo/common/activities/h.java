package co.uk.getmondo.common.activities;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public abstract class h extends b {
   private boolean a = true;

   private void b() {
      Toolbar var = this.r();
      if(var != null && this.a) {
         var.setNavigationIcon(android.support.v4.content.a.a(var.getContext(), 2130837836).mutate());
      }

   }

   protected void a() {
      this.setResult(0);
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      switch(var.getItemId()) {
      case 16908332:
         if(this.a) {
            this.a();
         }
      default:
         return super.onOptionsItemSelected(var);
      }
   }

   public void setContentView(int var) {
      super.setContentView(var);
      this.b();
   }

   public void setContentView(View var) {
      super.setContentView(var);
      this.b();
   }

   public void setContentView(View var, LayoutParams var) {
      super.setContentView(var, var);
      this.b();
   }
}
