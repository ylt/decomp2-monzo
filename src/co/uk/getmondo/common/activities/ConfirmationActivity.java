package co.uk.getmondo.common.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmationActivity extends b {
   @BindView(2131820857)
   TextView subTitleView;
   @BindView(2131820583)
   TextView titleView;

   public static void a(Context var, Intent var) {
      a(var, var.getString(2131362792), var.getString(2131362791), var);
   }

   public static void a(Context var, String var, String var) {
      var.startActivity(b(var, var, var));
   }

   public static void a(Context var, String var, String var, Intent var) {
      Intent var = b(var, var, var);
      var.putExtra("EXTRA_NEXT_ACTIVITY_INTENT", var);
      var.startActivity(var);
   }

   // $FF: synthetic method
   static void a(ConfirmationActivity var) {
      Intent var = (Intent)var.getIntent().getParcelableExtra("EXTRA_NEXT_ACTIVITY_INTENT");
      if(var != null) {
         var.startActivity(var);
         var.finishAffinity();
      } else {
         var.setResult(-1);
         var.finish();
      }

   }

   public static Intent b(Context var, String var, String var) {
      Intent var = new Intent(var, ConfirmationActivity.class);
      var.putExtra("EXTRA_TITLE", var);
      var.putExtra("EXTRA_SUBTITLE", var);
      return var;
   }

   public void onBackPressed() {
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034154);
      ButterKnife.bind((Activity)this);
      this.titleView.setText(this.getIntent().getStringExtra("EXTRA_TITLE"));
      this.subTitleView.setText(this.getIntent().getStringExtra("EXTRA_SUBTITLE"));
      (new Handler()).postDelayed(g.a(this), 2500L);
   }
}
