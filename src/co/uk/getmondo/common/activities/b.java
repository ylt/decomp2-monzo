package co.uk.getmondo.common.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.splash.SplashActivity;
import com.crashlytics.android.Crashlytics;

public abstract class b extends android.support.v7.app.e implements co.uk.getmondo.common.e.a.a {
   private co.uk.getmondo.common.h.b.b a;
   private Toolbar b;
   protected com.c.b.b d = com.c.b.b.c();

   // $FF: synthetic method
   static void a(String var, co.uk.getmondo.common.f.c var) {
      var.c(var);
   }

   // $FF: synthetic method
   static void b(String var, co.uk.getmondo.common.f.c var) {
      var.b(var);
   }

   protected void a(View var) {
      if(var != null) {
         ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(var.getWindowToken(), 2);
      }

   }

   public void b(int var) {
      this.b(this.getString(var));
   }

   public void b(String var) {
      this.a(this.n());
      this.d.a(c.a(var));
   }

   public void c(int var) {
      this.startActivity(SplashActivity.a(this, this.getString(var)));
   }

   public void c(String var) {
      this.d.a(d.a(var));
   }

   protected co.uk.getmondo.common.h.b.b l() {
      return this.a;
   }

   public View m() {
      return this.n();
   }

   public View n() {
      View var;
      if(this.getCurrentFocus() == null) {
         var = this.getWindow().getDecorView().findViewById(16908290);
      } else {
         var = this.getCurrentFocus();
      }

      return var;
   }

   protected void o() {
      try {
         this.onBackPressed();
      } catch (IllegalStateException var) {
         Crashlytics.logException(var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.a = co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a(this).b()).a(new co.uk.getmondo.common.h.b.c(this)).a();
   }

   protected void onDestroy() {
      this.d = com.c.b.b.c();
      super.onDestroy();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      boolean var;
      switch(var.getItemId()) {
      case 16908332:
         this.o();
         var = true;
         break;
      default:
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   protected void p() {
      View var = this.n().findViewById(2131820798);
      if(var != null && var instanceof Toolbar) {
         this.b = (Toolbar)var;
         this.setSupportActionBar(this.b);
         android.support.v7.app.a var = this.getSupportActionBar();
         if(var != null) {
            var.b(true);
            var.d(true);
         }
      }

   }

   public void q() {
      this.a(this.n());
      this.startActivity(ForceUpgradeActivity.a((Context)this));
   }

   protected Toolbar r() {
      return this.b;
   }

   public void s() {
      this.d.a(e.a());
   }

   public void setContentView(int var) {
      super.setContentView(var);
      this.p();
      this.d = com.c.b.b.b(new co.uk.getmondo.common.f.c(this, this.m()));
   }

   public void setContentView(View var) {
      super.setContentView(var);
      this.p();
      this.d = com.c.b.b.b(new co.uk.getmondo.common.f.c(this, this.m()));
   }

   public void setContentView(View var, LayoutParams var) {
      super.setContentView(var, var);
      this.p();
   }

   public void t() {
      this.d.a(f.a());
   }

   public void u() {
      SplashActivity.a(this);
   }

   public static enum a {
      a,
      b;
   }
}
