package co.uk.getmondo.common.activities;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a&\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b¨\u0006\t"},
   d2 = {"launchCustomTab", "", "Landroid/app/Activity;", "url", "", "background", "", "showTitle", "", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class a {
   public static final void a(Activity var, String var, int var, boolean var) {
      l.b(var, "$receiver");
      l.b(var, "url");
      (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)var, var)).a(var).a().a((Context)var, Uri.parse(var));
   }
}
