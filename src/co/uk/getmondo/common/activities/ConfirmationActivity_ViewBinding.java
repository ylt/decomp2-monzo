package co.uk.getmondo.common.activities;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ConfirmationActivity_ViewBinding implements Unbinder {
   private ConfirmationActivity a;

   public ConfirmationActivity_ViewBinding(ConfirmationActivity var, View var) {
      this.a = var;
      var.titleView = (TextView)Utils.findRequiredViewAsType(var, 2131820583, "field 'titleView'", TextView.class);
      var.subTitleView = (TextView)Utils.findRequiredViewAsType(var, 2131820857, "field 'subTitleView'", TextView.class);
   }

   public void unbind() {
      ConfirmationActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleView = null;
         var.subTitleView = null;
      }
   }
}
