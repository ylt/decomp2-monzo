package co.uk.getmondo.common;

import android.content.Context;
import android.content.SharedPreferences;

public class i {
   private final SharedPreferences a;

   public i(Context var) {
      this.a = var.getSharedPreferences("delete_feed", 0);
   }

   public void a() {
      this.a.edit().putBoolean("delete_kyc_request", true).apply();
   }

   public boolean b() {
      return this.a.getBoolean("delete_kyc_request", false);
   }

   public void c() {
      this.a.edit().remove("delete_kyc_request").apply();
   }
}
