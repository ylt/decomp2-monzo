package co.uk.getmondo.common.e;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import java.io.IOException;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.n;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001b\u001cB\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J,\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eH\u0002J\u0016\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\fJ*\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eJ\u0012\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0013H\u0002J \u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000b\u001a\u00020\fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler;", "", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;)V", "handleApiException", "", "apiException", "Lco/uk/getmondo/api/ApiException;", "view", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "showError", "Lkotlin/Function1;", "", "handleError", "", "throwable", "", "isNetworkException", "t", "performLogOut", "errorCause", "Lco/uk/getmondo/api/authentication/AuthError;", "logoutReason", "", "ApiErrorView", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.b a = new a.b((i)null);
   private final co.uk.getmondo.common.accounts.d b;
   private final co.uk.getmondo.common.a c;

   public a(co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.a var) {
      l.b(var, "accountService");
      l.b(var, "analyticsService");
      super();
      this.b = var;
      this.c = var;
   }

   private final void a(ApiException var, a.a var, kotlin.d.a.b var) {
      d.a.a.a((Throwable)(new Exception("Handling api error", (Throwable)var)), "Api Error with view %s", new Object[]{var.getClass().getSimpleName()});
      co.uk.getmondo.common.a var = this.c;
      Impression.Companion var = Impression.Companion;
      int var = var.b();
      String var = var.c();
      l.a(var, "apiException.path");
      var.a(var.b(var, var, var.d(), var.e()));
      var = var.b();
      co.uk.getmondo.api.model.b var = var.e();
      if(400 <= var && 499 >= var && var != null) {
         co.uk.getmondo.api.authentication.a var = (co.uk.getmondo.api.authentication.a)d.a((f[])co.uk.getmondo.api.authentication.a.values(), var.a());
         if(var != null && var.b()) {
            String var = var.b();
            l.a(var, "apiError.message");
            this.a(var, var, var);
            return;
         }

         if(var.c()) {
            var.q();
            return;
         }
      }

      if(var >= 500) {
         var.a(Integer.valueOf(2131362199));
      } else {
         var.a(Integer.valueOf(2131362198));
      }

   }

   private final void a(co.uk.getmondo.api.authentication.a var, String var, a.a var) {
      if(this.b.a()) {
         String var;
         label21: {
            ak var = this.b.b();
            if(var != null) {
               ac var = var.d();
               if(var != null) {
                  var = var.b();
                  break label21;
               }
            }

            var = null;
         }

         if(var == null) {
            l.a();
         }

         this.c.a(Impression.Companion.a(var, var));
         this.b.e();
         if(l.a(var, co.uk.getmondo.api.authentication.a.a)) {
            var.c(2131362864);
         } else {
            var.u();
         }
      }

   }

   public static final boolean a(Throwable var) {
      return a.a(var);
   }

   private final boolean b(Throwable var) {
      boolean var;
      if(var != null && var instanceof IOException) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final boolean a(Throwable var, final a.a var) {
      l.b(var, "throwable");
      l.b(var, "view");
      return this.a(var, var, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var) {
            this.a(((Number)var).intValue());
            return n.a;
         }

         public final void a(int var) {
            var.b(var);
         }
      }));
   }

   public final boolean a(Throwable var, a.a var, kotlin.d.a.b var) {
      l.b(var, "throwable");
      l.b(var, "view");
      l.b(var, "showError");
      boolean var;
      if(!a.a(var) && !a.a(var.getCause())) {
         if(var instanceof ApiException) {
            this.a((ApiException)var, var, var);
            var = true;
         } else if(var.getCause() != null && var.getCause() instanceof ApiException) {
            var = var.getCause();
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.ApiException");
            }

            this.a((ApiException)var, var, var);
            var = true;
         } else if(!this.b(var) && !this.b(var.getCause())) {
            d.a.a.a((Throwable)(new Exception("Failed to handle error", var)), "Failed to handle error with view %s", new Object[]{var.getClass().getSimpleName()});
            var = false;
         } else {
            var.a(Integer.valueOf(2131362247));
            d.a.a.a((Throwable)(new Exception("Network error", var)), "Network failure with view %s", new Object[]{var.getClass().getSimpleName()});
            var = true;
         }
      } else {
         var.a(Integer.valueOf(2131362179));
         d.a.a.a((Throwable)(new Exception("Wrong certificate", var)), "Wrong certificate with view %s", new Object[]{var.getClass().getSimpleName()});
         var = true;
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0012\u0010\u0005\u001a\u00020\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/common/errors/ErrorView;", "openForceUpgrade", "", "openSplash", "openSplashWithMessage", "messageRes", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends e {
      void c(int var);

      void q();

      void u();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler$Companion;", "", "()V", "isCertificatePinningException", "", "t", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private b() {
      }

      // $FF: synthetic method
      public b(i var) {
         this();
      }

      public final boolean a(Throwable var) {
         boolean var;
         if(var != null && var instanceof SSLPeerUnverifiedException) {
            var = true;
         } else {
            var = false;
         }

         return var;
      }
   }
}
