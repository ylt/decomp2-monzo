package co.uk.getmondo.common.e;

import co.uk.getmondo.api.ApiException;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\u001a)\u0010\t\u001a\u0004\u0018\u0001H\n\"\b\b\u0000\u0010\n*\u00020\u000b*\u00020\u00022\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\r¢\u0006\u0002\u0010\u000e\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u0006*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u000f"},
   d2 = {"apiErrorCode", "", "", "getApiErrorCode", "(Ljava/lang/Throwable;)Ljava/lang/String;", "httpErrorCode", "", "getHttpErrorCode", "(Ljava/lang/Throwable;)Ljava/lang/Integer;", "matches", "E", "Lco/uk/getmondo/common/errors/MatchableError;", "errors", "", "(Ljava/lang/Throwable;[Lco/uk/getmondo/common/errors/MatchableError;)Lco/uk/getmondo/common/errors/MatchableError;", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class c {
   public static final f a(Throwable var, f[] var) {
      l.b(var, "$receiver");
      l.b(var, "errors");
      return d.a(var, a(var));
   }

   public static final String a(Throwable var) {
      l.b(var, "$receiver");
      String var;
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         if(var != null) {
            var = var.a();
            if(var != null) {
               return var;
            }
         }

         var = "";
      } else {
         var = "";
      }

      return var;
   }

   public static final Integer b(Throwable var) {
      l.b(var, "$receiver");
      Integer var;
      if(var instanceof ApiException) {
         var = Integer.valueOf(((ApiException)var).b());
      } else {
         var = null;
      }

      return var;
   }
}
