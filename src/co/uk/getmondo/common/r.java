package co.uk.getmondo.common;

import android.app.Application;
import co.uk.getmondo.api.MonzoApi;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!r.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public r(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new r(var, var, var);
   }

   public q a() {
      return new q((Application)this.b.b(), (MonzoApi)this.c.b(), (io.reactivex.u)this.d.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
