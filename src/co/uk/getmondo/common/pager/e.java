package co.uk.getmondo.common.pager;

import co.uk.getmondo.api.model.tracking.Impression;

public class e extends a implements f.d {
   private final String a;
   private final int b;
   private final int c;

   public e(String var, int var, int var, Impression var) {
      super(var);
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public String b() {
      return this.a;
   }

   public int c() {
      return this.b;
   }

   public int d() {
      return this.c;
   }
}
