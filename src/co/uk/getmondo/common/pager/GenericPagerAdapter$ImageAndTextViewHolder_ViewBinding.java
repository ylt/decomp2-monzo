package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.ImageAndTextViewHolder a;

   public GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding(GenericPagerAdapter.ImageAndTextViewHolder var, View var) {
      this.a = var;
      var.imageView = (ImageView)Utils.findRequiredViewAsType(var, 2131820750, "field 'imageView'", ImageView.class);
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var.contentTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821119, "field 'contentTextView'", TextView.class);
      var.footerTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821665, "field 'footerTextView'", TextView.class);
      var.spacerView = Utils.findRequiredView(var, 2131820755, "field 'spacerView'");
   }

   public void unbind() {
      GenericPagerAdapter.ImageAndTextViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.imageView = null;
         var.titleTextView = null;
         var.contentTextView = null;
         var.footerTextView = null;
         var.spacerView = null;
      }
   }
}
