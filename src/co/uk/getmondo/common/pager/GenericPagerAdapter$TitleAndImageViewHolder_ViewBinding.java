package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.TitleAndImageViewHolder a;

   public GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding(GenericPagerAdapter.TitleAndImageViewHolder var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var.imageView = (ImageView)Utils.findRequiredViewAsType(var, 2131820750, "field 'imageView'", ImageView.class);
   }

   public void unbind() {
      GenericPagerAdapter.TitleAndImageViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
         var.imageView = null;
      }
   }
}
