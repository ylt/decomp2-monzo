package co.uk.getmondo.common.pager;

import co.uk.getmondo.api.model.tracking.Impression;

public class d extends a implements f.c {
   private final int a;
   private final int b;
   private final int c;
   private final Integer d;

   public d(int var, int var, int var, Impression var) {
      this(var, var, var, (Integer)null, var);
   }

   public d(int var, int var, int var, Integer var, Impression var) {
      super(var);
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public int b() {
      return this.a;
   }

   public int c() {
      return this.b;
   }

   public int d() {
      return this.c;
   }

   public Integer e() {
      return this.d;
   }
}
