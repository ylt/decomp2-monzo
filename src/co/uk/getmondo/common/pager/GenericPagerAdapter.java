package co.uk.getmondo.common.pager;

import android.support.v4.view.p;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.airbnb.lottie.LottieAnimationView;
import java.util.Arrays;
import java.util.List;

public class GenericPagerAdapter extends p {
   final List a;

   public GenericPagerAdapter(List var) {
      this.a = var;
   }

   public GenericPagerAdapter(f... var) {
      this.a = Arrays.asList(var);
   }

   public Object a(ViewGroup var, int var) {
      f var = (f)this.a.get(var);
      LayoutInflater var = LayoutInflater.from(var.getContext());
      View var;
      if(var instanceof f.a) {
         var = var.inflate(((f.a)var).b(), var, false);
      } else if(var instanceof f.c) {
         var = (new GenericPagerAdapter.ImageAndTextViewHolder(var.inflate(2131034406, var, false))).a((f.c)var);
      } else if(var instanceof f.e) {
         var = (new GenericPagerAdapter.TitleAndImageViewHolder(var.inflate(2131034410, var, false))).a((f.e)var);
      } else if(var instanceof f.b) {
         var = ((f.b)var).c();
      } else {
         if(!(var instanceof f.d)) {
            throw new RuntimeException("Unsupported page type: " + var);
         }

         var = (new GenericPagerAdapter.LottieAnimationAndTextViewHolder(var.inflate(2131034407, var, false))).a((f.d)var);
      }

      var.addView(var);
      return var;
   }

   public void a(ViewGroup var, int var, Object var) {
      f var = (f)this.a.get(var);
      if(var instanceof f.b) {
         ((f.b)var).b();
      }

      var.removeView((View)var);
   }

   public boolean a(View var, Object var) {
      boolean var;
      if(var == var) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public int b() {
      return this.a.size();
   }

   public void b(ViewGroup var, int var, Object var) {
      super.b(var, var, var);
      LottieAnimationView var = (LottieAnimationView)((View)var).findViewById(2131821666);
      if(var != null && !var.b()) {
         var.c();
      }

   }

   static class ImageAndTextViewHolder extends g {
      @BindView(2131821119)
      TextView contentTextView;
      @BindView(2131821665)
      TextView footerTextView;
      @BindView(2131820750)
      ImageView imageView;
      @BindView(2131820755)
      View spacerView;
      @BindView(2131821026)
      TextView titleTextView;

      ImageAndTextViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      View a(f.c var) {
         this.imageView.setImageResource(var.b());
         this.titleTextView.setText(var.c());
         this.contentTextView.setText(var.d());
         if(var.e() == null) {
            this.footerTextView.setVisibility(8);
            this.spacerView.setVisibility(8);
         } else {
            this.footerTextView.setText(var.e().intValue());
            this.footerTextView.setVisibility(0);
            this.spacerView.setVisibility(0);
         }

         return this.a;
      }
   }

   static class LottieAnimationAndTextViewHolder extends g {
      @BindView(2131821119)
      TextView contentTextView;
      @BindView(2131821666)
      LottieAnimationView lottieAnimationView;
      @BindView(2131821026)
      TextView titleTextView;

      LottieAnimationAndTextViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      View a(f.d var) {
         this.lottieAnimationView.setAnimation(var.b());
         this.titleTextView.setText(var.c());
         this.contentTextView.setText(var.d());
         return this.a;
      }
   }

   static class TitleAndImageViewHolder extends g {
      @BindView(2131820750)
      ImageView imageView;
      @BindView(2131821026)
      TextView titleTextView;

      TitleAndImageViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      View a(f.e var) {
         this.imageView.setImageResource(var.c());
         this.titleTextView.setText(var.b());
         return this.a;
      }
   }
}
