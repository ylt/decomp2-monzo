package co.uk.getmondo.common.pager;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new i(var);
   }

   public h a() {
      return new h((co.uk.getmondo.common.a)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
