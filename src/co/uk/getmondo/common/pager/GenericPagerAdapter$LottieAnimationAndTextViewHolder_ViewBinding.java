package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;

public class GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.LottieAnimationAndTextViewHolder a;

   public GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding(GenericPagerAdapter.LottieAnimationAndTextViewHolder var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var.contentTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821119, "field 'contentTextView'", TextView.class);
      var.lottieAnimationView = (LottieAnimationView)Utils.findRequiredViewAsType(var, 2131821666, "field 'lottieAnimationView'", LottieAnimationView.class);
   }

   public void unbind() {
      GenericPagerAdapter.LottieAnimationAndTextViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
         var.contentTextView = null;
         var.lottieAnimationView = null;
      }
   }
}
