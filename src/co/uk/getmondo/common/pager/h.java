package co.uk.getmondo.common.pager;

import android.support.v4.view.ViewPager;
import android.support.v4.view.p;
import android.support.v4.view.ViewPager.j;

public class h {
   private final co.uk.getmondo.common.a a;
   private ViewPager b;
   private final android.support.v4.view.ViewPager.f c = new j() {
      public void b(int var) {
         h.this.a(var);
      }
   };

   public h(co.uk.getmondo.common.a var) {
      this.a = var;
   }

   private void a(int var) {
      if(var >= 0) {
         p var = this.b.getAdapter();
         if(var == null) {
            throw new RuntimeException("The ViewPager must have an adapter");
         }

         if(!(var instanceof GenericPagerAdapter)) {
            throw new RuntimeException("PageViewTracker requires an adapter of type " + GenericPagerAdapter.class.getSimpleName());
         }

         GenericPagerAdapter var = (GenericPagerAdapter)var;
         if(var < var.b()) {
            f var = (f)var.a.get(var);
            if(var.a() != null) {
               this.a.a(var.a());
            }
         }
      }

   }

   public void a() {
      if(this.b != null) {
         this.b.b(this.c);
         this.b = null;
      }

   }

   public void a(ViewPager var) {
      this.a();
      this.b = var;
      this.a(var.getCurrentItem());
      var.a(this.c);
   }
}
