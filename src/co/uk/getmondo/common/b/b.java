package co.uk.getmondo.common.b;

import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\b\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002:\u0001\u0015B'\u0012\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0010\b\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004HÆ\u0003J\u0011\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004HÆ\u0003J1\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u00042\u0010\b\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0019\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/data/QueryResults;", "T", "", "results", "", "changes", "Lco/uk/getmondo/common/data/QueryResults$Change;", "(Ljava/util/List;Ljava/util/List;)V", "getChanges", "()Ljava/util/List;", "getResults", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Change", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final List a;
   private final List b;

   public b() {
      this((List)null, (List)null, 3, (i)null);
   }

   public b(List var, List var) {
      l.b(var, "results");
      super();
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   public b(List var, List var, int var, i var) {
      if((var & 1) != 0) {
         var = m.a();
      }

      if((var & 2) != 0) {
         var = (List)null;
      }

      this(var, var);
   }

   public final List a() {
      return this.a;
   }

   public final List b() {
      return this.b;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof b) {
               b var = (b)var;
               if(l.a(this.a, var.a) && l.a(this.b, var.b)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      List var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "QueryResults(results=" + this.a + ", changes=" + this.b + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0017B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0018"},
      d2 = {"Lco/uk/getmondo/common/data/QueryResults$Change;", "", "type", "Lco/uk/getmondo/common/data/QueryResults$Change$Type;", "startIndex", "", "length", "(Lco/uk/getmondo/common/data/QueryResults$Change$Type;II)V", "getLength", "()I", "getStartIndex", "getType", "()Lco/uk/getmondo/common/data/QueryResults$Change$Type;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "Type", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private final b.a a;
      private final int b;
      private final int c;

      public a(b.a var, int var, int var) {
         l.b(var, "type");
         super();
         this.a = var;
         this.b = var;
         this.c = var;
      }

      public final b.a a() {
         return this.a;
      }

      public final int b() {
         return this.b;
      }

      public final int c() {
         return this.c;
      }

      public boolean equals(Object var) {
         boolean var = false;
         boolean var;
         if(this != var) {
            var = var;
            if(!(var instanceof b.a)) {
               return var;
            }

            b.a var = (b.a)var;
            var = var;
            if(!l.a(this.a, var.a)) {
               return var;
            }

            boolean var;
            if(this.b == var.b) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }

            if(this.c == var.c) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         b.a var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return (var * 31 + this.b) * 31 + this.c;
      }

      public String toString() {
         return "Change(type=" + this.a + ", startIndex=" + this.b + ", length=" + this.c + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/common/data/QueryResults$Change$Type;", "", "(Ljava/lang/String;I)V", "INSERTION", "DELETION", "MODIFICATION", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b,
      c;

      static {
         b.a var = new b.a("INSERTION", 0);
         a = var;
         b.a var = new b.a("DELETION", 1);
         b = var;
         b.a var = new b.a("MODIFICATION", 2);
         c = var;
      }
   }
}
