package co.uk.getmondo.common.c;

import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.chrono.ChronoLocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u0004\u0018\u00010\u00022\u0006\u0010\u0007\u001a\u00020\b\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\t"},
   d2 = {"dayOfMonthWithSuffix", "", "Lorg/threeten/bp/LocalDate;", "getDayOfMonthWithSuffix", "(Lorg/threeten/bp/LocalDate;)Ljava/lang/String;", "isOlderThan", "", "years", "", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class c {
   public static final String a(LocalDate var) {
      l.b(var, "$receiver");
      return var.g() + a.a(var.g());
   }

   public static final boolean a(LocalDate var, long var) {
      boolean var = false;
      if(var != null) {
         if(!var.b((ChronoLocalDate)LocalDate.a().f(var))) {
            var = true;
         } else {
            var = false;
         }
      }

      return var;
   }
}
