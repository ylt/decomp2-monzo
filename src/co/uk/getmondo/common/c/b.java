package co.uk.getmondo.common.c;

import com.squareup.moshi.g;
import com.squareup.moshi.x;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\bH\u0007J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0004H\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000eH\u0007¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/date/ThreeTenMoshiAdapter;", "", "()V", "localDateFromJson", "Lorg/threeten/bp/LocalDate;", "date", "", "localDateTimeFromJson", "Lorg/threeten/bp/LocalDateTime;", "localDateTimeToJason", "localDateTime", "localDateToJason", "localDate", "zonedDateTimeFromJson", "Lorg/threeten/bp/ZonedDateTime;", "zonedDateTimeToJason", "zonedDateTime", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   @g
   public final LocalDate localDateFromJson(String var) {
      l.b(var, "date");
      boolean var;
      if(!j.a((CharSequence)var)) {
         var = true;
      } else {
         var = false;
      }

      LocalDate var;
      if(var) {
         var = LocalDate.a((CharSequence)var);
      } else {
         var = null;
      }

      return var;
   }

   @g
   public final LocalDateTime localDateTimeFromJson(String var) {
      l.b(var, "date");
      boolean var;
      if(!j.a((CharSequence)var)) {
         var = true;
      } else {
         var = false;
      }

      LocalDateTime var;
      if(var) {
         var = ZonedDateTime.a((CharSequence)var).d();
      } else {
         var = null;
      }

      return var;
   }

   @x
   public final String localDateTimeToJason(LocalDateTime var) {
      l.b(var, "localDateTime");
      String var = var.a(DateTimeFormatter.g);
      l.a(var, "localDateTime.format(Dat…tter.ISO_LOCAL_DATE_TIME)");
      return var;
   }

   @x
   public final String localDateToJason(LocalDate var) {
      l.b(var, "localDate");
      String var = var.a(DateTimeFormatter.a);
      l.a(var, "localDate.format(DateTimeFormatter.ISO_LOCAL_DATE)");
      return var;
   }

   @g
   public final ZonedDateTime zonedDateTimeFromJson(String var) {
      l.b(var, "date");
      boolean var;
      if(!j.a((CharSequence)var)) {
         var = true;
      } else {
         var = false;
      }

      ZonedDateTime var;
      if(var) {
         var = ZonedDateTime.a((CharSequence)var);
      } else {
         var = null;
      }

      return var;
   }

   @x
   public final String zonedDateTimeToJason(ZonedDateTime var) {
      l.b(var, "zonedDateTime");
      String var = var.a(DateTimeFormatter.i);
      l.a(var, "zonedDateTime.format(Dat…tter.ISO_ZONED_DATE_TIME)");
      return var;
   }
}
