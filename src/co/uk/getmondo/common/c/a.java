package co.uk.getmondo.common.c;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;

public final class a {
   private static final Calendar a = Calendar.getInstance();
   private static final SimpleDateFormat b;
   private static final SimpleDateFormat c;
   private static final SimpleDateFormat d;
   private static final SimpleDateFormat e;

   static {
      b = new SimpleDateFormat("EEEE", Locale.UK);
      c = new SimpleDateFormat("MMMM", Locale.UK);
      d = new SimpleDateFormat("HH:mm", Locale.UK);
      e = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm:ss", Locale.UK);
   }

   public static int a(long var) {
      a.setTimeInMillis(var);
      return a.get(6);
   }

   public static String a(int var) {
      if(var >= 1 && var <= 31) {
         String var;
         if(var >= 11 && var <= 13) {
            var = "th";
         } else {
            switch(var % 10) {
            case 1:
               var = "st";
               break;
            case 2:
               var = "nd";
               break;
            case 3:
               var = "rd";
               break;
            default:
               var = "th";
            }
         }

         return var;
      } else {
         throw new IllegalArgumentException("Illegal day of month: " + var);
      }
   }

   public static String a(long var, boolean var) {
      Date var = new Date(var);
      a.setTimeInMillis(var);
      int var = a.get(5);
      Calendar var = Calendar.getInstance();
      String var;
      if(var.get(1) == a.get(1) && var.get(6) == a.get(6)) {
         var = "Today";
      } else if(!var || var.get(1) >= a.get(1) && var.get(6) >= a.get(6)) {
         String var = a(var);
         var = var + var;
         String var = b.format(var);
         var = c.format(var);
         var = String.format(Locale.UK, "%1$s %2$s %3$s", new Object[]{var, var, var});
      } else {
         var = String.format(Locale.UK, "%1$s days from now", new Object[]{Long.valueOf(TimeUnit.DAYS.convert(a.getTimeInMillis() - var.getTimeInMillis(), TimeUnit.MILLISECONDS))});
      }

      return var;
   }

   public static String a(Date var) {
      a.setTime(var);
      String var = d.format(var);
      return String.format(Locale.UK, "%1$s, %2$s", new Object[]{b(var.getTime()), var});
   }

   public static Date a() {
      a.setTimeInMillis(System.currentTimeMillis());
      a.add(6, -7);
      return a.getTime();
   }

   public static Date a(Date var, Date var) {
      if(var.getTime() >= var.getTime()) {
         var = var;
      }

      return var;
   }

   public static Date a(LocalDateTime var) {
      Calendar var = Calendar.getInstance();
      var.set(var.b(), var.c() - 1, var.e(), var.f(), var.g(), var.h());
      return var.getTime();
   }

   public static LocalDateTime a(Date var, TimeZone var) {
      return LocalDateTime.a(Instant.b(var.getTime()), DateTimeUtils.a(var));
   }

   public static String b(long var) {
      return a(var, false);
   }

   public static Date b(Date var) {
      a.setTime(var);
      a.add(6, -1);
      return a.getTime();
   }

   public static YearMonth b(Date var, TimeZone var) {
      LocalDateTime var = a(var, var);
      return YearMonth.a(var.b(), var.d());
   }

   public static String c(Date var) {
      SimpleDateFormat var = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK);
      var.setTimeZone(TimeZone.getTimeZone("UTC"));
      return var.format(var);
   }

   public static String d(Date var) {
      return e.format(var);
   }
}
