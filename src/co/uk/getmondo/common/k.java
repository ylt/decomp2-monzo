package co.uk.getmondo.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Deprecated
public class k {
   public static Intent a() {
      Intent var = new Intent("android.intent.action.OPEN_DOCUMENT");
      var.addCategory("android.intent.category.OPENABLE");
      var.setType("image/*");
      return var;
   }

   private Intent a(Context var, PackageManager var, List var) {
      Intent var;
      if(var.size() <= 0) {
         var = null;
      } else {
         Intent var = var.getLaunchIntentForPackage(((ResolveInfo)var.get(0)).activityInfo.packageName);
         if(var == null) {
            var = null;
         } else {
            var = Intent.createChooser(var, var.getString(2131362504));
         }
      }

      return var;
   }

   public static Intent a(Context var, File var) {
      Intent var = new Intent("android.media.action.IMAGE_CAPTURE");
      Intent var;
      if(var.resolveActivity(var.getPackageManager()) != null) {
         var.putExtra("output", FileProvider.a(var, var.getString(2131362187), var));
         var = var;
      } else {
         var = null;
      }

      return var;
   }

   private void a(PackageManager var, List var, List var, int var) {
      ResolveInfo var = (ResolveInfo)var.get(var);
      if(!"com.paypal.android.p2pmobile".equals(var.activityInfo.packageName)) {
         Intent var = var.getLaunchIntentForPackage(var.activityInfo.packageName);
         if(var != null) {
            var.add(new LabeledIntent(var, var.activityInfo.packageName, var.loadLabel(var), var.icon));
         }
      }

   }

   private LabeledIntent[] a(PackageManager var, List var) {
      ArrayList var = new ArrayList();

      for(int var = 1; var < var.size(); ++var) {
         this.a(var, var, var, var);
      }

      return (LabeledIntent[])var.toArray(new LabeledIntent[var.size()]);
   }

   public void a(Context var) {
      PackageManager var = var.getPackageManager();
      List var = var.queryIntentActivities(new Intent("android.intent.action.SENDTO", Uri.parse("mailto:")), 0);
      Intent var = this.a(var, var, var);
      if(var == null) {
         throw new NoSuchElementException(var.getString(2131362496));
      } else {
         var.putExtra("android.intent.extra.INITIAL_INTENTS", this.a(var, var));
         var.startActivity(var);
      }
   }

   public void a(Context var, String var) {
      Intent var = new Intent("android.intent.action.VIEW");
      var.setData(Uri.parse(var));
      var.startActivity(var);
   }
}
