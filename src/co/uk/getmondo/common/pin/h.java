package co.uk.getmondo.common.pin;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final g b;

   static {
      boolean var;
      if(!h.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public h(g var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(g var) {
      return new h(var);
   }

   public co.uk.getmondo.common.pin.a.a.b a() {
      return (co.uk.getmondo.common.pin.a.a.b)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
