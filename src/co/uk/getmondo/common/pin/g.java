package co.uk.getmondo.common.pin;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
   d2 = {"Lco/uk/getmondo/common/pin/PinModule;", "", "operation", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "(Lco/uk/getmondo/common/pin/data/model/PinOperation;)V", "providePinOperation", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   private final co.uk.getmondo.common.pin.a.a.b a;

   public g(co.uk.getmondo.common.pin.a.a.b var) {
      l.b(var, "operation");
      super();
      this.a = var;
   }

   public final co.uk.getmondo.common.pin.a.a.b a() {
      return this.a;
   }
}
