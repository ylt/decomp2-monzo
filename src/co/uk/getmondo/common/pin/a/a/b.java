package co.uk.getmondo.common.pin.a.a;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.s;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n\u0082\u0001\u0001\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/pin/data/model/PinOperation;", "Landroid/os/Parcelable;", "()V", "from", "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "getFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "intent", "Landroid/content/Intent;", "getIntent", "()Landroid/content/Intent;", "UpdateAddress", "Lco/uk/getmondo/common/pin/data/model/PinOperation$UpdateAddress;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class b implements Parcelable {
   private b() {
   }

   // $FF: synthetic method
   public b(i var) {
      this();
   }

   public abstract Intent a();

   public abstract Impression.PinFrom b();

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B\u001f\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0014\u001a\u00020\tHÆ\u0003J\t\u0010\u0015\u001a\u00020\u000bHÆ\u0003J'\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000bHÆ\u0001J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cHÖ\u0003J\t\u0010\u001d\u001a\u00020\u0018HÖ\u0001J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u0018H\u0016R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\n\u001a\u00020\u000bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006%"},
      d2 = {"Lco/uk/getmondo/common/pin/data/model/PinOperation$UpdateAddress;", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "intent", "Landroid/content/Intent;", "address", "Lco/uk/getmondo/model/LegacyAddress;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "(Landroid/content/Intent;Lco/uk/getmondo/model/LegacyAddress;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "getIntent", "()Landroid/content/Intent;", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends b implements Parcelable {
      public static final Creator CREATOR = (Creator)(new Creator() {
         public b.a a(Parcel var) {
            l.b(var, "source");
            return new b.a(var);
         }

         public b.a[] a(int var) {
            return new b.a[var];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var) {
            return this.a(var);
         }

         // $FF: synthetic method
         public Object[] newArray(int var) {
            return (Object[])this.a(var);
         }
      });
      public static final b.a a = new b.a((i)null);
      private final Intent b;
      private final s c;
      private final Impression.PinFrom d;

      public a(Intent var, s var, Impression.PinFrom var) {
         l.b(var, "intent");
         l.b(var, "address");
         l.b(var, "from");
         super((i)null);
         this.b = var;
         this.c = var;
         this.d = var;
      }

      // $FF: synthetic method
      public a(Intent var, s var, Impression.PinFrom var, int var, i var) {
         if((var & 4) != 0) {
            var = Impression.PinFrom.ADDRESS_CHANGE;
         }

         this(var, var, var);
      }

      public a(Parcel var) {
         l.b(var, "source");
         Parcelable var = var.readParcelable(Intent.class.getClassLoader());
         l.a(var, "source.readParcelable<In…::class.java.classLoader)");
         Intent var = (Intent)var;
         var = var.readParcelable(s.class.getClassLoader());
         l.a(var, "source.readParcelable<Le…::class.java.classLoader)");
         this(var, (s)var, Impression.PinFrom.values()[var.readInt()]);
      }

      public Intent a() {
         return this.b;
      }

      public Impression.PinFrom b() {
         return this.d;
      }

      public final s c() {
         return this.c;
      }

      public int describeContents() {
         return 0;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label30: {
               if(var instanceof b.a) {
                  b.a var = (b.a)var;
                  if(l.a(this.a(), var.a()) && l.a(this.c, var.c) && l.a(this.b(), var.b())) {
                     break label30;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         Intent var = this.a();
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         s var = this.c;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         Impression.PinFrom var = this.b();
         if(var != null) {
            var = var.hashCode();
         }

         return (var + var * 31) * 31 + var;
      }

      public String toString() {
         return "UpdateAddress(intent=" + this.a() + ", address=" + this.c + ", from=" + this.b() + ")";
      }

      public void writeToParcel(Parcel var, int var) {
         l.b(var, "dest");
         var.writeParcelable((Parcelable)this.a(), 0);
         var.writeParcelable((Parcelable)this.c, 0);
         var.writeInt(this.b().ordinal());
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/common/pin/data/model/PinOperation$UpdateAddress$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/common/pin/data/model/PinOperation$UpdateAddress;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
