package co.uk.getmondo.common.pin.a.a;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u00152\u00020\u0001:\u0002\u0014\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/pin/data/model/Challenge;", "", "challengeType", "Lco/uk/getmondo/common/pin/data/model/Challenge$ChallengeType;", "value", "", "(Lco/uk/getmondo/common/pin/data/model/Challenge$ChallengeType;Ljava/lang/String;)V", "getChallengeType", "()Lco/uk/getmondo/common/pin/data/model/Challenge$ChallengeType;", "getValue", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "ChallengeType", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.b a = new a.b((i)null);
   private final a.a b;
   private final String c;

   public a(a.a var, String var) {
      l.b(var, "challengeType");
      l.b(var, "value");
      super();
      this.b = var;
      this.c = var;
   }

   public final a.a a() {
      return this.b;
   }

   public final String b() {
      return this.c;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof a) {
               a var = (a)var;
               if(l.a(this.b, var.b) && l.a(this.c, var.c)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      a.a var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.c;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "Challenge(challengeType=" + this.b + ", value=" + this.c + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/common/pin/data/model/Challenge$ChallengeType;", "", "type", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getType", "()Ljava/lang/String;", "PIN", "SECURE_TOKEN", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b;

      private final String d;

      static {
         a.a var = new a.a("PIN", 0, "pin");
         a = var;
         a.a var = new a.a("SECURE_TOKEN", 1, "secure_token");
         b = var;
      }

      protected a(String var) {
         l.b(var, "type");
         super(var, var);
         this.d = var;
      }

      public final String a() {
         return this.d;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0005¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/common/pin/data/model/Challenge$Companion;", "", "()V", "pin", "Lco/uk/getmondo/common/pin/data/model/Challenge;", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private b() {
      }

      // $FF: synthetic method
      public b(i var) {
         this();
      }

      public final a a(String var) {
         l.b(var, "pin");
         return new a(a.a.a, var);
      }
   }
}
