package co.uk.getmondo.common.pin;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class d {
   // $FF: synthetic field
   public static final int[] a = new int[co.uk.getmondo.payments.a.h.values().length];

   static {
      a[co.uk.getmondo.payments.a.h.a.ordinal()] = 1;
      a[co.uk.getmondo.payments.a.h.b.ordinal()] = 2;
   }
}
