package co.uk.getmondo.common.ui;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

public class VideoImageView_ViewBinding implements Unbinder {
   private VideoImageView a;

   public VideoImageView_ViewBinding(VideoImageView var, View var) {
      this.a = var;
      var.exoPlayerView = (SimpleExoPlayerView)Utils.findRequiredViewAsType(var, 2131821711, "field 'exoPlayerView'", SimpleExoPlayerView.class);
      var.placeholderImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821712, "field 'placeholderImageView'", ImageView.class);
   }

   public void unbind() {
      VideoImageView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.exoPlayerView = null;
         var.placeholderImageView = null;
      }
   }
}
