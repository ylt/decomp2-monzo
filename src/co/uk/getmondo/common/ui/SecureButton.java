package co.uk.getmondo.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SecureButton extends android.support.v7.widget.i {
   public SecureButton(Context var) {
      super(var);
      this.setFilterTouchesWhenObscured(true);
   }

   public SecureButton(Context var, AttributeSet var) {
      super(var, var);
      this.setFilterTouchesWhenObscured(true);
   }

   public SecureButton(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.setFilterTouchesWhenObscured(true);
   }

   public boolean onFilterTouchEventForSecurity(MotionEvent var) {
      boolean var;
      if(!super.onFilterTouchEventForSecurity(var)) {
         var = true;
      } else {
         var = false;
      }

      if(var && this.getContext() instanceof android.support.v4.app.j) {
         co.uk.getmondo.common.d.a.a(this.getContext().getString(2131362625), this.getContext().getString(2131362624), false).show(((android.support.v4.app.j)this.getContext()).getSupportFragmentManager(), "MESSAGE_FRAGMENT_TAG");
      }

      boolean var;
      if(!var) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void setFilterTouchesWhenObscured(boolean var) {
      super.setFilterTouchesWhenObscured(true);
   }
}
