package co.uk.getmondo.common.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import java.text.NumberFormat;
import java.util.Locale;

public class AnimatedNumberView extends TextView {
   private ObjectAnimator a;
   private int b;
   private int c;

   public AnimatedNumberView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public AnimatedNumberView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public AnimatedNumberView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      if(var != null) {
         TypedArray var = this.getContext().obtainStyledAttributes(var, co.uk.getmondo.c.b.AnimatedNumberView);
         this.c = var.getInteger(0, 3000);
         var.recycle();
      } else {
         this.c = 3000;
      }

   }

   private void setNumberAsString(int var) {
      this.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format((long)var)));
   }

   public void a(int var) {
      if(this.a != null) {
         this.a.cancel();
      }

      this.a = ObjectAnimator.ofInt(this, "numberInterpolator", new int[]{Math.max(this.b - var, 0), this.b});
      this.a.setInterpolator(new DecelerateInterpolator(3.0F));
      this.a.setDuration((long)this.c);
      this.a.start();
   }

   public void setNumber(int var) {
      if(var == 0) {
         this.setText(2131362497);
      } else {
         this.setNumberAsString(var);
      }

      if(this.c == 0) {
         this.setNumberAsString(var);
      } else {
         if(this.a != null) {
            this.a.cancel();
         }

         this.a = ObjectAnimator.ofInt(this, "numberInterpolator", new int[]{this.b, var});
         this.a.setInterpolator(new DecelerateInterpolator(3.0F));
         this.a.setDuration((long)this.c);
         this.a.start();
      }

   }

   public void setNumberInterpolator(int var) {
      this.b = var;
      this.setNumberAsString(var);
   }
}
