package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.at;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u0010H\u0014J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u00102\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014R\u000e\u0010\t\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/common/ui/CopyableLinearLayout;", "Landroid/support/v7/widget/LinearLayoutCompat;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "copyAsContent", "copyAsLabel", "checkLayoutParams", "", "p", "Landroid/view/ViewGroup$LayoutParams;", "generateDefaultLayoutParams", "Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;", "generateLayoutParams", "LayoutParams", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CopyableLinearLayout extends at {
   private final int a;
   private final int b;

   public CopyableLinearLayout(Context var) {
      this(var, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public CopyableLinearLayout(Context var, AttributeSet var) {
      this(var, var, 0, 4, (kotlin.d.b.i)null);
   }

   public CopyableLinearLayout(final Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      this.a = 1;
      this.b = 2;
      TypedArray var = var.obtainStyledAttributes(var, co.uk.getmondo.c.b.CopyableLinearLayout);
      final kotlin.d.b.x.c var = new kotlin.d.b.x.c();
      var.a = var.getString(0);
      if((String)var.a == null) {
         var.a = " ";
      }

      var.recycle();
      this.setOnLongClickListener((OnLongClickListener)(new OnLongClickListener() {
         public final boolean onLongClick(View varx) {
            Iterable var = (Iterable)m.a(CopyableLinearLayout.this);
            Collection var = (Collection)(new ArrayList());
            Iterator var = var.iterator();

            boolean var;
            Object varx;
            while(var.hasNext()) {
               label44: {
                  varx = var.next();
                  View var = (View)varx;
                  if(var instanceof TextView) {
                     LayoutParams var = ((TextView)var).getLayoutParams();
                     if(var == null) {
                        throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams");
                     }

                     if(((CopyableLinearLayout.a)var).a() == CopyableLinearLayout.this.a) {
                        var = true;
                        break label44;
                     }
                  }

                  var = false;
               }

               if(var) {
                  var.add(varx);
               }
            }

            var = (Iterable)((List)var);
            String var = (String)var.a;
            kotlin.d.b.l.a(var, "separator");
            var = kotlin.a.m.a(var, (CharSequence)var, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null.a, 30, (Object)null);
            Iterable var = (Iterable)m.a(CopyableLinearLayout.this);
            Collection var = (Collection)(new ArrayList());
            Iterator var = var.iterator();

            while(var.hasNext()) {
               label34: {
                  varx = var.next();
                  View var = (View)varx;
                  if(var instanceof TextView) {
                     LayoutParams var = ((TextView)var).getLayoutParams();
                     if(var == null) {
                        throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams");
                     }

                     if(((CopyableLinearLayout.a)var).a() == CopyableLinearLayout.this.b) {
                        var = true;
                        break label34;
                     }
                  }

                  var = false;
               }

               if(var) {
                  var.add(varx);
               }
            }

            var = (Iterable)((List)var);
            String var = (String)var.a;
            kotlin.d.b.l.a(var, "separator");
            String var = kotlin.a.m.a(var, (CharSequence)var, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null.a, 30, (Object)null);
            co.uk.getmondo.common.e.a(var, var, var);
            return true;
         }
      }));
   }

   // $FF: synthetic method
   public CopyableLinearLayout(Context var, AttributeSet var, int var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public android.support.v7.widget.at.a b(AttributeSet var) {
      kotlin.d.b.l.b(var, "attrs");
      Context var = this.getContext();
      kotlin.d.b.l.a(var, "context");
      return (android.support.v7.widget.at.a)(new CopyableLinearLayout.a(var, var));
   }

   protected android.support.v7.widget.at.a b(LayoutParams var) {
      return new android.support.v7.widget.at.a(var);
   }

   protected boolean checkLayoutParams(LayoutParams var) {
      return var instanceof CopyableLinearLayout.a;
   }

   // $FF: synthetic method
   public LayoutParams generateDefaultLayoutParams() {
      return (LayoutParams)this.j();
   }

   // $FF: synthetic method
   public LayoutParams generateLayoutParams(AttributeSet var) {
      return (LayoutParams)this.b(var);
   }

   // $FF: synthetic method
   public LayoutParams generateLayoutParams(LayoutParams var) {
      return (LayoutParams)this.b(var);
   }

   protected android.support.v7.widget.at.a j() {
      return new android.support.v7.widget.at.a(this.getContext(), (AttributeSet)null);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u000f\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/common/ui/CopyableLinearLayout$LayoutParams;", "Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "p", "Landroid/view/ViewGroup$LayoutParams;", "(Landroid/view/ViewGroup$LayoutParams;)V", "copyAs", "", "getCopyAs", "()I", "setCopyAs", "(I)V", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends android.support.v7.widget.at.a {
      private int a;

      public a(Context var, AttributeSet var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "attrs");
         super(var, var);
         TypedArray var = var.obtainStyledAttributes(var, co.uk.getmondo.c.b.CopyableLinearLayout_layout);
         this.a = var.getInteger(0, 0);
         var.recycle();
      }

      public final int a() {
         return this.a;
      }
   }
}
