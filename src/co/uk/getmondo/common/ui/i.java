package co.uk.getmondo.common.ui;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public final class i {
   public static Snackbar a(Context var, View var, String var, int var, boolean var) {
      int var;
      if(var) {
         var = 2131689680;
      } else {
         var = 2131689663;
      }

      var = android.support.v4.content.a.c(var, var);
      int var = android.support.v4.content.a.c(var, 2131689706);
      Snackbar var = Snackbar.a(var, var, var);
      var.b().setBackgroundColor(var);
      var.e(var);
      TextView var = (TextView)var.b().findViewById(2131821234);
      var.setTextColor(var);
      var.setMaxLines(5);
      return var;
   }
}
