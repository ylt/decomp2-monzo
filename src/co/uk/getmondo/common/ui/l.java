package co.uk.getmondo.common.ui;

import com.google.android.exoplayer2.upstream.RawResourceDataSource;

// $FF: synthetic class
final class l implements com.google.android.exoplayer2.upstream.c.a {
   private final RawResourceDataSource a;

   private l(RawResourceDataSource var) {
      this.a = var;
   }

   public static com.google.android.exoplayer2.upstream.c.a a(RawResourceDataSource var) {
      return new l(var);
   }

   public com.google.android.exoplayer2.upstream.c a() {
      return VideoImageView.a(this.a);
   }
}
