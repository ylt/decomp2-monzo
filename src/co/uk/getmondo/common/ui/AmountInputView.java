package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.k.p;
import io.reactivex.n;
import java.util.Locale;
import java.util.regex.Pattern;

public class AmountInputView extends LinearLayout {
   private final co.uk.getmondo.common.i.b a = new co.uk.getmondo.common.i.b(false, false, true);
   private co.uk.getmondo.common.i.c b;
   private int c;
   private co.uk.getmondo.d.c d;
   @BindView(2131821730)
   EditText editText;
   @BindView(2131821729)
   TextInputLayout textInputLayout;
   @BindView(2131821728)
   TextView titleTextView;

   public AmountInputView(Context var) {
      super(var);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a((AttributeSet)null);
   }

   public AmountInputView(Context var, AttributeSet var) {
      super(var, var);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a(var);
   }

   public AmountInputView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a(var);
   }

   private void a(AttributeSet var) {
      boolean var = false;
      this.setOrientation(1);
      ButterKnife.bind(this, (View)LayoutInflater.from(this.getContext()).inflate(2131034429, this, true));
      if(var != null) {
         TypedArray var = this.getContext().getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.AmountInputView, 0, 0);

         try {
            String var = var.getString(0);
            if(p.c(var)) {
               co.uk.getmondo.common.i.c var = new co.uk.getmondo.common.i.c(var);
               this.b = var;
            }

            this.c = var.getInteger(1, 6);
            var = var.getBoolean(2, false);
         } finally {
            var.recycle();
         }
      }

      this.textInputLayout.setErrorEnabled(var);
      this.b();
      this.c();
      this.d();
   }

   private void b() {
      if(this.editText != null) {
         this.editText.setFilters(new InputFilter[]{new AmountInputView.a(this.c, this.b.a())});
      }

   }

   private void c() {
      if(this.titleTextView != null) {
         this.titleTextView.setText(this.getContext().getString(2131362010, new Object[]{this.b.a(Locale.ENGLISH)}));
      }

   }

   private void d() {
      if(this.editText != null && this.d != null) {
         this.editText.setText(this.a.a(this.d));
      }

   }

   public n a() {
      return com.b.a.d.e.c(this.editText);
   }

   public Editable getAmountText() {
      return this.editText.getText();
   }

   public void setCurrency(co.uk.getmondo.common.i.c var) {
      if(!this.b.equals(var)) {
         this.b = var;
         this.b();
         this.c();
      }

   }

   public void setDefaultAmount(co.uk.getmondo.d.c var) {
      this.d = var;
      this.d();
      if(this.b != var.l()) {
         this.b = var.l();
         this.c();
      }

   }

   public void setError(CharSequence var) {
      this.textInputLayout.setError(var);
   }

   private static class a implements InputFilter {
      private Pattern a;
      private final int b;
      private final int c;

      a(int var, int var) {
         String var;
         if(var > 0) {
            var = "[0-9.]*";
         } else {
            var = "[0-9]*";
         }

         this.a = Pattern.compile(var);
         this.c = var;
         this.b = var;
      }

      public CharSequence filter(CharSequence var, int var, int var, Spanned var, int var, int var) {
         var = var.subSequence(var, var);
         String var;
         if(!this.a.matcher(var).matches()) {
            var = "";
         } else {
            SpannableStringBuilder var = new SpannableStringBuilder(var);
            var.replace(var, var, var);
            var = var.toString();
            String[] var = var.split("\\.");
            if(var.length > 2) {
               var = "";
            } else {
               if(var.length > 0) {
                  var = var[0];
               }

               String var;
               if(var.length > 1) {
                  var = var[1];
               } else {
                  var = "";
               }

               if(var.length() <= this.c && var.length() <= this.b) {
                  var = null;
               } else {
                  var = "";
               }
            }
         }

         return var;
      }
   }
}
