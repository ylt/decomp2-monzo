package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import com.airbnb.lottie.LottieAnimationView;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.n;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ,\u00109\u001a\u00020.2\u0006\u0010\u001f\u001a\u00020\u001b2\b\b\u0002\u0010:\u001a\u00020\u00122\b\b\u0002\u00106\u001a\u00020\u001b2\b\b\u0003\u0010\f\u001a\u00020\u0007J\u001a\u0010;\u001a\u00020.2\u0006\u0010<\u001a\u00020\u00122\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u001bJ\f\u0010=\u001a\b\u0012\u0004\u0012\u00020.0>R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R&\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R&\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u000e\"\u0004\b\u001a\u0010\u0010R\u001e\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b@BX\u0082\u000e¢\u0006\b\n\u0000\"\u0004\b\u001d\u0010\u001eR(\u0010\u001f\u001a\u0004\u0018\u00010\u001b2\b\u0010\u000b\u001a\u0004\u0018\u00010\u001b8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010\u001eR$\u0010#\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0007@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u000e\"\u0004\b%\u0010\u0010R&\u0010&\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010\u000e\"\u0004\b(\u0010\u0010R$\u0010)\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0015\"\u0004\b+\u0010\u0017R\"\u0010,\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R$\u00103\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0015\"\u0004\b5\u0010\u0017R$\u00106\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b7\u0010!\"\u0004\b8\u0010\u001e¨\u0006?"},
   d2 = {"Lco/uk/getmondo/common/ui/LoadingErrorView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "circleBackground", "Landroid/graphics/drawable/ShapeDrawable;", "value", "iconRes", "getIconRes", "()I", "setIconRes", "(I)V", "iconTint", "", "loading", "getLoading", "()Z", "setLoading", "(Z)V", "loadingIconBackgroundColor", "getLoadingIconBackgroundColor", "setLoadingIconBackgroundColor", "", "lottieAnimationFileName", "setLottieAnimationFileName", "(Ljava/lang/String;)V", "message", "getMessage", "()Ljava/lang/String;", "setMessage", "messageTextColor", "getMessageTextColor", "setMessageTextColor", "overlayBackgroundColor", "getOverlayBackgroundColor", "setOverlayBackgroundColor", "overlayEnabled", "getOverlayEnabled", "setOverlayEnabled", "retryClickListener", "Lkotlin/Function0;", "", "getRetryClickListener", "()Lkotlin/jvm/functions/Function0;", "setRetryClickListener", "(Lkotlin/jvm/functions/Function0;)V", "retryEnabled", "getRetryEnabled", "setRetryEnabled", "retryLabel", "getRetryLabel", "setRetryLabel", "error", "shouldRetry", "load", "isLoading", "retryClicks", "Lio/reactivex/Observable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LoadingErrorView extends ConstraintLayout {
   private final ShapeDrawable c;
   private boolean d;
   private int e;
   private boolean f;
   private int g;
   private int h;
   private boolean i;
   private int j;
   private kotlin.d.a.a k;
   private int l;
   private String m;
   private HashMap n;

   public LoadingErrorView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public LoadingErrorView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (kotlin.d.b.i)null);
   }

   public LoadingErrorView(Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      this.c = new ShapeDrawable((Shape)(new OvalShape()));
      this.e = 2131689558;
      this.h = 2131689589;
      this.j = 2131689560;
      this.m = "lottie/loading_spinner_colored.json";
      View.inflate(var, 2131034434, (ViewGroup)this);
      final FrameLayout var = (FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout);
      var.getViewTreeObserver().addOnGlobalLayoutListener((OnGlobalLayoutListener)(new OnGlobalLayoutListener() {
         public void onGlobalLayout() {
            var.getViewTreeObserver().removeOnGlobalLayoutListener((OnGlobalLayoutListener)this);
            FrameLayout var = (FrameLayout)var;
            ShapeDrawable var = LoadingErrorView.this.c;
            var.setIntrinsicWidth(((FrameLayout)((View)var).findViewById(co.uk.getmondo.c.a.progressLayout)).getMeasuredWidth());
            var.setIntrinsicHeight(((FrameLayout)((View)var).findViewById(co.uk.getmondo.c.a.progressLayout)).getMeasuredHeight());
         }
      }));
      if(var != null) {
         TypedArray var = var.obtainStyledAttributes(var, co.uk.getmondo.c.b.LoadingErrorView, var, 0);
         this.setOverlayEnabled(var.getBoolean(0, false));
         this.setOverlayBackgroundColor(var.getResourceId(1, 2131689558));
         this.setLoadingIconBackgroundColor(var.getResourceId(5, 0));
         this.setLoading(var.getBoolean(2, false));
         this.l = var.getResourceId(4, 0);
         this.setIconRes(var.getResourceId(3, 0));
         this.setMessage(var.getString(6));
         this.setRetryEnabled(var.getBoolean(8, false));
         String var = var.getString(9);
         if(var == null) {
            var = ae.a(this, 2131362405);
         }

         this.setRetryLabel(var);
         var = var.getString(10);
         if(var == null) {
            var = "lottie/loading_spinner_colored.json";
         }

         this.setLottieAnimationFileName(var);
         this.setMessageTextColor(var.getResourceId(7, 2131689560));
         var.recycle();
      }

      ((Button)this.b(co.uk.getmondo.c.a.retryButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            kotlin.d.a.a var = LoadingErrorView.this.getRetryClickListener();
            if(var != null) {
               n var = (n)var.v_();
            }

         }
      }));
   }

   // $FF: synthetic method
   public LoadingErrorView(Context var, AttributeSet var, int var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 2130772372;
      }

      this(var, var, var);
   }

   // $FF: synthetic method
   public static void a(LoadingErrorView var, String var, boolean var, String var, int var, int var, Object var) {
      if((var & 2) != 0) {
         var = var.i;
      }

      if((var & 4) != 0) {
         var = var.getRetryLabel();
      }

      if((var & 8) != 0) {
         var = var.g;
      }

      var.a(var, var, var, var);
   }

   private final void setLottieAnimationFileName(String var) {
      this.m = var;
      ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).setAnimation(this.m);
   }

   public final void a(String var, boolean var, String var, int var) {
      kotlin.d.b.l.b(var, "message");
      kotlin.d.b.l.b(var, "retryLabel");
      this.setMessage(var);
      this.setRetryEnabled(var);
      this.setRetryLabel(var);
      this.setIconRes(var);
   }

   public final void a(boolean var, String var) {
      this.setLoading(var);
      this.setMessage(var);
      ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout));
      ae.b((Button)this.b(co.uk.getmondo.c.a.retryButton));
   }

   public View b(int var) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var = (View)this.n.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.n.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final io.reactivex.n c() {
      io.reactivex.n var = io.reactivex.n.create((p)(new p() {
         public final void a(final o var) {
            kotlin.d.b.l.b(var, "emitter");
            LoadingErrorView.this.setRetryClickListener((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var.a(n.a);
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return n.a;
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  LoadingErrorView.this.setRetryClickListener((kotlin.d.a.a)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…stener = null }\n        }");
      return var;
   }

   public final int getIconRes() {
      return this.g;
   }

   public final boolean getLoading() {
      return this.f;
   }

   public final int getLoadingIconBackgroundColor() {
      return this.h;
   }

   public final String getMessage() {
      return ((TextView)this.b(co.uk.getmondo.c.a.messageText)).getText().toString();
   }

   public final int getMessageTextColor() {
      return this.j;
   }

   public final int getOverlayBackgroundColor() {
      return this.e;
   }

   public final boolean getOverlayEnabled() {
      return this.d;
   }

   public final kotlin.d.a.a getRetryClickListener() {
      return this.k;
   }

   public final boolean getRetryEnabled() {
      return this.i;
   }

   public final String getRetryLabel() {
      return ((Button)this.b(co.uk.getmondo.c.a.retryButton)).getText().toString();
   }

   public final void setIconRes(int var) {
      this.g = var;
      if(var != 0) {
         Drawable var = android.support.v4.content.a.a(this.getContext(), var);
         if(this.l != 0) {
            android.support.v4.a.a.a.a(var, android.support.v4.content.a.c(this.getContext(), this.l));
         }

         ((ImageView)this.b(co.uk.getmondo.c.a.iconImageView)).setImageDrawable(var);
         ae.a((View)((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout)));
      } else {
         ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout));
      }

   }

   public final void setLoading(boolean var) {
      this.f = var;
      if(var) {
         ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).c();
         ae.a((View)((FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout)));
      } else {
         ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout));
         ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).d();
      }

   }

   public final void setLoadingIconBackgroundColor(int var) {
      this.h = var;
      switch(var) {
      default:
         final ShapeDrawable var = this.c;
         var.getPaint().setColor(android.support.v4.content.a.c(this.getContext(), var));
         this.post((Runnable)(new Runnable() {
            public final void run() {
               ((ImageView)LoadingErrorView.this.b(co.uk.getmondo.c.a.progressBackgroundView)).setImageDrawable((Drawable)var);
               ((ImageView)LoadingErrorView.this.b(co.uk.getmondo.c.a.iconBackgroundView)).setImageDrawable((Drawable)var);
            }
         }));
      case 0:
      }
   }

   public final void setMessage(String var) {
      ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setText((CharSequence)var);
      if(var == null) {
         ae.b((TextView)this.b(co.uk.getmondo.c.a.messageText));
      } else {
         ae.a((View)((TextView)this.b(co.uk.getmondo.c.a.messageText)));
      }

   }

   public final void setMessageTextColor(int var) {
      this.j = var;
      ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextColor(android.support.v4.content.a.c(this.getContext(), var));
   }

   public final void setOverlayBackgroundColor(int var) {
      this.e = var;
      this.b(co.uk.getmondo.c.a.loadingOverlay).setBackgroundResource(var);
   }

   public final void setOverlayEnabled(boolean var) {
      this.d = var;
      if(var) {
         ae.a(this.b(co.uk.getmondo.c.a.loadingOverlay));
         ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextSize(2, 16.0F);
      } else {
         ae.b(this.b(co.uk.getmondo.c.a.loadingOverlay));
         ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextSize(2, 14.0F);
      }

   }

   public final void setRetryClickListener(kotlin.d.a.a var) {
      this.k = var;
   }

   public final void setRetryEnabled(boolean var) {
      this.i = var;
      if(var) {
         ae.a((View)((Button)this.b(co.uk.getmondo.c.a.retryButton)));
      } else {
         ae.b((Button)this.b(co.uk.getmondo.c.a.retryButton));
      }

   }

   public final void setRetryLabel(String var) {
      kotlin.d.b.l.b(var, "value");
      ((Button)this.b(co.uk.getmondo.c.a.retryButton)).setText((CharSequence)var);
   }
}
