package co.uk.getmondo.common.ui;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/ui/TypefaceSpan;", "Landroid/text/style/MetricAffectingSpan;", "typeface", "Landroid/graphics/Typeface;", "(Landroid/graphics/Typeface;)V", "apply", "", "paint", "Landroid/graphics/Paint;", "updateDrawState", "drawState", "Landroid/text/TextPaint;", "updateMeasureState", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends MetricAffectingSpan {
   private final Typeface a;

   public k(Typeface var) {
      kotlin.d.b.l.b(var, "typeface");
      super();
      this.a = var;
   }

   private final void a(Paint var) {
      Typeface var = var.getTypeface();
      int var;
      if(var != null) {
         var = var.getStyle();
      } else {
         var = 0;
      }

      var &= ~this.a.getStyle();
      if((var & 1) != 0) {
         var.setFakeBoldText(true);
      }

      if((var & 2) != 0) {
         var.setTextSkewX(-0.25F);
      }

      var.setTypeface(this.a);
   }

   public void updateDrawState(TextPaint var) {
      kotlin.d.b.l.b(var, "drawState");
      this.a((Paint)var);
   }

   public void updateMeasureState(TextPaint var) {
      kotlin.d.b.l.b(var, "paint");
      this.a((Paint)var);
   }
}
