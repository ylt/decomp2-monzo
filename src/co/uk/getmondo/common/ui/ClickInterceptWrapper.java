package co.uk.getmondo.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public class ClickInterceptWrapper extends FrameLayout {
   private OnClickListener a;
   private GestureDetector b = new GestureDetector(this.getContext(), new SimpleOnGestureListener() {
      public boolean onDoubleTap(MotionEvent var) {
         return false;
      }

      public boolean onSingleTapConfirmed(MotionEvent var) {
         if(ClickInterceptWrapper.this.a != null) {
            ClickInterceptWrapper.this.a.onClick(ClickInterceptWrapper.this);
         }

         return false;
      }
   });

   public ClickInterceptWrapper(Context var) {
      super(var);
   }

   public ClickInterceptWrapper(Context var, AttributeSet var) {
      super(var, var);
   }

   public ClickInterceptWrapper(Context var, AttributeSet var, int var) {
      super(var, var, var);
   }

   public boolean onInterceptTouchEvent(MotionEvent var) {
      this.b.onTouchEvent(var);
      return super.onInterceptTouchEvent(var);
   }

   public void setOnClickListener(OnClickListener var) {
      this.a = var;
   }
}
