package co.uk.getmondo.common.ui;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AmountInputView_ViewBinding implements Unbinder {
   private AmountInputView a;

   public AmountInputView_ViewBinding(AmountInputView var, View var) {
      this.a = var;
      var.editText = (EditText)Utils.findRequiredViewAsType(var, 2131821730, "field 'editText'", EditText.class);
      var.textInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821729, "field 'textInputLayout'", TextInputLayout.class);
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821728, "field 'titleTextView'", TextView.class);
   }

   public void unbind() {
      AmountInputView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.editText = null;
         var.textInputLayout = null;
         var.titleTextView = null;
      }
   }
}
