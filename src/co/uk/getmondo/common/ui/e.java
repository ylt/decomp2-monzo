package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.support.v7.widget.al;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001BI\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0005\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005\u0012\b\b\u0002\u0010\n\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0005¢\u0006\u0002\u0010\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/ui/InsetDividerItemDecoration;", "Landroid/support/v7/widget/DividerItemDecoration;", "context", "Landroid/content/Context;", "orientation", "", "dividerDrawable", "Landroid/graphics/drawable/Drawable;", "insetLeft", "insetTop", "insetRight", "insetBottom", "(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIII)V", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends al {
   public e(Context var, int var, Drawable var, int var, int var, int var, int var) {
      kotlin.d.b.l.b(var, "dividerDrawable");
      super(var, var);
      this.a((Drawable)(new InsetDrawable(var, var, var, var, var)));
   }

   // $FF: synthetic method
   public e(Context var, int var, Drawable var, int var, int var, int var, int var, int var, kotlin.d.b.i var) {
      if((var & 4) != 0) {
         var = android.support.v4.content.a.a(var, 2130838012);
         kotlin.d.b.l.a(var, "ContextCompat.getDrawabl….drawable.simple_divider)");
      }

      if((var & 8) != 0) {
         var = 0;
      }

      if((var & 16) != 0) {
         var = 0;
      }

      if((var & 32) != 0) {
         var = 0;
      }

      if((var & 64) != 0) {
         var = 0;
      }

      this(var, var, var, var, var, var, var);
   }
}
