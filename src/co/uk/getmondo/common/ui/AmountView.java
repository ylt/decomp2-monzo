package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.aa;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\t\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J0\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\n2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0007H\u0002J\u0010\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u0007H\u0002J\u000e\u0010'\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eJ\u0018\u0010(\u001a\u00020\u00192\b\b\u0001\u0010)\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001eR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006*"},
   d2 = {"Lco/uk/getmondo/common/ui/AmountView;", "Landroid/support/v7/widget/AppCompatTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currencyFont", "", "currencyTextSize", "fractionalFont", "fractionalSemiTransparent", "", "fractionalTextSize", "integerFont", "integerTextSize", "negativeAmountColor", "positiveAmountColor", "showCurrency", "showFractionalPart", "showNegativeIfDebit", "showPlusIfCredit", "applyAttributes", "", "a", "Landroid/content/res/TypedArray;", "bind", "amount", "Lco/uk/getmondo/model/Amount;", "currencySymbol", "amountValue", "", "integerPartAbs", "fractionalPartAbs", "fractionalDigits", "convertToSemiTransparent", "originalColor", "setAmount", "setAmountWithStyle", "style", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AmountView extends aa {
   private boolean a;
   private boolean b;
   private boolean c;
   private boolean d;
   private boolean e;
   private int f;
   private int g;
   private int h;
   private int i;
   private int j;
   private String k;
   private String l;
   private String m;

   public AmountView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AmountView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (kotlin.d.b.i)null);
   }

   public AmountView(Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      if(var != null) {
         TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.AmountView, var, 2131493041);

         try {
            kotlin.d.b.l.a(var, "attributes");
            this.a(var);
         } finally {
            var.recycle();
         }
      }

      if(this.isInEditMode()) {
         this.a("£", 12050L, 120L, 50, 2);
      }

   }

   // $FF: synthetic method
   public AmountView(Context var, AttributeSet var, int var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   private final int a(int var) {
      return Color.argb(102, var >> 16 & 255, var >> 8 & 255, var & 255);
   }

   private final void a(TypedArray var) {
      this.a = var.getBoolean(9, true);
      this.b = var.getBoolean(7, false);
      this.c = var.getBoolean(8, false);
      this.d = var.getBoolean(6, true);
      this.f = var.getColor(3, android.support.v4.content.a.c(this.getContext(), 2131689706));
      this.g = var.getColor(4, this.f);
      this.e = var.getBoolean(5, false);
      this.h = var.getDimensionPixelSize(0, this.getResources().getDimensionPixelSize(2131427482));
      this.i = var.getDimensionPixelSize(1, this.getResources().getDimensionPixelSize(2131427431));
      this.j = var.getDimensionPixelSize(2, this.getResources().getDimensionPixelSize(2131427427));
      String var;
      if(var.hasValue(12)) {
         var = var.getString(12);
      } else {
         var = "sans-serif-light";
      }

      this.k = var;
      if(var.hasValue(10)) {
         var = var.getString(10);
      } else {
         var = "sans-serif-light";
      }

      this.l = var;
      String var;
      if(var.hasValue(11)) {
         var = var.getString(11);
      } else {
         var = "sans-serif";
      }

      this.m = var;
   }

   private final void a(co.uk.getmondo.d.c var) {
      this.a(var.g(), var.k(), var.c(), var.d(), var.l().a());
   }

   private final void a(String var, long var, long var, int var, int var) {
      this.setText((CharSequence)null);
      String var;
      if(this.b && var > 0L) {
         var = "+";
      } else if(this.c && var < 0L) {
         var = "-";
      } else {
         var = "";
      }

      if(!this.d) {
         var = "";
      }

      String var = co.uk.getmondo.common.i.b.a.a(var);
      String var;
      if(!this.a) {
         var = "";
      } else {
         var = co.uk.getmondo.common.i.d.a(var, var);
      }

      SpannableString var = new SpannableString((CharSequence)(var + var + var + var));
      if(var < 0L) {
         var = this.g;
      } else {
         var = this.f;
      }

      boolean var;
      if(((CharSequence)var).length() > 0) {
         var = true;
      } else {
         var = false;
      }

      int var;
      Typeface var;
      if(var) {
         var = var.length();
         if(this.d) {
            var = this.h;
         } else {
            var = this.i;
         }

         String var;
         if(this.d) {
            var = this.k;
         } else {
            var = this.l;
         }

         var.setSpan(new ForegroundColorSpan(var), 0, var, 33);
         var.setSpan(new AbsoluteSizeSpan(var), 0, var, 33);
         var = Typeface.create(var, 0);
         kotlin.d.b.l.a(var, "Typeface.create(prefixFont, Typeface.NORMAL)");
         var.setSpan(new k(var), 0, var, 33);
      }

      if(((CharSequence)var).length() > 0) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         var = var.length();
         var = var.length() + var;
         var.setSpan(new ForegroundColorSpan(var), var, var, 33);
         var.setSpan(new AbsoluteSizeSpan(this.h), var, var, 33);
         var = Typeface.create(this.k, 0);
         kotlin.d.b.l.a(var, "Typeface.create(currencyFont, Typeface.NORMAL)");
         var.setSpan(new k(var), var, var, 33);
      }

      if(((CharSequence)var).length() > 0) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         var = var.length() + var.length();
         var = var.length() + var;
         var.setSpan(new ForegroundColorSpan(var), var, var, 33);
         var.setSpan(new AbsoluteSizeSpan(this.i), var, var, 33);
         var = Typeface.create(this.l, 0);
         kotlin.d.b.l.a(var, "Typeface.create(integerFont, Typeface.NORMAL)");
         var.setSpan(new k(var), var, var, 33);
      }

      if(((CharSequence)var).length() > 0) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         var = var;
         if(this.e) {
            var = this.a(var);
         }

         var = var.length() + var.length() + var.length();
         var = var.length() + var;
         var.setSpan(new ForegroundColorSpan(var), var, var, 33);
         var.setSpan(new AbsoluteSizeSpan(this.j), var, var, 33);
         Typeface var = Typeface.create(this.m, 0);
         kotlin.d.b.l.a(var, "Typeface.create(fractionalFont, Typeface.NORMAL)");
         var.setSpan(new k(var), var, var, 33);
      }

      this.setText((CharSequence)var);
   }

   public final void a(int var, co.uk.getmondo.d.c var) {
      kotlin.d.b.l.b(var, "amount");
      TypedArray var = this.getContext().obtainStyledAttributes(var, co.uk.getmondo.c.b.AmountView);

      try {
         kotlin.d.b.l.a(var, "attributes");
         this.a(var);
      } finally {
         var.recycle();
      }

      this.a(var);
   }

   public final void setAmount(co.uk.getmondo.d.c var) {
      kotlin.d.b.l.b(var, "amount");
      this.a(var);
   }
}
