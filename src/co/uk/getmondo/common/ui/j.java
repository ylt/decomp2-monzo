package co.uk.getmondo.common.ui;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u000f\u001a\u00020\u0010J\u0018\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eJ \u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/common/ui/SpannableBuilder;", "", "text", "", "textToFormat", "(Ljava/lang/String;Ljava/lang/String;)V", "matcher", "Ljava/util/regex/Matcher;", "spannableString", "Landroid/text/SpannableString;", "apply", "", "span", "applyOnlyToSubstring", "", "build", "", "setColour", "colour", "", "setSize", "size", "setTypeface", "fontFamily", "style", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class j {
   private final Matcher a;
   private final SpannableString b;

   public j(String var, String var) {
      kotlin.d.b.l.b(var, "text");
      kotlin.d.b.l.b(var, "textToFormat");
      super();
      Matcher var = Pattern.compile(var).matcher((CharSequence)var);
      kotlin.d.b.l.a(var, "Pattern.compile(textToFormat).matcher(text)");
      this.a = var;
      this.b = new SpannableString((CharSequence)var);
   }

   private final void a(Object var, boolean var) {
      if(!var) {
         this.b.setSpan(var, 0, this.b.length(), 33);
      } else {
         while(true) {
            if(!this.a.find()) {
               this.a.reset();
               break;
            }

            this.b.setSpan(var, this.a.start(), this.a.end(), 33);
         }
      }

   }

   public final j a(int var, boolean var) {
      this.a(new ForegroundColorSpan(var), var);
      return this;
   }

   public final j a(String var, int var, boolean var) {
      kotlin.d.b.l.b(var, "fontFamily");
      Typeface var = Typeface.create(var, var);
      kotlin.d.b.l.a(var, "Typeface.create(fontFamily, style)");
      this.a(new k(var), var);
      return this;
   }

   public final CharSequence a() {
      return (CharSequence)this.b;
   }
}
