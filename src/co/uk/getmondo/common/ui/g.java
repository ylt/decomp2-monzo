package co.uk.getmondo.common.ui;

import android.view.View;
import android.view.View.OnFocusChangeListener;

// $FF: synthetic class
final class g implements OnFocusChangeListener {
   private final PinEntryView a;

   private g(PinEntryView var) {
      this.a = var;
   }

   public static OnFocusChangeListener a(PinEntryView var) {
      return new g(var);
   }

   public void onFocusChange(View var, boolean var) {
      PinEntryView.a(this.a, var, var);
   }
}
