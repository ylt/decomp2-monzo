package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import co.uk.getmondo.common.k.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\fB\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tR\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator;", "", "colors", "", "", "(Ljava/util/List;)V", "from", "Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;", "text", "", "generateColor", "Companion", "Creator", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.a a = new a.a((kotlin.d.b.i)null);
   private final List b;

   public a(List var) {
      kotlin.d.b.l.b(var, "colors");
      super();
      this.b = var;
   }

   public static final a a(Context var) {
      kotlin.d.b.l.b(var, "context");
      return a.a(var);
   }

   public final a.b a(String var) {
      kotlin.d.b.l.b(var, "text");
      if(kotlin.h.j.a((CharSequence)var)) {
         throw (Throwable)(new IllegalArgumentException("'Input is blank"));
      } else {
         boolean var;
         if(kotlin.h.j.f((CharSequence)var) != 43 && !Character.isDigit(kotlin.h.j.f((CharSequence)var))) {
            var = false;
         } else {
            var = true;
         }

         String var;
         if(var) {
            var = "#";
         } else {
            var = String.valueOf(kotlin.h.j.f((CharSequence)var));
         }

         return new a.b(var, this.b(var));
      }
   }

   public final int b(String var) {
      kotlin.d.b.l.b(var, "text");
      CharSequence var = (CharSequence)var;
      int var = 0;

      int var;
      for(var = 0; var < var.length(); ++var) {
         var += var.charAt(var);
      }

      var = var % this.b.size();
      if(var < this.b.size()) {
         var = ((Number)this.b.get(var)).intValue();
      } else {
         var = ((Number)this.b.get(0)).intValue();
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator$Companion;", "", "()V", "ofCategories", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final a a(Context var) {
         kotlin.d.b.l.b(var, "context");
         Object[] var = (Object[])(new co.uk.getmondo.d.h[]{co.uk.getmondo.d.h.GROCERIES, co.uk.getmondo.d.h.ENTERTAINMENT, co.uk.getmondo.d.h.EATING_OUT, co.uk.getmondo.d.h.SHOPPING, co.uk.getmondo.d.h.HOLIDAYS, co.uk.getmondo.d.h.EXPENSES, co.uk.getmondo.d.h.TRANSPORT, co.uk.getmondo.d.h.CASH});
         Collection var = (Collection)(new ArrayList(var.length));

         for(int var = 0; var < var.length; ++var) {
            var.add(Integer.valueOf(android.support.v4.content.a.c(var, ((co.uk.getmondo.d.h)var[var]).b())));
         }

         return new a((List)var);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J&\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u0011H\u0007R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012"},
      d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;", "", "text", "", "color", "", "(Ljava/lang/String;I)V", "getColor", "()I", "getText", "()Ljava/lang/String;", "create", "Landroid/graphics/drawable/Drawable;", "fontSize", "typeface", "Landroid/graphics/Typeface;", "isSquare", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private final String a;
      private final int b;

      public b(String var, int var) {
         kotlin.d.b.l.b(var, "text");
         super();
         this.a = var;
         this.b = var;
      }

      // $FF: synthetic method
      public static Drawable a(a.b var, int var, Typeface var, boolean var, int var, Object var) {
         if((var & 1) != 0) {
            var = n.b(20);
         }

         if((var & 2) != 0) {
            var = Typeface.create("sans-serif-medium", 0);
            kotlin.d.b.l.a(var, "Typeface.create(\"sans-se…medium\", Typeface.NORMAL)");
         }

         if((var & 4) != 0) {
            var = false;
         }

         return var.a(var, var, var);
      }

      public final Drawable a(int var) {
         return a(this, var, (Typeface)null, false, 6, (Object)null);
      }

      public final Drawable a(int var, Typeface var, boolean var) {
         kotlin.d.b.l.b(var, "typeface");
         int var = co.uk.getmondo.common.k.c.a(this.b, 0.2F);
         com.a.a.a.d var = com.a.a.a.a().b().b(var).a(var).a(this.b).a().c();
         com.a.a.a var;
         Drawable var;
         if(var) {
            var = var.a(this.a, var);
            kotlin.d.b.l.a(var, "builder\n                …ct(text, backgroundColor)");
            var = (Drawable)var;
         } else {
            var = var.b(this.a, var);
            kotlin.d.b.l.a(var, "builder\n                …nd(text, backgroundColor)");
            var = (Drawable)var;
         }

         return var;
      }
   }
}
