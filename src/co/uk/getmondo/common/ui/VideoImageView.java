package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.n;
import com.google.android.exoplayer2.t;
import com.google.android.exoplayer2.u;
import com.google.android.exoplayer2.source.q;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.upstream.RawResourceDataSource.RawResourceDataSourceException;

public class VideoImageView extends FrameLayout {
   @BindView(2131821711)
   SimpleExoPlayerView exoPlayerView;
   @BindView(2131821712)
   ImageView placeholderImageView;

   public VideoImageView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public VideoImageView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public VideoImageView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      LayoutInflater.from(this.getContext()).inflate(2131034426, this, true);
      ButterKnife.bind((View)this);
      TypedArray var = this.getContext().getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.VideoImageView, 0, 0);

      int var;
      try {
         var = var.getResourceId(0, 0);
         this.placeholderImageView.setImageResource(var);
         var = var.getResourceId(1, 0);
         this.placeholderImageView.setImageResource(var);
         VideoImageView.a var = VideoImageView.a.a(var.getInt(2, 0));
         this.exoPlayerView.setResizeMode(var.f);
      } finally {
         var.recycle();
      }

      this.a(var);
   }

   // $FF: synthetic method
   static com.google.android.exoplayer2.upstream.c a(RawResourceDataSource var) {
      return var;
   }

   private void a(int var) {
      try {
         Context var = this.getContext();
         com.google.android.exoplayer2.b.b var = new com.google.android.exoplayer2.b.b();
         com.google.android.exoplayer2.c var = new com.google.android.exoplayer2.c();
         t var = com.google.android.exoplayer2.f.a(var, var, var);
         this.exoPlayerView.setPlayer(var);
         RawResourceDataSource var = new RawResourceDataSource(this.getContext());
         com.google.android.exoplayer2.upstream.e var = new com.google.android.exoplayer2.upstream.e(RawResourceDataSource.a(var));
         var.a(var);
         Uri var = var.a();
         com.google.android.exoplayer2.upstream.c.a var = l.a(var);
         com.google.android.exoplayer2.extractor.c var = new com.google.android.exoplayer2.extractor.c();
         com.google.android.exoplayer2.source.f var = new com.google.android.exoplayer2.source.f(var, var, var, (Handler)null, (com.google.android.exoplayer2.source.f.a)null);
         com.google.android.exoplayer2.source.g var = new com.google.android.exoplayer2.source.g(var);
         var.a(var);
         var.a(true);
         com.google.android.exoplayer2.e.a var = new com.google.android.exoplayer2.e.a() {
            public void a() {
            }

            public void a(int var) {
            }

            public void a(ExoPlaybackException var) {
               VideoImageView.this.exoPlayerView.setVisibility(8);
               VideoImageView.this.placeholderImageView.setVisibility(0);
            }

            public void a(n var) {
            }

            public void a(q var, com.google.android.exoplayer2.b.f var) {
            }

            public void a(u var, Object var) {
            }

            public void a(boolean var) {
            }

            public void a(boolean var, int var) {
               if(var == 3) {
                  VideoImageView.this.exoPlayerView.setVisibility(0);
                  VideoImageView.this.placeholderImageView.setVisibility(8);
               }

            }
         };
         var.a(var);
      } catch (RawResourceDataSourceException var) {
         d.a.a.a(var, "Failed to play video", new Object[0]);
      }

   }

   public void a() {
      t var = this.exoPlayerView.getPlayer();
      if(var != null) {
         var.d();
      }

   }

   private static enum a {
      a(0, 3),
      b(1, 0),
      c(2, 2),
      d(3, 1);

      private final int e;
      private final int f;

      private a(int var, int var) {
         this.e = var;
         this.f = var;
      }

      static VideoImageView.a a(int var) {
         VideoImageView.a[] var = values();
         int var = var.length;

         for(int var = 0; var < var; ++var) {
            VideoImageView.a var = var[var];
            if(var.e == var) {
               return var;
            }
         }

         throw new IllegalArgumentException("Unexpected ResizeMode id: " + var);
      }
   }
}
