package co.uk.getmondo.common.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoTextActivity extends co.uk.getmondo.common.activities.b {
   @BindView(2131820811)
   ViewGroup mainContentGroup;

   private void a() {
      for(int var = 0; var < this.mainContentGroup.getChildCount(); ++var) {
         if(this.mainContentGroup.getChildAt(var) instanceof TextView) {
            ((TextView)this.mainContentGroup.getChildAt(var)).setMovementMethod(LinkMovementMethod.getInstance());
         }
      }

   }

   public static void a(Context var, String var, int var) {
      Intent var = new Intent(var, InfoTextActivity.class);
      var.putExtra("EXTRA_TITLE", var);
      var.putExtra("EXTRA_LAYOUT_ID", var);
      var.startActivity(var);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      int var = this.getIntent().getIntExtra("EXTRA_LAYOUT_ID", 2131034217);
      String var = this.getIntent().getStringExtra("EXTRA_TITLE");
      this.setContentView(var);
      ButterKnife.bind((Activity)this);
      this.setTitle(var);
      this.getSupportActionBar().b(true);
      this.a();
   }
}
