package co.uk.getmondo.common.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v7.widget.n;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.InputFilter.LengthFilter;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

public class PinEntryView extends n {
   private final Paint a;
   private final Paint b;
   private final Paint c;
   private final Paint d;
   private final Paint e;
   private final int f;
   private final int g;
   private final int h;
   private final int i;
   private final int j;
   private PinEntryView.a k;
   private int l;
   private boolean m;
   private boolean n;

   public PinEntryView(Context var) {
      this(var, (AttributeSet)null, 2130772212);
   }

   public PinEntryView(Context var, AttributeSet var) {
      this(var, var, 2130772212);
   }

   public PinEntryView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.a = new Paint(1);
      this.b = new Paint(1);
      this.c = new Paint(1);
      this.d = new Paint(1);
      this.e = new Paint(1);
      this.l = 0;
      this.n = false;
      DisplayMetrics var = this.getResources().getDisplayMetrics();
      int var = android.support.v4.content.a.c(var, 2131689582);
      TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.PinEntryView, 0, 0);

      try {
         var = var.getColor(1, var);
         this.n = var.getBoolean(0, false);
      } finally {
         var.recycle();
      }

      this.a.setColor(var);
      this.i = (int)TypedValue.applyDimension(2, 6.0F, var);
      TypedValue var = new TypedValue();
      var.getTheme().resolveAttribute(16843829, var, true);
      if(var.resourceId > 0) {
         var = android.support.v4.content.a.c(var, var.resourceId);
      } else {
         var = var.data;
      }

      this.c.setColor(var);
      this.c.setStrokeWidth(TypedValue.applyDimension(2, 2.0F, var));
      this.d.setColor(var);
      this.d.setStrokeWidth(this.c.getStrokeWidth());
      this.e.setColor(android.support.v4.content.a.c(var, 2131689680));
      this.e.setStrokeWidth(this.c.getStrokeWidth());
      this.b.setColor(-1);
      this.b.setStyle(Style.FILL);
      this.b.setTextSize(48.0F);
      this.b.setTypeface(android.support.v4.content.a.b.a(var, 2130968576));
      this.j = this.getResources().getDimensionPixelOffset(2131427603);
      this.h = this.getResources().getDimensionPixelSize(2131427605);
      this.f = this.getResources().getDimensionPixelSize(2131427368);
      this.g = this.getResources().getDimensionPixelSize(2131427367);
      this.setBackgroundColor(android.support.v4.content.a.c(this.getContext(), 17170445));
      this.setTextColor(android.support.v4.content.a.c(this.getContext(), 17170445));
      this.setCursorVisible(false);
      this.setFilters(new InputFilter[]{new LengthFilter(4)});
      this.setInputType(18);
      this.setImeOptions(268435456);
      this.setOnFocusChangeListener(g.a(this));
      this.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var) {
            if(PinEntryView.this.isEnabled()) {
               PinEntryView.this.l = var.length();
               if(PinEntryView.this.l == 4 && PinEntryView.this.k != null) {
                  PinEntryView.this.k.a(var.toString());
               }

               PinEntryView.this.invalidate();
            }

         }

         public void beforeTextChanged(CharSequence var, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence var, int var, int var, int var) {
         }
      });
   }

   // $FF: synthetic method
   static void a(PinEntryView var, View var, boolean var) {
      var.setSelection(var.getText().length());
   }

   public void a() {
      this.l = 0;
      this.setText("");
   }

   public void a(boolean var) {
      this.l = -1;
      this.m = true;
      this.invalidate();
      if(var) {
         ObjectAnimator.ofFloat(this, "translationX", new float[]{0.0F, 25.0F, -25.0F, 25.0F, -25.0F, 15.0F, -15.0F, 6.0F, -6.0F, 0.0F}).setDuration(300L).start();
      }

   }

   public void b() {
      this.setEnabled(false);
   }

   public void c() {
      this.setEnabled(true);
   }

   public void d() {
      this.l = this.getText().length();
      this.m = false;
      this.invalidate();
   }

   protected void dispatchDraw(Canvas var) {
      super.dispatchDraw(var);

      int var;
      int var;
      for(var = 0; var < this.l; ++var) {
         var = this.f * var + this.h * var + this.i / 2 + this.f / 2;
         int var = this.g / 2 + this.i / 2;
         var.drawCircle((float)var, (float)var, (float)this.i, this.a);
         if(this.n) {
            String var = this.getText().charAt(var) + "";
            float var = this.b.measureText(var);
            var.drawText(var, (float)var - var / 2.0F, (float)(var - this.j), this.b);
         }
      }

      for(var = 0; var < 4; ++var) {
         var = this.f * var + this.h * var;
         Paint var;
         if(var == this.l) {
            var = this.d;
         } else if(this.m) {
            var = this.e;
         } else {
            var = this.c;
         }

         var.drawLine((float)var, (float)this.g, (float)(var + this.f), (float)this.g, var);
      }

   }

   protected void onMeasure(int var, int var) {
      this.setMeasuredDimension(this.f * 4 + this.h * 3 + this.getPaddingLeft() + this.getPaddingRight(), this.g + this.getPaddingTop() + this.getPaddingBottom());
   }

   public void setOnPinEnteredListener(PinEntryView.a var) {
      this.k = var;
   }

   public interface a {
      void a(String var);
   }
}
