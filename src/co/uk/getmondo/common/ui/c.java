package co.uk.getmondo.common.ui;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class c extends com.bumptech.glide.g.b.b {
   public c(ImageView var) {
      super(var);
   }

   protected void a(Bitmap var) {
      android.support.v4.a.a.f var = android.support.v4.a.a.h.a(((ImageView)this.c).getResources(), var);
      var.a(true);
      ((ImageView)this.c).setImageDrawable(var);
   }
}
