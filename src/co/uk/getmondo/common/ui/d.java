package co.uk.getmondo.common.ui;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J(\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/ui/GridSpacingItemDecoration;", "Landroid/support/v7/widget/RecyclerView$ItemDecoration;", "spacing", "", "includeEdge", "", "(IZ)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroid/support/v7/widget/RecyclerView;", "state", "Landroid/support/v7/widget/RecyclerView$State;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends android.support.v7.widget.RecyclerView.g {
   private final int a;
   private final boolean b;

   public d(int var, boolean var) {
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   public d(int var, boolean var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = false;
      }

      this(var, var);
   }

   public void getItemOffsets(Rect var, View var, RecyclerView var, t var) {
      kotlin.d.b.l.b(var, "outRect");
      kotlin.d.b.l.b(var, "view");
      kotlin.d.b.l.b(var, "parent");
      kotlin.d.b.l.b(var, "state");
      int var = var.f(var);
      android.support.v7.widget.RecyclerView.h var = var.getLayoutManager();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.v7.widget.GridLayoutManager");
      } else {
         GridLayoutManager var = (GridLayoutManager)var;
         int var = var.c();
         int var = var.b().a(var);
         if(var >= 0) {
            int var = var % var;
            if(this.b) {
               var.left = this.a - this.a * var / var;
               var.right = (var + 1) * this.a / var;
               if(var < var) {
                  var.top = this.a;
               }

               var.bottom = this.a;
            } else {
               var.left = this.a * var / var;
               if(var < var) {
                  var.right = this.a - (var + 1) * this.a / var;
               }

               if(var >= var) {
                  var.top = this.a;
               }
            }
         } else {
            var.left = 0;
            var.right = 0;
            var.top = 0;
            var.bottom = 0;
         }

      }
   }
}
