package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;

public class h extends android.support.v7.widget.RecyclerView.g {
   private final int a;
   private final Drawable b;
   private final a.a.a.a.a.a c;
   private final int d;
   private final int e;

   public h(Context var, a.a.a.a.a.a var, int var) {
      this.a = var;
      this.b = android.support.v4.content.a.a(var, 2130837967);
      this.d = var.getResources().getDimensionPixelSize(2131427605);
      this.e = var.getResources().getDimensionPixelSize(2131427603);
      this.c = var;
   }

   public void getItemOffsets(Rect var, View var, RecyclerView var, t var) {
      int var = var.f(var);
      if(!var.s() && var != -1) {
         if(var == 0) {
            var = this.d;
         } else if(this.c.a(var) != this.c.a(var - 1)) {
            var = this.e;
         } else {
            var = 0;
         }

         var.set(0, var, 0, 0);
      }

   }

   public void onDrawOver(Canvas var, RecyclerView var, t var) {
      if(!var.s()) {
         int var = var.getPaddingLeft();
         int var = this.a;
         int var = var.getWidth();
         int var = var.getPaddingRight();
         int var = var.getChildCount();

         for(int var = 0; var < var - 1; ++var) {
            View var = var.getChildAt(var);
            int var = var.f(var);
            if(var != -1 && this.c.a(var) == this.c.a(var + 1)) {
               android.support.v7.widget.RecyclerView.i var = (android.support.v7.widget.RecyclerView.i)var.getLayoutParams();
               var = var.getBottom();
               int var = var.bottomMargin + var;
               var = this.b.getIntrinsicHeight();
               this.b.setBounds(var + var, var, var - var, var + var);
               this.b.draw(var);
            }
         }
      }

   }
}
