package co.uk.getmondo.common.ui;

import android.view.View;
import android.view.ViewGroup;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class InfoTextActivity_ViewBinding implements Unbinder {
   private InfoTextActivity a;

   public InfoTextActivity_ViewBinding(InfoTextActivity var, View var) {
      this.a = var;
      var.mainContentGroup = (ViewGroup)Utils.findRequiredViewAsType(var, 2131820811, "field 'mainContentGroup'", ViewGroup.class);
   }

   public void unbind() {
      InfoTextActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.mainContentGroup = null;
      }
   }
}
