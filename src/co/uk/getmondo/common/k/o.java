package co.uk.getmondo.common.k;

import java.util.Iterator;
import kotlin.Metadata;
import kotlin.a.x;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\f\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/common/utils/SortCodeFormatter;", "", "()V", "divider", "", "maxCharacters", "", "addDashes", "", "sortCode", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o {
   public static final o a;

   static {
      new o();
   }

   private o() {
      a = (o)this;
   }

   public static final String a(String var) {
      kotlin.d.b.l.b(var, "sortCode");
      boolean var;
      if(((CharSequence)var).length() == 0) {
         var = true;
      } else {
         var = false;
      }

      if(!var) {
         StringBuilder var = new StringBuilder();
         char[] var = var.toCharArray();
         kotlin.d.b.l.a(var, "(this as java.lang.String).toCharArray()");
         Iterator var = kotlin.a.g.b(var).iterator();

         while(var.hasNext()) {
            x var = (x)var.next();
            int var = var.c();
            var.append(((Character)var.d()).charValue());
            if((var + 1) % 2 == 0 && var.length() < 8) {
               var.append('-');
            }
         }

         if(kotlin.h.j.g((CharSequence)var) == 45) {
            var.setLength(var.length() - 1);
         }

         var = var.toString();
         kotlin.d.b.l.a(var, "sb.toString()");
      }

      return var;
   }
}
