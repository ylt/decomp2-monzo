package co.uk.getmondo.common.k;

import android.content.Context;

public final class m implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!m.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public m(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new m(var);
   }

   public l a() {
      return new l((Context)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
