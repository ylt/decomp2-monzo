package co.uk.getmondo.common.k;

import android.text.Editable;
import android.text.TextWatcher;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\f\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J(\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0016J(\u0010\u0010\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/utils/DateInputFormatter;", "Landroid/text/TextWatcher;", "()V", "after", "", "before", "divider", "", "afterTextChanged", "", "text", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "onTextChanged", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements TextWatcher {
   private final char a = 47;
   private int b;
   private int c;

   public void afterTextChanged(Editable var) {
      boolean var = true;
      kotlin.d.b.l.b(var, "text");
      boolean var;
      if(this.c < this.b) {
         var = true;
      } else {
         var = false;
      }

      if(!var) {
         if(((CharSequence)var).length() == 0) {
            var = true;
         } else {
            var = false;
         }

         if(!var) {
            if(var.charAt(kotlin.h.j.e((CharSequence)var)) == this.a) {
               var = true;
            } else {
               var = false;
            }

            boolean var;
            if(var.length() != 2 && var.length() != 5) {
               var = false;
            } else {
               var = true;
            }

            boolean var = var;
            if(var.length() != 3) {
               if(var.length() == 6) {
                  var = var;
               } else {
                  var = false;
               }
            }

            if(var && var.length() == 2) {
               var.insert(0, (CharSequence)"0");
            } else if(var && var.charAt(2) == this.a && var.length() == 5) {
               var.insert(3, (CharSequence)"0");
            } else if(var) {
               var.append(this.a);
            } else if(var && !var) {
               var.insert(kotlin.h.j.e((CharSequence)var), (CharSequence)("" + this.a));
            } else if(!var && var) {
               var.delete(kotlin.h.j.e((CharSequence)var), kotlin.h.j.e((CharSequence)var) + 1);
            }
         }
      }

   }

   public void beforeTextChanged(CharSequence var, int var, int var, int var) {
      kotlin.d.b.l.b(var, "text");
      this.c = var;
   }

   public void onTextChanged(CharSequence var, int var, int var, int var) {
      kotlin.d.b.l.b(var, "text");
      this.b = var;
   }
}
