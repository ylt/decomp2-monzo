package co.uk.getmondo.common.k;

import android.text.Editable;
import android.text.TextWatcher;

public class i {
   private char a;
   private int b;

   private i(char var) {
      this.a = var;
      this.b = 2;
   }

   public static i a(char var) {
      return new i(var);
   }

   public TextWatcher a() {
      return new TextWatcher() {
         public void afterTextChanged(Editable var) {
            for(int var = 0; var < var.length(); ++var) {
               if(var % (i.this.b + 1) == i.this.b && var.charAt(var) != i.this.a) {
                  var.insert(var, String.valueOf(i.this.a));
               }
            }

         }

         public void beforeTextChanged(CharSequence var, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence var, int var, int var, int var) {
         }
      };
   }

   public i a(int var) {
      this.b = var;
      return this;
   }
}
