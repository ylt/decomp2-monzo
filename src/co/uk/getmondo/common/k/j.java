package co.uk.getmondo.common.k;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import co.uk.getmondo.common.ThirdPartyShareReceiver;

public class j {
   public static Intent a(Context var, String var, String var, co.uk.getmondo.api.model.tracking.a var) {
      Intent var = a(var);
      Intent var;
      if(VERSION.SDK_INT >= 22) {
         Intent var = new Intent(var, ThirdPartyShareReceiver.class);
         var.putExtra("KEY_ANALYTIC_NAME", var.value);
         var = Intent.createChooser(var, var, PendingIntent.getBroadcast(var, 0, var, 134217728).getIntentSender());
      } else {
         var = Intent.createChooser(var, var);
      }

      return var;
   }

   public static Intent a(String var) {
      Intent var = new Intent("android.intent.action.SEND");
      var.setType("text/plain");
      var.putExtra("android.intent.extra.TEXT", var);
      return var;
   }
}
