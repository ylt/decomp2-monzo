package co.uk.getmondo.common.k;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class l {
   private final Context a;

   l(Context var) {
      this.a = var;
   }

   public File a() {
      File var = null;

      File var;
      try {
         var = h.a(this.a);
      } catch (IOException var) {
         d.a.a.a(var, "Failed to get photo file from file", new Object[0]);
         return var;
      }

      var = var;
      return var;
   }

   public InputStream a(String var) {
      Object var = null;

      InputStream var;
      try {
         var = this.a.getContentResolver().openInputStream(Uri.parse(var));
      } catch (FileNotFoundException var) {
         d.a.a.a(var, "Failed to get photo file from input stream", new Object[0]);
         var = (InputStream)var;
      }

      return var;
   }

   public long b(String var) {
      long var;
      try {
         var = h.a(Uri.parse(var), this.a.getContentResolver());
      } catch (IOException var) {
         d.a.a.a(var, "Failed to get photo file size", new Object[0]);
         var = 0L;
      }

      return var;
   }
}
