package co.uk.getmondo.common.k;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u001a\u001c\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0002\u001a\u001a\u0010\u000b\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e\"\u0014\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00040\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"MATRIX", "Ljava/lang/ThreadLocal;", "Landroid/graphics/Matrix;", "RECT_F", "Landroid/graphics/RectF;", "offsetDescendantMatrix", "", "Landroid/view/ViewGroup;", "view", "Landroid/view/View;", "m", "offsetDescendantRect", "child", "rect", "Landroid/graphics/Rect;", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class q {
   private static final ThreadLocal a = new ThreadLocal();
   private static final ThreadLocal b = new ThreadLocal();

   private static final void a(ViewGroup var, View var, Matrix var) {
      ViewParent var = var.getParent();
      if(var instanceof View && var != var) {
         View var = (View)var;
         a(var, var, var);
         var.preTranslate((float)(-var.getScrollX()), (float)(-var.getScrollY()));
      }

      var.preTranslate((float)var.getLeft(), (float)var.getTop());
      if(!var.getMatrix().isIdentity()) {
         var.preConcat(var.getMatrix());
      }

   }

   public static final void a(ViewGroup var, View var, Rect var) {
      kotlin.d.b.l.b(var, "$receiver");
      kotlin.d.b.l.b(var, "child");
      kotlin.d.b.l.b(var, "rect");
      var.set(0, 0, var.getWidth(), var.getHeight());
      Matrix var = (Matrix)a.get();
      if(var == null) {
         var = new Matrix();
         a.set(var);
      } else {
         var.reset();
      }

      a(var, var, var);
      RectF var = (RectF)b.get();
      RectF var = var;
      if(var == null) {
         var = new RectF();
         b.set(var);
      }

      var.set(var);
      var.mapRect(var);
      var.set((int)(var.left + 0.5F), (int)(var.top + 0.5F), (int)(var.right + 0.5F), (int)(var.bottom + 0.5F));
   }
}
