package co.uk.getmondo.common.k;

import android.content.res.Resources;
import android.util.TypedValue;

public class n {
   public static int a(int var) {
      return Math.round(TypedValue.applyDimension(2, (float)var, Resources.getSystem().getDisplayMetrics()));
   }

   public static int b(int var) {
      return Math.round(Resources.getSystem().getDisplayMetrics().density * (float)var);
   }
}
