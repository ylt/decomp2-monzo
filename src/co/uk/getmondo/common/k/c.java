package co.uk.getmondo.common.k;

import android.graphics.Color;

public class c {
   public static int a(int var) {
      float var = 0.0F;
      float[] var = new float[3];
      Color.colorToHSV(var, var);
      float var = var[2] - 0.25F;
      if(var < 0.0F) {
         var = var;
      }

      var[2] = var;
      return Color.HSVToColor(var);
   }

   public static int a(int var, float var) {
      return Color.argb(Math.round((float)Color.alpha(var) * var), Color.red(var), Color.green(var), Color.blue(var));
   }
}
