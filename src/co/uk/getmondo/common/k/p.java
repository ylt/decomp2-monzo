package co.uk.getmondo.common.k;

import android.text.Spannable;
import android.text.style.URLSpan;
import java.util.Iterator;
import java.util.regex.Pattern;

public final class p {
   private static final Pattern a = Pattern.compile("[^\\p{ASCII}]");

   public static Spannable a(Spannable var) {
      URLSpan[] var = (URLSpan[])var.getSpans(0, var.length(), URLSpan.class);
      int var = var.length;

      for(int var = 0; var < var; ++var) {
         URLSpan var = var[var];
         int var = var.getSpanStart(var);
         int var = var.getSpanEnd(var);
         var.removeSpan(var);
         var.setSpan(new co.uk.getmondo.transaction.details.views.a(var.getURL()), var, var, 0);
      }

      return var;
   }

   public static String a(CharSequence var, Iterable var) {
      StringBuilder var = new StringBuilder();
      Iterator var = var.iterator();
      if(var.hasNext()) {
         var.append(var.next());

         while(var.hasNext()) {
            var.append(var);
            var.append(var.next());
         }
      }

      return var.toString();
   }

   public static String a(String var, char var) {
      StringBuilder var = new StringBuilder();
      char[] var = var.toCharArray();

      for(int var = var.length; var > 0; --var) {
         if(var % 3 == 0 && var != var.length) {
            var.append(var).append(var[var.length - var]);
         } else {
            var.append(var[var.length - var]);
         }
      }

      return var.toString();
   }

   public static boolean a(String var) {
      boolean var = true;
      if(var.split(" ").length <= 1) {
         var = false;
      }

      return var;
   }

   public static String b(String var) {
      String var = var;
      if(var.contains(" ")) {
         var = var.split(" ")[0];
      }

      return var;
   }

   public static boolean c(String var) {
      boolean var;
      if(var != null && !var.isEmpty()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public static boolean d(String var) {
      boolean var;
      if(var != null && !var.isEmpty()) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   public static String e(String var) {
      return a(var, ' ');
   }

   public static String f(String var) {
      return a(var, '-');
   }

   public static String g(String var) {
      String var = var;
      if(var != null) {
         if(var.isEmpty()) {
            var = var;
         } else {
            var = a.matcher(var).replaceAll("?");
         }
      }

      return var;
   }

   public static String h(String var) {
      String var = var;
      if(var == null) {
         var = "";
      }

      return var;
   }

   public static String i(String var) {
      if(!var.isEmpty()) {
         var = var.substring(0, 1).toUpperCase() + var.substring(1);
      }

      return var;
   }
}
