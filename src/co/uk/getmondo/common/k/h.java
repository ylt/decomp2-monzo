package co.uk.getmondo.common.k;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Environment;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Deprecated
public class h {
   public static long a(Uri var, ContentResolver var) throws IOException {
      Object var = null;
      Cursor var = var.query(var, (String[])null, (String)null, (String[])null, (String)null, (CancellationSignal)null);
      String var = (String)var;
      if(var != null) {
         var = (String)var;

         try {
            if(var.moveToFirst()) {
               int var = var.getColumnIndex("_size");
               if(var.isNull(var)) {
                  IOException var = new IOException("No file size defined");
                  throw var;
               }

               var = var.getString(var);
            }
         } finally {
            if(var != null) {
               var.close();
            }

         }
      }

      return Long.parseLong(var);
   }

   public static File a(Context var) throws IOException {
      String var = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)).format(new Date());
      return File.createTempFile("JPEG_" + var + "_", ".jpg", var.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
   }
}
