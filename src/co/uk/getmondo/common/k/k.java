package co.uk.getmondo.common.k;

import android.content.Context;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0004"},
   d2 = {"Lco/uk/getmondo/common/utils/PermissionUtils;", "", "()V", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k {
   public static final k.a a = new k.a((kotlin.d.b.i)null);

   public static final boolean a(Context var) {
      kotlin.d.b.l.b(var, "context");
      return a.a(var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/utils/PermissionUtils$Companion;", "", "()V", "hasSmsPermissions", "", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final boolean a(Context var) {
         kotlin.d.b.l.b(var, "context");
         boolean var;
         if(android.support.v4.content.a.b(var, "android.permission.RECEIVE_SMS") == 0 && android.support.v4.content.a.b(var, "android.permission.READ_SMS") == 0) {
            var = true;
         } else {
            var = false;
         }

         return var;
      }
   }
}
