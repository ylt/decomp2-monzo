package co.uk.getmondo.common.address;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class LegacyEnterAddressActivity_ViewBinding implements Unbinder {
   private LegacyEnterAddressActivity a;

   public LegacyEnterAddressActivity_ViewBinding(LegacyEnterAddressActivity var, View var) {
      this.a = var;
      var.postalCodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820897, "field 'postalCodeWrapper'", TextInputLayout.class);
      var.postalCodeView = (EditText)Utils.findRequiredViewAsType(var, 2131820898, "field 'postalCodeView'", EditText.class);
      var.streetAddressView = (EditText)Utils.findRequiredViewAsType(var, 2131820896, "field 'streetAddressView'", EditText.class);
      var.streetAddressWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820895, "field 'streetAddressWrapper'", TextInputLayout.class);
      var.cityWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820899, "field 'cityWrapper'", TextInputLayout.class);
      var.cityView = (EditText)Utils.findRequiredViewAsType(var, 2131820900, "field 'cityView'", EditText.class);
      var.countryView = (EditText)Utils.findRequiredViewAsType(var, 2131820901, "field 'countryView'", EditText.class);
      var.submitButton = (Button)Utils.findRequiredViewAsType(var, 2131820902, "field 'submitButton'", Button.class);
   }

   public void unbind() {
      LegacyEnterAddressActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.postalCodeWrapper = null;
         var.postalCodeView = null;
         var.streetAddressView = null;
         var.streetAddressWrapper = null;
         var.cityWrapper = null;
         var.cityView = null;
         var.countryView = null;
         var.submitButton = null;
      }
   }
}
