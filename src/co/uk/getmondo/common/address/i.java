package co.uk.getmondo.common.address;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class i implements OnEditorActionListener {
   private final SelectAddressActivity a;

   private i(SelectAddressActivity var) {
      this.a = var;
   }

   public static OnEditorActionListener a(SelectAddressActivity var) {
      return new i(var);
   }

   public boolean onEditorAction(TextView var, int var, KeyEvent var) {
      return SelectAddressActivity.a(this.a, var, var, var);
   }
}
