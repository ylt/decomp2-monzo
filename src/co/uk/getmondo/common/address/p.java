package co.uk.getmondo.common.address;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiPostcodeResponse;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.z;
import java.util.List;

public class p extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.d.a.i i = new co.uk.getmondo.d.a.i();
   private boolean j;

   p(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, MonzoApi var, co.uk.getmondo.common.a var, boolean var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.j = var;
   }

   // $FF: synthetic method
   static List a(ApiPostcodeResponse var) throws Exception {
      return var.a();
   }

   // $FF: synthetic method
   static void a(p var, Throwable var) throws Exception {
      var.f.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   // $FF: synthetic method
   static void a(p var, List var) throws Exception {
      if(var.isEmpty()) {
         ((p.a)var.a).a();
         ((p.a)var.a).c();
      } else {
         ((p.a)var.a).b();
         ((p.a)var.a).d();
         ((p.a)var.a).a(var);
      }

   }

   // $FF: synthetic method
   static void a(p var, List var, Throwable var) throws Exception {
      ((p.a)var.a).t();
   }

   // $FF: synthetic method
   static z b(p var, List var) throws Exception {
      io.reactivex.n var = io.reactivex.n.fromIterable(var);
      co.uk.getmondo.d.a.i var = var.i;
      var.getClass();
      return var.map(v.a(var)).toList();
   }

   private void b(String var) {
      ((p.a)this.a).s();
      ((p.a)this.a).g();
      this.a((io.reactivex.b.b)this.g.lookupPostcode(var, co.uk.getmondo.d.i.i().e()).d(q.a()).a(r.a(this)).b(this.d).a(this.c).a(s.a(this)).a(t.a(this), u.a(this)));
   }

   void a() {
      ((p.a)this.a).f();
   }

   public void a(p.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      co.uk.getmondo.common.a var = this.h;
      Impression var;
      if(this.j) {
         var = Impression.q();
      } else {
         var = Impression.F();
      }

      var.a(var);
      if(this.j) {
         String var = this.e.b().d().h().c();
         var.a(var);
         this.b(var);
      } else {
         var.a();
      }

   }

   void a(co.uk.getmondo.d.s var) {
      if(this.j) {
         ak var = this.e.b();
         this.e.a(var.d().a(var));
         ((p.a)this.a).e();
      } else {
         this.h.a(Impression.G());
         ((p.a)this.a).a(var);
      }

   }

   void a(String var) {
      var = var.trim();
      if(!var.isEmpty()) {
         this.b(var);
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.s var);

      void a(String var);

      void a(List var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();

      void s();

      void t();
   }
}
