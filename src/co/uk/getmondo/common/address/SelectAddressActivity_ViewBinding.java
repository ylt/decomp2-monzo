package co.uk.getmondo.common.address;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SelectAddressActivity_ViewBinding implements Unbinder {
   private SelectAddressActivity a;

   public SelectAddressActivity_ViewBinding(SelectAddressActivity var, View var) {
      this.a = var;
      var.postalCodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820869, "field 'postalCodeWrapper'", TextInputLayout.class);
      var.selectAddressLabel = (TextView)Utils.findRequiredViewAsType(var, 2131821053, "field 'selectAddressLabel'", TextView.class);
      var.postalCode = (EditText)Utils.findRequiredViewAsType(var, 2131820870, "field 'postalCode'", EditText.class);
      var.addressesRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131821054, "field 'addressesRecyclerView'", RecyclerView.class);
   }

   public void unbind() {
      SelectAddressActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.postalCodeWrapper = null;
         var.selectAddressLabel = null;
         var.postalCode = null;
         var.addressesRecyclerView = null;
      }
   }
}
