package co.uk.getmondo.common.address;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final AddressAdapter.EnterAddressViewHolder a;

   private b(AddressAdapter.EnterAddressViewHolder var) {
      this.a = var;
   }

   public static OnClickListener a(AddressAdapter.EnterAddressViewHolder var) {
      return new b(var);
   }

   public void onClick(View var) {
      AddressAdapter.EnterAddressViewHolder.a(this.a, var);
   }
}
