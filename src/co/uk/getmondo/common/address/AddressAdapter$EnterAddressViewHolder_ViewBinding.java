package co.uk.getmondo.common.address;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AddressAdapter$EnterAddressViewHolder_ViewBinding implements Unbinder {
   private AddressAdapter.EnterAddressViewHolder a;

   public AddressAdapter$EnterAddressViewHolder_ViewBinding(AddressAdapter.EnterAddressViewHolder var, View var) {
      this.a = var;
      var.addressNotInListText = (TextView)Utils.findRequiredViewAsType(var, 2131821585, "field 'addressNotInListText'", TextView.class);
   }

   public void unbind() {
      AddressAdapter.EnterAddressViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.addressNotInListText = null;
      }
   }
}
