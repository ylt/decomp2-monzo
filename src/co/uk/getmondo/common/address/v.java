package co.uk.getmondo.common.address;

import co.uk.getmondo.api.model.LegacyApiAddress;

// $FF: synthetic class
final class v implements io.reactivex.c.h {
   private final co.uk.getmondo.d.a.i a;

   private v(co.uk.getmondo.d.a.i var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(co.uk.getmondo.d.a.i var) {
      return new v(var);
   }

   public Object a(Object var) {
      return this.a.a((LegacyApiAddress)var);
   }
}
