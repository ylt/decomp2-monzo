package co.uk.getmondo.common.address;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.phone_number.EnterPhoneNumberActivity;
import java.util.List;

@Deprecated
public class SelectAddressActivity extends co.uk.getmondo.common.activities.b implements p.a {
   p a;
   @BindView(2131821054)
   RecyclerView addressesRecyclerView;
   private AddressAdapter b;
   private co.uk.getmondo.common.activities.b.a c;
   @BindView(2131820870)
   EditText postalCode;
   @BindView(2131820869)
   TextInputLayout postalCodeWrapper;
   @BindView(2131821053)
   TextView selectAddressLabel;

   public static co.uk.getmondo.d.s a(Intent var) {
      return (co.uk.getmondo.d.s)var.getParcelableExtra("RESULT_EXTRA_ADDRESS");
   }

   public static void a(Context var, co.uk.getmondo.common.activities.b.a var, boolean var) {
      var.startActivity(b(var, var, var));
   }

   // $FF: synthetic method
   static boolean a(SelectAddressActivity var, TextView var, int var, KeyEvent var) {
      boolean var;
      if(var == 3) {
         var.a((View)var);
         var.clearFocus();
         var.a.a(var.postalCode.getText().toString());
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public static Intent b(Context var, co.uk.getmondo.common.activities.b.a var, boolean var) {
      Intent var = new Intent(var, SelectAddressActivity.class);
      var.putExtra("EXTRA_THEME", var);
      var.putExtra("EXTRA_IS_SIGN_UP_FLOW", var);
      return var;
   }

   public void a() {
      this.selectAddressLabel.setVisibility(4);
   }

   public void a(co.uk.getmondo.d.s var) {
      Intent var = new Intent();
      var.putExtra("RESULT_EXTRA_ADDRESS", var);
      this.setResult(-1, var);
      this.finish();
   }

   public void a(String var) {
      this.postalCode.setText(var);
   }

   public void a(List var) {
      this.selectAddressLabel.setVisibility(0);
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(var);
      this.addressesRecyclerView.getLayoutManager().e(0);
   }

   public void b() {
      this.selectAddressLabel.setVisibility(0);
   }

   public void c() {
      this.postalCodeWrapper.setErrorEnabled(true);
      this.postalCodeWrapper.setError(this.getString(2131362495));
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(this.getString(2131362169));
   }

   public void d() {
      this.postalCodeWrapper.setErrorEnabled(false);
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(this.getString(2131361998));
   }

   public void e() {
      EnterPhoneNumberActivity.a((Context)this);
   }

   public void f() {
      this.startActivityForResult(LegacyEnterAddressActivity.a(this, this.getString(2131362494), this.c), 1);
   }

   public void g() {
      this.b.a();
   }

   protected void onActivityResult(int var, int var, Intent var) {
      super.onActivityResult(var, var, var);
      if(var == 1 && var == -1) {
         co.uk.getmondo.d.s var = LegacyEnterAddressActivity.a(var);
         this.a.a(var);
      }

   }

   protected void onCreate(Bundle var) {
      this.c = (co.uk.getmondo.common.activities.b.a)this.getIntent().getSerializableExtra("EXTRA_THEME");
      if(this.c != null) {
         int var;
         if(this.c == co.uk.getmondo.common.activities.b.a.b) {
            var = 2131493160;
         } else {
            var = 2131493157;
         }

         this.setTheme(var);
      }

      super.onCreate(var);
      this.setContentView(2131034204);
      ButterKnife.bind((Activity)this);
      boolean var = this.getIntent().getBooleanExtra("EXTRA_IS_SIGN_UP_FLOW", false);
      this.l().a(new n(var)).a(this);
      if(!var) {
         this.getWindow().setSoftInputMode(4);
         this.postalCode.requestFocus();
      }

      this.postalCode.setOnEditorActionListener(i.a(this));
      p var = this.a;
      var.getClass();
      AddressAdapter.a var = j.a(var);
      var = this.a;
      var.getClass();
      this.b = new AddressAdapter(var, k.a(var), this.getString(2131361998));
      this.addressesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
      this.addressesRecyclerView.setAdapter(this.b);
      this.a.a((p.a)this);
      this.setTitle(this.getString(2131362775));
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
