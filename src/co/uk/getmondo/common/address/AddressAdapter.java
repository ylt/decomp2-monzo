package co.uk.getmondo.common.address;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class AddressAdapter extends android.support.v7.widget.RecyclerView.a {
   private final AddressAdapter.a a;
   private final AddressAdapter.b b;
   private final List c = new ArrayList();
   private String d;

   public AddressAdapter(AddressAdapter.a var, AddressAdapter.b var, String var) {
      this.a = var;
      this.b = var;
      this.d = var;
   }

   public void a() {
      int var = this.c.size();
      this.c.clear();
      this.notifyItemRangeRemoved(0, var);
   }

   public void a(String var) {
      this.d = var;
      this.notifyItemChanged(this.getItemCount() - 1);
   }

   public void a(List var) {
      int var = this.c.size();
      this.c.clear();
      this.notifyItemRangeRemoved(0, var);
      this.c.addAll(var);
      this.notifyItemRangeInserted(0, var.size());
   }

   public int getItemCount() {
      return this.c.size() + 1;
   }

   public int getItemViewType(int var) {
      if(var == this.c.size()) {
         var = 1;
      } else if(var < this.c.size()) {
         var = 0;
      } else {
         var = super.getItemViewType(var);
      }

      return var;
   }

   public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var, int var) {
      if(var instanceof AddressAdapter.AddressViewHolder) {
         ((AddressAdapter.AddressViewHolder)var).a((co.uk.getmondo.d.s)this.c.get(var));
      } else {
         if(!(var instanceof AddressAdapter.EnterAddressViewHolder)) {
            throw new RuntimeException("Unknown view holder type");
         }

         ((AddressAdapter.EnterAddressViewHolder)var).a(this.d);
      }

   }

   public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var, int var) {
      Object var;
      switch(var) {
      case 0:
         var = new AddressAdapter.AddressViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034360, var, false), this.a);
         break;
      case 1:
         var = new AddressAdapter.EnterAddressViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034361, var, false), this.b);
         break;
      default:
         throw new RuntimeException("Unknown view type");
      }

      return (android.support.v7.widget.RecyclerView.w)var;
   }

   class AddressViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821583)
      TextView addressLine1;
      @BindView(2131821584)
      TextView addressLine2;
      private final AddressAdapter.a b;

      public AddressViewHolder(View var, AddressAdapter.a var) {
         super(var);
         ButterKnife.bind(this, (View)var);
         this.b = var;
      }

      // $FF: synthetic method
      static void a(AddressAdapter.AddressViewHolder var, co.uk.getmondo.d.s var, View var) {
         if(var.b != null) {
            var.b.a(var);
         }

      }

      public void a(co.uk.getmondo.d.s var) {
         String var = co.uk.getmondo.common.k.a.a(var.h());
         this.addressLine1.setText(var);
         this.addressLine2.setText(this.itemView.getResources().getString(2131361995, new Object[]{var.c(), var.i()}));
         this.itemView.setOnClickListener(a.a(this, var));
      }
   }

   class EnterAddressViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821585)
      TextView addressNotInListText;
      private final AddressAdapter.b b;

      public EnterAddressViewHolder(View var, AddressAdapter.b var) {
         super(var);
         this.b = var;
         ButterKnife.bind(this, (View)var);
      }

      // $FF: synthetic method
      static void a(AddressAdapter.EnterAddressViewHolder var, View var) {
         if(var.b != null) {
            var.b.a();
         }

      }

      public void a(String var) {
         this.addressNotInListText.setText(var);
         this.itemView.setOnClickListener(b.a(this));
      }
   }

   public interface a {
      void a(co.uk.getmondo.d.s var);
   }

   public interface b {
      void a();
   }
}
