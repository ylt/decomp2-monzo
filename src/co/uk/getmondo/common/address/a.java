package co.uk.getmondo.common.address;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final AddressAdapter.AddressViewHolder a;
   private final co.uk.getmondo.d.s b;

   private a(AddressAdapter.AddressViewHolder var, co.uk.getmondo.d.s var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(AddressAdapter.AddressViewHolder var, co.uk.getmondo.d.s var) {
      return new a(var, var);
   }

   public void onClick(View var) {
      AddressAdapter.AddressViewHolder.a(this.a, this.b, var);
   }
}
