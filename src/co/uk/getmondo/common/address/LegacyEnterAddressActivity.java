package co.uk.getmondo.common.address;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;

@Deprecated
public class LegacyEnterAddressActivity extends co.uk.getmondo.common.activities.b implements e.a {
   e a;
   @BindView(2131820900)
   EditText cityView;
   @BindView(2131820899)
   TextInputLayout cityWrapper;
   @BindView(2131820901)
   EditText countryView;
   @BindView(2131820898)
   EditText postalCodeView;
   @BindView(2131820897)
   TextInputLayout postalCodeWrapper;
   @BindView(2131820896)
   EditText streetAddressView;
   @BindView(2131820895)
   TextInputLayout streetAddressWrapper;
   @BindView(2131820902)
   Button submitButton;

   public static Intent a(Context var, String var, co.uk.getmondo.common.activities.b.a var) {
      Intent var = new Intent(var, LegacyEnterAddressActivity.class);
      var.putExtra("EXTRA_SUBMIT_TEXT", var);
      var.putExtra("EXTRA_THEME", var);
      return var;
   }

   public static Intent a(Context var, String var, co.uk.getmondo.common.activities.b.a var, co.uk.getmondo.d.s var) {
      Intent var = a(var, var, var);
      if(var != null) {
         var.putExtra("EXTRA_ADDRESS", var);
      }

      return var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.common.address.a.a.a a(LegacyEnterAddressActivity var, Object var) throws Exception {
      return new co.uk.getmondo.common.address.a.a.a(var.streetAddressView.getText().toString(), var.postalCodeView.getText().toString(), var.cityView.getText().toString());
   }

   public static co.uk.getmondo.d.s a(Intent var) {
      return (co.uk.getmondo.d.s)var.getParcelableExtra("EXTRA_ADDRESS");
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.submitButton).map(c.a(this));
   }

   public void a(co.uk.getmondo.d.s var) {
      Intent var = new Intent();
      var.putExtra("EXTRA_ADDRESS", var);
      this.setResult(-1, var);
      this.finish();
   }

   public void a(String var) {
      this.streetAddressView.setText(var);
   }

   public void b() {
      this.streetAddressWrapper.setErrorEnabled(true);
      this.streetAddressWrapper.setError(this.getString(2131362578));
   }

   public void c() {
      this.postalCodeWrapper.setErrorEnabled(true);
      this.postalCodeWrapper.setError(this.getString(2131362577));
   }

   public void d() {
      this.cityWrapper.setErrorEnabled(true);
      this.cityWrapper.setError(this.getString(2131362575));
   }

   public void d(String var) {
      this.postalCodeView.setText(var);
   }

   public void e() {
      this.streetAddressWrapper.setErrorEnabled(false);
      this.postalCodeWrapper.setErrorEnabled(false);
      this.cityWrapper.setErrorEnabled(false);
   }

   public void e(String var) {
      this.cityView.setText(var);
   }

   public void f(String var) {
      this.countryView.setText(var);
   }

   protected void onCreate(Bundle var) {
      co.uk.getmondo.common.activities.b.a var = (co.uk.getmondo.common.activities.b.a)this.getIntent().getSerializableExtra("EXTRA_THEME");
      if(var != null) {
         int var;
         if(var == co.uk.getmondo.common.activities.b.a.b) {
            var = 2131493160;
         } else {
            var = 2131493157;
         }

         this.setTheme(var);
      }

      super.onCreate(var);
      this.setContentView(2131034166);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      String var = this.getIntent().getStringExtra("EXTRA_SUBMIT_TEXT");
      this.submitButton.setText(var);
      this.getSupportActionBar().b(true);
      this.a.a((co.uk.getmondo.d.s)this.getIntent().getParcelableExtra("EXTRA_ADDRESS"));
      this.a.a((e.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
