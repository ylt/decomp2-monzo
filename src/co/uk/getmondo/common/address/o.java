package co.uk.getmondo.common.address;

public final class o implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final n b;

   static {
      boolean var;
      if(!o.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public o(n var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(n var) {
      return new o(var);
   }

   public Boolean a() {
      return (Boolean)b.a.d.a(Boolean.valueOf(this.b.a()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
