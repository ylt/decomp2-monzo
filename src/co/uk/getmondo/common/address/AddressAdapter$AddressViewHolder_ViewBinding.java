package co.uk.getmondo.common.address;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AddressAdapter$AddressViewHolder_ViewBinding implements Unbinder {
   private AddressAdapter.AddressViewHolder a;

   public AddressAdapter$AddressViewHolder_ViewBinding(AddressAdapter.AddressViewHolder var, View var) {
      this.a = var;
      var.addressLine1 = (TextView)Utils.findRequiredViewAsType(var, 2131821583, "field 'addressLine1'", TextView.class);
      var.addressLine2 = (TextView)Utils.findRequiredViewAsType(var, 2131821584, "field 'addressLine2'", TextView.class);
   }

   public void unbind() {
      AddressAdapter.AddressViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.addressLine1 = null;
         var.addressLine2 = null;
      }
   }
}
