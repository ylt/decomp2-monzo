package co.uk.getmondo.common;

import android.app.Application;

public final class n implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!n.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public n(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new n(var);
   }

   public m a() {
      return new m((Application)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
