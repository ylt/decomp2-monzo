package co.uk.getmondo.common.j;

import io.reactivex.h;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.v;
import io.reactivex.z;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a@\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00030\u00012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0001\u001a\u0015\u0010\u0007\u001a\u00020\b*\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0086\u0002\u001a#\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00030\f\"\u0004\b\u0000\u0010\u0003*\u00020\r2\u0006\u0010\u000e\u001a\u0002H\u0003¢\u0006\u0002\u0010\u000f\u001a\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u0002H\u00030\f\"\u0004\b\u0000\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0011\u001a\"\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00030\u00130\u0011\"\u0004\b\u0000\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\f\u001a<\u0010\u0014\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00030\u00012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0016\u001a<\u0010\u0017\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0011\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00030\u00112\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0018¨\u0006\u0019"},
   d2 = {"combineLatest", "Lio/reactivex/Observable;", "Lkotlin/Pair;", "T", "U", "source1", "source2", "plus", "Lio/reactivex/disposables/CompositeDisposable;", "disposable", "Lio/reactivex/disposables/Disposable;", "toMaybeDefaultOnErrorComplete", "Lio/reactivex/Maybe;", "Lio/reactivex/Completable;", "completionValue", "(Lio/reactivex/Completable;Ljava/lang/Object;)Lio/reactivex/Maybe;", "toMaybeOnErrorComplete", "Lio/reactivex/Single;", "toOptionalSingle", "Lcom/memoizrlabs/poweroptional/Optional;", "withLatestFrom", "other", "Lio/reactivex/ObservableSource;", "zipWith", "Lio/reactivex/SingleSource;", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class f {
   public static final io.reactivex.b.a a(io.reactivex.b.a var, io.reactivex.b.b var) {
      l.b(var, "$receiver");
      l.b(var, "disposable");
      var.a(var);
      return var;
   }

   public static final h a(io.reactivex.b var, Object var) {
      l.b(var, "$receiver");
      h var = var.a(var).e().f();
      l.a(var, "toSingleDefault(completi…Maybe().onErrorComplete()");
      return var;
   }

   public static final h a(v var) {
      l.b(var, "$receiver");
      h var = var.e().f();
      l.a(var, "toMaybe().onErrorComplete()");
      return var;
   }

   public static final n a(n var, n var) {
      l.b(var, "source1");
      l.b(var, "source2");
      var = n.combineLatest((r)var, (r)var, (io.reactivex.c.c)null.a);
      l.a(var, "Observable.combineLatest…> { t, u -> Pair(t, u) })");
      return var;
   }

   public static final n a(n var, r var) {
      l.b(var, "$receiver");
      l.b(var, "other");
      var = var.withLatestFrom(var, (io.reactivex.c.c)null.a);
      l.a(var, "withLatestFrom(other, Bi…n { t, u -> Pair(t, u) })");
      return var;
   }

   public static final v a(v var, z var) {
      l.b(var, "$receiver");
      l.b(var, "other");
      var = var.a(var, (io.reactivex.c.c)null.a);
      l.a(var, "zipWith(other, BiFunction { t, u -> Pair(t, u) })");
      return var;
   }
}
