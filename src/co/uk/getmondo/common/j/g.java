package co.uk.getmondo.common.j;

import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import io.realm.al;
import io.realm.am;
import io.realm.av;
import io.realm.bb;
import io.realm.bg;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J8\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\n0\t0\b\"\b\b\u0000\u0010\n*\u00020\u000b2\u0018\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\n0\u000f0\rH\u0007J\u001a\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/rx/RxRealm;", "", "()V", "asCompletable", "Lio/reactivex/Completable;", "realmTransaction", "Lio/realm/Realm$Transaction;", "asObservable", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/rx/RxRealm$Result;", "T", "Lio/realm/RealmModel;", "query", "Lkotlin/Function1;", "Lio/realm/Realm;", "Lio/realm/RealmResults;", "calculateDiff", "", "Lco/uk/getmondo/common/data/QueryResults$Change;", "realmChangeSet", "Lio/realm/OrderedCollectionChangeSet;", "Result", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   public static final g a;

   static {
      new g();
   }

   private g() {
      a = (g)this;
   }

   public static final io.reactivex.b a(final av.a var) {
      l.b(var, "realmTransaction");
      io.reactivex.b var = io.reactivex.b.a((io.reactivex.c.a)(new io.reactivex.c.a() {
         public final void a() {
            // $FF: Couldn't be decompiled
         }
      }));
      l.a(var, "Completable.fromAction {…)\n            }\n        }");
      return var;
   }

   public static final n a(final kotlin.d.a.b var) {
      l.b(var, "query");
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            l.b(var, "emitter");
            final av var = av.n();
            final am var = (am)(new am() {
               public final void a(bg varx, al varx) {
                  if(varx.c() && varx.a()) {
                     o var = var;
                     av var = var;
                     l.a(var, "realm");
                     l.a(varx, "realmResults");
                     var.a(new g.a(var, new co.uk.getmondo.common.b.b((List)varx, g.a.a(varx))));
                  }

               }
            });
            kotlin.d.a.b var = var;
            l.a(var, "realm");
            final bg var = (bg)var.a(var);
            var.a(var);
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var.b(var);
                  var.close();
               }
            }));
            if(var.c() && var.a()) {
               var.a(new g.a(var, new co.uk.getmondo.common.b.b((List)var, (List)null, 2, (i)null)));
            }

         }
      }));
      l.a(var, "Observable.create<Result…)\n            }\n        }");
      return var;
   }

   private final List a(al var) {
      byte var = 0;
      List var;
      if(var == null) {
         var = null;
      } else {
         List var = (List)(new ArrayList());
         al.a[] var = var.a();
         if(var != null) {
            List var = kotlin.a.g.f((Object[])var);
            if(var != null) {
               Iterator var = ((Iterable)var).iterator();

               while(var.hasNext()) {
                  al.a var = (al.a)var.next();
                  var.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.b, var.a, var.b));
               }
            }
         }

         var = var.b();
         int var;
         Object[] var;
         if(var != null) {
            var = (Object[])var;

            for(var = 0; var < var.length; ++var) {
               al.a var = (al.a)var[var];
               var.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.a, var.a, var.b));
            }
         }

         var = var.c();
         var = var;
         if(var != null) {
            var = (Object[])var;
            var = var;

            while(true) {
               var = var;
               if(var >= var.length) {
                  break;
               }

               al.a var = (al.a)var[var];
               var.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.c, var.a, var.b));
               ++var;
            }
         }
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0003B\u001b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0005HÂ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007HÆ\u0003J)\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007HÆ\u0001J\r\u0010\u0012\u001a\u0004\u0018\u00018\u0000¢\u0006\u0002\u0010\u0013J\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015J\u0013\u0010\u0016\u001a\u00020\n2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\t\u0010\u000bR\u0011\u0010\f\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\f\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/common/rx/RxRealm$Result;", "T", "Lio/realm/RealmModel;", "", "realm", "Lio/realm/Realm;", "queryResults", "Lco/uk/getmondo/common/data/QueryResults;", "(Lio/realm/Realm;Lco/uk/getmondo/common/data/QueryResults;)V", "isEmpty", "", "()Z", "isNotEmpty", "getQueryResults", "()Lco/uk/getmondo/common/data/QueryResults;", "component1", "component2", "copy", "copyFirstFromRealm", "()Lio/realm/RealmModel;", "copyFromRealm", "", "equals", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private final av a;
      private final co.uk.getmondo.common.b.b b;

      public a(av var, co.uk.getmondo.common.b.b var) {
         l.b(var, "realm");
         l.b(var, "queryResults");
         super();
         this.a = var;
         this.b = var;
      }

      public final boolean a() {
         boolean var;
         if(!((Collection)this.b.a()).isEmpty()) {
            var = true;
         } else {
            var = false;
         }

         return var;
      }

      public final boolean b() {
         return this.b.a().isEmpty();
      }

      public final List c() {
         List var = this.a.a((Iterable)this.b.a());
         l.a(var, "realm.copyFromRealm(queryResults.results)");
         return var;
      }

      public final bb d() {
         bb var;
         if(this.b.a().isEmpty()) {
            var = null;
         } else {
            var = this.a.e((bb)this.b.a().get(0));
         }

         return var;
      }

      public final co.uk.getmondo.common.b.b e() {
         return this.b;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label28: {
               if(var instanceof g.a) {
                  g.a var = (g.a)var;
                  if(l.a(this.a, var.a) && l.a(this.b, var.b)) {
                     break label28;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         av var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         co.uk.getmondo.common.b.b var = this.b;
         if(var != null) {
            var = var.hashCode();
         }

         return var * 31 + var;
      }

      public String toString() {
         return "Result(realm=" + this.a + ", queryResults=" + this.b + ")";
      }
   }
}
