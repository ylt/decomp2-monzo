package co.uk.getmondo.common.j;

import android.content.Context;
import android.content.IntentFilter;
import io.reactivex.o;
import io.reactivex.p;

// $FF: synthetic class
final class c implements p {
   private final Context a;
   private final IntentFilter b;

   private c(Context var, IntentFilter var) {
      this.a = var;
      this.b = var;
   }

   public static p a(Context var, IntentFilter var) {
      return new c(var, var);
   }

   public void a(o var) {
      b.a(this.a, this.b, var);
   }
}
