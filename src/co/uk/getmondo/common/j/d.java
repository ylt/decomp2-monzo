package co.uk.getmondo.common.j;

import android.content.BroadcastReceiver;
import android.content.Context;

// $FF: synthetic class
final class d implements io.reactivex.c.f {
   private final Context a;
   private final BroadcastReceiver b;

   private d(Context var, BroadcastReceiver var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.f a(Context var, BroadcastReceiver var) {
      return new d(var, var);
   }

   public void a() {
      b.a(this.a, this.b);
   }
}
