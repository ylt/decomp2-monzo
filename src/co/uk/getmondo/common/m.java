package co.uk.getmondo.common;

import android.app.Application;
import android.os.Bundle;
import com.facebook.appevents.AppEventsLogger;

public class m {
   private final Application a;
   private AppEventsLogger b;

   m(Application var) {
      this.a = var;
   }

   private AppEventsLogger c() {
      if(this.b == null) {
         this.b = AppEventsLogger.newLogger(this.a);
      }

      return this.b;
   }

   public void a() {
      Bundle var = new Bundle();
      var.putString("fb_registration_method", "Email");
      this.c().logEvent("fb_mobile_complete_registration", var);
   }

   public void b() {
      Bundle var = new Bundle();
      var.putInt("fb_success", 1);
      this.c().logEvent("fb_mobile_add_payment_info", var);
   }
}
