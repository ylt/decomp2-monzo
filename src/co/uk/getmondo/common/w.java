package co.uk.getmondo.common;

import android.content.res.Resources;

public final class w implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!w.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public w(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new w(var);
   }

   public v a() {
      return new v((Resources)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
