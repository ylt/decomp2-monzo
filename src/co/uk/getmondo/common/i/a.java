package co.uk.getmondo.common.i;

public class a {
   private final c a;
   private long b;
   private int c;

   public a(c var) {
      this.a = var;
      this.b = 0L;
      this.c = 0;
   }

   public co.uk.getmondo.d.c a() {
      return new co.uk.getmondo.d.c(this.b, this.a);
   }

   public void a(co.uk.getmondo.d.c var) {
      if(var != null) {
         if(!this.a.b().equals(var.l().b())) {
            throw new IllegalArgumentException("This amount aggregator only supports amounts with currency " + this.a.b());
         }

         this.b += var.k();
         ++this.c;
      }

   }

   public int b() {
      return this.c;
   }

   public void c() {
      this.b = 0L;
      this.c = 0;
   }
}
