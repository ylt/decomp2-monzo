package co.uk.getmondo.common.i;

import java.util.Locale;

public class d {
   public static int a(long var, int var) {
      if(var <= 0) {
         var = 0;
      } else {
         var = (int)Math.abs(var) % (int)Math.pow(10.0D, (double)var);
      }

      return var;
   }

   public static String a(int var, int var) {
      String var;
      if(var <= 0) {
         var = "";
      } else {
         var = String.format(Locale.UK, ".%0" + var + "d", new Object[]{Integer.valueOf(var)});
      }

      return var;
   }

   public static long b(long var, int var) {
      if(var <= 0) {
         var = Math.abs(var);
      } else {
         var = Math.abs(var) / (long)((int)Math.pow(10.0D, (double)var));
      }

      return var;
   }

   public static double c(long var, int var) {
      double var;
      if(var <= 0) {
         var = (double)var;
      } else {
         var = (double)var / Math.pow(10.0D, (double)var);
      }

      return var;
   }

   public static long d(long var, int var) {
      long var;
      if(var != 0L && var > 0) {
         var = Math.abs(var);
         var = (int)Math.pow(10.0D, (double)var);
         int var = (int)var % var;
         long var;
         if(var <= var / 2) {
            var = var - (long)var;
         } else {
            var = var - (long)var + (long)var;
         }

         var = var;
         if(var < 0L) {
            var = var * -1L;
         }
      } else {
         var = var;
      }

      return var;
   }

   public static long e(long var, int var) {
      long var;
      if(var != 0L && var > 0) {
         var = Math.abs(var);
         var = (int)Math.pow(10.0D, (double)var);
         int var = (int)var % var;
         long var = var;
         if(var > 0) {
            var = var - (long)var + (long)var;
         }

         var = var;
         if(var < 0L) {
            var = var * -1L;
         }
      } else {
         var = var;
      }

      return var;
   }
}
