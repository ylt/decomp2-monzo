package co.uk.getmondo.common.i;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Locale;
import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;

public class c implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public c a(Parcel var) {
         return new c(var);
      }

      public c[] a(int var) {
         return new c[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return this.a(var);
      }
   };
   public static final c a = new c("GBP");
   private CurrencyUnit b;
   private String c;

   protected c(Parcel var) {
      this.b = (CurrencyUnit)var.readSerializable();
      this.c = var.readString();
   }

   public c(String var) {
      Object var;
      try {
         this.b = CurrencyUnit.of(var);
         return;
      } catch (IllegalCurrencyException var) {
         var = var;
      } catch (NullPointerException var) {
         var = var;
      }

      this.b = CurrencyUnit.GBP;
      String var = var;
      if(var == null) {
         var = "";
      }

      this.c = var;
      d.a.a.a((Throwable)(new IllegalArgumentException("Invalid currency code", (Throwable)var)));
   }

   public int a() {
      return this.b.getDecimalPlaces();
   }

   public String a(Locale var) {
      String var;
      if(this.c != null) {
         var = this.c;
      } else {
         var = this.b.getSymbol(var);
      }

      return var;
   }

   public String b() {
      String var;
      if(this.c != null) {
         var = this.c;
      } else {
         var = this.b.getCurrencyCode();
      }

      return var;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var) {
      boolean var = true;
      if(this != var) {
         if(var != null && this.getClass() == var.getClass()) {
            c var;
            label35: {
               var = (c)var;
               if(this.b != null) {
                  if(this.b.equals(var.b)) {
                     break label35;
                  }
               } else if(var.b == null) {
                  break label35;
               }

               var = false;
               return var;
            }

            if(this.c != null) {
               var = this.c.equals(var.c);
            } else if(var.c != null) {
               var = false;
            }
         } else {
            var = false;
         }
      }

      return var;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.b != null) {
         var = this.b.hashCode();
      } else {
         var = 0;
      }

      if(this.c != null) {
         var = this.c.hashCode();
      }

      return var * 31 + var;
   }

   public void writeToParcel(Parcel var, int var) {
      var.writeSerializable(this.b);
      var.writeString(this.c);
   }
}
