package co.uk.getmondo.common.i;

import java.text.NumberFormat;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB%\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/common/money/AmountFormatter;", "", "alwaysPrintFractionalPart", "", "showNegative", "hideCurrency", "(ZZZ)V", "getAlwaysPrintFractionalPart", "()Z", "getHideCurrency", "getShowNegative", "format", "", "amount", "Lco/uk/getmondo/model/Amount;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   public static final b.a a = new b.a((i)null);
   private final boolean b;
   private final boolean c;
   private final boolean d;

   public b() {
      this(false, false, false, 7, (i)null);
   }

   public b(boolean var, boolean var, boolean var) {
      this.b = var;
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   public b(boolean var, boolean var, boolean var, int var, i var) {
      if((var & 1) != 0) {
         var = false;
      }

      if((var & 2) != 0) {
         var = false;
      }

      if((var & 4) != 0) {
         var = false;
      }

      this(var, var, var);
   }

   public final String a(co.uk.getmondo.d.c var) {
      l.b(var, "amount");
      String var;
      if(this.d) {
         var = "";
      } else {
         var = var.g();
      }

      String var;
      if(this.c && var.a()) {
         var = "-";
      } else {
         var = "";
      }

      String var = a.a(var.c());
      int var = var.d();
      int var = var.l().a();
      boolean var;
      if(!this.b && var <= 0) {
         var = false;
      } else {
         var = true;
      }

      String var;
      if(var) {
         var = d.a(var, var);
      } else {
         var = "";
      }

      return var + var + var + var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/money/AmountFormatter$Companion;", "", "()V", "formatIntegerPart", "", "absoluteIntegerPart", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final String a(long var) {
         String var = NumberFormat.getNumberInstance(Locale.getDefault()).format(var);
         l.a(var, "NumberFormat.getNumberIn…rmat(absoluteIntegerPart)");
         return var;
      }
   }
}
