package co.uk.getmondo.common;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;

public class ThirdPartyShareReceiver extends BroadcastReceiver {
   a a;

   @TargetApi(22)
   public void onReceive(Context var, Intent var) {
      MonzoApplication.a(var).b().a(this);
      ComponentName var = (ComponentName)var.getParcelableExtra("android.intent.extra.CHOSEN_COMPONENT");
      String var = var.getStringExtra("KEY_ANALYTIC_NAME");
      String var = var.getClassName();
      this.a.a(Impression.b(var, var));
   }
}
