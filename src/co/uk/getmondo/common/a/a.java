package co.uk.getmondo.common.a;

import android.accounts.Account;
import android.accounts.OnAccountsUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.pm.ShortcutInfo.Builder;
import android.graphics.drawable.Icon;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.accounts.k;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.h;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;
import io.reactivex.c.f;
import io.reactivex.c.g;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00172\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u0015H\u0016J\u0018\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/common/app_shortcuts/Api25AppShortcuts;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;", "context", "Landroid/content/Context;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "androidAccountManager", "Lco/uk/getmondo/common/accounts/AndroidAccountManager;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/accounts/AndroidAccountManager;Lco/uk/getmondo/common/AnalyticsService;)V", "shortcutManager", "Landroid/content/pm/ShortcutManager;", "kotlin.jvm.PlatformType", "buildInfo", "Landroid/content/pm/ShortcutInfo;", "shortcut", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "initialise", "", "refresh", "user", "Lco/uk/getmondo/model/User;", "reportUsed", "appShortcut", "appShortcutId", "", "analyticEvent", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
@TargetApi(25)
public final class a implements c {
   private final ShortcutManager a;
   private final Context b;
   private final p c;
   private final h d;
   private final co.uk.getmondo.common.accounts.d e;
   private final k f;
   private final co.uk.getmondo.common.a g;

   public a(Context var, p var, h var, co.uk.getmondo.common.accounts.d var, k var, co.uk.getmondo.common.a var) {
      l.b(var, "context");
      l.b(var, "userSettingsStorage");
      l.b(var, "userSettingsRepository");
      l.b(var, "accountService");
      l.b(var, "androidAccountManager");
      l.b(var, "analyticsService");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.a = (ShortcutManager)this.b.getSystemService(ShortcutManager.class);
   }

   private final ShortcutInfo a(b var, Context var) {
      ShortcutInfo var = (new Builder(var, var.a())).setShortLabel((CharSequence)var.getString(var.b())).setIcon(Icon.createWithResource(var, var.c())).setIntent(var.a(var)).build();
      l.a(var, "ShortcutInfo.Builder(con…\n                .build()");
      return var;
   }

   private final void a(ak var) {
      if(var != null && var.a() == ak.a.HAS_ACCOUNT) {
         ShortcutInfo var = (ShortcutInfo)null;
         if(l.a(this.c.a(), co.uk.getmondo.payments.send.data.a.d.a)) {
            var = this.a(b.a, this.b);
         } else {
            int var;
            if(l.a(this.c.a(), co.uk.getmondo.payments.send.data.a.d.b)) {
               var = 2131362014;
            } else {
               var = 2131362013;
            }

            this.a.disableShortcuts(m.a(b.a.a()), (CharSequence)this.b.getString(var));
         }

         ShortcutInfo var = (ShortcutInfo)null;
         co.uk.getmondo.d.a var = var.c();
         ShortcutInfo var = var;
         if(var != null) {
            var = var;
            if(!var.e()) {
               var = this.a(b.b, this.b);
            }
         }

         this.a.setDynamicShortcuts(m.c(new ShortcutInfo[]{this.a(b.d, this.b), this.a(b.c, this.b), var, var}));
      } else {
         this.a.removeAllDynamicShortcuts();
         this.a.disableShortcuts(Arrays.asList(new String[]{b.d.a(), b.c.a(), b.b.a(), b.a.a()}), (CharSequence)this.b.getString(2131362015));
      }

   }

   public void a() {
      n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final o var) {
            l.b(var, "emitter");
            final OnAccountsUpdateListener var = (OnAccountsUpdateListener)(new OnAccountsUpdateListener() {
               public final void onAccountsUpdated(Account[] varx) {
                  var.a(co.uk.getmondo.common.b.a.a);
               }
            });
            a.this.f.a(var);
            var.a((f)(new f() {
               public final void a() {
                  a.this.f.b(var);
               }
            }));
         }
      })).mergeWith((r)this.d.b()).subscribe((g)(new g() {
         public final void a(Object var) {
            a.this.a(a.this.e.b());
         }
      }), (g)null.a);
   }

   public void a(b var) {
      l.b(var, "appShortcut");
      this.a(var.a(), var.d());
   }

   public void a(String var, String var) {
      l.b(var, "appShortcutId");
      l.b(var, "analyticEvent");
      this.a.reportShortcutUsed(var);
      this.g.a(Impression.Companion.m(var));
   }
}
