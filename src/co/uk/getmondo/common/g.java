package co.uk.getmondo.common;

import com.crashlytics.android.Crashlytics;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/common/CrashlyticsWrapper;", "", "()V", "setUserIdentifier", "", "userId", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   public final void a(String var) {
      kotlin.d.b.l.b(var, "userId");
      Crashlytics.setUserIdentifier(var);
   }
}
