package co.uk.getmondo.common.g;

import com.google.gson.f;
import com.google.gson.l;
import com.google.gson.s;
import com.google.gson.t;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

public class a implements t {
   public s a(f var, com.google.gson.c.a var) {
      return (new a.a(var.a(this, var), var.a(l.class))).a();
   }

   public static class a extends s {
      private final s a;
      private final s b;

      public a(s var, s var) {
         this.a = var;
         this.b = var;
      }

      public Object a(JsonReader var) throws IOException {
         l var = (l)this.b.a(var);
         Object var;
         if(var.h() && var.k().o().isEmpty()) {
            var = null;
         } else {
            var = this.a.a(var);
         }

         return var;
      }

      public void a(JsonWriter var, Object var) throws IOException {
         this.a.a(var, var);
      }
   }
}
