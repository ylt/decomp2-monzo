package co.uk.getmondo.common.g;

import com.google.gson.JsonParseException;
import com.google.gson.f;
import com.google.gson.l;
import com.google.gson.n;
import com.google.gson.o;
import com.google.gson.s;
import com.google.gson.t;
import com.google.gson.b.j;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class b implements t {
   private final Class a;
   private final String b;
   private final Map c = new LinkedHashMap();
   private final Map d = new LinkedHashMap();
   private Class e;

   private b(Class var, String var) {
      if(var != null && var != null) {
         this.a = var;
         this.b = var;
      } else {
         throw new NullPointerException();
      }
   }

   public static b a(Class var, String var) {
      return new b(var, var);
   }

   public b a(Class var) {
      if(var == null) {
         throw new NullPointerException();
      } else if(var == this.e) {
         throw new IllegalArgumentException("type must be unique");
      } else {
         this.e = var;
         return this;
      }
   }

   public s a(final f var, com.google.gson.c.a var) {
      s var;
      if(var.a() != this.a) {
         var = null;
      } else {
         final LinkedHashMap var = new LinkedHashMap();
         final LinkedHashMap var = new LinkedHashMap();
         Iterator var = this.c.entrySet().iterator();

         while(var.hasNext()) {
            Entry var = (Entry)var.next();
            s var = var.a(this, com.google.gson.c.a.b((Class)var.getValue()));
            var.put(var.getKey(), var);
            var.put(var.getValue(), var);
         }

         var = (new s() {
            public Object a(JsonReader varx) throws IOException {
               l var = j.a(varx);
               l var = var.k().a(b.this.b);
               if(var == null && b.this.e == null) {
                  throw new JsonParseException("cannot deserialize " + b.this.a + " because it does not define a field named " + b.this.b + " and no read fallback type is registered");
               } else {
                  Object var;
                  if(var == null) {
                     var = var.a(b.this, com.google.gson.c.a.b(b.this.e)).a(var);
                  } else {
                     String varx = var.b();
                     s var = (s)var.get(varx);
                     if(var == null) {
                        throw new JsonParseException("cannot deserialize " + b.this.a + " subtype named " + varx + "; did you forget to register a subtype?");
                     }

                     var = var.a(var);
                  }

                  return var;
               }
            }

            public void a(JsonWriter varx, Object var) throws IOException {
               Class var = var.getClass();
               String varx = (String)b.this.d.get(var);
               s var = (s)var.get(var);
               if(var == null) {
                  throw new JsonParseException("cannot serialize " + var.getName() + "; did you forget to register a subtype?");
               } else {
                  n var = var.a(var).k();
                  if(var.b(b.this.b)) {
                     throw new JsonParseException("cannot serialize " + var.getName() + " because it already defines a field named " + b.this.b);
                  } else {
                     n var = new n();
                     var.a(b.this.b, new o(varx));
                     Iterator var = var.o().iterator();

                     while(var.hasNext()) {
                        Entry varx = (Entry)var.next();
                        var.a((String)varx.getKey(), (l)varx.getValue());
                     }

                     j.a(var, varx);
                  }
               }
            }
         }).a();
      }

      return var;
   }

   public b b(Class var, String var) {
      if(var != null && var != null) {
         if(!this.d.containsKey(var) && !this.c.containsKey(var)) {
            this.c.put(var, var);
            this.d.put(var, var);
            return this;
         } else {
            throw new IllegalArgumentException("types and labels must be unique");
         }
      } else {
         throw new NullPointerException();
      }
   }
}
