package co.uk.getmondo.common;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004¨\u0006\u0006"},
   d2 = {"copyToClipboard", "", "Landroid/content/Context;", "label", "", "content", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class e {
   public static final void a(Context var, String var, String var) {
      kotlin.d.b.l.b(var, "$receiver");
      kotlin.d.b.l.b(var, "label");
      kotlin.d.b.l.b(var, "content");
      ClipData var = ClipData.newPlainText((CharSequence)var, (CharSequence)var);
      Object var = var.getSystemService("clipboard");
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.content.ClipboardManager");
      } else {
         ((ClipboardManager)var).setPrimaryClip(var);
         Toast.makeText(var, 2131362764, 0).show();
      }
   }
}
