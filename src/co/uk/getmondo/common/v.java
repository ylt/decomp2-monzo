package co.uk.getmondo.common;

import android.content.res.Resources;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/common/ResourceProvider;", "", "resources", "Landroid/content/res/Resources;", "(Landroid/content/res/Resources;)V", "getString", "", "stringRes", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class v {
   private final Resources a;

   public v(Resources var) {
      kotlin.d.b.l.b(var, "resources");
      super();
      this.a = var;
   }

   public final String a(int var) {
      String var = this.a.getString(var);
      kotlin.d.b.l.a(var, "resources.getString(stringRes)");
      return var;
   }
}
