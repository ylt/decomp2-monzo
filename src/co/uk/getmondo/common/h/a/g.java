package co.uk.getmondo.common.h.a;

import android.content.Context;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(b var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b var) {
      return new g(var);
   }

   public Context a() {
      return (Context)b.a.d.a(this.b.b(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
