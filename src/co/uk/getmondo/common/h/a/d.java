package co.uk.getmondo.common.h.a;

import android.content.Context;
import co.uk.getmondo.payments.send.data.p;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(b var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new d(var, var, var, var, var, var, var);
   }

   public co.uk.getmondo.common.a.c a() {
      return (co.uk.getmondo.common.a.c)b.a.d.a(this.b.a((Context)this.c.b(), (p)this.d.b(), (co.uk.getmondo.payments.send.data.h)this.e.b(), (co.uk.getmondo.common.accounts.d)this.f.b(), (co.uk.getmondo.common.accounts.k)this.g.b(), (co.uk.getmondo.common.a)this.h.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
