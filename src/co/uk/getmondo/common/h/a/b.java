package co.uk.getmondo.common.h.a;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import co.uk.getmondo.common.s;
import co.uk.getmondo.common.u;
import co.uk.getmondo.payments.send.data.p;
import io.intercom.android.sdk.push.IntercomPushClient;

public class b {
   protected final Application a;

   public b(Application var) {
      this.a = var;
   }

   Application a() {
      return this.a;
   }

   co.uk.getmondo.common.a.c a(Context var, p var, co.uk.getmondo.payments.send.data.h var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.accounts.k var, co.uk.getmondo.common.a var) {
      Object var;
      if(VERSION.SDK_INT >= 25) {
         var = new co.uk.getmondo.common.a.a(var, var, var, var, var, var);
      } else {
         var = new co.uk.getmondo.common.a.d();
      }

      return (co.uk.getmondo.common.a.c)var;
   }

   s a(Context var) {
      return new u(var);
   }

   Context b() {
      return this.a;
   }

   io.michaelrocks.libphonenumber.android.h b(Context var) {
      return io.michaelrocks.libphonenumber.android.h.a(var);
   }

   Resources c() {
      return this.a.getResources();
   }

   android.support.v4.b.b.a c(Context var) {
      return android.support.v4.b.b.a.a(var);
   }

   co.uk.getmondo.common.accounts.k d() {
      return new co.uk.getmondo.common.accounts.k(AccountManager.get(this.a));
   }

   io.reactivex.u e() {
      return io.reactivex.h.a.b();
   }

   io.reactivex.u f() {
      return io.reactivex.a.b.a.a();
   }

   io.reactivex.u g() {
      return io.reactivex.h.a.a();
   }

   IntercomPushClient h() {
      return new IntercomPushClient();
   }
}
