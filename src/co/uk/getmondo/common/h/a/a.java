package co.uk.getmondo.common.h.a;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.HelpApi;
import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.MigrationApi;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.OverdraftApi;
import co.uk.getmondo.api.PaymentLimitsApi;
import co.uk.getmondo.api.PaymentsApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.TaxResidencyApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.background_sync.SyncFeedAndBalanceJobService;
import co.uk.getmondo.common.ThirdPartyShareReceiver;
import co.uk.getmondo.common.q;
import co.uk.getmondo.common.s;
import co.uk.getmondo.fcm.FcmRegistrationJobService;
import co.uk.getmondo.fcm.InstanceIDListenerService;
import co.uk.getmondo.fcm.PushNotificationService;
import co.uk.getmondo.payments.send.data.p;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import co.uk.getmondo.waitlist.referral.InstallReferrerReceiver;
import co.uk.getmondo.waitlist.ui.ParallaxLinearLayout;
import io.intercom.android.sdk.push.IntercomPushClient;
import io.reactivex.u;

public interface a {
   co.uk.getmondo.main.h A();

   Resources B();

   co.uk.getmondo.transaction.a.j C();

   MonzoApi D();

   IdentityVerificationApi E();

   PaymentsApi F();

   TaxResidencyApi G();

   MonzoProfileApi H();

   SignupApi I();

   ServiceStatusApi J();

   HelpApi K();

   MigrationApi L();

   OverdraftApi M();

   PaymentLimitsApi N();

   com.google.gson.f O();

   ae P();

   Context a();

   void a(MonzoApplication var);

   void a(SyncFeedAndBalanceJobService var);

   void a(ThirdPartyShareReceiver var);

   void a(FcmRegistrationJobService var);

   void a(InstanceIDListenerService var);

   void a(PushNotificationService var);

   void a(InstallReferrerReceiver var);

   void a(ParallaxLinearLayout var);

   co.uk.getmondo.common.accounts.d b();

   co.uk.getmondo.common.accounts.b c();

   s d();

   u e();

   u f();

   u g();

   co.uk.getmondo.common.a h();

   co.uk.getmondo.b.c i();

   co.uk.getmondo.common.i j();

   co.uk.getmondo.waitlist.i k();

   co.uk.getmondo.api.b.a l();

   co.uk.getmondo.a.a m();

   co.uk.getmondo.feed.a.d n();

   co.uk.getmondo.transaction.a.a o();

   q p();

   IntercomPushClient q();

   co.uk.getmondo.common.m r();

   co.uk.getmondo.payments.send.payment_category.b.a s();

   co.uk.getmondo.payments.send.a.e t();

   co.uk.getmondo.common.a.c u();

   p v();

   co.uk.getmondo.signup.identity_verification.a.h w();

   co.uk.getmondo.settings.k x();

   co.uk.getmondo.common.accounts.o y();

   co.uk.getmondo.payments.send.data.h z();
}
