package co.uk.getmondo.common.h.a;

import android.content.res.Resources;

public final class m implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var;
      if(!m.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public m(b var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b var) {
      return new m(var);
   }

   public Resources a() {
      return (Resources)b.a.d.a(this.b.c(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
