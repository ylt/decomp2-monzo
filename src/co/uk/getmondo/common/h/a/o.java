package co.uk.getmondo.common.h.a;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.HelpApi;
import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.MigrationApi;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.OverdraftApi;
import co.uk.getmondo.api.PaymentLimitsApi;
import co.uk.getmondo.api.PaymentsApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.TaxResidencyApi;
import co.uk.getmondo.api.aa;
import co.uk.getmondo.api.ad;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.ag;
import co.uk.getmondo.api.r;
import co.uk.getmondo.api.u;
import co.uk.getmondo.api.v;
import co.uk.getmondo.api.w;
import co.uk.getmondo.api.x;
import co.uk.getmondo.api.y;
import co.uk.getmondo.api.z;
import co.uk.getmondo.api.authentication.q;
import co.uk.getmondo.api.b.af;
import co.uk.getmondo.background_sync.SyncFeedAndBalanceJobService;
import co.uk.getmondo.card.t;
import co.uk.getmondo.common.ThirdPartyShareReceiver;
import co.uk.getmondo.common.ab;
import co.uk.getmondo.common.accounts.p;
import co.uk.getmondo.fcm.FcmRegistrationJobService;
import co.uk.getmondo.fcm.InstanceIDListenerService;
import co.uk.getmondo.fcm.PushNotificationService;
import co.uk.getmondo.feed.a.s;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import co.uk.getmondo.transaction.a.ac;
import co.uk.getmondo.waitlist.referral.InstallReferrerReceiver;
import co.uk.getmondo.waitlist.ui.ParallaxLinearLayout;
import io.intercom.android.sdk.push.IntercomPushClient;

public final class o implements a {
   // $FF: synthetic field
   static final boolean a;
   private javax.a.a A;
   private javax.a.a B;
   private javax.a.a C;
   private javax.a.a D;
   private javax.a.a E;
   private javax.a.a F;
   private javax.a.a G;
   private javax.a.a H;
   private javax.a.a I;
   private javax.a.a J;
   private javax.a.a K;
   private javax.a.a L;
   private javax.a.a M;
   private javax.a.a N;
   private javax.a.a O;
   private javax.a.a P;
   private javax.a.a Q;
   private javax.a.a R;
   private javax.a.a S;
   private javax.a.a T;
   private javax.a.a U;
   private javax.a.a V;
   private javax.a.a W;
   private javax.a.a X;
   private javax.a.a Y;
   private javax.a.a Z;
   private b.a aA;
   private javax.a.a aB;
   private b.a aC;
   private b.a aD;
   private javax.a.a aE;
   private javax.a.a aF;
   private javax.a.a aG;
   private b.a aH;
   private b.a aI;
   private javax.a.a aJ;
   private javax.a.a aK;
   private javax.a.a aL;
   private javax.a.a aM;
   private javax.a.a aN;
   private b.a aO;
   private javax.a.a aa;
   private javax.a.a ab;
   private javax.a.a ac;
   private javax.a.a ad;
   private javax.a.a ae;
   private javax.a.a af;
   private javax.a.a ag;
   private javax.a.a ah;
   private javax.a.a ai;
   private javax.a.a aj;
   private javax.a.a ak;
   private javax.a.a al;
   private javax.a.a am;
   private javax.a.a an;
   private javax.a.a ao;
   private javax.a.a ap;
   private javax.a.a aq;
   private javax.a.a ar;
   private javax.a.a as;
   private javax.a.a at;
   private javax.a.a au;
   private javax.a.a av;
   private javax.a.a aw;
   private javax.a.a ax;
   private javax.a.a ay;
   private b.a az;
   private javax.a.a b;
   private javax.a.a c;
   private javax.a.a d;
   private javax.a.a e;
   private javax.a.a f;
   private javax.a.a g;
   private javax.a.a h;
   private javax.a.a i;
   private javax.a.a j;
   private javax.a.a k;
   private javax.a.a l;
   private javax.a.a m;
   private javax.a.a n;
   private javax.a.a o;
   private javax.a.a p;
   private javax.a.a q;
   private javax.a.a r;
   private javax.a.a s;
   private javax.a.a t;
   private javax.a.a u;
   private javax.a.a v;
   private javax.a.a w;
   private javax.a.a x;
   private javax.a.a y;
   private javax.a.a z;

   static {
      boolean var;
      if(!o.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   private o(o.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.a(var);
      }
   }

   // $FF: synthetic method
   o(o.a var, Object var) {
      this(var);
   }

   public static o.a Q() {
      return new o.a();
   }

   private void a(o.a var) {
      this.b = g.a(var.a);
      this.c = e.a(var.a);
      this.d = b.a.a.a(c.a(var.a));
      this.e = j.a(var.a);
      this.f = b.a.a.a(co.uk.getmondo.waitlist.j.a(this.b));
      this.g = b.a.a.a(co.uk.getmondo.api.e.a(var.b));
      this.h = co.uk.getmondo.api.k.a(var.b);
      this.i = b.a.a.a(k.a(var.a, this.b));
      this.j = co.uk.getmondo.api.b.a(this.b, this.i);
      this.k = aa.a(var.c);
      this.l = co.uk.getmondo.developer_options.b.a(this.b);
      this.m = b.a.a.a(co.uk.getmondo.api.d.a(var.b, this.b, this.g, this.h, this.j, this.k, this.l));
      this.n = b.a.a.a(co.uk.getmondo.api.i.a(var.b));
      this.o = b.a.a.a(co.uk.getmondo.api.authentication.j.a(this.b, this.n));
      this.p = ad.a(this.b, this.n);
      this.q = b.a.a.a(r.a(var.b, this.m, this.n, this.p, this.k));
      this.r = b.a.a.a(co.uk.getmondo.api.authentication.h.a(this.o, this.q));
      this.s = b.a.a.a(p.a(this.d));
      this.t = co.uk.getmondo.api.authentication.c.a(this.r, this.s);
      this.u = q.a(this.q, this.s);
      this.v = b.a.a.a(co.uk.getmondo.api.authentication.l.a(this.s, this.r, this.n, this.u));
      this.w = b.a.a.a(co.uk.getmondo.api.g.a(var.b, this.m, this.t, this.p, this.v));
      this.x = b.a.a.a(co.uk.getmondo.api.h.a(var.b, this.w, this.k, this.n));
      this.y = b.a.a.a(co.uk.getmondo.api.o.a(var.b, this.x));
      this.z = b.a.a.a(co.uk.getmondo.common.r.a(this.c, this.y, this.e));
      this.A = b.a.a.a(co.uk.getmondo.payments.send.payment_category.g.a(this.b));
      this.B = b.a.a.a(co.uk.getmondo.settings.o.a(this.b));
      this.C = b.a.a.a(co.uk.getmondo.signup.identity_verification.a.i.a(this.b, this.n));
      this.D = co.uk.getmondo.signup.status.h.a(this.b);
      this.E = co.uk.getmondo.migration.e.a(this.b);
      this.F = b.a.a.a(co.uk.getmondo.common.accounts.j.a(this.d, this.e, this.f, this.z, this.A, this.B, this.C, this.D, this.E));
      this.G = co.uk.getmondo.common.accounts.c.a(this.d);
      this.H = n.a(var.a);
      this.I = f.a(var.a);
      this.J = b.a.a.a(co.uk.getmondo.api.n.a(var.b, this.m, this.n, this.t, this.p, this.v, this.k));
      this.K = b.a.a.a(co.uk.getmondo.common.d.a(this.J, this.e));
      this.L = b.a.a.a(co.uk.getmondo.b.d.a(this.K));
      this.M = b.a.a.a(co.uk.getmondo.common.j.a(this.b));
      this.N = b.a.a.a(co.uk.getmondo.api.q.a(var.b));
      this.O = b.a.a.a(u.a(var.b, this.w, this.N, this.k));
      this.P = co.uk.getmondo.card.d.a(this.G, this.y, t.c());
      this.Q = b.a.a.a(co.uk.getmondo.common.h.c());
      this.R = b.a.a.a(co.uk.getmondo.payments.send.data.r.c());
      this.S = b.a.a.a(co.uk.getmondo.payments.send.data.o.a(this.y, this.R, co.uk.getmondo.common.p.c(), ab.c()));
      this.T = b.a.a.a(af.a(this.i, this.F, this.y, this.q, this.O, this.P, co.uk.getmondo.common.k.g.c(), this.K, this.z, this.Q, co.uk.getmondo.common.accounts.n.c(), this.S));
      this.U = b.a.a.a(co.uk.getmondo.a.j.c());
      this.V = b.a.a.a(co.uk.getmondo.a.d.a(this.y, this.U));
      this.W = b.a.a.a(s.c());
      this.X = co.uk.getmondo.feed.b.a(this.b);
      this.Y = co.uk.getmondo.d.a.u.a(this.X, this.G);
      this.Z = co.uk.getmondo.d.a.g.a(this.Y);
      this.aa = co.uk.getmondo.payments.send.payment_category.f.a(this.y, this.A);
      this.ab = co.uk.getmondo.feed.a.c.a(this.b);
      this.ac = co.uk.getmondo.feed.search.b.a(this.b);
      this.ad = l.a(var.a, this.b);
      this.ae = b.a.a.a(co.uk.getmondo.payments.send.a.j.a(this.b, this.H, this.e, this.ad));
      this.af = co.uk.getmondo.feed.a.q.a(this.W, this.y, this.Z, this.aa, this.ab, this.ac, this.ae, this.G, this.e);
      this.ag = b.a.a.a(ac.c());
      this.ah = ag.a(this.g);
      this.ai = b.a.a.a(co.uk.getmondo.transaction.a.i.a(this.y, this.ag, this.ah));
      this.aj = i.a(var.a);
      this.ak = co.uk.getmondo.common.n.a(this.c);
      this.al = b.a.a.a(d.a(var.a, this.b, this.R, this.S, this.F, this.d, this.K));
      this.am = b.a.a.a(co.uk.getmondo.main.i.c());
      this.an = b.a.a.a(m.a(var.a));
      this.ao = b.a.a.a(co.uk.getmondo.api.l.a(var.b, this.x));
      this.ap = b.a.a.a(co.uk.getmondo.api.p.a(var.b, this.x));
      this.aq = b.a.a.a(co.uk.getmondo.api.f.a(var.b, this.m, this.t, this.p, this.v));
      this.ar = b.a.a.a(x.a(var.b, this.aq, this.N, this.k));
      this.as = b.a.a.a(w.a(var.b, this.aq, this.N, this.k));
      this.at = co.uk.getmondo.api.ab.a(var.c);
      this.au = b.a.a.a(v.a(var.b, this.g, this.h, this.n, this.at));
      this.av = b.a.a.a(co.uk.getmondo.api.j.a(var.b, this.w, this.N, this.k));
      this.aw = b.a.a.a(co.uk.getmondo.api.m.a(var.b, this.w, this.N, this.k));
      this.ax = b.a.a.a(co.uk.getmondo.api.s.a(var.b, this.w, this.N, this.k));
      this.ay = b.a.a.a(co.uk.getmondo.api.t.a(var.b, this.w, this.N, this.k));
      this.az = co.uk.getmondo.fcm.g.a(this.n, this.F, this.aj, this.B, this.K);
      this.aA = co.uk.getmondo.waitlist.referral.b.a(this.K);
      this.aB = co.uk.getmondo.fcm.f.a(this.b, this.aj);
      this.aC = co.uk.getmondo.fcm.d.a(this.aB, this.F);
      this.aD = co.uk.getmondo.fcm.c.a(this.e, this.y);
      this.aE = co.uk.getmondo.overdraft.a.e.a(this.ax, co.uk.getmondo.overdraft.a.g.c());
      this.aF = co.uk.getmondo.overdraft.a.b.a(this.b);
      this.aG = co.uk.getmondo.background_sync.f.a(this.H, this.af, this.V, this.G, this.aE, this.aF);
      this.aH = co.uk.getmondo.background_sync.c.a(this.e, this.aG);
      this.aI = co.uk.getmondo.common.ad.a(this.K);
      this.aJ = h.a(var.a, this.b);
      this.aK = b.a.a.a(co.uk.getmondo.b.b.a(this.aJ, this.K));
      this.aL = z.a(var.c);
      this.aM = co.uk.getmondo.adjust.h.a(this.b);
      this.aN = b.a.a.a(co.uk.getmondo.adjust.i.a(this.b, this.e, this.aL, this.aM, this.g, this.J, this.h));
      this.aO = co.uk.getmondo.b.a(this.F, this.aK, this.al, this.z, this.ak, this.Q, this.aN);
   }

   public co.uk.getmondo.main.h A() {
      return (co.uk.getmondo.main.h)this.am.b();
   }

   public Resources B() {
      return (Resources)this.an.b();
   }

   public co.uk.getmondo.transaction.a.j C() {
      return (co.uk.getmondo.transaction.a.j)this.ag.b();
   }

   public MonzoApi D() {
      return (MonzoApi)this.y.b();
   }

   public IdentityVerificationApi E() {
      return (IdentityVerificationApi)this.ao.b();
   }

   public PaymentsApi F() {
      return (PaymentsApi)this.ap.b();
   }

   public TaxResidencyApi G() {
      return (TaxResidencyApi)this.ar.b();
   }

   public MonzoProfileApi H() {
      return (MonzoProfileApi)this.O.b();
   }

   public SignupApi I() {
      return (SignupApi)this.as.b();
   }

   public ServiceStatusApi J() {
      return (ServiceStatusApi)this.au.b();
   }

   public HelpApi K() {
      return (HelpApi)this.av.b();
   }

   public MigrationApi L() {
      return (MigrationApi)this.aw.b();
   }

   public OverdraftApi M() {
      return (OverdraftApi)this.ax.b();
   }

   public PaymentLimitsApi N() {
      return (PaymentLimitsApi)this.ay.b();
   }

   public com.google.gson.f O() {
      return (com.google.gson.f)this.n.b();
   }

   public ae P() {
      return (ae)this.ah.b();
   }

   public Context a() {
      return (Context)this.b.b();
   }

   public void a(MonzoApplication var) {
      this.aO.a(var);
   }

   public void a(SyncFeedAndBalanceJobService var) {
      this.aH.a(var);
   }

   public void a(ThirdPartyShareReceiver var) {
      this.aI.a(var);
   }

   public void a(FcmRegistrationJobService var) {
      this.aD.a(var);
   }

   public void a(InstanceIDListenerService var) {
      this.aC.a(var);
   }

   public void a(PushNotificationService var) {
      this.az.a(var);
   }

   public void a(InstallReferrerReceiver var) {
      this.aA.a(var);
   }

   public void a(ParallaxLinearLayout var) {
      b.a.c.a().a(var);
   }

   public co.uk.getmondo.common.accounts.d b() {
      return (co.uk.getmondo.common.accounts.d)this.F.b();
   }

   public co.uk.getmondo.common.accounts.b c() {
      return (co.uk.getmondo.common.accounts.b)this.G.b();
   }

   public co.uk.getmondo.common.s d() {
      return (co.uk.getmondo.common.s)this.i.b();
   }

   public io.reactivex.u e() {
      return (io.reactivex.u)this.H.b();
   }

   public io.reactivex.u f() {
      return (io.reactivex.u)this.e.b();
   }

   public io.reactivex.u g() {
      return (io.reactivex.u)this.I.b();
   }

   public co.uk.getmondo.common.a h() {
      return (co.uk.getmondo.common.a)this.K.b();
   }

   public co.uk.getmondo.b.c i() {
      return (co.uk.getmondo.b.c)this.L.b();
   }

   public co.uk.getmondo.common.i j() {
      return (co.uk.getmondo.common.i)this.M.b();
   }

   public co.uk.getmondo.waitlist.i k() {
      return (co.uk.getmondo.waitlist.i)this.f.b();
   }

   public co.uk.getmondo.api.b.a l() {
      return (co.uk.getmondo.api.b.a)this.T.b();
   }

   public co.uk.getmondo.a.a m() {
      return (co.uk.getmondo.a.a)this.V.b();
   }

   public co.uk.getmondo.feed.a.d n() {
      return (co.uk.getmondo.feed.a.d)this.af.b();
   }

   public co.uk.getmondo.transaction.a.a o() {
      return (co.uk.getmondo.transaction.a.a)this.ai.b();
   }

   public co.uk.getmondo.common.q p() {
      return (co.uk.getmondo.common.q)this.z.b();
   }

   public IntercomPushClient q() {
      return (IntercomPushClient)this.aj.b();
   }

   public co.uk.getmondo.common.m r() {
      return (co.uk.getmondo.common.m)this.ak.b();
   }

   public co.uk.getmondo.payments.send.payment_category.b.a s() {
      return (co.uk.getmondo.payments.send.payment_category.b.a)this.A.b();
   }

   public co.uk.getmondo.payments.send.a.e t() {
      return (co.uk.getmondo.payments.send.a.e)this.ae.b();
   }

   public co.uk.getmondo.common.a.c u() {
      return (co.uk.getmondo.common.a.c)this.al.b();
   }

   public co.uk.getmondo.payments.send.data.p v() {
      return (co.uk.getmondo.payments.send.data.p)this.R.b();
   }

   public co.uk.getmondo.signup.identity_verification.a.h w() {
      return (co.uk.getmondo.signup.identity_verification.a.h)this.C.b();
   }

   public co.uk.getmondo.settings.k x() {
      return (co.uk.getmondo.settings.k)this.B.b();
   }

   public co.uk.getmondo.common.accounts.o y() {
      return (co.uk.getmondo.common.accounts.o)this.s.b();
   }

   public co.uk.getmondo.payments.send.data.h z() {
      return (co.uk.getmondo.payments.send.data.h)this.S.b();
   }

   public static final class a {
      private b a;
      private co.uk.getmondo.api.c b;
      private y c;

      private a() {
      }

      // $FF: synthetic method
      a(Object var) {
         this();
      }

      public a a() {
         if(this.a == null) {
            throw new IllegalStateException(b.class.getCanonicalName() + " must be set");
         } else {
            if(this.b == null) {
               this.b = new co.uk.getmondo.api.c();
            }

            if(this.c == null) {
               this.c = new y();
            }

            return new o(this);
         }
      }

      public o.a a(b var) {
         this.a = (b)b.a.d.a(var);
         return this;
      }
   }
}
