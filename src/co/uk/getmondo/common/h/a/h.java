package co.uk.getmondo.common.h.a;

import android.content.Context;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!h.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public h(b var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(b var, javax.a.a var) {
      return new h(var, var);
   }

   public android.support.v4.b.b.a a() {
      return (android.support.v4.b.b.a)b.a.d.a(this.b.c((Context)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
