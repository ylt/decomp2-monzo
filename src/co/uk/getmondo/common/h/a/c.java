package co.uk.getmondo.common.h.a;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(b var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b var) {
      return new c(var);
   }

   public co.uk.getmondo.common.accounts.k a() {
      return (co.uk.getmondo.common.accounts.k)b.a.d.a(this.b.d(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
