package co.uk.getmondo.common.h.a;

import android.content.Context;
import co.uk.getmondo.common.s;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!k.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public k(b var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(b var, javax.a.a var) {
      return new k(var, var);
   }

   public s a() {
      return (s)b.a.d.a(this.b.a((Context)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
