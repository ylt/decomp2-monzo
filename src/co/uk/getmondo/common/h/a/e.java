package co.uk.getmondo.common.h.a;

import android.app.Application;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(b var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b var) {
      return new e(var);
   }

   public Application a() {
      return (Application)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
