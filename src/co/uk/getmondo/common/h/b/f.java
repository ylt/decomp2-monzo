package co.uk.getmondo.common.h.b;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(c var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var) {
      return new f(var, var);
   }

   public ThreeDsResolver a() {
      return (ThreeDsResolver)b.a.d.a(this.b.a((co.uk.getmondo.common.a)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
