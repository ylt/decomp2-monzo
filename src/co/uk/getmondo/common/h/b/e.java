package co.uk.getmondo.common.h.b;

import android.content.Context;
import com.stripe.android.Stripe;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(c var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var) {
      return new e(var, var);
   }

   public Stripe a() {
      return (Stripe)b.a.d.a(this.b.a((Context)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
