package co.uk.getmondo.common.h.b;

import android.app.Activity;
import android.content.Context;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.Stripe;

public class c {
   private Activity a;

   public c(Activity var) {
      this.a = var;
   }

   Context a() {
      return this.a;
   }

   ThreeDsResolver a(co.uk.getmondo.common.a var) {
      return new co.uk.getmondo.topup.three_d_secure.b(this.a, var);
   }

   Stripe a(Context var) {
      return new Stripe(var, "pk_live_LW3b2bi1iXHicd88NnwlCkaA");
   }
}
