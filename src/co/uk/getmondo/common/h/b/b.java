package co.uk.getmondo.common.h.b;

import co.uk.getmondo.bump_up.BumpUpActivity;
import co.uk.getmondo.bump_up.WebEventActivity;
import co.uk.getmondo.card.CardReplacementActivity;
import co.uk.getmondo.common.address.LegacyEnterAddressActivity;
import co.uk.getmondo.common.address.m;
import co.uk.getmondo.common.address.n;
import co.uk.getmondo.common.pin.g;
import co.uk.getmondo.create_account.VerifyIdentityActivity;
import co.uk.getmondo.create_account.phone_number.EnterCodeActivity;
import co.uk.getmondo.create_account.phone_number.EnterPhoneNumberActivity;
import co.uk.getmondo.create_account.topup.InitialTopupActivity;
import co.uk.getmondo.create_account.topup.TopupInfoActivity;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.feed.MonthlySpendingReportActivity;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.feed.welcome.WelcomeToMonzoActivity;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.help.HelpActivity;
import co.uk.getmondo.help.HelpSearchActivity;
import co.uk.getmondo.main.EddLimitsActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.migration.MigrationAnnouncementActivity;
import co.uk.getmondo.migration.MigrationTourActivity;
import co.uk.getmondo.monzo.me.onboarding.MonzoMeOnboardingActivity;
import co.uk.getmondo.monzo.me.request.RequestMoneyFragment;
import co.uk.getmondo.news.k;
import co.uk.getmondo.overdraft.ChangeOverdraftLimitActivity;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import co.uk.getmondo.payments.send.SendMoneyFragment;
import co.uk.getmondo.payments.send.bank.BankPaymentActivity;
import co.uk.getmondo.payments.send.bank.payee.UkBankPayeeDetailsFragment;
import co.uk.getmondo.payments.send.bank.payment.h;
import co.uk.getmondo.payments.send.bank.payment.i;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerIntroActivity;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerMoreInfoActivity;
import co.uk.getmondo.payments.send.payment_category.l;
import co.uk.getmondo.pin.ForgotPinActivity;
import co.uk.getmondo.pots.CreatePotActivity;
import co.uk.getmondo.pots.CustomPotNameActivity;
import co.uk.getmondo.profile.address.SelectAddressActivity;
import co.uk.getmondo.settings.LimitsActivity;
import co.uk.getmondo.settings.SettingsActivity;
import co.uk.getmondo.signup.EmailActivity;
import co.uk.getmondo.signup.card_activation.ActivateCardActivity;
import co.uk.getmondo.signup.card_activation.CardOnItsWayActivity;
import co.uk.getmondo.signup.card_ordering.OrderCardActivity;
import co.uk.getmondo.signup.documents.LegalDocumentsActivity;
import co.uk.getmondo.signup.identity_verification.CountrySelectionActivity;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.o;
import co.uk.getmondo.signup.identity_verification.p;
import co.uk.getmondo.signup.identity_verification.t;
import co.uk.getmondo.signup.identity_verification.chat_with_us.ChatWithUsActivity;
import co.uk.getmondo.signup.marketing_opt_in.MarketingOptInActivity;
import co.uk.getmondo.signup.pending.SignupPendingActivity;
import co.uk.getmondo.signup.phone_verification.PhoneVerificationActivity;
import co.uk.getmondo.signup.phone_verification.j;
import co.uk.getmondo.signup.profile.AddressConfirmationActivity;
import co.uk.getmondo.signup.profile.ProfileCreationActivity;
import co.uk.getmondo.signup.rejected.SignupRejectedActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import co.uk.getmondo.signup.tax_residency.TaxResidencyActivity;
import co.uk.getmondo.signup.tax_residency.b.q;
import co.uk.getmondo.signup_old.CreateProfileActivity;
import co.uk.getmondo.signup_old.SignUpActivity;
import co.uk.getmondo.splash.SplashActivity;
import co.uk.getmondo.terms_and_conditions.TermsAndConditionsActivity;
import co.uk.getmondo.top.NotInCountryActivity;
import co.uk.getmondo.top.TopActivity;
import co.uk.getmondo.topup.TopUpActivity;
import co.uk.getmondo.topup.bank.TopUpInstructionsActivity;
import co.uk.getmondo.topup.card.TopUpWithCardFragment;
import co.uk.getmondo.topup.card.TopUpWithNewCardActivity;
import co.uk.getmondo.topup.card.TopUpWithSavedCardActivity;
import co.uk.getmondo.topup.three_d_secure.ThreeDsWebActivity;
import co.uk.getmondo.transaction.attachment.AttachmentActivity;
import co.uk.getmondo.transaction.attachment.AttachmentDetailsActivity;
import co.uk.getmondo.waitlist.ShareActivity;
import co.uk.getmondo.waitlist.WaitlistActivity;
import co.uk.getmondo.welcome.WelcomeOnboardingActivity;

public interface b {
   co.uk.getmondo.card.activate.b a(co.uk.getmondo.card.activate.c var);

   m a(n var);

   co.uk.getmondo.common.pin.a a(g var);

   co.uk.getmondo.golden_ticket.c a(co.uk.getmondo.golden_ticket.d var);

   co.uk.getmondo.help.b.a a(co.uk.getmondo.help.b.b var);

   co.uk.getmondo.help.b.e a(co.uk.getmondo.help.b.f var);

   co.uk.getmondo.monzo.me.customise.d a(co.uk.getmondo.monzo.me.customise.e var);

   co.uk.getmondo.monzo.me.deeplink.b a(co.uk.getmondo.monzo.me.deeplink.c var);

   co.uk.getmondo.news.b a(k var);

   co.uk.getmondo.payments.recurring_cancellation.e a(co.uk.getmondo.payments.recurring_cancellation.f var);

   co.uk.getmondo.payments.send.authentication.e a(co.uk.getmondo.payments.send.authentication.f var);

   h a(i var);

   l a(co.uk.getmondo.payments.send.payment_category.m var);

   co.uk.getmondo.payments.send.peer.e a(co.uk.getmondo.payments.send.peer.f var);

   co.uk.getmondo.profile.address.b a(co.uk.getmondo.profile.address.c var);

   o a(p var);

   co.uk.getmondo.signup.identity_verification.sdd.b a(co.uk.getmondo.signup.identity_verification.sdd.c var);

   co.uk.getmondo.signup.identity_verification.video.e a(co.uk.getmondo.signup.identity_verification.video.f var);

   co.uk.getmondo.spending.merchant.a a(co.uk.getmondo.spending.merchant.f var);

   co.uk.getmondo.spending.transactions.b a(co.uk.getmondo.spending.transactions.d var);

   co.uk.getmondo.transaction.c a(co.uk.getmondo.transaction.d var);

   void a(BumpUpActivity var);

   void a(WebEventActivity var);

   void a(co.uk.getmondo.c.a var);

   void a(CardReplacementActivity var);

   void a(co.uk.getmondo.card.a var);

   void a(LegacyEnterAddressActivity var);

   void a(co.uk.getmondo.common.d.c var);

   void a(VerifyIdentityActivity var);

   void a(EnterCodeActivity var);

   void a(EnterPhoneNumberActivity var);

   void a(InitialTopupActivity var);

   void a(TopupInfoActivity var);

   void a(CardShippedActivity var);

   void a(MonthlySpendingReportActivity var);

   void a(co.uk.getmondo.feed.e var);

   void a(FeedSearchActivity var);

   void a(WelcomeToMonzoActivity var);

   void a(ForceUpgradeActivity var);

   void a(HelpActivity var);

   void a(HelpSearchActivity var);

   void a(EddLimitsActivity var);

   void a(HomeActivity var);

   void a(MigrationAnnouncementActivity var);

   void a(MigrationTourActivity var);

   void a(MonzoMeOnboardingActivity var);

   void a(RequestMoneyFragment var);

   void a(ChangeOverdraftLimitActivity var);

   void a(RecurringPaymentsActivity var);

   void a(SendMoneyFragment var);

   void a(BankPaymentActivity var);

   void a(UkBankPayeeDetailsFragment var);

   void a(PeerToPeerIntroActivity var);

   void a(PeerToPeerMoreInfoActivity var);

   void a(ForgotPinActivity var);

   void a(CreatePotActivity var);

   void a(CustomPotNameActivity var);

   void a(SelectAddressActivity var);

   void a(co.uk.getmondo.profile.address.n var);

   void a(LimitsActivity var);

   void a(SettingsActivity var);

   void a(EmailActivity var);

   void a(ActivateCardActivity var);

   void a(CardOnItsWayActivity var);

   void a(OrderCardActivity var);

   void a(co.uk.getmondo.signup.card_ordering.a var);

   void a(co.uk.getmondo.signup.card_ordering.e var);

   void a(LegalDocumentsActivity var);

   void a(co.uk.getmondo.signup.documents.b var);

   void a(CountrySelectionActivity var);

   void a(IdentityApprovedActivity var);

   void a(ChatWithUsActivity var);

   void a(t var);

   void a(MarketingOptInActivity var);

   void a(co.uk.getmondo.signup.marketing_opt_in.d var);

   void a(SignupPendingActivity var);

   void a(PhoneVerificationActivity var);

   void a(co.uk.getmondo.signup.phone_verification.e var);

   void a(j var);

   void a(AddressConfirmationActivity var);

   void a(ProfileCreationActivity var);

   void a(co.uk.getmondo.signup.profile.e var);

   void a(co.uk.getmondo.signup.profile.l var);

   void a(SignupRejectedActivity var);

   void a(SignupStatusActivity var);

   void a(TaxResidencyActivity var);

   void a(co.uk.getmondo.signup.tax_residency.b.b var);

   void a(co.uk.getmondo.signup.tax_residency.b.g var);

   void a(co.uk.getmondo.signup.tax_residency.b.l var);

   void a(q var);

   void a(CreateProfileActivity var);

   void a(SignUpActivity var);

   void a(co.uk.getmondo.spending.b.a var);

   void a(co.uk.getmondo.spending.b var);

   void a(SplashActivity var);

   void a(TermsAndConditionsActivity var);

   void a(NotInCountryActivity var);

   void a(TopActivity var);

   void a(TopUpActivity var);

   void a(TopUpInstructionsActivity var);

   void a(TopUpWithCardFragment var);

   void a(TopUpWithNewCardActivity var);

   void a(TopUpWithSavedCardActivity var);

   void a(ThreeDsWebActivity var);

   void a(AttachmentActivity var);

   void a(AttachmentDetailsActivity var);

   void a(ShareActivity var);

   void a(WaitlistActivity var);

   void a(WelcomeOnboardingActivity var);
}
