package co.uk.getmondo.common;

import co.uk.getmondo.api.AnalyticsApi;
import co.uk.getmondo.api.model.tracking.Impression;

public class a {
   private final AnalyticsApi a;
   private final io.reactivex.u b;

   public a(AnalyticsApi var, io.reactivex.u var) {
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   static void b(Impression var) throws Exception {
      d.a.a.b("Impression tracked: %s", new Object[]{var.toString()});
   }

   public void a(Impression var) {
      this.a.trackEvent(var).b(this.b).a(b.a(var), c.a());
   }
}
