package co.uk.getmondo.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import co.uk.getmondo.api.model.service_status.ServiceStatusIncident;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.q;
import co.uk.getmondo.common.t;
import co.uk.getmondo.help.HelpActivity;
import co.uk.getmondo.migration.MigrationAnnouncementActivity;
import co.uk.getmondo.news.NewsActivity;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerIntroActivity;
import co.uk.getmondo.payments.send.peer.PeerPaymentActivity;
import co.uk.getmondo.settings.SettingsActivity;
import co.uk.getmondo.signup.identity_verification.VerificationPendingActivity;
import co.uk.getmondo.signup.identity_verification.a.j;
import co.uk.getmondo.signup.identity_verification.sdd.IdentityVerificationSddActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import io.reactivex.n;
import io.reactivex.r;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.ab;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;
import org.threeten.bp.YearMonth;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000´\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u008e\u00012\u00020\u00012\u00020\u0002:\u0002\u008e\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u00103\u001a\u000202H\u0002J\n\u00104\u001a\u0004\u0018\u000105H\u0002J\n\u00106\u001a\u0004\u0018\u00010'H\u0016J\b\u00107\u001a\u000202H\u0016J\b\u00108\u001a\u000202H\u0016J\b\u00109\u001a\u000202H\u0016J\"\u0010:\u001a\u0002022\u0006\u0010;\u001a\u00020+2\u0006\u0010<\u001a\u00020+2\b\u0010=\u001a\u0004\u0018\u00010>H\u0014J\b\u0010?\u001a\u000202H\u0016J\u000e\u0010@\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\u000e\u0010B\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\u0012\u0010C\u001a\u0002022\b\u0010D\u001a\u0004\u0018\u00010EH\u0014J\b\u0010F\u001a\u000202H\u0014J\u000e\u0010G\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\u000e\u0010H\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\u0010\u0010I\u001a\u0002022\u0006\u0010J\u001a\u00020>H\u0002J\u000e\u0010K\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\u0010\u0010L\u001a\u0002022\u0006\u0010J\u001a\u00020>H\u0014J\u000e\u0010M\u001a\b\u0012\u0004\u0012\u00020$0AH\u0016J\u000e\u0010N\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J,\u0010O\u001a&\u0012\f\u0012\n %*\u0004\u0018\u00010202 %*\u0012\u0012\f\u0012\n %*\u0004\u0018\u00010202\u0018\u00010#0#H\u0016J\u000e\u0010P\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\b\u0010Q\u001a\u000202H\u0014J\u0010\u0010R\u001a\u0002022\u0006\u0010S\u001a\u00020EH\u0014J\u000e\u0010T\u001a\b\u0012\u0004\u0012\u00020\u00130AH\u0016J\u000e\u0010U\u001a\b\u0012\u0004\u0012\u0002020AH\u0016J\b\u0010V\u001a\u000202H\u0014J\b\u0010W\u001a\u000202H\u0016J\b\u0010X\u001a\u000202H\u0016J\b\u0010Y\u001a\u000202H\u0016J\b\u0010Z\u001a\u000202H\u0016J\b\u0010[\u001a\u000202H\u0016J\b\u0010\\\u001a\u000202H\u0016J\b\u0010]\u001a\u000202H\u0016J\u0010\u0010^\u001a\u0002022\u0006\u0010_\u001a\u00020`H\u0016J\u0010\u0010a\u001a\u0002022\u0006\u0010_\u001a\u00020`H\u0016J\u0012\u0010b\u001a\u0002022\b\u0010c\u001a\u0004\u0018\u00010dH\u0016J\u0012\u0010e\u001a\u0002022\b\u0010c\u001a\u0004\u0018\u00010dH\u0016J\b\u0010f\u001a\u000202H\u0016J\b\u0010g\u001a\u000202H\u0016J\b\u0010h\u001a\u000202H\u0016J\u0010\u0010i\u001a\u0002022\u0006\u0010j\u001a\u00020!H\u0002J\u001a\u0010k\u001a\u0002022\b\u0010l\u001a\u0004\u0018\u00010d2\u0006\u0010m\u001a\u00020dH\u0016J\u000e\u0010n\u001a\u0002022\u0006\u0010o\u001a\u00020pJ\b\u0010q\u001a\u000202H\u0016J\u000e\u0010r\u001a\u0002022\u0006\u0010s\u001a\u00020!J\b\u0010t\u001a\u000202H\u0016J\b\u0010u\u001a\u000202H\u0016J\b\u0010v\u001a\u000202H\u0016J\b\u0010w\u001a\u000202H\u0016J\u0012\u0010x\u001a\u0002022\b\b\u0001\u0010y\u001a\u00020+H\u0016J\u0010\u0010x\u001a\u0002022\u0006\u0010y\u001a\u00020dH\u0016J\u0010\u0010z\u001a\u0002022\u0006\u0010{\u001a\u00020dH\u0016J\b\u0010|\u001a\u000202H\u0016J!\u0010}\u001a\u0002022\u0006\u0010~\u001a\u00020d2\u0006\u0010\u007f\u001a\u00020d2\u0007\u0010\u0080\u0001\u001a\u00020!H\u0016J\t\u0010\u0081\u0001\u001a\u000202H\u0016J\u0013\u0010\u0082\u0001\u001a\u0002022\b\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0016J\u0013\u0010\u0085\u0001\u001a\u0002022\b\u0010\u0086\u0001\u001a\u00030\u0087\u0001H\u0016J\t\u0010\u0088\u0001\u001a\u000202H\u0016J\u001b\u0010\u0089\u0001\u001a\u0002022\u0007\u0010\u008a\u0001\u001a\u00020d2\u0007\u0010\u008b\u0001\u001a\u00020dH\u0016J\u0012\u0010\u008c\u0001\u001a\u0002022\u0007\u0010\u008d\u0001\u001a\u00020\u0013H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0014\u001a\u00020\u00158\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001e\u0010\u001a\u001a\u00020\u001b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u000e\u0010 \u001a\u00020!X\u0082\u000e¢\u0006\u0002\n\u0000R2\u0010\"\u001a&\u0012\f\u0012\n %*\u0004\u0018\u00010$0$ %*\u0012\u0012\f\u0012\n %*\u0004\u0018\u00010$0$\u0018\u00010#0#X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u00020'8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b(\u0010)R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010\u0011\u001a\u0004\b,\u0010-R\u0010\u0010/\u001a\u0004\u0018\u000100X\u0082\u000e¢\u0006\u0002\n\u0000R2\u00101\u001a&\u0012\f\u0012\n %*\u0004\u0018\u00010202 %*\u0012\u0012\f\u0012\n %*\u0004\u0018\u00010202\u0018\u00010#0#X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u008f\u0001"},
   d2 = {"Lco/uk/getmondo/main/HomeActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/main/HomePresenter$View;", "()V", "actionBarDrawerToggle", "Landroid/support/v7/app/ActionBarDrawerToggle;", "appShortcuts", "Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;", "getAppShortcuts", "()Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;", "setAppShortcuts", "(Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;)V", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "getAvatarGenerator", "()Lco/uk/getmondo/common/ui/AvatarGenerator;", "avatarGenerator$delegate", "Lkotlin/Lazy;", "currentScreen", "Lco/uk/getmondo/main/Screen;", "homePresenter", "Lco/uk/getmondo/main/HomePresenter;", "getHomePresenter", "()Lco/uk/getmondo/main/HomePresenter;", "setHomePresenter", "(Lco/uk/getmondo/main/HomePresenter;)V", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "isRecoveringState", "", "monzoMeDataChangedSubject", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lco/uk/getmondo/main/MonzoMeData;", "kotlin.jvm.PlatformType", "navigationDrawerHeader", "Landroid/view/View;", "getNavigationDrawerHeader", "()Landroid/view/View;", "profileInitialFontSize", "", "getProfileInitialFontSize", "()I", "profileInitialFontSize$delegate", "spendingYearMonth", "Lorg/threeten/bp/YearMonth;", "startSubject", "", "closeDrawer", "findCurrentFragment", "Lco/uk/getmondo/common/fragments/BaseFragment;", "getSnackbarView", "hideLoading", "hideMigrationBanner", "hideOutageWarning", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onChatClicked", "Lio/reactivex/Observable;", "onCommunityClicked", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onGhostLogOutClicked", "onHelpClicked", "onIntent", "intent", "onMigrationBannerClicked", "onNewIntent", "onPendingMonzoMeDataChanged", "onRefreshIncidents", "onRefreshMigrationBanner", "onRefreshUserSettings", "onResume", "onSaveInstanceState", "outState", "onScreenChanged", "onSettingsClicked", "onStart", "openChat", "openCommunity", "openCurrentAccountComing", "openCurrentAccountHere", "openFaqs", "openHelp", "openP2pOnboardingFromContacts", "openP2pOnboardingFromMonzoMeDeepLink", "pendingPayment", "Lco/uk/getmondo/payments/send/data/model/PeerPayment;", "openPaymentInfo", "openSddMigrationDismissible", "rejectionNote", "", "openSddMigrationPersistent", "openSettings", "openSignup", "openVerificationPending", "setMigrationBannerStyle", "loudStyle", "setProfileInformation", "nameToDisplay", "emailAddress", "setupNavigationDrawer", "toolbar", "Landroid/support/v7/widget/Toolbar;", "showActivateCardBanner", "showBottomNavShadow", "show", "showCannotPayYourself", "showContactHasP2pDisabled", "showContactNotOnMonzo", "showContinueSignupBanner", "showError", "message", "showGhostBanner", "email", "showLoading", "showMigrationBanner", "title", "subtitle", "signupAllowed", "showMonzoMeGenericError", "showNews", "newsItem", "Lco/uk/getmondo/news/NewsItem;", "showOutageWarning", "incident", "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;", "showP2pBlocked", "showRetailUi", "sortCode", "accountNumber", "showScreen", "screen", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HomeActivity extends co.uk.getmondo.common.activities.b implements c.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(HomeActivity.class), "avatarGenerator", "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;")), (l)y.a(new w(y.a(HomeActivity.class), "profileInitialFontSize", "getProfileInitialFontSize()I"))};
   public static final HomeActivity.a f = new HomeActivity.a((kotlin.d.b.i)null);
   public c b;
   public q c;
   public co.uk.getmondo.common.a.c e;
   private final com.b.b.c g = com.b.b.c.a();
   private final com.b.b.c h = com.b.b.c.a();
   private final kotlin.c i = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.common.ui.a b() {
         return co.uk.getmondo.common.ui.a.a.a((Context)HomeActivity.this);
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c j;
   private boolean k;
   private YearMonth l;
   private android.support.v7.app.b m;
   private g n;
   private HashMap o;

   public HomeActivity() {
      this.j = kotlin.d.a((kotlin.d.a.a)null.a);
   }

   private final co.uk.getmondo.common.ui.a R() {
      kotlin.c var = this.i;
      l var = a[0];
      return (co.uk.getmondo.common.ui.a)var.a();
   }

   private final int S() {
      kotlin.c var = this.j;
      l var = a[1];
      return ((Number)var.a()).intValue();
   }

   private final View T() {
      View var = ((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).c(0);
      kotlin.d.b.l.a(var, "navigationDrawer.getHeaderView(0)");
      return var;
   }

   private final void U() {
      ((DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout)).post((Runnable)(new Runnable() {
         public final void run() {
            ((DrawerLayout)HomeActivity.this.a(co.uk.getmondo.c.a.drawerLayout)).f(8388611);
         }
      }));
   }

   private final co.uk.getmondo.common.f.a V() {
      return (co.uk.getmondo.common.f.a)this.getSupportFragmentManager().a(2131820954);
   }

   public static final Intent a(Context var, YearMonth var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "yearMonth");
      return f.a(var, var);
   }

   public static final void a(Context var) {
      kotlin.d.b.l.b(var, "context");
      f.a(var);
   }

   public static final void a(Context var, String var, co.uk.getmondo.d.c var, String var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "username");
      f.a(var, var, var, var);
   }

   private final void a(Intent var) {
      if(var.hasExtra("KEY_ID_FROM_SHORTCUT")) {
         co.uk.getmondo.common.a.c var = this.e;
         if(var == null) {
            kotlin.d.b.l.b("appShortcuts");
         }

         var.a(var.getStringExtra("KEY_ID_FROM_SHORTCUT"), var.getStringExtra("KEY_ANALYTIC_FROM_SHORTCUT"));
      }

      if(var.hasExtra("KEY_YEAR_MONTH")) {
         Serializable var = var.getSerializableExtra("KEY_YEAR_MONTH");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.YearMonth");
         }

         this.l = (YearMonth)var;
      }

      g var = g.a(var.getIntExtra("KEY_CURRENT_SCREEN", g.a.e));
      if(var != null) {
         ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).setSelectedItemId(var.f);
      }

   }

   public static final Intent b(Context var) {
      kotlin.d.b.l.b(var, "context");
      return f.b(var);
   }

   private final void b(boolean var) {
      if(var) {
         ((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)).setBackgroundResource(2131689477);
         ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerTitle)).setTextColor(-1);
         ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle)).setTextColor(-1);
         ((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).setTextColor(-1);
      } else {
         ((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)).setBackgroundResource(2131689589);
         ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerTitle)).setTextColor(android.support.v4.content.a.c((Context)this, 2131689526));
         ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle)).setTextColor(android.support.v4.content.a.c((Context)this, 2131689582));
         ((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).setTextColor(android.support.v4.content.a.c((Context)this, 2131689477));
      }

   }

   public static final Intent c(Context var) {
      kotlin.d.b.l.b(var, "context");
      return f.c(var);
   }

   public void A() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362649), this.getString(2131362636), false).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
   }

   public void B() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362649), this.getString(2131362642), false).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
   }

   public void C() {
      co.uk.getmondo.common.d.e.a().show(this.getFragmentManager(), "TAG_ERROR_P2P_BLOCKED");
      ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).post((Runnable)(new Runnable() {
         public final void run() {
            g var = HomeActivity.this.n;
            if(var != null) {
               ((BottomNavigationView)HomeActivity.this.a(co.uk.getmondo.c.a.bottomNavigationView)).setSelectedItemId(var.f);
            }

         }
      }));
   }

   public n D() {
      com.b.b.c var = this.g;
      kotlin.d.b.l.a(var, "monzoMeDataChangedSubject");
      return (n)var;
   }

   public void E() {
      ((LinearLayout)this.a(co.uk.getmondo.c.a.outageBanner)).setVisibility(8);
   }

   public void F() {
      this.s();
   }

   public void G() {
      this.t();
   }

   public void H() {
      this.startActivity(VerificationPendingActivity.c.a((Context)this, j.b, Impression.KycFrom.KYC_SDD_BLOCKED));
      this.finishAffinity();
   }

   public com.b.b.c I() {
      return this.h;
   }

   // $FF: synthetic method
   public n J() {
      return (n)this.I();
   }

   public n K() {
      n var = com.b.a.c.c.a((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      r var = (r)var;
      var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = n.merge(var, (r)var);
      kotlin.d.b.l.a(var, "Observable.merge(migrati…ionBannerButton.clicks())");
      return var;
   }

   public void L() {
      this.b(true);
      ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerTitle)).setText((CharSequence)this.getString(2131362426));
      ((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).setText((CharSequence)this.getString(2131362421));
      ae.b((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle));
      ae.a((View)((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)));
   }

   public void M() {
      this.b(true);
      ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerTitle)).setText((CharSequence)this.getString(2131362424));
      ((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).setText((CharSequence)this.getString(2131362425));
      ae.b((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle));
      ae.a((View)((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)));
   }

   public void N() {
      ae.b((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner));
   }

   public void O() {
      MigrationAnnouncementActivity.a var = MigrationAnnouncementActivity.c;
      Context var = (Context)this;
      String var = this.getString(2131362428);
      kotlin.d.b.l.a(var, "getString(R.string.migra…ent_account_coming_title)");
      String var = this.getString(2131362427);
      kotlin.d.b.l.a(var, "getString(R.string.migra…rent_account_coming_body)");
      this.startActivity(var.a(var, var, var, false));
   }

   public void P() {
      MigrationAnnouncementActivity.a var = MigrationAnnouncementActivity.c;
      Context var = (Context)this;
      String var = this.getString(2131362430);
      kotlin.d.b.l.a(var, "getString(R.string.migra…rrent_account_here_title)");
      String var = this.getString(2131362429);
      kotlin.d.b.l.a(var, "getString(R.string.migra…urrent_account_here_body)");
      this.startActivity(var.a(var, var, var, true));
   }

   public void Q() {
      this.startActivity(SignupStatusActivity.c.a((Context)this, co.uk.getmondo.signup.j.a));
   }

   public View a(int var) {
      if(this.o == null) {
         this.o = new HashMap();
      }

      View var = (View)this.o.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.o.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.ghostBannerLogoutButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public final void a(Toolbar var) {
      kotlin.d.b.l.b(var, "toolbar");
      android.support.v7.app.b var = this.m;
      if(var != null) {
         ((DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout)).b((android.support.v4.widget.DrawerLayout.c)var);
      }

      this.m = new android.support.v7.app.b((Activity)this, (DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout), var, 2131362492, 2131362491);
      android.support.v7.app.b var = this.m;
      if(var != null) {
         ((DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout)).a((android.support.v4.widget.DrawerLayout.c)var);
         var.a();
      }

   }

   public void a(final ServiceStatusIncident var) {
      kotlin.d.b.l.b(var, "incident");
      ((LinearLayout)this.a(co.uk.getmondo.c.a.outageBanner)).setVisibility(0);
      ((TextView)this.a(co.uk.getmondo.c.a.outageBannerTextView)).setText((CharSequence)var.b());
      ((Button)this.a(co.uk.getmondo.c.a.outageBannerButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View varx) {
            (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)HomeActivity.this, 2131689487)).a().a((Context)HomeActivity.this, Uri.parse(var.c()));
         }
      }));
   }

   public void a(g var) {
      kotlin.d.b.l.b(var, "screen");
      if(!kotlin.d.b.l.a(var, this.n)) {
         if(!this.k) {
            Fragment var = var.a();
            if(this.l != null && kotlin.d.b.l.a(var, g.b)) {
               Bundle var = new Bundle();
               var.putSerializable("KEY_YEAR_MONTH", (Serializable)this.l);
               var.setArguments(var);
               this.l = (YearMonth)null;
            }

            this.getSupportFragmentManager().a().b(2131820954, var).c();
         }

         this.n = var;
         this.k = false;
         this.a(true);
      }

   }

   public void a(co.uk.getmondo.news.c var) {
      kotlin.d.b.l.b(var, "newsItem");
      this.startActivity(NewsActivity.a((Context)this, var));
   }

   public void a(co.uk.getmondo.payments.send.data.a.g var) {
      kotlin.d.b.l.b(var, "pendingPayment");
      this.startActivity(PeerPaymentActivity.a((Context)this, var, Impression.PaymentFlowFrom.FROM_MONZO_ME));
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "email");
      this.getWindow().setStatusBarColor(android.support.v4.content.a.c((Context)this, 2131689713));
      ((TextView)this.a(co.uk.getmondo.c.a.ghostBannerTextView)).setText((CharSequence)("" + co.uk.getmondo.common.k.e.c() + ' ' + var));
      ((RelativeLayout)this.a(co.uk.getmondo.c.a.ghostBanner)).setVisibility(0);
   }

   public void a(String var, String var) {
      kotlin.d.b.l.b(var, "emailAddress");
      ((TextView)this.T().findViewById(co.uk.getmondo.c.a.nameTextView)).setText((CharSequence)var);
      ((TextView)this.T().findViewById(co.uk.getmondo.c.a.emailTextView)).setText((CharSequence)var);
      if(var != null) {
         boolean var;
         if(!kotlin.h.j.a((CharSequence)var)) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            Drawable var = co.uk.getmondo.common.ui.a.b.a(this.R().a(var), this.S(), (Typeface)null, false, 6, (Object)null);
            ((ImageView)this.T().findViewById(co.uk.getmondo.c.a.avatarImageView)).setImageDrawable(var);
         }
      }

   }

   public void a(String var, String var, boolean var) {
      kotlin.d.b.l.b(var, "title");
      kotlin.d.b.l.b(var, "subtitle");
      this.b(false);
      ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerTitle)).setText((CharSequence)var);
      ((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle)).setText((CharSequence)var);
      int var;
      if(var) {
         var = 2131362423;
      } else {
         var = 2131362422;
      }

      ((Button)this.a(co.uk.getmondo.c.a.migrationBannerButton)).setText((CharSequence)this.getString(var));
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.migrationBannerSubtitle)));
      ae.a((View)((ConstraintLayout)this.a(co.uk.getmondo.c.a.migrationBanner)));
   }

   public final void a(boolean var) {
      View var = this.a(co.uk.getmondo.c.a.bottomNavShadow);
      byte var;
      if(var) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }

   public n b() {
      com.b.b.c var = this.h;
      kotlin.d.b.l.a(var, "startSubject");
      return (n)var;
   }

   public void b(int var) {
      co.uk.getmondo.common.f.a var = this.V();
      if(var != null) {
         var.b(var);
      } else {
         super.b(var);
         d.a.a.d("Tried to show error but couldn't find current fragment", new Object[0]);
      }

   }

   public void b(co.uk.getmondo.payments.send.data.a.g var) {
      kotlin.d.b.l.b(var, "pendingPayment");
      this.startActivityForResult(PeerToPeerIntroActivity.a((Context)this, t.a, var), 1);
   }

   public void b(String var) {
      kotlin.d.b.l.b(var, "message");
      co.uk.getmondo.common.f.a var = this.V();
      if(var != null) {
         var.d(var);
      } else {
         super.b(var);
         d.a.a.d("Tried to show error but couldn't find current fragment", new Object[0]);
      }

   }

   public void b(String var, String var) {
      kotlin.d.b.l.b(var, "sortCode");
      kotlin.d.b.l.b(var, "accountNumber");
      TextView var = (TextView)this.T().findViewById(co.uk.getmondo.c.a.accountTextView);
      ab var = ab.a;
      Object[] var = new Object[]{var, var};
      var = String.format("%s  •  %s", Arrays.copyOf(var, var.length));
      kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
      var.setText((CharSequence)var);
      ((TextView)this.T().findViewById(co.uk.getmondo.c.a.accountTextView)).setVisibility(0);
      ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).getMenu().findItem(2131821775).setTitle(2131362489);
      MenuItem var = ((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821770);
      var.setTitle(2131362488);
      var.setIcon(2130837861);
      var = ((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821771);
      var.setEnabled(true);
      var.setVisible(true);
   }

   public void c() {
      this.startActivity(HelpActivity.c.a((Context)this));
   }

   public void d() {
      this.startActivityForResult(PeerToPeerIntroActivity.a((Context)this, t.b), 1);
   }

   public void d(String var) {
      this.startActivity(IdentityVerificationSddActivity.a((Context)this, co.uk.getmondo.signup.identity_verification.sdd.j.b, var));
   }

   public void e() {
      co.uk.getmondo.common.activities.a.a(this, "https://community.monzo.com", 0, false, 6, (Object)null);
   }

   public void e(String var) {
      this.startActivity(IdentityVerificationSddActivity.a((Context)this, co.uk.getmondo.signup.identity_verification.sdd.j.c, var).addFlags(268468224));
   }

   public void f() {
      (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)this, 2131689487)).a().a((Context)this, Uri.parse("https://monzo.com/faq/in-app/android/"));
   }

   public void g() {
      q var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("intercomService");
      }

      var.a();
   }

   public n h() {
      com.b.b.c var = this.h;
      kotlin.d.b.l.a(var, "startSubject");
      return (n)var;
   }

   public n i() {
      n var = com.b.a.b.a.a.b.a((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView));
      kotlin.d.b.l.a(var, "RxBottomNavigationView.itemSelections(this)");
      var = var.map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "bottomNavigationView.ite…urceId(menuItem.itemId) }");
      return var;
   }

   public n j() {
      n var = com.b.a.c.b.a(((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821768)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxMenuItem.clicks(this).map(VoidToUnit)");
      var = var.doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            HomeActivity.this.U();
         }
      }));
      kotlin.d.b.l.a(var, "navigationDrawer.menu.fi…oOnNext { closeDrawer() }");
      return var;
   }

   public n k() {
      n var = com.b.a.c.b.a(((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821769)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxMenuItem.clicks(this).map(VoidToUnit)");
      var = var.doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            HomeActivity.this.U();
         }
      }));
      kotlin.d.b.l.a(var, "navigationDrawer.menu.fi…oOnNext { closeDrawer() }");
      return var;
   }

   public View m() {
      View var = this.findViewById(2131821303);
      if(var == null) {
         var = this.findViewById(2131820954);
      }

      return var;
   }

   protected void onActivityResult(int var, int var, Intent var) {
      switch(var) {
      case 1:
         if(var == 0) {
            ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).post((Runnable)(new Runnable() {
               public final void run() {
                  BottomNavigationView var = (BottomNavigationView)HomeActivity.this.a(co.uk.getmondo.c.a.bottomNavigationView);
                  g var = HomeActivity.this.n;
                  if(var == null) {
                     kotlin.d.b.l.a();
                  }

                  var.setSelectedItemId(var.f);
               }
            }));
         } else {
            t var = PeerToPeerIntroActivity.a(var);
            if(kotlin.d.b.l.a(var, t.b)) {
               ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).post((Runnable)(new Runnable() {
                  public final void run() {
                     ((BottomNavigationView)HomeActivity.this.a(co.uk.getmondo.c.a.bottomNavigationView)).setSelectedItemId(g.d.f);
                  }
               }));
            } else if(kotlin.d.b.l.a(var, t.a)) {
               co.uk.getmondo.payments.send.data.a.g var = PeerToPeerIntroActivity.b(var);
               if(var == null) {
                  kotlin.d.b.l.a();
               }

               kotlin.d.b.l.a(var, "PeerToPeerIntroActivity.getPendingPayment(data)!!");
               this.a(var);
            }
         }
         break;
      default:
         super.onActivityResult(var, var, var);
      }

   }

   public void onBackPressed() {
      if(((DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout)).g(8388611)) {
         ((DrawerLayout)this.a(co.uk.getmondo.c.a.drawerLayout)).f(8388611);
      } else {
         super.onBackPressed();
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034177);
      this.l().a(this);
      if(var != null) {
         this.k = true;
         Serializable var = var.getSerializable("KEY_CURRENT_SCREEN");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.Screen");
         }

         g var = (g)var;
         ((BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView)).setSelectedItemId(var.f);
      } else {
         Intent var = this.getIntent();
         kotlin.d.b.l.a(var, "intent");
         this.a(var);
      }

      HomeActivity.a var = f;
      BottomNavigationView var = (BottomNavigationView)this.a(co.uk.getmondo.c.a.bottomNavigationView);
      kotlin.d.b.l.a(var, "bottomNavigationView");
      var.a(var);
      c var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("homePresenter");
      }

      var.a((c.a)this);
   }

   protected void onDestroy() {
      c var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("homePresenter");
      }

      var.b();
      super.onDestroy();
   }

   protected void onNewIntent(Intent var) {
      kotlin.d.b.l.b(var, "intent");
      super.onNewIntent(var);
      this.setIntent(var);
      this.a(var);
   }

   protected void onResume() {
      super.onResume();
      if(this.getIntent().hasExtra("KEY_MONZO_ME_DATA")) {
         f var = (f)this.getIntent().getParcelableExtra("KEY_MONZO_ME_DATA");
         this.g.a((Object)var);
         this.getIntent().removeExtra("KEY_MONZO_ME_DATA");
      }

   }

   protected void onSaveInstanceState(Bundle var) {
      kotlin.d.b.l.b(var, "outState");
      super.onSaveInstanceState(var);
      var.putSerializable("KEY_CURRENT_SCREEN", (Serializable)this.n);
   }

   protected void onStart() {
      super.onStart();
      this.h.a((Object)kotlin.n.a);
   }

   public n v() {
      n var = com.b.a.c.b.a(((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821770)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxMenuItem.clicks(this).map(VoidToUnit)");
      var = var.doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            HomeActivity.this.U();
         }
      }));
      kotlin.d.b.l.a(var, "navigationDrawer.menu.fi…oOnNext { closeDrawer() }");
      return var;
   }

   public n w() {
      n var = com.b.a.c.b.a(((NavigationView)this.a(co.uk.getmondo.c.a.navigationDrawer)).getMenu().findItem(2131821771)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxMenuItem.clicks(this).map(VoidToUnit)");
      var = var.doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            HomeActivity.this.U();
         }
      }));
      kotlin.d.b.l.a(var, "navigationDrawer.menu.fi…oOnNext { closeDrawer() }");
      return var;
   }

   public void x() {
      SettingsActivity.c.a((Context)this);
   }

   public void y() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362649), this.getString(2131362639), false).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
   }

   public void z() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362649), this.getString(2131362637), false).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u0010\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u001aH\u0007J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J,\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010#\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006%"},
      d2 = {"Lco/uk/getmondo/main/HomeActivity$Companion;", "", "()V", "KEY_ANALYTIC_FROM_SHORTCUT", "", "KEY_CURRENT_SCREEN", "KEY_ID_FROM_SHORTCUT", "KEY_MONZO_ME_DATA", "KEY_YEAR_MONTH", "REQUEST_P2P_ONBOARDING", "", "TAG_ERROR_DIALOG_FRAGMENT", "TAG_ERROR_P2P_BLOCKED", "buildAppShortcutIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "screen", "Lco/uk/getmondo/main/Screen;", "shortcut", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "buildIntent", "buildIntentForScreen", "buildNotificationIntent", "buildSpendingIntent", "yearMonth", "Lorg/threeten/bp/YearMonth;", "disableShiftMode", "", "view", "Landroid/support/design/widget/BottomNavigationView;", "start", "username", "amount", "Lco/uk/getmondo/model/Amount;", "notes", "startWithoutClear", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      private final void a(BottomNavigationView param1) {
         // $FF: Couldn't be decompiled
      }

      public final Intent a(Context var, g var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "screen");
         Intent var = (new Intent(var, HomeActivity.class)).putExtra("KEY_CURRENT_SCREEN", var.e).setFlags(67108864);
         kotlin.d.b.l.a(var, "Intent(context, HomeActi….FLAG_ACTIVITY_CLEAR_TOP)");
         return var;
      }

      public final Intent a(Context var, g var, co.uk.getmondo.common.a.b var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "screen");
         kotlin.d.b.l.b(var, "shortcut");
         Intent var = (new Intent("android.intent.action.MAIN", Uri.EMPTY, var, HomeActivity.class)).putExtra("KEY_CURRENT_SCREEN", var.e).putExtra("KEY_ID_FROM_SHORTCUT", var.a()).putExtra("KEY_ANALYTIC_FROM_SHORTCUT", var.d()).setFlags('耀');
         kotlin.d.b.l.a(var, "Intent(Intent.ACTION_MAI…FLAG_ACTIVITY_CLEAR_TASK)");
         return var;
      }

      public final Intent a(Context var, YearMonth var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "yearMonth");
         Intent var = (new Intent(var, HomeActivity.class)).putExtra("KEY_CURRENT_SCREEN", g.b.e).putExtra("KEY_YEAR_MONTH", (Serializable)var).setFlags(67108864);
         kotlin.d.b.l.a(var, "Intent(context, HomeActi….FLAG_ACTIVITY_CLEAR_TOP)");
         return var;
      }

      public final void a(Context var) {
         kotlin.d.b.l.b(var, "context");
         var.startActivity(((HomeActivity.a)this).b(var).addFlags(268533760));
      }

      public final void a(Context var, String var, co.uk.getmondo.d.c var, String var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "username");
         Intent var = new Intent(var, HomeActivity.class);
         var.putExtra("KEY_MONZO_ME_DATA", (Parcelable)(new f(var, var, var)));
         var.addFlags(335544320);
         var.startActivity(var);
      }

      public final Intent b(Context var) {
         kotlin.d.b.l.b(var, "context");
         return new Intent(var, HomeActivity.class);
      }

      public final Intent c(Context var) {
         kotlin.d.b.l.b(var, "context");
         Intent var = new Intent(var, HomeActivity.class);
         var.addFlags(335544320);
         var.putExtra("KEY_CURRENT_SCREEN", g.a.e);
         return var;
      }
   }
}
