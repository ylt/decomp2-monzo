package co.uk.getmondo.main;

import android.support.v4.app.Fragment;

public enum g {
   a(1, 2131821772, co.uk.getmondo.feed.e.class),
   b(2, 2131821773, co.uk.getmondo.spending.b.class),
   c(3, 2131821774, co.uk.getmondo.card.a.class),
   d(4, 2131821775, co.uk.getmondo.c.a.class);

   final int e;
   int f;
   Class g;

   private g(int var, int var, Class var) {
      this.e = var;
      this.f = var;
      this.g = var;
   }

   static g a(int var) {
      g[] var = values();
      int var = var.length;
      int var = 0;

      g var;
      while(true) {
         if(var >= var) {
            var = null;
            break;
         }

         var = var[var];
         if(var.e == var) {
            break;
         }

         ++var;
      }

      return var;
   }

   public static g b(int var) {
      g[] var = values();
      int var = var.length;
      int var = 0;

      g var;
      while(true) {
         if(var >= var) {
            var = null;
            break;
         }

         var = var[var];
         if(var.f == var) {
            break;
         }

         ++var;
      }

      return var;
   }

   Fragment a() {
      Object var;
      try {
         Fragment var = (Fragment)this.g.newInstance();
         return var;
      } catch (InstantiationException var) {
         var = var;
      } catch (IllegalAccessException var) {
         var = var;
      }

      throw new RuntimeException((Throwable)var);
   }
}
