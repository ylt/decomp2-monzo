package co.uk.getmondo.main;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.api.model.service_status.ServiceStatusIncident;
import co.uk.getmondo.api.model.sign_up.MigrationInfo;
import co.uk.getmondo.api.model.signup.SignupInfo;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.x;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ag;
import co.uk.getmondo.d.ah;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.feed.a.t;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.q;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001/B£\u0001\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020'¢\u0006\u0002\u0010(J\u0010\u0010+\u001a\u00020,2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010-\u001a\u00020,2\u0006\u0010.\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020'X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00060"},
   d2 = {"Lco/uk/getmondo/main/HomePresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/main/HomePresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "serviceStatusApi", "Lco/uk/getmondo/api/ServiceStatusApi;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "peerToPeerRepository", "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "newsManager", "Lco/uk/getmondo/news/NewsManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "sddMigrationStorage", "Lco/uk/getmondo/common/SddMigrationStorage;", "identityVerificationApi", "Lco/uk/getmondo/api/IdentityVerificationApi;", "sddDismissibleState", "Lco/uk/getmondo/main/SddDismissibleState;", "peerManager", "Lco/uk/getmondo/feed/data/PeerManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "signupStatusManager", "Lco/uk/getmondo/signup/status/SignupStatusManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/ServiceStatusApi;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/news/NewsManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/SddMigrationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/main/SddDismissibleState;Lco/uk/getmondo/feed/data/PeerManager;Lco/uk/getmondo/common/FeatureFlagsStorage;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/signup/status/SignupStatusManager;)V", "migrationInfo", "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "displayMigrationBanner", "", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private MigrationInfo c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.api.b.a h;
   private final co.uk.getmondo.common.a i;
   private final ServiceStatusApi j;
   private final co.uk.getmondo.payments.send.data.h k;
   private final p l;
   private final PeerToPeerRepository m;
   private final co.uk.getmondo.news.d n;
   private final co.uk.getmondo.background_sync.d o;
   private final x p;
   private final IdentityVerificationApi q;
   private final h r;
   private final t s;
   private final o t;
   private final co.uk.getmondo.card.c u;
   private final co.uk.getmondo.signup.status.b v;

   public c(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.a var, ServiceStatusApi var, co.uk.getmondo.payments.send.data.h var, p var, PeerToPeerRepository var, co.uk.getmondo.news.d var, co.uk.getmondo.background_sync.d var, x var, IdentityVerificationApi var, h var, t var, o var, co.uk.getmondo.card.c var, co.uk.getmondo.signup.status.b var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "accountService");
      l.b(var, "apiErrorHandler");
      l.b(var, "userInteractor");
      l.b(var, "analyticsService");
      l.b(var, "serviceStatusApi");
      l.b(var, "userSettingsRepository");
      l.b(var, "userSettingsStorage");
      l.b(var, "peerToPeerRepository");
      l.b(var, "newsManager");
      l.b(var, "syncManager");
      l.b(var, "sddMigrationStorage");
      l.b(var, "identityVerificationApi");
      l.b(var, "sddDismissibleState");
      l.b(var, "peerManager");
      l.b(var, "featureFlagsStorage");
      l.b(var, "cardManager");
      l.b(var, "signupStatusManager");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
      this.n = var;
      this.o = var;
      this.p = var;
      this.q = var;
      this.r = var;
      this.s = var;
      this.t = var;
      this.u = var;
      this.v = var;
   }

   private final void a(MigrationInfo var) {
      SignupInfo.Status var = var.e();
      SignupInfo.Stage var = var.f();
      if(!var.b()) {
         ((c.a)this.a).a(var.c(), var.d(), false);
         this.i.a(Impression.Companion.aX());
      } else if(l.a(var, SignupInfo.Status.NOT_STARTED)) {
         ((c.a)this.a).a(var.c(), var.d(), true);
         this.i.a(Impression.Companion.ba());
      } else if(l.a(var, SignupInfo.Stage.CARD_ACTIVATION)) {
         ((c.a)this.a).M();
      } else {
         ((c.a)this.a).L();
         this.i.a(Impression.Companion.bg());
      }

   }

   public void a(final c.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      ak var = this.f.b();
      if(var == null) {
         l.a();
      }

      final ac var = var.d();
      co.uk.getmondo.d.a var = var.c();
      Boolean var = co.uk.getmondo.a.b;
      l.a(var, "BuildConfig.GHOST");
      if(var.booleanValue()) {
         if(var == null) {
            l.a();
         }

         var.a(var.d());
      }

      final boolean var;
      if(var != null && var.e()) {
         var = true;
      } else {
         var = false;
      }

      if(var) {
         ad var = (ad)var;
         if(var == null) {
            l.a();
         }

         var.b(var.g(), var.j());
      }

      if(var == null) {
         l.a();
      }

      var.a(var.a(), var.d());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.h.a().b(this.e).a(this.d).a((io.reactivex.c.g)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = c.this.g;
            l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      l.a(var, "userInteractor.refreshAc…ndleError(error, view) })");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = this.u.c().b(this.e).a(this.d).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = c.this.g;
            l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      l.a(var, "cardManager.syncCard()\n …view) }\n                )");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = this.n.a().b(this.e).a(this.d).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.news.c varx) {
            c.a var = var;
            l.a(varx, "it");
            var.a(varx);
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = c.this.g;
            l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      l.a(var, "newsManager.unreadNewsIt…ndleError(error, view) })");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.h().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(n var) {
            l.b(var, "it");
            return c.this.j.getUnresolvedIncidents().b((io.reactivex.c.h)null.a).filter((q)null.a).toList().b(c.this.e).e().f();
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List varx) {
            boolean var;
            if(!((Collection)varx).isEmpty()) {
               var = true;
            } else {
               var = false;
            }

            if(var) {
               c.a var = var;
               Object var = varx.get(0);
               l.a(var, "incidents[0]");
               var.a((ServiceStatusIncident)var);
            } else {
               var.E();
            }

         }
      }));
      l.a(var, "view.onRefreshIncidents(…      }\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.b().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n varx) {
            l.b(varx, "it");
            return c.this.k.a().b(c.this.e).a(c.this.d).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = c.this.g;
                  l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "view.onRefreshUserSettin…ibe({}, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.p.a().distinctUntilChanged().filter((q)null.a).switchMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(final ag varx) {
            l.b(varx, "sddMigration");
            return c.this.q.status(SignupSource.SDD_MIGRATION).a(3L).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(IdentityVerification varxx) {
                  l.b(varxx, "identityVerification");
                  return new kotlin.h(varx, varxx);
               }
            })).b(c.this.e).a(c.this.d).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = c.this.g;
                  l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            }));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            ag var = (ag)varx.c();
            IdentityVerification var = (IdentityVerification)varx.d();
            String var = var.d();
            IdentityVerification.Status var = var.e();
            boolean var;
            if(var != IdentityVerification.Status.PENDING_SUBMISSION && var != IdentityVerification.Status.NOT_STARTED) {
               var = false;
            } else {
               var = true;
            }

            ah var = var.a();
            if(var != null) {
               switch(d.a[var.ordinal()]) {
               case 1:
                  if(!c.this.r.a() && var) {
                     var.d(var);
                     c.this.r.a(true);
                  }
                  break;
               case 2:
                  if(var) {
                     var.e(var);
                  } else if(var == IdentityVerification.Status.PENDING_REVIEW || var == IdentityVerification.Status.BLOCKED) {
                     var.H();
                  }
               }
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "sddMigrationStorage.sddM…     }, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.D().observeOn(this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(f varx) {
            var.F();
         }
      })).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(final f varx) {
            l.b(varx, "monzoMeData");
            return c.this.m.a(varx.a()).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(aa varxx) {
                  l.b(varxx, "peer");
                  return new kotlin.h(varxx, varx);
               }
            })).b(c.this.e).a(c.this.d).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.G();
                  d.a.a.a(varx, "Failed opening a monzo.me link", new Object[0]);
                  if(varx instanceof PeerToPeerRepository.a) {
                     var.z();
                  } else if(varx instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                     var.A();
                  } else {
                     var.B();
                  }

               }
            })).f().onErrorResumeNext((r)io.reactivex.n.empty());
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            var.G();
            aa var = (aa)varx.a();
            f var = (f)varx.b();
            if(l.a(var.b(), var.a())) {
               var.y();
            } else {
               c.a varx;
               if(l.a(c.this.l.a(), co.uk.getmondo.payments.send.data.a.d.b)) {
                  varx = var;
                  l.a(var, "peer");
                  varx.b(var.a(var));
               } else if(l.a(c.this.l.a(), co.uk.getmondo.payments.send.data.a.d.a)) {
                  varx = var;
                  l.a(var, "peer");
                  varx.a(var.a(var));
               }
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var, "view.onPendingMonzoMeDat…     }, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            c.this.i.a(Impression.Companion.V());
            var.x();
         }
      }));
      l.a(var, "view.onSettingsClicked()…tings()\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      io.reactivex.b.a var = this.b;
      var = var.k().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            c.this.i.a(Impression.Companion.a(Impression.OpenedCommunityFrom.FROM_MAIN_NAV));
            var.e();
         }
      }));
      l.a(var, "view.onCommunityClicked(…unity()\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.w().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            c.this.i.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.MAIN_NAV, (String)null, (String)null, 6, (Object)null));
            var.g();
         }
      }));
      l.a(var, "view.onChatClicked()\n   …nChat()\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.v().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            if(var) {
               var.f();
            } else {
               var.c();
            }

         }
      }));
      l.a(var, "view.onHelpClicked()\n   …      }\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = var.a().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var) {
            c.this.f.e();
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            var.u();
         }
      }));
      l.a(var, "view.onGhostLogOutClicke…ibe { view.openSplash() }");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = this.o.a().b(this.e).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = c.this.g;
            l.a(varx, "throwable");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      l.a(var, "syncManager.syncFeedAndB…Error(throwable, view) })");
      io.reactivex.rxkotlin.a.a(var, var);
      var = this.b;
      var = this.s.a().subscribe((io.reactivex.c.g)null.a, (io.reactivex.c.g)null.a);
      l.a(var, "peerManager.keepPeersEnr…iching peers\", error)) })");
      io.reactivex.rxkotlin.a.a(var, var);
      final io.reactivex.n var = var.i().replay().a();
      var = this.b;
      var = var.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(g varx) {
            if(l.a(varx, g.d)) {
               boolean varx;
               if(var && !c.this.t.b().a()) {
                  varx = false;
               } else {
                  varx = true;
               }

               if(varx) {
                  co.uk.getmondo.payments.send.data.a.d var = c.this.l.a();
                  if(var != null) {
                     switch(d.b[var.ordinal()]) {
                     case 1:
                        var.a(g.d);
                        break;
                     case 2:
                        var.d();
                        break;
                     case 3:
                        var.C();
                     }
                  }
               } else {
                  var.a(varx);
               }
            } else {
               c.a var = var;
               l.a(varx, "screen");
               var.a(varx);
            }

         }
      }));
      l.a(var, "onScreenChanged\n        …      }\n                }");
      io.reactivex.rxkotlin.a.a(var, var);
      if(!co.uk.getmondo.a.c.booleanValue() && !this.v.a()) {
         var = this.b;
         var = var.J().switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.n a(n var) {
               l.b(var, "it");
               io.reactivex.rxkotlin.b var = io.reactivex.rxkotlin.b.a;
               io.reactivex.n var = c.this.v.d().subscribeOn(c.this.e);
               l.a(var, "signupStatusManager.migr….subscribeOn(ioScheduler)");
               io.reactivex.n var = var;
               l.a(var, "onScreenChanged");
               return var.a(var, var);
            }
         })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h varx) {
               MigrationInfo var = (MigrationInfo)varx.c();
               g var = (g)varx.d();
               c.this.c = var;
               if(var.a() && l.a(var, g.a)) {
                  c var = c.this;
                  l.a(var, "migrationInfo");
                  var.a(var);
               } else {
                  var.N();
               }

            }
         }), (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(Throwable varx) {
               co.uk.getmondo.common.e.a var = c.this.g;
               l.a(varx, "it");
               var.a(varx, (co.uk.getmondo.common.e.a.a)var);
            }
         }));
         l.a(var, "view.onRefreshMigrationB….handleError(it, view) })");
         io.reactivex.rxkotlin.a.a(var, var);
         var = this.b;
         io.reactivex.b.b var = var.K().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(n varx) {
               MigrationInfo var = c.this.c;
               if(var != null) {
                  if(var.b() && l.a(var.e(), SignupInfo.Status.NOT_STARTED) ^ true) {
                     var.Q();
                     c.this.i.a(Impression.Companion.bh());
                  } else if(var.b()) {
                     var.P();
                  } else {
                     var.O();
                  }
               }

            }
         }));
         l.a(var, "view.onMigrationBannerCl…  }\n                    }");
         io.reactivex.rxkotlin.a.a(var, var);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\bH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\bH&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\b\u0010\u0015\u001a\u00020\u0004H&J\b\u0010\u0016\u001a\u00020\u0004H&J\b\u0010\u0017\u001a\u00020\u0004H&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&J\b\u0010\u001b\u001a\u00020\u0004H&J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001eH&J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001eH&J\u0012\u0010 \u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\"H&J\u0012\u0010#\u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\"H&J\b\u0010$\u001a\u00020\u0004H&J\b\u0010%\u001a\u00020\u0004H&J\b\u0010&\u001a\u00020\u0004H&J\u001a\u0010'\u001a\u00020\u00042\b\u0010(\u001a\u0004\u0018\u00010\"2\u0006\u0010)\u001a\u00020\"H&J\b\u0010*\u001a\u00020\u0004H&J\b\u0010+\u001a\u00020\u0004H&J\b\u0010,\u001a\u00020\u0004H&J\b\u0010-\u001a\u00020\u0004H&J\b\u0010.\u001a\u00020\u0004H&J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020\"H&J\b\u00101\u001a\u00020\u0004H&J \u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\"2\u0006\u00104\u001a\u00020\"2\u0006\u00105\u001a\u000206H&J\b\u00107\u001a\u00020\u0004H&J\u0010\u00108\u001a\u00020\u00042\u0006\u00109\u001a\u00020:H&J\u0010\u0010;\u001a\u00020\u00042\u0006\u0010<\u001a\u00020=H&J\b\u0010>\u001a\u00020\u0004H&J\u0018\u0010?\u001a\u00020\u00042\u0006\u0010@\u001a\u00020\"2\u0006\u0010A\u001a\u00020\"H&J\u0010\u0010B\u001a\u00020\u00042\u0006\u0010C\u001a\u00020\u0013H&¨\u0006D"},
      d2 = {"Lco/uk/getmondo/main/HomePresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideLoading", "", "hideMigrationBanner", "hideOutageWarning", "onChatClicked", "Lio/reactivex/Observable;", "onCommunityClicked", "onGhostLogOutClicked", "onHelpClicked", "onMigrationBannerClicked", "onPendingMonzoMeDataChanged", "Lco/uk/getmondo/main/MonzoMeData;", "onRefreshIncidents", "onRefreshMigrationBanner", "onRefreshUserSettings", "onScreenChanged", "Lco/uk/getmondo/main/Screen;", "onSettingsClicked", "openChat", "openCommunity", "openCurrentAccountComing", "openCurrentAccountHere", "openFaqs", "openHelp", "openP2pOnboardingFromContacts", "openP2pOnboardingFromMonzoMeDeepLink", "pendingPayment", "Lco/uk/getmondo/payments/send/data/model/PeerPayment;", "openPaymentInfo", "openSddMigrationDismissible", "rejectionNote", "", "openSddMigrationPersistent", "openSettings", "openSignup", "openVerificationPending", "setProfileInformation", "nameToDisplay", "emailAddress", "showActivateCardBanner", "showCannotPayYourself", "showContactHasP2pDisabled", "showContactNotOnMonzo", "showContinueSignupBanner", "showGhostBanner", "email", "showLoading", "showMigrationBanner", "title", "subtitle", "signupAllowed", "", "showMonzoMeGenericError", "showNews", "newsItem", "Lco/uk/getmondo/news/NewsItem;", "showOutageWarning", "incident", "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;", "showP2pBlocked", "showRetailUi", "sortCode", "accountNumber", "showScreen", "screen", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      io.reactivex.n D();

      void E();

      void F();

      void G();

      void H();

      io.reactivex.n J();

      io.reactivex.n K();

      void L();

      void M();

      void N();

      void O();

      void P();

      void Q();

      io.reactivex.n a();

      void a(ServiceStatusIncident var);

      void a(g var);

      void a(co.uk.getmondo.news.c var);

      void a(co.uk.getmondo.payments.send.data.a.g var);

      void a(String var);

      void a(String var, String var);

      void a(String var, String var, boolean var);

      io.reactivex.n b();

      void b(co.uk.getmondo.payments.send.data.a.g var);

      void b(String var, String var);

      void c();

      void d();

      void d(String var);

      void e();

      void e(String var);

      void f();

      void g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();

      void x();

      void y();

      void z();
   }
}
