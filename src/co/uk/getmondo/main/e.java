package co.uk.getmondo.main;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.x;
import co.uk.getmondo.feed.a.t;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.u;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;
   private final javax.a.a n;
   private final javax.a.a o;
   private final javax.a.a p;
   private final javax.a.a q;
   private final javax.a.a r;
   private final javax.a.a s;
   private final javax.a.a t;
   private final javax.a.a u;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                                 if(!a && var == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var;
                                    if(!a && var == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var;
                                       if(!a && var == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var;
                                          if(!a && var == null) {
                                             throw new AssertionError();
                                          } else {
                                             this.n = var;
                                             if(!a && var == null) {
                                                throw new AssertionError();
                                             } else {
                                                this.o = var;
                                                if(!a && var == null) {
                                                   throw new AssertionError();
                                                } else {
                                                   this.p = var;
                                                   if(!a && var == null) {
                                                      throw new AssertionError();
                                                   } else {
                                                      this.q = var;
                                                      if(!a && var == null) {
                                                         throw new AssertionError();
                                                      } else {
                                                         this.r = var;
                                                         if(!a && var == null) {
                                                            throw new AssertionError();
                                                         } else {
                                                            this.s = var;
                                                            if(!a && var == null) {
                                                               throw new AssertionError();
                                                            } else {
                                                               this.t = var;
                                                               if(!a && var == null) {
                                                                  throw new AssertionError();
                                                               } else {
                                                                  this.u = var;
                                                               }
                                                            }
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new e(var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public c a() {
      return (c)b.a.c.a(this.b, new c((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.common.accounts.d)this.e.b(), (co.uk.getmondo.common.e.a)this.f.b(), (co.uk.getmondo.api.b.a)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (ServiceStatusApi)this.i.b(), (co.uk.getmondo.payments.send.data.h)this.j.b(), (p)this.k.b(), (PeerToPeerRepository)this.l.b(), (co.uk.getmondo.news.d)this.m.b(), (co.uk.getmondo.background_sync.d)this.n.b(), (x)this.o.b(), (IdentityVerificationApi)this.p.b(), (h)this.q.b(), (t)this.r.b(), (o)this.s.b(), (co.uk.getmondo.card.c)this.t.b(), (co.uk.getmondo.signup.status.b)this.u.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
