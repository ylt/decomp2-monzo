package co.uk.getmondo.main;

import co.uk.getmondo.common.q;

public final class b implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!b.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public b(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new b(var, var, var);
   }

   public void a(HomeActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.b = (c)this.b.b();
         var.c = (q)this.c.b();
         var.e = (co.uk.getmondo.common.a.c)this.d.b();
      }
   }
}
