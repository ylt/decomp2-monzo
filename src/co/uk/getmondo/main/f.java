package co.uk.getmondo.main;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import co.uk.getmondo.d.aa;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 $2\u00020\u0001:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B!\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0006HÆ\u0003J+\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\t\u0010\u001f\u001a\u00020\u0006HÖ\u0001J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u0015H\u0016R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000e¨\u0006%"},
   d2 = {"Lco/uk/getmondo/main/MonzoMeData;", "Landroid/os/Parcelable;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "username", "", "amount", "Lco/uk/getmondo/model/Amount;", "notes", "(Ljava/lang/String;Lco/uk/getmondo/model/Amount;Ljava/lang/String;)V", "getAmount", "()Lco/uk/getmondo/model/Amount;", "getNotes", "()Ljava/lang/String;", "getUsername", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toPeerPayment", "Lco/uk/getmondo/payments/send/data/model/PeerPayment;", "peer", "Lco/uk/getmondo/model/Peer;", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public f a(Parcel var) {
         l.b(var, "source");
         return new f(var);
      }

      public f[] a(int var) {
         return new f[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final f.a a = new f.a((kotlin.d.b.i)null);
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final String d;

   public f(Parcel var) {
      l.b(var, "parcel");
      String var = var.readString();
      l.a(var, "parcel.readString()");
      this(var, (co.uk.getmondo.d.c)var.readParcelable(co.uk.getmondo.d.c.class.getClassLoader()), var.readString());
   }

   public f(String var, co.uk.getmondo.d.c var, String var) {
      l.b(var, "username");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final co.uk.getmondo.payments.send.data.a.g a(aa var) {
      l.b(var, "peer");
      co.uk.getmondo.d.c var = this.c;
      if(var == null) {
         co.uk.getmondo.common.i.c var = co.uk.getmondo.common.i.c.a;
         l.a(var, "Currency.GBP");
         var = new co.uk.getmondo.d.c(0L, var);
      }

      String var = this.d;
      if(var == null) {
         var = "";
      }

      return new co.uk.getmondo.payments.send.data.a.g(var, var, var);
   }

   public final String a() {
      return this.b;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof f) {
               f var = (f)var;
               if(l.a(this.b, var.b) && l.a(this.c, var.c) && l.a(this.d, var.d)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.d.c var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "MonzoMeData(username=" + this.b + ", amount=" + this.c + ", notes=" + this.d + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeString(this.b);
      var.writeParcelable((Parcelable)this.c, var);
      var.writeString(this.d);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/main/MonzoMeData$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/main/MonzoMeData;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
