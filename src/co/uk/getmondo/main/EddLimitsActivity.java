package co.uk.getmondo.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.settings.LimitsActivity;

public class EddLimitsActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;

   public static Intent a(Context var) {
      return new Intent(var, EddLimitsActivity.class);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034163);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a(Impression.V());
   }

   @OnClick({2131820889})
   public void onNotNowClicked() {
      this.finish();
   }

   @OnClick({2131820888})
   public void onViewLimitsClicked() {
      LimitsActivity.a((Context)this);
   }
}
