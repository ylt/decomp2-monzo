package co.uk.getmondo.main;

import android.view.View;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class EddLimitsActivity_ViewBinding implements Unbinder {
   private EddLimitsActivity a;
   private View b;
   private View c;

   public EddLimitsActivity_ViewBinding(final EddLimitsActivity var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131820888, "method 'onViewLimitsClicked'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onViewLimitsClicked();
         }
      });
      var = Utils.findRequiredView(var, 2131820889, "method 'onNotNowClicked'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onNotNowClicked();
         }
      });
   }

   public void unbind() {
      if(this.a == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
