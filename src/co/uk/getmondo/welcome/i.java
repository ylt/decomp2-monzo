package co.uk.getmondo.welcome;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(b.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b.a var) {
      return new i(var);
   }

   public e a() {
      return (e)b.a.c.a(this.b, new e());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
