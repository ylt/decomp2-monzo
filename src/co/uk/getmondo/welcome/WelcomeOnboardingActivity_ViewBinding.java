package co.uk.getmondo.welcome;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class WelcomeOnboardingActivity_ViewBinding implements Unbinder {
   private WelcomeOnboardingActivity a;

   public WelcomeOnboardingActivity_ViewBinding(WelcomeOnboardingActivity var, View var) {
      this.a = var;
      var.viewPager = (ViewPager)Utils.findRequiredViewAsType(var, 2131820808, "field 'viewPager'", ViewPager.class);
      var.mainActionButton = (Button)Utils.findRequiredViewAsType(var, 2131820809, "field 'mainActionButton'", Button.class);
      var.logInButton = (Button)Utils.findRequiredViewAsType(var, 2131820810, "field 'logInButton'", Button.class);
   }

   public void unbind() {
      WelcomeOnboardingActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.viewPager = null;
         var.mainActionButton = null;
         var.logInButton = null;
      }
   }
}
