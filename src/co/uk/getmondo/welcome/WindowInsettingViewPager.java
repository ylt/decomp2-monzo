package co.uk.getmondo.welcome;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowInsets;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.ViewGroup.LayoutParams;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0017B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\b\u0010\u0010\u001a\u00020\u000fH\u0014J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0014R\"\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b@BX\u0082\u000e¢\u0006\b\n\u0000\"\u0004\b\n\u0010\u000b¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/welcome/WindowInsettingViewPager;", "Landroid/support/v4/view/ViewPager;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "value", "Landroid/view/WindowInsets;", "lastInsets", "setLastInsets", "(Landroid/view/WindowInsets;)V", "checkLayoutParams", "", "p", "Landroid/view/ViewGroup$LayoutParams;", "generateDefaultLayoutParams", "generateLayoutParams", "onMeasure", "", "widthMeasureSpec", "", "heightMeasureSpec", "LayoutParams", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class WindowInsettingViewPager extends ViewPager {
   private WindowInsets d;

   public WindowInsettingViewPager(Context var) {
      this(var, (AttributeSet)null, 2, (kotlin.d.b.i)null);
   }

   public WindowInsettingViewPager(Context var, AttributeSet var) {
      l.b(var, "context");
      super(var, var);
      this.setOnApplyWindowInsetsListener((OnApplyWindowInsetsListener)(new OnApplyWindowInsetsListener() {
         public final WindowInsets onApplyWindowInsets(View var, WindowInsets var) {
            WindowInsettingViewPager.this.setLastInsets(var);
            return var.consumeSystemWindowInsets();
         }
      }));
   }

   // $FF: synthetic method
   public WindowInsettingViewPager(Context var, AttributeSet var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      this(var, var);
   }

   private final void setLastInsets(WindowInsets var) {
      this.d = var;
      this.requestLayout();
   }

   protected boolean checkLayoutParams(LayoutParams var) {
      boolean var;
      if(var instanceof WindowInsettingViewPager.a && super.checkLayoutParams(var)) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   protected LayoutParams generateDefaultLayoutParams() {
      return (LayoutParams)(new WindowInsettingViewPager.a());
   }

   public LayoutParams generateLayoutParams(AttributeSet var) {
      return (LayoutParams)(new android.support.v4.view.ViewPager.c(this.getContext(), var));
   }

   protected LayoutParams generateLayoutParams(LayoutParams var) {
      return this.generateDefaultLayoutParams();
   }

   protected void onMeasure(int var, int var) {
      int var = this.getChildCount();

      for(int var = 0; var < var; ++var) {
         View var = this.getChildAt(var);
         if(var.getVisibility() != 8) {
            WindowInsets var = this.d;
            if(var != null && this.getFitsSystemWindows()) {
               if(var.getFitsSystemWindows()) {
                  var.dispatchApplyWindowInsets(this.d);
               } else {
                  LayoutParams var = var.getLayoutParams();
                  if(var == null) {
                     throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.welcome.WindowInsettingViewPager.LayoutParams");
                  }

                  WindowInsettingViewPager.a var = (WindowInsettingViewPager.a)var;
                  int var = var.getPaddingLeft();
                  int var = var.a();
                  int var = var.getPaddingTop();
                  int var = var.b();
                  int var = var.getPaddingRight();
                  int var = var.d();
                  int var = var.getPaddingBottom();
                  int var = var.c();
                  var.a(var.getSystemWindowInsetLeft());
                  var.b(var.getSystemWindowInsetTop());
                  var.d(var.getSystemWindowInsetRight());
                  var.c(var.getSystemWindowInsetBottom());
                  var.setPadding(var.a() + (var - var), var.b() + (var - var), var.d() + (var - var), var.c() + (var - var));
               }
            }
         }
      }

      super.onMeasure(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001a\u0010\u0011\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000b\"\u0004\b\u0013\u0010\rR\u001a\u0010\u0014\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000b\"\u0004\b\u0016\u0010\r¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/welcome/WindowInsettingViewPager$LayoutParams;", "Landroid/support/v4/view/ViewPager$LayoutParams;", "()V", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "offsetBottom", "", "getOffsetBottom", "()I", "setOffsetBottom", "(I)V", "offsetLeft", "getOffsetLeft", "setOffsetLeft", "offsetRight", "getOffsetRight", "setOffsetRight", "offsetTop", "getOffsetTop", "setOffsetTop", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends android.support.v4.view.ViewPager.c {
      private int g;
      private int h;
      private int i;
      private int j;

      public final int a() {
         return this.g;
      }

      public final void a(int var) {
         this.g = var;
      }

      public final int b() {
         return this.h;
      }

      public final void b(int var) {
         this.h = var;
      }

      public final int c() {
         return this.i;
      }

      public final void c(int var) {
         this.i = var;
      }

      public final int d() {
         return this.j;
      }

      public final void d(int var) {
         this.j = var;
      }
   }
}
