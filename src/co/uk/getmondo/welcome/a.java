package co.uk.getmondo.welcome;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator.AnimatorListener;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnApplyWindowInsetsListener;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.VideoImageView;
import com.airbnb.lottie.LottieAnimationView;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/welcome/VideoViewPage;", "Lco/uk/getmondo/common/pager/Page$CustomView;", "viewPager", "Landroid/support/v4/view/ViewPager;", "(Landroid/support/v4/view/ViewPager;)V", "videoImageView", "Lco/uk/getmondo/common/ui/VideoImageView;", "createView", "Landroid/view/View;", "destroyView", "", "getOnPageViewedImpression", "Lco/uk/getmondo/api/model/tracking/Impression;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements co.uk.getmondo.common.pager.f.b {
   private VideoImageView a;
   private final ViewPager b;

   public a(ViewPager var) {
      l.b(var, "viewPager");
      super();
      this.b = var;
   }

   public Impression a() {
      return Impression.Companion.b(1);
   }

   public void b() {
      VideoImageView var = this.a;
      if(var != null) {
         var.a();
      }

      this.a = (VideoImageView)null;
   }

   public View c() {
      View var = LayoutInflater.from(this.b.getContext()).inflate(2131034404, (ViewGroup)this.b, false);
      this.a = (VideoImageView)var.findViewById(2131821660);
      final View var = var.findViewById(2131821661);
      LottieAnimationView var = (LottieAnimationView)var.findViewById(2131821662);
      var.findViewById(2131821659).setOnApplyWindowInsetsListener((OnApplyWindowInsetsListener)null.a);
      if(var.b()) {
         var.setVisibility(8);
      } else {
         var.a((AnimatorListener)(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator varx) {
               l.b(varx, "animation");
               super.onAnimationStart(varx);
               var.setVisibility(8);
            }
         }));
      }

      l.a(var, "view");
      return var;
   }
}
