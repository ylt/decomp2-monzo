package co.uk.getmondo.welcome;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.pager.GenericPagerAdapter;
import co.uk.getmondo.signup_old.SignUpActivity;
import io.reactivex.n;
import java.util.Arrays;
import java.util.List;

public class WelcomeOnboardingActivity extends co.uk.getmondo.common.activities.b implements e.a {
   e a;
   co.uk.getmondo.common.pager.h b;
   @BindView(2131820810)
   Button logInButton;
   @BindView(2131820809)
   Button mainActionButton;
   @BindView(2131820808)
   ViewPager viewPager;

   // $FF: synthetic method
   static Boolean a(WelcomeOnboardingActivity var, Integer var) throws Exception {
      boolean var;
      if(var.viewPager.getCurrentItem() >= var.viewPager.getAdapter().b() - 1) {
         var = true;
      } else {
         var = false;
      }

      return Boolean.valueOf(var);
   }

   // $FF: synthetic method
   static Boolean a(WelcomeOnboardingActivity var, Object var) throws Exception {
      boolean var;
      if(var.viewPager.getCurrentItem() >= var.viewPager.getAdapter().b() - 1) {
         var = true;
      } else {
         var = false;
      }

      return Boolean.valueOf(var);
   }

   public static void a(Activity var) {
      var.startActivity(new Intent(var, WelcomeOnboardingActivity.class));
   }

   public n a() {
      return com.b.a.b.b.a.a.a(this.viewPager).map(b.a(this));
   }

   public n b() {
      return com.b.a.c.c.a(this.mainActionButton).map(c.a(this));
   }

   public n c() {
      return com.b.a.c.c.a(this.logInButton);
   }

   public void d() {
      this.mainActionButton.setText(2131362892);
   }

   public void e() {
      this.mainActionButton.setText(2131362886);
   }

   public void f() {
      this.viewPager.a(this.viewPager.getCurrentItem() + 1, true);
   }

   public void g() {
      SignUpActivity.a(this, this.viewPager.getAdapter().b());
   }

   public void onBackPressed() {
      if(this.viewPager.getCurrentItem() > 0) {
         this.viewPager.a(this.viewPager.getCurrentItem() - 1, true);
      } else {
         super.onBackPressed();
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.l().a(this);
      this.setContentView(2131034143);
      ButterKnife.bind((Activity)this);
      List var = Arrays.asList(new co.uk.getmondo.common.pager.f[]{new a(this.viewPager), new co.uk.getmondo.common.pager.e("lottie/welcome_imagine.json", 2131362891, 2131362890, Impression.b(2)), new co.uk.getmondo.common.pager.d(2130838031, 2131362885, 2131362884, Impression.b(3)), new co.uk.getmondo.common.pager.d(2130838030, 2131362883, 2131362882, Impression.b(4)), new co.uk.getmondo.common.pager.b(2131034405, Impression.b(5)), new co.uk.getmondo.common.pager.d(2130838033, 2131362889, 2131362888, Impression.b(6))});
      this.viewPager.setAdapter(new GenericPagerAdapter(var) {
         public Object a(ViewGroup var, int var) {
            View var = (View)super.a(var, var);
            if(var != 0) {
               var = co.uk.getmondo.common.k.n.b(116);
               var.setPadding(var.getPaddingLeft(), var.getPaddingTop(), var.getPaddingRight(), var + var.getPaddingBottom());
            }

            return var;
         }
      });
      this.b.a(this.viewPager);
      this.a.a((e.a)this);
      this.mainActionButton.animate().setStartDelay(1200L).alpha(1.0F);
      this.logInButton.animate().setStartDelay(1200L).alpha(1.0F);
   }

   protected void onDestroy() {
      this.b.a();
      this.a.b();
      super.onDestroy();
   }
}
