package co.uk.getmondo.d;

import io.realm.bb;
import io.realm.bk;

public class ag implements bb, bk {
   private String sddMigrationType;

   public ag() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public ag(String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
   }

   public ah a() {
      return ah.a(this.b());
   }

   public void a(String var) {
      this.sddMigrationType = var;
   }

   public String b() {
      return this.sddMigrationType;
   }

   public boolean equals(Object var) {
      boolean var = true;
      if(this != var) {
         if(var != null && this.getClass() == var.getClass()) {
            ag var = (ag)var;
            if(this.b() != null) {
               var = this.b().equals(var.b());
            } else if(var.b() != null) {
               var = false;
            }
         } else {
            var = false;
         }
      }

      return var;
   }

   public int hashCode() {
      int var;
      if(this.b() != null) {
         var = this.b().hashCode();
      } else {
         var = 0;
      }

      return var;
   }
}
