package co.uk.getmondo.d;

import io.realm.bb;
import java.util.Date;

public class n implements bb, io.realm.v {
   private String id;
   private Date lastFetched;

   public n() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("feed_metadata_id");
   }

   public n(Date var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("feed_metadata_id");
      this.a(var);
   }

   public String a() {
      return this.id;
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(Date var) {
      this.lastFetched = var;
   }

   public Date b() {
      return this.lastFetched;
   }

   public boolean equals(Object var) {
      boolean var = true;
      if(this != var) {
         if(var != null && this.getClass() == var.getClass()) {
            n var;
            label35: {
               var = (n)var;
               if(this.a() != null) {
                  if(this.a().equals(var.a())) {
                     break label35;
                  }
               } else if(var.a() == null) {
                  break label35;
               }

               var = false;
               return var;
            }

            if(this.b() != null) {
               var = this.b().equals(var.b());
            } else if(var.b() != null) {
               var = false;
            }
         } else {
            var = false;
         }
      }

      return var;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.a() != null) {
         var = this.a().hashCode();
      } else {
         var = 0;
      }

      if(this.b() != null) {
         var = this.b().hashCode();
      }

      return var * 31 + var;
   }
}
