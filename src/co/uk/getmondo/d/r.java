package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.bb;

public class r implements Parcelable, io.realm.ad, bb {
   public static final Creator CREATOR = new Creator() {
      public r a(Parcel var) {
         return new r(var);
      }

      public r[] a(int var) {
         return new r[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return this.a(var);
      }
   };
   private String customerRejectedMessage;
   private String id;
   private String rejectedReason;

   public r() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   protected r(Parcel var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var.readString());
      this.b(var.readString());
      this.c(var.readString());
   }

   public r(String var, String var, String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
   }

   public co.uk.getmondo.signup.identity_verification.y a() {
      return co.uk.getmondo.signup.identity_verification.y.a(this.d());
   }

   public void a(String var) {
      this.id = var;
   }

   public String b() {
      return this.e();
   }

   public void b(String var) {
      this.rejectedReason = var;
   }

   public String c() {
      return this.id;
   }

   public void c(String var) {
      this.customerRejectedMessage = var;
   }

   public String d() {
      return this.rejectedReason;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.customerRejectedMessage;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               r var = (r)var;
               if(this.c() != null) {
                  var = var;
                  if(!this.c().equals(var.c())) {
                     return var;
                  }
               } else if(var.c() != null) {
                  var = var;
                  return var;
               }

               if(this.d() != null) {
                  var = var;
                  if(!this.d().equals(var.d())) {
                     return var;
                  }
               } else if(var.d() != null) {
                  var = var;
                  return var;
               }

               if(this.e() != null) {
                  var = this.e().equals(var.e());
               } else {
                  var = var;
                  if(var.e() != null) {
                     var = false;
                  }
               }
            }
         }
      }

      return var;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.c() != null) {
         var = this.c().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.d() != null) {
         var = this.d().hashCode();
      } else {
         var = 0;
      }

      if(this.e() != null) {
         var = this.e().hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public void writeToParcel(Parcel var, int var) {
      var.writeString(this.c());
      var.writeString(this.d());
      var.writeString(this.e());
   }
}
