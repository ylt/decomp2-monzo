package co.uk.getmondo.d;

public enum j {
   CARD_BLOCKED,
   CARD_INACTIVE,
   CONTACTLESS_FAILURE,
   EXCEEDS_TRANSACTION_AMOUNT_LIMIT,
   EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED,
   EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT,
   EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED,
   EXCEEDS_WITHDRAWAL_COUNT_LIMIT,
   EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED,
   INSUFFICIENT_FUNDS,
   INVALID_CVC,
   INVALID_EXPIRY_DATE,
   INVALID_MERCHANT,
   INVALID_PIN,
   KYC_REQUIRED,
   MAGNETIC_STRIP_ATM,
   NOT_DECLINED,
   OTHER,
   PIN_RETRY_COUNT_EXCEEDED;

   public static j a(String var) {
      j var;
      if(var == null) {
         var = NOT_DECLINED;
      } else {
         byte var = -1;
         switch(var.hashCode()) {
         case -1777036633:
            if(var.equals("EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED")) {
               var = 15;
            }
            break;
         case -1205294198:
            if(var.equals("EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED")) {
               var = 14;
            }
            break;
         case -1022719036:
            if(var.equals("PIN_RETRY_COUNT_EXCEEDED")) {
               var = 16;
            }
            break;
         case -760002350:
            if(var.equals("INVALID_EXPIRY_DATE")) {
               var = 6;
            }
            break;
         case -627306704:
            if(var.equals("INVALID_MERCHANT")) {
               var = 4;
            }
            break;
         case -510312674:
            if(var.equals("EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT")) {
               var = 11;
            }
            break;
         case -89191199:
            if(var.equals("EXCEEDS_WITHDRAWAL_COUNT_LIMIT")) {
               var = 12;
            }
            break;
         case 75532016:
            if(var.equals("OTHER")) {
               var = 0;
            }
            break;
         case 86317810:
            if(var.equals("INSUFFICIENT_FUNDS")) {
               var = 1;
            }
            break;
         case 274890761:
            if(var.equals("KYC_REQUIRED")) {
               var = 9;
            }
            break;
         case 284837365:
            if(var.equals("EXCEEDS_TRANSACTION_AMOUNT_LIMIT")) {
               var = 10;
            }
            break;
         case 449948728:
            if(var.equals("MAGNETIC_STRIP_ATM")) {
               var = 5;
            }
            break;
         case 693598109:
            if(var.equals("CARD_BLOCKED")) {
               var = 2;
            }
            break;
         case 715947194:
            if(var.equals("CARD_INACTIVE")) {
               var = 3;
            }
            break;
         case 1201074120:
            if(var.equals("INVALID_CVC")) {
               var = 7;
            }
            break;
         case 1201086221:
            if(var.equals("INVALID_PIN")) {
               var = 17;
            }
            break;
         case 1491188243:
            if(var.equals("EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED")) {
               var = 13;
            }
            break;
         case 1811088580:
            if(var.equals("CONTACTLESS_FAILURE")) {
               var = 8;
            }
         }

         switch(var) {
         case 0:
            var = OTHER;
            break;
         case 1:
            var = INSUFFICIENT_FUNDS;
            break;
         case 2:
            var = CARD_BLOCKED;
            break;
         case 3:
            var = CARD_INACTIVE;
            break;
         case 4:
            var = INVALID_MERCHANT;
            break;
         case 5:
            var = MAGNETIC_STRIP_ATM;
            break;
         case 6:
            var = INVALID_EXPIRY_DATE;
            break;
         case 7:
            var = INVALID_CVC;
            break;
         case 8:
            var = CONTACTLESS_FAILURE;
            break;
         case 9:
            var = KYC_REQUIRED;
            break;
         case 10:
            var = EXCEEDS_TRANSACTION_AMOUNT_LIMIT;
            break;
         case 11:
            var = EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT;
            break;
         case 12:
            var = EXCEEDS_WITHDRAWAL_COUNT_LIMIT;
            break;
         case 13:
            var = EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED;
            break;
         case 14:
            var = EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED;
            break;
         case 15:
            var = EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED;
            break;
         case 16:
            var = PIN_RETRY_COUNT_EXCEEDED;
            break;
         case 17:
            var = INVALID_PIN;
            break;
         default:
            var = OTHER;
         }
      }

      return var;
   }
}
