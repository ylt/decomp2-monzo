package co.uk.getmondo.d;

import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001a\u001a\u00020\bHÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\nHÆ\u0003J\t\u0010\u001c\u001a\u00020\bHÆ\u0003JG\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000b\u001a\u00020\bHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\b2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020\u0003HÖ\u0001J\u0010\u0010$\u001a\u00020\u00012\u0006\u0010%\u001a\u00020\bH\u0016J\u000e\u0010&\u001a\u00020\u00012\u0006\u0010'\u001a\u00020\bR\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006("},
   d2 = {"Lco/uk/getmondo/model/PrepaidAccount;", "Lco/uk/getmondo/model/Account;", "id", "", "description", "created", "Lorg/threeten/bp/LocalDateTime;", "cardActivated", "", "type", "Lco/uk/getmondo/model/Account$Type;", "initialTopupCompleted", "(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/model/Account$Type;Z)V", "getCardActivated", "()Z", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getDescription", "()Ljava/lang/String;", "getId", "getInitialTopupCompleted", "getType", "()Lco/uk/getmondo/model/Account$Type;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "", "hashCode", "", "toString", "withCardActivated", "isActivated", "withInitialTopUpCompleted", "isCompleted", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ab implements a {
   private final boolean cardActivated;
   private final LocalDateTime created;
   private final String description;
   private final String id;
   private final boolean initialTopupCompleted;
   private final a.b type;

   public ab(String var, String var, LocalDateTime var, boolean var, a.b var, boolean var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "description");
      kotlin.d.b.l.b(var, "created");
      super();
      this.id = var;
      this.description = var;
      this.created = var;
      this.cardActivated = var;
      this.type = var;
      this.initialTopupCompleted = var;
   }

   // $FF: synthetic method
   public static ab a(ab var, String var, String var, LocalDateTime var, boolean var, a.b var, boolean var, int var, Object var) {
      if((var & 1) != 0) {
         var = var.a();
      }

      if((var & 2) != 0) {
         var = var.g();
      }

      if((var & 4) != 0) {
         var = var.b();
      }

      if((var & 8) != 0) {
         var = var.c();
      }

      if((var & 16) != 0) {
         var = var.d();
      }

      if((var & 32) != 0) {
         var = var.initialTopupCompleted;
      }

      return var.a(var, var, var, var, var, var);
   }

   public a a(boolean var) {
      return (a)a(this, (String)null, (String)null, (LocalDateTime)null, var, (a.b)null, false, 55, (Object)null);
   }

   public final ab a(String var, String var, LocalDateTime var, boolean var, a.b var, boolean var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "description");
      kotlin.d.b.l.b(var, "created");
      return new ab(var, var, var, var, var, var);
   }

   public String a() {
      return this.id;
   }

   public final a b(boolean var) {
      return (a)a(this, (String)null, (String)null, (LocalDateTime)null, false, (a.b)null, var, 31, (Object)null);
   }

   public LocalDateTime b() {
      return this.created;
   }

   public boolean c() {
      return this.cardActivated;
   }

   public a.b d() {
      return this.type;
   }

   public boolean e() {
      return a.a.a(this);
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ab)) {
            return var;
         }

         ab var = (ab)var;
         var = var;
         if(!kotlin.d.b.l.a(this.a(), var.a())) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.g(), var.g())) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.b(), var.b())) {
            return var;
         }

         boolean var;
         if(this.c() == var.c()) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.d(), var.d())) {
            return var;
         }

         if(this.initialTopupCompleted == var.initialTopupCompleted) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public boolean f() {
      return a.a.b(this);
   }

   public String g() {
      return this.description;
   }

   public final boolean h() {
      return this.initialTopupCompleted;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "PrepaidAccount(id=" + this.a() + ", description=" + this.g() + ", created=" + this.b() + ", cardActivated=" + this.c() + ", type=" + this.d() + ", initialTopupCompleted=" + this.initialTopupCompleted + ")";
   }
}
