package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import co.uk.getmondo.api.model.Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 12\u00020\u0001:\u00011B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004BA\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\fJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u0016\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u0010\u0019J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0006HÆ\u0003JP\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0002\u0010!J\b\u0010\"\u001a\u00020#H\u0016J\u0013\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010'H\u0096\u0002J\b\u0010(\u001a\u00020\u0006H\u0016J\u0006\u0010)\u001a\u00020%J\u0006\u0010*\u001a\u00020%J\b\u0010+\u001a\u00020#H\u0016J\t\u0010,\u001a\u00020\u0006HÖ\u0001J\u0018\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020#H\u0016R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012R\u0014\u0010\u0015\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0012R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u001b\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b¢\u0006\n\n\u0002\u0010\u001a\u001a\u0004\b\u0018\u0010\u0019¨\u00062"},
   d2 = {"Lco/uk/getmondo/model/LegacyAddress;", "Lco/uk/getmondo/api/model/Address;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "postalCode", "", "streetAddress", "", "city", "country", "administrativeArea", "(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "addressLines", "", "getAddressLines", "()Ljava/util/List;", "getAdministrativeArea", "()Ljava/lang/String;", "getCity", "getCountry", "countryCode", "getCountryCode", "getPostalCode", "getStreetAddress", "()[Ljava/lang/String;", "[Ljava/lang/String;", "component1", "component2", "component3", "component4", "component5", "copy", "(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/model/LegacyAddress;", "describeContents", "", "equals", "", "other", "", "formattedSingleLine", "hasCity", "hasStreetAddress", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class s implements Address {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public s a(Parcel var) {
         kotlin.d.b.l.b(var, "source");
         return new s(var);
      }

      public s[] a(int var) {
         return new s[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final s.a Companion = new s.a((kotlin.d.b.i)null);
   private final String administrativeArea;
   private final String city;
   private final String country;
   private final String postalCode;
   private final String[] streetAddress;

   public s(Parcel var) {
      kotlin.d.b.l.b(var, "source");
      this(var.readString(), var.createStringArray(), var.readString(), var.readString(), var.readString());
   }

   public s(String var, String[] var, String var, String var, String var) {
      this.postalCode = var;
      this.streetAddress = var;
      this.city = var;
      this.country = var;
      this.administrativeArea = var;
   }

   // $FF: synthetic method
   public s(String var, String[] var, String var, String var, String var, int var, kotlin.d.b.i var) {
      if((var & 1) != 0) {
         var = "";
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var);
   }

   public List a() {
      String[] var = this.streetAddress;
      List var;
      if(var != null) {
         var = kotlin.a.g.h((Object[])var);
         if(var != null) {
            return var;
         }
      }

      var = kotlin.a.m.a();
      return var;
   }

   public String b() {
      return this.administrativeArea;
   }

   public String c() {
      return this.postalCode;
   }

   public String d() {
      String var = this.country;
      if(var == null) {
         var = "";
      }

      return var;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      Iterable var = (Iterable)kotlin.a.m.b((Collection)this.a(), (Iterable)kotlin.a.m.c(new String[]{this.city, this.b(), this.d()}));
      Collection var = (Collection)(new ArrayList());
      Iterator var = var.iterator();

      while(var.hasNext()) {
         Object var = var.next();
         boolean var;
         if(!kotlin.h.j.a((CharSequence)((String)var))) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            var.add(var);
         }
      }

      return kotlin.a.m.a((Iterable)((List)var), (CharSequence)", ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null);
   }

   public boolean equals(Object var) {
      boolean var;
      if((s)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(kotlin.d.b.l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.LegacyAddress");
            }

            s var = (s)var;
            if(kotlin.d.b.l.a(this.c(), ((s)var).c()) ^ true) {
               var = false;
            } else if(!Arrays.equals((Object[])this.streetAddress, (Object[])((s)var).streetAddress)) {
               var = false;
            } else if(kotlin.d.b.l.a(this.city, ((s)var).city) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.country, ((s)var).country) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.b(), ((s)var).b()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public final boolean f() {
      boolean var;
      if(this.streetAddress != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final boolean g() {
      CharSequence var = (CharSequence)this.city;
      boolean var;
      if(var != null && var.length() != 0) {
         var = false;
      } else {
         var = true;
      }

      boolean var;
      if(!var) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final String[] h() {
      return this.streetAddress;
   }

   public int hashCode() {
      int var = 0;
      String var = this.c();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String[] var = this.streetAddress;
      int var;
      if(var != null) {
         var = Arrays.hashCode((Object[])var);
      } else {
         var = 0;
      }

      var = this.city;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.country;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b();
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public final String i() {
      return this.city;
   }

   public final String j() {
      return this.country;
   }

   public String toString() {
      return "LegacyAddress(postalCode=" + this.c() + ", streetAddress=" + Arrays.toString(this.streetAddress) + ", city=" + this.city + ", country=" + this.country + ", administrativeArea=" + this.b() + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      kotlin.d.b.l.b(var, "dest");
      var.writeString(this.c());
      var.writeStringArray(this.streetAddress);
      var.writeString(this.city);
      var.writeString(this.country);
      var.writeString(this.b());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/model/LegacyAddress$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/LegacyAddress;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
