package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b%\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010'\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0002\u0010\u001fJ\t\u0010(\u001a\u00020\fHÆ\u0003J\t\u0010)\u001a\u00020\u000eHÆ\u0003Jp\u0010*\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000eHÆ\u0001¢\u0006\u0002\u0010+J\u0013\u0010,\u001a\u00020\u000e2\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010.\u001a\u00020\nHÖ\u0001J\t\u0010/\u001a\u00020\u0003HÖ\u0001J\u000e\u00100\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\fJ\u0016\u00101\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0003J\u000e\u00102\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\u0003R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u0011\u0010\u0017\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0015R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u00038F¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0015R\u0015\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\n\n\u0002\u0010 \u001a\u0004\b\u001e\u0010\u001f¨\u00063"},
   d2 = {"Lco/uk/getmondo/model/Profile;", "", "userId", "", "name", "preferredName", "email", "dateOfBirth", "phoneNumber", "userNumber", "", "address", "Lco/uk/getmondo/model/LegacyAddress;", "addressUpdatable", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/model/LegacyAddress;Z)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getAddressUpdatable", "()Z", "getDateOfBirth", "()Ljava/lang/String;", "getEmail", "isStudent", "getName", "getPhoneNumber", "getPreferredName", "preferredNameOrFullName", "getPreferredNameOrFullName", "getUserId", "getUserNumber", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/model/LegacyAddress;Z)Lco/uk/getmondo/model/Profile;", "equals", "other", "hashCode", "toString", "withAddress", "withNameAndDob", "withPhoneNumber", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ac {
   private final s address;
   private final boolean addressUpdatable;
   private final String dateOfBirth;
   private final String email;
   private final String name;
   private final String phoneNumber;
   private final String preferredName;
   private final String userId;
   private final Integer userNumber;

   public ac(String var, String var, String var, String var, String var, String var, Integer var, s var, boolean var) {
      kotlin.d.b.l.b(var, "userId");
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "email");
      kotlin.d.b.l.b(var, "address");
      super();
      this.userId = var;
      this.name = var;
      this.preferredName = var;
      this.email = var;
      this.dateOfBirth = var;
      this.phoneNumber = var;
      this.userNumber = var;
      this.address = var;
      this.addressUpdatable = var;
   }

   // $FF: synthetic method
   public ac(String var, String var, String var, String var, String var, String var, Integer var, s var, boolean var, int var, kotlin.d.b.i var) {
      if((var & 4) != 0) {
         var = (String)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      if((var & 32) != 0) {
         var = (String)null;
      }

      if((var & 64) != 0) {
         var = (Integer)null;
      }

      if((var & 256) != 0) {
         var = false;
      }

      this(var, var, var, var, var, var, var, var, var);
   }

   public final ac a(s var) {
      kotlin.d.b.l.b(var, "address");
      return a(this, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, var, false, 383, (Object)null);
   }

   public final ac a(String var) {
      kotlin.d.b.l.b(var, "phoneNumber");
      return a(this, (String)null, (String)null, (String)null, (String)null, (String)null, var, (Integer)null, (s)null, false, 479, (Object)null);
   }

   public final ac a(String var, String var) {
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "dateOfBirth");
      return a(this, (String)null, var, (String)null, (String)null, var, (String)null, (Integer)null, (s)null, false, 493, (Object)null);
   }

   public final ac a(String var, String var, String var, String var, String var, String var, Integer var, s var, boolean var) {
      kotlin.d.b.l.b(var, "userId");
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "email");
      kotlin.d.b.l.b(var, "address");
      return new ac(var, var, var, var, var, var, var, var, var);
   }

   public final String a() {
      CharSequence var = (CharSequence)this.preferredName;
      boolean var;
      if(var != null && var.length() != 0) {
         var = false;
      } else {
         var = true;
      }

      String var;
      if(var) {
         var = this.name;
      } else {
         var = this.preferredName;
      }

      return var;
   }

   public final String b() {
      return this.userId;
   }

   public final String c() {
      return this.name;
   }

   public final String d() {
      return this.email;
   }

   public final String e() {
      return this.dateOfBirth;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof ac)) {
            return var;
         }

         ac var = (ac)var;
         var = var;
         if(!kotlin.d.b.l.a(this.userId, var.userId)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.name, var.name)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.preferredName, var.preferredName)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.email, var.email)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.dateOfBirth, var.dateOfBirth)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.phoneNumber, var.phoneNumber)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.userNumber, var.userNumber)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.address, var.address)) {
            return var;
         }

         boolean var;
         if(this.addressUpdatable == var.addressUpdatable) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.phoneNumber;
   }

   public final Integer g() {
      return this.userNumber;
   }

   public final s h() {
      return this.address;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final boolean i() {
      return this.addressUpdatable;
   }

   public String toString() {
      return "Profile(userId=" + this.userId + ", name=" + this.name + ", preferredName=" + this.preferredName + ", email=" + this.email + ", dateOfBirth=" + this.dateOfBirth + ", phoneNumber=" + this.phoneNumber + ", userNumber=" + this.userNumber + ", address=" + this.address + ", addressUpdatable=" + this.addressUpdatable + ")";
   }
}
