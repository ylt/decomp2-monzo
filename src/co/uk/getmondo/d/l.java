package co.uk.getmondo.d;

import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0013\u0010\f\u001a\u00020\u00032\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0007\"\u0004\b\u000b\u0010\t¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/model/FeatureFlags;", "Lio/realm/RealmObject;", "currentAccountP2pEnabled", "", "potsEnabled", "(ZZ)V", "getCurrentAccountP2pEnabled", "()Z", "setCurrentAccountP2pEnabled", "(Z)V", "getPotsEnabled", "setPotsEnabled", "equals", "other", "", "hashCode", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class l extends bc implements io.realm.r {
   private boolean currentAccountP2pEnabled;
   private boolean potsEnabled;

   public l() {
      this(false, false, 3, (kotlin.d.b.i)null);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public l(boolean var) {
      this(var, false, 2, (kotlin.d.b.i)null);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public l(boolean var, boolean var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
   }

   // $FF: synthetic method
   public l(boolean var, boolean var, int var, kotlin.d.b.i var) {
      if((var & 1) != 0) {
         var = false;
      }

      if((var & 2) != 0) {
         var = false;
      }

      this(var, var);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public void a(boolean var) {
      this.currentAccountP2pEnabled = var;
   }

   public final boolean a() {
      return this.b();
   }

   public void b(boolean var) {
      this.potsEnabled = var;
   }

   public boolean b() {
      return this.currentAccountP2pEnabled;
   }

   public boolean c() {
      return this.potsEnabled;
   }

   public boolean equals(Object var) {
      boolean var;
      if((l)this == var) {
         var = true;
      } else {
         Class var = this.getClass();
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(kotlin.d.b.l.a(var, var) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.FeatureFlags");
            }

            l var = (l)var;
            if(this.b() != ((l)var).b()) {
               var = false;
            } else if(this.c() != ((l)var).c()) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public int hashCode() {
      return Boolean.valueOf(this.b()).hashCode() * 31 + Boolean.valueOf(this.c()).hashCode();
   }
}
