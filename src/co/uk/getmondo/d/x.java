package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/model/PaymentLimit;", "", "()V", "AmountLimit", "CountLimit", "VirtualLimit", "Lco/uk/getmondo/model/PaymentLimit$AmountLimit;", "Lco/uk/getmondo/model/PaymentLimit$CountLimit;", "Lco/uk/getmondo/model/PaymentLimit$VirtualLimit;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class x {
   private x() {
   }

   // $FF: synthetic method
   public x(kotlin.d.b.i var) {
      this();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$AmountLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "limit", "Lco/uk/getmondo/model/Amount;", "used", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getId", "()Ljava/lang/String;", "getLimit", "()Lco/uk/getmondo/model/Amount;", "getName", "getUsed", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends x {
      private final String id;
      private final c limit;
      private final String name;
      private final c used;

      public a(String var, String var, c var, c var) {
         kotlin.d.b.l.b(var, "id");
         kotlin.d.b.l.b(var, "name");
         kotlin.d.b.l.b(var, "limit");
         kotlin.d.b.l.b(var, "used");
         super((kotlin.d.b.i)null);
         this.id = var;
         this.name = var;
         this.limit = var;
         this.used = var;
      }

      public final String a() {
         return this.name;
      }

      public final c b() {
         return this.limit;
      }

      public final c c() {
         return this.used;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label32: {
               if(var instanceof x.a) {
                  x.a var = (x.a)var;
                  if(kotlin.d.b.l.a(this.id, var.id) && kotlin.d.b.l.a(this.name, var.name) && kotlin.d.b.l.a(this.limit, var.limit) && kotlin.d.b.l.a(this.used, var.used)) {
                     break label32;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.id;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.name;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         c var = this.limit;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.used;
         if(var != null) {
            var = var.hashCode();
         }

         return (var + (var + var * 31) * 31) * 31 + var;
      }

      public String toString() {
         return "AmountLimit(id=" + this.id + ", name=" + this.name + ", limit=" + this.limit + ", used=" + this.used + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$CountLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "limit", "", "used", "(Ljava/lang/String;Ljava/lang/String;JJ)V", "getId", "()Ljava/lang/String;", "getLimit", "()J", "getName", "getUsed", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends x {
      private final String id;
      private final long limit;
      private final String name;
      private final long used;

      public b(String var, String var, long var, long var) {
         kotlin.d.b.l.b(var, "id");
         kotlin.d.b.l.b(var, "name");
         super((kotlin.d.b.i)null);
         this.id = var;
         this.name = var;
         this.limit = var;
         this.used = var;
      }

      public final String a() {
         return this.name;
      }

      public final long b() {
         return this.limit;
      }

      public final long c() {
         return this.used;
      }

      public boolean equals(Object var) {
         boolean var = false;
         boolean var;
         if(this != var) {
            var = var;
            if(!(var instanceof x.b)) {
               return var;
            }

            x.b var = (x.b)var;
            var = var;
            if(!kotlin.d.b.l.a(this.id, var.id)) {
               return var;
            }

            var = var;
            if(!kotlin.d.b.l.a(this.name, var.name)) {
               return var;
            }

            boolean var;
            if(this.limit == var.limit) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }

            if(this.used == var.used) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.id;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.name;
         if(var != null) {
            var = var.hashCode();
         }

         long var = this.limit;
         int var = (int)(var ^ var >>> 32);
         var = this.used;
         return ((var * 31 + var) * 31 + var) * 31 + (int)(var ^ var >>> 32);
      }

      public String toString() {
         return "CountLimit(id=" + this.id + ", name=" + this.name + ", limit=" + this.limit + ", used=" + this.used + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$VirtualLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "(Ljava/lang/String;Ljava/lang/String;)V", "getId", "()Ljava/lang/String;", "getName", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends x {
      private final String id;
      private final String name;

      public c(String var, String var) {
         kotlin.d.b.l.b(var, "id");
         kotlin.d.b.l.b(var, "name");
         super((kotlin.d.b.i)null);
         this.id = var;
         this.name = var;
      }

      public final String a() {
         return this.name;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label28: {
               if(var instanceof x.c) {
                  x.c var = (x.c)var;
                  if(kotlin.d.b.l.a(this.id, var.id) && kotlin.d.b.l.a(this.name, var.name)) {
                     break label28;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.id;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.name;
         if(var != null) {
            var = var.hashCode();
         }

         return var * 31 + var;
      }

      public String toString() {
         return "VirtualLimit(id=" + this.id + ", name=" + this.name + ")";
      }
   }
}
