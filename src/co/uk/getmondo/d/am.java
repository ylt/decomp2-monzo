package co.uk.getmondo.d;

import io.realm.bb;
import io.realm.bp;

public class am implements bb, bp {
   public static final String ID = "user_settings_storage_static_id";
   private String id;
   private p inboundP2P;
   private String monzoMeUsername;
   private boolean prepaidAccountMigrated;
   private String statusId;

   public am() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("user_settings_storage_static_id");
   }

   public am(String var, String var, p var, boolean var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("user_settings_storage_static_id");
      this.b(var);
      this.c(var);
      this.a(var);
      this.a(var);
   }

   public co.uk.getmondo.payments.send.data.a.d a() {
      return co.uk.getmondo.payments.send.data.a.d.a(this.f());
   }

   public void a(p var) {
      this.inboundP2P = var;
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(boolean var) {
      this.prepaidAccountMigrated = var;
   }

   public String b() {
      return this.g();
   }

   public void b(String var) {
      this.statusId = var;
   }

   public p c() {
      return this.h();
   }

   public void c(String var) {
      this.monzoMeUsername = var;
   }

   public boolean d() {
      return this.i();
   }

   public String e() {
      return this.id;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               am var = (am)var;
               var = var;
               if(this.i() == var.i()) {
                  if(this.e() != null) {
                     var = var;
                     if(!this.e().equals(var.e())) {
                        return var;
                     }
                  } else if(var.e() != null) {
                     var = var;
                     return var;
                  }

                  if(this.f() != null) {
                     var = var;
                     if(!this.f().equals(var.f())) {
                        return var;
                     }
                  } else if(var.f() != null) {
                     var = var;
                     return var;
                  }

                  if(this.g() != null) {
                     var = var;
                     if(!this.g().equals(var.g())) {
                        return var;
                     }
                  } else if(var.g() != null) {
                     var = var;
                     return var;
                  }

                  if(this.h() != null) {
                     var = this.h().equals(var.h());
                  } else {
                     var = var;
                     if(var.h() != null) {
                        var = false;
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public String f() {
      return this.statusId;
   }

   public String g() {
      return this.monzoMeUsername;
   }

   public p h() {
      return this.inboundP2P;
   }

   public int hashCode() {
      byte var = 0;
      int var;
      if(this.e() != null) {
         var = this.e().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.f() != null) {
         var = this.f().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.g() != null) {
         var = this.g().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.h() != null) {
         var = this.h().hashCode();
      } else {
         var = 0;
      }

      if(this.i()) {
         var = 1;
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public boolean i() {
      return this.prepaidAccountMigrated;
   }
}
