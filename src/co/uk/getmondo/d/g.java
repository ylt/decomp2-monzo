package co.uk.getmondo.d;

import io.realm.bb;

public class g implements bb, io.realm.k {
   private String accountId;
   private String cardStatus;
   private String expires;
   private String id;
   private String lastDigits;
   private String processorToken;
   private boolean replacementOrdered;

   public g() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public g(String var, String var, String var, String var, String var, String var, boolean var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
      this.a(var);
   }

   public String a() {
      return this.i();
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(boolean var) {
      this.replacementOrdered = var;
   }

   public String b() {
      return this.j();
   }

   public void b(String var) {
      this.accountId = var;
   }

   public String c() {
      return this.r_();
   }

   public void c(String var) {
      this.expires = var;
   }

   public String d() {
      return this.l();
   }

   public void d(String var) {
      this.lastDigits = var;
   }

   public String e() {
      return this.m();
   }

   public void e(String var) {
      this.processorToken = var;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               g var = (g)var;
               var = var;
               if(this.o() == var.o()) {
                  if(this.i() != null) {
                     var = var;
                     if(!this.i().equals(var.i())) {
                        return var;
                     }
                  } else if(var.i() != null) {
                     var = var;
                     return var;
                  }

                  if(this.j() != null) {
                     var = var;
                     if(!this.j().equals(var.j())) {
                        return var;
                     }
                  } else if(var.j() != null) {
                     var = var;
                     return var;
                  }

                  if(this.r_() != null) {
                     var = var;
                     if(!this.r_().equals(var.r_())) {
                        return var;
                     }
                  } else if(var.r_() != null) {
                     var = var;
                     return var;
                  }

                  if(this.l() != null) {
                     var = var;
                     if(!this.l().equals(var.l())) {
                        return var;
                     }
                  } else if(var.l() != null) {
                     var = var;
                     return var;
                  }

                  if(this.m() != null) {
                     var = var;
                     if(!this.m().equals(var.m())) {
                        return var;
                     }
                  } else if(var.m() != null) {
                     var = var;
                     return var;
                  }

                  if(this.n() != null) {
                     var = this.n().equals(var.n());
                  } else {
                     var = var;
                     if(var.n() != null) {
                        var = false;
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public void f(String var) {
      this.cardStatus = var;
   }

   public boolean f() {
      return this.o();
   }

   public co.uk.getmondo.api.model.a g() {
      return co.uk.getmondo.api.model.a.valueOf(this.n());
   }

   public boolean h() {
      boolean var;
      if(this.g() == co.uk.getmondo.api.model.a.ACTIVE) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public int hashCode() {
      byte var = 0;
      int var;
      if(this.i() != null) {
         var = this.i().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.j() != null) {
         var = this.j().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.r_() != null) {
         var = this.r_().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.l() != null) {
         var = this.l().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.m() != null) {
         var = this.m().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.n() != null) {
         var = this.n().hashCode();
      } else {
         var = 0;
      }

      if(this.o()) {
         var = 1;
      }

      return (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String i() {
      return this.id;
   }

   public String j() {
      return this.accountId;
   }

   public String l() {
      return this.lastDigits;
   }

   public String m() {
      return this.processorToken;
   }

   public String n() {
      return this.cardStatus;
   }

   public boolean o() {
      return this.replacementOrdered;
   }

   public String r_() {
      return this.expires;
   }
}
