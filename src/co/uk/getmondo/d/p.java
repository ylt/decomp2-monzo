package co.uk.getmondo.d;

import io.realm.bb;

public class p implements io.realm.ab, bb {
   public static final String ID = "inbound_p2p_storage_static_id";
   private boolean enabled;
   private String id;
   private String ineligibilityReason;
   private int max;
   private int min;

   public p() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("inbound_p2p_storage_static_id");
   }

   public p(boolean var, int var, int var, String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("inbound_p2p_storage_static_id");
      this.a(var);
      this.a(var);
      this.b(var);
      this.b(var);
   }

   public int a() {
      return this.e();
   }

   public void a(int var) {
      this.max = var;
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(boolean var) {
      this.enabled = var;
   }

   public int b() {
      return this.f();
   }

   public void b(int var) {
      this.min = var;
   }

   public void b(String var) {
      this.ineligibilityReason = var;
   }

   public String c() {
      return this.id;
   }

   public boolean d() {
      return this.enabled;
   }

   public int e() {
      return this.max;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               p var = (p)var;
               var = var;
               if(this.d() == var.d()) {
                  var = var;
                  if(this.e() == var.e()) {
                     var = var;
                     if(this.f() == var.f()) {
                        if(this.g() != null) {
                           var = this.g().equals(var.g());
                        } else {
                           var = var;
                           if(var.g() != null) {
                              var = false;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public int f() {
      return this.min;
   }

   public String g() {
      return this.ineligibilityReason;
   }

   public int hashCode() {
      int var = 0;
      byte var;
      if(this.d()) {
         var = 1;
      } else {
         var = 0;
      }

      int var = this.e();
      int var = this.f();
      if(this.g() != null) {
         var = this.g().hashCode();
      }

      return ((var * 31 + var) * 31 + var) * 31 + var;
   }
}
