package co.uk.getmondo.d;

import io.realm.bb;

public class o implements bb, io.realm.z {
   private String ticketId;

   public o() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public o(String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
   }

   public String a() {
      return this.b();
   }

   public void a(String var) {
      this.ticketId = var;
   }

   public String b() {
      return this.ticketId;
   }

   public boolean equals(Object var) {
      boolean var = true;
      if(this != var) {
         if(var != null && this.getClass() == var.getClass()) {
            o var = (o)var;
            if(this.b() != null) {
               var = this.b().equals(var.b());
            } else if(var.b() != null) {
               var = false;
            }
         } else {
            var = false;
         }
      }

      return var;
   }

   public int hashCode() {
      int var;
      if(this.b() != null) {
         var = this.b().hashCode();
      } else {
         var = 0;
      }

      return var;
   }
}
