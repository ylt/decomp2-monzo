package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001:\u0001.BA\b\u0007\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0003HÆ\u0003JE\u0010!\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\"\u001a\u00020\u000f2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u0003HÖ\u0001J\u000e\u0010'\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010(\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020+J\u0018\u0010,\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010-\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\tR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u00168F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006/"},
   d2 = {"Lco/uk/getmondo/model/User;", "", "userId", "", "account", "Lco/uk/getmondo/model/Account;", "profile", "Lco/uk/getmondo/model/Profile;", "waitlistProfile", "Lco/uk/getmondo/model/WaitlistProfile;", "secondaryAccountId", "(Ljava/lang/String;Lco/uk/getmondo/model/Account;Lco/uk/getmondo/model/Profile;Lco/uk/getmondo/model/WaitlistProfile;Ljava/lang/String;)V", "getAccount", "()Lco/uk/getmondo/model/Account;", "isGoogleTestUser", "", "()Z", "getProfile", "()Lco/uk/getmondo/model/Profile;", "getSecondaryAccountId", "()Ljava/lang/String;", "state", "Lco/uk/getmondo/model/User$State;", "getState", "()Lco/uk/getmondo/model/User$State;", "getUserId", "getWaitlistProfile", "()Lco/uk/getmondo/model/WaitlistProfile;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "withAccount", "withProfile", "withProfileAndAccountInfo", "info", "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;", "withProfileAndWaitlistProfile", "withWaitlistProfile", "State", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ak {
   private final a account;
   private final ac profile;
   private final String secondaryAccountId;
   private final String userId;
   private final an waitlistProfile;

   public ak(String var) {
      this(var, (a)null, (ac)null, (an)null, (String)null, 30, (kotlin.d.b.i)null);
   }

   public ak(String var, a var) {
      this(var, var, (ac)null, (an)null, (String)null, 28, (kotlin.d.b.i)null);
   }

   public ak(String var, a var, ac var) {
      this(var, var, var, (an)null, (String)null, 24, (kotlin.d.b.i)null);
   }

   public ak(String var, a var, ac var, an var) {
      this(var, var, var, var, (String)null, 16, (kotlin.d.b.i)null);
   }

   public ak(String var, a var, ac var, an var, String var) {
      this.userId = var;
      this.account = var;
      this.profile = var;
      this.waitlistProfile = var;
      this.secondaryAccountId = var;
   }

   // $FF: synthetic method
   public ak(String var, a var, ac var, an var, String var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (a)null;
      }

      if((var & 4) != 0) {
         var = (ac)null;
      }

      if((var & 8) != 0) {
         var = (an)null;
      }

      if((var & 16) != 0) {
         var = (String)null;
      }

      this(var, var, var, var, var);
   }

   private final boolean g() {
      boolean var;
      if(this.profile != null && kotlin.d.b.l.a(this.profile.d(), "apmonzotest@gmail.com")) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final ak.a a() {
      Boolean var = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var, "BuildConfig.BANK");
      ak.a var;
      if(var.booleanValue()) {
         if(this.profile == null) {
            var = ak.a.NO_PROFILE;
         } else if(this.account == null) {
            var = ak.a.ACCOUNT_CREATION;
         } else if(!this.account.c()) {
            var = ak.a.NO_CARD;
         } else {
            var = ak.a.HAS_ACCOUNT;
         }
      } else if(this.account instanceof ab && !((ab)this.account).h() && !this.g()) {
         var = ak.a.INITIAL_TOPUP_REQUIRED;
      } else {
         a var = this.account;
         if(var != null && !var.c()) {
            var = ak.a.NO_CARD;
         } else if(this.account != null) {
            var = ak.a.HAS_ACCOUNT;
         } else {
            an var = this.waitlistProfile;
            boolean var;
            if(var != null) {
               var = var.a();
            } else {
               var = false;
            }

            if(var) {
               var = ak.a.INELIGIBLE;
            } else {
               var = this.waitlistProfile;
               if(var != null) {
                  var = var.f();
               } else {
                  var = false;
               }

               if(var) {
                  var = ak.a.ACCOUNT_CREATION;
               } else if(this.profile != null) {
                  var = ak.a.ON_WAITLIST;
               } else {
                  var = ak.a.NO_PROFILE;
               }
            }
         }
      }

      return var;
   }

   public final ak a(co.uk.getmondo.common.accounts.l var) {
      kotlin.d.b.l.b(var, "info");
      ak var;
      if(var.a() == null) {
         ac var = var.a();
         var = a(this, (String)null, (a)var.a(), var, (an)null, var.c(), 9, (Object)null);
      } else {
         String var = var.a().b();
         ac var = var.a();
         var = a(this, var, var.b(), var, (an)null, var.c(), 8, (Object)null);
      }

      return var;
   }

   public final ak a(a var) {
      kotlin.d.b.l.b(var, "account");
      return a(this, (String)null, var, (ac)null, (an)null, (String)null, 29, (Object)null);
   }

   public final ak a(ac var) {
      kotlin.d.b.l.b(var, "profile");
      return a(this, var.b(), (a)null, var, (an)null, (String)null, 26, (Object)null);
   }

   public final ak a(ac var, an var) {
      kotlin.d.b.l.b(var, "profile");
      return a(this, var.b(), (a)null, var, var, (String)null, 18, (Object)null);
   }

   public final ak a(an var) {
      return a(this, (String)null, (a)null, (ac)null, var, (String)null, 23, (Object)null);
   }

   public final ak a(String var, a var, ac var, an var, String var) {
      return new ak(var, var, var, var, var);
   }

   public final String b() {
      return this.userId;
   }

   public final a c() {
      return this.account;
   }

   public final ac d() {
      return this.profile;
   }

   public final an e() {
      return this.waitlistProfile;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof ak) {
               ak var = (ak)var;
               if(kotlin.d.b.l.a(this.userId, var.userId) && kotlin.d.b.l.a(this.account, var.account) && kotlin.d.b.l.a(this.profile, var.profile) && kotlin.d.b.l.a(this.waitlistProfile, var.waitlistProfile) && kotlin.d.b.l.a(this.secondaryAccountId, var.secondaryAccountId)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.secondaryAccountId;
   }

   public int hashCode() {
      int var = 0;
      String var = this.userId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      a var = this.account;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ac var = this.profile;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      an var = this.waitlistProfile;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.secondaryAccountId;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "User(userId=" + this.userId + ", account=" + this.account + ", profile=" + this.profile + ", waitlistProfile=" + this.waitlistProfile + ", secondaryAccountId=" + this.secondaryAccountId + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/model/User$State;", "", "(Ljava/lang/String;I)V", "NO_PROFILE", "ON_WAITLIST", "INELIGIBLE", "ACCOUNT_CREATION", "INITIAL_TOPUP_REQUIRED", "NO_CARD", "HAS_ACCOUNT", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      ACCOUNT_CREATION,
      HAS_ACCOUNT,
      INELIGIBLE,
      INITIAL_TOPUP_REQUIRED,
      NO_CARD,
      NO_PROFILE,
      ON_WAITLIST;

      static {
         ak.a var = new ak.a("NO_PROFILE", 0);
         NO_PROFILE = var;
         ak.a var = new ak.a("ON_WAITLIST", 1);
         ON_WAITLIST = var;
         ak.a var = new ak.a("INELIGIBLE", 2);
         INELIGIBLE = var;
         ak.a var = new ak.a("ACCOUNT_CREATION", 3);
         ACCOUNT_CREATION = var;
         ak.a var = new ak.a("INITIAL_TOPUP_REQUIRED", 4);
         INITIAL_TOPUP_REQUIRED = var;
         ak.a var = new ak.a("NO_CARD", 5);
         NO_CARD = var;
         ak.a var = new ak.a("HAS_ACCOUNT", 6);
         HAS_ACCOUNT = var;
      }
   }
}
