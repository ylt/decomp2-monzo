package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 '2\u00020\u0001:\u0001'B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B)\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003J3\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\t\u001a\u00020\u0006HÆ\u0001J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0006J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\u001bHÖ\u0001J\t\u0010\"\u001a\u00020\u0006HÖ\u0001J\u0018\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u001bH\u0016R\u001a\u0010\t\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\b\u001a\u0004\u0018\u00010\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\f\"\u0004\b\u0010\u0010\u000eR\u001a\u0010\u0007\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\f\"\u0004\b\u0012\u0010\u000eR\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\f\"\u0004\b\u0014\u0010\u000e¨\u0006("},
   d2 = {"Lco/uk/getmondo/model/SavedStripeCard;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "stripeCardId", "", "lastFour", "endDate", "brand", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBrand", "()Ljava/lang/String;", "setBrand", "(Ljava/lang/String;)V", "getEndDate", "setEndDate", "getLastFour", "setLastFour", "getStripeCardId", "setStripeCardId", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "endDateFormatted", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ae implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public ae a(Parcel var) {
         kotlin.d.b.l.b(var, "parcel");
         return new ae(var);
      }

      public ae[] a(int var) {
         return new ae[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final ae.a Companion = new ae.a((kotlin.d.b.i)null);
   private String brand;
   private String endDate;
   private String lastFour;
   private String stripeCardId;

   public ae(Parcel var) {
      kotlin.d.b.l.b(var, "source");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      this(var, var, var, var);
   }

   public ae(String var, String var, String var, String var) {
      kotlin.d.b.l.b(var, "stripeCardId");
      kotlin.d.b.l.b(var, "lastFour");
      kotlin.d.b.l.b(var, "brand");
      super();
      this.stripeCardId = var;
      this.lastFour = var;
      this.endDate = var;
      this.brand = var;
   }

   // $FF: synthetic method
   public ae(String var, String var, String var, String var, int var, kotlin.d.b.i var) {
      if((var & 4) != 0) {
         var = (String)null;
      }

      this(var, var, var, var);
   }

   public final String a() {
      Object var = null;
      StringBuilder var = null;
      String var = var;
      if(this.endDate != null) {
         String var = this.endDate;
         var = var;
         if(var != null) {
            if(var.length() != 4) {
               var = var;
            } else {
               var = new StringBuilder();
               var = this.endDate;
               if(var != null) {
                  if(var == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  var = var.substring(0, 2);
                  kotlin.d.b.l.a(var, "(this as java.lang.Strin…ing(startIndex, endIndex)");
               } else {
                  var = null;
               }

               var = var.append(var).append("/");
               var = this.endDate;
               var = (String)var;
               if(var != null) {
                  if(var == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  var = var.substring(2, 4);
                  kotlin.d.b.l.a(var, "(this as java.lang.Strin…ing(startIndex, endIndex)");
               }

               var = var.append(var).toString();
            }
         }
      }

      return var;
   }

   public final String b() {
      return this.stripeCardId;
   }

   public final String c() {
      return this.lastFour;
   }

   public final String d() {
      return this.brand;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof ae) {
               ae var = (ae)var;
               if(kotlin.d.b.l.a(this.stripeCardId, var.stripeCardId) && kotlin.d.b.l.a(this.lastFour, var.lastFour) && kotlin.d.b.l.a(this.endDate, var.endDate) && kotlin.d.b.l.a(this.brand, var.brand)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.stripeCardId;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.lastFour;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.endDate;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.brand;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "SavedStripeCard(stripeCardId=" + this.stripeCardId + ", lastFour=" + this.lastFour + ", endDate=" + this.endDate + ", brand=" + this.brand + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      kotlin.d.b.l.b(var, "dest");
      var.writeString(this.stripeCardId);
      var.writeString(this.lastFour);
      var.writeString(this.endDate);
      var.writeString(this.brand);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/model/SavedStripeCard$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/SavedStripeCard;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
