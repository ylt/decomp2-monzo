package co.uk.getmondo.d;

import io.realm.az;
import io.realm.bb;
import java.util.Iterator;
import java.util.List;

public class b implements io.realm.b, bb {
   private String accountId;
   private long balanceAmountValue;
   private String balanceCurrency;
   private az localSpend;
   private long spentTodayAmountValue;
   private String spentTodayCurrency;

   public b() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(String var, long var, String var, long var, String var, az var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.a(var);
      this.b(var);
      this.b(var);
      this.c(var);
      this.a(var);
   }

   public c a() {
      return new c(this.f(), this.g());
   }

   public void a(long var) {
      this.balanceAmountValue = var;
   }

   public void a(az var) {
      this.localSpend = var;
   }

   public void a(String var) {
      this.accountId = var;
   }

   public c b() {
      return new c(this.h(), this.i());
   }

   public void b(long var) {
      this.spentTodayAmountValue = var;
   }

   public void b(String var) {
      this.balanceCurrency = var;
   }

   public List c() {
      return this.j();
   }

   public void c(String var) {
      this.spentTodayCurrency = var;
   }

   public boolean d() {
      Iterator var = this.j().iterator();

      boolean var;
      while(true) {
         if(var.hasNext()) {
            if(((t)var.next()).a().l().b().equals(this.g())) {
               continue;
            }

            var = true;
            break;
         }

         var = false;
         break;
      }

      return var;
   }

   public String e() {
      return this.accountId;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               b var = (b)var;
               var = var;
               if(this.f() == var.f()) {
                  var = var;
                  if(this.h() == var.h()) {
                     if(this.e() != null) {
                        var = var;
                        if(!this.e().equals(var.e())) {
                           return var;
                        }
                     } else if(var.e() != null) {
                        var = var;
                        return var;
                     }

                     if(this.g() != null) {
                        var = var;
                        if(!this.g().equals(var.g())) {
                           return var;
                        }
                     } else if(var.g() != null) {
                        var = var;
                        return var;
                     }

                     if(this.i() != null) {
                        var = var;
                        if(!this.i().equals(var.i())) {
                           return var;
                        }
                     } else if(var.i() != null) {
                        var = var;
                        return var;
                     }

                     if(this.j() != null) {
                        var = this.j().equals(var.j());
                     } else {
                        var = var;
                        if(var.j() != null) {
                           var = false;
                        }
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public long f() {
      return this.balanceAmountValue;
   }

   public String g() {
      return this.balanceCurrency;
   }

   public long h() {
      return this.spentTodayAmountValue;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.e() != null) {
         var = this.e().hashCode();
      } else {
         var = 0;
      }

      int var = (int)(this.f() ^ this.f() >>> 32);
      int var;
      if(this.g() != null) {
         var = this.g().hashCode();
      } else {
         var = 0;
      }

      int var = (int)(this.h() ^ this.h() >>> 32);
      int var;
      if(this.i() != null) {
         var = this.i().hashCode();
      } else {
         var = 0;
      }

      if(this.j() != null) {
         var = this.j().hashCode();
      }

      return (var + ((var + (var * 31 + var) * 31) * 31 + var) * 31) * 31 + var;
   }

   public String i() {
      return this.spentTodayCurrency;
   }

   public az j() {
      return this.localSpend;
   }
}
