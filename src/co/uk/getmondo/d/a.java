package co.uk.getmondo.d;

import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001:\u0001\u0018J\u0010\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0003H&R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\rR\u0014\u0010\u0010\u001a\u00020\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0005R\u0014\u0010\u0011\u001a\u00020\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0005R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u0013X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/model/Account;", "", "cardActivated", "", "getCardActivated", "()Z", "created", "Lorg/threeten/bp/LocalDateTime;", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "description", "", "getDescription", "()Ljava/lang/String;", "id", "getId", "isPrepaidWithCard", "isRetail", "type", "Lco/uk/getmondo/model/Account$Type;", "getType", "()Lco/uk/getmondo/model/Account$Type;", "withCardActivated", "isActivated", "Type", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface a {
   a a(boolean var);

   String a();

   LocalDateTime b();

   boolean c();

   a.b d();

   boolean e();

   boolean f();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      public static boolean a(a var) {
         return kotlin.d.b.l.a(var.d(), a.b.RETAIL);
      }

      public static boolean b(a var) {
         boolean var;
         if(kotlin.d.b.l.a(var.d(), a.b.PREPAID) && var.c()) {
            var = true;
         } else {
            var = false;
         }

         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/model/Account$Type;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue", "()Ljava/lang/String;", "PREPAID", "RETAIL", "Find", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum b {
      public static final a.a Find;
      PREPAID,
      RETAIL;

      private final String value;

      static {
         a.b var = new a.b("PREPAID", 0, "uk_prepaid");
         PREPAID = var;
         a.b var = new a.b("RETAIL", 1, "uk_retail");
         RETAIL = var;
         Find = new a.a((kotlin.d.b.i)null);
      }

      protected b(String var) {
         kotlin.d.b.l.b(var, "value");
         super(var, var);
         this.value = var;
      }

      public static final a.b a(String var) {
         return Find.a(var);
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/model/Account$Type$Find;", "", "()V", "fromValue", "Lco/uk/getmondo/model/Account$Type;", "value", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final a.b a(String var) {
         Object[] var = (Object[])a.b.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(kotlin.d.b.l.a(((a.b)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         a.b var = (a.b)var;
         if(var == null) {
            var = a.b.PREPAID;
         }

         return var;
      }
   }
}
