package co.uk.getmondo.d;

import io.realm.az;
import io.realm.bb;
import io.realm.bn;
import java.util.Date;

public class aj implements bb, bn {
   private String amountCurrency;
   private String amountLocalCurrency;
   private long amountLocalValue;
   private long amountValue;
   private az attachments;
   private String bacsDirectDebitInstructionId;
   private co.uk.getmondo.payments.send.data.a.a bankDetails;
   private String category;
   private Date created;
   private String createdDateFormatted;
   private String declineReason;
   private String description;
   private boolean fromAtm;
   private boolean fromMonzoMe;
   private boolean hideAmount;
   private String id;
   private boolean includeInSpending;
   private u merchant;
   private String merchantDescription;
   private String notes;
   private aa peer;
   private boolean peerToPeer;
   private String scheme;
   private boolean settled;
   private boolean topUp;
   private Date updated;

   public aj() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public aj(String var, long var, String var, long var, String var, Date var, String var, Date var, String var, String var, String var, String var, boolean var, boolean var, u var, String var, boolean var, boolean var, boolean var, boolean var, az var, aa var, boolean var, co.uk.getmondo.payments.send.data.a.a var, String var, String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var);
      this.a(var);
      this.d(var);
      this.b(var);
      this.e(var);
      this.a(var);
      this.f(var);
      this.b(var);
      this.g(var);
      this.h(var);
      this.i(var);
      this.j(var);
      this.a(var);
      this.b(var);
      this.a(var);
      this.k(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
      this.a(var);
      this.a(var);
      this.g(var);
      this.a(var);
      this.l(var);
      this.m(var);
   }

   public az A() {
      return this.ab();
   }

   public aa B() {
      return this.ac();
   }

   public co.uk.getmondo.payments.send.data.a.a C() {
      return this.ad();
   }

   public af D() {
      return af.a(this.af());
   }

   public boolean E() {
      return this.ae();
   }

   public String F() {
      String var;
      if(this.ab() != null && !this.ab().isEmpty()) {
         var = ((d)this.ab().b()).b();
      } else {
         var = null;
      }

      return var;
   }

   public String G() {
      return this.ag();
   }

   public String H() {
      return this.id;
   }

   public long I() {
      return this.amountValue;
   }

   public String J() {
      return this.amountCurrency;
   }

   public long K() {
      return this.amountLocalValue;
   }

   public String L() {
      return this.amountLocalCurrency;
   }

   public Date M() {
      return this.created;
   }

   public String N() {
      return this.createdDateFormatted;
   }

   public Date O() {
      return this.updated;
   }

   public String P() {
      return this.merchantDescription;
   }

   public String Q() {
      return this.description;
   }

   public String R() {
      return this.declineReason;
   }

   public String S() {
      return this.notes;
   }

   public boolean T() {
      return this.hideAmount;
   }

   public boolean U() {
      return this.settled;
   }

   public u V() {
      return this.merchant;
   }

   public String W() {
      return this.category;
   }

   public boolean X() {
      return this.fromAtm;
   }

   public boolean Y() {
      return this.peerToPeer;
   }

   public boolean Z() {
      return this.topUp;
   }

   public void a(long var) {
      this.amountValue = var;
   }

   public void a(aa var) {
      this.peer = var;
   }

   public void a(u var) {
      this.merchant = var;
   }

   public void a(co.uk.getmondo.payments.send.data.a.a var) {
      this.bankDetails = var;
   }

   public void a(az var) {
      this.attachments = var;
   }

   public void a(String var) {
      this.k(var);
   }

   public void a(Date var) {
      this.created = var;
   }

   public void a(boolean var) {
      this.hideAmount = var;
   }

   public boolean a() {
      boolean var;
      if(this.V() != null && this.V().a()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean aa() {
      return this.includeInSpending;
   }

   public az ab() {
      return this.attachments;
   }

   public aa ac() {
      return this.peer;
   }

   public co.uk.getmondo.payments.send.data.a.a ad() {
      return this.bankDetails;
   }

   public boolean ae() {
      return this.fromMonzoMe;
   }

   public String af() {
      return this.scheme;
   }

   public String ag() {
      return this.bacsDirectDebitInstructionId;
   }

   public void b(long var) {
      this.amountLocalValue = var;
   }

   public void b(String var) {
      this.j(var);
   }

   public void b(Date var) {
      this.updated = var;
   }

   public void b(boolean var) {
      this.settled = var;
   }

   public boolean b() {
      boolean var;
      if(this.V() != null && this.c() == h.EATING_OUT && "USD".equals(this.L())) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public h c() {
      return h.a(this.W());
   }

   public void c(String var) {
      this.id = var;
   }

   public void c(boolean var) {
      this.fromAtm = var;
   }

   public com.c.b.b d() {
      return com.c.b.b.b(this.R());
   }

   public void d(String var) {
      this.amountCurrency = var;
   }

   public void d(boolean var) {
      this.peerToPeer = var;
   }

   public String e() {
      return this.S();
   }

   public void e(String var) {
      this.amountLocalCurrency = var;
   }

   public void e(boolean var) {
      this.topUp = var;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               aj var = (aj)var;
               var = var;
               if(this.I() == var.I()) {
                  var = var;
                  if(this.K() == var.K()) {
                     var = var;
                     if(this.T() == var.T()) {
                        var = var;
                        if(this.U() == var.U()) {
                           var = var;
                           if(this.X() == var.X()) {
                              var = var;
                              if(this.Y() == var.Y()) {
                                 var = var;
                                 if(this.Z() == var.Z()) {
                                    var = var;
                                    if(this.aa() == var.aa()) {
                                       var = var;
                                       if(this.ae() == var.ae()) {
                                          if(this.H() != null) {
                                             var = var;
                                             if(!this.H().equals(var.H())) {
                                                return var;
                                             }
                                          } else if(var.H() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.J() != null) {
                                             var = var;
                                             if(!this.J().equals(var.J())) {
                                                return var;
                                             }
                                          } else if(var.J() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.L() != null) {
                                             var = var;
                                             if(!this.L().equals(var.L())) {
                                                return var;
                                             }
                                          } else if(var.L() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.M() != null) {
                                             var = var;
                                             if(!this.M().equals(var.M())) {
                                                return var;
                                             }
                                          } else if(var.M() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.N() != null) {
                                             var = var;
                                             if(!this.N().equals(var.N())) {
                                                return var;
                                             }
                                          } else if(var.N() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.O() != null) {
                                             var = var;
                                             if(!this.O().equals(var.O())) {
                                                return var;
                                             }
                                          } else if(var.O() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.P() != null) {
                                             var = var;
                                             if(!this.P().equals(var.P())) {
                                                return var;
                                             }
                                          } else if(var.P() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.Q() != null) {
                                             var = var;
                                             if(!this.Q().equals(var.Q())) {
                                                return var;
                                             }
                                          } else if(var.Q() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.R() != null) {
                                             var = var;
                                             if(!this.R().equals(var.R())) {
                                                return var;
                                             }
                                          } else if(var.R() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.S() != null) {
                                             var = var;
                                             if(!this.S().equals(var.S())) {
                                                return var;
                                             }
                                          } else if(var.S() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.V() != null) {
                                             var = var;
                                             if(!this.V().equals(var.V())) {
                                                return var;
                                             }
                                          } else if(var.V() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.W() != null) {
                                             var = var;
                                             if(!this.W().equals(var.W())) {
                                                return var;
                                             }
                                          } else if(var.W() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.ab() != null) {
                                             var = var;
                                             if(!this.ab().equals(var.ab())) {
                                                return var;
                                             }
                                          } else if(var.ab() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.ac() != null) {
                                             var = var;
                                             if(!this.ac().equals(var.ac())) {
                                                return var;
                                             }
                                          } else if(var.ac() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.ad() != null) {
                                             var = var;
                                             if(!this.ad().equals(var.ad())) {
                                                return var;
                                             }
                                          } else if(var.ad() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.af() != null) {
                                             var = var;
                                             if(!this.af().equals(var.af())) {
                                                return var;
                                             }
                                          } else if(var.af() != null) {
                                             var = var;
                                             return var;
                                          }

                                          if(this.ag() != null) {
                                             var = this.ag().equals(var.ag());
                                          } else {
                                             var = var;
                                             if(var.ag() != null) {
                                                var = false;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public u f() {
      return this.V();
   }

   public void f(String var) {
      this.createdDateFormatted = var;
   }

   public void f(boolean var) {
      this.includeInSpending = var;
   }

   public c g() {
      return new c(this.I(), this.J());
   }

   public void g(String var) {
      this.merchantDescription = var;
   }

   public void g(boolean var) {
      this.fromMonzoMe = var;
   }

   public c h() {
      return new c(this.K(), this.L());
   }

   public void h(String var) {
      this.description = var;
   }

   public int hashCode() {
      byte var = 1;
      int var = 0;
      int var;
      if(this.H() != null) {
         var = this.H().hashCode();
      } else {
         var = 0;
      }

      int var = (int)(this.I() ^ this.I() >>> 32);
      int var;
      if(this.J() != null) {
         var = this.J().hashCode();
      } else {
         var = 0;
      }

      int var = (int)(this.K() ^ this.K() >>> 32);
      int var;
      if(this.L() != null) {
         var = this.L().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.M() != null) {
         var = this.M().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.N() != null) {
         var = this.N().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.O() != null) {
         var = this.O().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.P() != null) {
         var = this.P().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.Q() != null) {
         var = this.Q().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.R() != null) {
         var = this.R().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.S() != null) {
         var = this.S().hashCode();
      } else {
         var = 0;
      }

      byte var;
      if(this.T()) {
         var = 1;
      } else {
         var = 0;
      }

      byte var;
      if(this.U()) {
         var = 1;
      } else {
         var = 0;
      }

      int var;
      if(this.V() != null) {
         var = this.V().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.W() != null) {
         var = this.W().hashCode();
      } else {
         var = 0;
      }

      byte var;
      if(this.X()) {
         var = 1;
      } else {
         var = 0;
      }

      byte var;
      if(this.Y()) {
         var = 1;
      } else {
         var = 0;
      }

      byte var;
      if(this.Z()) {
         var = 1;
      } else {
         var = 0;
      }

      byte var;
      if(this.aa()) {
         var = 1;
      } else {
         var = 0;
      }

      int var;
      if(this.ab() != null) {
         var = this.ab().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.ac() != null) {
         var = this.ac().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.ad() != null) {
         var = this.ad().hashCode();
      } else {
         var = 0;
      }

      if(!this.ae()) {
         var = 0;
      }

      int var;
      if(this.af() != null) {
         var = this.af().hashCode();
      } else {
         var = 0;
      }

      if(this.ag() != null) {
         var = this.ag().hashCode();
      }

      return (var + ((var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + ((var + (var * 31 + var) * 31) * 31 + var) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var) * 31) * 31 + var;
   }

   public void i(String var) {
      this.declineReason = var;
   }

   public boolean i() {
      boolean var;
      if(!this.J().equals(this.L())) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public double j() {
      double var = 0.0D;
      double var = this.g().f();
      if(var != 0.0D) {
         var = this.h().f() / var;
      }

      return var;
   }

   public void j(String var) {
      this.notes = var;
   }

   public void k(String var) {
      this.category = var;
   }

   public boolean k() {
      boolean var;
      if(!this.U() && this.d().d() && !this.T() && this.g().a()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public String l() {
      return this.P().trim().replaceAll(" +", " ");
   }

   public void l(String var) {
      this.scheme = var;
   }

   public void m(String var) {
      this.bacsDirectDebitInstructionId = var;
   }

   public boolean m() {
      boolean var;
      if(this.S() != null && !this.S().equals("")) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean n() {
      boolean var;
      if(this.f() != null && co.uk.getmondo.common.k.p.c(this.f().j())) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public String o() {
      return co.uk.getmondo.common.c.a.a(this.v());
   }

   public boolean p() {
      boolean var;
      if(this.d().b() && co.uk.getmondo.common.k.p.c((String)this.d().a())) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean q() {
      return this.Y();
   }

   public boolean r() {
      boolean var;
      if(this.D() == af.FASTER_PAYMENT && this.ad() != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean s() {
      boolean var;
      if(this.D() == af.BACS) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean t() {
      boolean var;
      if(this.D() == af.OVERDRAFT) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public boolean u() {
      boolean var;
      if(this.s() && this.G() != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public Date v() {
      return this.M();
   }

   public String w() {
      return this.H();
   }

   public String x() {
      return this.Q();
   }

   public boolean y() {
      return this.T();
   }

   public boolean z() {
      return this.Z();
   }
}
