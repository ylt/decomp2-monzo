package co.uk.getmondo.d;

import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003J-\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/model/PaymentLimitsSection;", "", "id", "", "name", "limits", "", "Lco/uk/getmondo/model/PaymentLimit;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getId", "()Ljava/lang/String;", "getLimits", "()Ljava/util/List;", "getName", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class z {
   private final String id;
   private final List limits;
   private final String name;

   public z(String var, String var, List var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "limits");
      super();
      this.id = var;
      this.name = var;
      this.limits = var;
   }

   // $FF: synthetic method
   public z(String var, String var, List var, int var, kotlin.d.b.i var) {
      if((var & 4) != 0) {
         var = kotlin.a.m.a();
      }

      this(var, var, var);
   }

   public final String a() {
      return this.name;
   }

   public final List b() {
      return this.limits;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof z) {
               z var = (z)var;
               if(kotlin.d.b.l.a(this.id, var.id) && kotlin.d.b.l.a(this.name, var.name) && kotlin.d.b.l.a(this.limits, var.limits)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.name;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      List var = this.limits;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "PaymentLimitsSection(id=" + this.id + ", name=" + this.name + ", limits=" + this.limits + ")";
   }
}
