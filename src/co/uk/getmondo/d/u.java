package co.uk.getmondo.d;

import io.realm.bb;

public class u implements io.realm.ah, bb {
   public static final String TFL_MERCHANT_NAME = "Transport for London";
   private boolean atm;
   private String category;
   private String emoji;
   private String formattedAddress;
   private String groupId;
   private String id;
   private Double latitude;
   private String logoUrl;
   private Double longitude;
   private String name;
   private boolean online;

   public u() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public u(String var, String var, String var, String var, String var, String var, boolean var, boolean var, String var, Double var, Double var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
      this.a(var);
      this.b(var);
      this.g(var);
      this.a(var);
      this.b(var);
   }

   public void a(Double var) {
      this.latitude = var;
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(boolean var) {
      this.online = var;
   }

   boolean a() {
      return "Transport for London".equalsIgnoreCase(this.o());
   }

   public h b() {
      return h.a(this.r());
   }

   public void b(Double var) {
      this.longitude = var;
   }

   public void b(String var) {
      this.groupId = var;
   }

   public void b(boolean var) {
      this.atm = var;
   }

   public com.c.b.b c() {
      return com.c.b.b.b(this.u());
   }

   public void c(String var) {
      this.name = var;
   }

   public Double d() {
      return this.v();
   }

   public void d(String var) {
      this.logoUrl = var;
   }

   public Double e() {
      return this.w();
   }

   public void e(String var) {
      this.emoji = var;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               u var = (u)var;
               var = var;
               if(this.s() == var.s()) {
                  var = var;
                  if(this.t() == var.t()) {
                     if(this.m() != null) {
                        var = var;
                        if(!this.m().equals(var.m())) {
                           return var;
                        }
                     } else if(var.m() != null) {
                        var = var;
                        return var;
                     }

                     if(this.n() != null) {
                        var = var;
                        if(!this.n().equals(var.n())) {
                           return var;
                        }
                     } else if(var.n() != null) {
                        var = var;
                        return var;
                     }

                     if(this.o() != null) {
                        var = var;
                        if(!this.o().equals(var.o())) {
                           return var;
                        }
                     } else if(var.o() != null) {
                        var = var;
                        return var;
                     }

                     if(this.p() != null) {
                        var = var;
                        if(!this.p().equals(var.p())) {
                           return var;
                        }
                     } else if(var.p() != null) {
                        var = var;
                        return var;
                     }

                     if(this.q() != null) {
                        var = var;
                        if(!this.q().equals(var.q())) {
                           return var;
                        }
                     } else if(var.q() != null) {
                        var = var;
                        return var;
                     }

                     if(this.r() != null) {
                        var = var;
                        if(!this.r().equals(var.r())) {
                           return var;
                        }
                     } else if(var.r() != null) {
                        var = var;
                        return var;
                     }

                     if(this.u() != null) {
                        var = var;
                        if(!this.u().equals(var.u())) {
                           return var;
                        }
                     } else if(var.u() != null) {
                        var = var;
                        return var;
                     }

                     if(this.v() != null) {
                        var = var;
                        if(!this.v().equals(var.v())) {
                           return var;
                        }
                     } else if(var.v() != null) {
                        var = var;
                        return var;
                     }

                     if(this.w() != null) {
                        var = this.w().equals(var.w());
                     } else {
                        var = var;
                        if(var.w() != null) {
                           var = false;
                        }
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public String f() {
      String var;
      if(co.uk.getmondo.common.k.p.d(this.q())) {
         var = this.o();
      } else {
         var = this.q() + " " + this.o();
      }

      return var;
   }

   public void f(String var) {
      this.category = var;
   }

   public void g(String var) {
      this.formattedAddress = var;
   }

   public boolean g() {
      return this.t();
   }

   public String h() {
      return this.n();
   }

   public int hashCode() {
      byte var = 1;
      int var = 0;
      int var;
      if(this.m() != null) {
         var = this.m().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.n() != null) {
         var = this.n().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.o() != null) {
         var = this.o().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.p() != null) {
         var = this.p().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.q() != null) {
         var = this.q().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.r() != null) {
         var = this.r().hashCode();
      } else {
         var = 0;
      }

      byte var;
      if(this.s()) {
         var = 1;
      } else {
         var = 0;
      }

      if(!this.t()) {
         var = 0;
      }

      int var;
      if(this.u() != null) {
         var = this.u().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.v() != null) {
         var = this.v().hashCode();
      } else {
         var = 0;
      }

      if(this.w() != null) {
         var = this.w().hashCode();
      }

      return (var + (var + ((var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var) * 31) * 31) * 31 + var;
   }

   public String i() {
      return this.o();
   }

   public String j() {
      return this.p();
   }

   public boolean k() {
      return this.s();
   }

   public String l() {
      return this.q();
   }

   public String m() {
      return this.id;
   }

   public String n() {
      return this.groupId;
   }

   public String o() {
      return this.name;
   }

   public String p() {
      return this.logoUrl;
   }

   public String q() {
      return this.emoji;
   }

   public String r() {
      return this.category;
   }

   public boolean s() {
      return this.online;
   }

   public boolean t() {
      return this.atm;
   }

   public String u() {
      return this.formattedAddress;
   }

   public Double v() {
      return this.latitude;
   }

   public Double w() {
      return this.longitude;
   }
}
