package co.uk.getmondo.d;

import io.realm.bb;
import java.util.Date;

public class d implements bb, io.realm.d {
   private Date created;
   private String externalId;
   private String id;
   private String url;

   public d() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public d(String var, String var, Date var, String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.a(var);
      this.c(var);
   }

   public String a() {
      return this.c();
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(Date var) {
      this.created = var;
   }

   public String b() {
      return this.f();
   }

   public void b(String var) {
      this.externalId = var;
   }

   public String c() {
      return this.id;
   }

   public void c(String var) {
      this.url = var;
   }

   public String d() {
      return this.externalId;
   }

   public Date e() {
      return this.created;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               d var = (d)var;
               if(this.c() != null) {
                  var = var;
                  if(!this.c().equals(var.c())) {
                     return var;
                  }
               } else if(var.c() != null) {
                  var = var;
                  return var;
               }

               if(this.d() != null) {
                  var = var;
                  if(!this.d().equals(var.d())) {
                     return var;
                  }
               } else if(var.d() != null) {
                  var = var;
                  return var;
               }

               if(this.e() != null) {
                  var = var;
                  if(!this.e().equals(var.e())) {
                     return var;
                  }
               } else if(var.e() != null) {
                  var = var;
                  return var;
               }

               if(this.f() != null) {
                  var = this.f().equals(var.f());
               } else {
                  var = var;
                  if(var.f() != null) {
                     var = false;
                  }
               }
            }
         }
      }

      return var;
   }

   public String f() {
      return this.url;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.c() != null) {
         var = this.c().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.d() != null) {
         var = this.d().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.e() != null) {
         var = this.e().hashCode();
      } else {
         var = 0;
      }

      if(this.f() != null) {
         var = this.f().hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }
}
