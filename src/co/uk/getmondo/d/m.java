package co.uk.getmondo.d;

import android.net.Uri;
import io.realm.bb;
import java.util.Date;

public class m implements bb, io.realm.t {
   private String accountId;
   private String appUri;
   private f basicItemInfo;
   private Date created;
   private o goldenTicket;
   private String id;
   private boolean isDismissable;
   private r kycRejected;
   private v monthlySpendingReport;
   private String sddRejectedReason;
   private aj transaction;
   private String type;

   public m() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public m(String var, String var, Date var, String var, String var, o var, v var, aj var, r var, String var, f var, boolean var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.a(var);
      this.c(var);
      this.d(var);
      this.a(var);
      this.a(var);
      this.a(var);
      this.a(var);
      this.e(var);
      this.a(var);
      this.a(var);
   }

   public String a() {
      return this.l();
   }

   public void a(aj var) {
      this.transaction = var;
   }

   public void a(f var) {
      this.basicItemInfo = var;
   }

   public void a(o var) {
      this.goldenTicket = var;
   }

   public void a(r var) {
      this.kycRejected = var;
   }

   public void a(v var) {
      this.monthlySpendingReport = var;
   }

   public void a(String var) {
      this.id = var;
   }

   public void a(Date var) {
      this.created = var;
   }

   public void a(boolean var) {
      this.isDismissable = var;
   }

   public String b() {
      return this.m();
   }

   public void b(String var) {
      this.accountId = var;
   }

   public Date c() {
      return this.n();
   }

   public void c(String var) {
      this.type = var;
   }

   public Uri d() {
      Uri var;
      if(this.p() == null) {
         var = null;
      } else {
         var = Uri.parse(this.p());
      }

      return var;
   }

   public void d(String var) {
      this.appUri = var;
   }

   public com.c.b.b e() {
      return com.c.b.b.b(this.s());
   }

   public void e(String var) {
      this.sddRejectedReason = var;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               m var = (m)var;
               var = var;
               if(this.w() == var.w()) {
                  if(this.l() != null) {
                     var = var;
                     if(!this.l().equals(var.l())) {
                        return var;
                     }
                  } else if(var.l() != null) {
                     var = var;
                     return var;
                  }

                  if(this.m() != null) {
                     var = var;
                     if(!this.m().equals(var.m())) {
                        return var;
                     }
                  } else if(var.m() != null) {
                     var = var;
                     return var;
                  }

                  if(this.n() != null) {
                     var = var;
                     if(!this.n().equals(var.n())) {
                        return var;
                     }
                  } else if(var.n() != null) {
                     var = var;
                     return var;
                  }

                  if(this.o() != null) {
                     var = var;
                     if(!this.o().equals(var.o())) {
                        return var;
                     }
                  } else if(var.o() != null) {
                     var = var;
                     return var;
                  }

                  if(this.p() != null) {
                     var = var;
                     if(!this.p().equals(var.p())) {
                        return var;
                     }
                  } else if(var.p() != null) {
                     var = var;
                     return var;
                  }

                  if(this.q() != null) {
                     var = var;
                     if(!this.q().equals(var.q())) {
                        return var;
                     }
                  } else if(var.q() != null) {
                     var = var;
                     return var;
                  }

                  if(this.r() != null) {
                     var = var;
                     if(!this.r().equals(var.r())) {
                        return var;
                     }
                  } else if(var.r() != null) {
                     var = var;
                     return var;
                  }

                  if(this.s() != null) {
                     var = var;
                     if(!this.s().equals(var.s())) {
                        return var;
                     }
                  } else if(var.s() != null) {
                     var = var;
                     return var;
                  }

                  if(this.t() != null) {
                     var = var;
                     if(!this.t().equals(var.t())) {
                        return var;
                     }
                  } else if(var.t() != null) {
                     var = var;
                     return var;
                  }

                  if(this.u() != null) {
                     var = var;
                     if(!this.u().equals(var.u())) {
                        return var;
                     }
                  } else if(var.u() != null) {
                     var = var;
                     return var;
                  }

                  if(this.v() != null) {
                     var = this.v().equals(var.v());
                  } else {
                     var = var;
                     if(var.v() != null) {
                        var = false;
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public o f() {
      return this.q();
   }

   public v g() {
      return this.r();
   }

   public r h() {
      return this.t();
   }

   public int hashCode() {
      byte var = 0;
      int var;
      if(this.l() != null) {
         var = this.l().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.m() != null) {
         var = this.m().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.n() != null) {
         var = this.n().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.o() != null) {
         var = this.o().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.p() != null) {
         var = this.p().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.q() != null) {
         var = this.q().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.r() != null) {
         var = this.r().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.s() != null) {
         var = this.s().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.t() != null) {
         var = this.t().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.u() != null) {
         var = this.u().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.v() != null) {
         var = this.v().hashCode();
      } else {
         var = 0;
      }

      if(this.w()) {
         var = 1;
      }

      return (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public f i() {
      return this.v();
   }

   public co.uk.getmondo.feed.a.a.a j() {
      return co.uk.getmondo.feed.a.a.a.o.a(this.o());
   }

   public boolean k() {
      return this.w();
   }

   public String l() {
      return this.id;
   }

   public String m() {
      return this.accountId;
   }

   public Date n() {
      return this.created;
   }

   public String o() {
      return this.type;
   }

   public String p() {
      return this.appUri;
   }

   public o q() {
      return this.goldenTicket;
   }

   public v r() {
      return this.monthlySpendingReport;
   }

   public aj s() {
      return this.transaction;
   }

   public r t() {
      return this.kycRejected;
   }

   public String u() {
      return this.sddRejectedReason;
   }

   public f v() {
      return this.basicItemInfo;
   }

   public boolean w() {
      return this.isDismissable;
   }
}
