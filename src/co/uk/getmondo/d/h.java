package co.uk.getmondo.d;

public enum h {
   BILLS("bills", 2131362066, 2130837783, 2130837784, 2130837785, 2130837786, 2130837787, 2131689516),
   CASH("cash", 2131362067, 2130837788, 2130837790, 2130837791, 2130837792, 2130837793, 2131689517),
   EATING_OUT("eating_out", 2131362068, 2130837794, 2130837795, 2130837796, 2130837797, 2130837798, 2131689518),
   ENTERTAINMENT("entertainment", 2131362069, 2130837799, 2130837800, 2130837801, 2130837802, 2130837803, 2131689519),
   EXPENSES("expenses", 2131362070, 2130837804, 2130837805, 2130837806, 2130837807, 2130837808, 2131689520),
   GENERAL("general", 2131362071, 2130837809, 2130837810, 2130837811, 2130837812, 2130837813, 2131689521),
   GROCERIES("groceries", 2131362072, 2130837814, 2130837815, 2130837816, 2130837817, 2130837818, 2131689522),
   HOLIDAYS("holidays", 2131362073, 2130837819, 2130837820, 2130837821, 2130837822, 2130837823, 2131689523),
   SHOPPING("shopping", 2131362074, 2130837824, 2130837825, 2130837826, 2130837827, 2130837828, 2131689524),
   TRANSPORT("transport", 2131362075, 2130837829, 2130837830, 2130837831, 2130837832, 2130837833, 2131689525);

   private final String apiValue;
   private final int colorResource;
   private final int iconInverseLargeResource;
   private final int iconInverseResource;
   private final int iconResource;
   private final int iconRound;
   private final int iconRoundFull;
   private final int nameResource;

   private h(String var, int var, int var, int var, int var, int var, int var, int var) {
      this.apiValue = var;
      this.nameResource = var;
      this.iconResource = var;
      this.iconInverseResource = var;
      this.iconInverseLargeResource = var;
      this.iconRound = var;
      this.iconRoundFull = var;
      this.colorResource = var;
   }

   public static h a(String var) {
      h[] var = values();
      int var = var.length;
      int var = 0;

      h var;
      while(true) {
         if(var >= var) {
            var = GENERAL;
            break;
         }

         h var = var[var];
         if(var.apiValue.equals(var)) {
            var = var;
            break;
         }

         ++var;
      }

      return var;
   }

   public int a() {
      return this.nameResource;
   }

   public int b() {
      return this.colorResource;
   }

   public int c() {
      return this.iconInverseResource;
   }

   public int d() {
      return this.iconInverseLargeResource;
   }

   public int e() {
      return this.iconRoundFull;
   }

   public String f() {
      return this.apiValue;
   }
}
