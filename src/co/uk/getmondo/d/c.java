package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.math.BigDecimal;
import java.util.Locale;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 ?2\u00020\u0001:\u0001?B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u0017\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bB\u000f\b\u0014\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000fJ\u0006\u0010'\u001a\u00020\u0000J\u0006\u0010(\u001a\u00020\u0000J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\nHÆ\u0003J\u001d\u0010+\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\nHÆ\u0001J\b\u0010,\u001a\u00020\u0015H\u0016J\u0013\u0010-\u001a\u00020#2\b\u0010.\u001a\u0004\u0018\u00010/HÖ\u0003J\t\u00100\u001a\u00020\u0015HÖ\u0001J\u0011\u00101\u001a\u00020\u00002\u0006\u00102\u001a\u00020\u0003H\u0086\u0002J\u0011\u00103\u001a\u00020\u00002\u0006\u00104\u001a\u00020\u0003H\u0086\u0002J\u0006\u00105\u001a\u00020\u0000J\u000e\u00106\u001a\u00020\u00002\u0006\u00107\u001a\u00020\u0015J\u0011\u00108\u001a\u00020\u00002\u0006\u00109\u001a\u00020\u0003H\u0086\u0002J\b\u0010:\u001a\u00020\u0005H\u0016J\u0018\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\r2\u0006\u0010>\u001a\u00020\u0015H\u0016R\u0011\u0010\u0010\u001a\u00020\u00118F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u00158F¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u001d\u001a\u00020\u00058F¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010 \u001a\u00020\u00118F¢\u0006\u0006\u001a\u0004\b!\u0010\u0013R\u0011\u0010\"\u001a\u00020#8F¢\u0006\u0006\u001a\u0004\b\"\u0010$R\u0011\u0010%\u001a\u00020#8F¢\u0006\u0006\u001a\u0004\b%\u0010$R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001a¨\u0006@"},
   d2 = {"Lco/uk/getmondo/model/Amount;", "Landroid/os/Parcelable;", "value", "", "currencyCode", "", "(JLjava/lang/String;)V", "bigDecimal", "Ljava/math/BigDecimal;", "currency", "Lco/uk/getmondo/common/money/Currency;", "(Ljava/math/BigDecimal;Lco/uk/getmondo/common/money/Currency;)V", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "(JLco/uk/getmondo/common/money/Currency;)V", "absoluteDecimal", "", "getAbsoluteDecimal", "()D", "absoluteFractionalPart", "", "getAbsoluteFractionalPart", "()I", "absoluteIntegerPart", "getAbsoluteIntegerPart", "()J", "getCurrency", "()Lco/uk/getmondo/common/money/Currency;", "currencySymbol", "getCurrencySymbol", "()Ljava/lang/String;", "decimal", "getDecimal", "isDebit", "", "()Z", "isZero", "getValue", "abs", "ceil", "component1", "component2", "copy", "describeContents", "equals", "other", "", "hashCode", "minus", "valueToSubtract", "plus", "valueToAdd", "round", "split", "shares", "times", "valueToMultiply", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public c a(Parcel var) {
         kotlin.d.b.l.b(var, "parcel");
         return new c(var);
      }

      public c[] a(int var) {
         return new c[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final c.a Companion = new c.a((kotlin.d.b.i)null);
   private static final co.uk.getmondo.common.i.b defaultFormatter = new co.uk.getmondo.common.i.b(false, true, false, 5, (kotlin.d.b.i)null);
   private final co.uk.getmondo.common.i.c currency;
   private final long value;

   public c(long var, co.uk.getmondo.common.i.c var) {
      kotlin.d.b.l.b(var, "currency");
      super();
      this.value = var;
      this.currency = var;
   }

   public c(long var, String var) {
      kotlin.d.b.l.b(var, "currencyCode");
      this(var, new co.uk.getmondo.common.i.c(var));
   }

   protected c(Parcel var) {
      kotlin.d.b.l.b(var, "source");
      long var = var.readLong();
      Parcelable var = var.readParcelable(co.uk.getmondo.common.i.c.class.getClassLoader());
      kotlin.d.b.l.a(var, "source.readParcelable<Cu…::class.java.classLoader)");
      this(var, (co.uk.getmondo.common.i.c)var);
   }

   public c(BigDecimal var, co.uk.getmondo.common.i.c var) {
      kotlin.d.b.l.b(var, "bigDecimal");
      kotlin.d.b.l.b(var, "currency");
      this(var.scaleByPowerOfTen(var.a()).longValue(), var);
   }

   public static final c a(String var, co.uk.getmondo.common.i.c var) {
      kotlin.d.b.l.b(var, "currency");
      return Companion.a(var, var);
   }

   public final c a(int var) {
      double var = (double)this.value / (double)var;
      if(this.a()) {
         var = Math.floor(var);
      } else {
         var = Math.ceil(var);
      }

      return new c((long)var, this.currency);
   }

   public final c a(long var) {
      return this.a(this.value + var, this.currency);
   }

   public final c a(long var, co.uk.getmondo.common.i.c var) {
      kotlin.d.b.l.b(var, "currency");
      return new c(var, var);
   }

   public final boolean a() {
      boolean var;
      if(this.value < 0L) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final c b(long var) {
      return this.a(this.value - var, this.currency);
   }

   public final boolean b() {
      boolean var;
      if(this.value == 0L) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final long c() {
      return co.uk.getmondo.common.i.d.b(this.value, this.currency.a());
   }

   public final c c(long var) {
      return this.a(this.value * var, this.currency);
   }

   public final int d() {
      return co.uk.getmondo.common.i.d.a(this.value, this.currency.a());
   }

   public int describeContents() {
      return 0;
   }

   public final double e() {
      return Math.abs(this.f());
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof c)) {
            return var;
         }

         c var = (c)var;
         boolean var;
         if(this.value == var.value) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.currency, var.currency)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final double f() {
      return co.uk.getmondo.common.i.d.c(this.value, this.currency.a());
   }

   public final String g() {
      String var = this.currency.a(Locale.ENGLISH);
      kotlin.d.b.l.a(var, "currency.getSymbol(Locale.ENGLISH)");
      return var;
   }

   public final c h() {
      long var = co.uk.getmondo.common.i.d.d(this.value, this.currency.a());
      c var = this;
      if(var != this.value) {
         var = this.a(var, this.currency);
      }

      return var;
   }

   public int hashCode() {
      long var = this.value;
      int var = (int)(var ^ var >>> 32);
      co.uk.getmondo.common.i.c var = this.currency;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var + var * 31;
   }

   public final c i() {
      long var = co.uk.getmondo.common.i.d.e(this.value, this.currency.a());
      c var = this;
      if(var != this.value) {
         var = this.a(var, this.currency);
      }

      return var;
   }

   public final c j() {
      return this.a(Math.abs(this.value), this.currency);
   }

   public final long k() {
      return this.value;
   }

   public final co.uk.getmondo.common.i.c l() {
      return this.currency;
   }

   public String toString() {
      return Companion.a().a(this);
   }

   public void writeToParcel(Parcel var, int var) {
      kotlin.d.b.l.b(var, "dest");
      var.writeLong(this.value);
      var.writeParcelable((Parcelable)this.currency, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u0004\u0018\u00010\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/model/Amount$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/Amount;", "defaultFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "getDefaultFormatter", "()Lco/uk/getmondo/common/money/AmountFormatter;", "parseDecimal", "decimalString", "", "currency", "Lco/uk/getmondo/common/money/Currency;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      private final co.uk.getmondo.common.i.b a() {
         return c.defaultFormatter;
      }

      public final c a(String var, co.uk.getmondo.common.i.c var) {
         kotlin.d.b.l.b(var, "currency");
         c var;
         if(var != null) {
            boolean var;
            if(((CharSequence)var).length() == 0) {
               var = true;
            } else {
               var = false;
            }

            if(!var) {
               c var;
               try {
                  BigDecimal var = new BigDecimal(var);
                  var = new c(var, var);
               } catch (NumberFormatException var) {
                  var = null;
                  return var;
               }

               var = var;
               return var;
            }
         }

         var = null;
         return var;
      }
   }
}
