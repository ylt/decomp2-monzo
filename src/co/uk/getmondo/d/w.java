package co.uk.getmondo.d;

import io.realm.ap;
import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001BW\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t\u0012\u0006\u0010\f\u001a\u00020\t\u0012\u0006\u0010\r\u001a\u00020\t\u0012\u0006\u0010\u000e\u001a\u00020\u0003¢\u0006\u0002\u0010\u000fB\u0005¢\u0006\u0002\u0010\u0010J\u0013\u00101\u001a\u00020\u00052\b\u00102\u001a\u0004\u0018\u000103H\u0096\u0002J\b\u00104\u001a\u000205H\u0016J\b\u00106\u001a\u00020\u0003H\u0016R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u0011\u0010$\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b%\u0010\u001fR\u0011\u0010\u000e\u001a\u00020&8F¢\u0006\u0006\u001a\u0004\b'\u0010(R\u0011\u0010)\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b*\u0010\u001fR\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010!\"\u0004\b,\u0010#R\u0011\u0010-\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b.\u0010\u001fR\u0011\u0010/\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b0\u0010\u001f¨\u00067"},
   d2 = {"Lco/uk/getmondo/model/OverdraftStatus;", "Lio/realm/RealmObject;", "accountId", "", "active", "", "eligible", "currency", "accruedFeesAmount", "", "bufferAmount", "dailyFeeAmount", "limitAmount", "preapprovedLimitAmount", "chargeDate", "(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V", "()V", "_accruedFeesAmount", "_bufferAmount", "_chargeDate", "_currency", "_dailyFeeAmount", "_limitAmount", "_preapprovedLimitAmount", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "accruedFees", "Lco/uk/getmondo/model/Amount;", "getAccruedFees", "()Lco/uk/getmondo/model/Amount;", "getActive", "()Z", "setActive", "(Z)V", "buffer", "getBuffer", "Lorg/threeten/bp/LocalDate;", "getChargeDate", "()Lorg/threeten/bp/LocalDate;", "dailyFee", "getDailyFee", "getEligible", "setEligible", "limit", "getLimit", "preapprovedLimit", "getPreapprovedLimit", "equals", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class w extends bc implements ap {
   private long _accruedFeesAmount;
   private long _bufferAmount;
   private String _chargeDate;
   private String _currency;
   private long _dailyFeeAmount;
   private long _limitAmount;
   private long _preapprovedLimitAmount;
   private String accountId;
   private boolean active;
   private boolean eligible;

   public w() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
   }

   public w(String var, boolean var, boolean var, String var, long var, long var, long var, long var, long var, String var) {
      kotlin.d.b.l.b(var, "accountId");
      kotlin.d.b.l.b(var, "currency");
      kotlin.d.b.l.b(var, "chargeDate");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.a(var);
      this.b(var);
      this.b(var);
      this.a(var);
      this.b(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.c(var);
   }

   public final String a() {
      return this.j();
   }

   public void a(long var) {
      this._accruedFeesAmount = var;
   }

   public void a(String var) {
      this.accountId = var;
   }

   public void a(boolean var) {
      this.active = var;
   }

   public void b(long var) {
      this._bufferAmount = var;
   }

   public void b(String var) {
      this._currency = var;
   }

   public void b(boolean var) {
      this.eligible = var;
   }

   public final boolean b() {
      return this.k();
   }

   public void c(long var) {
      this._dailyFeeAmount = var;
   }

   public void c(String var) {
      this._chargeDate = var;
   }

   public final boolean c() {
      return this.l();
   }

   public final c d() {
      return new c(this.n(), this.m());
   }

   public void d(long var) {
      this._limitAmount = var;
   }

   public final c e() {
      return new c(this.o(), this.m());
   }

   public void e(long var) {
      this._preapprovedLimitAmount = var;
   }

   public boolean equals(Object var) {
      boolean var;
      if((w)this == var) {
         var = true;
      } else {
         Class var = this.getClass();
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(kotlin.d.b.l.a(var, var) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.OverdraftStatus");
            }

            w var = (w)var;
            if(kotlin.d.b.l.a(this.j(), ((w)var).j()) ^ true) {
               var = false;
            } else if(this.k() != ((w)var).k()) {
               var = false;
            } else if(this.l() != ((w)var).l()) {
               var = false;
            } else if(kotlin.d.b.l.a(this.m(), ((w)var).m()) ^ true) {
               var = false;
            } else if(this.n() != ((w)var).n()) {
               var = false;
            } else if(this.o() != ((w)var).o()) {
               var = false;
            } else if(this.p() != ((w)var).p()) {
               var = false;
            } else if(this.q() != ((w)var).q()) {
               var = false;
            } else if(this.r() != ((w)var).r()) {
               var = false;
            } else if(kotlin.d.b.l.a(this.s(), ((w)var).s()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public final c f() {
      return new c(this.p(), this.m());
   }

   public final c g() {
      return new c(this.q(), this.m());
   }

   public final c h() {
      return new c(this.r(), this.m());
   }

   public int hashCode() {
      return ((((((((this.j().hashCode() * 31 + Boolean.valueOf(this.k()).hashCode()) * 31 + Boolean.valueOf(this.l()).hashCode()) * 31 + this.m().hashCode()) * 31 + Long.valueOf(this.n()).hashCode()) * 31 + Long.valueOf(this.o()).hashCode()) * 31 + Long.valueOf(this.p()).hashCode()) * 31 + Long.valueOf(this.q()).hashCode()) * 31 + Long.valueOf(this.r()).hashCode()) * 31 + this.s().hashCode();
   }

   public final LocalDate i() {
      LocalDate var = LocalDate.a((CharSequence)this.s());
      kotlin.d.b.l.a(var, "LocalDate.parse(_chargeDate)");
      return var;
   }

   public String j() {
      return this.accountId;
   }

   public boolean k() {
      return this.active;
   }

   public boolean l() {
      return this.eligible;
   }

   public String m() {
      return this._currency;
   }

   public long n() {
      return this._accruedFeesAmount;
   }

   public long o() {
      return this._bufferAmount;
   }

   public long p() {
      return this._dailyFeeAmount;
   }

   public long q() {
      return this._limitAmount;
   }

   public long r() {
      return this._preapprovedLimitAmount;
   }

   public String s() {
      return this._chargeDate;
   }

   public String toString() {
      return "OverdraftStatus(accountId='" + this.j() + "', active=" + this.k() + ", eligible=" + this.l() + ", _currency='" + this.m() + "'," + " _accruedFeesAmount=" + this.n() + ", _bufferAmount=" + this.o() + ", _dailyFeeAmount=" + this.p() + ',' + " _limitAmount=" + this.q() + ", _preapprovedLimitAmount=" + this.r() + ", _chargeDate='" + this.s() + "')";
   }
}
