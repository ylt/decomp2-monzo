package co.uk.getmondo.d;

import co.uk.getmondo.api.model.VerificationType;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\bB\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u00ad\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0005HÆ\u0003J\t\u00103\u001a\u00020\u0005HÆ\u0003J\t\u00104\u001a\u00020\u0005HÆ\u0003J\t\u00105\u001a\u00020\u0005HÆ\u0003J\t\u00106\u001a\u00020\u0005HÆ\u0003J\t\u00107\u001a\u00020\u0005HÆ\u0003J\t\u00108\u001a\u00020\u0005HÆ\u0003J\t\u00109\u001a\u00020\u0005HÆ\u0003J\t\u0010:\u001a\u00020\u0005HÆ\u0003J\t\u0010;\u001a\u00020\u0005HÆ\u0003J\t\u0010<\u001a\u00020\u0005HÆ\u0003J\t\u0010=\u001a\u00020\u0005HÆ\u0003J\t\u0010>\u001a\u00020\u0005HÆ\u0003J\t\u0010?\u001a\u00020\u0005HÆ\u0003J\t\u0010@\u001a\u00020\u0005HÆ\u0003J\t\u0010A\u001a\u00020\u0005HÆ\u0003J\t\u0010B\u001a\u00020\u0005HÆ\u0003J\t\u0010C\u001a\u00020\u0005HÆ\u0003J\t\u0010D\u001a\u00020\u0005HÆ\u0003J\t\u0010E\u001a\u00020\u0005HÆ\u0003JÛ\u0001\u0010F\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\u00052\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\u00052\b\b\u0002\u0010\u0018\u001a\u00020\u0005HÆ\u0001J\u0013\u0010G\u001a\u00020H2\b\u0010I\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010J\u001a\u00020KHÖ\u0001J\t\u0010L\u001a\u00020MHÖ\u0001R\u0011\u0010\u0011\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0012\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001bR\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001bR\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001bR\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001bR\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001bR\u0011\u0010\n\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001bR\u0011\u0010\u0018\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001bR\u0011\u0010\u0016\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001bR\u0011\u0010\u0014\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001bR\u0011\u0010\u0015\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001bR\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b/\u00100¨\u0006N"},
   d2 = {"Lco/uk/getmondo/model/BalanceLimit;", "", "verificationType", "Lco/uk/getmondo/api/model/VerificationType;", "maxSingleCardPayment", "Lco/uk/getmondo/model/Amount;", "maxBalance", "maxDailyTopUp", "maxDailyTopUpRemaining", "maxTopUpOver30Days", "maxTopUpOver30DaysRemaining", "maxAnnualTopUp", "maxAnnualTopUpRemaining", "dailyWithdrawalLimit", "dailyWithdrawalLimitRemaining", "rolling30DayLimit", "rolling30DayLimitRemaining", "annualWithdrawalLimit", "annualWithdrawalLimitRemaining", "paymentsMaxSinglePayment", "paymentsMaxReceivedOver30Days", "paymentsMaxReceivedOver30DaysRemaining", "monzoMeMaxSinglePayment", "monzoMeMaxReceivedOver30Days", "monzoMeMaxReceivedOver30DaysRemaining", "(Lco/uk/getmondo/api/model/VerificationType;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getAnnualWithdrawalLimit", "()Lco/uk/getmondo/model/Amount;", "getAnnualWithdrawalLimitRemaining", "getDailyWithdrawalLimit", "getDailyWithdrawalLimitRemaining", "getMaxAnnualTopUp", "getMaxAnnualTopUpRemaining", "getMaxBalance", "getMaxDailyTopUp", "getMaxDailyTopUpRemaining", "getMaxSingleCardPayment", "getMaxTopUpOver30Days", "getMaxTopUpOver30DaysRemaining", "getMonzoMeMaxReceivedOver30Days", "getMonzoMeMaxReceivedOver30DaysRemaining", "getMonzoMeMaxSinglePayment", "getPaymentsMaxReceivedOver30Days", "getPaymentsMaxReceivedOver30DaysRemaining", "getPaymentsMaxSinglePayment", "getRolling30DayLimit", "getRolling30DayLimitRemaining", "getVerificationType", "()Lco/uk/getmondo/api/model/VerificationType;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   private final c annualWithdrawalLimit;
   private final c annualWithdrawalLimitRemaining;
   private final c dailyWithdrawalLimit;
   private final c dailyWithdrawalLimitRemaining;
   private final c maxAnnualTopUp;
   private final c maxAnnualTopUpRemaining;
   private final c maxBalance;
   private final c maxDailyTopUp;
   private final c maxDailyTopUpRemaining;
   private final c maxSingleCardPayment;
   private final c maxTopUpOver30Days;
   private final c maxTopUpOver30DaysRemaining;
   private final c monzoMeMaxReceivedOver30Days;
   private final c monzoMeMaxReceivedOver30DaysRemaining;
   private final c monzoMeMaxSinglePayment;
   private final c paymentsMaxReceivedOver30Days;
   private final c paymentsMaxReceivedOver30DaysRemaining;
   private final c paymentsMaxSinglePayment;
   private final c rolling30DayLimit;
   private final c rolling30DayLimitRemaining;
   private final VerificationType verificationType;

   public e(VerificationType var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var, c var) {
      kotlin.d.b.l.b(var, "verificationType");
      kotlin.d.b.l.b(var, "maxSingleCardPayment");
      kotlin.d.b.l.b(var, "maxBalance");
      kotlin.d.b.l.b(var, "maxDailyTopUp");
      kotlin.d.b.l.b(var, "maxDailyTopUpRemaining");
      kotlin.d.b.l.b(var, "maxTopUpOver30Days");
      kotlin.d.b.l.b(var, "maxTopUpOver30DaysRemaining");
      kotlin.d.b.l.b(var, "maxAnnualTopUp");
      kotlin.d.b.l.b(var, "maxAnnualTopUpRemaining");
      kotlin.d.b.l.b(var, "dailyWithdrawalLimit");
      kotlin.d.b.l.b(var, "dailyWithdrawalLimitRemaining");
      kotlin.d.b.l.b(var, "rolling30DayLimit");
      kotlin.d.b.l.b(var, "rolling30DayLimitRemaining");
      kotlin.d.b.l.b(var, "annualWithdrawalLimit");
      kotlin.d.b.l.b(var, "annualWithdrawalLimitRemaining");
      kotlin.d.b.l.b(var, "paymentsMaxSinglePayment");
      kotlin.d.b.l.b(var, "paymentsMaxReceivedOver30Days");
      kotlin.d.b.l.b(var, "paymentsMaxReceivedOver30DaysRemaining");
      kotlin.d.b.l.b(var, "monzoMeMaxSinglePayment");
      kotlin.d.b.l.b(var, "monzoMeMaxReceivedOver30Days");
      kotlin.d.b.l.b(var, "monzoMeMaxReceivedOver30DaysRemaining");
      super();
      this.verificationType = var;
      this.maxSingleCardPayment = var;
      this.maxBalance = var;
      this.maxDailyTopUp = var;
      this.maxDailyTopUpRemaining = var;
      this.maxTopUpOver30Days = var;
      this.maxTopUpOver30DaysRemaining = var;
      this.maxAnnualTopUp = var;
      this.maxAnnualTopUpRemaining = var;
      this.dailyWithdrawalLimit = var;
      this.dailyWithdrawalLimitRemaining = var;
      this.rolling30DayLimit = var;
      this.rolling30DayLimitRemaining = var;
      this.annualWithdrawalLimit = var;
      this.annualWithdrawalLimitRemaining = var;
      this.paymentsMaxSinglePayment = var;
      this.paymentsMaxReceivedOver30Days = var;
      this.paymentsMaxReceivedOver30DaysRemaining = var;
      this.monzoMeMaxSinglePayment = var;
      this.monzoMeMaxReceivedOver30Days = var;
      this.monzoMeMaxReceivedOver30DaysRemaining = var;
   }

   public final VerificationType a() {
      return this.verificationType;
   }

   public final c b() {
      return this.maxSingleCardPayment;
   }

   public final c c() {
      return this.maxBalance;
   }

   public final c d() {
      return this.maxDailyTopUp;
   }

   public final c e() {
      return this.maxDailyTopUpRemaining;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label66: {
            if(var instanceof e) {
               e var = (e)var;
               if(kotlin.d.b.l.a(this.verificationType, var.verificationType) && kotlin.d.b.l.a(this.maxSingleCardPayment, var.maxSingleCardPayment) && kotlin.d.b.l.a(this.maxBalance, var.maxBalance) && kotlin.d.b.l.a(this.maxDailyTopUp, var.maxDailyTopUp) && kotlin.d.b.l.a(this.maxDailyTopUpRemaining, var.maxDailyTopUpRemaining) && kotlin.d.b.l.a(this.maxTopUpOver30Days, var.maxTopUpOver30Days) && kotlin.d.b.l.a(this.maxTopUpOver30DaysRemaining, var.maxTopUpOver30DaysRemaining) && kotlin.d.b.l.a(this.maxAnnualTopUp, var.maxAnnualTopUp) && kotlin.d.b.l.a(this.maxAnnualTopUpRemaining, var.maxAnnualTopUpRemaining) && kotlin.d.b.l.a(this.dailyWithdrawalLimit, var.dailyWithdrawalLimit) && kotlin.d.b.l.a(this.dailyWithdrawalLimitRemaining, var.dailyWithdrawalLimitRemaining) && kotlin.d.b.l.a(this.rolling30DayLimit, var.rolling30DayLimit) && kotlin.d.b.l.a(this.rolling30DayLimitRemaining, var.rolling30DayLimitRemaining) && kotlin.d.b.l.a(this.annualWithdrawalLimit, var.annualWithdrawalLimit) && kotlin.d.b.l.a(this.annualWithdrawalLimitRemaining, var.annualWithdrawalLimitRemaining) && kotlin.d.b.l.a(this.paymentsMaxSinglePayment, var.paymentsMaxSinglePayment) && kotlin.d.b.l.a(this.paymentsMaxReceivedOver30Days, var.paymentsMaxReceivedOver30Days) && kotlin.d.b.l.a(this.paymentsMaxReceivedOver30DaysRemaining, var.paymentsMaxReceivedOver30DaysRemaining) && kotlin.d.b.l.a(this.monzoMeMaxSinglePayment, var.monzoMeMaxSinglePayment) && kotlin.d.b.l.a(this.monzoMeMaxReceivedOver30Days, var.monzoMeMaxReceivedOver30Days) && kotlin.d.b.l.a(this.monzoMeMaxReceivedOver30DaysRemaining, var.monzoMeMaxReceivedOver30DaysRemaining)) {
                  break label66;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final c f() {
      return this.maxTopUpOver30Days;
   }

   public final c g() {
      return this.maxTopUpOver30DaysRemaining;
   }

   public final c h() {
      return this.maxAnnualTopUp;
   }

   public int hashCode() {
      int var = 0;
      VerificationType var = this.verificationType;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      c var = this.maxSingleCardPayment;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxBalance;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxDailyTopUp;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxDailyTopUpRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxTopUpOver30Days;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxTopUpOver30DaysRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxAnnualTopUp;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.maxAnnualTopUpRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.dailyWithdrawalLimit;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.dailyWithdrawalLimitRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.rolling30DayLimit;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.rolling30DayLimitRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.annualWithdrawalLimit;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.annualWithdrawalLimitRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.paymentsMaxSinglePayment;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.paymentsMaxReceivedOver30Days;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.paymentsMaxReceivedOver30DaysRemaining;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.monzoMeMaxSinglePayment;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.monzoMeMaxReceivedOver30Days;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.monzoMeMaxReceivedOver30DaysRemaining;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public final c i() {
      return this.maxAnnualTopUpRemaining;
   }

   public final c j() {
      return this.dailyWithdrawalLimit;
   }

   public final c k() {
      return this.dailyWithdrawalLimitRemaining;
   }

   public final c l() {
      return this.rolling30DayLimit;
   }

   public final c m() {
      return this.rolling30DayLimitRemaining;
   }

   public final c n() {
      return this.annualWithdrawalLimit;
   }

   public final c o() {
      return this.annualWithdrawalLimitRemaining;
   }

   public final c p() {
      return this.paymentsMaxSinglePayment;
   }

   public final c q() {
      return this.paymentsMaxReceivedOver30Days;
   }

   public final c r() {
      return this.paymentsMaxReceivedOver30DaysRemaining;
   }

   public final c s() {
      return this.monzoMeMaxSinglePayment;
   }

   public final c t() {
      return this.monzoMeMaxReceivedOver30Days;
   }

   public String toString() {
      return "BalanceLimit(verificationType=" + this.verificationType + ", maxSingleCardPayment=" + this.maxSingleCardPayment + ", maxBalance=" + this.maxBalance + ", maxDailyTopUp=" + this.maxDailyTopUp + ", maxDailyTopUpRemaining=" + this.maxDailyTopUpRemaining + ", maxTopUpOver30Days=" + this.maxTopUpOver30Days + ", maxTopUpOver30DaysRemaining=" + this.maxTopUpOver30DaysRemaining + ", maxAnnualTopUp=" + this.maxAnnualTopUp + ", maxAnnualTopUpRemaining=" + this.maxAnnualTopUpRemaining + ", dailyWithdrawalLimit=" + this.dailyWithdrawalLimit + ", dailyWithdrawalLimitRemaining=" + this.dailyWithdrawalLimitRemaining + ", rolling30DayLimit=" + this.rolling30DayLimit + ", rolling30DayLimitRemaining=" + this.rolling30DayLimitRemaining + ", annualWithdrawalLimit=" + this.annualWithdrawalLimit + ", annualWithdrawalLimitRemaining=" + this.annualWithdrawalLimitRemaining + ", paymentsMaxSinglePayment=" + this.paymentsMaxSinglePayment + ", paymentsMaxReceivedOver30Days=" + this.paymentsMaxReceivedOver30Days + ", paymentsMaxReceivedOver30DaysRemaining=" + this.paymentsMaxReceivedOver30DaysRemaining + ", monzoMeMaxSinglePayment=" + this.monzoMeMaxSinglePayment + ", monzoMeMaxReceivedOver30Days=" + this.monzoMeMaxReceivedOver30Days + ", monzoMeMaxReceivedOver30DaysRemaining=" + this.monzoMeMaxReceivedOver30DaysRemaining + ")";
   }

   public final c u() {
      return this.monzoMeMaxReceivedOver30DaysRemaining;
   }
}
