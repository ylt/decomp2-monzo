package co.uk.getmondo.d;

import java.io.Serializable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J1\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u000e2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0007HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\f¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/model/Event;", "Ljava/io/Serializable;", "id", "", "type", "text", "bump", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V", "getBump", "()I", "getId", "()Ljava/lang/String;", "isWebEvent", "", "()Z", "getText", "getType", "component1", "component2", "component3", "component4", "copy", "equals", "other", "", "hashCode", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k implements Serializable {
   private final int bump;
   private final String id;
   private final String text;
   private final String type;

   public k(String var, String var, String var, int var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "type");
      kotlin.d.b.l.b(var, "text");
      super();
      this.id = var;
      this.type = var;
      this.text = var;
      this.bump = var;
   }

   public final boolean a() {
      return kotlin.h.j.a("webview", this.type, true);
   }

   public final String b() {
      return this.id;
   }

   public final String c() {
      return this.text;
   }

   public final int d() {
      return this.bump;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof k)) {
            return var;
         }

         k var = (k)var;
         var = var;
         if(!kotlin.d.b.l.a(this.id, var.id)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.type, var.type)) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.text, var.text)) {
            return var;
         }

         boolean var;
         if(this.bump == var.bump) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.id;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.type;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.text;
      if(var != null) {
         var = var.hashCode();
      }

      return ((var + var * 31) * 31 + var) * 31 + this.bump;
   }

   public String toString() {
      return "Event(id=" + this.id + ", type=" + this.type + ", text=" + this.text + ", bump=" + this.bump + ")";
   }
}
