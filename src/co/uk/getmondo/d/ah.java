package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/model/SddMigrationType;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "DISMISSIBLE", "PERSISTENT", "NONE", "Find", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum ah {
   DISMISSIBLE;

   public static final ah.a Find;
   NONE,
   PERSISTENT;

   private final String apiValue;

   static {
      ah var = new ah("DISMISSIBLE", 0, "dismissible");
      DISMISSIBLE = var;
      ah var = new ah("PERSISTENT", 1, "persistent");
      PERSISTENT = var;
      ah var = new ah("NONE", 2, "");
      NONE = var;
      Find = new ah.a((kotlin.d.b.i)null);
   }

   protected ah(String var) {
      kotlin.d.b.l.b(var, "apiValue");
      super(var, var);
      this.apiValue = var;
   }

   public static final ah a(String var) {
      return Find.a(var);
   }

   public final String a() {
      return this.apiValue;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/model/SddMigrationType$Find;", "", "()V", "fromApiType", "Lco/uk/getmondo/model/SddMigrationType;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final ah a(String var) {
         Object[] var = (Object[])ah.values();
         int var = 0;

         Object var;
         while(true) {
            if(var >= var.length) {
               var = null;
               break;
            }

            Object var = var[var];
            if(kotlin.d.b.l.a(((ah)var).a(), var)) {
               var = var;
               break;
            }

            ++var;
         }

         ah var = (ah)var;
         if(var == null) {
            var = ah.NONE;
         }

         return var;
      }
   }
}
