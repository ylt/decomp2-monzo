package co.uk.getmondo.d;

import io.realm.bb;
import org.threeten.bp.YearMonth;

public class v implements io.realm.aj, bb {
   private String amountCurrency;
   private long amountValue;
   private String id;
   private String month;

   public v() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public v(String var, long var, String var, String var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.a(var);
      this.b(var);
      this.c(var);
   }

   public c a() {
      return new c(this.d(), this.e());
   }

   public void a(long var) {
      this.amountValue = var;
   }

   public void a(String var) {
      this.id = var;
   }

   public YearMonth b() {
      return YearMonth.a((CharSequence)this.f());
   }

   public void b(String var) {
      this.amountCurrency = var;
   }

   public String c() {
      return this.id;
   }

   public void c(String var) {
      this.month = var;
   }

   public long d() {
      return this.amountValue;
   }

   public String e() {
      return this.amountCurrency;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               v var = (v)var;
               var = var;
               if(this.d() == var.d()) {
                  if(this.c() != null) {
                     var = var;
                     if(!this.c().equals(var.c())) {
                        return var;
                     }
                  } else if(var.c() != null) {
                     var = var;
                     return var;
                  }

                  if(this.e() != null) {
                     var = var;
                     if(!this.e().equals(var.e())) {
                        return var;
                     }
                  } else if(var.e() != null) {
                     var = var;
                     return var;
                  }

                  if(this.f() != null) {
                     var = this.f().equals(var.f());
                  } else {
                     var = var;
                     if(var.f() != null) {
                        var = false;
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public String f() {
      return this.month;
   }

   public int hashCode() {
      int var = 0;
      int var;
      if(this.c() != null) {
         var = this.c().hashCode();
      } else {
         var = 0;
      }

      int var = (int)(this.d() ^ this.d() >>> 32);
      int var;
      if(this.e() != null) {
         var = this.e().hashCode();
      } else {
         var = 0;
      }

      if(this.f() != null) {
         var = this.f().hashCode();
      }

      return (var + (var * 31 + var) * 31) * 31 + var;
   }
}
