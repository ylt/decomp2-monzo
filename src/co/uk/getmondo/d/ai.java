package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0018B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/model/Token;", "", "accessToken", "", "refreshData", "Lco/uk/getmondo/model/Token$RefreshData;", "(Ljava/lang/String;Lco/uk/getmondo/model/Token$RefreshData;)V", "getAccessToken", "()Ljava/lang/String;", "ageInMilliseconds", "", "getAgeInMilliseconds", "()J", "getRefreshData", "()Lco/uk/getmondo/model/Token$RefreshData;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "RefreshData", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ai {
   private final String accessToken;
   private final ai.a refreshData;

   public ai(String var, ai.a var) {
      kotlin.d.b.l.b(var, "accessToken");
      kotlin.d.b.l.b(var, "refreshData");
      super();
      this.accessToken = var;
      this.refreshData = var;
   }

   public final long a() {
      return System.currentTimeMillis() - this.refreshData.c();
   }

   public final String b() {
      return this.accessToken;
   }

   public final ai.a c() {
      return this.refreshData;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof ai) {
               ai var = (ai)var;
               if(kotlin.d.b.l.a(this.accessToken, var.accessToken) && kotlin.d.b.l.a(this.refreshData, var.refreshData)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.accessToken;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      ai.a var = this.refreshData;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "Token(accessToken=" + this.accessToken + ", refreshData=" + this.refreshData + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0007HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/model/Token$RefreshData;", "", "refreshToken", "", "expiresIn", "refreshAttempt", "createdAt", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V", "getCreatedAt", "()J", "getExpiresIn", "()Ljava/lang/String;", "getRefreshAttempt", "getRefreshToken", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private final long createdAt;
      private final String expiresIn;
      private final String refreshAttempt;
      private final String refreshToken;

      public a(String var, String var, String var, long var) {
         kotlin.d.b.l.b(var, "refreshToken");
         kotlin.d.b.l.b(var, "expiresIn");
         kotlin.d.b.l.b(var, "refreshAttempt");
         super();
         this.refreshToken = var;
         this.expiresIn = var;
         this.refreshAttempt = var;
         this.createdAt = var;
      }

      public final String a() {
         return this.refreshToken;
      }

      public final String b() {
         return this.refreshAttempt;
      }

      public final long c() {
         return this.createdAt;
      }

      public boolean equals(Object var) {
         boolean var = false;
         boolean var;
         if(this != var) {
            var = var;
            if(!(var instanceof ai.a)) {
               return var;
            }

            ai.a var = (ai.a)var;
            var = var;
            if(!kotlin.d.b.l.a(this.refreshToken, var.refreshToken)) {
               return var;
            }

            var = var;
            if(!kotlin.d.b.l.a(this.expiresIn, var.expiresIn)) {
               return var;
            }

            var = var;
            if(!kotlin.d.b.l.a(this.refreshAttempt, var.refreshAttempt)) {
               return var;
            }

            boolean var;
            if(this.createdAt == var.createdAt) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         String var = this.refreshToken;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.expiresIn;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         var = this.refreshAttempt;
         if(var != null) {
            var = var.hashCode();
         }

         long var = this.createdAt;
         return ((var + var * 31) * 31 + var) * 31 + (int)(var ^ var >>> 32);
      }

      public String toString() {
         return "RefreshData(refreshToken=" + this.refreshToken + ", expiresIn=" + this.expiresIn + ", refreshAttempt=" + this.refreshAttempt + ", createdAt=" + this.createdAt + ")";
      }
   }
}
