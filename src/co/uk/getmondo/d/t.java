package co.uk.getmondo.d;

import io.realm.bb;

public class t implements io.realm.af, bb {
   private long amountValue;
   private String currency;

   public t() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public t(String var, long var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.a(var);
   }

   public c a() {
      return new c(this.c(), this.b());
   }

   public void a(long var) {
      this.amountValue = var;
   }

   public void a(String var) {
      this.currency = var;
   }

   public String b() {
      return this.currency;
   }

   public long c() {
      return this.amountValue;
   }

   public boolean equals(Object var) {
      boolean var = true;
      if(this != var) {
         if(var != null && this.getClass() == var.getClass()) {
            t var = (t)var;
            if(this.c() != var.c()) {
               var = false;
            } else if(this.b() != null) {
               var = this.b().equals(var.b());
            } else if(var.b() != null) {
               var = false;
            }
         } else {
            var = false;
         }
      }

      return var;
   }

   public int hashCode() {
      int var;
      if(this.b() != null) {
         var = this.b().hashCode();
      } else {
         var = 0;
      }

      return var * 31 + (int)(this.c() ^ this.c() >>> 32);
   }
}
