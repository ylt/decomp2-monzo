package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B%\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0006HÆ\u0003J1\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aHÖ\u0003J\t\u0010\u001b\u001a\u00020\u0016HÖ\u0001J\u0006\u0010\u001c\u001a\u00020\u0018J\t\u0010\u001d\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u0016H\u0016R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\f¨\u0006#"},
   d2 = {"Lco/uk/getmondo/model/Country;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "name", "", "displayName", "alpha2", "alpha3", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAlpha2", "()Ljava/lang/String;", "getAlpha3", "getDisplayName", "getName", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "isUk", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public i a(Parcel var) {
         kotlin.d.b.l.b(var, "source");
         return new i(var);
      }

      public i[] a(int var) {
         return new i[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final i.a Companion = new i.a((kotlin.d.b.i)null);
   public static final i UNITED_KINGDOM = new i("United Kingdom", "the United Kingdom", "GB", "GBR");
   public static final i UNITED_STATES = new i("United States of America", "the USA", "US", "USA");
   private final String alpha2;
   private final String alpha3;
   private final String displayName;
   private final String name;

   public i(Parcel var) {
      kotlin.d.b.l.b(var, "source");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      this(var, var, var, var);
   }

   public i(String var, String var, String var, String var) {
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "displayName");
      kotlin.d.b.l.b(var, "alpha2");
      kotlin.d.b.l.b(var, "alpha3");
      super();
      this.name = var;
      this.displayName = var;
      this.alpha2 = var;
      this.alpha3 = var;
   }

   public static final i a(String var) {
      return Companion.a(var);
   }

   public static final i i() {
      return Companion.a();
   }

   public static final List j() {
      return Companion.b();
   }

   public final boolean a() {
      return kotlin.d.b.l.a(this.alpha3, UNITED_KINGDOM.alpha3);
   }

   public final String b() {
      return this.name;
   }

   public final String c() {
      return this.displayName;
   }

   public final String d() {
      return this.alpha2;
   }

   public int describeContents() {
      return 0;
   }

   public final String e() {
      return this.alpha3;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof i) {
               i var = (i)var;
               if(kotlin.d.b.l.a(this.name, var.name) && kotlin.d.b.l.a(this.displayName, var.displayName) && kotlin.d.b.l.a(this.alpha2, var.alpha2) && kotlin.d.b.l.a(this.alpha3, var.alpha3)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public final String f() {
      return this.name;
   }

   public final String g() {
      return this.alpha2;
   }

   public final String h() {
      return this.alpha3;
   }

   public int hashCode() {
      int var = 0;
      String var = this.name;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.displayName;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.alpha2;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.alpha3;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "Country(name=" + this.name + ", displayName=" + this.displayName + ", alpha2=" + this.alpha2 + ", alpha3=" + this.alpha3 + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      kotlin.d.b.l.b(var, "dest");
      var.writeString(this.name);
      var.writeString(this.displayName);
      var.writeString(this.alpha2);
      var.writeString(this.alpha3);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\tH\u0002J\u0014\u0010\n\u001a\u0004\u0018\u00010\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007J\b\u0010\r\u001a\u00020\u0005H\u0007J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\tH\u0007R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/model/Country$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/Country;", "UNITED_KINGDOM", "UNITED_STATES", "allCountries", "", "fromCode", "code", "", "getDefaultCountry", "prioritisedCountries", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      private final List c() {
         return kotlin.a.m.b(new i[]{new i("Afghanistan", "Afghanistan", "AF", "AFG"), new i("Åland Islands", "Åland Islands", "AX", "ALA"), new i("Albania", "Albania", "AL", "ALB"), new i("Algeria", "Algeria", "DZ", "DZA"), new i("American Samoa", "American Samoa", "AS", "ASM"), new i("Andorra", "Andorra", "AD", "AND"), new i("Angola", "Angola", "AO", "AGO"), new i("Anguilla", "Anguilla", "AI", "AIA"), new i("Antarctica", "Antarctica", "AQ", "ATA"), new i("Antigua and Barbuda", "Antigua and Barbuda", "AG", "ATG"), new i("Argentina", "Argentina", "AR", "ARG"), new i("Armenia", "Armenia", "AM", "ARM"), new i("Aruba", "Aruba", "AW", "ABW"), new i("Australia", "Australia", "AU", "AUS"), new i("Austria", "Austria", "AT", "AUT"), new i("Azerbaijan", "Azerbaijan", "AZ", "AZE"), new i("Bahamas", "the Bahamas", "BS", "BHS"), new i("Bahrain", "Bahrain", "BH", "BHR"), new i("Bangladesh", "Bangladesh", "BD", "BGD"), new i("Barbados", "Barbados", "BB", "BRB"), new i("Belarus", "Belarus", "BY", "BLR"), new i("Belgium", "Belgium", "BE", "BEL"), new i("Belize", "Belize", "BZ", "BLZ"), new i("Benin", "Benin", "BJ", "BEN"), new i("Bermuda", "Bermuda", "BM", "BMU"), new i("Bhutan", "Bhutan", "BT", "BTN"), new i("Bolivia (Plurinational State of)", "Bolivia", "BO", "BOL"), new i("Bonaire", "Bonaire", "BQ", "BES"), new i("Bosnia and Herzegovina", "Bosnia and Herzegovina", "BA", "BIH"), new i("Botswana", "Botswana", "BW", "BWA"), new i("Bouvet Island", "Bouvet Island", "BV", "BVT"), new i("Brazil", "Brazil", "BR", "BRA"), new i("British Indian Ocean Territory", "British Indian Ocean Territory", "IO", "IOT"), new i("Brunei Darussalam", "Brunei Darussalam", "BN", "BRN"), new i("Bulgaria", "Bulgaria", "BG", "BGR"), new i("Burkina Faso", "Burkina Faso", "BF", "BFA"), new i("Burundi", "Burundi", "BI", "BDI"), new i("Cambodia", "Cambodia", "KH", "KHM"), new i("Cameroon", "Cameroon", "CM", "CMR"), new i("Canada", "Canada", "CA", "CAN"), new i("Cabo Verde", "Cabo Verde", "CV", "CPV"), new i("Cayman Islands", "the Cayman Islands", "KY", "CYM"), new i("Central African Republic", "the Central African Republic", "CF", "CAF"), new i("Chad", "Chad", "TD", "TCD"), new i("Chile", "Chile", "CL", "CHL"), new i("China", "China", "CN", "CHN"), new i("Christmas Island", "Christmas Island", "CX", "CXR"), new i("Cocos (Keeling) Islands", "the Cocos (Keeling) Islands", "CC", "CCK"), new i("Colombia", "Colombia", "CO", "COL"), new i("Comoros", "the Comoros", "KM", "COM"), new i("Congo", "Congo", "CG", "COG"), new i("Congo (Democratic Republic of the)", "the Democratic Republic of the Congo", "CD", "COD"), new i("Cook Islands", "the Cook Islands", "CK", "COK"), new i("Costa Rica", "Costa Rica", "CR", "CRI"), new i("Côte d'Ivoire", "Côte d'Ivoire", "CI", "CIV"), new i("Croatia", "Croatia", "HR", "HRV"), new i("Cuba", "Cuba", "CU", "CUB"), new i("Curaçao", "Curaçao", "CW", "CUW"), new i("Cyprus", "Cyprus", "CY", "CYP"), new i("Czech Republic", "the Czech Republic", "CZ", "CZE"), new i("Denmark", "Denmark", "DK", "DNK"), new i("Djibouti", "Djibouti", "DJ", "DJI"), new i("Dominica", "Dominica", "DM", "DMA"), new i("Dominican Republic", "the Dominican Republic", "DO", "DOM"), new i("Ecuador", "Ecuador", "EC", "ECU"), new i("Egypt", "Egypt", "EG", "EGY"), new i("El Salvador", "El Salvador", "SV", "SLV"), new i("Equatorial Guinea", "Equatorial Guinea", "GQ", "GNQ"), new i("Eritrea", "Eritrea", "ER", "ERI"), new i("Estonia", "Estonia", "EE", "EST"), new i("Ethiopia", "Ethiopia", "ET", "ETH"), new i("Falkland Islands (Malvinas)", "the Falkland Islands", "FK", "FLK"), new i("Faroe Islands", "Faroe Islands", "FO", "FRO"), new i("Fiji", "Fiji", "FJ", "FJI"), new i("Finland", "Finland", "FI", "FIN"), new i("France", "France", "FR", "FRA"), new i("French Guiana", "French Guiana", "GF", "GUF"), new i("French Polynesia", "French Polynesia", "PF", "PYF"), new i("French Southern Territories", "the French Southern Territories", "TF", "ATF"), new i("Gabon", "Gabon", "GA", "GAB"), new i("Gambia", "the Gambia", "GM", "GMB"), new i("Georgia", "Georgia", "GE", "GEO"), new i("Germany", "Germany", "DE", "DEU"), new i("Ghana", "Ghana", "GH", "GHA"), new i("Gibraltar", "Gibraltar", "GI", "GIB"), new i("Greece", "Greece", "GR", "GRC"), new i("Greenland", "Greenland", "GL", "GRL"), new i("Grenada", "Grenada", "GD", "GRD"), new i("Guadeloupe", "Guadeloupe", "GP", "GLP"), new i("Guam", "Guam", "GU", "GUM"), new i("Guatemala", "Guatemala", "GT", "GTM"), new i("Guernsey", "Guernsey", "GG", "GGY"), new i("Guinea", "Guinea", "GN", "GIN"), new i("Guinea-Bissau", "Guinea-Bissau", "GW", "GNB"), new i("Guyana", "Guyana", "GY", "GUY"), new i("Haiti", "Haiti", "HT", "HTI"), new i("Heard Island and McDonald Islands", "Heard Island and McDonald Islands", "HM", "HMD"), new i("Holy See", "Holy See", "VA", "VAT"), new i("Honduras", "Honduras", "HN", "HND"), new i("Hong Kong", "Hong Kong", "HK", "HKG"), new i("Hungary", "Hungary", "HU", "HUN"), new i("Iceland", "Iceland", "IS", "ISL"), new i("India", "India", "IN", "IND"), new i("Indonesia", "Indonesia", "ID", "IDN"), new i("Iran (Islamic Republic of)", "Iran", "IR", "IRN"), new i("Iraq", "Iraq", "IQ", "IRQ"), new i("Ireland", "Ireland", "IE", "IRL"), new i("Isle of Man", "the Isle of Man", "IM", "IMN"), new i("Israel", "Israel", "IL", "ISR"), new i("Italy", "Italy", "IT", "ITA"), new i("Jamaica", "Jamaica", "JM", "JAM"), new i("Japan", "Japan", "JP", "JPN"), new i("Jersey", "Jersey", "JE", "JEY"), new i("Jordan", "Jordan", "JO", "JOR"), new i("Kazakhstan", "Kazakhstan", "KZ", "KAZ"), new i("Kenya", "Kenya", "KE", "KEN"), new i("Kiribati", "Kiribati", "KI", "KIR"), new i("Korea (Democratic People's Republic of)", "North Korea", "KP", "PRK"), new i("Korea (Republic of)", "South Korea", "KR", "KOR"), new i("Kuwait", "Kuwait", "KW", "KWT"), new i("Kyrgyzstan", "Kyrgyzstan", "KG", "KGZ"), new i("Lao People's Democratic Republic", "Laos", "LA", "LAO"), new i("Latvia", "Latvia", "LV", "LVA"), new i("Lebanon", "Lebanon", "LB", "LBN"), new i("Lesotho", "Lesotho", "LS", "LSO"), new i("Liberia", "Liberia", "LR", "LBR"), new i("Libya", "Libya", "LY", "LBY"), new i("Liechtenstein", "Liechtenstein", "LI", "LIE"), new i("Lithuania", "Lithuania", "LT", "LTU"), new i("Luxembourg", "Luxembourg", "LU", "LUX"), new i("Macao", "Macao", "MO", "MAC"), new i("Macedonia (the former Yugoslav Republic of)", "Macedonia", "MK", "MKD"), new i("Madagascar", "Madagascar", "MG", "MDG"), new i("Malawi", "Malawi", "MW", "MWI"), new i("Malaysia", "Malaysia", "MY", "MYS"), new i("Maldives", "the Maldives", "MV", "MDV"), new i("Mali", "Mali", "ML", "MLI"), new i("Malta", "Malta", "MT", "MLT"), new i("Marshall Islands", "the Marshall Islands", "MH", "MHL"), new i("Martinique", "Martinique", "MQ", "MTQ"), new i("Mauritania", "Mauritania", "MR", "MRT"), new i("Mauritius", "Mauritius", "MU", "MUS"), new i("Mayotte", "Mayotte", "YT", "MYT"), new i("Mexico", "Mexico", "MX", "MEX"), new i("Micronesia (Federated States of)", "Micronesia", "FM", "FSM"), new i("Moldova (Republic of)", "Moldova", "MD", "MDA"), new i("Monaco", "Monaco", "MC", "MCO"), new i("Mongolia", "Mongolia", "MN", "MNG"), new i("Montenegro", "Montenegro", "ME", "MNE"), new i("Montserrat", "Montserrat", "MS", "MSR"), new i("Morocco", "Morocco", "MA", "MAR"), new i("Mozambique", "Mozambique", "MZ", "MOZ"), new i("Myanmar", "Myanmar", "MM", "MMR"), new i("Namibia", "Namibia", "NA", "NAM"), new i("Nauru", "Nauru", "NR", "NRU"), new i("Nepal", "Nepal", "NP", "NPL"), new i("Netherlands", "the Netherlands", "NL", "NLD"), new i("New Caledonia", "New Caledonia", "NC", "NCL"), new i("New Zealand", "New Zealand", "NZ", "NZL"), new i("Nicaragua", "Nicaragua", "NI", "NIC"), new i("Niger", "Niger", "NE", "NER"), new i("Nigeria", "Nigeria", "NG", "NGA"), new i("Niue", "Niue", "NU", "NIU"), new i("Norfolk Island", "Norfolk Island", "NF", "NFK"), new i("Northern Mariana Islands", "Northern Mariana Islands", "MP", "MNP"), new i("Norway", "Norway", "NO", "NOR"), new i("Oman", "Oman", "OM", "OMN"), new i("Pakistan", "Pakistan", "PK", "PAK"), new i("Palau", "Palau", "PW", "PLW"), new i("Palestine", "Palestine", "PS", "PSE"), new i("Panama", "Panama", "PA", "PAN"), new i("Papua New Guinea", "Papua New Guinea", "PG", "PNG"), new i("Paraguay", "Paraguay", "PY", "PRY"), new i("Peru", "Peru", "PE", "PER"), new i("Philippines", "the Philippines", "PH", "PHL"), new i("Pitcairn", "Pitcairn", "PN", "PCN"), new i("Poland", "Poland", "PL", "POL"), new i("Portugal", "Portugal", "PT", "PRT"), new i("Puerto Rico", "Puerto Rico", "PR", "PRI"), new i("Qatar", "Qatar", "QA", "QAT"), new i("Réunion", "Réunion", "RE", "REU"), new i("Romania", "Romania", "RO", "ROU"), new i("Russian Federation", "Russia", "RU", "RUS"), new i("Rwanda", "Rwanda", "RW", "RWA"), new i("Saint Barthélemy", "Saint Barthélemy", "BL", "BLM"), new i("Saint Helena", "Saint Helena, Ascension and Tristan da Cunha", "SH", "SHN"), new i("Saint Kitts and Nevis", "Saint Kitts and Nevis", "KN", "KNA"), new i("Saint Lucia", "Saint Lucia", "LC", "LCA"), new i("Saint Martin (French part)", "Saint Martin (French part)", "MF", "MAF"), new i("Saint Pierre and Miquelon", "Saint Pierre and Miquelon", "PM", "SPM"), new i("Saint Vincent and the Grenadines", "Saint Vincent and the Grenadines", "VC", "VCT"), new i("Samoa", "Samoa", "WS", "WSM"), new i("San Marino", "San Marino", "SM", "SMR"), new i("Sao Tome and Principe", "Sao Tome and Principe", "ST", "STP"), new i("Saudi Arabia", "Saudi Arabia", "SA", "SAU"), new i("Senegal", "Senegal", "SN", "SEN"), new i("Serbia", "Serbia", "RS", "SRB"), new i("Seychelles", "Seychelles", "SC", "SYC"), new i("Sierra Leone", "Sierra Leone", "SL", "SLE"), new i("Singapore", "Singapore", "SG", "SGP"), new i("Sint Maarten (Dutch part)", "Sint Maarten (Dutch part)", "SX", "SXM"), new i("Slovakia", "Slovakia", "SK", "SVK"), new i("Slovenia", "Slovenia", "SI", "SVN"), new i("Solomon Islands", "the Solomon Islands", "SB", "SLB"), new i("Somalia", "Somalia", "SO", "SOM"), new i("South Africa", "South Africa", "ZA", "ZAF"), new i("South Georgia and the South Sandwich Islands", "South Georgia and the South Sandwich Islands", "GS", "SGS"), new i("South Sudan", "South Sudan", "SS", "SSD"), new i("Spain", "Spain", "ES", "ESP"), new i("Sri Lanka", "Sri Lanka", "LK", "LKA"), new i("Sudan", "Sudan", "SD", "SDN"), new i("Suriname", "SuricommonName", "SR", "SUR"), new i("Svalbard and Jan Mayen", "Svalbard and Jan Mayen", "SJ", "SJM"), new i("Swaziland", "Swaziland", "SZ", "SWZ"), new i("Sweden", "Sweden", "SE", "SWE"), new i("Switzerland", "Switzerland", "CH", "CHE"), new i("Syrian Arab Republic", "Syria", "SY", "SYR"), new i("Taiwan", "Taiwan", "TW", "TWN"), new i("Tajikistan", "Tajikistan", "TJ", "TJK"), new i("Tanzania", "Tanzania", "TZ", "TZA"), new i("Thailand", "Thailand", "TH", "THA"), new i("Timor-Leste", "Timor-Leste", "TL", "TLS"), new i("Togo", "Togo", "TG", "TGO"), new i("Tokelau", "Tokelau", "TK", "TKL"), new i("Tonga", "Tonga", "TO", "TON"), new i("Trinidad and Tobago", "Trinidad and Tobago", "TT", "TTO"), new i("Tunisia", "Tunisia", "TN", "TUN"), new i("Turkey", "Turkey", "TR", "TUR"), new i("Turkmenistan", "Turkmenistan", "TM", "TKM"), new i("Turks and Caicos Islands", "the Turks and Caicos Islands", "TC", "TCA"), new i("Tuvalu", "Tuvalu", "TV", "TUV"), new i("Uganda", "Uganda", "UG", "UGA"), new i("Ukraine", "Ukraine", "UA", "UKR"), new i("United Arab Emirates", "the UAE", "AE", "ARE"), i.UNITED_KINGDOM, i.UNITED_STATES, new i("United States Minor Outlying Islands", "the United States Minor Outlying Islands", "UM", "UMI"), new i("Uruguay", "Uruguay", "UY", "URY"), new i("Uzbekistan", "Uzbekistan", "UZ", "UZB"), new i("Vanuatu", "Vanuatu", "VU", "VUT"), new i("Venezuela (Bolivarian Republic of)", "Venezuela", "VE", "VEN"), new i("Viet Nam", "Vietnam", "VN", "VNM"), new i("Virgin Islands (British)", "the Virgin Islands", "VG", "VGB"), new i("Virgin Islands (U.S.)", "the U.S. Virgin Islands", "VI", "VIR"), new i("Wallis and Futuna", "Wallis and Futuna", "WF", "WLF"), new i("Western Sahara", "Western Sahara", "EH", "ESH"), new i("Yemen", "Yemen", "YE", "YEM"), new i("Zambia", "Zambia", "ZM", "ZMB"), new i("Zimbabwe", "Zimbabwe", "ZW", "ZWE")});
      }

      public final i a() {
         return i.UNITED_KINGDOM;
      }

      public final i a(String var) {
         Iterator var = ((Iterable)((i.a)this).c()).iterator();

         Object var;
         while(true) {
            if(!var.hasNext()) {
               var = null;
               break;
            }

            Object var = var.next();
            i var = (i)var;
            boolean var;
            if(!kotlin.d.b.l.a(var.d(), var) && !kotlin.d.b.l.a(var.e(), var)) {
               var = false;
            } else {
               var = true;
            }

            if(var) {
               var = var;
               break;
            }
         }

         return (i)var;
      }

      public final List b() {
         List var = (List)(new ArrayList());
         var.add(i.UNITED_KINGDOM);
         var.add(i.UNITED_STATES);
         Iterator var = ((Iterable)((i.a)this).c()).iterator();

         while(var.hasNext()) {
            i var = (i)var.next();
            if(kotlin.d.b.l.a(var.e(), "GBR") ^ true && kotlin.d.b.l.a(var.e(), "USA") ^ true) {
               var.add(var);
            }
         }

         return var;
      }
   }
}
