package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiPaymentLimit;
import co.uk.getmondo.api.model.ApiPaymentLimits;
import co.uk.getmondo.api.model.ApiPaymentLimitsSection;
import co.uk.getmondo.d.x;
import co.uk.getmondo.d.y;
import co.uk.getmondo.d.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/model/mapper/PaymentLimitsMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/ApiPaymentLimits;", "Lco/uk/getmondo/model/PaymentLimits;", "()V", "apply", "apiValue", "mapLimit", "Lco/uk/getmondo/model/PaymentLimit;", "limit", "Lco/uk/getmondo/api/model/ApiPaymentLimit;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class m implements j {
   public static final m INSTANCE;

   static {
      new m();
   }

   private m() {
      INSTANCE = (m)this;
   }

   private final x a(ApiPaymentLimit var) {
      ApiPaymentLimit.LimitType var = var.c();
      long var;
      String var;
      Long var;
      x var;
      String var;
      switch(n.$EnumSwitchMapping$0[var.ordinal()]) {
      case 1:
         var = var.a();
         var = var.b();
         var = var.e();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var = var.longValue();
         String var = var.d();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         co.uk.getmondo.d.c var = new co.uk.getmondo.d.c(var, var);
         Long var = var.f();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var = var.longValue();
         String var = var.d();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var = (x)(new x.a(var, var, var, new co.uk.getmondo.d.c(var, var)));
         break;
      case 2:
         var = var.a();
         var = var.b();
         var = var.e();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var = var.longValue();
         Long var = var.f();
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var = (x)(new x.b(var, var, var, var.longValue()));
         break;
      case 3:
         var = (x)(new x.c(var.a(), var.b()));
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var;
   }

   public y a(ApiPaymentLimits var) {
      kotlin.d.b.l.b(var, "apiValue");
      Iterable var = (Iterable)var.a();
      Collection var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
      Iterator var = var.iterator();

      while(var.hasNext()) {
         ApiPaymentLimitsSection var = (ApiPaymentLimitsSection)var.next();
         String var = var.a();
         String var = var.b();
         Iterable var = (Iterable)var.c();
         Collection var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
         Iterator var = var.iterator();

         while(var.hasNext()) {
            ApiPaymentLimit var = (ApiPaymentLimit)var.next();
            var.add(INSTANCE.a(var));
         }

         var.add(new z(var, var, (List)var));
      }

      return new y((List)var);
   }
}
