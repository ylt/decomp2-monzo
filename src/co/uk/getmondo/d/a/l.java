package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.overdraft.ApiOverdraftStatus;
import co.uk.getmondo.d.w;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/model/mapper/OverdraftMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;", "Lco/uk/getmondo/model/OverdraftStatus;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l implements j {
   public static final l INSTANCE;

   static {
      new l();
   }

   private l() {
      INSTANCE = (l)this;
   }

   public w a(ApiOverdraftStatus var) {
      kotlin.d.b.l.b(var, "apiValue");
      return new w(var.a(), var.b(), var.c(), var.d(), var.e(), var.f(), var.g(), var.h(), var.i(), var.j());
   }
}
