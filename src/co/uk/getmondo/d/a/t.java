package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.feed.ApiAttachment;
import co.uk.getmondo.api.model.feed.ApiCounterparty;
import co.uk.getmondo.api.model.feed.ApiMetadata;
import co.uk.getmondo.api.model.feed.ApiTransaction;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.aj;
import io.realm.az;
import io.realm.bb;
import java.util.Date;
import java.util.Iterator;

public class t {
   private final co.uk.getmondo.common.accounts.b accountManager;
   private final b attachmentMapper;
   private final co.uk.getmondo.feed.a declineReasonStringProvider;
   private final k merchantMapper;

   public t(co.uk.getmondo.feed.a var, co.uk.getmondo.common.accounts.b var) {
      this.declineReasonStringProvider = var;
      this.accountManager = var;
      this.merchantMapper = new k();
      this.attachmentMapper = new b();
   }

   private boolean a(String var) {
      return co.uk.getmondo.common.k.p.c(var);
   }

   private co.uk.getmondo.payments.send.data.a.a b(ApiTransaction var) {
      ApiCounterparty var = var.q();
      co.uk.getmondo.payments.send.data.a.a var;
      if(this.accountManager.b() && var != null && var.b() != null && var.e() != null && var.f() != null) {
         var = new co.uk.getmondo.payments.send.data.a.a(var.b(), var.e(), var.f());
      } else {
         var = null;
      }

      return var;
   }

   private aa c(ApiTransaction var) {
      aa var;
      if(var.q() != null) {
         var = new aa(var.q().d(), var.q().b(), var.q().c(), (String)null, (String)null, (String)null, false);
      } else {
         var = null;
      }

      return var;
   }

   private az d(ApiTransaction var) {
      az var = new az();
      if(var.u() != null) {
         Iterator var = var.u().iterator();

         while(var.hasNext()) {
            ApiAttachment var = (ApiAttachment)var.next();
            var.a((bb)this.attachmentMapper.a(var));
         }
      }

      return var;
   }

   private co.uk.getmondo.d.u e(ApiTransaction var) {
      co.uk.getmondo.d.u var;
      if(var.b()) {
         var = this.merchantMapper.a(var.h());
      } else {
         var = null;
      }

      return var;
   }

   private String f(ApiTransaction var) {
      co.uk.getmondo.d.j var = co.uk.getmondo.d.j.a(var.p());
      return this.declineReasonStringProvider.a(var);
   }

   private String g(ApiTransaction var) {
      String var;
      if(var.e() && var.s().a() && !var.s().b().isEmpty()) {
         var = var.s().b();
      } else {
         var = null;
      }

      return var;
   }

   private String h(ApiTransaction var) {
      String var;
      if(var.q() != null) {
         if(var.q().a()) {
            var = var.q().b();
         } else {
            var = var.q().c();
         }
      } else if(var.b()) {
         var = var.h().g();
      } else {
         var = var.o();
      }

      return var;
   }

   private boolean i(ApiTransaction var) {
      boolean var = false;
      ApiMetadata var = null;
      if(var.e()) {
         var = var.s();
      }

      boolean var;
      if(var != null) {
         if(var.c() != null && var.c().booleanValue()) {
            var = true;
         } else {
            var = false;
         }
      } else {
         var = false;
      }

      if(var || var.f()) {
         var = true;
      }

      return var;
   }

   private String j(ApiTransaction var) {
      ApiMetadata var = var.s();
      String var;
      if(var != null) {
         var = var.f();
      } else {
         var = null;
      }

      return var;
   }

   public aj a(ApiTransaction var) {
      String var = var.g();
      long var = var.i();
      String var = var.m();
      long var = var.j();
      String var = var.n();
      Date var = var.k();
      String var = co.uk.getmondo.common.c.a.d(var.k());
      Date var = var.l();
      String var = var.o();
      String var = this.h(var);
      String var = this.f(var);
      String var = this.g(var);
      boolean var = this.i(var);
      boolean var = this.a(var.r());
      co.uk.getmondo.d.u var = this.e(var);
      String var = co.uk.getmondo.d.h.a(var.t()).f();
      boolean var = var.a();
      boolean var = var.d();
      boolean var = var.c();
      boolean var = var.v();
      az var = this.d(var);
      aa var = this.c(var);
      boolean var;
      if(var.s() != null && co.uk.getmondo.common.k.p.c(var.s().d())) {
         var = true;
      } else {
         var = false;
      }

      return new aj(var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, var, this.b(var), var.w(), this.j(var));
   }
}
