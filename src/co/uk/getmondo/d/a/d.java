package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiBalance;
import co.uk.getmondo.api.model.ApiLocalSpend;
import io.realm.az;
import io.realm.bb;
import java.util.Iterator;
import java.util.List;

public class d implements j {
   private final String accountId;

   public d(String var) {
      this.accountId = var;
   }

   private az a(List var) {
      az var = new az();
      Iterator var = var.iterator();

      while(var.hasNext()) {
         ApiLocalSpend var = (ApiLocalSpend)var.next();
         var.a((bb)(new co.uk.getmondo.d.t(var.b(), var.a())));
      }

      return var;
   }

   public co.uk.getmondo.d.b a(ApiBalance var) {
      return new co.uk.getmondo.d.b(this.accountId, var.a(), var.c(), var.b(), var.c(), this.a(var.d()));
   }
}
