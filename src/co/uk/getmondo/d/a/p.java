package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiProfile;
import co.uk.getmondo.d.ac;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/model/mapper/ProfileMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/ApiProfile;", "Lco/uk/getmondo/model/Profile;", "()V", "addressMapper", "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;", "apply", "apiProfile", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class p implements j {
   private final i addressMapper = new i();

   public ac a(ApiProfile var) {
      kotlin.d.b.l.b(var, "apiProfile");
      return new ac(var.a(), var.b(), var.c(), var.d(), var.g(), var.h(), Integer.valueOf(var.i()), this.addressMapper.a(var.e()), var.f());
   }
}
