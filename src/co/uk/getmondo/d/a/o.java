package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiAccount;
import co.uk.getmondo.d.ab;
import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0017\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\tH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/model/mapper/PrepaidAccountMapper;", "Lco/uk/getmondo/model/mapper/AccountMapper;", "Lco/uk/getmondo/model/PrepaidAccount;", "initialTopupCompleted", "", "cardActivated", "(ZZ)V", "apply", "apiValue", "Lco/uk/getmondo/api/model/ApiAccount;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o extends a {
   private final boolean initialTopupCompleted;

   public o(boolean var, boolean var) {
      super(var);
      this.initialTopupCompleted = var;
   }

   // $FF: synthetic method
   public o(boolean var, boolean var, int var, kotlin.d.b.i var) {
      if((var & 1) != 0) {
         var = false;
      }

      this(var, var);
   }

   public ab a(ApiAccount var) {
      kotlin.d.b.l.b(var, "apiValue");
      String var = var.a();
      String var = var.c();
      LocalDateTime var = var.b();
      boolean var = this.initialTopupCompleted;
      return new ab(var, var, var, this.a(), co.uk.getmondo.d.a.b.Find.a(var.f()), var);
   }
}
