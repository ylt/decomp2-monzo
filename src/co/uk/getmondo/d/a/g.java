package co.uk.getmondo.d.a;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private final javax.a.a transactionMapperProvider;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      $assertionsDisabled = var;
   }

   public g(javax.a.a var) {
      if(!$assertionsDisabled && var == null) {
         throw new AssertionError();
      } else {
         this.transactionMapperProvider = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new g(var);
   }

   public f a() {
      return new f((t)this.transactionMapperProvider.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
