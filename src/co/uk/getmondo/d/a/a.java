package co.uk.getmondo.d.a;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\b&\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0014\u0010\u0005\u001a\u00020\u0006X\u0084\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/model/mapper/AccountMapper;", "A", "Lco/uk/getmondo/model/Account;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/ApiAccount;", "cardActivated", "", "(Z)V", "getCardActivated", "()Z", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class a implements j {
   private final boolean cardActivated;

   public a(boolean var) {
      this.cardActivated = var;
   }

   protected final boolean a() {
      return this.cardActivated;
   }
}
