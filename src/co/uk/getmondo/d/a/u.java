package co.uk.getmondo.d.a;

public final class u implements b.a.b {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private final javax.a.a accountManagerProvider;
   private final javax.a.a declineReasonStringProvider;

   static {
      boolean var;
      if(!u.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      $assertionsDisabled = var;
   }

   public u(javax.a.a var, javax.a.a var) {
      if(!$assertionsDisabled && var == null) {
         throw new AssertionError();
      } else {
         this.declineReasonStringProvider = var;
         if(!$assertionsDisabled && var == null) {
            throw new AssertionError();
         } else {
            this.accountManagerProvider = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new u(var, var);
   }

   public t a() {
      return new t((co.uk.getmondo.feed.a)this.declineReasonStringProvider.b(), (co.uk.getmondo.common.accounts.b)this.accountManagerProvider.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
