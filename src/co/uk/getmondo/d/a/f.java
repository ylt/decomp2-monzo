package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.feed.ApiFeedItem;
import co.uk.getmondo.d.aj;
import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.TemporalAccessor;

public class f implements j {
   private final t transactionMapper;

   public f(t var) {
      this.transactionMapper = var;
   }

   private aj b(ApiFeedItem var) {
      aj var;
      if(var.f() == null) {
         var = null;
      } else {
         var = this.transactionMapper.a(var.f());
      }

      return var;
   }

   private co.uk.getmondo.d.o c(ApiFeedItem var) {
      co.uk.getmondo.d.o var;
      if(var.g() != null && !co.uk.getmondo.common.k.p.d(var.g().a())) {
         var = new co.uk.getmondo.d.o(var.g().a());
      } else {
         var = null;
      }

      return var;
   }

   private co.uk.getmondo.d.v d(ApiFeedItem var) {
      Object var = null;
      co.uk.getmondo.d.v var = (co.uk.getmondo.d.v)var;
      if(var.g() != null) {
         if(co.uk.getmondo.common.k.p.d(var.g().f())) {
            var = (co.uk.getmondo.d.v)var;
         } else {
            var = (co.uk.getmondo.d.v)var;
            if(var.g().f().equals("month")) {
               LocalDateTime var = var.g().d();
               var = var.c(Duration.a(var, var.g().e()).b() / 2L);
               var = new co.uk.getmondo.d.v(var.c(), var.g().c().longValue(), var.g().b(), YearMonth.a((TemporalAccessor)var).toString());
            }
         }
      }

      return var;
   }

   private co.uk.getmondo.d.r e(ApiFeedItem var) {
      co.uk.getmondo.d.r var;
      if(var.g() == null) {
         var = null;
      } else {
         var = new co.uk.getmondo.d.r(var.c(), var.g().g(), var.g().h());
      }

      return var;
   }

   private String f(ApiFeedItem var) {
      String var;
      if(var.g() == null) {
         var = null;
      } else {
         var = var.g().i();
      }

      return var;
   }

   private co.uk.getmondo.d.f g(ApiFeedItem var) {
      ApiFeedItem.Params var = var.g();
      co.uk.getmondo.d.f var;
      if(var == null) {
         var = null;
      } else {
         String var;
         if(var.j() != null) {
            var = var.j();
         } else {
            var = "";
         }

         String var;
         if(var.k() != null) {
            var = var.k();
         } else {
            var = "";
         }

         var = new co.uk.getmondo.d.f(var.c(), var, var, var.l());
      }

      return var;
   }

   public co.uk.getmondo.d.m a(ApiFeedItem var) {
      return new co.uk.getmondo.d.m(var.a(), var.b(), var.e(), var.d(), var.h(), this.c(var), this.d(var), this.b(var), this.e(var), this.f(var), this.g(var), var.i());
   }
}
