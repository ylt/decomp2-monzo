package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiAccount;
import co.uk.getmondo.d.ad;
import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/model/mapper/RetailAccountMapper;", "Lco/uk/getmondo/model/mapper/AccountMapper;", "Lco/uk/getmondo/model/RetailAccount;", "cardActivated", "", "(Z)V", "apply", "apiValue", "Lco/uk/getmondo/api/model/ApiAccount;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q extends a {
   public q(boolean var) {
      super(var);
   }

   public ad a(ApiAccount var) {
      kotlin.d.b.l.b(var, "apiValue");
      String var = var.a();
      String var = var.c();
      LocalDateTime var = var.b();
      boolean var = this.a();
      String var = var.e();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      String var = var.d();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      return new ad(var, var, var, var, co.uk.getmondo.d.a.b.Find.a(var.f()), var, var);
   }
}
