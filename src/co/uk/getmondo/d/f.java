package co.uk.getmondo.d;

import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\b\u0016\u0018\u00002\u00020\u0001B)\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0007B\u0005¢\u0006\u0002\u0010\bJ\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0096\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0016R\u0012\u0010\u0002\u001a\u00020\u00038\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\fR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\n\"\u0004\b\u0010\u0010\f¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/model/BasicItemInfo;", "Lio/realm/RealmObject;", "id", "", "title", "subtitle", "imageUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "()V", "getImageUrl", "()Ljava/lang/String;", "setImageUrl", "(Ljava/lang/String;)V", "getSubtitle", "setSubtitle", "getTitle", "setTitle", "equals", "", "other", "", "hashCode", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class f extends bc implements io.realm.i {
   private String id;
   private String imageUrl;
   private String subtitle;
   private String title;

   public f() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
   }

   public f(String var, String var, String var, String var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "title");
      kotlin.d.b.l.b(var, "subtitle");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
      this.d(var);
   }

   public final String a() {
      return this.e();
   }

   public void a(String var) {
      this.id = var;
   }

   public final String b() {
      return this.f();
   }

   public void b(String var) {
      this.title = var;
   }

   public final String c() {
      return this.g();
   }

   public void c(String var) {
      this.subtitle = var;
   }

   public String d() {
      return this.id;
   }

   public void d(String var) {
      this.imageUrl = var;
   }

   public String e() {
      return this.title;
   }

   public boolean equals(Object var) {
      boolean var;
      if((f)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(kotlin.d.b.l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.BasicItemInfo");
            }

            f var = (f)var;
            if(kotlin.d.b.l.a(this.d(), ((f)var).d()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.e(), ((f)var).e()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.f(), ((f)var).f()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.g(), ((f)var).g()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public String f() {
      return this.subtitle;
   }

   public String g() {
      return this.imageUrl;
   }

   public int hashCode() {
      int var = this.d().hashCode();
      int var = this.e().hashCode();
      int var = this.f().hashCode();
      String var = this.g();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var + ((var * 31 + var) * 31 + var) * 31;
   }
}
