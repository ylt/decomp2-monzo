package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.at;
import io.realm.bb;

public class aa implements Parcelable, co.uk.getmondo.payments.send.data.a.e, at, bb {
   public static final Creator CREATOR = new Creator() {
      public aa a(Parcel var) {
         return new aa(var);
      }

      public aa[] a(int var) {
         return new aa[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return this.a(var);
      }
   };
   private String contactName;
   private String contactPhoto;
   private boolean isEnriched;
   private String peerName;
   private String phoneNumber;
   private String userId;
   private String username;

   public aa() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   protected aa(Parcel var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var.readString());
      this.d(var.readString());
      this.e(var.readString());
      this.f(var.readString());
      this.g(var.readString());
      this.h(var.readString());
      boolean var;
      if(var.readByte() != 0) {
         var = true;
      } else {
         var = false;
      }

      this.b(var);
   }

   public aa(String var, String var, String var, String var, String var, String var, boolean var) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
      this.g(var);
      this.h(var);
      this.b(var);
   }

   public aa a(co.uk.getmondo.payments.send.a.b var) {
      String var = null;
      String var;
      if(var != null) {
         var = var.b();
      } else {
         var = null;
      }

      if(var != null) {
         var = var.e();
      }

      d.a.a.a("Enriching peer %s(phone: %s) | Contact name: %s photo: %s", new Object[]{this.a(), this.d(), var, var});
      this.a(var);
      this.b(var);
      boolean var;
      if(var != null) {
         var = true;
      } else {
         var = false;
      }

      this.a(var);
      return this;
   }

   public String a() {
      return this.j();
   }

   public void a(String var) {
      if((var != null || this.n() != null) && (this.n() == null || !this.n().equals(var))) {
         this.g(var);
      }

   }

   public void a(boolean var) {
      this.b(var);
   }

   public String b() {
      String var;
      if(this.n() != null) {
         var = this.n();
      } else if(this.k() != null) {
         var = this.k();
      } else if(this.l() != null) {
         var = this.l();
      } else {
         var = "";
      }

      return var;
   }

   public void b(String var) {
      if((var != null || this.o() != null) && (this.o() == null || !this.o().equals(var))) {
         this.h(var);
      }

   }

   public void b(boolean var) {
      this.isEnriched = var;
   }

   public String c() {
      String var = this.b();
      if(co.uk.getmondo.common.k.p.d(var)) {
         var = this.l();
      } else {
         var = var.split(" ")[0];
      }

      return var;
   }

   public void c(String var) {
      this.userId = var;
   }

   public String d() {
      return this.l();
   }

   public void d(String var) {
      this.peerName = var;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.m();
   }

   public void e(String var) {
      this.phoneNumber = var;
   }

   public boolean equals(Object var) {
      boolean var = true;
      boolean var = false;
      boolean var;
      if(this == var) {
         var = true;
      } else {
         var = var;
         if(var != null) {
            var = var;
            if(this.getClass() == var.getClass()) {
               aa var = (aa)var;
               var = var;
               if(this.p() == var.p()) {
                  if(this.j() != null) {
                     var = var;
                     if(!this.j().equals(var.j())) {
                        return var;
                     }
                  } else if(var.j() != null) {
                     var = var;
                     return var;
                  }

                  if(this.k() != null) {
                     var = var;
                     if(!this.k().equals(var.k())) {
                        return var;
                     }
                  } else if(var.k() != null) {
                     var = var;
                     return var;
                  }

                  if(this.l() != null) {
                     var = var;
                     if(!this.l().equals(var.l())) {
                        return var;
                     }
                  } else if(var.l() != null) {
                     var = var;
                     return var;
                  }

                  if(this.m() != null) {
                     var = var;
                     if(!this.m().equals(var.m())) {
                        return var;
                     }
                  } else if(var.m() != null) {
                     var = var;
                     return var;
                  }

                  if(this.n() != null) {
                     var = var;
                     if(!this.n().equals(var.n())) {
                        return var;
                     }
                  } else if(var.n() != null) {
                     var = var;
                     return var;
                  }

                  if(this.o() != null) {
                     var = this.o().equals(var.o());
                  } else {
                     var = var;
                     if(var.o() != null) {
                        var = false;
                     }
                  }
               }
            }
         }
      }

      return var;
   }

   public void f(String var) {
      this.username = var;
   }

   public boolean f() {
      boolean var;
      if(this.o() != null && !this.o().isEmpty()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public String g() {
      return this.o();
   }

   public void g(String var) {
      this.contactName = var;
   }

   public void h(String var) {
      this.contactPhoto = var;
   }

   public boolean h() {
      boolean var;
      if(this.n() != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public int hashCode() {
      byte var = 0;
      int var;
      if(this.j() != null) {
         var = this.j().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.k() != null) {
         var = this.k().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.l() != null) {
         var = this.l().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.m() != null) {
         var = this.m().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.n() != null) {
         var = this.n().hashCode();
      } else {
         var = 0;
      }

      int var;
      if(this.o() != null) {
         var = this.o().hashCode();
      } else {
         var = 0;
      }

      if(this.p()) {
         var = 1;
      }

      return (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public boolean i() {
      return co.uk.getmondo.common.k.p.c(this.l());
   }

   public String j() {
      return this.userId;
   }

   public String k() {
      return this.peerName;
   }

   public String l() {
      return this.phoneNumber;
   }

   public String m() {
      return this.username;
   }

   public String n() {
      return this.contactName;
   }

   public String o() {
      return this.contactPhoto;
   }

   public boolean p() {
      return this.isEnriched;
   }

   public void writeToParcel(Parcel var, int var) {
      var.writeString(this.j());
      var.writeString(this.k());
      var.writeString(this.l());
      var.writeString(this.m());
      var.writeString(this.n());
      var.writeString(this.o());
      byte var;
      if(this.p()) {
         var = 1;
      } else {
         var = 0;
      }

      var.writeByte((byte)var);
   }
}
