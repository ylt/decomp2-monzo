package co.uk.getmondo;

import co.uk.getmondo.common.g;
import co.uk.getmondo.common.m;
import co.uk.getmondo.common.q;
import co.uk.getmondo.common.accounts.d;

public final class b implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;

   static {
      boolean var;
      if(!b.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public b(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new b(var, var, var, var, var, var, var);
   }

   public void a(MonzoApplication var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (d)this.b.b();
         var.b = (co.uk.getmondo.b.a)this.c.b();
         var.c = (co.uk.getmondo.common.a.c)this.d.b();
         var.d = (q)this.e.b();
         var.e = (m)this.f.b();
         var.f = (g)this.g.b();
         var.g = (co.uk.getmondo.adjust.a)this.h.b();
      }
   }
}
