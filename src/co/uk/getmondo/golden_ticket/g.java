package co.uk.getmondo.golden_ticket;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiGoldenTicket;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.v;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final String e;
   private final boolean f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.common.a i;
   private String j;

   g(u var, u var, String var, boolean var, MonzoApi var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static void a(g.a var, io.reactivex.b.b var) throws Exception {
      var.a();
   }

   // $FF: synthetic method
   static void a(g var, co.uk.getmondo.common.b.a var) throws Exception {
      var.i.a(Impression.j(var.f));
   }

   // $FF: synthetic method
   static void a(g var, g.a var, ApiGoldenTicket var) throws Exception {
      var.j = var.b();
      if(var.a() == ApiGoldenTicket.GoldenTicketStatus.ACTIVE) {
         if(var.f) {
            var.c();
         } else {
            var.d();
         }
      } else {
         var.e();
      }

   }

   // $FF: synthetic method
   static void a(g var, g.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.a(var.j);
   }

   // $FF: synthetic method
   static void a(g var, g.a var, Throwable var) throws Exception {
      var.h.a(var, var);
   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.i(this.f));
      v var = this.g.goldenTicketStatus(this.e).b(this.c).a(this.d).b(h.a(var));
      var.getClass();
      this.a((io.reactivex.b.b)var.a(i.a(var)).a(j.a(this, var), k.a(this, var)));
      this.a((io.reactivex.b.b)var.f().doOnNext(l.a(this)).subscribe(m.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var);

      void b();

      void c();

      void d();

      void e();

      io.reactivex.n f();
   }
}
