package co.uk.getmondo.golden_ticket;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.VideoImageView;

public class GoldenTicketActivity_ViewBinding implements Unbinder {
   private GoldenTicketActivity a;

   public GoldenTicketActivity_ViewBinding(GoldenTicketActivity var, View var) {
      this.a = var;
      var.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var, 2131820912, "field 'progressBar'", ProgressBar.class);
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820914, "field 'titleTextView'", TextView.class);
      var.bodyTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820917, "field 'bodyTextView'", TextView.class);
      var.imageView = (ImageView)Utils.findRequiredViewAsType(var, 2131820916, "field 'imageView'", ImageView.class);
      var.videoImageView = (VideoImageView)Utils.findRequiredViewAsType(var, 2131820915, "field 'videoImageView'", VideoImageView.class);
      var.shareLinkButton = (Button)Utils.findRequiredViewAsType(var, 2131820918, "field 'shareLinkButton'", Button.class);
   }

   public void unbind() {
      GoldenTicketActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.progressBar = null;
         var.titleTextView = null;
         var.bodyTextView = null;
         var.imageView = null;
         var.videoImageView = null;
         var.shareLinkButton = null;
      }
   }
}
