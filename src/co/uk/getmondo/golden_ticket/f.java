package co.uk.getmondo.golden_ticket;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final d b;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(d var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(d var) {
      return new f(var);
   }

   public String a() {
      return (String)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
