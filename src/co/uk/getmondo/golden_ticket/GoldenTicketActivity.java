package co.uk.getmondo.golden_ticket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.VideoImageView;
import co.uk.getmondo.d.o;

public class GoldenTicketActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131820917)
   TextView bodyTextView;
   @BindView(2131820916)
   ImageView imageView;
   @BindView(2131820912)
   ProgressBar progressBar;
   @BindView(2131820918)
   Button shareLinkButton;
   @BindView(2131820914)
   TextView titleTextView;
   @BindView(2131820915)
   VideoImageView videoImageView;

   // $FF: synthetic method
   static co.uk.getmondo.common.b.a a(Object var) throws Exception {
      return co.uk.getmondo.common.b.a.a;
   }

   public static void a(Context var, o var, boolean var) {
      Intent var = new Intent(var, GoldenTicketActivity.class);
      var.putExtra("KEY_GOLDEN_TICKET_ID", var.a());
      var.putExtra("KEY_IS_FIRST_GOLDEN_TICKET", var);
      var.startActivity(var);
   }

   public void a() {
      this.progressBar.setVisibility(0);
   }

   public void a(String var) {
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362208), var, co.uk.getmondo.api.model.tracking.a.GOLDEN_TICKET));
   }

   public void b() {
      this.progressBar.setVisibility(8);
   }

   public void c() {
      this.titleTextView.setText(2131362206);
      this.bodyTextView.setText(2131362205);
      this.shareLinkButton.setVisibility(0);
      this.videoImageView.setVisibility(0);
   }

   public void d() {
      this.titleTextView.setText(2131362210);
      this.bodyTextView.setText(2131362209);
      this.shareLinkButton.setVisibility(0);
      this.videoImageView.setVisibility(0);
   }

   public void e() {
      this.titleTextView.setText(2131362212);
      this.bodyTextView.setText(2131362211);
      this.shareLinkButton.setVisibility(8);
      this.imageView.setVisibility(0);
   }

   public io.reactivex.n f() {
      return com.b.a.c.c.a(this.shareLinkButton).map(a.a());
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034172);
      ButterKnife.bind((Activity)this);
      Intent var = this.getIntent();
      this.l().a(new d(var.getStringExtra("KEY_GOLDEN_TICKET_ID"), var.getBooleanExtra("KEY_IS_FIRST_GOLDEN_TICKET", false))).a(this);
      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      this.videoImageView.a();
      this.a.b();
      super.onDestroy();
   }
}
