package co.uk.getmondo.golden_ticket;

import co.uk.getmondo.api.model.ApiGoldenTicket;

// $FF: synthetic class
final class j implements io.reactivex.c.g {
   private final g a;
   private final g.a b;

   private j(g var, g.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(g var, g.a var) {
      return new j(var, var);
   }

   public void a(Object var) {
      g.a(this.a, this.b, (ApiGoldenTicket)var);
   }
}
