package co.uk.getmondo.golden_ticket;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final d b;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(d var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(d var) {
      return new e(var);
   }

   public Boolean a() {
      return (Boolean)b.a.d.a(Boolean.valueOf(this.b.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
