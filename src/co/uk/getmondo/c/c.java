package co.uk.getmondo.c;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.ui.f;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/contacts/ContactsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/contacts/ContactsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.a e;
   private final co.uk.getmondo.common.accounts.b f;
   private final co.uk.getmondo.a.a g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.card.c i;
   private final o j;

   public c(u var, u var, co.uk.getmondo.common.a var, co.uk.getmondo.common.accounts.b var, co.uk.getmondo.a.a var, co.uk.getmondo.common.e.a var, co.uk.getmondo.card.c var, o var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "analyticsService");
      l.b(var, "accountManager");
      l.b(var, "balanceRepository");
      l.b(var, "apiErrorHandler");
      l.b(var, "cardManager");
      l.b(var, "featureFlagsStorage");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   public void a(final c.a var) {
      l.b(var, "view");
      super.a((f)var);
      this.e.a(Impression.Companion.aC());
      io.reactivex.b.a var;
      if(this.f.b()) {
         var.b();
         var = this.b;
         io.reactivex.b.b var = this.j.a().map((h)null.a).distinctUntilChanged().subscribe((g)(new g() {
            public final void a(Boolean varx) {
               c.a var = var;
               l.a(varx, "it");
               var.a(varx.booleanValue());
            }
         }));
         l.a(var, "featureFlagsStorage.feat…tContactsFabVisible(it) }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      } else {
         var.a();
         var.a(true);
      }

      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.g.b(this.f.c()).b(this.c).a(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = c.this.h;
            l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      l.a(var, "balanceRepository.refres…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = co.uk.getmondo.common.j.f.a(this.g.a(this.f.c()).observeOn(this.d).map((h)null.a), (r)this.i.a()).subscribe((g)(new g() {
         public final void a(kotlin.h varx) {
            co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)varx.c();
            if(((co.uk.getmondo.d.g)varx.d()).h()) {
               c.a var = var;
               l.a(var, "balance");
               var.a(var);
            } else {
               var.c();
            }

         }
      }), (g)null.a);
      l.a(var, "balanceRepository.balanc…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u0004H&J\b\u0010\u000b\u001a\u00020\u0004H&J\b\u0010\f\u001a\u00020\u0004H&¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/contacts/ContactsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "setContactsFabVisible", "", "visible", "", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showCardFrozen", "showPrepaidUi", "showRetailUi", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      void a();

      void a(co.uk.getmondo.d.c var);

      void a(boolean var);

      void b();

      void c();
   }
}
