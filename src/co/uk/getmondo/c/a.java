package co.uk.getmondo.c;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.support.v4.app.r;
import android.support.v4.view.ViewPager;
import android.support.v4.view.p;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.ViewPager.j;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.monzo.me.request.RequestMoneyFragment;
import co.uk.getmondo.payments.send.SendMoneyFragment;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.ab;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000  2\u00020\u00012\u00020\u0002:\u0002 !B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J&\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\b\u0010\u0014\u001a\u00020\u000bH\u0016J\u001c\u0010\u0015\u001a\u00020\u000b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u000f2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u000bH\u0016J\b\u0010\u001e\u001a\u00020\u000bH\u0016J\b\u0010\u001f\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\""},
   d2 = {"Lco/uk/getmondo/contacts/ContactsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/contacts/ContactsPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/contacts/ContactsPresenter;", "getPresenter", "()Lco/uk/getmondo/contacts/ContactsPresenter;", "setPresenter", "(Lco/uk/getmondo/contacts/ContactsPresenter;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "setContactsFabVisible", "visible", "", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showCardFrozen", "showPrepaidUi", "showRetailUi", "Companion", "ContactsPagerAdapter", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends co.uk.getmondo.common.f.a implements c.a {
   public static final a.a c = new a.a((i)null);
   private static final int d = 0;
   private static final int e = 1;
   public c a;
   private HashMap f;

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.f.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public void a() {
      ViewPager var = (ViewPager)this.a(co.uk.getmondo.c.a.contactsViewPager);
      n var = this.getChildFragmentManager();
      l.a(var, "childFragmentManager");
      Resources var = this.getResources();
      l.a(var, "resources");
      var.setAdapter((p)(new a.b(var, var, 2)));
      ((ViewPager)this.a(co.uk.getmondo.c.a.contactsViewPager)).a((f)(new j() {
         public void b(int var) {
            if(var == a.c.a()) {
               ((FloatingActionButton)a.this.a(co.uk.getmondo.c.a.contactsFab)).a();
            } else {
               ((FloatingActionButton)a.this.a(co.uk.getmondo.c.a.contactsFab)).b();
            }

         }
      }));
      ((TabLayout)this.a(co.uk.getmondo.c.a.contactsTabLayout)).setupWithViewPager((ViewPager)this.a(co.uk.getmondo.c.a.contactsViewPager));
   }

   public void a(co.uk.getmondo.d.c var) {
      l.b(var, "balance");
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle((CharSequence)this.getString(2131362484));
      Toolbar var = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
      ab var = ab.a;
      String var = this.getString(2131362021);
      l.a(var, "getString(R.string.available_balance_format)");
      Object[] var = new Object[]{var.toString()};
      String var = String.format(var, Arrays.copyOf(var, var.length));
      l.a(var, "java.lang.String.format(format, *args)");
      var.setSubtitle((CharSequence)var);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 17170443));
   }

   public void a(boolean var) {
      if(var) {
         ae.a((View)((FloatingActionButton)this.a(co.uk.getmondo.c.a.contactsFab)));
      } else {
         ae.b((FloatingActionButton)this.a(co.uk.getmondo.c.a.contactsFab));
      }

   }

   public void b() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle((CharSequence)this.getString(2131362489));
      ae.b((TabLayout)this.a(co.uk.getmondo.c.a.contactsTabLayout));
      LayoutParams var = ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).getLayoutParams();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.design.widget.AppBarLayout.LayoutParams");
      } else {
         ((android.support.design.widget.AppBarLayout.a)var).a(0);
         ViewPager var = (ViewPager)this.a(co.uk.getmondo.c.a.contactsViewPager);
         n var = this.getChildFragmentManager();
         l.a(var, "childFragmentManager");
         Resources var = this.getResources();
         l.a(var, "resources");
         var.setAdapter((p)(new a.b(var, var, 1)));
         ((TabLayout)this.a(co.uk.getmondo.c.a.contactsTabLayout)).setupWithViewPager((ViewPager)this.a(co.uk.getmondo.c.a.contactsViewPager));
      }
   }

   public void c() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle((CharSequence)this.getString(2131362481));
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setSubtitle((CharSequence)null);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689593));
   }

   public void f() {
      if(this.f != null) {
         this.f.clear();
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      l.b(var, "inflater");
      return var.inflate(2131034269, var, false);
   }

   public void onDestroyView() {
      c var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.f();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      c var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((c.a)this);
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var = (HomeActivity)var;
         var.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         l.a(var, "toolbar");
         var.a(var);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/contacts/ContactsFragment$Companion;", "", "()V", "TAB_INDEX_REQUEST_MONEY", "", "getTAB_INDEX_REQUEST_MONEY", "()I", "TAB_INDEX_SEND_MONEY", "getTAB_INDEX_SEND_MONEY", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      private final int a() {
         return a.d;
      }

      private final int b() {
         return a.e;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u0007H\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0007H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0010\u001a\u00020\u0007H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/contacts/ContactsFragment$ContactsPagerAdapter;", "Landroid/support/v4/app/FragmentPagerAdapter;", "supportFragmentManager", "Landroid/support/v4/app/FragmentManager;", "resources", "Landroid/content/res/Resources;", "pageCount", "", "(Landroid/support/v4/app/FragmentManager;Landroid/content/res/Resources;I)V", "getPageCount", "()I", "getResources", "()Landroid/content/res/Resources;", "getCount", "getItem", "Landroid/support/v4/app/Fragment;", "position", "getPageTitle", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends r {
      private final Resources a;
      private final int b;

      public b(n var, Resources var, int var) {
         l.b(var, "supportFragmentManager");
         l.b(var, "resources");
         super(var);
         this.a = var;
         this.b = var;
      }

      public Fragment a(int var) {
         Fragment var;
         if(var == a.c.a()) {
            var = SendMoneyFragment.a();
            l.a(var, "SendMoneyFragment.newInstance()");
         } else {
            if(var != a.c.b()) {
               throw (Throwable)(new RuntimeException("Invalid position for contacts pager adapter"));
            }

            var = RequestMoneyFragment.a();
            l.a(var, "RequestMoneyFragment.newInstance()");
         }

         return var;
      }

      public int b() {
         return this.b;
      }

      public CharSequence c(int var) {
         String var;
         CharSequence var;
         if(var == a.c.a()) {
            var = this.a.getString(2131362087);
            l.a(var, "resources.getString(R.st…ontacts_title_send_money)");
            var = (CharSequence)var;
         } else {
            if(var != a.c.b()) {
               throw (Throwable)(new RuntimeException("Invalid position for contacts pager adapter"));
            }

            var = this.a.getString(2131362086);
            l.a(var, "resources.getString(R.st…acts_title_request_money)");
            var = (CharSequence)var;
         }

         return var;
      }
   }
}
