package co.uk.getmondo.force_upgrade;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.n;

public class ForceUpgradeActivity extends co.uk.getmondo.common.activities.b implements c.a {
   c a;
   @BindView(2131820910)
   Button upgradeButton;

   public static Intent a(Context var) {
      return (new Intent(var, ForceUpgradeActivity.class)).addFlags(268533760);
   }

   // $FF: synthetic method
   static co.uk.getmondo.common.b.a a(Object var) throws Exception {
      return co.uk.getmondo.common.b.a.a;
   }

   public void a() {
      try {
         Intent var = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=co.uk.getmondo"));
         this.startActivity(var);
      } catch (ActivityNotFoundException var) {
         this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=co.uk.getmondo")));
      }

   }

   public n b() {
      return com.b.a.c.c.a(this.upgradeButton).map(a.a());
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034170);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((c.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
