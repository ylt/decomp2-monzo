package co.uk.getmondo.force_upgrade;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ForceUpgradeActivity_ViewBinding implements Unbinder {
   private ForceUpgradeActivity a;

   public ForceUpgradeActivity_ViewBinding(ForceUpgradeActivity var, View var) {
      this.a = var;
      var.upgradeButton = (Button)Utils.findRequiredViewAsType(var, 2131820910, "field 'upgradeButton'", Button.class);
   }

   public void unbind() {
      ForceUpgradeActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.upgradeButton = null;
      }
   }
}
