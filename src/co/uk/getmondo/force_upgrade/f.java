package co.uk.getmondo.force_upgrade;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(b.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var) {
      return new f(var, var);
   }

   public c a() {
      return (c)b.a.c.a(this.b, new c((co.uk.getmondo.common.a)this.c.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
