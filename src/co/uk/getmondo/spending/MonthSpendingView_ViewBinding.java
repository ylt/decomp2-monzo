package co.uk.getmondo.spending;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthSpendingView_ViewBinding implements Unbinder {
   private MonthSpendingView a;

   public MonthSpendingView_ViewBinding(MonthSpendingView var, View var) {
      this.a = var;
      var.monthAmountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821753, "field 'monthAmountView'", AmountView.class);
      var.monthNameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821616, "field 'monthNameTextView'", TextView.class);
      var.cardsContainer = (LinearLayout)Utils.findRequiredViewAsType(var, 2131821754, "field 'cardsContainer'", LinearLayout.class);
   }

   public void unbind() {
      MonthSpendingView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.monthAmountView = null;
         var.monthNameTextView = null;
         var.cardsContainer = null;
      }
   }
}
