package co.uk.getmondo.spending;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingCategoryDetailsActivity_ViewBinding implements Unbinder {
   private SpendingCategoryDetailsActivity a;

   public SpendingCategoryDetailsActivity_ViewBinding(SpendingCategoryDetailsActivity var, View var) {
      this.a = var;
      var.appBarLayout = (AppBarLayout)Utils.findRequiredViewAsType(var, 2131821096, "field 'appBarLayout'", AppBarLayout.class);
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
      var.tabLayout = (TabLayout)Utils.findRequiredViewAsType(var, 2131821097, "field 'tabLayout'", TabLayout.class);
      var.viewPager = (ViewPager)Utils.findRequiredViewAsType(var, 2131821095, "field 'viewPager'", ViewPager.class);
   }

   public void unbind() {
      SpendingCategoryDetailsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.appBarLayout = null;
         var.toolbar = null;
         var.tabLayout = null;
         var.viewPager = null;
      }
   }
}
