package co.uk.getmondo.spending.a;

import android.util.Pair;
import io.realm.av;

// $FF: synthetic class
final class s implements kotlin.d.a.b {
   private final Pair a;
   private final co.uk.getmondo.d.h b;

   private s(Pair var, co.uk.getmondo.d.h var) {
      this.a = var;
      this.b = var;
   }

   public static kotlin.d.a.b a(Pair var, co.uk.getmondo.d.h var) {
      return new s(var, var);
   }

   public Object a(Object var) {
      return i.a(this.a, this.b, (av)var);
   }
}
