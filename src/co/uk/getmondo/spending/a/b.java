package co.uk.getmondo.spending.a;

import co.uk.getmondo.d.aj;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.threeten.bp.YearMonth;

public class b {
   private static final co.uk.getmondo.common.i.c a;
   private static final TimeZone b;

   static {
      a = co.uk.getmondo.common.i.c.a;
      b = TimeZone.getTimeZone("UTC");
   }

   // $FF: synthetic method
   static int a(aj var, aj var) {
      return var.v().compareTo(var.v());
   }

   // $FF: synthetic method
   static int a(h var, h var) {
      long var = var.a().a().k();
      return Long.compare(var.a().a().k(), var) * -1;
   }

   private static g a(Map var, Map var, a var, co.uk.getmondo.common.i.a var) {
      ArrayList var = new ArrayList(var.size());
      Iterator var = var.keySet().iterator();

      while(var.hasNext()) {
         Object var = var.next();
         co.uk.getmondo.common.i.a var = (co.uk.getmondo.common.i.a)var.get(var);
         var.add(var.a(new g(var.a(), var.b()), (aj)var.get(var)));
      }

      Collections.sort(var, c.a());
      return new g(var.a(), var.b(), var);
   }

   // $FF: synthetic method
   static void a(List var, a var, io.reactivex.o var) throws Exception {
      if(co.uk.getmondo.common.k.b.a(var)) {
         var.a();
      } else {
         Collections.sort(var, Collections.reverseOrder(e.a()));
         co.uk.getmondo.common.i.a var = new co.uk.getmondo.common.i.a(a);
         HashMap var = new HashMap();
         HashMap var = new HashMap();
         YearMonth var = co.uk.getmondo.common.c.a.b(((aj)var.get(0)).v(), b);

         YearMonth var;
         for(int var = 0; var < var.size(); var = var) {
            aj var = (aj)var.get(var);
            Object var = var.a(var);
            if(var != null) {
               co.uk.getmondo.common.i.a var = (co.uk.getmondo.common.i.a)var.get(var);
               co.uk.getmondo.common.i.a var = var;
               if(var == null) {
                  var = new co.uk.getmondo.common.i.a(a);
                  var.put(var, var);
               }

               var.a(var.g());
               var.a(var.g());
               if(!var.containsKey(var)) {
                  var.put(var, var);
               }
            }

            boolean var;
            if(var == var.size() - 1) {
               var = true;
            } else {
               var = false;
            }

            if(var) {
               var = null;
            } else {
               var = co.uk.getmondo.common.c.a.b(((aj)var.get(var + 1)).v(), b);
            }

            label59: {
               if(var == null || var.b(var)) {
                  var.a(new android.support.v4.g.j(var, a(var, var, var, var)));
                  if(!var) {
                     var.clear();
                     var.clear();
                     var.c();
                     break label59;
                  }
               }

               var = var;
            }

            ++var;
         }

         var.a();
      }

   }

   public io.reactivex.n a(List var, a var) {
      return io.reactivex.n.create(d.a(var, var));
   }
}
