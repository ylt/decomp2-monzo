package co.uk.getmondo.spending.a;

public final class t implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!t.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public t(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new t(var);
   }

   public i a() {
      return new i((co.uk.getmondo.transaction.a.j)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
