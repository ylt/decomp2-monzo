package co.uk.getmondo.spending.a;

import android.util.Pair;

// $FF: synthetic class
final class l implements io.reactivex.c.h {
   private final co.uk.getmondo.d.h a;
   private final String b;
   private final String c;

   private l(co.uk.getmondo.d.h var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(co.uk.getmondo.d.h var, String var, String var) {
      return new l(var, var, var);
   }

   public Object a(Object var) {
      return i.a(this.a, this.b, this.c, (Pair)var);
   }
}
