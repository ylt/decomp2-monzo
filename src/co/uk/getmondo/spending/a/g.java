package co.uk.getmondo.spending.a;

import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J-\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/spending/data/SpendingData;", "", "totalSpent", "Lco/uk/getmondo/model/Amount;", "numberOfTransactions", "", "spendingBreakdown", "", "Lco/uk/getmondo/spending/data/SpendingGroup;", "(Lco/uk/getmondo/model/Amount;ILjava/util/List;)V", "getNumberOfTransactions", "()I", "getSpendingBreakdown", "()Ljava/util/List;", "getTotalSpent", "()Lco/uk/getmondo/model/Amount;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   private final co.uk.getmondo.d.c a;
   private final int b;
   private final List c;

   public g(co.uk.getmondo.d.c var, int var) {
      this(var, var, (List)null, 4, (kotlin.d.b.i)null);
   }

   public g(co.uk.getmondo.d.c var, int var, List var) {
      kotlin.d.b.l.b(var, "totalSpent");
      kotlin.d.b.l.b(var, "spendingBreakdown");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
   }

   // $FF: synthetic method
   public g(co.uk.getmondo.d.c var, int var, List var, int var, kotlin.d.b.i var) {
      if((var & 4) != 0) {
         var = kotlin.a.m.a();
      }

      this(var, var, var);
   }

   public final co.uk.getmondo.d.c a() {
      return this.a;
   }

   public final int b() {
      return this.b;
   }

   public final List c() {
      return this.c;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof g)) {
            return var;
         }

         g var = (g)var;
         var = var;
         if(!kotlin.d.b.l.a(this.a, var.a)) {
            return var;
         }

         boolean var;
         if(this.b == var.b) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!kotlin.d.b.l.a(this.c, var.c)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      co.uk.getmondo.d.c var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      int var = this.b;
      List var = this.c;
      if(var != null) {
         var = var.hashCode();
      }

      return (var * 31 + var) * 31 + var;
   }

   public String toString() {
      return "SpendingData(totalSpent=" + this.a + ", numberOfTransactions=" + this.b + ", spendingBreakdown=" + this.c + ")";
   }
}
