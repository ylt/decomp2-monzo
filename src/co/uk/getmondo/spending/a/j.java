package co.uk.getmondo.spending.a;

import java.util.concurrent.Callable;
import org.threeten.bp.YearMonth;

// $FF: synthetic class
final class j implements Callable {
   private final YearMonth a;
   private final YearMonth b;

   private j(YearMonth var, YearMonth var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(YearMonth var, YearMonth var) {
      return new j(var, var);
   }

   public Object call() {
      return i.a(this.a, this.b);
   }
}
