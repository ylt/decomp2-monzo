package co.uk.getmondo.spending.a;

import android.util.Pair;
import co.uk.getmondo.d.aj;
import io.reactivex.v;
import io.realm.av;
import io.realm.bf;
import io.realm.bg;
import io.realm.bl;
import java.util.Date;
import java.util.TimeZone;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;

public class i {
   private co.uk.getmondo.transaction.a.j a;

   public i(co.uk.getmondo.transaction.a.j var) {
      this.a = var;
   }

   // $FF: synthetic method
   static Pair a(YearMonth var, YearMonth var) throws Exception {
      LocalDateTime var = var.c(1).m();
      LocalDateTime var = var.f().b(23, 59).a(59).b(999999999);
      return new Pair(co.uk.getmondo.common.c.a.a(var), co.uk.getmondo.common.c.a.a(var));
   }

   // $FF: synthetic method
   static io.reactivex.r a(co.uk.getmondo.d.h var, Pair var) throws Exception {
      return co.uk.getmondo.common.j.g.a(s.a(var, var));
   }

   // $FF: synthetic method
   static io.reactivex.r a(co.uk.getmondo.d.h var, String var, String var, Pair var) throws Exception {
      return co.uk.getmondo.common.j.g.a(k.a(var, var, var, var));
   }

   // $FF: synthetic method
   static bg a(Pair var, co.uk.getmondo.d.h var, av var) {
      return var.a(aj.class).a("created", (Date)var.first).b("created", (Date)var.second).a("category", var.f()).a("includeInSpending", Boolean.valueOf(true)).g();
   }

   // $FF: synthetic method
   static bg a(Pair var, co.uk.getmondo.d.h var, String var, String var, av var) {
      bf var = var.a(co.uk.getmondo.d.m.class).b("transaction").a("transaction.created", (Date)var.first).b("transaction.created", (Date)var.second).a("transaction.includeInSpending", Boolean.valueOf(true)).a("transaction.category", var.f());
      bf var = var;
      if(var != null) {
         var = var.a("transaction.merchant.groupId", var);
      }

      var = var;
      if(var != null) {
         var = var.a("transaction.peer.userId", var);
      }

      return var.a("created", bl.b);
   }

   // $FF: synthetic method
   static bg a(av var) {
      return var.a(aj.class).a("includeInSpending", Boolean.valueOf(true)).g();
   }

   // $FF: synthetic method
   static LocalDate a(aj var) throws Exception {
      return co.uk.getmondo.common.c.a.a(var.v(), TimeZone.getTimeZone("UTC")).j();
   }

   private static v b(YearMonth var, YearMonth var) {
      return v.c(j.a(var, var));
   }

   public io.reactivex.n a() {
      return co.uk.getmondo.common.j.g.a(n.a()).map(o.a());
   }

   public io.reactivex.n a(YearMonth var, co.uk.getmondo.d.h var) {
      return b(var, var).b(p.a(var)).map(q.a());
   }

   public io.reactivex.n a(YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      return b(var, var).b(l.a(var, var, var)).map(m.a());
   }

   public v a(LocalDate var, LocalDate var) {
      return this.a.a(var.m(), var.m());
   }

   public v b() {
      return this.a.c().d(r.a());
   }
}
