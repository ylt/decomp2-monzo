package co.uk.getmondo.spending.a;

import android.util.Pair;
import io.realm.av;

// $FF: synthetic class
final class k implements kotlin.d.a.b {
   private final Pair a;
   private final co.uk.getmondo.d.h b;
   private final String c;
   private final String d;

   private k(Pair var, co.uk.getmondo.d.h var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public static kotlin.d.a.b a(Pair var, co.uk.getmondo.d.h var, String var, String var) {
      return new k(var, var, var, var);
   }

   public Object a(Object var) {
      return i.a(this.a, this.b, this.c, this.d, (av)var);
   }
}
