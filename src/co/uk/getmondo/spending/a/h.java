package co.uk.getmondo.spending.a;

import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup;", "", "()V", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "OfCategory", "OfMerchant", "OfPeer", "Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;", "Lco/uk/getmondo/spending/data/SpendingGroup$OfPeer;", "Lco/uk/getmondo/spending/data/SpendingGroup$OfCategory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class h {
   private h() {
   }

   // $FF: synthetic method
   public h(kotlin.d.b.i var) {
      this();
   }

   public abstract g a();

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfCategory;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "category", "Lco/uk/getmondo/model/Category;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Category;)V", "getCategory", "()Lco/uk/getmondo/model/Category;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends h {
      private final g a;
      private final co.uk.getmondo.d.h b;

      public a(g var, co.uk.getmondo.d.h var) {
         kotlin.d.b.l.b(var, "spendingData");
         kotlin.d.b.l.b(var, "category");
         super((kotlin.d.b.i)null);
         this.a = var;
         this.b = var;
      }

      public g a() {
         return this.a;
      }

      public final co.uk.getmondo.d.h b() {
         return this.b;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label28: {
               if(var instanceof h.a) {
                  h.a var = (h.a)var;
                  if(kotlin.d.b.l.a(this.a(), var.a()) && kotlin.d.b.l.a(this.b, var.b)) {
                     break label28;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         g var = this.a();
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         co.uk.getmondo.d.h var = this.b;
         if(var != null) {
            var = var.hashCode();
         }

         return var * 31 + var;
      }

      public String toString() {
         return "OfCategory(spendingData=" + this.a() + ", category=" + this.b + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "merchant", "Lco/uk/getmondo/model/Merchant;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Merchant;)V", "getMerchant", "()Lco/uk/getmondo/model/Merchant;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends h {
      private final g a;
      private final u b;

      public b(g var, u var) {
         kotlin.d.b.l.b(var, "spendingData");
         kotlin.d.b.l.b(var, "merchant");
         super((kotlin.d.b.i)null);
         this.a = var;
         this.b = var;
      }

      public g a() {
         return this.a;
      }

      public final u b() {
         return this.b;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label28: {
               if(var instanceof h.b) {
                  h.b var = (h.b)var;
                  if(kotlin.d.b.l.a(this.a(), var.a()) && kotlin.d.b.l.a(this.b, var.b)) {
                     break label28;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         g var = this.a();
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         u var = this.b;
         if(var != null) {
            var = var.hashCode();
         }

         return var * 31 + var;
      }

      public String toString() {
         return "OfMerchant(spendingData=" + this.a() + ", merchant=" + this.b + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfPeer;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "peer", "Lco/uk/getmondo/model/Peer;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Peer;)V", "getPeer", "()Lco/uk/getmondo/model/Peer;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends h {
      private final g a;
      private final aa b;

      public c(g var, aa var) {
         kotlin.d.b.l.b(var, "spendingData");
         kotlin.d.b.l.b(var, "peer");
         super((kotlin.d.b.i)null);
         this.a = var;
         this.b = var;
      }

      public g a() {
         return this.a;
      }

      public final aa b() {
         return this.b;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label28: {
               if(var instanceof h.c) {
                  h.c var = (h.c)var;
                  if(kotlin.d.b.l.a(this.a(), var.a()) && kotlin.d.b.l.a(this.b, var.b)) {
                     break label28;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         g var = this.a();
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         aa var = this.b;
         if(var != null) {
            var = var.hashCode();
         }

         return var * 31 + var;
      }

      public String toString() {
         return "OfPeer(spendingData=" + this.a() + ", peer=" + this.b + ")";
      }
   }
}
