package co.uk.getmondo.spending;

import android.support.v4.g.j;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import co.uk.getmondo.spending.a.g;
import co.uk.getmondo.spending.a.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.YearMonth;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\b\u0000\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\"B]\u0012\u001a\b\u0002\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004\u0012:\b\u0002\u0010\b\u001a4\u0012\u0013\u0012\u00110\u0006¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\t¢\u0006\u0002\u0010\u0010J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u001c\u0010\u0019\u001a\u00020\u000f2\n\u0010\u001a\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\u001c\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0016J \u0010 \u001a\u00020\u000f2\u0018\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050!RL\u0010\b\u001a4\u0012\u0013\u0012\u00110\u0006¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\f\u0012\u0013\u0012\u00110\r¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R#\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006#"},
   d2 = {"Lco/uk/getmondo/spending/SpendingAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/spending/SpendingAdapter$ViewHolder;", "spendingData", "", "Landroid/support/v4/util/Pair;", "Lorg/threeten/bp/YearMonth;", "Lco/uk/getmondo/spending/data/SpendingData;", "clickListener", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "yearMonth", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingGroup", "", "(Ljava/util/List;Lkotlin/jvm/functions/Function2;)V", "getClickListener", "()Lkotlin/jvm/functions/Function2;", "setClickListener", "(Lkotlin/jvm/functions/Function2;)V", "getSpendingData", "()Ljava/util/List;", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "", "ViewHolder", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends android.support.v7.widget.RecyclerView.a {
   private final List a;
   private m b;

   public a() {
      this((List)null, (m)null, 3, (i)null);
   }

   public a(List var, m var) {
      l.b(var, "spendingData");
      super();
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   public a(List var, m var, int var, i var) {
      if((var & 1) != 0) {
         var = (List)(new ArrayList());
      }

      if((var & 2) != 0) {
         var = (m)null;
      }

      this(var, var);
   }

   public a.a a(ViewGroup var, int var) {
      l.b(var, "parent");
      MonthSpendingView var = new MonthSpendingView(var.getContext());
      var.setLayoutParams(new LayoutParams(-1, -2));
      var = var.getContext().getResources().getDimensionPixelSize(2131427603);
      var.setPaddingRelative(0, var, 0, var);
      return new a.a(var);
   }

   public final List a() {
      return this.a;
   }

   public void a(a.a var, int var) {
      l.b(var, "holder");
      j var = (j)this.a.get(var);
      var.a().a((YearMonth)var.a, (g)var.b);
   }

   public final void a(List var) {
      l.b(var, "spendingData");
      this.a.clear();
      this.a.addAll((Collection)var);
      this.notifyDataSetChanged();
   }

   public final void a(m var) {
      this.b = var;
   }

   public final m b() {
      return this.b;
   }

   public int getItemCount() {
      return this.a.size();
   }

   // $FF: synthetic method
   public void onBindViewHolder(w var, int var) {
      this.a((a.a)var, var);
   }

   // $FF: synthetic method
   public w onCreateViewHolder(ViewGroup var, int var) {
      return (w)this.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/spending/SpendingAdapter$ViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "monthSpendingView", "Lco/uk/getmondo/spending/MonthSpendingView;", "(Lco/uk/getmondo/spending/SpendingAdapter;Lco/uk/getmondo/spending/MonthSpendingView;)V", "getMonthSpendingView", "()Lco/uk/getmondo/spending/MonthSpendingView;", "setMonthSpendingView", "(Lco/uk/getmondo/spending/MonthSpendingView;)V", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class a extends w {
      private MonthSpendingView b;

      public a(MonthSpendingView var) {
         l.b(var, "monthSpendingView");
         super((View)var);
         this.b = var;
         this.b.setItemClickListener((MonthSpendingView.a)(new MonthSpendingView.a() {
            public final void a(YearMonth var, h var) {
               m var = a.this.b();
               if(var != null) {
                  l.a(var, "yearMonth");
                  l.a(var, "spendingGroup");
                  n var = (n)var.a(var, var);
               }

            }
         }));
      }

      public final MonthSpendingView a() {
         return this.b;
      }
   }
}
