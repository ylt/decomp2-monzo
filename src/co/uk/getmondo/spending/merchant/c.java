package co.uk.getmondo.spending.merchant;

import co.uk.getmondo.spending.MonthSpendingView;
import org.threeten.bp.YearMonth;

// $FF: synthetic class
final class c implements MonthSpendingView.a {
   private final io.reactivex.o a;

   private c(io.reactivex.o var) {
      this.a = var;
   }

   public static MonthSpendingView.a a(io.reactivex.o var) {
      return new c(var);
   }

   public void a(YearMonth var, co.uk.getmondo.spending.a.h var) {
      SpendingByMerchantFragment.a(this.a, var, var);
   }
}
