package co.uk.getmondo.spending.merchant;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import io.reactivex.r;
import io.reactivex.u;
import java.util.List;
import org.threeten.bp.YearMonth;

public class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.spending.a.i d;
   private final co.uk.getmondo.spending.a.b e;
   private final YearMonth f;
   private final co.uk.getmondo.d.h g;
   private final co.uk.getmondo.common.a h;

   h(u var, co.uk.getmondo.spending.a.i var, co.uk.getmondo.spending.a.b var, YearMonth var, co.uk.getmondo.d.h var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static r a(h var, List var) throws Exception {
      return var.e.a(var, co.uk.getmondo.spending.a.a.a).defaultIfEmpty(new android.support.v4.g.j(var.f, new co.uk.getmondo.spending.a.g(new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), 0)));
   }

   // $FF: synthetic method
   static void a(h.a var, android.support.v4.g.j var) throws Exception {
      var.a((YearMonth)var.a, (co.uk.getmondo.spending.a.g)var.b);
   }

   // $FF: synthetic method
   static void a(h var, h.a var, co.uk.getmondo.spending.a.h var) throws Exception {
      if(var instanceof co.uk.getmondo.spending.a.h.b) {
         co.uk.getmondo.d.u var = ((co.uk.getmondo.spending.a.h.b)var).b();
         var.a(var.f, var.g, var.i(), var.h());
      } else if(var instanceof co.uk.getmondo.spending.a.h.c) {
         aa var = ((co.uk.getmondo.spending.a.h.c)var).b();
         var.b(var.f, var.g, var.b(), var.a());
      }

   }

   // $FF: synthetic method
   static void a(h var, Boolean var) throws Exception {
      Impression var = Impression.b(var.g, var.f);
      var.h.a(var);
   }

   // $FF: synthetic method
   static boolean a(Boolean var) throws Exception {
      return var.booleanValue();
   }

   public void a(h.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.a((io.reactivex.b.b)var.b().filter(i.a()).subscribe(j.a(this), k.a()));
      this.a((io.reactivex.b.b)this.d.a(this.f, this.g).flatMap(l.a(this)).observeOn(this.c).subscribe(m.a(var), n.a()));
      this.a((io.reactivex.b.b)var.a().subscribe(o.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(YearMonth var, co.uk.getmondo.d.h var, String var, String var);

      void a(YearMonth var, co.uk.getmondo.spending.a.g var);

      io.reactivex.n b();

      void b(YearMonth var, co.uk.getmondo.d.h var, String var, String var);
   }
}
