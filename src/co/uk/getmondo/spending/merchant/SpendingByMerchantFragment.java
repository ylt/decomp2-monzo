package co.uk.getmondo.spending.merchant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.spending.MonthSpendingView;
import co.uk.getmondo.spending.transactions.SpendingTransactionsActivity;
import org.threeten.bp.YearMonth;

public class SpendingByMerchantFragment extends co.uk.getmondo.common.f.a implements h.a {
   h a;
   private final com.b.b.c c = com.b.b.c.a();
   private Unbinder d;
   @BindView(2131821376)
   MonthSpendingView monthSpendingView;

   public static SpendingByMerchantFragment a(YearMonth var, co.uk.getmondo.d.h var) {
      Bundle var = new Bundle();
      var.putSerializable("KEY_YEAR_MONTH", var);
      var.putSerializable("KEY_CATEGORY", var);
      SpendingByMerchantFragment var = new SpendingByMerchantFragment();
      var.setArguments(var);
      return var;
   }

   // $FF: synthetic method
   static void a(SpendingByMerchantFragment var) throws Exception {
      var.monthSpendingView.setItemClickListener((MonthSpendingView.a)null);
   }

   // $FF: synthetic method
   static void a(SpendingByMerchantFragment var, io.reactivex.o var) throws Exception {
      var.monthSpendingView.setItemClickListener(c.a(var));
      var.a(d.a(var));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var, YearMonth var, co.uk.getmondo.spending.a.h var) {
      var.a(var);
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(b.a(this));
   }

   public void a(YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      SpendingTransactionsActivity.a(this.getActivity(), var, var, var, var);
   }

   public void a(YearMonth var, co.uk.getmondo.spending.a.g var) {
      this.monthSpendingView.a(var, var);
   }

   public io.reactivex.n b() {
      return this.c;
   }

   public void b(YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      SpendingTransactionsActivity.c(this.getActivity(), var, var, var, var);
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      YearMonth var = (YearMonth)this.getArguments().getSerializable("KEY_YEAR_MONTH");
      co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)this.getArguments().getSerializable("KEY_CATEGORY");
      this.B().a(new f(var, var)).a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034282, var, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.d.unbind();
      super.onDestroyView();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.d = ButterKnife.bind(this, (View)var);
      this.a.a((h.a)this);
   }

   public void setUserVisibleHint(boolean var) {
      super.setUserVisibleHint(var);
      this.c.a((Object)Boolean.valueOf(var));
   }
}
