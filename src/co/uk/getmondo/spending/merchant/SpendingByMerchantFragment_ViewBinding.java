package co.uk.getmondo.spending.merchant;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.spending.MonthSpendingView;

public class SpendingByMerchantFragment_ViewBinding implements Unbinder {
   private SpendingByMerchantFragment a;

   public SpendingByMerchantFragment_ViewBinding(SpendingByMerchantFragment var, View var) {
      this.a = var;
      var.monthSpendingView = (MonthSpendingView)Utils.findRequiredViewAsType(var, 2131821376, "field 'monthSpendingView'", MonthSpendingView.class);
   }

   public void unbind() {
      SpendingByMerchantFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.monthSpendingView = null;
      }
   }
}
