package co.uk.getmondo.spending;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.spending.a.i;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B]\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014¢\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/spending/SpendingPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/spending/SpendingPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "computation", "spendingRepository", "Lco/uk/getmondo/spending/data/SpendingRepository;", "spendingCalculator", "Lco/uk/getmondo/spending/data/SpendingCalculator;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "recurringPaymentsManager", "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/spending/data/SpendingCalculator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/data/RecurringPaymentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/accounts/AccountManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final u e;
   private final i f;
   private final co.uk.getmondo.spending.a.b g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.payments.a.i i;
   private final co.uk.getmondo.common.e.a j;
   private final co.uk.getmondo.card.c k;
   private final co.uk.getmondo.common.accounts.b l;

   public d(u var, u var, u var, i var, co.uk.getmondo.spending.a.b var, co.uk.getmondo.common.a var, co.uk.getmondo.payments.a.i var, co.uk.getmondo.common.e.a var, co.uk.getmondo.card.c var, co.uk.getmondo.common.accounts.b var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "computation");
      l.b(var, "spendingRepository");
      l.b(var, "spendingCalculator");
      l.b(var, "analyticsService");
      l.b(var, "recurringPaymentsManager");
      l.b(var, "apiErrorHandler");
      l.b(var, "cardManager");
      l.b(var, "accountManager");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
   }

   public void a(final d.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.Companion.S());
      io.reactivex.b.a var = this.b;
      n var = this.f.a().flatMapSingle((h)(new h() {
         public final v a(List var) {
            l.b(var, "transactions");
            return d.this.g.a(var, co.uk.getmondo.spending.a.a.b).toList().b(d.this.e);
         }
      })).observeOn(this.d);
      g var = (g)(new g() {
         public final void a(List varx) {
            boolean var;
            if(!((Collection)varx).isEmpty()) {
               var = true;
            } else {
               var = false;
            }

            if(!var) {
               var.c();
            } else {
               d.a var = var;
               l.a(varx, "spendingData");
               var.a(varx);
            }

            var.a(var);
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new e(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (g)var);
      l.a(var, "spendingRepository.allTr…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      var = this.k.a().observeOn(this.d).subscribe((g)(new g() {
         public final void a(co.uk.getmondo.d.g varx) {
            if(varx.h()) {
               var.f();
            } else {
               var.g();
            }

         }
      }), (g)null.a);
      l.a(var, "cardManager.card()\n     …ed to get card state\") })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.a().subscribe((g)(new g() {
         public final void a(List varx) {
            d.a var = var;
            l.a(varx, "it");
            var.b(varx);
         }
      }));
      l.a(var, "view.exportClicks\n      …iew.showExportData(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      if(this.l.b()) {
         var = this.b;
         var = this.i.c().b(this.c).a(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
            public final void a(Throwable varx) {
               co.uk.getmondo.common.e.a var = d.this.j;
               l.a(varx, "error");
               if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                  var.b(2131362612);
               }

            }
         }));
         l.a(var, "recurringPaymentsManager…  }\n                    )");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         io.reactivex.b.a var = this.b;
         io.reactivex.b.b var = this.i.g().observeOn(this.d).subscribe((g)(new g() {
            public final void a(kotlin.h varx) {
               int var = ((Number)varx.c()).intValue();
               int var = ((Number)varx.d()).intValue();
               if(var + var > 0) {
                  var.a(var, var);
               } else {
                  var.e();
               }

            }
         }));
         l.a(var, "recurringPaymentsManager…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         io.reactivex.b.b var = var.b().subscribe((g)(new g() {
            public final void a(Object varx) {
               var.d();
            }
         }));
         l.a(var, "view.onRecurringPayments…openRecurringPayments() }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\t\u001a\u00020\nH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0004H&J\b\u0010\r\u001a\u00020\nH&J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H&J\"\u0010\u0011\u001a\u00020\n2\u0018\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00140\u00130\u0005H&J\b\u0010\u0015\u001a\u00020\nH&J\b\u0010\u0016\u001a\u00020\nH&J\u0016\u0010\u0017\u001a\u00020\n2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0018\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH&J\b\u0010\u001d\u001a\u00020\nH&R\u001e\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u001e"},
      d2 = {"Lco/uk/getmondo/spending/SpendingPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "exportClicks", "Lio/reactivex/Observable;", "", "Lorg/threeten/bp/YearMonth;", "getExportClicks", "()Lio/reactivex/Observable;", "hideRecurringPaymentsBanner", "", "onRecurringPaymentsClicked", "", "openRecurringPayments", "setExportEnabled", "isEnabled", "", "setSpendingData", "spendingData", "Landroid/support/v4/util/Pair;", "Lco/uk/getmondo/spending/data/SpendingData;", "showCardFrozen", "showEmptyNotice", "showExportData", "months", "showRecurringPaymentsBanner", "directDebitCount", "", "paymentSeriesCount", "showTitle", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a();

      void a(int var, int var);

      void a(List var);

      void a(boolean var);

      n b();

      void b(List var);

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
