package co.uk.getmondo.spending.transactions;

public final class c implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var) {
      return new c(var, var);
   }

   public void a(SpendingTransactionsFragment var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (f)this.b.b();
         var.c = (SpendingFeedAdapter)this.c.b();
      }
   }
}
