package co.uk.getmondo.spending.transactions;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class SpendingFeedAdapter$SpendingHeaderViewHolder_ViewBinding implements Unbinder {
   private SpendingFeedAdapter.SpendingHeaderViewHolder a;

   public SpendingFeedAdapter$SpendingHeaderViewHolder_ViewBinding(SpendingFeedAdapter.SpendingHeaderViewHolder var, View var) {
      this.a = var;
      var.amountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821615, "field 'amountView'", AmountView.class);
      var.monthNameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821616, "field 'monthNameTextView'", TextView.class);
   }

   public void unbind() {
      SpendingFeedAdapter.SpendingHeaderViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountView = null;
         var.monthNameTextView = null;
      }
   }
}
