package co.uk.getmondo.spending.transactions;

import android.support.v4.g.j;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import io.reactivex.u;
import java.util.Iterator;
import java.util.List;
import org.threeten.bp.YearMonth;

public class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.spending.a.i d;
   private final co.uk.getmondo.common.a e;
   private final YearMonth f;
   private final co.uk.getmondo.d.h g;
   private final String h;
   private final String i;

   f(u var, co.uk.getmondo.spending.a.i var, co.uk.getmondo.common.a var, YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static j a(List var) throws Exception {
      co.uk.getmondo.common.i.a var = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
      Iterator var = var.iterator();

      while(var.hasNext()) {
         var.a(((aj)((m)var.next()).e().a()).g());
      }

      return j.a(var, var.a());
   }

   // $FF: synthetic method
   static void a(f var, f.a var, j var) throws Exception {
      var.a((List)var.a, (co.uk.getmondo.d.c)var.b, var.f);
   }

   public void a(f.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      co.uk.getmondo.common.a var = this.e;
      Impression var;
      if(this.h == null && this.i == null) {
         var = Impression.a(this.g, this.f);
      } else {
         var = Impression.c(this.g, this.f);
      }

      var.a(var);
      this.a((io.reactivex.b.b)this.d.a(this.f, this.g, this.h, this.i).observeOn(this.c).map(g.a()).subscribe(h.a(this, var), i.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(List var, co.uk.getmondo.d.c var, YearMonth var);
   }
}
