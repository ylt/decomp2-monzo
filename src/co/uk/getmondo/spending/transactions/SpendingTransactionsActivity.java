package co.uk.getmondo.spending.transactions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.threeten.bp.YearMonth;

public class SpendingTransactionsActivity extends co.uk.getmondo.common.activities.b {
   @BindView(2131820798)
   Toolbar toolbar;

   public static void a(Context var, YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      var.startActivity(b(var, var, var, var, var));
   }

   public static Intent b(Context var, YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      Intent var = new Intent(var, SpendingTransactionsActivity.class);
      var.putExtra("EXTRA_YEAR_MONTH", var);
      var.putExtra("EXTRA_CATEGORY", var);
      var.putExtra("EXTRA_TITLE", var);
      var.putExtra("EXTRA_MERCHANT_GROUP_ID", var);
      return var;
   }

   public static void c(Context var, YearMonth var, co.uk.getmondo.d.h var, String var, String var) {
      Intent var = new Intent(var, SpendingTransactionsActivity.class);
      var.putExtra("EXTRA_YEAR_MONTH", var);
      var.putExtra("EXTRA_CATEGORY", var);
      var.putExtra("EXTRA_TITLE", var);
      var.putExtra("EXTRA_PEER_ID", var);
      var.startActivity(var);
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034212);
      ButterKnife.bind((Activity)this);
      this.setSupportActionBar(this.toolbar);
      co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)this.getIntent().getSerializableExtra("EXTRA_CATEGORY");
      String var = this.getIntent().getStringExtra("EXTRA_TITLE");
      this.toolbar.setTitle(var);
      int var = android.support.v4.content.a.c(this, var.b());
      this.toolbar.setBackgroundColor(var);
      this.getWindow().setStatusBarColor(co.uk.getmondo.common.k.c.a(var));
      if(var == null) {
         String var = this.getIntent().getStringExtra("EXTRA_MERCHANT_GROUP_ID");
         YearMonth var = (YearMonth)this.getIntent().getSerializableExtra("EXTRA_YEAR_MONTH");
         var = this.getIntent().getStringExtra("EXTRA_PEER_ID");
         SpendingTransactionsFragment var;
         if(var != null) {
            var = SpendingTransactionsFragment.a(var, var, var);
         } else {
            var = SpendingTransactionsFragment.b(var, var, var);
         }

         this.getSupportFragmentManager().a().a(2131821098, var).c();
      }

   }
}
