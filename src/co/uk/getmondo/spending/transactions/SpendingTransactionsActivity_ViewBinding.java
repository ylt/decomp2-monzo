package co.uk.getmondo.spending.transactions;

import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingTransactionsActivity_ViewBinding implements Unbinder {
   private SpendingTransactionsActivity a;

   public SpendingTransactionsActivity_ViewBinding(SpendingTransactionsActivity var, View var) {
      this.a = var;
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      SpendingTransactionsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.toolbar = null;
      }
   }
}
