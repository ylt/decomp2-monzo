package co.uk.getmondo.spending.transactions;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingTransactionsFragment_ViewBinding implements Unbinder {
   private SpendingTransactionsFragment a;

   public SpendingTransactionsFragment_ViewBinding(SpendingTransactionsFragment var, View var) {
      this.a = var;
      var.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131821377, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      SpendingTransactionsFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.recyclerView = null;
      }
   }
}
