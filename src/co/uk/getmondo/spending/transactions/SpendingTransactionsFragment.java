package co.uk.getmondo.spending.transactions;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;
import java.util.List;
import org.threeten.bp.YearMonth;

public class SpendingTransactionsFragment extends co.uk.getmondo.common.f.a implements co.uk.getmondo.feed.adapter.a.a, f.a {
   f a;
   SpendingFeedAdapter c;
   private Unbinder d;
   @BindView(2131821377)
   RecyclerView recyclerView;

   public static SpendingTransactionsFragment a(YearMonth var, co.uk.getmondo.d.h var) {
      Bundle var = new Bundle();
      var.putSerializable("year_month", var);
      var.putSerializable("category", var);
      SpendingTransactionsFragment var = new SpendingTransactionsFragment();
      var.setArguments(var);
      return var;
   }

   public static SpendingTransactionsFragment a(YearMonth var, co.uk.getmondo.d.h var, String var) {
      Bundle var = new Bundle();
      var.putSerializable("year_month", var);
      var.putString("merchant_group_id", var);
      var.putSerializable("category", var);
      SpendingTransactionsFragment var = new SpendingTransactionsFragment();
      var.setArguments(var);
      return var;
   }

   public static SpendingTransactionsFragment b(YearMonth var, co.uk.getmondo.d.h var, String var) {
      Bundle var = new Bundle();
      var.putSerializable("year_month", var);
      var.putString("peer_id", var);
      var.putSerializable("category", var);
      SpendingTransactionsFragment var = new SpendingTransactionsFragment();
      var.setArguments(var);
      return var;
   }

   public void a(m var) {
      if(var.j() == co.uk.getmondo.feed.a.a.a.a) {
         this.getActivity().startActivity(TransactionDetailsActivity.a(this.getActivity(), ((aj)var.e().a()).w()));
      }

   }

   public void a(List var, co.uk.getmondo.d.c var, YearMonth var) {
      this.c.a(var, var, var);
   }

   public void b(m var) {
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      YearMonth var = (YearMonth)this.getArguments().get("year_month");
      co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)this.getArguments().get("category");
      String var = this.getArguments().getString("merchant_group_id");
      String var = this.getArguments().getString("peer_id");
      this.B().a(new d(var, var, var, var)).a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034283, var, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.d.unbind();
      super.onDestroyView();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.d = ButterKnife.bind(this, (View)var);
      LinearLayoutManager var = new LinearLayoutManager(this.getActivity());
      this.c.a(this);
      this.recyclerView.setLayoutManager(var);
      this.recyclerView.setHasFixedSize(true);
      this.recyclerView.setAdapter(this.c);
      int var = this.getResources().getDimensionPixelSize(2131427609);
      this.recyclerView.a(new co.uk.getmondo.common.ui.h(this.getActivity(), this.c, var));
      this.recyclerView.a(new a.a.a.a.a.b(this.c));
      this.a.a((f.a)this);
   }
}
