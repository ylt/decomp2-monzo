package co.uk.getmondo.spending.transactions;

import android.support.v4.g.j;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import java.util.List;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

class SpendingFeedAdapter extends co.uk.getmondo.feed.adapter.a {
   private static final int b = co.uk.getmondo.feed.a.a.a.values().length;
   private j c;

   SpendingFeedAdapter(co.uk.getmondo.feed.a.a var) {
      super(var);
   }

   private int b(int var) {
      if(this.c != null) {
         --var;
      }

      return var;
   }

   public long a(int var) {
      long var;
      if(this.c != null && var == 0) {
         var = -1L;
      } else {
         var = super.a(this.b(var));
      }

      return var;
   }

   public void a(w var, int var) {
      super.a(var, this.b(var));
   }

   void a(List var, co.uk.getmondo.d.c var, YearMonth var) {
      this.c = j.a(var, var);
      this.a = var;
      this.notifyDataSetChanged();
   }

   public int getItemCount() {
      int var = super.getItemCount();
      byte var;
      if(this.c != null) {
         var = 1;
      } else {
         var = 0;
      }

      return var + var;
   }

   public long getItemId(int var) {
      return super.getItemId(this.b(var));
   }

   public int getItemViewType(int var) {
      if(this.c != null && var == 0) {
         var = b;
      } else {
         var = super.getItemViewType(this.b(var));
      }

      return var;
   }

   public void onBindViewHolder(w var, int var) {
      if(var instanceof SpendingFeedAdapter.SpendingHeaderViewHolder) {
         ((SpendingFeedAdapter.SpendingHeaderViewHolder)var).a((YearMonth)this.c.a, (co.uk.getmondo.d.c)this.c.b);
      } else {
         super.onBindViewHolder(var, this.b(var));
      }

   }

   public w onCreateViewHolder(ViewGroup var, int var) {
      Object var;
      if(var == b) {
         var = new SpendingFeedAdapter.SpendingHeaderViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034377, var, false));
      } else {
         var = super.onCreateViewHolder(var, var);
      }

      return (w)var;
   }

   static class SpendingHeaderViewHolder extends w {
      @BindView(2131821615)
      AmountView amountView;
      @BindView(2131821616)
      TextView monthNameTextView;

      SpendingHeaderViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      void a(YearMonth var, co.uk.getmondo.d.c var) {
         this.amountView.setAmount(var);
         this.monthNameTextView.setText(var.c().a(TextStyle.a, Locale.ENGLISH));
      }
   }
}
