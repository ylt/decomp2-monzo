package co.uk.getmondo.spending;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.g.j;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import io.reactivex.n;
import io.reactivex.c.h;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;
import org.threeten.bp.YearMonth;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\u0018\u0000 F2\u00020\u00012\u00020\u0002:\u0001FB\u0005¢\u0006\u0002\u0010\u0003J\"\u0010\u001f\u001a\u00020 2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\bH\u0002J\b\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020%2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\u0018\u0010)\u001a\u00020%2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0016J&\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010,\u001a\u0002002\b\u00101\u001a\u0004\u0018\u0001022\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u00103\u001a\u00020%H\u0016J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0005H\u0016J\u000e\u00107\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0007H\u0016J\u001a\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020/2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010:\u001a\u00020%H\u0016J\u0010\u0010;\u001a\u00020%2\u0006\u0010<\u001a\u000205H\u0016J\"\u0010=\u001a\u00020%2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\bH\u0016J\b\u0010>\u001a\u00020%H\u0016J\b\u0010?\u001a\u00020%H\u0016J\u0016\u0010@\u001a\u00020%2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016J\u0018\u0010B\u001a\u00020%2\u0006\u0010C\u001a\u00020 2\u0006\u0010D\u001a\u00020 H\u0016J\b\u0010E\u001a\u00020%H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R \u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR2\u0010\f\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\r0\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0016\u001a\u00020\u00178\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006G"},
   d2 = {"Lco/uk/getmondo/spending/SpendingFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/spending/SpendingPresenter$View;", "()V", "exportAction", "Landroid/view/MenuItem;", "exportClicks", "Lio/reactivex/Observable;", "", "Lorg/threeten/bp/YearMonth;", "getExportClicks", "()Lio/reactivex/Observable;", "exportRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "linearLayoutManager", "Landroid/support/v7/widget/LinearLayoutManager;", "getLinearLayoutManager", "()Landroid/support/v7/widget/LinearLayoutManager;", "linearLayoutManager$delegate", "Lkotlin/Lazy;", "presenter", "Lco/uk/getmondo/spending/SpendingPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/spending/SpendingPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/spending/SpendingPresenter;)V", "spendingAdapter", "Lco/uk/getmondo/spending/SpendingAdapter;", "yearMonth", "findPositionByMonth", "", "spendingData", "Landroid/support/v4/util/Pair;", "Lco/uk/getmondo/spending/data/SpendingData;", "hideRecurringPaymentsBanner", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onOptionsItemSelected", "", "item", "onRecurringPaymentsClicked", "onViewCreated", "view", "openRecurringPayments", "setExportEnabled", "isEnabled", "setSpendingData", "showCardFrozen", "showEmptyNotice", "showExportData", "months", "showRecurringPaymentsBanner", "directDebitCount", "paymentSeriesCount", "showTitle", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.f.a implements d.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(b.class), "linearLayoutManager", "getLinearLayoutManager()Landroid/support/v7/widget/LinearLayoutManager;"))};
   public static final b.a d = new b.a((i)null);
   public d c;
   private final com.b.b.c e = com.b.b.c.a();
   private final a f = new a((List)null, (m)null, 3, (i)null);
   private final kotlin.c g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final LinearLayoutManager b() {
         return new LinearLayoutManager((Context)b.this.getActivity());
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private YearMonth h;
   private MenuItem i;
   private HashMap j;

   private final int c(List var) {
      Iterator var = ((Iterable)kotlin.a.m.a((Collection)var)).iterator();

      Object var;
      while(true) {
         if(var.hasNext()) {
            Object var = var.next();
            if(!kotlin.d.b.l.a((YearMonth)((j)var.get(((Number)var).intValue())).a, this.h)) {
               continue;
            }

            var = var;
            break;
         }

         var = null;
         break;
      }

      Integer var = (Integer)var;
      int var;
      if(var != null) {
         var = var.intValue();
      } else {
         var = 0;
      }

      return var;
   }

   private final LinearLayoutManager i() {
      kotlin.c var = this.g;
      l var = a[0];
      return (LinearLayoutManager)var.a();
   }

   public View a(int var) {
      if(this.j == null) {
         this.j = new HashMap();
      }

      View var = (View)this.j.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.j.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public n a() {
      n var = this.e.map((h)(new h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return this.b(var);
         }

         public final List b(Object var) {
            kotlin.d.b.l.b(var, "it");
            int var = b.this.i().n();
            int var = b.this.i().p();
            Iterable var = (Iterable)b.this.f.a().subList(var, var + 1);
            Collection var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
            Iterator var = var.iterator();

            while(var.hasNext()) {
               var.add((YearMonth)((j)var.next()).a);
            }

            return (List)var;
         }
      }));
      kotlin.d.b.l.a(var, "exportRelay.map {\n      …ap { it.first }\n        }");
      return var;
   }

   public void a(int var, int var) {
      StringBuilder var = new StringBuilder();
      if(var > 0) {
         var.append(this.getResources().getQuantityString(2131886082, var, new Object[]{Integer.valueOf(var)}));
      }

      if(var > 0 && var > 0) {
         var.append(", ");
      }

      if(var > 0) {
         var.append(this.getResources().getQuantityString(2131886083, var, new Object[]{Integer.valueOf(var)}));
      }

      ((TextView)this.a(co.uk.getmondo.c.a.recurringBannerSubtitle)).setText((CharSequence)var.toString());
      ((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner)).setVisibility(0);
      this.a(co.uk.getmondo.c.a.topShadow).setVisibility(0);
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         ((HomeActivity)var).a(false);
      }
   }

   public void a(List var) {
      kotlin.d.b.l.b(var, "spendingData");
      this.f.a(var);
      if(this.h != null) {
         ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).a(this.c(var));
         this.h = (YearMonth)null;
      }

      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setVisibility(0);
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendingEmptyView)).setVisibility(8);
   }

   public void a(boolean var) {
      MenuItem var = this.i;
      if(var != null) {
         var.setVisible(var);
      }

   }

   public n b() {
      n var = com.b.a.c.c.a((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner));
      kotlin.d.b.l.a(var, "RxView.clicks(recurringPaymentsBanner)");
      return var;
   }

   public void b(List var) {
      kotlin.d.b.l.b(var, "months");
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      (new co.uk.getmondo.spending.b.a((Activity)var, var)).show();
   }

   public void c() {
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendingEmptyView)).setVisibility(0);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setVisibility(8);
   }

   public void d() {
      this.startActivity(new Intent((Context)this.getActivity(), RecurringPaymentsActivity.class));
   }

   public void e() {
      ((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner)).setVisibility(8);
      this.a(co.uk.getmondo.c.a.topShadow).setVisibility(8);
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         ((HomeActivity)var).a(true);
      }
   }

   public void f() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362490);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 17170443));
   }

   public void g() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362481);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689593));
   }

   public void h() {
      if(this.j != null) {
         this.j.clear();
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
      if(this.getArguments() != null && this.getArguments().containsKey("KEY_YEAR_MONTH")) {
         Serializable var = this.getArguments().getSerializable("KEY_YEAR_MONTH");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.YearMonth");
         }

         this.h = (YearMonth)var;
      }

   }

   public void onCreateOptionsMenu(Menu var, MenuInflater var) {
      kotlin.d.b.l.b(var, "menu");
      kotlin.d.b.l.b(var, "inflater");
      super.onCreateOptionsMenu(var, var);
      var.inflate(2131951623, var);
      this.i = var.findItem(2131821782);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      return var.inflate(2131034281, var, false);
   }

   public void onDestroyView() {
      d var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.h();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      kotlin.d.b.l.b(var, "item");
      boolean var;
      if(var.getItemId() == 2131821782) {
         this.e.a((Object)kotlin.n.a);
         var = true;
      } else {
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      this.setHasOptionsMenu(true);
      this.f.a((m)(new m() {
         public final void a(YearMonth var, co.uk.getmondo.spending.a.h var) {
            kotlin.d.b.l.b(var, "yearMonth");
            kotlin.d.b.l.b(var, "spendingGroup");
            if(var instanceof co.uk.getmondo.spending.a.h.a) {
               co.uk.getmondo.d.h var = ((co.uk.getmondo.spending.a.h.a)var).b();
               SpendingCategoryDetailsActivity.a((Context)b.this.getActivity(), var, var);
            }

         }
      }));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.f);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)this.i());
      d var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((d.a)this);
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var = (HomeActivity)var;
         var.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         kotlin.d.b.l.a(var, "toolbar");
         var.a(var);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/spending/SpendingFragment$Companion;", "", "()V", "KEY_YEAR_MONTH", "", "newInstance", "Landroid/support/v4/app/Fragment;", "yearMonth", "Lorg/threeten/bp/YearMonth;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
