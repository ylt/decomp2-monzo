package co.uk.getmondo.spending.b.a;

import co.uk.getmondo.d.aj;
import co.uk.getmondo.spending.b.a.a.e;
import io.reactivex.v;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;
import kotlin.h.g;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J \u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002J\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u001e\u0010\u0013\u001a\u00020\u00142\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\tH\u0002J\u001e\u0010\u0019\u001a\u00020\u00142\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u00162\u0006\u0010\u0018\u001a\u00020\tH\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/spending/export/data/TransactionExporter;", "", "()V", "filenameDateTimeFormatter", "Lorg/threeten/bp/format/DateTimeFormatter;", "kotlin.jvm.PlatformType", "qifDateFormatter", "Ljava/text/SimpleDateFormat;", "createExportFile", "Ljava/io/File;", "directoryPathname", "", "description", "extension", "createFilename", "export", "Lio/reactivex/Single;", "exportData", "Lco/uk/getmondo/spending/export/data/model/ExportData;", "exportAsCsv", "", "transactionRecordItems", "", "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;", "file", "exportAsQif", "transactions", "Lco/uk/getmondo/model/Transaction;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final DateTimeFormatter a = DateTimeFormatter.a("yyyy-MM-dd_HHmmss");
   private final SimpleDateFormat b;

   public a() {
      this.b = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
   }

   private final File a(String var, String var, String var) {
      File var = new File(new File(var), "export");
      var.mkdirs();
      var = new File(var, this.a(var, var));
      var.createNewFile();
      return var;
   }

   private final String a(String var, String var) {
      CharSequence var = (CharSequence)var;
      String var = (new g("\\s+")).a(var, "");
      var = LocalDateTime.a().a(this.a);
      return "MonzoDataExport_" + var + '_' + var + "" + var;
   }

   private final void a(List param1, File param2) {
      // $FF: Couldn't be decompiled
   }

   private final void b(List param1, File param2) {
      // $FF: Couldn't be decompiled
   }

   public final v a(final co.uk.getmondo.spending.b.a.a.a var) {
      l.b(var, "exportData");
      v var = v.c((Callable)(new Callable() {
         public final String a() {
            File varx = a.this.a(var.a().b(), var.b(), var.a().a().b());
            co.uk.getmondo.spending.b.a.a.b.a var = var.a().a();
            switch(b.a[var.ordinal()]) {
            case 1:
               a var = a.this;
               Iterable var = (Iterable)var.c();
               Collection var = (Collection)(new ArrayList(m.a(var, 10)));
               Iterator var = var.iterator();

               while(var.hasNext()) {
                  aj var = (aj)var.next();
                  var.add(e.a.a(var));
               }

               var.a((List)var, varx);
               break;
            case 2:
               a.this.b(var.c(), varx);
            }

            return varx.getAbsolutePath();
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      l.a(var, "Single.fromCallable {\n  …le.absolutePath\n        }");
      return var;
   }
}
