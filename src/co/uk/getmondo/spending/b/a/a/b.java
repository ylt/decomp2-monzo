package co.uk.getmondo.spending.b.a.a;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0014B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportOutput;", "", "format", "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "directoryPathname", "", "(Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;Ljava/lang/String;)V", "getDirectoryPathname", "()Ljava/lang/String;", "getFormat", "()Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Format", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final b.a a;
   private final String b;

   public b(b.a var, String var) {
      l.b(var, "format");
      l.b(var, "directoryPathname");
      super();
      this.a = var;
      this.b = var;
   }

   public final b.a a() {
      return this.a;
   }

   public final String b() {
      return this.b;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof b) {
               b var = (b)var;
               if(l.a(this.a, var.a) && l.a(this.b, var.b)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      b.a var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.b;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "ExportOutput(format=" + this.a + ", directoryPathname=" + this.b + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0019\b\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "", "displayTextRes", "", "extension", "", "(Ljava/lang/String;IILjava/lang/String;)V", "getDisplayTextRes", "()I", "getExtension", "()Ljava/lang/String;", "CSV", "QIF", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b;

      private final int d;
      private final String e;

      static {
         b.a var = new b.a("CSV", 0, 2131362733, ".csv");
         a = var;
         b.a var = new b.a("QIF", 1, 2131362734, ".qif");
         b = var;
      }

      protected a(int var, String var) {
         l.b(var, "extension");
         super(var, var);
         this.d = var;
         this.e = var;
      }

      public final int a() {
         return this.d;
      }

      public final String b() {
         return this.e;
      }
   }
}
