package co.uk.getmondo.spending.b.a.a;

import co.uk.getmondo.common.v;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001fB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u000e\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u001bJ\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006 "},
   d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportPeriod;", "", "startDate", "Lorg/threeten/bp/LocalDate;", "endDate", "displayText", "", "length", "Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;)V", "getDisplayText", "()Ljava/lang/String;", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getLength", "()Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "getStartDate", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "getDescription", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "hashCode", "", "toString", "Length", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final LocalDate a;
   private final LocalDate b;
   private final String c;
   private final c.a d;

   public c(LocalDate var, LocalDate var, String var, c.a var) {
      l.b(var, "startDate");
      l.b(var, "endDate");
      l.b(var, "displayText");
      l.b(var, "length");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final String a(v var) {
      l.b(var, "resourceProvider");
      c.a var = this.d;
      String var;
      switch(d.a[var.ordinal()]) {
      case 1:
         var = "" + this.a.f().a(TextStyle.a, Locale.getDefault()) + ' ' + this.a.d();
         break;
      case 2:
         var = var.a(2131362731);
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var;
   }

   public final LocalDate a() {
      return this.a;
   }

   public final LocalDate b() {
      return this.b;
   }

   public final String c() {
      return this.c;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof c) {
               c var = (c)var;
               if(l.a(this.a, var.a) && l.a(this.b, var.b) && l.a(this.c, var.c) && l.a(this.d, var.d)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      LocalDate var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      c.a var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "ExportPeriod(startDate=" + this.a + ", endDate=" + this.b + ", displayText=" + this.c + ", length=" + this.d + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "", "(Ljava/lang/String;I)V", "MONTH", "ALL_TIME", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b;

      static {
         c.a var = new c.a("MONTH", 0);
         a = var;
         c.a var = new c.a("ALL_TIME", 1);
         b = var;
      }
   }
}
