package co.uk.getmondo.spending.b.a.a;

import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003¢\u0006\u0002\u0010\u000fJ\t\u0010\u0010\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÂ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÂ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÂ\u0003J\u0081\u0001\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\u0011\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00030#¢\u0006\u0002\u0010$J\t\u0010%\u001a\u00020\u0003HÖ\u0001R\u000e\u0010\f\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006'"},
   d2 = {"Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;", "", "id", "", "created", "amount", "currency", "localAmount", "localCurrency", "category", "emoji", "description", "address", "notes", "receipt", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toComponentRecord", "", "()[Ljava/lang/String;", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   public static final e.a a = new e.a((i)null);
   private static final SimpleDateFormat n = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS Z", Locale.getDefault());
   private static final co.uk.getmondo.common.i.b o = new co.uk.getmondo.common.i.b(false, true, true, 1, (i)null);
   private static final String[] p;
   private final String b;
   private final String c;
   private final String d;
   private final String e;
   private final String f;
   private final String g;
   private final String h;
   private final String i;
   private final String j;
   private final String k;
   private final String l;
   private final String m;

   static {
      Object[] var = (Object[])(new String[]{"id", "created", "amount", "currency", "local_amount", "local_currency", "category", "emoji", "description", "address", "notes", "receipt"});
      p = (String[])var;
   }

   public e(String var, String var, String var, String var, String var, String var, String var, String var, String var, String var, String var, String var) {
      l.b(var, "id");
      l.b(var, "created");
      l.b(var, "amount");
      l.b(var, "currency");
      l.b(var, "localAmount");
      l.b(var, "localCurrency");
      l.b(var, "category");
      l.b(var, "emoji");
      l.b(var, "description");
      l.b(var, "address");
      l.b(var, "notes");
      l.b(var, "receipt");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
   }

   public final String[] a() {
      Object[] var = (Object[])(new String[]{this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m});
      return (String[])var;
   }

   public boolean equals(Object var) {
      boolean var;
      label57: {
         if(this != var) {
            if(!(var instanceof e)) {
               break label57;
            }

            e var = (e)var;
            if(!l.a(this.b, var.b) || !l.a(this.c, var.c) || !l.a(this.d, var.d) || !l.a(this.e, var.e) || !l.a(this.f, var.f) || !l.a(this.g, var.g) || !l.a(this.h, var.h) || !l.a(this.i, var.i) || !l.a(this.j, var.j) || !l.a(this.k, var.k) || !l.a(this.l, var.l) || !l.a(this.m, var.m)) {
               break label57;
            }
         }

         var = true;
         return var;
      }

      var = false;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.e;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.f;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.g;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.h;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.i;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.j;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.k;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.l;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.m;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + (var + (var + (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "TransactionRecordItem(id=" + this.b + ", created=" + this.c + ", amount=" + this.d + ", currency=" + this.e + ", localAmount=" + this.f + ", localCurrency=" + this.g + ", category=" + this.h + ", emoji=" + this.i + ", description=" + this.j + ", address=" + this.k + ", notes=" + this.l + ", receipt=" + this.m + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0017R\u0019\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem$Companion;", "", "()V", "HEADERS", "", "", "getHEADERS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "getAmountFormatter", "()Lco/uk/getmondo/common/money/AmountFormatter;", "dateFormatter", "Ljava/text/SimpleDateFormat;", "getDateFormatter", "()Ljava/text/SimpleDateFormat;", "formatAddress", "merchant", "Lco/uk/getmondo/model/Merchant;", "from", "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;", "transaction", "Lco/uk/getmondo/model/Transaction;", "getDescription", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      private final SimpleDateFormat b() {
         return e.n;
      }

      private final co.uk.getmondo.common.i.b c() {
         return e.o;
      }

      public final e a(aj var) {
         l.b(var, "transaction");
         String var = var.w();
         l.a(var, "transaction.id");
         String var = ((e.a)this).b().format(var.v());
         l.a(var, "dateFormatter.format(transaction.created)");
         co.uk.getmondo.common.i.b var = ((e.a)this).c();
         co.uk.getmondo.d.c var = var.g();
         l.a(var, "transaction.amount");
         String var = var.a(var);
         String var = var.g().l().b();
         if(var == null) {
            var = "";
         }

         var = ((e.a)this).c();
         co.uk.getmondo.d.c var = var.h();
         l.a(var, "transaction.localAmount");
         String var = var.a(var);
         String var = var.h().l().b();
         if(var == null) {
            var = "";
         }

         String var;
         String var;
         label42: {
            var = var.c().f();
            l.a(var, "transaction.category.apiValue");
            u var = var.f();
            if(var != null) {
               var = var.l();
               if(var != null) {
                  break label42;
               }
            }

            var = "";
         }

         String var;
         String var;
         label37: {
            var = ((e.a)this).b(var);
            u var = var.f();
            if(var != null) {
               e.a var = e.a;
               l.a(var, "it");
               var = var.a(var);
               if(var != null) {
                  break label37;
               }
            }

            var = "";
         }

         String var = var.e();
         if(var == null) {
            var = "";
         }

         Iterable var = (Iterable)var.A();
         Collection var = (Collection)(new ArrayList(m.a(var, 10)));
         Iterator var = var.iterator();

         while(var.hasNext()) {
            var.add(((co.uk.getmondo.d.d)var.next()).b());
         }

         List var = (List)var;
         String var;
         if(var.isEmpty()) {
            var = "";
         } else {
            var = var.toString();
         }

         e var = new e(var, var, var, var, var, var, var, var, var, var, var, var);
         return var;
      }

      public final String a(u var) {
         l.b(var, "merchant");
         String var;
         if(var.k()) {
            var = "Online transaction";
         } else {
            Object var = var.c().a((Object)"");
            l.a(var, "merchant.formattedAddress.orElse(\"\")");
            var = (String)var;
         }

         return var;
      }

      public final String[] a() {
         return e.p;
      }

      public final String b(aj var) {
         l.b(var, "transaction");
         String var;
         if(var.f() != null) {
            u var = var.f();
            if(var == null) {
               l.a();
            }

            var = var.i();
            l.a(var, "transaction.merchant!!.name");
         } else {
            if(var.B() != null) {
               String var = var.B().b();
               if(var.g().a()) {
                  var = "Payment to " + var;
               } else {
                  var = "Payment from " + var;
               }
            } else {
               boolean var;
               if(!j.a((CharSequence)var.x())) {
                  var = true;
               } else {
                  var = false;
               }

               if(var) {
                  var = var.x();
               } else {
                  var = "";
               }
            }

            l.a(var, "if (transaction.peer != …         \"\"\n            }");
         }

         return var;
      }
   }
}
