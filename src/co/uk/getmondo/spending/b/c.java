package co.uk.getmondo.spending.b;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.v;
import co.uk.getmondo.common.e.e;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.spending.a.i;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.z;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.ab;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;
import org.threeten.bp.format.TextStyle;
import org.threeten.bp.temporal.TemporalAccessor;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B;\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/spending/export/ExportPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/spending/export/ExportPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "spendingRepository", "Lco/uk/getmondo/spending/data/SpendingRepository;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "transactionExporter", "Lco/uk/getmondo/spending/export/data/TransactionExporter;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/common/ResourceProvider;Lco/uk/getmondo/spending/export/data/TransactionExporter;Lco/uk/getmondo/common/AnalyticsService;)V", "getPeriodDisplayName", "", "yearMonth", "Lorg/threeten/bp/YearMonth;", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final i e;
   private final v f;
   private final co.uk.getmondo.spending.b.a.a g;
   private final co.uk.getmondo.common.a h;

   public c(u var, u var, i var, v var, co.uk.getmondo.spending.b.a.a var, co.uk.getmondo.common.a var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "spendingRepository");
      l.b(var, "resourceProvider");
      l.b(var, "transactionExporter");
      l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   private final String a(YearMonth var) {
      return "" + var.c().a(TextStyle.a, Locale.getDefault()) + ' ' + var.b();
   }

   public void a(final c.a var) {
      l.b(var, "view");
      super.a((f)var);
      io.reactivex.v var = var.c().d((h)(new h() {
         public final List a(List var) {
            l.b(var, "it");
            Iterable var = (Iterable)var;
            Collection var = (Collection)(new ArrayList(m.a(var, 10)));
            Iterator var = var.iterator();

            while(var.hasNext()) {
               YearMonth var = (YearMonth)var.next();
               c var = c.this;
               YearMonth var = YearMonth.a(var.b(), var.c());
               l.a(var, "YearMonth.of(it.year, it.month)");
               String var = var.a(var);
               LocalDate var = var.c(1);
               l.a(var, "it.atDay(1)");
               LocalDate var = var.f();
               l.a(var, "it.atEndOfMonth()");
               co.uk.getmondo.spending.b.a.a.c var = new co.uk.getmondo.spending.b.a.a.c(var, var, var, co.uk.getmondo.spending.b.a.a.c.a.a);
               var.add(var);
            }

            return (List)var;
         }
      }));
      io.reactivex.v var = this.e.b().d((h)(new h() {
         public final co.uk.getmondo.spending.b.a.a.c a(LocalDate var) {
            l.b(var, "it");
            String var = DateTimeFormatter.a(FormatStyle.d).a((TemporalAccessor)var);
            String var = c.this.f.a(2131362736);
            LocalDate var = LocalDate.a();
            l.a(var, "LocalDate.now()");
            ab var = ab.a;
            Object[] var = new Object[]{var};
            var = String.format(var, Arrays.copyOf(var, var.length));
            l.a(var, "java.lang.String.format(format, *args)");
            return new co.uk.getmondo.spending.b.a.a.c(var, var, var, co.uk.getmondo.spending.b.a.a.c.a.b);
         }
      }));
      io.reactivex.b.a var = this.b;
      l.a(var, "allExportPeriod");
      var = var.a((z)var, (io.reactivex.c.c)(new io.reactivex.c.c() {
         public final Object a(Object var, Object var) {
            co.uk.getmondo.spending.b.a.a.c var = (co.uk.getmondo.spending.b.a.a.c)var;
            return m.a((Collection)((List)var), var);
         }
      }));
      l.a(var, "zipWith(other, BiFunctio…-> zipper.invoke(t, u) })");
      io.reactivex.b.b var = var.e((g)(new g() {
         public final void a(List varx) {
            c.a var = var;
            l.a(varx, "exportPeriods");
            var.a(varx);
         }
      }));
      l.a(var, "monthExportPeriods\n     …Periods(exportPeriods) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      n var = var.d().doOnNext((g)(new g() {
         public final void a(co.uk.getmondo.spending.b.a.a.c varx) {
            var.b(kotlin.a.g.m((Object[])co.uk.getmondo.spending.b.a.a.b.a.values()));
         }
      })).zipWith((r)var.e(), (io.reactivex.c.c)(new io.reactivex.c.c() {
         public final Object a(Object var, Object var) {
            co.uk.getmondo.spending.b.a.a.b var = (co.uk.getmondo.spending.b.a.a.b)var;
            return kotlin.l.a((co.uk.getmondo.spending.b.a.a.c)var, var);
         }
      }));
      l.a(var, "zipWith(other, BiFunctio…-> zipper.invoke(t, u) })");
      io.reactivex.b.b var = var.flatMapSingle((h)(new h() {
         public final io.reactivex.v a(kotlin.h var) {
            l.b(var, "<name for destructuring parameter 0>");
            final co.uk.getmondo.spending.b.a.a.c var = (co.uk.getmondo.spending.b.a.a.c)var.c();
            final co.uk.getmondo.spending.b.a.a.b var = (co.uk.getmondo.spending.b.a.a.b)var.d();
            return c.this.e.a(var.a(), var.b()).d((h)(new h() {
               public final co.uk.getmondo.spending.b.a.a.a a(List var) {
                  l.b(var, "it");
                  co.uk.getmondo.spending.b.a.a.b varx = var;
                  l.a(varx, "exportOutput");
                  return new co.uk.getmondo.spending.b.a.a.a(varx, var.a(c.this.f), var);
               }
            })).a((h)(new h() {
               public final io.reactivex.v a(co.uk.getmondo.spending.b.a.a.a var) {
                  l.b(var, "it");
                  return c.this.g.a(var);
               }
            })).b(c.this.c).a(c.this.d).c((g)(new g() {
               public final void a(String var) {
                  co.uk.getmondo.common.a var = c.this.h;
                  Impression.Companion var = Impression.Companion;
                  String varx = var.a().name();
                  if(varx == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  } else {
                     varx = varx.toLowerCase();
                     l.a(varx, "(this as java.lang.String).toLowerCase()");
                     var.a(Impression.Companion.a(var, (String)null, varx, 1, (Object)null));
                  }
               }
            }));
         }
      })).subscribe((g)(new g() {
         public final void a(String varx) {
            c.a var = var;
            l.a(varx, "it");
            var.a(varx);
         }
      }), (g)(new g() {
         public final void a(Throwable varx) {
            d.a.a.a(varx);
            var.b(2131362732);
         }
      }));
      l.a(var, "view.exportPeriodClicks\n…                       })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0016\u0010\u0011\u001a\u00020\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\rH&J\u0016\u0010\u0015\u001a\u00020\u00122\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\rH&J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0019H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0007R\u001e\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/spending/export/ExportPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ErrorView;", "exportOutputClicks", "Lio/reactivex/Observable;", "Lco/uk/getmondo/spending/export/data/model/ExportOutput;", "getExportOutputClicks", "()Lio/reactivex/Observable;", "exportPeriodClicks", "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;", "getExportPeriodClicks", "months", "Lio/reactivex/Single;", "", "Lorg/threeten/bp/YearMonth;", "getMonths", "()Lio/reactivex/Single;", "showExportFormats", "", "formats", "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "showExportPeriods", "periods", "showExportShare", "filePathname", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends e, f {
      void a(String var);

      void a(List var);

      void b(List var);

      io.reactivex.v c();

      n d();

      n e();
   }
}
