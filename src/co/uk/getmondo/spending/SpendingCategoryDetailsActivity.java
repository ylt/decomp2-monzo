package co.uk.getmondo.spending;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.support.v4.app.r;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.h;
import co.uk.getmondo.spending.merchant.SpendingByMerchantFragment;
import co.uk.getmondo.spending.transactions.SpendingTransactionsFragment;
import org.threeten.bp.YearMonth;

public class SpendingCategoryDetailsActivity extends co.uk.getmondo.common.activities.b {
   private YearMonth a;
   @BindView(2131821096)
   AppBarLayout appBarLayout;
   private h b;
   @BindView(2131821097)
   TabLayout tabLayout;
   @BindView(2131820798)
   Toolbar toolbar;
   @BindView(2131821095)
   ViewPager viewPager;

   public static void a(Context var, YearMonth var, h var) {
      var.startActivity(b(var, var, var));
   }

   public static Intent b(Context var, YearMonth var, h var) {
      Intent var = new Intent(var, SpendingCategoryDetailsActivity.class);
      var.putExtra("year_month", var);
      var.putExtra("category", var);
      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034211);
      ButterKnife.bind((Activity)this);
      this.a = (YearMonth)this.getIntent().getSerializableExtra("year_month");
      this.b = (h)this.getIntent().getSerializableExtra("category");
      this.setSupportActionBar(this.toolbar);
      this.viewPager.setAdapter(new SpendingCategoryDetailsActivity.a(this.getSupportFragmentManager()));
      this.tabLayout.setupWithViewPager(this.viewPager);
      this.toolbar.setTitle(this.b.a());
      int var = android.support.v4.content.a.c(this, this.b.b());
      this.appBarLayout.setBackgroundColor(var);
      this.getWindow().setStatusBarColor(co.uk.getmondo.common.k.c.a(var));
   }

   private class a extends r {
      public a(n var) {
         super(var);
      }

      public Fragment a(int var) {
         Object var;
         switch(var) {
         case 0:
            var = SpendingTransactionsFragment.a(SpendingCategoryDetailsActivity.this.a, SpendingCategoryDetailsActivity.this.b);
            break;
         case 1:
            var = SpendingByMerchantFragment.a(SpendingCategoryDetailsActivity.this.a, SpendingCategoryDetailsActivity.this.b);
            break;
         default:
            throw new RuntimeException("Invalid position for spending pager adapter");
         }

         return (Fragment)var;
      }

      public int b() {
         return 2;
      }

      public CharSequence c(int var) {
         String var;
         switch(var) {
         case 0:
            var = SpendingCategoryDetailsActivity.this.getString(2131362740);
            break;
         case 1:
            var = SpendingCategoryDetailsActivity.this.getString(2131362739);
            break;
         default:
            throw new RuntimeException("Invalid position for spending pager adapter");
         }

         return var;
      }
   }
}
