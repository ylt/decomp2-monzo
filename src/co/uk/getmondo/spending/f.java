package co.uk.getmondo.spending;

import co.uk.getmondo.spending.a.i;
import io.reactivex.u;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                                 if(!a && var == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var;
                                    if(!a && var == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new f(var, var, var, var, var, var, var, var, var, var, var);
   }

   public d a() {
      return (d)b.a.c.a(this.b, new d((u)this.c.b(), (u)this.d.b(), (u)this.e.b(), (i)this.f.b(), (co.uk.getmondo.spending.a.b)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (co.uk.getmondo.payments.a.i)this.i.b(), (co.uk.getmondo.common.e.a)this.j.b(), (co.uk.getmondo.card.c)this.k.b(), (co.uk.getmondo.common.accounts.b)this.l.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
