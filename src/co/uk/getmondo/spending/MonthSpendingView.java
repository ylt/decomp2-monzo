package co.uk.getmondo.spending;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.h;
import co.uk.getmondo.spending.a.g;
import com.bumptech.glide.g.b.j;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

public class MonthSpendingView extends FrameLayout {
   private final co.uk.getmondo.common.ui.a a;
   private YearMonth b;
   private MonthSpendingView.a c;
   @BindView(2131821754)
   LinearLayout cardsContainer;
   private int d;
   @BindView(2131821753)
   AmountView monthAmountView;
   @BindView(2131821616)
   TextView monthNameTextView;

   public MonthSpendingView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public MonthSpendingView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public MonthSpendingView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.a = co.uk.getmondo.common.ui.a.a(var);
      LayoutInflater.from(var).inflate(2131034435, this);
      ButterKnife.bind((View)this);
      if(this.isInEditMode()) {
         ArrayList var = new ArrayList();
         g var = new g(new co.uk.getmondo.d.c(2000L, "GBP"), 3, new ArrayList());
         h[] var = h.values();
         int var = var.length;

         for(var = 0; var < var; ++var) {
            var.add(new co.uk.getmondo.spending.a.h.a(var, var[var]));
         }

         this.setSpendingBreakdownData(var);
      }

      this.d = (int)TypedValue.applyDimension(2, 20.0F, this.getResources().getDisplayMetrics());
   }

   private void setSpendingBreakdownData(List var) {
      int var;
      for(var = 0; var < var.size(); ++var) {
         co.uk.getmondo.spending.a.h var = (co.uk.getmondo.spending.a.h)var.get(var);
         View var = this.cardsContainer.getChildAt(var);
         MonthSpendingView.ViewHolder var;
         if(var != null) {
            var.setVisibility(0);
            var = (MonthSpendingView.ViewHolder)var.getTag();
         } else {
            View var = LayoutInflater.from(this.getContext()).inflate(2131034378, this.cardsContainer, false);
            var = new MonthSpendingView.ViewHolder(var);
            var.setTag(var);
            this.cardsContainer.addView(var);
         }

         var.a(var);
      }

      if(this.cardsContainer.getChildCount() > var.size()) {
         for(var = var.size(); var < this.cardsContainer.getChildCount(); ++var) {
            this.cardsContainer.getChildAt(var).setVisibility(8);
         }
      }

   }

   public void a(YearMonth var, g var) {
      this.b = var;
      this.monthAmountView.setAmount(var.a());
      this.monthNameTextView.setText(var.c().a(TextStyle.a, Locale.ENGLISH));
      this.setSpendingBreakdownData(var.c());
   }

   public void setItemClickListener(MonthSpendingView.a var) {
      this.c = var;
   }

   class ViewHolder {
      @BindView(2131821019)
      AmountView amountView;
      private co.uk.getmondo.spending.a.h b;
      @BindView(2131821617)
      CardView cardView;
      @BindView(2131821618)
      ImageView logoImageView;
      @BindView(2131821619)
      TextView subtitleTextView;
      @BindView(2131821026)
      TextView titleTextView;

      ViewHolder(View var) {
         ButterKnife.bind(this, (View)var);
      }

      private void a(co.uk.getmondo.spending.a.h.a var) {
         this.titleTextView.setText(var.b().a());
         this.logoImageView.setImageResource(var.b().d());
         this.logoImageView.setClipToOutline(false);
         this.logoImageView.setBackgroundResource(0);
      }

      private void a(co.uk.getmondo.spending.a.h.b var) {
         this.titleTextView.setText(var.b().i());
         this.logoImageView.setClipToOutline(true);
         this.logoImageView.setBackgroundResource(2130838008);
         if(!p.d(var.b().j())) {
            com.bumptech.glide.g.b(this.logoImageView.getContext()).a(var.b().j()).a().a(this.logoImageView);
         } else {
            this.logoImageView.setImageResource(var.b().b().c());
         }

      }

      private void a(co.uk.getmondo.spending.a.h.c var) {
         aa var = var.b();
         this.titleTextView.setText(var.b());
         if(var.f()) {
            com.bumptech.glide.g.b(this.logoImageView.getContext()).a(var.g()).h().a().a((j)(new co.uk.getmondo.common.ui.c(this.logoImageView)));
         } else {
            this.logoImageView.setImageDrawable(MonthSpendingView.this.a.a(var.b()).a(MonthSpendingView.this.d));
         }

      }

      void a(co.uk.getmondo.spending.a.h var) {
         this.b = var;
         co.uk.getmondo.d.c var = var.a().a();
         AmountView var = this.amountView;
         if(var.e() >= 1.0D) {
            var = var.h();
         } else {
            var = var.i();
         }

         var.setAmount(var);
         int var = var.a().b();
         this.subtitleTextView.setText(MonthSpendingView.this.getResources().getQuantityString(2131886084, var, new Object[]{Integer.valueOf(var)}));
         if(var instanceof co.uk.getmondo.spending.a.h.a) {
            this.a((co.uk.getmondo.spending.a.h.a)var);
         } else if(var instanceof co.uk.getmondo.spending.a.h.b) {
            this.a((co.uk.getmondo.spending.a.h.b)var);
         } else if(var instanceof co.uk.getmondo.spending.a.h.c) {
            this.a((co.uk.getmondo.spending.a.h.c)var);
         }

      }

      @OnClick({2131821617})
      void onCardClicked() {
         if(MonthSpendingView.this.c != null) {
            MonthSpendingView.this.c.a(MonthSpendingView.this.b, this.b);
         }

      }
   }

   public interface a {
      void a(YearMonth var, co.uk.getmondo.spending.a.h var);
   }
}
