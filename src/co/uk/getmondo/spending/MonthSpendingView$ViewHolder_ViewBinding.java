package co.uk.getmondo.spending;

import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthSpendingView$ViewHolder_ViewBinding implements Unbinder {
   private MonthSpendingView.ViewHolder a;
   private View b;

   public MonthSpendingView$ViewHolder_ViewBinding(final MonthSpendingView.ViewHolder var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131821617, "field 'cardView' and method 'onCardClicked'");
      var.cardView = (CardView)Utils.castView(var, 2131821617, "field 'cardView'", CardView.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCardClicked();
         }
      });
      var.logoImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821618, "field 'logoImageView'", ImageView.class);
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var.subtitleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821619, "field 'subtitleTextView'", TextView.class);
      var.amountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821019, "field 'amountView'", AmountView.class);
   }

   public void unbind() {
      MonthSpendingView.ViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.cardView = null;
         var.logoImageView = null;
         var.titleTextView = null;
         var.subtitleTextView = null;
         var.amountView = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
