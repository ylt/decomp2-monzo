package co.uk.getmondo.news;

import co.uk.getmondo.api.model.ApiNewsItem;
import io.reactivex.c.q;

// $FF: synthetic class
final class i implements q {
   private final d a;

   private i(d var) {
      this.a = var;
   }

   public static q a(d var) {
      return new i(var);
   }

   public boolean a(Object var) {
      return d.a(this.a, (ApiNewsItem)var);
   }
}
