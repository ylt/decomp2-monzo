package co.uk.getmondo.news;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiNewsItem;
import io.reactivex.z;
import java.util.List;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.chrono.ChronoLocalDateTime;

public class d {
   private final MonzoApi a;
   private final co.uk.getmondo.settings.k b;

   public d(MonzoApi var, co.uk.getmondo.settings.k var) {
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   static c a(List var) throws Exception {
      ApiNewsItem var = (ApiNewsItem)var.get(0);
      return new c(var.a(), var.b(), var.c());
   }

   // $FF: synthetic method
   static z a(d var, List var) throws Exception {
      return io.reactivex.n.fromIterable(var).filter(i.a(var)).toList();
   }

   private boolean a(ApiNewsItem var) {
      return LocalDateTime.a().c((ChronoLocalDateTime)var.d());
   }

   // $FF: synthetic method
   static boolean a(d var, ApiNewsItem var) throws Exception {
      boolean var;
      if(var.a(var) && !var.b(var)) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private boolean b(ApiNewsItem var) {
      return this.b.a(var.a());
   }

   // $FF: synthetic method
   static boolean b(List var) throws Exception {
      boolean var;
      if(!var.isEmpty()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public io.reactivex.h a() {
      return this.a.news().d(e.a()).a(f.a(this)).a(g.a()).c(h.a());
   }
}
