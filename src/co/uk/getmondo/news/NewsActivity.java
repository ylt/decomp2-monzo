package co.uk.getmondo.news;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsActivity extends co.uk.getmondo.common.activities.b implements m.a {
   m a;
   @BindView(2131821004)
   WebView newsWebView;
   @BindView(2131820798)
   Toolbar toolbar;

   public static Intent a(Context var, c var) {
      Intent var = new Intent(var, NewsActivity.class);
      var.putExtra("KEY_NEWS_ITEM", var);
      return var;
   }

   public void a(c var) {
      this.toolbar.setNavigationIcon(2130837837);
      this.newsWebView.loadUrl(var.b());
      this.setTitle(var.c());
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034189);
      ButterKnife.bind((Activity)this);
      this.l().a(new k((c)this.getIntent().getParcelableExtra("KEY_NEWS_ITEM"))).a(this);
      this.a.a((m.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
