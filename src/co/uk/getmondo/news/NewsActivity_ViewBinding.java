package co.uk.getmondo.news;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class NewsActivity_ViewBinding implements Unbinder {
   private NewsActivity a;

   public NewsActivity_ViewBinding(NewsActivity var, View var) {
      this.a = var;
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
      var.newsWebView = (WebView)Utils.findRequiredViewAsType(var, 2131821004, "field 'newsWebView'", WebView.class);
   }

   public void unbind() {
      NewsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.toolbar = null;
         var.newsWebView = null;
      }
   }
}
