package co.uk.getmondo.news;

import co.uk.getmondo.api.model.tracking.Impression;

class m extends co.uk.getmondo.common.ui.b {
   private final c c;
   private final co.uk.getmondo.settings.k d;
   private final co.uk.getmondo.common.a e;

   m(c var, co.uk.getmondo.settings.k var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public void a(m.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.c);
      this.d.b(this.c.a());
      this.e.a(Impression.Y());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(c var);
   }
}
