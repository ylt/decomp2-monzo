package co.uk.getmondo.news;

import co.uk.getmondo.api.MonzoApi;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new j(var, var);
   }

   public d a() {
      return new d((MonzoApi)this.b.b(), (co.uk.getmondo.settings.k)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
