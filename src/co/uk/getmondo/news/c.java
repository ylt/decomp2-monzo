package co.uk.getmondo.news;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001d\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0006HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001J\u001a\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/news/NewsItem;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "uri", "title", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getId", "()Ljava/lang/String;", "getTitle", "getUri", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public c a(Parcel var) {
         kotlin.d.b.l.b(var, "source");
         return new c(var);
      }

      public c[] a(int var) {
         return new c[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final c.a a = new c.a((kotlin.d.b.i)null);
   private final String b;
   private final String c;
   private final String d;

   public c(Parcel var) {
      kotlin.d.b.l.b(var, "source");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      String var = var.readString();
      kotlin.d.b.l.a(var, "source.readString()");
      this(var, var, var);
   }

   public c(String var, String var, String var) {
      kotlin.d.b.l.b(var, "id");
      kotlin.d.b.l.b(var, "uri");
      kotlin.d.b.l.b(var, "title");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final String a() {
      return this.b;
   }

   public final String b() {
      return this.c;
   }

   public final String c() {
      return this.d;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof c) {
               c var = (c)var;
               if(kotlin.d.b.l.a(this.b, var.b) && kotlin.d.b.l.a(this.c, var.c) && kotlin.d.b.l.a(this.d, var.d)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "NewsItem(id=" + this.b + ", uri=" + this.c + ", title=" + this.d + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      if(var != null) {
         var.writeString(this.b);
      }

      if(var != null) {
         var.writeString(this.c);
      }

      if(var != null) {
         var.writeString(this.d);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/news/NewsItem$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/news/NewsItem;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
