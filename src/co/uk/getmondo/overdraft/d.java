package co.uk.getmondo.overdraft;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.i;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \r2\u00020\u0001:\u0002\r\u000eB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment;", "Landroid/support/v4/app/DialogFragment;", "()V", "listener", "Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$NewLimitConfirmationListener;", "onAttach", "", "context", "Landroid/content/Context;", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "NewLimitConfirmationListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends i {
   public static final d.a a = new d.a((kotlin.d.b.i)null);
   private d.b b;
   private HashMap c;

   // $FF: synthetic method
   public static final d.b a(d var) {
      d.b var = var.b;
      if(var == null) {
         l.b("listener");
      }

      return var;
   }

   public void a() {
      if(this.c != null) {
         this.c.clear();
      }

   }

   public void onAttach(Context var) {
      super.onAttach(var);
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.overdraft.ConfirmNewLimitDialogFragment.NewLimitConfirmationListener");
      } else {
         this.b = (d.b)var;
      }
   }

   public Dialog onCreateDialog(Bundle var) {
      AlertDialog var = (new Builder((Context)this.getActivity(), 2131493136)).setTitle(2131362527).setMessage((CharSequence)this.getString(2131362526, new Object[]{this.getArguments().getParcelable("ARG_LIMIT")})).setPositiveButton((CharSequence)this.getString(2131362141), (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            d.a(d.this).h();
         }
      })).setNegativeButton((CharSequence)this.getString(2131362140), (OnClickListener)null).create();
      l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.a();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$Companion;", "", "()V", "ARG_LIMIT", "", "newInstance", "Landroid/support/v4/app/DialogFragment;", "newLimit", "Lco/uk/getmondo/model/Amount;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final i a(co.uk.getmondo.d.c var) {
         l.b(var, "newLimit");
         d var = new d();
         Bundle var = new Bundle();
         var.putParcelable("ARG_LIMIT", (Parcelable)var);
         var.setArguments(var);
         return (i)var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$NewLimitConfirmationListener;", "", "onNewLimitConfirmed", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void h();
   }
}
