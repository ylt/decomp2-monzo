package co.uk.getmondo.overdraft;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.ProgressButton;
import io.reactivex.n;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u0000 :2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001:B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0018\u001a\u00020\u0007H\u0016J\"\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u00072\b\u0010 \u001a\u0004\u0018\u00010!H\u0014J\u0012\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\b\u0010&\u001a\u00020\u0007H\u0014J\b\u0010'\u001a\u00020\u0007H\u0016J\u0010\u0010(\u001a\u00020#2\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J\u0010\u00100\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J\u0010\u00101\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J \u00102\u001a\u00020\u00072\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u0002042\u0006\u00106\u001a\u000204H\u0016J\b\u00107\u001a\u00020\u0007H\u0016J\u0010\u00108\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00109\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016R\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0011X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\tR\u001a\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\t¨\u0006;"},
   d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;", "Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$NewLimitConfirmationListener;", "()V", "buttonConfirmationClicks", "Lio/reactivex/Observable;", "", "getButtonConfirmationClicks", "()Lio/reactivex/Observable;", "changeOverdraftLimitPresenter", "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;", "getChangeOverdraftLimitPresenter", "()Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;", "setChangeOverdraftLimitPresenter", "(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;)V", "dialogConfirmationClicks", "Lcom/jakewharton/rxrelay2/PublishRelay;", "getDialogConfirmationClicks", "()Lcom/jakewharton/rxrelay2/PublishRelay;", "minusButtonClicks", "getMinusButtonClicks", "plusButtonClicks", "getPlusButtonClicks", "hideLoading", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onNewLimitConfirmed", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "openOverdraftConfirmationScreen", "newLimit", "Lco/uk/getmondo/model/Amount;", "setConfirmationButtonEnabled", "enabled", "setMinusButtonEnabled", "setPlusButtonEnabled", "showDescription", "dailyFee", "", "buffer", "maxCharge", "showLoading", "showNewLimit", "showOverdraftConfirmationPrompt", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ChangeOverdraftLimitActivity extends co.uk.getmondo.common.activities.b implements b.a, d.b {
   public static final ChangeOverdraftLimitActivity.a b = new ChangeOverdraftLimitActivity.a((i)null);
   private static final String e = "CONFIRMATION_FRAGMENT_TAG";
   private static final int f = 1;
   public b a;
   private final com.b.b.c c;
   private HashMap g;

   public ChangeOverdraftLimitActivity() {
      com.b.b.c var = com.b.b.c.a();
      l.a(var, "PublishRelay.create<Unit>()");
      this.c = var;
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.plusButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(co.uk.getmondo.d.c var) {
      l.b(var, "newLimit");
      ((AmountView)this.a(co.uk.getmondo.c.a.currentLimitView)).setAmount(var);
   }

   public void a(String var, String var, String var) {
      l.b(var, "dailyFee");
      l.b(var, "buffer");
      l.b(var, "maxCharge");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.changeLimitDescriptionView)));
      ((TextView)this.a(co.uk.getmondo.c.a.changeLimitDescriptionView)).setText((CharSequence)this.getString(2131362522, new Object[]{var, var, var}));
   }

   public void a(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.confirmLimitButton)).setEnabled(var);
   }

   public n b() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.minusButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void b(co.uk.getmondo.d.c var) {
      l.b(var, "newLimit");
      d.a.a(var).show(this.getSupportFragmentManager(), b.a());
   }

   public void b(boolean var) {
      ((Button)this.a(co.uk.getmondo.c.a.minusButton)).setEnabled(var);
   }

   public n c() {
      n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.confirmLimitButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void c(co.uk.getmondo.d.c var) {
      l.b(var, "newLimit");
      this.startActivityForResult(ConfirmationActivity.b((Context)this, this.getString(2131362520), this.getString(2131362521, new Object[]{var.toString()})), b.b());
   }

   public void c(boolean var) {
      ((Button)this.a(co.uk.getmondo.c.a.plusButton)).setEnabled(var);
   }

   public com.b.b.c d() {
      return this.c;
   }

   // $FF: synthetic method
   public n e() {
      return (n)this.d();
   }

   public void f() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.confirmLimitButton)).setLoading(true);
   }

   public void g() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.confirmLimitButton)).setLoading(false);
   }

   public void h() {
      this.d().a((Object)kotlin.n.a);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == b.b() && var == -1) {
         this.finish();
         this.overridePendingTransition(0, 0);
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034152);
      this.l().a(this);
      b var = this.a;
      if(var == null) {
         l.b("changeOverdraftLimitPresenter");
      }

      var.a((b.a)this);
   }

   public boolean onCreateOptionsMenu(Menu var) {
      this.getMenuInflater().inflate(2131951620, var);
      return true;
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("changeOverdraftLimitPresenter");
      }

      var.b();
      super.onDestroy();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      l.b(var, "item");
      boolean var;
      if(var.getItemId() == 2131821778) {
         var = true;
      } else {
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$Companion;", "", "()V", "CONFIRMATION_FRAGMENT_TAG", "", "getCONFIRMATION_FRAGMENT_TAG", "()Ljava/lang/String;", "CONFIRMATION_REQUEST_CODE", "", "getCONFIRMATION_REQUEST_CODE", "()I", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      private final String a() {
         return ChangeOverdraftLimitActivity.e;
      }

      private final int b() {
         return ChangeOverdraftLimitActivity.f;
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, ChangeOverdraftLimitActivity.class);
      }
   }
}
