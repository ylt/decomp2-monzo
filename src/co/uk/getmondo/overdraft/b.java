package co.uk.getmondo.overdraft;

import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.w;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import io.reactivex.c.q;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0011H\u0002J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "overdraftManager", "Lco/uk/getmondo/overdraft/data/OverdraftManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/overdraft/data/OverdraftManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "limitIncrementAmount", "", "maxDaysInMonth", "selectedLimit", "Lco/uk/getmondo/model/Amount;", "status", "Lco/uk/getmondo/model/OverdraftStatus;", "handleNewLimit", "", "newLimit", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final long c;
   private final long d;
   private w e;
   private co.uk.getmondo.d.c f;
   private final u g;
   private final u h;
   private final co.uk.getmondo.common.accounts.b i;
   private final co.uk.getmondo.overdraft.a.c j;
   private final co.uk.getmondo.common.e.a k;

   public b(u var, u var, co.uk.getmondo.common.accounts.b var, co.uk.getmondo.overdraft.a.c var, co.uk.getmondo.common.e.a var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "accountManager");
      l.b(var, "overdraftManager");
      l.b(var, "apiErrorHandler");
      super();
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.c = 10000L;
      this.d = 31L;
   }

   private final void a(co.uk.getmondo.d.c var) {
      boolean var = true;
      this.f = var;
      ((b.a)this.a).a(var);
      b.a var = (b.a)this.a;
      boolean var;
      if(var.k() > 0L) {
         var = true;
      } else {
         var = false;
      }

      var.b(var);
      var = (b.a)this.a;
      long var = var.k();
      w var = this.e;
      if(var == null) {
         l.b("status");
      }

      if(var < var.h().k()) {
         var = var;
      } else {
         var = false;
      }

      var.c(var);
      var = (b.a)this.a;
      var = this.e;
      if(var == null) {
         l.b("status");
      }

      var.a(l.a(var, var.g()) ^ true);
   }

   // $FF: synthetic method
   public static final w b(b var) {
      w var = var.e;
      if(var == null) {
         l.b("status");
      }

      return var;
   }

   // $FF: synthetic method
   public static final co.uk.getmondo.d.c d(b var) {
      co.uk.getmondo.d.c var = var.f;
      if(var == null) {
         l.b("selectedLimit");
      }

      return var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.i.e().flatMap((h)(new h() {
         public final n a(String var) {
            l.b(var, "accountId");
            return b.this.j.b(var);
         }
      })).subscribe((g)(new g() {
         public final void a(w varx) {
            b var = b.this;
            l.a(varx, "status");
            var.e = varx;
            var.a(varx.f().k() + "p", varx.e().toString(), varx.f().c(b.this.d).toString());
            b.this.a(varx.g());
         }
      }));
      l.a(var, "accountManager.accountId….limit)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.a().filter((q)(new q() {
         public final boolean a(kotlin.n var) {
            l.b(var, "it");
            boolean var;
            if(b.d(b.this).k() + b.this.c <= b.b(b.this).h().k()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n var) {
            b.this.a(b.d(b.this).a(b.this.c));
         }
      }));
      l.a(var, "view.plusButtonClicks\n  …+ limitIncrementAmount) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.b().filter((q)(new q() {
         public final boolean a(kotlin.n var) {
            l.b(var, "it");
            boolean var;
            if(b.d(b.this).k() - b.this.c >= 0L) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n var) {
            b.this.a(b.d(b.this).b(b.this.c));
         }
      }));
      l.a(var, "view.minusButtonClicks\n …- limitIncrementAmount) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().subscribe((g)(new g() {
         public final void a(kotlin.n varx) {
            var.b(b.d(b.this));
         }
      }));
      l.a(var, "view.buttonConfirmationC…onPrompt(selectedLimit) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.e().flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(kotlin.n varx) {
            l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)b.this.i.e().flatMapCompletable((h)(new h() {
               public final io.reactivex.b a(String varx) {
                  l.b(varx, "accountId");
                  return b.this.j.a(varx, b.d(b.this));
               }
            })).b(b.this.g).a(b.this.h).c((g)(new g() {
               public final void a(io.reactivex.b.b varx) {
                  var.f();
               }
            })).b((g)(new g() {
               public final void a(Throwable varx) {
                  var.g();
               }
            })).a((g)(new g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = b.this.k;
                  l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n varx) {
            var.c(b.d(b.this));
         }
      }));
      l.a(var, "view.dialogConfirmationC…onScreen(selectedLimit) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u000e\u001a\u00020\u0005H&J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0010\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0010\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J \u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0019H&J\b\u0010\u001c\u001a\u00020\u0005H&J\u0010\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007R\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "buttonConfirmationClicks", "Lio/reactivex/Observable;", "", "getButtonConfirmationClicks", "()Lio/reactivex/Observable;", "dialogConfirmationClicks", "getDialogConfirmationClicks", "minusButtonClicks", "getMinusButtonClicks", "plusButtonClicks", "getPlusButtonClicks", "hideLoading", "openOverdraftConfirmationScreen", "newLimit", "Lco/uk/getmondo/model/Amount;", "setConfirmationButtonEnabled", "enabled", "", "setMinusButtonEnabled", "setPlusButtonEnabled", "showDescription", "dailyFee", "", "buffer", "maxCharge", "showLoading", "showNewLimit", "showOverdraftConfirmationPrompt", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      n a();

      void a(co.uk.getmondo.d.c var);

      void a(String var, String var, String var);

      void a(boolean var);

      n b();

      void b(co.uk.getmondo.d.c var);

      void b(boolean var);

      n c();

      void c(co.uk.getmondo.d.c var);

      void c(boolean var);

      n e();

      void f();

      void g();
   }
}
