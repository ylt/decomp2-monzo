package co.uk.getmondo.overdraft.a;

import co.uk.getmondo.d.w;
import io.reactivex.n;
import io.reactivex.c.h;
import io.reactivex.c.q;
import io.realm.av;
import io.realm.bb;
import io.realm.bg;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\b2\u0006\u0010\t\u001a\u00020\n¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/overdraft/data/OverdraftStorage;", "", "()V", "save", "Lio/reactivex/Completable;", "newStatus", "Lco/uk/getmondo/model/OverdraftStatus;", "status", "Lio/reactivex/Observable;", "accountId", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f {
   public final io.reactivex.b a(final w var) {
      l.b(var, "newStatus");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            varx.d((bb)var);
         }
      }));
   }

   public final n a(final String var) {
      l.b(var, "accountId");
      n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final bg a(av varx) {
            l.b(varx, "realm");
            return varx.a(w.class).a("accountId", var).g();
         }
      })).filter((q)null.a).map((h)null.a).distinctUntilChanged();
      l.a(var, "RxRealm.asObservable { r…  .distinctUntilChanged()");
      return var;
   }
}
