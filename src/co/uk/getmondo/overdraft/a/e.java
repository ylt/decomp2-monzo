package co.uk.getmondo.overdraft.a;

import co.uk.getmondo.api.OverdraftApi;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new e(var, var);
   }

   public c a() {
      return new c((OverdraftApi)this.b.b(), (f)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
