package co.uk.getmondo.overdraft.a;

import co.uk.getmondo.api.OverdraftApi;
import co.uk.getmondo.api.model.overdraft.ApiOverdraftStatus;
import co.uk.getmondo.d.w;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u0011\u001a\u00020\b*\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/overdraft/data/OverdraftManager;", "", "overdraftApi", "Lco/uk/getmondo/api/OverdraftApi;", "overdraftStorage", "Lco/uk/getmondo/overdraft/data/OverdraftStorage;", "(Lco/uk/getmondo/api/OverdraftApi;Lco/uk/getmondo/overdraft/data/OverdraftStorage;)V", "refreshStatus", "Lio/reactivex/Completable;", "accountId", "", "setLimit", "limit", "Lco/uk/getmondo/model/Amount;", "status", "Lio/reactivex/Observable;", "Lco/uk/getmondo/model/OverdraftStatus;", "storeOverdraftStatus", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final OverdraftApi a;
   private final f b;

   public c(OverdraftApi var, f var) {
      l.b(var, "overdraftApi");
      l.b(var, "overdraftStorage");
      super();
      this.a = var;
      this.b = var;
   }

   private final io.reactivex.b a(v var) {
      io.reactivex.b var = var.d((h)(new d((kotlin.d.a.b)(new kotlin.d.a.b(co.uk.getmondo.d.a.l.INSTANCE) {
         public final w a(ApiOverdraftStatus var) {
            l.b(var, "p1");
            return ((co.uk.getmondo.d.a.l)this.b).a(var);
         }

         public final kotlin.reflect.e a() {
            return y.a(co.uk.getmondo.d.a.l.class);
         }

         public final String b() {
            return "apply";
         }

         public final String c() {
            return "apply(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/model/OverdraftStatus;";
         }
      })))).c((h)(new h() {
         public final io.reactivex.b a(w var) {
            l.b(var, "status");
            return c.this.b.a(var);
         }
      }));
      l.a(var, "map(OverdraftMapper::app…aftStorage.save(status) }");
      return var;
   }

   public final io.reactivex.b a(String var) {
      l.b(var, "accountId");
      return this.a(this.a.overdraftStatus(var));
   }

   public final io.reactivex.b a(String var, co.uk.getmondo.d.c var) {
      l.b(var, "accountId");
      l.b(var, "limit");
      OverdraftApi var = this.a;
      long var = var.k();
      String var = var.l().b();
      l.a(var, "limit.currency.currencyCode");
      return this.a(var.setOverdraftLimit(var, var, var));
   }

   public final n b(String var) {
      l.b(var, "accountId");
      return this.b.a(var);
   }
}
