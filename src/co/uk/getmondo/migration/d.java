package co.uk.getmondo.migration;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.common.ac;
import io.reactivex.n;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0013\u001a\u00020\u0014J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0015R$\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR#\u0010\f\u001a\n \u000e*\u0004\u0018\u00010\r0\r8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/migration/MigrationStorage;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "value", "", "hasSeenAnnouncementBanner", "getHasSeenAnnouncementBanner", "()Z", "setHasSeenAnnouncementBanner", "(Z)V", "preferences", "Landroid/content/SharedPreferences;", "kotlin.jvm.PlatformType", "getPreferences", "()Landroid/content/SharedPreferences;", "preferences$delegate", "Lkotlin/Lazy;", "clear", "", "Lio/reactivex/Observable;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(d.class), "preferences", "getPreferences()Landroid/content/SharedPreferences;"))};
   public static final d.a b = new d.a((i)null);
   private final kotlin.c c;

   public d(final Context var) {
      kotlin.d.b.l.b(var, "context");
      super();
      this.c = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final SharedPreferences b() {
            return var.getSharedPreferences("migration-storage", 0);
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
   }

   private final SharedPreferences d() {
      kotlin.c var = this.c;
      l var = a[0];
      return (SharedPreferences)var.a();
   }

   public final void a(boolean var) {
      this.d().edit().putBoolean("KEY_ANNOUNCEMENT_BANNER_SEEN", var).apply();
   }

   public final boolean a() {
      return this.d().getBoolean("KEY_ANNOUNCEMENT_BANNER_SEEN", false);
   }

   public final n b() {
      n var = ac.a(this.d(), "KEY_ANNOUNCEMENT_BANNER_SEEN").map((h)null.a).startWith((Object)Boolean.valueOf(this.a()));
      kotlin.d.b.l.a(var, "preferences.changes(filt…asSeenAnnouncementBanner)");
      return var;
   }

   public final void c() {
      this.d().edit().clear().apply();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/migration/MigrationStorage$Companion;", "", "()V", "KEY_ANNOUNCEMENT_BANNER_SEEN", "", "SHARED_PREFERENCES_NAME", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
