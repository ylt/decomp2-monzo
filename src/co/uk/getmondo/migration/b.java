package co.uk.getmondo.migration;

import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter$View;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "migrationStorage", "Lco/uk/getmondo/migration/MigrationStorage;", "(Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/migration/MigrationStorage;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final d d;

   public b(co.uk.getmondo.common.a var, d var) {
      l.b(var, "analyticsService");
      l.b(var, "migrationStorage");
      super();
      this.c = var;
      this.d = var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      if(var.a()) {
         var.d();
         var.f();
         this.c.a(Impression.Companion.bb());
      } else {
         this.d.a(true);
         var.e();
         this.c.a(Impression.Companion.aY());
      }

      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            if(var.a()) {
               b.this.c.a(Impression.Companion.bd());
               var.g();
            } else {
               var.a(false);
               b.this.c.a(Impression.Companion.aZ());
            }

         }
      }));
      l.a(var, "view.primaryButtonClicke…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            var.a(true);
            b.this.c.a(Impression.Companion.bc());
         }
      }));
      l.a(var, "view.secondaryButtonClic…eTap())\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\bf\u0018\u00002\u00020\u0001J\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH&J\b\u0010\u000e\u001a\u00020\u0004H&J\b\u0010\u000f\u001a\u00020\u0004H&J\b\u0010\u0010\u001a\u00020\u0004H&J\b\u0010\u0011\u001a\u00020\u0004H&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006R\u0012\u0010\t\u001a\u00020\nX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0012"},
      d2 = {"Lco/uk/getmondo/migration/MigrationAnnouncementPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "primaryButtonClicked", "Lio/reactivex/Observable;", "", "getPrimaryButtonClicked", "()Lio/reactivex/Observable;", "secondaryButtonClicked", "getSecondaryButtonClicked", "signupAllowed", "", "getSignupAllowed", "()Z", "openLearnMore", "openMigrationTour", "showLearnMorePrimaryButton", "showLearnMoreSecondaryButton", "showStartSignupButton", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      void a(boolean var);

      boolean a();

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();
   }
}
