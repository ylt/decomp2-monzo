package co.uk.getmondo.migration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.p;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.pager.GenericPagerAdapter;
import co.uk.getmondo.common.pager.h;
import co.uk.getmondo.signup.j;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00162\u00020\u0001:\u0002\u0016\u0017B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0012\u0010\u0012\u001a\u00020\u000f2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\b\u0010\u0015\u001a\u00020\u000fH\u0014R\u001c\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00048BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001e\u0010\b\u001a\u00020\t8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/migration/MigrationTourActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "()V", "infoItemBodies", "", "Landroid/view/View;", "getInfoItemBodies", "()Ljava/util/List;", "pageViewTracker", "Lco/uk/getmondo/common/pager/PageViewTracker;", "getPageViewTracker", "()Lco/uk/getmondo/common/pager/PageViewTracker;", "setPageViewTracker", "(Lco/uk/getmondo/common/pager/PageViewTracker;)V", "makeInfoItemVisible", "", "index", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "Companion", "MigrationInfoPage", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class MigrationTourActivity extends co.uk.getmondo.signup.a {
   public static final MigrationTourActivity.a b = new MigrationTourActivity.a((i)null);
   public h a;
   private HashMap g;

   private final List c() {
      return m.b(new TextView[]{(TextView)this.a(co.uk.getmondo.c.a.migrationItem1Body), (TextView)this.a(co.uk.getmondo.c.a.migrationItem2Body), (TextView)this.a(co.uk.getmondo.c.a.migrationItem3Body)});
   }

   private final void d(int var) {
      boolean var;
      if(var <= m.a(this.c()) && var >= 0) {
         var = true;
      } else {
         var = false;
      }

      if(!var) {
         throw (Throwable)(new IllegalStateException("Check failed.".toString()));
      } else {
         Iterator var = ((Iterable)this.c()).iterator();

         View var;
         while(var.hasNext()) {
            var = (View)var.next();
            if(var != null) {
               ae.b(var);
            }
         }

         var = (View)this.c().get(var);
         if(var != null) {
            ae.a(var);
         }

      }
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034187);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(false);
      }

      var = this.getSupportActionBar();
      if(var != null) {
         var.d(false);
      }

      this.l().a(this);
      ((ViewPager)this.a(co.uk.getmondo.c.a.migrationTourViewPager)).setAdapter((p)(new GenericPagerAdapter(new co.uk.getmondo.common.pager.f[]{(co.uk.getmondo.common.pager.f)(new MigrationTourActivity.b(Impression.Companion.be())), (co.uk.getmondo.common.pager.f)(new co.uk.getmondo.common.pager.b(2131034409, Impression.Companion.bf()))})));
      h var = this.a;
      if(var == null) {
         l.b("pageViewTracker");
      }

      var.a((ViewPager)this.a(co.uk.getmondo.c.a.migrationTourViewPager));
      ((Button)this.a(co.uk.getmondo.c.a.migrationTourButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            if(((ViewPager)MigrationTourActivity.this.a(co.uk.getmondo.c.a.migrationTourViewPager)).getCurrentItem() == 1) {
               MigrationTourActivity.this.startActivity(SignupStatusActivity.c.a((Context)MigrationTourActivity.this, j.a));
               MigrationTourActivity.this.finish();
            } else {
               List var = m.i((Iterable)MigrationTourActivity.this.c());
               Iterable var = (Iterable)var;
               int var = 0;
               Iterator var = var.iterator();

               while(true) {
                  if(!var.hasNext()) {
                     ((ViewPager)MigrationTourActivity.this.a(co.uk.getmondo.c.a.migrationTourViewPager)).a(1, true);
                     break;
                  }

                  View var = (View)var.next();
                  if(var != null && var.getVisibility() == 0 && var != 0) {
                     ae.b(var);
                     var = (View)var.get(var - 1);
                     if(var != null) {
                        ae.a(var);
                     }
                     break;
                  }

                  ++var;
               }
            }

         }
      }));
   }

   protected void onDestroy() {
      h var = this.a;
      if(var == null) {
         l.b("pageViewTracker");
      }

      var.a();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/migration/MigrationTourActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         Intent var = (new Intent(var, MigrationTourActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)j.a);
         l.a(var, "Intent(context, Migratio…gnupEntryPoint.MIGRATION)");
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/migration/MigrationTourActivity$MigrationInfoPage;", "Lco/uk/getmondo/common/pager/CustomViewPage;", "impression", "Lco/uk/getmondo/api/model/tracking/Impression;", "(Lco/uk/getmondo/migration/MigrationTourActivity;Lco/uk/getmondo/api/model/tracking/Impression;)V", "createView", "Landroid/view/View;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class b extends co.uk.getmondo.common.pager.c {
      public b(Impression var) {
         super(var);
      }

      public View c() {
         View var = MigrationTourActivity.this.getLayoutInflater().inflate(2131034408, (ViewPager)MigrationTourActivity.this.a(co.uk.getmondo.c.a.migrationTourViewPager), false);
         var.findViewById(2131821670).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               MigrationTourActivity.this.d(0);
            }
         }));
         var.findViewById(2131821672).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               MigrationTourActivity.this.d(1);
            }
         }));
         var.findViewById(2131821675).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               MigrationTourActivity.this.d(2);
            }
         }));
         l.a(var, "view");
         return var;
      }
   }
}
