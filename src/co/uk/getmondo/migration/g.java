package co.uk.getmondo.migration;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/migration/SnoozeMigrationDialogFragment;", "Landroid/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends DialogFragment {
   private HashMap a;

   public void a() {
      if(this.a != null) {
         this.a.clear();
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      android.support.v7.app.d var = (new android.support.v7.app.d.a((Context)this.getActivity(), 2131493135)).a(2131362446).b(2131362443).a(2131362445, (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            g.this.getActivity().finish();
         }
      })).b(2131362444, (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            g.this.dismiss();
         }
      })).b();
      l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.a();
   }
}
