package co.uk.getmondo.migration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import io.reactivex.n;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000  2\u00020\u00012\u00020\u0002:\u0001 B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0017\u001a\u00020\f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\b\u0010\u001a\u001a\u00020\fH\u0014J\u0010\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u001c\u001a\u00020\fH\u0016J\b\u0010\u001d\u001a\u00020\fH\u0016J\b\u0010\u001e\u001a\u00020\fH\u0016J\b\u0010\u001f\u001a\u00020\fH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\u00128VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0013\u0010\u0014¨\u0006!"},
   d2 = {"Lco/uk/getmondo/migration/MigrationAnnouncementActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;", "getPresenter", "()Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;", "setPresenter", "(Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;)V", "primaryButtonClicked", "Lio/reactivex/Observable;", "", "getPrimaryButtonClicked", "()Lio/reactivex/Observable;", "secondaryButtonClicked", "getSecondaryButtonClicked", "signupAllowed", "", "getSignupAllowed", "()Z", "signupAllowed$delegate", "Lkotlin/Lazy;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "openLearnMore", "openMigrationTour", "showLearnMorePrimaryButton", "showLearnMoreSecondaryButton", "showStartSignupButton", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class MigrationAnnouncementActivity extends co.uk.getmondo.common.activities.b implements b.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(MigrationAnnouncementActivity.class), "signupAllowed", "getSignupAllowed()Z"))};
   public static final MigrationAnnouncementActivity.a c = new MigrationAnnouncementActivity.a((i)null);
   public b b;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final boolean b() {
         return MigrationAnnouncementActivity.this.getIntent().getBooleanExtra("KEY_SIGNUP_ALLOWED", false);
      }

      // $FF: synthetic method
      public Object v_() {
         return Boolean.valueOf(this.b());
      }
   }));
   private HashMap f;

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.f.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void a(boolean var) {
      String var;
      if(var) {
         var = "https://monzo.com/-webviews/account/upgrade";
      } else {
         var = "https://monzo.com/-webviews/announcements/current-account-migration";
      }

      co.uk.getmondo.common.activities.a.a(this, var, 0, false, 6, (Object)null);
   }

   public boolean a() {
      kotlin.c var = this.e;
      l var = a[0];
      return ((Boolean)var.a()).booleanValue();
   }

   public n b() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.migrationPrimaryButton)).map((h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public n c() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.migrationSecondaryButton)).map((h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void d() {
      ((Button)this.a(co.uk.getmondo.c.a.migrationPrimaryButton)).setText((CharSequence)this.getString(2131362420));
   }

   public void e() {
      ((Button)this.a(co.uk.getmondo.c.a.migrationPrimaryButton)).setText((CharSequence)this.getString(2131362419));
   }

   public void f() {
      ae.a((View)((Button)this.a(co.uk.getmondo.c.a.migrationSecondaryButton)));
   }

   public void g() {
      this.startActivity(MigrationTourActivity.b.a((Context)this));
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034186);
      ((TextView)this.a(co.uk.getmondo.c.a.migrationTitle)).setText((CharSequence)this.getIntent().getStringExtra("KEY_TITLE"));
      ((TextView)this.a(co.uk.getmondo.c.a.migrationBody)).setText((CharSequence)this.getIntent().getStringExtra("KEY_BODY"));
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.c(false);
      }

      this.l().a(this);
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/migration/MigrationAnnouncementActivity$Companion;", "", "()V", "KEY_BODY", "", "KEY_SIGNUP_ALLOWED", "KEY_TITLE", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "title", "body", "signupAllowed", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, String var, String var, boolean var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "title");
         kotlin.d.b.l.b(var, "body");
         Intent var = (new Intent(var, MigrationAnnouncementActivity.class)).putExtra("KEY_TITLE", var).putExtra("KEY_BODY", var).putExtra("KEY_SIGNUP_ALLOWED", var);
         kotlin.d.b.l.a(var, "Intent(context, Migratio…P_ALLOWED, signupAllowed)");
         return var;
      }
   }
}
