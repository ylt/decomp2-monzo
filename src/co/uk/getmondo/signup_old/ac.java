package co.uk.getmondo.signup_old;

import co.uk.getmondo.api.model.signup.SignupInfo;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\bH\u0007¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/signup_old/SignupAdapter;", "", "()V", "fromJson", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "jsonReader", "Lcom/squareup/moshi/JsonReader;", "delegate", "Lcom/squareup/moshi/JsonAdapter;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ac {
   @com.squareup.moshi.g
   public final SignupInfo.Stage fromJson(com.squareup.moshi.k var, com.squareup.moshi.i var) {
      kotlin.d.b.l.b(var, "jsonReader");
      kotlin.d.b.l.b(var, "delegate");
      String var = var.j();
      SignupInfo.Stage var;
      if(kotlin.h.j.a(var, "pending", false, 2, (Object)null)) {
         var = SignupInfo.Stage.PENDING;
      } else {
         var = (SignupInfo.Stage)var.a((Object)var);
      }

      return var;
   }
}
