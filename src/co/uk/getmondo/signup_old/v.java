package co.uk.getmondo.signup_old;

public final class v implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!v.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public v(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a a(javax.a.a var) {
      return new v(var);
   }

   public void a(SignUpActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (w)this.b.b();
      }
   }
}
