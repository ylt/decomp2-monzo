package co.uk.getmondo.signup_old;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

public class CreateProfileActivity extends co.uk.getmondo.common.activities.b implements f.a {
   f a;
   private final Handler b = new Handler();
   private final Runnable c = c.a(this);
   @BindView(2131820872)
   EditText countryEditText;
   @BindView(2131820871)
   TextInputLayout countryWrapper;
   @BindView(2131820873)
   Button createAccountButton;
   @BindView(2131820867)
   TextInputLayout dateWrapper;
   @BindView(2131820865)
   TextInputLayout nameWrapper;
   @BindView(2131820869)
   TextInputLayout postcodeWrapper;

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a a(CreateProfileActivity var, Object var) throws Exception {
      return new co.uk.getmondo.signup_old.a.a.a(var.a(var.nameWrapper), var.a(var.dateWrapper), new co.uk.getmondo.d.s(var.a(var.postcodeWrapper), (String[])null, (String)null, (String)null, (String)null));
   }

   private String a(TextInputLayout var) {
      String var;
      if(var.getEditText() != null) {
         var = var.getEditText().getText().toString();
      } else {
         var = null;
      }

      return var;
   }

   @Deprecated
   public static void a(Context var) {
      Intent var = b(var);
      var.addFlags(268533760);
      var.startActivity(var);
   }

   private void a(TextInputLayout var, String var) {
      if(var.getEditText() != null) {
         var.getEditText().setText(var);
      }

   }

   public static Intent b(Context var) {
      return new Intent(var, CreateProfileActivity.class);
   }

   private void k() {
      final r var = new r();
      EditText var = this.dateWrapper.getEditText();
      if(var != null) {
         var.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable varx) {
            }

            public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
            }

            public void onTextChanged(CharSequence varx, int var, int var, int var) {
               CreateProfileActivity.this.dateWrapper.setError((CharSequence)null);
               var.a(varx, var, var, this, CreateProfileActivity.this.dateWrapper.getEditText());
            }
         });
      }

   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.createAccountButton).map(d.a(this));
   }

   public void a(String var) {
      this.a(this.nameWrapper, var);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.countryEditText);
   }

   public void c() {
      this.setResult(-1);
      this.finish();
   }

   public void d() {
      this.nameWrapper.setError(this.getString(2131362173));
   }

   public void d(String var) {
      this.a(this.dateWrapper, var);
   }

   public void e() {
      this.dateWrapper.setError(this.getString(2131362171));
   }

   public void e(String var) {
      this.a(this.postcodeWrapper, var);
   }

   public void f() {
      this.postcodeWrapper.setError(this.getString(2131362174));
   }

   public void f(String var) {
      this.countryEditText.setText(var);
      this.countryEditText.setSelection(var.length() - 1);
   }

   public void g() {
      this.dateWrapper.setError(this.getString(2131362903));
   }

   public void h() {
      this.b.postDelayed(this.c, 300L);
   }

   public void i() {
      this.b.removeCallbacks(this.c);
      this.t();
   }

   public void j() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362856), this.getString(2131362857), 2131362217, false).show(this.getSupportFragmentManager(), "TAG_UNAVAILABLE_OUTSIDE_UK");
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034158);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.a.a((f.a)this);
      this.k();
      if(co.uk.getmondo.a.c.booleanValue()) {
         this.countryEditText.setAlpha(0.5F);
      }

   }

   @OnEditorAction({2131820868})
   boolean onDateAction() {
      this.postcodeWrapper.requestFocus();
      return true;
   }

   @OnFocusChange({2131820868})
   void onDateFocusChange(boolean var) {
      String var = this.a(this.dateWrapper);
      if(var && this.dateWrapper.getEditText() != null && var != null) {
         this.dateWrapper.getEditText().setSelection(var.length());
      }

   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnTextChanged({2131820866})
   void onNameChanged() {
      this.nameWrapper.setError((CharSequence)null);
   }

   @OnTextChanged({2131820870})
   void onPostcodeChanged() {
      this.postcodeWrapper.setError((CharSequence)null);
   }
}
