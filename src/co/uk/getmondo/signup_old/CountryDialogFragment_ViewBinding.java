package co.uk.getmondo.signup_old;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CountryDialogFragment_ViewBinding implements Unbinder {
   private CountryDialogFragment a;

   public CountryDialogFragment_ViewBinding(CountryDialogFragment var, View var) {
      this.a = var;
      var.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131820862, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      CountryDialogFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.recyclerView = null;
      }
   }
}
