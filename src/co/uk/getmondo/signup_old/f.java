package co.uk.getmondo.signup_old;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import java.text.ParseException;
import java.util.Date;

public class f extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.s c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.m h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.fcm.e j;
   private final co.uk.getmondo.api.b.a k;
   private co.uk.getmondo.d.i l;

   f(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.s var, co.uk.getmondo.common.m var, co.uk.getmondo.common.a var, co.uk.getmondo.fcm.e var, co.uk.getmondo.api.b.a var) {
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.c = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a a(f var, co.uk.getmondo.signup_old.a.a.a var) throws Exception {
      return var.a(var.a(var));
   }

   // $FF: synthetic method
   static io.reactivex.l a(f var, f.a var, co.uk.getmondo.signup_old.a.a.a var) throws Exception {
      return var.k.a(var).a((Object)co.uk.getmondo.common.b.a.a).e().b(var.e).a(var.d).b(o.a(var)).a(p.a(var, var)).a(h.a(var)).a((io.reactivex.l)io.reactivex.h.a());
   }

   // $FF: synthetic method
   static void a(f.a var, co.uk.getmondo.common.b.a var, Throwable var) throws Exception {
      var.i();
   }

   // $FF: synthetic method
   static void a(f.a var, io.reactivex.b.b var) throws Exception {
      var.h();
   }

   // $FF: synthetic method
   static void a(f.a var, Object var) throws Exception {
      var.j();
   }

   // $FF: synthetic method
   static void a(f var, f.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.h.a();
      var.j.a();
      var.c.a(true);
      var.c();
   }

   // $FF: synthetic method
   static void a(f var, f.a var, Throwable var) throws Exception {
      d.a.a.a(var);
      if(!var.f.a(var, var)) {
         var.b(2131362868);
      }

   }

   private boolean a(co.uk.getmondo.signup_old.a.a.a var) {
      boolean var;
      if(!this.c(var.a())) {
         ((f.a)this.a).d();
         var = false;
      } else if(!this.b(var.b())) {
         ((f.a)this.a).e();
         var = false;
      } else {
         Date var = new Date();

         Date var;
         try {
            var = co.uk.getmondo.create_account.b.a(var.b());
         } catch (ParseException var) {
            ((f.a)this.a).e();
            var = false;
            return var;
         }

         if(!co.uk.getmondo.create_account.b.a(var, var)) {
            ((f.a)this.a).g();
            var = false;
         } else if(this.l.a() && !this.a(var.c().c())) {
            ((f.a)this.a).f();
            var = false;
         } else {
            var = true;
         }
      }

      return var;
   }

   private boolean a(String var) {
      return co.uk.getmondo.common.k.p.c(var);
   }

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a b(f var, co.uk.getmondo.signup_old.a.a.a var) throws Exception {
      String var;
      if(var.l.a()) {
         var = var.c().c();
      } else {
         var = null;
      }

      return var.a(new co.uk.getmondo.d.s(var, (String[])null, (String)null, var.l.e(), (String)null));
   }

   private boolean b(String var) {
      boolean var;
      if(var == null) {
         var = false;
      } else {
         var = var.replace(" ", "");
         var = (new s()).a(var);
      }

      return var;
   }

   private boolean c(String var) {
      return co.uk.getmondo.common.k.p.c(var);
   }

   public void a(f.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.f());
      this.l = co.uk.getmondo.d.i.i();
      var.f(this.l.b());
      ak var = this.g.b();
      co.uk.getmondo.d.ac var = null;
      if(var != null) {
         var = var.d();
      }

      if(var != null) {
         var.a(var.c());
         var.d(co.uk.getmondo.create_account.b.b(var.e()));
         var.e(var.h().c());
      }

      this.a((io.reactivex.b.b)var.a().map(g.a(this)).map(i.a(this)).filter(j.a()).flatMapMaybe(k.a(this, var)).subscribe(l.a(this, var), m.a()));
      this.a((io.reactivex.b.b)var.b().subscribe(n.a(var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var);

      io.reactivex.n b();

      void c();

      void d();

      void d(String var);

      void e();

      void e(String var);

      void f();

      void f(String var);

      void g();

      void h();

      void i();

      void j();
   }
}
