package co.uk.getmondo.signup_old;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CountryDialogFragment extends DialogFragment {
   private Unbinder a;
   private CountryDialogFragment.a b;
   private CountryDialogFragment.b c;
   @BindView(2131820862)
   RecyclerView recyclerView;

   // $FF: synthetic method
   static kotlin.n a(CountryDialogFragment var, co.uk.getmondo.d.i var) {
      var.b.a(var);
      var.getDialog().dismiss();
      return kotlin.n.a;
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setStyle(0, 2131493134);
   }

   @SuppressLint({"InflateParams"})
   public Dialog onCreateDialog(Bundle var) {
      ViewGroup var = (ViewGroup)LayoutInflater.from(this.getActivity()).inflate(2131034260, (ViewGroup)null, false);
      this.a = ButterKnife.bind(this, (View)var);
      a var = new a(co.uk.getmondo.d.i.j(), b.a(this));
      this.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
      this.recyclerView.setAdapter(var);
      this.recyclerView.setHasFixedSize(true);
      return (new Builder(this.getActivity())).setView(var).create();
   }

   public void onDestroyView() {
      this.a.unbind();
      super.onDestroyView();
   }

   public void onDismiss(DialogInterface var) {
      if(this.c != null) {
         this.c.a();
      }

      super.onDismiss(var);
   }

   @FunctionalInterface
   interface a {
      void a(co.uk.getmondo.d.i var);
   }

   @FunctionalInterface
   interface b {
      void a();
   }
}
