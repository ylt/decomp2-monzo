package co.uk.getmondo.signup_old;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import co.uk.getmondo.signup.EmailActivity;

public class SignUpActivity extends co.uk.getmondo.common.activities.b implements w.a {
   w a;
   @BindView(2131821086)
   Button continueButton;
   @BindView(2131821085)
   EditText emailEditText;
   @BindView(2131821084)
   TextInputLayout emailWrapper;

   public static void a(Activity var, int var) {
      Intent var = new Intent(var, SignUpActivity.class);
      var.putExtra("page_extra", var);
      var.startActivity(var);
   }

   public io.reactivex.n a() {
      return com.b.a.d.e.a(this.emailEditText).map(u.a());
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.continueButton);
   }

   public String c() {
      return this.emailEditText.getText().toString();
   }

   public void d() {
      EmailActivity.a((Activity)this);
      this.t();
   }

   public void e() {
      this.emailWrapper.setError((CharSequence)null);
   }

   public void f() {
      this.emailWrapper.setError(this.getString(2131362172));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034207);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.a.a((w.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnTextChanged({2131821085})
   void onTextChanged(CharSequence var) {
      if(var.length() > 0) {
         this.emailWrapper.setError((CharSequence)null);
      }

   }
}
