package co.uk.getmondo.signup_old;

import co.uk.getmondo.api.model.tracking.Impression;
import java.util.regex.Pattern;

public class w extends co.uk.getmondo.common.ui.b {
   private static final Pattern c = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;

   w(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.a var) {
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static io.reactivex.l a(w var, w.a var, Object var) throws Exception {
      io.reactivex.h var;
      if(!var.a(var.c())) {
         var.f();
         var = io.reactivex.h.a();
      } else {
         var.e();
         var = var.g.a(var.c()).a((Object)co.uk.getmondo.common.b.a.a).e().b(var.e).a(var.d).a(aa.a(var, var)).a((io.reactivex.l)io.reactivex.h.a());
      }

      return var;
   }

   // $FF: synthetic method
   static void a(w.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.d();
   }

   // $FF: synthetic method
   static void a(w var, w.a var, Throwable var) throws Exception {
      if(!var.f.a(var, var)) {
         var.b(2131362709);
      }

   }

   private boolean a(String var) {
      boolean var;
      if(var != null && var.length() != 0) {
         var = c.matcher(var).matches();
      } else {
         var = false;
      }

      return var;
   }

   private int b(w.a var) {
      int var;
      try {
         var = ((SignUpActivity)var).getIntent().getIntExtra("page_extra", 1);
      } catch (Exception var) {
         var = 0;
      }

      return var;
   }

   public void a(w.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.a(this.b(var)));
      this.a((io.reactivex.b.b)io.reactivex.n.merge(var.b(), var.a()).flatMapMaybe(x.a(this, var)).observeOn(this.d).subscribe(y.a(var), z.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      io.reactivex.n b();

      String c();

      void d();

      void e();

      void f();
   }
}
