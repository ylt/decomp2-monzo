package co.uk.getmondo.signup_old.a.a;

import co.uk.getmondo.d.s;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B)\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J1\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0015\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/signup_old/data/model/ProfileData;", "", "name", "", "dateOfBirth", "address", "Lco/uk/getmondo/model/LegacyAddress;", "isValid", "", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;Z)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getDateOfBirth", "()Ljava/lang/String;", "()Z", "getName", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "withAddress", "withValidity", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final String a;
   private final String b;
   private final s c;
   private final boolean d;

   public a(String var, String var, s var) {
      this(var, var, var, false, 8, (i)null);
   }

   public a(String var, String var, s var, boolean var) {
      l.b(var, "name");
      l.b(var, "dateOfBirth");
      l.b(var, "address");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   public a(String var, String var, s var, boolean var, int var, i var) {
      if((var & 8) != 0) {
         var = false;
      }

      this(var, var, var, var);
   }

   public final a a(s var) {
      l.b(var, "address");
      return a(this, (String)null, (String)null, var, false, 11, (Object)null);
   }

   public final a a(String var, String var, s var, boolean var) {
      l.b(var, "name");
      l.b(var, "dateOfBirth");
      l.b(var, "address");
      return new a(var, var, var, var);
   }

   public final a a(boolean var) {
      return a(this, (String)null, (String)null, (s)null, var, 7, (Object)null);
   }

   public final String a() {
      return this.a;
   }

   public final String b() {
      return this.b;
   }

   public final s c() {
      return this.c;
   }

   public final boolean d() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof a)) {
            return var;
         }

         a var = (a)var;
         var = var;
         if(!l.a(this.a, var.a)) {
            return var;
         }

         var = var;
         if(!l.a(this.b, var.b)) {
            return var;
         }

         var = var;
         if(!l.a(this.c, var.c)) {
            return var;
         }

         boolean var;
         if(this.d == var.d) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "ProfileData(name=" + this.a + ", dateOfBirth=" + this.b + ", address=" + this.c + ", isValid=" + this.d + ")";
   }
}
