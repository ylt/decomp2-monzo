package co.uk.getmondo.signup_old;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CreateProfileActivity_ViewBinding implements Unbinder {
   private CreateProfileActivity a;
   private View b;
   private View c;
   private TextWatcher d;
   private View e;
   private TextWatcher f;

   public CreateProfileActivity_ViewBinding(final CreateProfileActivity var, View var) {
      this.a = var;
      var.nameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820865, "field 'nameWrapper'", TextInputLayout.class);
      var.dateWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820867, "field 'dateWrapper'", TextInputLayout.class);
      var.postcodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820869, "field 'postcodeWrapper'", TextInputLayout.class);
      var.countryWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820871, "field 'countryWrapper'", TextInputLayout.class);
      var.countryEditText = (EditText)Utils.findRequiredViewAsType(var, 2131820872, "field 'countryEditText'", EditText.class);
      var.createAccountButton = (Button)Utils.findRequiredViewAsType(var, 2131820873, "field 'createAccountButton'", Button.class);
      View var = Utils.findRequiredView(var, 2131820868, "method 'onDateAction' and method 'onDateFocusChange'");
      this.b = var;
      ((TextView)var).setOnEditorActionListener(new OnEditorActionListener() {
         public boolean onEditorAction(TextView varx, int var, KeyEvent var) {
            return var.onDateAction();
         }
      });
      var.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View varx, boolean var) {
            var.onDateFocusChange(var);
         }
      });
      var = Utils.findRequiredView(var, 2131820866, "method 'onNameChanged'");
      this.c = var;
      this.d = new TextWatcher() {
         public void afterTextChanged(Editable varx) {
         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            var.onNameChanged();
         }
      };
      ((TextView)var).addTextChangedListener(this.d);
      var = Utils.findRequiredView(var, 2131820870, "method 'onPostcodeChanged'");
      this.e = var;
      this.f = new TextWatcher() {
         public void afterTextChanged(Editable varx) {
         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            var.onPostcodeChanged();
         }
      };
      ((TextView)var).addTextChangedListener(this.f);
   }

   public void unbind() {
      CreateProfileActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.nameWrapper = null;
         var.dateWrapper = null;
         var.postcodeWrapper = null;
         var.countryWrapper = null;
         var.countryEditText = null;
         var.createAccountButton = null;
         ((TextView)this.b).setOnEditorActionListener((OnEditorActionListener)null);
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         ((TextView)this.c).removeTextChangedListener(this.d);
         this.d = null;
         this.c = null;
         ((TextView)this.e).removeTextChangedListener(this.f);
         this.f = null;
         this.e = null;
      }
   }
}
