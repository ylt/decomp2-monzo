package co.uk.getmondo.signup_old;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SignUpActivity_ViewBinding implements Unbinder {
   private SignUpActivity a;
   private View b;
   private TextWatcher c;

   public SignUpActivity_ViewBinding(final SignUpActivity var, View var) {
      this.a = var;
      var.emailWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821084, "field 'emailWrapper'", TextInputLayout.class);
      View var = Utils.findRequiredView(var, 2131821085, "field 'emailEditText' and method 'onTextChanged'");
      var.emailEditText = (EditText)Utils.castView(var, 2131821085, "field 'emailEditText'", EditText.class);
      this.b = var;
      this.c = new TextWatcher() {
         public void afterTextChanged(Editable varx) {
         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            var.onTextChanged(varx);
         }
      };
      ((TextView)var).addTextChangedListener(this.c);
      var.continueButton = (Button)Utils.findRequiredViewAsType(var, 2131821086, "field 'continueButton'", Button.class);
   }

   public void unbind() {
      SignUpActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.emailWrapper = null;
         var.emailEditText = null;
         var.continueButton = null;
         ((TextView)this.b).removeTextChangedListener(this.c);
         this.c = null;
         this.b = null;
      }
   }
}
