package co.uk.getmondo.transaction.attachment;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aj;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

class m extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.transaction.a.a f;
   private final co.uk.getmondo.common.k.l g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.e.a i;
   private String j;
   private String k;
   private File l;

   m(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.transaction.a.a var, co.uk.getmondo.common.k.l var, co.uk.getmondo.common.a var, co.uk.getmondo.common.e.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(m var, InputStream var, long var, String var) throws Exception {
      return var.f.a(var, var, var, var.j);
   }

   private void a(aj var) {
      String var;
      if(var.e() == null) {
         var = "";
      } else {
         var = var.e();
      }

      this.k = var;
      if(var.f() != null) {
         ((m.a)this.a).a(var.f().i());
      }

      if(var.e() != null) {
         ((m.a)this.a).d(var.e());
      }

      ((m.a)this.a).a((List)var.A());
   }

   // $FF: synthetic method
   static void a(m var) throws Exception {
      ((m.a)var.a).d();
      ((m.a)var.a).e();
   }

   // $FF: synthetic method
   static void a(m var, aj var) {
      var.a(var);
   }

   // $FF: synthetic method
   static void a(m var, Throwable var) throws Exception {
      ((m.a)var.a).d();
      var.i.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   private void a(InputStream var, long var) {
      ((m.a)this.a).b();
      this.a((io.reactivex.b.b)this.e.d().c(o.a(this, var, var)).b(this.d).a(this.c).a(p.a(this), q.a(this)));
   }

   // $FF: synthetic method
   static void b(m var) throws Exception {
      ((m.a)var.a).d();
   }

   // $FF: synthetic method
   static void b(m var, Throwable var) throws Exception {
      ((m.a)var.a).d();
      if(!var.i.a(var, (co.uk.getmondo.common.e.a.a)var.a)) {
         ((m.a)var.a).b(2131362277);
      }

   }

   void a() {
      this.h.a(Impression.o());
      this.l = this.g.a();
      if(this.l != null) {
         ((m.a)this.a).a(this.l);
      }

   }

   public void a(String var) {
      this.j = var;
      this.a((io.reactivex.b.b)this.f.b(var).observeOn(this.c).subscribe(n.a(this)));
   }

   void b(String var) {
      this.a(this.g.a(var), this.g.b(var));
   }

   void c() {
      if(this.l != null) {
         try {
            FileInputStream var = new FileInputStream(this.l);
            this.a(var, this.l.length());
         } catch (FileNotFoundException var) {
            d.a.a.a((Throwable)var);
         }
      }

   }

   void c(String var) {
      if(var.equals(this.k)) {
         ((m.a)this.a).e();
      } else {
         ((m.a)this.a).c();
         this.a((io.reactivex.b.b)this.f.a(var, this.j).b(this.d).a(this.c).a(r.a(this), s.a(this)));
      }

   }

   void d() {
      this.h.a(Impression.p());
      ((m.a)this.a).a();
   }

   void e() {
      this.h.a(Impression.n());
   }

   void f() {
      if(this.l != null) {
         ((m.a)this.a).a(this.l);
      }

   }

   void g() {
      ((m.a)this.a).b(2131362019);
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(File var);

      void a(String var);

      void a(List var);

      void b();

      void c();

      void d();

      void d(String var);

      void e();
   }
}
