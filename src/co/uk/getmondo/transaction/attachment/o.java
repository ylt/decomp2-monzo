package co.uk.getmondo.transaction.attachment;

import java.io.InputStream;

// $FF: synthetic class
final class o implements io.reactivex.c.h {
   private final m a;
   private final InputStream b;
   private final long c;

   private o(m var, InputStream var, long var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(m var, InputStream var, long var) {
      return new o(var, var, var);
   }

   public Object a(Object var) {
      return m.a(this.a, this.b, this.c, (String)var);
   }
}
