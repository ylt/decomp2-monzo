package co.uk.getmondo.transaction.attachment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.List;

class u extends android.support.v4.view.p {
   private final List a = new ArrayList();
   private OnClickListener b;

   // $FF: synthetic method
   static void a(u var, View var) {
      var.b.onClick(var);
   }

   // $FF: synthetic method
   static void a(u var, String var, int var, View var) {
      AttachmentDetailsActivity.a(var.getContext(), var, ((co.uk.getmondo.d.d)var.a.get(var)).a());
   }

   public int a(Object var) {
      return -2;
   }

   public Object a(ViewGroup var, int var) {
      ViewGroup var = (ViewGroup)LayoutInflater.from(var.getContext()).inflate(2131034232, var, false);
      var.addView(var);
      ImageView var = (ImageView)var.findViewById(2131821192);
      ImageView var = (ImageView)var.findViewById(2131821191);
      Button var = (Button)var.findViewById(2131821190);
      final ProgressBar var = (ProgressBar)var.findViewById(2131821193);
      if(var == this.a.size()) {
         var.setVisibility(8);
         var.setVisibility(0);
         var.setVisibility(0);
         var.setOnClickListener(v.a(this));
         var.setOnClickListener((OnClickListener)null);
         var.setVisibility(8);
         var.setBackgroundResource(0);
         var.setClipToOutline(false);
      } else if(var < this.a.size()) {
         var.setVisibility(0);
         var.setVisibility(8);
         var.setVisibility(8);
         var.setVisibility(0);
         String var = ((co.uk.getmondo.d.d)this.a.get(var)).b();
         var.setBackgroundResource(2130838008);
         var.setClipToOutline(true);
         com.bumptech.glide.g.b(var.getContext()).a(var).a(new com.bumptech.glide.g.d() {
            public boolean a(com.bumptech.glide.load.resource.a.b var, String var, com.bumptech.glide.g.b.j var, boolean varx, boolean var) {
               var.setVisibility(8);
               return false;
            }

            public boolean a(Exception var, String var, com.bumptech.glide.g.b.j var, boolean varx) {
               var.setVisibility(8);
               return false;
            }
         }).a(var);
         var.setOnClickListener(w.a(this, var, var));
      }

      return var;
   }

   void a(OnClickListener var) {
      this.b = var;
   }

   public void a(ViewGroup var, int var, Object var) {
      var.removeView((View)var);
   }

   void a(List var) {
      this.a.clear();
      this.a.addAll(var);
      this.c();
   }

   public boolean a(View var, Object var) {
      boolean var;
      if(var == var) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public int b() {
      return this.a.size() + 1;
   }

   public CharSequence c(int var) {
      return "";
   }
}
