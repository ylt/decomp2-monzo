package co.uk.getmondo.transaction.attachment;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import me.relex.circleindicator.CircleIndicator;

public class AttachmentActivity_ViewBinding implements Unbinder {
   private AttachmentActivity a;
   private View b;
   private View c;

   public AttachmentActivity_ViewBinding(final AttachmentActivity var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131820802, "field 'notesInput' and method 'onNotesFocusChanged'");
      var.notesInput = (EditText)Utils.castView(var, 2131820802, "field 'notesInput'", EditText.class);
      this.b = var;
      var.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View varx, boolean var) {
            var.onNotesFocusChanged(var);
         }
      });
      var.imagePager = (ViewPager)Utils.findRequiredViewAsType(var, 2131820803, "field 'imagePager'", ViewPager.class);
      var.viewpagerIndicator = (CircleIndicator)Utils.findRequiredViewAsType(var, 2131820804, "field 'viewpagerIndicator'", CircleIndicator.class);
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
      var = Utils.findRequiredView(var, 2131820805, "method 'onSelectFromDeviceClick'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onSelectFromDeviceClick();
         }
      });
   }

   public void unbind() {
      AttachmentActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.notesInput = null;
         var.imagePager = null;
         var.viewpagerIndicator = null;
         var.toolbar = null;
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
