package co.uk.getmondo.transaction.attachment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import java.io.File;
import java.util.List;
import me.relex.circleindicator.CircleIndicator;

public class AttachmentActivity extends co.uk.getmondo.common.activities.b implements OnClickListener, m.a {
   m a;
   private u b;
   private final Handler c = new Handler();
   private final Runnable e = a.a(this);
   @BindView(2131820803)
   ViewPager imagePager;
   @BindView(2131820802)
   EditText notesInput;
   @BindView(2131820798)
   Toolbar toolbar;
   @BindView(2131820804)
   CircleIndicator viewpagerIndicator;

   public static void a(Context var, String var) {
      Intent var = new Intent(var, AttachmentActivity.class);
      var.putExtra("EXTRA_TRANSACTION_ID", var);
      var.startActivity(var);
   }

   private void b(File var) {
      Intent var = co.uk.getmondo.common.k.a((Context)this, (File)var);
      if(var != null) {
         this.startActivityForResult(var, 1);
      }

   }

   private boolean f() {
      boolean var;
      if(android.support.v4.content.a.b(this, "android.permission.CAMERA") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private void g() {
      this.startActivityForResult(co.uk.getmondo.common.k.a(), 2);
   }

   public void a() {
      this.g();
   }

   public void a(File var) {
      if(this.f()) {
         this.b(var);
      } else {
         android.support.v4.app.a.a(this, new String[]{"android.permission.CAMERA"}, 3);
      }

   }

   public void a(String var) {
      this.toolbar.setTitle(var);
   }

   public void a(List var) {
      this.b.a(var);
   }

   public void b() {
      this.c(this.getString(2131362020));
   }

   public void c() {
      this.c.postDelayed(this.e, 300L);
   }

   public void d() {
      this.c.removeCallbacks(this.e);
      this.t();
   }

   public void d(String var) {
      this.notesInput.setText(var);
   }

   public void e() {
      this.finish();
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1 && var == -1) {
         this.a.c();
      }

      if(var == 2 && var == -1 && var != null && var.getData() != null) {
         String var = var.getData().toString();
         this.a.b(var);
      }

   }

   public void onBackPressed() {
      this.a.c(this.notesInput.getText().toString());
   }

   public void onClick(View var) {
      this.a.a();
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034141);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.setSupportActionBar(this.toolbar);
      this.b = new u();
      this.b.a((OnClickListener)this);
      this.imagePager.setAdapter(this.b);
      this.viewpagerIndicator.setViewPager(this.imagePager);
      this.a.a((co.uk.getmondo.common.ui.f)this);
      String var = this.getIntent().getStringExtra("EXTRA_TRANSACTION_ID");
      this.a.a(var);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnFocusChange({2131820802})
   void onNotesFocusChanged(boolean var) {
      if(var) {
         this.a.e();
      }

   }

   public boolean onOptionsItemSelected(MenuItem var) {
      boolean var;
      switch(var.getItemId()) {
      case 16908332:
         this.a.c(this.notesInput.getText().toString());
         var = true;
         break;
      default:
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      if(var == 3) {
         if(var.length == 1 && var[0] == 0) {
            this.a.f();
         } else {
            this.a.g();
         }
      } else {
         super.onRequestPermissionsResult(var, var, var);
      }

   }

   @OnClick({2131820805})
   void onSelectFromDeviceClick() {
      this.a.d();
   }
}
