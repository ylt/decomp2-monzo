package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class v implements OnClickListener {
   private final u a;

   private v(u var) {
      this.a = var;
   }

   public static OnClickListener a(u var) {
      return new v(var);
   }

   public void onClick(View var) {
      u.a(this.a, var);
   }
}
