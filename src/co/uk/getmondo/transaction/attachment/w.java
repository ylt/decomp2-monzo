package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class w implements OnClickListener {
   private final u a;
   private final String b;
   private final int c;

   private w(u var, String var, int var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static OnClickListener a(u var, String var, int var) {
      return new w(var, var, var);
   }

   public void onClick(View var) {
      u.a(this.a, this.b, this.c, var);
   }
}
