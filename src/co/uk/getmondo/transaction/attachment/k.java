package co.uk.getmondo.transaction.attachment;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final e a;
   private final e.a b;

   private k(e var, e.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(e var, e.a var) {
      return new k(var, var);
   }

   public void a(Object var) {
      e.a(this.a, this.b, (Throwable)var);
   }
}
