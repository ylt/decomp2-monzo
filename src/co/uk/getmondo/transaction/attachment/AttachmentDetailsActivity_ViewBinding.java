package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AttachmentDetailsActivity_ViewBinding implements Unbinder {
   private AttachmentDetailsActivity a;

   public AttachmentDetailsActivity_ViewBinding(AttachmentDetailsActivity var, View var) {
      this.a = var;
      var.zoomableImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131820813, "field 'zoomableImageView'", ImageView.class);
      var.deleteButton = (Button)Utils.findRequiredViewAsType(var, 2131820815, "field 'deleteButton'", Button.class);
      var.progress = (ProgressBar)Utils.findRequiredViewAsType(var, 2131820814, "field 'progress'", ProgressBar.class);
   }

   public void unbind() {
      AttachmentDetailsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.zoomableImageView = null;
         var.deleteButton = null;
         var.progress = null;
      }
   }
}
