package co.uk.getmondo.transaction.details.views;

import android.text.TextPaint;
import android.text.style.URLSpan;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/NoUnderlineUrlSpan;", "Landroid/text/style/URLSpan;", "url", "", "(Ljava/lang/String;)V", "updateDrawState", "", "textPaint", "Landroid/text/TextPaint;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends URLSpan {
   public a(String var) {
      l.b(var, "url");
      super(var);
   }

   public void updateDrawState(TextPaint var) {
      l.b(var, "textPaint");
      super.updateDrawState(var);
      var.setUnderlineText(false);
   }
}
