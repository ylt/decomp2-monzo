package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.bumptech.glide.g;
import com.bumptech.glide.g.b.b;
import com.bumptech.glide.g.b.j;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.c;
import kotlin.d;
import kotlin.n;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fRL\u0010\u000f\u001a4\u0012\u0013\u0012\u00110\u0011¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0010X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u00020\u00078BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u000e\u001a\u0004\b\u001d\u0010\u001e¨\u0006#"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/AvatarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "getAvatarGenerator", "()Lco/uk/getmondo/common/ui/AvatarGenerator;", "avatarGenerator$delegate", "Lkotlin/Lazy;", "backgroundListener", "Lkotlin/Function2;", "Landroid/graphics/drawable/Drawable;", "Lkotlin/ParameterName;", "name", "background", "", "showOverlay", "", "getBackgroundListener", "()Lkotlin/jvm/functions/Function2;", "setBackgroundListener", "(Lkotlin/jvm/functions/Function2;)V", "p2pPlaceholderSize", "getP2pPlaceholderSize", "()I", "p2pPlaceholderSize$delegate", "bind", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AvatarView extends FrameLayout {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(AvatarView.class), "avatarGenerator", "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;")), (l)y.a(new w(y.a(AvatarView.class), "p2pPlaceholderSize", "getP2pPlaceholderSize()I"))};
   private final c b;
   private final c c;
   private m d;
   private HashMap e;

   public AvatarView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public AvatarView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public AvatarView(final Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      this.b = d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final co.uk.getmondo.common.ui.a b() {
            return co.uk.getmondo.common.ui.a.a.a(var);
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.c = d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final int b() {
            return (int)TypedValue.applyDimension(2, 26.0F, AvatarView.this.getResources().getDisplayMetrics());
         }

         // $FF: synthetic method
         public Object v_() {
            return Integer.valueOf(this.b());
         }
      }));
      LayoutInflater.from(var).inflate(2131034421, (ViewGroup)this);
   }

   // $FF: synthetic method
   public AvatarView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   private final co.uk.getmondo.common.ui.a getAvatarGenerator() {
      c var = this.b;
      l var = a[0];
      return (co.uk.getmondo.common.ui.a)var.a();
   }

   private final int getP2pPlaceholderSize() {
      c var = this.c;
      l var = a[1];
      return ((Number)var.a()).intValue();
   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final void a(co.uk.getmondo.transaction.details.b.d var) {
      kotlin.d.b.l.b(var, "header");
      co.uk.getmondo.transaction.details.b.c var = var.j();
      if(!var.d()) {
         this.a(co.uk.getmondo.c.a.transactionLogoOutlineView).setBackgroundResource(2130837688);
      } else {
         ((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)).setClipToOutline(true);
      }

      Context var = this.getContext();
      kotlin.d.b.l.a(var, "context");
      final Integer var = var.a(var);
      if(var.a() == null) {
         String var = var.c();
         if(var != null) {
            int var = this.getAvatarGenerator().b(var);
            Drawable var = co.uk.getmondo.common.ui.a.b.a(this.getAvatarGenerator().a(var), this.getP2pPlaceholderSize(), (Typeface)null, var.d(), 2, (Object)null);
            ((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)).setImageDrawable(var);
            m var = this.d;
            if(var != null) {
               n var = (n)var.a(new ColorDrawable(co.uk.getmondo.common.k.c.a(var, 0.5F)), Boolean.valueOf(false));
            }
         } else {
            if(var != null) {
               m var = this.d;
               if(var != null) {
                  n var = (n)var.a(new ColorDrawable(var.intValue()), Boolean.valueOf(false));
               }
            }

            ImageView var = (ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView);
            Integer var = var.b();
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var.setImageResource(var.intValue());
         }
      } else {
         com.bumptech.glide.a var = g.b(this.getContext()).a(Uri.parse(var.a())).h().a();
         if(var.d()) {
            var.a((j)((j)(new b((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)) {
               public void a(Bitmap var, com.bumptech.glide.g.a.c var) {
                  kotlin.d.b.l.b(var, "bitmap");
                  super.a(var, var);
                  android.support.v7.d.b.a(var).a((android.support.v7.d.b.c)(new android.support.v7.d.b.c() {
                     public final void a(android.support.v7.d.b var) {
                        int var = var.a(android.support.v4.content.a.c(AvatarView.this.getContext(), 2131689582));
                        m var;
                        n var;
                        if(var != null) {
                           var = AvatarView.this.getBackgroundListener();
                           if(var != null) {
                              var = (n)var.a(new ColorDrawable(var.intValue()), Boolean.valueOf(false));
                           }
                        } else {
                           var = AvatarView.this.getBackgroundListener();
                           if(var != null) {
                              var = (n)var.a(new ColorDrawable(co.uk.getmondo.common.k.c.a(var, 0.5F)), Boolean.valueOf(false));
                           }
                        }

                     }
                  }));
               }
            })));
         } else {
            var.a(com.bumptech.glide.load.engine.b.b).a((j)((j)(new co.uk.getmondo.common.ui.c((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)) {
               public void a(Bitmap var, com.bumptech.glide.g.a.c var) {
                  kotlin.d.b.l.b(var, "bitmap");
                  super.a(var, var);
                  m var = AvatarView.this.getBackgroundListener();
                  if(var != null) {
                     n var = (n)var.a(new BitmapDrawable(AvatarView.this.getResources(), var), Boolean.valueOf(true));
                  }

               }
            })));
         }
      }

   }

   public final m getBackgroundListener() {
      return this.d;
   }

   public final void setBackgroundListener(m var) {
      this.d = var;
   }
}
