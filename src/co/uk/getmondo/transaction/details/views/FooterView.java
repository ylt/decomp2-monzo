package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.transaction.details.b.f;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/FooterView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bind", "", "footer", "Lco/uk/getmondo/transaction/details/base/Footer;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class FooterView extends LinearLayout {
   private HashMap a;

   public FooterView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public FooterView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public FooterView(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      this.setOrientation(1);
      LayoutInflater.from(var).inflate(2131034423, (ViewGroup)this);
   }

   // $FF: synthetic method
   public FooterView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public View a(int var) {
      if(this.a == null) {
         this.a = new HashMap();
      }

      View var = (View)this.a.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.a.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final void a(f var) {
      l.b(var, "footer");
      Resources var = this.getResources();
      l.a(var, "resources");
      String var = var.a(var);
      if(var == null) {
         ae.b((TextView)this.a(c.a.transactionFooterTextView));
      } else {
         ((TextView)this.a(c.a.transactionFooterTextView)).setText((CharSequence)var);
      }

   }
}
