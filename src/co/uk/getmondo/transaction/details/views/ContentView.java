package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.transaction.details.b.e;
import co.uk.getmondo.transaction.details.b.h;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.a.x;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/ContentView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bind", "", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "bindYourHistory", "yourHistory", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ContentView extends LinearLayout {
   private HashMap a;

   public ContentView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public ContentView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public ContentView(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      this.setOrientation(1);
      LayoutInflater.from(var).inflate(2131034422, (ViewGroup)this);
   }

   // $FF: synthetic method
   public ContentView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   private final void a(n var) {
      ae.a((View)((TextView)this.a(c.a.transactionHistoryHeaderTextView)));
      TextView var = (TextView)this.a(c.a.transactionHistoryHeaderTextView);
      Resources var = this.getResources();
      l.a(var, "resources");
      var.setText((CharSequence)var.a(var));
      ((LinearLayout)this.a(c.a.transactionHistoryViewGroup)).removeAllViews();
      LayoutInflater var = LayoutInflater.from(this.getContext());
      Iterator var = m.n((Iterable)var.a()).iterator();

      while(var.hasNext()) {
         x var = (x)var.next();
         int var = var.c();
         o var = (o)var.d();
         View var = var.inflate(2131034425, (LinearLayout)this.a(c.a.transactionHistoryViewGroup), false);
         TextView var = (TextView)var.findViewById(2131821708);
         Resources var = this.getResources();
         l.a(var, "resources");
         var.setText((CharSequence)var.a(var));
         Resources var = this.getResources();
         l.a(var, "resources");
         String var = var.b(var);
         if(var == null) {
            ae.b(var.findViewById(2131821709));
         } else {
            ((TextView)var.findViewById(2131821709)).setText((CharSequence)var);
         }

         TextView var = (TextView)var.findViewById(2131821710);
         var = this.getResources();
         l.a(var, "resources");
         var.setText((CharSequence)var.c(var));
         ((LinearLayout)this.a(c.a.transactionHistoryViewGroup)).addView(var);
         Resources var = this.getResources();
         l.a(var, "resources");
         final String var = var.b(var);
         if(var != null && var == 0) {
            var.setOnClickListener((OnClickListener)(new OnClickListener() {
               public final void onClick(View var) {
                  ContentView.this.getContext().startActivity(FeedSearchActivity.a(ContentView.this.getContext(), var));
               }
            }));
         }
      }

   }

   public View a(int var) {
      if(this.a == null) {
         this.a = new HashMap();
      }

      View var = (View)this.a.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.a.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final void a(e var) {
      l.b(var, "content");
      final h var = var.a();
      if(var != null) {
         ae.a((View)((Button)this.a(c.a.transactionFurtherInformationButton)));
         Button var = (Button)this.a(c.a.transactionFurtherInformationButton);
         Resources var = this.getResources();
         l.a(var, "resources");
         var.setText((CharSequence)var.a(var));
         ((Button)this.a(c.a.transactionFurtherInformationButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               h var = var;
               Context var = ContentView.this.getContext();
               l.a(var, "context");
               var.a(var);
            }
         }));
      }

      n var = var.b();
      if(var != null) {
         boolean var;
         if(!((Collection)var.a()).isEmpty()) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            this.a(var);
         }
      }

   }
}
