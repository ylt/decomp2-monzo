package co.uk.getmondo.transaction.details.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.d;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import co.uk.getmondo.c;
import co.uk.getmondo.common.k.n;
import co.uk.getmondo.common.k.q;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u001d2\u00020\u0001:\u0002\u001d\u001eB!\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nH\u0002J \u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\nH\u0003J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0019\u001a\u00020\u0012H\u0016J\b\u0010\u001a\u001a\u00020\u0012H\u0016J\b\u0010\u001b\u001a\u00020\u0012H\u0002J\b\u0010\u001c\u001a\u00020\u0012H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00060\u000fR\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;", "Landroid/support/design/widget/CoordinatorLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "appBarLayoutRect", "Landroid/graphics/Rect;", "childRect", "desiredChildRect", "headerRect", "onPreDrawListener", "Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;", "toolbarRect", "getChildRect", "", "child", "Landroid/view/View;", "out", "getDesiredChildRect", "gravity", "offsetChildIfNeeded", "onAttachedToWindow", "onDetachedFromWindow", "updateAvatar", "updateFab", "Companion", "OnPreDrawListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TransactionCoordinatorLayout extends CoordinatorLayout {
   public static final TransactionCoordinatorLayout.a f = new TransactionCoordinatorLayout.a((i)null);
   private static final float m = -((float)n.b(72));
   private final TransactionCoordinatorLayout.b g;
   private final Rect h;
   private final Rect i;
   private final Rect j;
   private final Rect k;
   private final Rect l;
   private HashMap n;

   public TransactionCoordinatorLayout(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public TransactionCoordinatorLayout(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      l.b(var, "attrs");
      super(var, var, var);
      this.g = new TransactionCoordinatorLayout.b();
      this.h = new Rect();
      this.i = new Rect();
      this.j = new Rect();
      this.k = new Rect();
      this.l = new Rect();
   }

   // $FF: synthetic method
   public TransactionCoordinatorLayout(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   @SuppressLint({"RtlHardcoded"})
   private final void a(View var, int var, Rect var) {
      LayoutParams var = var.getLayoutParams();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.design.widget.CoordinatorLayout.LayoutParams");
      } else {
         d var = (d)var;
         int var = android.support.v4.view.d.a(var, this.getLayoutDirection());
         int var = var.getMeasuredWidth();
         int var = var.getMeasuredHeight();
         switch(var & 7) {
         case 3:
            var = this.h.left;
            break;
         case 4:
         default:
            throw (Throwable)(new AssertionError());
         case 5:
            var = this.h.right - var;
         }

         switch(var & 112) {
         case 48:
            var = this.h.top - var / 2;
            break;
         case 80:
            var = this.h.bottom - var + var / 2;
            break;
         default:
            throw (Throwable)(new AssertionError());
         }

         var = Math.max(var, this.i.bottom - var / 2);
         var = Math.max(this.getPaddingLeft() + var.leftMargin, Math.min(var, this.getWidth() - this.getPaddingRight() - var - var.rightMargin));
         var = Math.max(this.getPaddingTop() + var.topMargin, Math.min(var, this.getHeight() - this.getPaddingBottom() - var - var.bottomMargin));
         var.set(var, var, var + var, var + var);
      }
   }

   private final void d(View var, Rect var) {
      if(!var.isLayoutRequested() && var.getVisibility() != 8) {
         var.set(var.getLeft(), var.getTop(), var.getRight(), var.getBottom());
      } else {
         var.set(0, 0, 0, 0);
      }

   }

   private final void e(View var) {
      int var = this.l.left - this.k.left;
      int var = this.l.top - this.k.top;
      if(var != 0) {
         var.offsetLeftAndRight(var);
      }

      if(var != 0) {
         var.offsetTopAndBottom(var);
      }

   }

   private final void f() {
      VisibilityAwareFloatingActionButton var = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var, "transactionActionFab");
      this.d((View)var, this.k);
      var = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var, "transactionActionFab");
      this.a((View)var, 8388693, this.l);
      var = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var, "transactionActionFab");
      this.e((View)var);
      if(this.l.top <= this.i.bottom) {
         ((VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab)).b();
      } else {
         ((VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab)).a();
      }

   }

   private final void g() {
      AvatarView var = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var, "transactionAvatarView");
      this.d((View)var, this.k);
      var = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var, "transactionAvatarView");
      this.a((View)var, 8388659, this.l);
      var = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var, "transactionAvatarView");
      this.e((View)var);
      float var = Math.min(-((float)((TransactionAppBarLayout)this.b(c.a.transactionAppBarLayout)).getTotalScrollRange()) - f.a(), 0.0F);
      float var = -((float)((TransactionAppBarLayout)this.b(c.a.transactionAppBarLayout)).getTotalScrollRange());
      var = Math.min(1.0F, ((float)this.j.top - var) / (var - var));
      ((AvatarView)this.b(c.a.transactionAvatarView)).setAlpha(var);
   }

   public View b(int var) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var = (View)this.n.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.n.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.getViewTreeObserver().addOnPreDrawListener((OnPreDrawListener)this.g);
   }

   public void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      this.getViewTreeObserver().removeOnPreDrawListener((OnPreDrawListener)this.g);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$Companion;", "", "()V", "MIN_FADE_OFFSET", "", "getMIN_FADE_OFFSET", "()F", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      private final float a() {
         return TransactionCoordinatorLayout.m;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;", "Landroid/view/ViewTreeObserver$OnPreDrawListener;", "(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V", "onPreDraw", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   private final class b implements OnPreDrawListener {
      public boolean onPreDraw() {
         TransactionCoordinatorLayout var = TransactionCoordinatorLayout.this;
         HeaderView var = (HeaderView)TransactionCoordinatorLayout.this.b(c.a.transactionHeaderView);
         l.a(var, "transactionHeaderView");
         q.a(var, (View)var, (Rect)TransactionCoordinatorLayout.this.h);
         var = TransactionCoordinatorLayout.this;
         Toolbar var = (Toolbar)TransactionCoordinatorLayout.this.b(c.a.toolbar);
         l.a(var, "toolbar");
         q.a(var, (View)var, (Rect)TransactionCoordinatorLayout.this.i);
         var = TransactionCoordinatorLayout.this;
         TransactionAppBarLayout var = (TransactionAppBarLayout)TransactionCoordinatorLayout.this.b(c.a.transactionAppBarLayout);
         l.a(var, "transactionAppBarLayout");
         q.a(var, (View)var, (Rect)TransactionCoordinatorLayout.this.j);
         TransactionCoordinatorLayout.this.f();
         TransactionCoordinatorLayout.this.g();
         return true;
      }
   }
}
