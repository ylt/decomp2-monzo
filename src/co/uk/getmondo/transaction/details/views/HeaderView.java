package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.text.Spannable;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.k;
import co.uk.getmondo.d.c;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.j;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nH\u0002J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nJ\u001a\u0010\u0016\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0006\u0010\u001b\u001a\u00020\u0007J \u0010\u001c\u001a\u00020\u0013*\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\b\u0001\u0010 \u001a\u00020\u0007H\u0002J\u001e\u0010!\u001a\u00020\u0013*\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u00182\u0006\u0010$\u001a\u00020\u001aH\u0002J\u0016\u0010%\u001a\u00020\u0013*\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0018H\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R(\u0010\f\u001a\u00020\u0007*\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00078B@BX\u0082\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006&"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/HeaderView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "value", "topMargin", "Landroid/view/View;", "getTopMargin", "(Landroid/view/View;)I", "setTopMargin", "(Landroid/view/View;I)V", "adjustAmountInfoMargin", "", "adjustSubtitleMargin", "bind", "setSubtitleText", "subtitleText", "", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/TextAppearance;", "titleScrollDistance", "setAmountAndStyleOrHide", "Lco/uk/getmondo/common/ui/AmountView;", "amount", "Lco/uk/getmondo/model/Amount;", "style", "setTextAndAppearanceOrHide", "Landroid/widget/TextView;", "text", "textAppearance", "setTextOrHide", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HeaderView extends ConstraintLayout {
   private d c;
   private HashMap d;

   public HeaderView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public HeaderView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public HeaderView(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      LayoutInflater.from(var).inflate(2131034424, (ViewGroup)this);
   }

   // $FF: synthetic method
   public HeaderView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   private final void a(View var, int var) {
      LayoutParams var = var.getLayoutParams();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
      } else {
         MarginLayoutParams var = (MarginLayoutParams)var;
         var.topMargin = var;
         var.setLayoutParams((LayoutParams)var);
      }
   }

   private final void a(TextView var, String var) {
      if(var == null) {
         ae.b((View)var);
      } else {
         var.setText((CharSequence)var);
      }

   }

   private final void a(TextView var, String var, j var) {
      this.a(var, var);
      var.setTextColor(var.b());
      var.setTextSize(2, var.a());
      var.setTypeface(var.getTypeface(), var.c());
   }

   private final void a(AmountView var, c var, int var) {
      if(var == null) {
         ae.b((View)var);
      } else {
         var.a(var, var);
      }

   }

   private final void a(String var, j var) {
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), var, var);
      if(Linkify.addLinks((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), 1)) {
         CharSequence var = ((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView)).getText();
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.text.Spannable");
         }

         Spannable var = p.a((Spannable)var);
         if(var == null) {
            l.a();
         }

         int var = var.length();
         Typeface var = Typeface.create(var.d(), var.c());
         l.a(var, "typeface");
         var.setSpan(new k(var), 0, var, 33);
         ((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView)).setText((CharSequence)var);
      }

   }

   private final void b(d var) {
      if(var instanceof co.uk.getmondo.transaction.details.d.f.a) {
         this.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), 0);
      }

   }

   private final void d() {
      if(((AmountView)this.b(co.uk.getmondo.c.a.transactionAmountView)).getVisibility() == 8) {
         this.a((TextView)this.b(co.uk.getmondo.c.a.transactionLocalAmountTextView), this.getResources().getDimensionPixelSize(2131427603));
      }

   }

   public final void a(d var) {
      l.b(var, "header");
      this.c = var;
      Resources var = this.getResources();
      l.a(var, "resources");
      String var = var.c(var);
      Context var = this.getContext();
      l.a(var, "context");
      j var = var.a(var);
      Resources var = this.getResources();
      l.a(var, "resources");
      String var = var.a(var);
      Context var = this.getContext();
      l.a(var, "context");
      j var = var.b(var);
      String var;
      j var;
      if(var.g()) {
         var = var;
         var = var;
         var = var;
      } else {
         var = var;
         var = var;
         var = var;
      }

      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionTitleTextView), var, var);
      this.a(var, var);
      ((TextView)this.b(co.uk.getmondo.c.a.transactionDateTextView)).setText((CharSequence)var.a());
      this.a((AmountView)this.b(co.uk.getmondo.c.a.transactionAmountView), var.b(), var.f());
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionLocalAmountTextView), var.e());
      if(var.c()) {
         ae.a((View)((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedTextView)));
      } else {
         ae.b((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedTextView));
      }

      TextView var = (TextView)this.b(co.uk.getmondo.c.a.transactionExtraInformationTextView);
      Resources var = this.getResources();
      l.a(var, "resources");
      this.a(var, var.b(var));
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedExtraInformationTextView), var.d());
      this.b(var);
      this.d();
   }

   public View b(int var) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var = (View)this.d.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.d.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final int c() {
      d var = this.c;
      int var;
      if(var != null) {
         boolean var = var.g();
         b var = (b)null.a;
         if(var) {
            var = ((Number)var.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView))).intValue();
         } else {
            var = ((Number)var.a((TextView)this.b(co.uk.getmondo.c.a.transactionTitleTextView))).intValue();
         }
      } else {
         var = 0;
      }

      return var;
   }
}
