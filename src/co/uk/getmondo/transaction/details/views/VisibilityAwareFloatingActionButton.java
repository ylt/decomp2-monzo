package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0007H\u0016J\b\u0010\r\u001a\u00020\u000bH\u0016R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/VisibilityAwareFloatingActionButton;", "Landroid/support/design/widget/FloatingActionButton;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "userSetVisibility", "setVisibility", "", "visibility", "show", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class VisibilityAwareFloatingActionButton extends FloatingActionButton {
   private int d;

   public VisibilityAwareFloatingActionButton(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public VisibilityAwareFloatingActionButton(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public VisibilityAwareFloatingActionButton(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      this.d = this.getVisibility();
   }

   // $FF: synthetic method
   public VisibilityAwareFloatingActionButton(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public void a() {
      if(this.d == 0) {
         super.a();
      }

   }

   public void setVisibility(int var) {
      super.setVisibility(var);
      this.d = var;
   }
}
