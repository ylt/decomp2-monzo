package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.e;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import co.uk.getmondo.common.ui.ActionView;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0014\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionActionsView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bind", "", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TransactionActionsView extends LinearLayout {
   public TransactionActionsView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (i)null);
   }

   public TransactionActionsView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (i)null);
   }

   public TransactionActionsView(Context var, AttributeSet var, int var) {
      l.b(var, "context");
      super(var, var, var);
      this.setOrientation(1);
   }

   // $FF: synthetic method
   public TransactionActionsView(Context var, AttributeSet var, int var, int var, i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public final void a(List var) {
      l.b(var, "actions");
      this.removeAllViews();
      Context var = this.getContext();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
      } else {
         final e var = (e)var;
         Iterator var = var.iterator();

         while(var.hasNext()) {
            final co.uk.getmondo.transaction.details.b.a var = (co.uk.getmondo.transaction.details.b.a)var.next();
            Context var = this.getContext();
            l.a(var, "context");
            ActionView var = new ActionView(var, (AttributeSet)null, 0, 6, (i)null);
            Resources var = this.getResources();
            l.a(var, "resources");
            var.setActionTitle(var.a(var));
            var = this.getResources();
            l.a(var, "resources");
            var.setActionSubtitle(var.b(var));
            var.setPlaceholderIcon(var.a());
            var.setIconUrl(var.b());
            var.setOnClickListener((OnClickListener)(new OnClickListener() {
               public final void onClick(View var) {
                  var.a(var);
               }
            }));
            this.addView((View)var);
         }

      }
   }
}
