package co.uk.getmondo.transaction.details.b;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/Content;", "", "furtherInformationWebLink", "Lco/uk/getmondo/transaction/details/base/InformationWebLink;", "getFurtherInformationWebLink", "()Lco/uk/getmondo/transaction/details/base/InformationWebLink;", "yourHistory", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "getYourHistory", "()Lco/uk/getmondo/transaction/details/base/YourHistory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface e {
   h a();

   n b();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      public static h a(e var) {
         return null;
      }
   }
}
