package co.uk.getmondo.transaction.details.b;

import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00020\u0010X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "getActions", "()Ljava/util/List;", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "getContent", "()Lco/uk/getmondo/transaction/details/base/Content;", "footer", "Lco/uk/getmondo/transaction/details/base/Footer;", "getFooter", "()Lco/uk/getmondo/transaction/details/base/Footer;", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "getHeader", "()Lco/uk/getmondo/transaction/details/base/BaseHeader;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface l {
   d a();

   List b();

   e c();

   f d();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      public static f a(l var) {
         return null;
      }
   }
}
