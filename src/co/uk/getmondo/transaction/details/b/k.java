package co.uk.getmondo.transaction.details.b;

import android.content.Context;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/TitleTextAppearance;", "Lco/uk/getmondo/transaction/details/base/TextAppearance;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends j {
   public k(Context var) {
      kotlin.d.b.l.b(var, "context");
      super(22.0F, android.support.v4.content.a.c(var, 2131689558), 0, (String)null, 8, (kotlin.d.b.i)null);
   }
}
