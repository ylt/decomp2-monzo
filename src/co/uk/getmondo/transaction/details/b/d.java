package co.uk.getmondo.transaction.details.b;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\b&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\n\u0010\u0014\u001a\u0004\u0018\u00010\fH\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\u00128VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/BaseHeader;", "Lco/uk/getmondo/transaction/details/base/Header;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "amount", "Lco/uk/getmondo/model/Amount;", "getAmount", "()Lco/uk/getmondo/model/Amount;", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "date", "", "getDate", "()Ljava/lang/String;", "declinedReason", "getDeclinedReason", "isDeclined", "", "()Z", "localAmountText", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class d implements g {
   private final co.uk.getmondo.common.i.b a;
   private final aj b;

   public d(aj var) {
      kotlin.d.b.l.b(var, "transaction");
      super();
      this.b = var;
      this.a = new co.uk.getmondo.common.i.b(true, false, false, 6, (kotlin.d.b.i)null);
   }

   public j a(Context var) {
      kotlin.d.b.l.b(var, "context");
      return g.a.a(this, (Context)var);
   }

   public String a() {
      String var = this.b.o();
      kotlin.d.b.l.a(var, "transaction.createdAtDateTimeString");
      return var;
   }

   public String a(Resources var) {
      kotlin.d.b.l.b(var, "resources");
      return g.a.a(this, (Resources)var);
   }

   public co.uk.getmondo.d.c b() {
      return this.b.g();
   }

   public j b(Context var) {
      kotlin.d.b.l.b(var, "context");
      return g.a.b(this, (Context)var);
   }

   public String b(Resources var) {
      kotlin.d.b.l.b(var, "resources");
      return g.a.b(this, (Resources)var);
   }

   public boolean c() {
      return this.b.p();
   }

   public String d() {
      String var;
      if(this.b.p()) {
         var = (String)this.b.d().a();
      } else {
         var = null;
      }

      return var;
   }

   public String e() {
      String var;
      if(this.b.i()) {
         co.uk.getmondo.common.i.b var = this.a;
         co.uk.getmondo.d.c var = this.b.h();
         kotlin.d.b.l.a(var, "transaction.localAmount");
         var = var.a(var);
         String var = this.b.h().l().b();
         var = "" + var + ' ' + var;
      } else {
         var = null;
      }

      return var;
   }

   public int f() {
      return g.a.a(this);
   }

   public boolean g() {
      return g.a.b(this);
   }

   public co.uk.getmondo.transaction.details.c.c h() {
      return g.a.c(this);
   }

   public boolean i() {
      return g.a.d(this);
   }
}
