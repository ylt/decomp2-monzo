package co.uk.getmondo.transaction.details.b;

import android.content.Context;
import android.content.res.Resources;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\n\u0010\"\u001a\u0004\u0018\u00010\u000fH\u0016J\u0012\u0010#\u001a\u0004\u0018\u00010\u000f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0016J\u0010\u0010(\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020!H&J\u0010\u0010)\u001a\u00020%2\u0006\u0010&\u001a\u00020'H\u0016R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u00078WX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0011R\u0012\u0010\u0014\u001a\u00020\u0015X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0016R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0016¨\u0006*"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/Header;", "", "amount", "Lco/uk/getmondo/model/Amount;", "getAmount", "()Lco/uk/getmondo/model/Amount;", "amountDisplayStyle", "", "getAmountDisplayStyle", "()I", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "date", "", "getDate", "()Ljava/lang/String;", "declinedReason", "getDeclinedReason", "isDeclined", "", "()Z", "mapCoordinates", "Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "getMapCoordinates", "()Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "showCategoryChooser", "getShowCategoryChooser", "showSubtitleBeforeTitle", "getShowSubtitleBeforeTitle", "extraInformation", "resources", "Landroid/content/res/Resources;", "localAmountText", "subtitle", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/TextAppearance;", "context", "Landroid/content/Context;", "title", "titleTextAppearance", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface g {
   String c(Resources var);

   c j();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      public static int a(g var) {
         return 2131493046;
      }

      public static j a(g var, Context var) {
         kotlin.d.b.l.b(var, "context");
         return (j)(new k(var));
      }

      public static String a(g var, Resources var) {
         kotlin.d.b.l.b(var, "resources");
         return null;
      }

      public static j b(g var, Context var) {
         kotlin.d.b.l.b(var, "context");
         return (j)(new i(var));
      }

      public static String b(g var, Resources var) {
         kotlin.d.b.l.b(var, "resources");
         return null;
      }

      public static boolean b(g var) {
         return false;
      }

      public static co.uk.getmondo.transaction.details.c.c c(g var) {
         return null;
      }

      public static boolean d(g var) {
         return true;
      }
   }
}
