package co.uk.getmondo.transaction.details.b;

import android.content.Context;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u000f\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016¢\u0006\u0002\u0010\u0012R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0005R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/Avatar;", "", "imageUrl", "", "getImageUrl", "()Ljava/lang/String;", "isSquare", "", "()Z", "name", "getName", "resourceId", "", "getResourceId", "()Ljava/lang/Integer;", "backgroundColour", "context", "Landroid/content/Context;", "(Landroid/content/Context;)Ljava/lang/Integer;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface c {
   Integer a(Context var);

   String a();

   Integer b();

   String c();

   boolean d();

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      public static Integer a(c var, Context var) {
         kotlin.d.b.l.b(var, "context");
         return null;
      }

      public static String a(c var) {
         return null;
      }

      public static Integer b(c var) {
         return null;
      }

      public static String c(c var) {
         return null;
      }
   }
}
