package co.uk.getmondo.transaction.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.text.emoji.widget.EmojiTextView;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.view.View;
import co.uk.getmondo.common.k.e;
import co.uk.getmondo.common.k.n;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0003J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0014¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/transaction/details/AtmInfoActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "()V", "createText", "", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AtmInfoActivity extends co.uk.getmondo.common.activities.b {
   public static final AtmInfoActivity.a a = new AtmInfoActivity.a((i)null);
   private HashMap b;

   private final CharSequence a() {
      Spanned var = Html.fromHtml(this.getString(2131362017, new Object[]{e.b(128184)}));
      String[] var = this.getResources().getStringArray(2131755008);
      Spanned var = Html.fromHtml(this.getString(2131362018, new Object[]{e.b(10084)}));
      int var = n.b(8);
      SpannableStringBuilder var = new SpannableStringBuilder();
      var.append((CharSequence)var);
      var.append((CharSequence)"\n\n");

      for(int var = 0; var < var.length; ++var) {
         String var = var[var];
         SpannableString var = new SpannableString((CharSequence)var);
         var.setSpan(new BulletSpan(var), 0, var.length(), 17);
         var.append((CharSequence)var);
         var.append('\n');
      }

      var.append('\n');
      var.append((CharSequence)var);
      return (CharSequence)var;
   }

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034144);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var == null) {
         l.a();
      }

      var.b(true);
      ((EmojiTextView)this.a(co.uk.getmondo.c.a.atmInfoText)).setText(this.a());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/transaction/details/AtmInfoActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, AtmInfoActivity.class);
      }
   }
}
