package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "total", "", "averageAmount", "Lco/uk/getmondo/model/Amount;", "totalAmount", "(JLco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getAverageAmount", "()Lco/uk/getmondo/model/Amount;", "getTotal", "()J", "getTotalAmount", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends f {
   private final long a;
   private final co.uk.getmondo.d.c b;
   private final co.uk.getmondo.d.c c;

   public a(long var, co.uk.getmondo.d.c var, co.uk.getmondo.d.c var) {
      l.b(var, "averageAmount");
      l.b(var, "totalAmount");
      super((i)null);
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public final long a() {
      return this.a;
   }

   public final co.uk.getmondo.d.c b() {
      return this.b;
   }

   public final co.uk.getmondo.d.c c() {
      return this.c;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof a)) {
            return var;
         }

         a var = (a)var;
         boolean var;
         if(this.a == var.a) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.b, var.b)) {
            return var;
         }

         var = var;
         if(!l.a(this.c, var.c)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      long var = this.a;
      int var = (int)(var ^ var >>> 32);
      co.uk.getmondo.d.c var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "AverageSpendingHistory(total=" + this.a + ", averageAmount=" + this.b + ", totalAmount=" + this.c + ")";
   }
}
