package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/CashFlowHistory;", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "totalOutgoing", "", "totalOutgoingAmount", "Lco/uk/getmondo/model/Amount;", "totalIncoming", "totalIncomingAmount", "(JLco/uk/getmondo/model/Amount;JLco/uk/getmondo/model/Amount;)V", "getTotalIncoming", "()J", "getTotalIncomingAmount", "()Lco/uk/getmondo/model/Amount;", "getTotalOutgoing", "getTotalOutgoingAmount", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends f {
   private final long a;
   private final co.uk.getmondo.d.c b;
   private final long c;
   private final co.uk.getmondo.d.c d;

   public b(long var, co.uk.getmondo.d.c var, long var, co.uk.getmondo.d.c var) {
      l.b(var, "totalOutgoingAmount");
      l.b(var, "totalIncomingAmount");
      super((i)null);
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final long a() {
      return this.a;
   }

   public final co.uk.getmondo.d.c b() {
      return this.b;
   }

   public final long c() {
      return this.c;
   }

   public final co.uk.getmondo.d.c d() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof b)) {
            return var;
         }

         b var = (b)var;
         boolean var;
         if(this.a == var.a) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.b, var.b)) {
            return var;
         }

         if(this.c == var.c) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.d, var.d)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      long var = this.a;
      int var = (int)(var ^ var >>> 32);
      co.uk.getmondo.d.c var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var = (int)(var ^ var >>> 32);
      var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return ((var + var * 31) * 31 + var) * 31 + var;
   }

   public String toString() {
      return "CashFlowHistory(totalOutgoing=" + this.a + ", totalOutgoingAmount=" + this.b + ", totalIncoming=" + this.c + ", totalIncomingAmount=" + this.d + ")";
   }
}
