package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.i;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0003\u0004\u0005¨\u0006\u0006"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "", "()V", "Lco/uk/getmondo/transaction/details/model/NoHistory;", "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "Lco/uk/getmondo/transaction/details/model/CashFlowHistory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class f {
   private f() {
   }

   // $FF: synthetic method
   public f(i var) {
      this();
   }
}
