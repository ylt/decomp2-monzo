package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/SpendingBehaviour;", "", "totalSpent", "Lco/uk/getmondo/model/Amount;", "averageSpend", "noOfTransactions", "", "(Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;J)V", "getAverageSpend", "()Lco/uk/getmondo/model/Amount;", "getNoOfTransactions", "()J", "getTotalSpent", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   private final co.uk.getmondo.d.c a;
   private final co.uk.getmondo.d.c b;
   private final long c;

   public e(co.uk.getmondo.d.c var, co.uk.getmondo.d.c var, long var) {
      l.b(var, "totalSpent");
      l.b(var, "averageSpend");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public final co.uk.getmondo.d.c a() {
      return this.a;
   }

   public final co.uk.getmondo.d.c b() {
      return this.b;
   }

   public final long c() {
      return this.c;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof e)) {
            return var;
         }

         e var = (e)var;
         var = var;
         if(!l.a(this.a, var.a)) {
            return var;
         }

         var = var;
         if(!l.a(this.b, var.b)) {
            return var;
         }

         boolean var;
         if(this.c == var.c) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      co.uk.getmondo.d.c var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b;
      if(var != null) {
         var = var.hashCode();
      }

      long var = this.c;
      return (var * 31 + var) * 31 + (int)(var ^ var >>> 32);
   }

   public String toString() {
      return "SpendingBehaviour(totalSpent=" + this.a + ", averageSpend=" + this.b + ", noOfTransactions=" + this.c + ")";
   }
}
