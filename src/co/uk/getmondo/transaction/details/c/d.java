package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.i;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/NoHistory;", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "()V", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends f {
   public static final d a;

   static {
      new d();
   }

   private d() {
      super((i)null);
      a = (d)this;
   }
}
