package co.uk.getmondo.transaction.details;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB[\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "transactionId", "", "transactionManager", "Lco/uk/getmondo/transaction/data/TransactionManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "costSplitter", "Lco/uk/getmondo/transaction/CostSplitter;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lco/uk/getmondo/transaction/data/TransactionManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final String e;
   private final co.uk.getmondo.transaction.a.a f;
   private final co.uk.getmondo.background_sync.d g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.transaction.a i;
   private final p j;
   private final co.uk.getmondo.common.a k;
   private final co.uk.getmondo.common.accounts.b l;

   public b(u var, u var, String var, co.uk.getmondo.transaction.a.a var, co.uk.getmondo.background_sync.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.transaction.a var, p var, co.uk.getmondo.common.a var, co.uk.getmondo.common.accounts.b var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "transactionId");
      l.b(var, "transactionManager");
      l.b(var, "syncManager");
      l.b(var, "apiErrorHandler");
      l.b(var, "costSplitter");
      l.b(var, "userSettingsStorage");
      l.b(var, "analyticsService");
      l.b(var, "accountManager");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((f)var);
      if(this.f.a(this.e) == null) {
         d.a.a.a((Throwable)(new RuntimeException("Transaction missing, syncing")));
         this.a((io.reactivex.b.b)this.g.a().b(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
            public final void a(Throwable varx) {
               co.uk.getmondo.common.e.a var = b.this.h;
               l.a(varx, "throwable");
               var.a(varx, (co.uk.getmondo.common.e.a.a)var);
            }
         })));
      }

      this.k.a(Impression.Companion.f(this.e));
      n var = this.f.b(this.e);
      io.reactivex.b.a var = this.b;
      n var = var.switchMapSingle((h)(new h() {
         public final v a(final aj var) {
            l.b(var, "transaction");
            return b.this.f.a(var).d((h)(new h() {
               public final kotlin.h a(co.uk.getmondo.transaction.details.c.f varx) {
                  l.b(varx, "history");
                  return new kotlin.h(var, varx);
               }
            }));
         }
      })).observeOn(this.c);
      g var = (g)(new g() {
         public final void a(kotlin.h varx) {
            aj var = (aj)varx.c();
            co.uk.getmondo.transaction.details.c.f var = (co.uk.getmondo.transaction.details.c.f)varx.d();
            b.a var = var;
            co.uk.getmondo.d.h var = var.c();
            l.a(var, "transaction.category");
            var.a(var);
            b.a var;
            if(var.r()) {
               var = var;
               l.a(var, "transaction");
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory");
               }

               var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.c.a(var, (co.uk.getmondo.transaction.details.c.b)var)));
            } else if(var.s()) {
               var = var;
               l.a(var, "transaction");
               l.a(var, "transactionHistory");
               var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.b.b(var, var)));
            } else if(var.q()) {
               var = var;
               l.a(var, "transaction");
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory");
               }

               var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.f.c(var, (co.uk.getmondo.transaction.details.c.b)var)));
            } else if(var.z()) {
               var = var;
               l.a(var, "transaction");
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.h.c(var, (co.uk.getmondo.transaction.details.c.a)var, b.this.l)));
            } else if(var.a()) {
               var = var;
               l.a(var, "transaction");
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.g.b(var, (co.uk.getmondo.transaction.details.c.a)var)));
            } else {
               co.uk.getmondo.d.u var = var.f();
               if(var != null && var.g()) {
                  var = var;
                  l.a(var, "transaction");
                  if(var == null) {
                     throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
                  }

                  var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.a.a(var, (co.uk.getmondo.transaction.details.c.a)var)));
               } else {
                  var = var;
                  l.a(var, "transaction");
                  l.a(var, "transactionHistory");
                  var.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.d.d(var, var, b.this.i, b.this.j, b.this.k, b.this.l)));
               }
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new c(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (g)var);
      l.a(var, "transactionObservable\n  …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      n var = var.a().switchMap((h)(new h() {
         public final n a(kotlin.n varx) {
            l.b(varx, "it");
            return var.a(b.this.e);
         }
      }));
      l.a(var, "transactionObservable");
      io.reactivex.b var = co.uk.getmondo.common.j.f.a(var, (r)var).doOnNext((g)(new g() {
         public final void a(kotlin.h var) {
            co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)var.c();
            aj var = (aj)var.d();
            co.uk.getmondo.common.a var = b.this.k;
            Impression.Companion var = Impression.Companion;
            co.uk.getmondo.d.h var = var.c();
            l.a(var, "transaction.category");
            l.a(var, "category");
            var.a(var.a(var, var));
         }
      })).flatMapCompletable((h)(new h() {
         public final io.reactivex.b a(kotlin.h varx) {
            l.b(varx, "<name for destructuring parameter 0>");
            co.uk.getmondo.d.h var = (co.uk.getmondo.d.h)varx.c();
            return b.this.f.a(b.this.e, var).b(b.this.d).a(b.this.c).a((g)(new g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = b.this.h;
                  l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).b();
         }
      }));
      io.reactivex.c.a var = (io.reactivex.c.a)null.a;
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new c(var);
      }

      var = var.a(var, (g)var);
      l.a(var, "view.onChangeCategoryCli….subscribe({}, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      var = var.c();
      g var = (g)(new g() {
         public final void a(kotlin.n var) {
            b.this.k.a(Impression.Companion.g(b.this.e));
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new c(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (g)var);
      l.a(var, "view.onMapClicked()\n    …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0004H&J\u0010\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0005H&J\u0010\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fH&¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onCategoryChanged", "Lio/reactivex/Observable;", "Lco/uk/getmondo/model/Category;", "transactionId", "", "onChangeCategoryClicked", "", "onMapClicked", "setCategory", "category", "setTransaction", "transactionViewModel", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      n a();

      n a(String var);

      void a(co.uk.getmondo.d.h var);

      void a(co.uk.getmondo.transaction.details.b.l var);

      n c();
   }
}
