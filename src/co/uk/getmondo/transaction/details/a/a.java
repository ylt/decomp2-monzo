package co.uk.getmondo.transaction.details.a;

import android.content.Intent;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/transaction/details/actions/AddContactAction;", "Lco/uk/getmondo/transaction/details/base/Action;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "action", "", "activity", "Landroid/support/v7/app/AppCompatActivity;", "placeholderIcon", "", "title", "", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements co.uk.getmondo.transaction.details.b.a {
   private final aj a;

   public a(aj var) {
      l.b(var, "transaction");
      super();
      this.a = var;
   }

   public int a() {
      return 2130837768;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var = var.getString(2131362839);
      l.a(var, "resources.getString(R.string.tx_p2p_add_contact)");
      return var;
   }

   public void a(android.support.v7.app.e var) {
      l.b(var, "activity");
      Intent var = new Intent("android.intent.action.INSERT");
      var.setType("vnd.android.cursor.dir/raw_contact");
      var.putExtra("finishActivityOnSaveCompleted", true);
      var.putExtra("phone", this.a.B().d());
      var.putExtra("name", this.a.B().b());
      var.startActivity(var);
   }

   public String b() {
      return co.uk.getmondo.transaction.details.b.a.a.a(this);
   }

   public String b(Resources var) {
      l.b(var, "resources");
      return co.uk.getmondo.transaction.details.b.a.a.a(this, var);
   }
}
