package co.uk.getmondo.transaction.details.a;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/transaction/details/actions/ManageDirectDebits;", "Lco/uk/getmondo/transaction/details/base/Action;", "()V", "action", "", "activity", "Landroid/support/v7/app/AppCompatActivity;", "placeholderIcon", "", "title", "", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements co.uk.getmondo.transaction.details.b.a {
   public int a() {
      return 2130837763;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var = var.getString(2131362825);
      l.a(var, "resources.getString(R.st…tion_manage_direct_debit)");
      return var;
   }

   public void a(android.support.v7.app.e var) {
      l.b(var, "activity");
      var.startActivity(new Intent((Context)var, RecurringPaymentsActivity.class));
   }

   public String b() {
      return co.uk.getmondo.transaction.details.b.a.a.a(this);
   }

   public String b(Resources var) {
      l.b(var, "resources");
      return co.uk.getmondo.transaction.details.b.a.a.a(this, var);
   }
}
