package co.uk.getmondo.transaction.details.a;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.attachment.AttachmentActivity;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/transaction/details/actions/AddReceiptAction;", "Lco/uk/getmondo/transaction/details/base/Action;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "iconUrl", "", "getIconUrl", "()Ljava/lang/String;", "action", "", "activity", "Landroid/support/v7/app/AppCompatActivity;", "placeholderIcon", "", "title", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements co.uk.getmondo.transaction.details.b.a {
   private final aj a;

   public c(aj var) {
      l.b(var, "transaction");
      super();
      this.a = var;
   }

   public int a() {
      return 2130837762;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var = var.getString(2131362823);
      l.a(var, "resources.getString(R.st…ction_add_receipt_action)");
      return var;
   }

   public void a(android.support.v7.app.e var) {
      l.b(var, "activity");
      AttachmentActivity.a((Context)var, this.a.w());
   }

   public String b() {
      return this.a.F();
   }

   public String b(Resources var) {
      l.b(var, "resources");
      return co.uk.getmondo.transaction.details.b.a.a.a(this, var);
   }
}
