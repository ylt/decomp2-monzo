package co.uk.getmondo.transaction.details.a;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.t;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import co.uk.getmondo.monzo.me.customise.CustomiseMonzoMeLinkActivity;
import co.uk.getmondo.payments.send.data.p;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerIntroActivity;
import co.uk.getmondo.transaction.splitting.SplitBottomSheetFragment;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/transaction/details/actions/SplitWithMonzoMe;", "Lco/uk/getmondo/transaction/details/base/Action;", "transaction", "Lco/uk/getmondo/model/Transaction;", "costSplitter", "Lco/uk/getmondo/transaction/CostSplitter;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;)V", "action", "", "activity", "Landroid/support/v7/app/AppCompatActivity;", "placeholderIcon", "", "title", "", "resources", "Landroid/content/res/Resources;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g implements co.uk.getmondo.transaction.details.b.a {
   public static final g.a a = new g.a((i)null);
   private final aj b;
   private final co.uk.getmondo.transaction.a c;
   private final p d;
   private final co.uk.getmondo.common.a e;

   public g(aj var, co.uk.getmondo.transaction.a var, p var, co.uk.getmondo.common.a var) {
      l.b(var, "transaction");
      l.b(var, "costSplitter");
      l.b(var, "userSettingsStorage");
      l.b(var, "analyticsService");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public int a() {
      return 2130837765;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var = var.getString(2131362742);
      l.a(var, "resources.getString(R.string.split_cost_action)");
      return var;
   }

   public void a(final android.support.v7.app.e var) {
      l.b(var, "activity");
      this.e.a(Impression.Companion.T());
      co.uk.getmondo.payments.send.data.a.d var = this.d.a();
      if(l.a(var, co.uk.getmondo.payments.send.data.a.d.b)) {
         var.startActivity(PeerToPeerIntroActivity.a((Context)var, t.c));
      } else if(l.a(var, co.uk.getmondo.payments.send.data.a.d.c)) {
         co.uk.getmondo.common.d.e.a().show(var.getFragmentManager(), "TAG_ERROR_P2P_BLOCKED");
      } else {
         SplitBottomSheetFragment.a(var, co.uk.getmondo.transaction.a.a(this.c, this.b, 0, 2, (Object)null), (SplitBottomSheetFragment.a)(new SplitBottomSheetFragment.a() {
            public void a(Dialog varx) {
               String var;
               label11: {
                  l.b(varx, "dialog");
                  g.this.e.a(Impression.Companion.U());
                  u var = g.this.b.f();
                  if(var != null) {
                     var = var.f();
                     if(var != null) {
                        break label11;
                     }
                  }

                  var = "";
               }

               CustomiseMonzoMeLinkActivity.a((Context)var, (co.uk.getmondo.d.c)null, var, Impression.CustomiseMonzoMeLinkFrom.SPLIT_COST);
               varx.dismiss();
            }

            public void a(Dialog varx, int var) {
               co.uk.getmondo.d.c var;
               String var;
               label11: {
                  l.b(varx, "dialog");
                  g.this.e.a(Impression.Companion.c(var));
                  var = g.this.b.g().a(var + 1);
                  u var = g.this.b.f();
                  if(var != null) {
                     var = var.f();
                     if(var != null) {
                        break label11;
                     }
                  }

                  var = "";
               }

               CustomiseMonzoMeLinkActivity.a((Context)var, var.j(), var, Impression.CustomiseMonzoMeLinkFrom.SPLIT_COST);
               varx.dismiss();
            }
         }));
      }

   }

   public String b() {
      return co.uk.getmondo.transaction.details.b.a.a.a(this);
   }

   public String b(Resources var) {
      l.b(var, "resources");
      return co.uk.getmondo.transaction.details.b.a.a.a(this, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/transaction/details/actions/SplitWithMonzoMe$Companion;", "", "()V", "TAG_ERROR_P2P_BLOCKED", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
