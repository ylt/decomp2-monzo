package co.uk.getmondo.transaction.details;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.transaction.change_category.ChangeCategoryDialogFragment;
import co.uk.getmondo.transaction.details.views.AvatarView;
import co.uk.getmondo.transaction.details.views.ContentView;
import co.uk.getmondo.transaction.details.views.FooterView;
import co.uk.getmondo.transaction.details.views.HeaderView;
import co.uk.getmondo.transaction.details.views.TransactionActionsView;
import co.uk.getmondo.transaction.details.views.TransactionAppBarLayout;
import co.uk.getmondo.transaction.details.views.VisibilityAwareFloatingActionButton;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import io.reactivex.c.f;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.d.b.y;
import kotlin.reflect.e;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005¢\u0006\u0002\u0010\u0003J\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u000fH\u0016J\u0012\u0010\u0014\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\b\u0010\u0017\u001a\u00020\u0006H\u0014J\b\u0010\u0018\u001a\u00020\u0006H\u0016J\u000e\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0016J\b\u0010\u001a\u001a\u00020\u0006H\u0014J\b\u0010\u001b\u001a\u00020\u0006H\u0014J\u0010\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0016H\u0014J\b\u0010\u001e\u001a\u00020\u0006H\u0014J\b\u0010\u001f\u001a\u00020\u0006H\u0014J\u0010\u0010 \u001a\u00020\u00062\u0006\u0010!\u001a\u00020\u0010H\u0016J\u0010\u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020$H\u0016R2\u0010\u0004\u001a&\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006 \u0007*\u0012\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\u00020\t8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r¨\u0006&"},
   d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;", "()V", "mapClickRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "presenter", "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;", "getPresenter", "()Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;", "setPresenter", "(Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;)V", "onCategoryChanged", "Lio/reactivex/Observable;", "Lco/uk/getmondo/model/Category;", "transactionId", "", "onChangeCategoryClicked", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onLowMemory", "onMapClicked", "onPause", "onResume", "onSaveInstanceState", "outState", "onStart", "onStop", "setCategory", "category", "setTransaction", "transactionViewModel", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TransactionDetailsActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final TransactionDetailsActivity.a b = new TransactionDetailsActivity.a((i)null);
   public b a;
   private final com.b.b.c c = com.b.b.c.a();
   private HashMap e;

   public static final Intent a(Context var, String var) {
      l.b(var, "context");
      l.b(var, "transactionId");
      return b.a(var, var);
   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((VisibilityAwareFloatingActionButton)this.a(co.uk.getmondo.c.a.transactionActionFab)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public n a(final String var) {
      l.b(var, "transactionId");
      n var = n.create((p)(new p() {
         public final void a(final o varx) {
            l.b(varx, "emitter");
            final ChangeCategoryDialogFragment var = ChangeCategoryDialogFragment.a(var);
            var.a((ChangeCategoryDialogFragment.a)(new ChangeCategoryDialogFragment.a() {
               public final void a(co.uk.getmondo.d.h varxx) {
                  varx.a(varxx);
               }
            }));
            var.show(TransactionDetailsActivity.this.getFragmentManager(), "TAG_CHANGE_CATEGORY");
            var.a((ChangeCategoryDialogFragment.b)(new ChangeCategoryDialogFragment.b() {
               public final void a() {
                  varx.a();
               }
            }));
            varx.a((f)(new f() {
               public final void a() {
                  var.a((ChangeCategoryDialogFragment.a)null);
                  var.a((ChangeCategoryDialogFragment.b)null);
                  var.dismiss();
               }
            }));
         }
      }));
      l.a(var, "Observable.create<Catego…)\n            }\n        }");
      return var;
   }

   public void a(co.uk.getmondo.d.h var) {
      l.b(var, "category");
      ((VisibilityAwareFloatingActionButton)this.a(co.uk.getmondo.c.a.transactionActionFab)).setContentDescription((CharSequence)this.getString(var.a()));
      ((VisibilityAwareFloatingActionButton)this.a(co.uk.getmondo.c.a.transactionActionFab)).setImageResource(var.d());
      ((VisibilityAwareFloatingActionButton)this.a(co.uk.getmondo.c.a.transactionActionFab)).setBackgroundTintList(ColorStateList.valueOf(android.support.v4.content.a.c((Context)this, var.b())));
   }

   public void a(co.uk.getmondo.transaction.details.b.l var) {
      l.b(var, "transactionViewModel");
      VisibilityAwareFloatingActionButton var = (VisibilityAwareFloatingActionButton)this.a(co.uk.getmondo.c.a.transactionActionFab);
      byte var;
      if(var.a().i()) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).a(var.a());
      ((AvatarView)this.a(co.uk.getmondo.c.a.transactionAvatarView)).a(var.a());
      ((HeaderView)this.a(co.uk.getmondo.c.a.transactionHeaderView)).a(var.a());
      ((TransactionActionsView)this.a(co.uk.getmondo.c.a.transactionActionsView)).a(var.b());
      ((ContentView)this.a(co.uk.getmondo.c.a.transactionContentView)).a(var.c());
      co.uk.getmondo.transaction.details.b.f var = var.d();
      if(var != null) {
         ((FooterView)this.a(co.uk.getmondo.c.a.transactionFooterView)).a(var);
      } else {
         ae.b((FooterView)this.a(co.uk.getmondo.c.a.transactionFooterView));
      }

   }

   public com.b.b.c b() {
      com.b.b.c var = this.c;
      l.a(var, "mapClickRelay");
      return var;
   }

   // $FF: synthetic method
   public n c() {
      return (n)this.b();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034224);
      String var = this.getIntent().getStringExtra("KEY_TRANSACTION_ID");
      this.l().a(new co.uk.getmondo.transaction.d(var)).a(this);
      this.setSupportActionBar(this.r());
      Object var = this.getSystemService("notification");
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
      } else {
         ((NotificationManager)var).cancel(var.hashCode());
         b var = this.a;
         if(var == null) {
            l.b("presenter");
         }

         var.a((b.a)this);
         ((AvatarView)this.a(co.uk.getmondo.c.a.transactionAvatarView)).setBackgroundListener((m)(new m() {
            // $FF: synthetic method
            public Object a(Object var, Object var) {
               this.a((Drawable)var, ((Boolean)var).booleanValue());
               return kotlin.n.a;
            }

            public final void a(Drawable var, boolean var) {
               l.b(var, "background");
               if(var) {
                  ae.a(TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.backgroundImageOverlay));
               } else {
                  ae.b(TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.backgroundImageOverlay));
               }

               ((ImageView)((TransactionAppBarLayout)TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).b(co.uk.getmondo.c.a.backgroundImage)).setImageDrawable(var);
            }
         }));
         ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).setMapListener((kotlin.d.a.b)(new kotlin.d.a.b(this.c) {
            public final e a() {
               return y.a(com.b.b.c.class);
            }

            public final void a(kotlin.n var) {
               ((com.b.b.c)this.b).a((Object)var);
            }

            public final String b() {
               return "accept";
            }

            public final String c() {
               return "accept(Ljava/lang/Object;)V";
            }
         }));
         ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).a(var);
         ((NestedScrollView)this.a(co.uk.getmondo.c.a.mainScrollContent)).setOnScrollChangeListener((android.support.v4.widget.NestedScrollView.b)(new android.support.v4.widget.NestedScrollView.b() {
            public final void a(NestedScrollView var, int var, int var, int var, int var) {
               if(var >= ((HeaderView)TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.transactionHeaderView)).c()) {
                  ((TransactionAppBarLayout)TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).e();
               } else {
                  ((TransactionAppBarLayout)TransactionDetailsActivity.this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).f();
               }

            }
         }));
      }
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).k();
      super.onDestroy();
   }

   public void onLowMemory() {
      super.onLowMemory();
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).l();
   }

   protected void onPause() {
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).i();
      super.onPause();
   }

   protected void onResume() {
      super.onResume();
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).h();
   }

   protected void onSaveInstanceState(Bundle var) {
      l.b(var, "outState");
      super.onSaveInstanceState(var);
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).b(var);
   }

   protected void onStart() {
      super.onStart();
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).g();
   }

   protected void onStop() {
      ((TransactionAppBarLayout)this.a(co.uk.getmondo.c.a.transactionAppBarLayout)).j();
      super.onStop();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsActivity$Companion;", "", "()V", "KEY_TRANSACTION_ID", "", "TAG_CHANGE_CATEGORY", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "transactionId", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, String var) {
         l.b(var, "context");
         l.b(var, "transactionId");
         Intent var = (new Intent(var, TransactionDetailsActivity.class)).putExtra("KEY_TRANSACTION_ID", var);
         l.a(var, "Intent(context, Transact…ACTION_ID, transactionId)");
         return var;
      }
   }
}
