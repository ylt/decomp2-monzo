package co.uk.getmondo.transaction.details.d.h;

import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.a.h;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.e;
import co.uk.getmondo.transaction.details.b.f;
import co.uk.getmondo.transaction.details.b.l;
import co.uk.getmondo.transaction.details.b.n;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u0016\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0017\u001a\u00020\u0005HÂ\u0003J\t\u0010\u0018\u001a\u00020\u0007HÂ\u0003J'\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u00138VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\""},
   d2 = {"Lco/uk/getmondo/transaction/details/types/topup/TopUpTransaction;", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;Lco/uk/getmondo/common/accounts/AccountManager;)V", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "getActions", "()Ljava/util/List;", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "getContent", "()Lco/uk/getmondo/transaction/details/base/Content;", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "getHeader", "()Lco/uk/getmondo/transaction/details/base/BaseHeader;", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements l {
   private final aj a;
   private final co.uk.getmondo.transaction.details.c.a b;
   private final co.uk.getmondo.common.accounts.b c;

   public c(aj var, co.uk.getmondo.transaction.details.c.a var, co.uk.getmondo.common.accounts.b var) {
      kotlin.d.b.l.b(var, "transaction");
      kotlin.d.b.l.b(var, "transactionHistory");
      kotlin.d.b.l.b(var, "accountManager");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public d a() {
      return (d)(new a(this.a));
   }

   public List b() {
      return m.b(new co.uk.getmondo.transaction.details.b.a[]{(co.uk.getmondo.transaction.details.b.a)(new h(this.c.b())), (co.uk.getmondo.transaction.details.b.a)(new co.uk.getmondo.transaction.details.a.b(this.a)), (co.uk.getmondo.transaction.details.b.a)(new co.uk.getmondo.transaction.details.a.c(this.a))});
   }

   public e c() {
      return (e)(new e() {
         public co.uk.getmondo.transaction.details.b.h a() {
            return e.a.a(this);
         }

         public n b() {
            return (n)(new b(c.this.b));
         }
      });
   }

   public f d() {
      return l.a.a(this);
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof c) {
               c var = (c)var;
               if(kotlin.d.b.l.a(this.a, var.a) && kotlin.d.b.l.a(this.b, var.b) && kotlin.d.b.l.a(this.c, var.c)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      aj var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.transaction.details.c.a var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.common.accounts.b var = this.c;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "TopUpTransaction(transaction=" + this.a + ", transactionHistory=" + this.b + ", accountManager=" + this.c + ")";
   }
}
