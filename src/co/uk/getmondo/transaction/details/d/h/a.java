package co.uk.getmondo.transaction.details.d.h;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.d;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/topup/TopUpHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "showCategoryChooser", "", "getShowCategoryChooser", "()Z", "title", "", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends d {
   public a(aj var) {
      l.b(var, "transaction");
      super(var);
   }

   public String c(Resources var) {
      l.b(var, "resources");
      String var = var.getString(2131362844);
      l.a(var, "resources.getString(R.string.tx_title_topup)");
      return var;
   }

   public boolean i() {
      return false;
   }

   public co.uk.getmondo.transaction.details.b.c j() {
      return (co.uk.getmondo.transaction.details.b.c)(new co.uk.getmondo.transaction.details.b.c() {
         public Integer a(Context var) {
            l.b(var, "context");
            return Integer.valueOf(android.support.v4.content.a.c(var, 2131689694));
         }

         public String a() {
            return co.uk.getmondo.transaction.details.b.c.a.a(this);
         }

         public Integer b() {
            return Integer.valueOf(2130837850);
         }

         public String c() {
            return co.uk.getmondo.transaction.details.b.c.a.c(this);
         }

         public boolean d() {
            return true;
         }
      });
   }
}
