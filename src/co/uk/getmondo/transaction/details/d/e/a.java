package co.uk.getmondo.transaction.details.d.e;

import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/merchant/MerchantHistory;", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;)V", "items", "", "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;", "getItems", "()Ljava/util/List;", "searchQuery", "", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements n {
   private final aj a;
   private final co.uk.getmondo.transaction.details.c.a b;

   public a(aj var, co.uk.getmondo.transaction.details.c.a var) {
      l.b(var, "transaction");
      l.b(var, "transactionHistory");
      super();
      this.a = var;
      this.b = var;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var;
      if(this.a.u()) {
         var = this.a.x();
      } else {
         u var = this.a.f();
         if(var != null) {
            var = var.i();
         } else {
            var = null;
         }
      }

      String var;
      if(var != null) {
         var = var.getString(2131362905, new Object[]{var});
         l.a(var, "resources.getString(R.st…r_merchant_history, name)");
      } else {
         var = "";
      }

      return var;
   }

   public List a() {
      List var;
      if(this.b.a() <= 1L) {
         var = m.a();
      } else {
         o var = new o() {
            public String a(Resources var) {
               l.b(var, "resources");
               String var;
               if(a.this.a.u()) {
                  var = var.getString(2131362835);
                  l.a(var, "resources.getString(R.st…direct_debit_title_total)");
               } else {
                  var = var.getString(2131362837);
                  l.a(var, "resources.getString(R.st…chant_title_no_of_visits)");
               }

               return var;
            }

            // $FF: synthetic method
            public String b(Resources var) {
               return (String)this.d(var);
            }

            public String c(Resources var) {
               l.b(var, "resources");
               return String.valueOf(a.this.b.a());
            }

            public Void d(Resources var) {
               l.b(var, "resources");
               return null;
            }
         };
         o var = new o() {
            public String a(Resources var) {
               l.b(var, "resources");
               String var = var.getString(2131362836);
               l.a(var, "resources.getString(R.st…x_merchant_average_spend)");
               return var;
            }

            public String b(Resources var) {
               l.b(var, "resources");
               int var = (int)a.this.b.a();
               return var.getQuantityString(2131886085, var, new Object[]{Integer.valueOf(var)});
            }

            public String c(Resources var) {
               l.b(var, "resources");
               String var = var.getString(2131362195, new Object[]{Double.valueOf(a.this.b.b().e())});
               l.a(var, "resources.getString(R.st…o_part_amount_gbp, value)");
               return var;
            }
         };
         o var = new o() {
            public String a(Resources var) {
               l.b(var, "resources");
               String var = var.getString(2131362838);
               l.a(var, "resources.getString(R.st….tx_merchant_total_spent)");
               return var;
            }

            public String b(Resources var) {
               l.b(var, "resources");
               int var = (int)a.this.b.a();
               return var.getQuantityString(2131886085, var, new Object[]{Integer.valueOf(var)});
            }

            public String c(Resources var) {
               l.b(var, "resources");
               String var = var.getString(2131362195, new Object[]{Double.valueOf(a.this.b.c().e())});
               l.a(var, "resources.getString(R.st…o_part_amount_gbp, value)");
               return var;
            }
         };
         var = m.b(new o[]{(o)var, (o)var, (o)var});
      }

      return var;
   }

   public String b(Resources var) {
      l.b(var, "resources");
      u var = this.a.f();
      String var;
      if(var != null) {
         var = var.i();
      } else {
         var = null;
      }

      return var;
   }
}
