package co.uk.getmondo.transaction.details.d.f;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.k.o;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.d;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/p2p/P2pHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "showSubtitleBeforeTitle", "", "getShowSubtitleBeforeTitle", "()Z", "getTitle", "", "subtitle", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends d {
   private final aj a;

   public a(aj var) {
      l.b(var, "transaction");
      super(var);
      this.a = var;
   }

   private final String k() {
      String var;
      if(this.a.r() && j.a((CharSequence)this.a.B().b())) {
         co.uk.getmondo.payments.send.data.a.a var = this.a.C();
         if(var != null) {
            var = var.b();
         } else {
            var = null;
         }

         if(var == null) {
            l.a();
         }

         var = o.a(var);
         StringBuilder var = (new StringBuilder()).append("").append(var).append(" • ");
         var = this.a.C();
         if(var != null) {
            var = var.c();
         } else {
            var = null;
         }

         var = var.append(var).toString();
      } else {
         var = this.a.B().b();
         l.a(var, "transaction.peer.name");
      }

      return var;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      int var;
      if(this.a.g().a()) {
         var = 2131362828;
      } else {
         var = 2131362827;
      }

      return var.getString(var);
   }

   public String c(Resources var) {
      l.b(var, "resources");
      return this.k();
   }

   public boolean g() {
      return this.a.g().a();
   }

   public co.uk.getmondo.transaction.details.b.c j() {
      return (co.uk.getmondo.transaction.details.b.c)(new co.uk.getmondo.transaction.details.b.c() {
         public Integer a(Context var) {
            l.b(var, "context");
            return co.uk.getmondo.transaction.details.b.c.a.a(this, var);
         }

         public String a() {
            return a.this.a.B().g();
         }

         public Integer b() {
            return co.uk.getmondo.transaction.details.b.c.a.b(this);
         }

         public String c() {
            return a.this.k();
         }

         public boolean d() {
            return false;
         }
      });
   }
}
