package co.uk.getmondo.transaction.details.d.f;

import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/p2p/P2pHistory;", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/CashFlowHistory;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/CashFlowHistory;)V", "items", "", "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;", "getItems", "()Ljava/util/List;", "searchQuery", "", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements n {
   private final aj a;
   private final co.uk.getmondo.transaction.details.c.b b;

   public b(aj var, co.uk.getmondo.transaction.details.c.b var) {
      l.b(var, "transaction");
      l.b(var, "transactionHistory");
      super();
      this.a = var;
      this.b = var;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      CharSequence var = (CharSequence)this.a.B().b();
      boolean var;
      if(var != null && !j.a(var)) {
         var = false;
      } else {
         var = true;
      }

      String var;
      if(var) {
         var = var.getString(2131362643);
         l.a(var, "resources.getString(R.string.send_money_history)");
      } else {
         var = var.getString(2131362644, new Object[]{this.a.B().b()});
         l.a(var, "resources.getString(R.st…t, transaction.peer.name)");
      }

      return var;
   }

   public List a() {
      o var = new o() {
         public String a(Resources var) {
            l.b(var, "resources");
            String var = var.getString(2131362840);
            l.a(var, "resources.getString(R.st…ng.tx_p2p_no_of_payments)");
            return var;
         }

         public String b(Resources var) {
            l.b(var, "resources");
            return null;
         }

         public String c(Resources var) {
            l.b(var, "resources");
            return String.valueOf(b.this.b.c() + b.this.b.a());
         }
      };
      o var = new o() {
         public String a(Resources var) {
            l.b(var, "resources");
            String var = var.getString(2131362842);
            l.a(var, "resources.getString(R.string.tx_p2p_total_sent)");
            return var;
         }

         public String b(Resources var) {
            l.b(var, "resources");
            int var = (int)b.this.b.a();
            return var.getQuantityString(2131886085, var, new Object[]{Integer.valueOf(var)});
         }

         public String c(Resources var) {
            l.b(var, "resources");
            String var = var.getString(2131362195, new Object[]{Double.valueOf(b.this.b.b().e())});
            l.a(var, "resources.getString(R.st…o_part_amount_gbp, value)");
            return var;
         }
      };
      o var = new o() {
         public String a(Resources var) {
            l.b(var, "resources");
            String var = var.getString(2131362841);
            l.a(var, "resources.getString(R.st…ng.tx_p2p_total_received)");
            return var;
         }

         public String b(Resources var) {
            l.b(var, "resources");
            int var = (int)b.this.b.c();
            return var.getQuantityString(2131886085, var, new Object[]{Integer.valueOf(var)});
         }

         public String c(Resources var) {
            l.b(var, "resources");
            String var = var.getString(2131362195, new Object[]{Double.valueOf(b.this.b.d().e())});
            l.a(var, "resources.getString(R.st…o_part_amount_gbp, value)");
            return var;
         }
      };
      return m.b(new o[]{(o)var, (o)var, (o)var});
   }

   public String b(Resources var) {
      l.b(var, "resources");
      return this.a.B().b();
   }
}
