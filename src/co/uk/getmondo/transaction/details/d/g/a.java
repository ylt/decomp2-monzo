package co.uk.getmondo.transaction.details.d.g;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import co.uk.getmondo.transaction.details.b.c;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.j;
import co.uk.getmondo.transaction.details.b.m;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/tfl/TflHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "merchant", "Lco/uk/getmondo/model/Merchant;", "subtitle", "", "resources", "Landroid/content/res/Resources;", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/UrlTextAppearance;", "context", "Landroid/content/Context;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends d {
   private final u a;

   public a(aj var) {
      l.b(var, "transaction");
      super(var);
      u var = var.f();
      if(var == null) {
         l.a();
      }

      this.a = var;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      return var.getString(2131362831);
   }

   // $FF: synthetic method
   public j b(Context var) {
      return (j)this.c(var);
   }

   public m c(Context var) {
      l.b(var, "context");
      return new m(var);
   }

   public String c(Resources var) {
      l.b(var, "resources");
      String var = this.a.i();
      l.a(var, "merchant.name");
      return var;
   }

   public c j() {
      return (c)(new c() {
         public Integer a(Context var) {
            l.b(var, "context");
            return Integer.valueOf(android.support.v4.content.a.c(var, 2131689671));
         }

         public String a() {
            CharSequence var = (CharSequence)a.this.a.j();
            boolean var;
            if(var != null && var.length() != 0) {
               var = false;
            } else {
               var = true;
            }

            String var;
            if(var) {
               var = null;
            } else {
               var = a.this.a.j();
            }

            return var;
         }

         public Integer b() {
            return c.a.b(this);
         }

         public String c() {
            return c.a.c(this);
         }

         public boolean d() {
            return true;
         }
      });
   }
}
