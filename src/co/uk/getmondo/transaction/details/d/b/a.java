package co.uk.getmondo.transaction.details.d.b;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.c;
import co.uk.getmondo.transaction.details.b.d;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/bacs/BacsHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "title", "", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends d {
   private final aj a;

   public a(aj var) {
      l.b(var, "transaction");
      super(var);
      this.a = var;
   }

   public String c(Resources var) {
      l.b(var, "resources");
      co.uk.getmondo.payments.send.data.a.a var = this.a.C();
      String var;
      if(var != null) {
         var = var.a();
         if(var != null) {
            return var;
         }
      }

      var = this.a.x();
      l.a(var, "transaction.description");
      return var;
   }

   public c j() {
      return (c)(new c() {
         public Integer a(Context var) {
            l.b(var, "context");
            return c.a.a(this, var);
         }

         public String a() {
            return a.this.a.B().g();
         }

         public Integer b() {
            return c.a.b(this);
         }

         public String c() {
            String var = a.this.a.B().b();
            l.a(var, "transaction.peer.name");
            return var;
         }

         public boolean d() {
            return true;
         }
      });
   }
}
