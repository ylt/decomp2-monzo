package co.uk.getmondo.transaction.details.d.b;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.ui.InfoTextActivity;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.a.c;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.e;
import co.uk.getmondo.transaction.details.b.h;
import co.uk.getmondo.transaction.details.b.l;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.c.f;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u0014\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÂ\u0003J\u001d\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aHÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/bacs/BacsTransaction;", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/TransactionHistory;)V", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "getActions", "()Ljava/util/List;", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "getContent", "()Lco/uk/getmondo/transaction/details/base/Content;", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "getHeader", "()Lco/uk/getmondo/transaction/details/base/BaseHeader;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements l {
   private final aj a;
   private final f b;

   public b(aj var, f var) {
      kotlin.d.b.l.b(var, "transaction");
      kotlin.d.b.l.b(var, "transactionHistory");
      super();
      this.a = var;
      this.b = var;
   }

   public d a() {
      return (d)(new a(this.a));
   }

   public List b() {
      co.uk.getmondo.transaction.details.a.d var;
      if(this.a.g().a()) {
         var = new co.uk.getmondo.transaction.details.a.d();
      } else {
         var = null;
      }

      return m.c(new co.uk.getmondo.transaction.details.b.a[]{(co.uk.getmondo.transaction.details.b.a)(new co.uk.getmondo.transaction.details.a.b(this.a)), (co.uk.getmondo.transaction.details.b.a)(new c(this.a)), (co.uk.getmondo.transaction.details.b.a)var});
   }

   public e c() {
      return (e)(new e() {
         public h a() {
            h var;
            if(b.this.a.u()) {
               var = (h)(new h() {
                  public String a(Resources var) {
                     kotlin.d.b.l.b(var, "resources");
                     String var = var.getString(2131362249);
                     kotlin.d.b.l.a(var, "resources.getString(R.st…ow_do_direct_debits_work)");
                     return var;
                  }

                  public void a(Context var) {
                     kotlin.d.b.l.b(var, "context");
                     InfoTextActivity.a(var, var.getString(2131362249), 2131034161);
                  }
               });
            } else {
               var = null;
            }

            return var;
         }

         public n b() {
            n var;
            if(b.this.a.u()) {
               aj var = b.this.a;
               f var = b.this.b;
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var = (n)(new co.uk.getmondo.transaction.details.d.e.a(var, (co.uk.getmondo.transaction.details.c.a)var));
            } else {
               var = null;
            }

            return var;
         }
      });
   }

   public co.uk.getmondo.transaction.details.b.f d() {
      return l.a.a(this);
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label28: {
            if(var instanceof b) {
               b var = (b)var;
               if(kotlin.d.b.l.a(this.a, var.a) && kotlin.d.b.l.a(this.b, var.b)) {
                  break label28;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      aj var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      f var = this.b;
      if(var != null) {
         var = var.hashCode();
      }

      return var * 31 + var;
   }

   public String toString() {
      return "BacsTransaction(transaction=" + this.a + ", transactionHistory=" + this.b + ")";
   }
}
