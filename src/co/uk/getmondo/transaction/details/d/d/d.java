package co.uk.getmondo.transaction.details.d.d;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.ui.InfoTextActivity;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.payments.send.data.p;
import co.uk.getmondo.transaction.details.a.g;
import co.uk.getmondo.transaction.details.b.h;
import co.uk.getmondo.transaction.details.b.l;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.c.f;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\t\u0010 \u001a\u00020\u0003HÂ\u0003J\t\u0010!\u001a\u00020\u0005HÂ\u0003J\t\u0010\"\u001a\u00020\u0007HÂ\u0003J\t\u0010#\u001a\u00020\tHÂ\u0003J\t\u0010$\u001a\u00020\u000bHÂ\u0003J\t\u0010%\u001a\u00020\rHÂ\u0003JE\u0010&\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*HÖ\u0003J\t\u0010+\u001a\u00020,HÖ\u0001J\t\u0010-\u001a\u00020.HÖ\u0001R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u00020\u00198VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001d8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006/"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/GeneralTransaction;", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "costSplitter", "Lco/uk/getmondo/transaction/CostSplitter;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/TransactionHistory;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;)V", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "getActions", "()Ljava/util/List;", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "getContent", "()Lco/uk/getmondo/transaction/details/base/Content;", "footer", "Lco/uk/getmondo/transaction/details/base/Footer;", "getFooter", "()Lco/uk/getmondo/transaction/details/base/Footer;", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "getHeader", "()Lco/uk/getmondo/transaction/details/base/BaseHeader;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements l {
   private final aj a;
   private final f b;
   private final co.uk.getmondo.transaction.a c;
   private final p d;
   private final co.uk.getmondo.common.a e;
   private final co.uk.getmondo.common.accounts.b f;

   public d(aj var, f var, co.uk.getmondo.transaction.a var, p var, co.uk.getmondo.common.a var, co.uk.getmondo.common.accounts.b var) {
      kotlin.d.b.l.b(var, "transaction");
      kotlin.d.b.l.b(var, "transactionHistory");
      kotlin.d.b.l.b(var, "costSplitter");
      kotlin.d.b.l.b(var, "userSettingsStorage");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "accountManager");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   public co.uk.getmondo.transaction.details.b.d a() {
      return (co.uk.getmondo.transaction.details.b.d)(new c(this.a));
   }

   public List b() {
      g var;
      if(this.a.g().k() <= (long)-200 && !this.a.p() && !this.f.b()) {
         var = new g(this.a, this.c, this.d, this.e);
      } else {
         var = null;
      }

      co.uk.getmondo.transaction.details.a.c var;
      if(this.a.p()) {
         var = null;
      } else {
         var = new co.uk.getmondo.transaction.details.a.c(this.a);
      }

      return m.c(new co.uk.getmondo.transaction.details.b.a[]{(co.uk.getmondo.transaction.details.b.a)(new co.uk.getmondo.transaction.details.a.b(this.a)), (co.uk.getmondo.transaction.details.b.a)var, (co.uk.getmondo.transaction.details.b.a)var});
   }

   public co.uk.getmondo.transaction.details.b.e c() {
      return (co.uk.getmondo.transaction.details.b.e)(new co.uk.getmondo.transaction.details.b.e() {
         public h a() {
            h var;
            if(!d.this.a.b()) {
               var = null;
            } else {
               var = (h)(new h() {
                  public String a(Resources var) {
                     kotlin.d.b.l.b(var, "resources");
                     String var = var.getString(2131362251);
                     kotlin.d.b.l.a(var, "resources.getString(R.st…do_us_restaurants_charge)");
                     return var;
                  }

                  public void a(Context var) {
                     kotlin.d.b.l.b(var, "context");
                     InfoTextActivity.a(var, var.getString(2131362860), 2131034225);
                  }
               });
            }

            return var;
         }

         public n b() {
            n var;
            if(d.this.a.f() != null) {
               aj var = d.this.a;
               f var = d.this.b;
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var = (n)(new co.uk.getmondo.transaction.details.d.e.a(var, (co.uk.getmondo.transaction.details.c.a)var));
            } else {
               var = null;
            }

            return var;
         }
      });
   }

   public co.uk.getmondo.transaction.details.b.f d() {
      return (co.uk.getmondo.transaction.details.b.f)(new b(this.a));
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label36: {
            if(var instanceof d) {
               d var = (d)var;
               if(kotlin.d.b.l.a(this.a, var.a) && kotlin.d.b.l.a(this.b, var.b) && kotlin.d.b.l.a(this.c, var.c) && kotlin.d.b.l.a(this.d, var.d) && kotlin.d.b.l.a(this.e, var.e) && kotlin.d.b.l.a(this.f, var.f)) {
                  break label36;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      aj var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      f var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.transaction.a var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      p var = this.d;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.common.a var = this.e;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.common.accounts.b var = this.f;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + (var + var * 31) * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "GeneralTransaction(transaction=" + this.a + ", transactionHistory=" + this.b + ", costSplitter=" + this.c + ", userSettingsStorage=" + this.d + ", analyticsService=" + this.e + ", accountManager=" + this.f + ")";
   }
}
