package co.uk.getmondo.transaction.details.d.d;

import android.content.Context;
import co.uk.getmondo.d.aj;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0007R\u0014\u0010\b\u001a\u00020\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/UnenrichedAvatar;", "Lco/uk/getmondo/transaction/details/base/Avatar;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "isSquare", "", "()Z", "name", "", "getName", "()Ljava/lang/String;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e implements co.uk.getmondo.transaction.details.b.c {
   private final aj a;

   public e(aj var) {
      l.b(var, "transaction");
      super();
      this.a = var;
   }

   public Integer a(Context var) {
      l.b(var, "context");
      return co.uk.getmondo.transaction.details.b.c.a.a(this, var);
   }

   public String a() {
      return co.uk.getmondo.transaction.details.b.c.a.a(this);
   }

   public Integer b() {
      return co.uk.getmondo.transaction.details.b.c.a.b(this);
   }

   public String c() {
      String var = this.a.x();
      l.a(var, "transaction.description");
      return var;
   }

   public boolean d() {
      return true;
   }
}
