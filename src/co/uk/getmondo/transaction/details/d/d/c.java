package co.uk.getmondo.transaction.details.d.d;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import co.uk.getmondo.transaction.details.b.j;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/GeneralHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "mapCoordinates", "Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "getMapCoordinates", "()Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "extraInformation", "", "resources", "Landroid/content/res/Resources;", "subtitle", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/AddressTextAppearance;", "context", "Landroid/content/Context;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.transaction.details.b.d {
   private final aj a;

   public c(aj var) {
      l.b(var, "transaction");
      super(var);
      this.a = var;
   }

   public String a(Resources var) {
      Object var = null;
      l.b(var, "resources");
      u var = this.a.f();
      String var = (String)var;
      if(var != null) {
         if(var.k()) {
            var = (String)var;
         } else {
            com.c.b.b var = var.c();
            var = (String)var;
            if(var != null) {
               var = (String)var.a((Object)null);
            }
         }
      }

      return var;
   }

   // $FF: synthetic method
   public j b(Context var) {
      return (j)this.c(var);
   }

   public String b(Resources var) {
      l.b(var, "resources");
      String var;
      if(this.a.i()) {
         String var = this.a.g().g();
         String var = this.a.h().g();
         double var = this.a.j();
         if(this.a.k()) {
            var = var.getString(2131362554, new Object[]{var, var, Double.valueOf(var)});
         } else {
            var = var.getString(2131362189, new Object[]{var, var, Double.valueOf(var)});
         }
      } else {
         u var = this.a.f();
         if(var != null && var.k()) {
            var = var.getString(2131362826);
         } else {
            var = null;
         }
      }

      return var;
   }

   public co.uk.getmondo.transaction.details.b.b c(Context var) {
      l.b(var, "context");
      return new co.uk.getmondo.transaction.details.b.b(var);
   }

   public String c(Resources var) {
      l.b(var, "resources");
      u var = this.a.f();
      String var;
      if(var != null) {
         if(var.g()) {
            var = var.getString(2131362843);
            l.a(var, "resources.getString(R.string.tx_title_atm)");
         } else {
            var = var.i();
            l.a(var, "merchant.name");
         }
      } else {
         var = this.a.x();
         l.a(var, "transaction.description");
      }

      return var;
   }

   public co.uk.getmondo.transaction.details.c.c h() {
      Object var = null;
      u var = this.a.f();
      co.uk.getmondo.transaction.details.c.c var = (co.uk.getmondo.transaction.details.c.c)var;
      if(var != null) {
         if(var.k()) {
            var = (co.uk.getmondo.transaction.details.c.c)var;
         } else {
            Double var = var.d();
            Double var = var.e();
            var = (co.uk.getmondo.transaction.details.c.c)var;
            if(var != null) {
               var = (co.uk.getmondo.transaction.details.c.c)var;
               if(var != null) {
                  String var = var.i();
                  l.a(var, "merchant.name");
                  var = new co.uk.getmondo.transaction.details.c.c(var, var.doubleValue(), var.doubleValue());
               }
            }
         }
      }

      return var;
   }

   public co.uk.getmondo.transaction.details.b.c j() {
      u var = this.a.f();
      co.uk.getmondo.transaction.details.b.c var;
      if(var == null) {
         var = (co.uk.getmondo.transaction.details.b.c)(new e(this.a));
      } else {
         var = (co.uk.getmondo.transaction.details.b.c)(new a(var));
      }

      return var;
   }
}
