package co.uk.getmondo.transaction.details.d.d;

import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.f;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/GeneralFooter;", "Lco/uk/getmondo/transaction/details/base/Footer;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "getTransaction", "()Lco/uk/getmondo/model/Transaction;", "transactionDetails", "", "resources", "Landroid/content/res/Resources;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements f {
   private final aj a;

   public b(aj var) {
      l.b(var, "transaction");
      super();
      this.a = var;
   }

   public String a(Resources var) {
      l.b(var, "resources");
      String var = this.a.l();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         String var = j.a(j.b((CharSequence)var).toString(), " +", " ", false, 4, (Object)null);
         var = var;
         if(this.a.k()) {
            var = var.getString(2131362555) + "\n" + var;
         }

         return var;
      }
   }
}
