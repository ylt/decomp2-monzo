package co.uk.getmondo.transaction;

import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.p;
import co.uk.getmondo.payments.send.data.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/transaction/CostSplitter;", "", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "(Lco/uk/getmondo/payments/send/data/UserSettingsRepository;)V", "formatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "splitCost", "Lco/uk/getmondo/transaction/splitting/model/Split;", "transaction", "Lco/uk/getmondo/model/Transaction;", "numberOfPeople", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final co.uk.getmondo.common.i.b a;
   private final h b;

   public a(h var) {
      l.b(var, "userSettingsRepository");
      super();
      this.b = var;
      this.a = new co.uk.getmondo.common.i.b(false, false, false, 7, (i)null);
   }

   public final co.uk.getmondo.transaction.splitting.a.a a(aj var, int var) {
      l.b(var, "transaction");
      p var = this.b.e();
      List var = (List)(new ArrayList());

      for(int var = 0; var < var; ++var) {
         co.uk.getmondo.d.c var = var.g().a(var + 2);
         int var = co.uk.getmondo.transaction.splitting.a.a.c.a;
         String var = "";
         if(var.e() > (double)var.a()) {
            var = co.uk.getmondo.transaction.splitting.a.a.c.c;
            var = var.g() + var.a();
         } else if(var.e() < (double)var.b()) {
            var = co.uk.getmondo.transaction.splitting.a.a.c.b;
            var = var.g() + var.b();
         }

         var.add(new co.uk.getmondo.transaction.splitting.a.a.c(var + 1, this.a.a(var), var, var));
      }

      Collection var = (Collection)var;
      Object[] var = var.toArray(new co.uk.getmondo.transaction.splitting.a.a.c[var.size()]);
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
      } else {
         return new co.uk.getmondo.transaction.splitting.a.a((co.uk.getmondo.transaction.splitting.a.a.c[])var);
      }
   }
}
