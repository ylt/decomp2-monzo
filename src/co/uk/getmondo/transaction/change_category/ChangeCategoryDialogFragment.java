package co.uk.getmondo.transaction.change_category;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.uk.getmondo.d.h;

public class ChangeCategoryDialogFragment extends DialogFragment {
   private Unbinder a;
   private ChangeCategoryDialogFragment.a b;
   private ChangeCategoryDialogFragment.b c;

   public static ChangeCategoryDialogFragment a(String var) {
      ChangeCategoryDialogFragment var = new ChangeCategoryDialogFragment();
      Bundle var = new Bundle();
      var.putString("KEY_TRANSACTION_ID", var);
      var.setArguments(var);
      return var;
   }

   public void a(ChangeCategoryDialogFragment.a var) {
      this.b = var;
   }

   public void a(ChangeCategoryDialogFragment.b var) {
      this.c = var;
   }

   @OnClick({2131821245, 2131821249, 2131821246, 2131821250, 2131821247, 2131821251, 2131821244, 2131821252, 2131821248, 2131821253})
   void onCategoryClicked(ViewGroup var) {
      switch(var.getId()) {
      case 2131821244:
         this.b.a(h.SHOPPING);
         break;
      case 2131821245:
         this.b.a(h.TRANSPORT);
         break;
      case 2131821246:
         this.b.a(h.EATING_OUT);
         break;
      case 2131821247:
         this.b.a(h.BILLS);
         break;
      case 2131821248:
         this.b.a(h.GENERAL);
         break;
      case 2131821249:
         this.b.a(h.GROCERIES);
         break;
      case 2131821250:
         this.b.a(h.CASH);
         break;
      case 2131821251:
         this.b.a(h.ENTERTAINMENT);
         break;
      case 2131821252:
         this.b.a(h.HOLIDAYS);
         break;
      case 2131821253:
         this.b.a(h.EXPENSES);
      }

      this.getDialog().dismiss();
   }

   public Dialog onCreateDialog(Bundle var) {
      View var = LayoutInflater.from(this.getActivity()).inflate(2131034259, (ViewGroup)null, false);
      this.a = ButterKnife.bind(this, (View)var);
      return (new Builder(this.getActivity())).setView(var).create();
   }

   public void onDestroyView() {
      this.a.unbind();
      super.onDestroyView();
   }

   public void onDismiss(DialogInterface var) {
      if(this.c != null) {
         this.c.a();
      }

      super.onDismiss(var);
   }

   public interface a {
      void a(h var);
   }

   public interface b {
      void a();
   }
}
