package co.uk.getmondo.transaction.splitting;

import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final SplitBottomSheetFragment a;
   private final Dialog b;
   private final int c;

   private a(SplitBottomSheetFragment var, Dialog var, int var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static OnClickListener a(SplitBottomSheetFragment var, Dialog var, int var) {
      return new a(var, var, var);
   }

   public void onClick(View var) {
      SplitBottomSheetFragment.a(this.a, this.b, this.c, var);
   }
}
