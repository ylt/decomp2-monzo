package co.uk.getmondo.transaction.splitting;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.d;
import android.support.v7.app.e;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SplitBottomSheetFragment extends d {
   private static final String a = SplitBottomSheetFragment.class.getSimpleName();
   private Unbinder b;
   private co.uk.getmondo.transaction.splitting.a.a c;
   private SplitBottomSheetFragment.a d;
   @BindView(2131821202)
   ViewGroup shareTextViews;

   public static SplitBottomSheetFragment a(co.uk.getmondo.transaction.splitting.a.a var) {
      Bundle var = new Bundle();
      var.putParcelable("view_model", var);
      SplitBottomSheetFragment var = new SplitBottomSheetFragment();
      var.setArguments(var);
      return var;
   }

   public static void a(e var, co.uk.getmondo.transaction.splitting.a.a var, SplitBottomSheetFragment.a var) {
      SplitBottomSheetFragment var = a(var);
      var.a(var);
      var.show(var.getSupportFragmentManager(), a);
   }

   // $FF: synthetic method
   static void a(SplitBottomSheetFragment var, Dialog var, int var, View var) {
      if(var.d != null) {
         var.d.a(var, var);
      }

   }

   public void a(SplitBottomSheetFragment.a var) {
      this.d = var;
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      var = this.getArguments();
      if(var != null && var.containsKey("view_model")) {
         this.c = (co.uk.getmondo.transaction.splitting.a.a)var.getParcelable("view_model");
      }

   }

   @OnClick({2131821207})
   public void onCustomiseAmount() {
      if(this.d != null) {
         this.d.a(this.getDialog());
      }

   }

   public void onDestroyView() {
      super.onDestroyView();
      this.b.unbind();
   }

   public void setupDialog(Dialog var, int var) {
      super.setupDialog(var, var);
      View var = View.inflate(this.getContext(), 2131034237, (ViewGroup)null);
      var.setContentView(var);
      this.b = ButterKnife.bind(this, (View)var);
      if(this.c != null) {
         Resources var = this.getResources();
         co.uk.getmondo.transaction.splitting.a.a.c[] var = this.c.a();

         for(var = 0; var < this.shareTextViews.getChildCount() && var < var.length; ++var) {
            TextView var = (TextView)this.shareTextViews.getChildAt(var);
            co.uk.getmondo.transaction.splitting.a.a.c var = var[var];
            String var;
            if(var.c() == -1) {
               var = this.getString(2131362707, new Object[]{var.d()});
            } else if(var.c() == 1) {
               var = this.getString(2131362703, new Object[]{var.d()});
            } else {
               var = this.getString(2131362699, new Object[]{var.b()});
            }

            int var = var.a();
            var.setText(var.getQuantityString(2131886088, var, new Object[]{Integer.valueOf(var)}) + " " + var);
            boolean var;
            if(var.c() == 0) {
               var = true;
            } else {
               var = false;
            }

            var.setEnabled(var);
            var.setOnClickListener(a.a(this, var, var));
         }
      }

   }

   public interface a {
      void a(Dialog var);

      void a(Dialog var, int var);
   }
}
