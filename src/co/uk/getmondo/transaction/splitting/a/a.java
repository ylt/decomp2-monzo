package co.uk.getmondo.transaction.splitting.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0013\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0002\u0010\nJ\u001e\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0002\u0010\u000eJ\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u0010H\u0016R\u0019\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "shares", "", "Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "([Lco/uk/getmondo/transaction/splitting/model/Split$Share;)V", "getShares", "()[Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "[Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "component1", "copy", "([Lco/uk/getmondo/transaction/splitting/model/Split$Share;)Lco/uk/getmondo/transaction/splitting/model/Split;", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "dest", "flags", "Companion", "Share", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public a a(Parcel var) {
         l.b(var, "source");
         return new a(var);
      }

      public a[] a(int var) {
         return new a[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final a.a a = new a.a((i)null);
   private final a.c[] b;

   public a(Parcel var) {
      l.b(var, "source");
      Object[] var = var.createTypedArray(a.c.CREATOR);
      l.a(var, "source.createTypedArray(Share.CREATOR)");
      this((a.c[])var);
   }

   public a(a.c[] var) {
      l.b(var, "shares");
      super();
      this.b = var;
   }

   public final a.c[] a() {
      return this.b;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label26: {
            if(var instanceof a) {
               a var = (a)var;
               if(l.a(this.b, var.b)) {
                  break label26;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      a.c[] var = this.b;
      int var;
      if(var != null) {
         var = Arrays.hashCode(var);
      } else {
         var = 0;
      }

      return var;
   }

   public String toString() {
      return "Split(shares=" + Arrays.toString(this.b) + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeTypedArray((Parcelable[])this.b, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/transaction/splitting/model/Split;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B%\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\b¢\u0006\u0002\u0010\u000bJ\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0015\u001a\u00020\bHÆ\u0003J1\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u00062\b\b\u0002\u0010\n\u001a\u00020\bHÆ\u0001J\b\u0010\u0017\u001a\u00020\u0006H\u0016J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u0006HÖ\u0001J\t\u0010\u001d\u001a\u00020\bHÖ\u0001J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u0006H\u0016R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000f¨\u0006#"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "number", "", "each", "", "limit", "limitValue", "(ILjava/lang/String;ILjava/lang/String;)V", "getEach", "()Ljava/lang/String;", "getLimit", "()I", "getLimitValue", "getNumber", "component1", "component2", "component3", "component4", "copy", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c implements Parcelable {
      public static final Creator CREATOR = (Creator)(new Creator() {
         public a.c a(Parcel var) {
            l.b(var, "source");
            return new a.c(var);
         }

         public a.c[] a(int var) {
            return new a.c[var];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var) {
            return this.a(var);
         }

         // $FF: synthetic method
         public Object[] newArray(int var) {
            return (Object[])this.a(var);
         }
      });
      public static final int a = 0;
      public static final int b = -1;
      public static final int c = 1;
      public static final a.a d = new a.a((i)null);
      private final int e;
      private final String f;
      private final int g;
      private final String h;

      public c(int var, String var, int var, String var) {
         l.b(var, "each");
         l.b(var, "limitValue");
         super();
         this.e = var;
         this.f = var;
         this.g = var;
         this.h = var;
      }

      public c(Parcel var) {
         l.b(var, "source");
         int var = var.readInt();
         String var = var.readString();
         l.a(var, "source.readString()");
         int var = var.readInt();
         String var = var.readString();
         l.a(var, "source.readString()");
         this(var, var, var, var);
      }

      public final int a() {
         return this.e;
      }

      public final String b() {
         return this.f;
      }

      public final int c() {
         return this.g;
      }

      public final String d() {
         return this.h;
      }

      public int describeContents() {
         return 0;
      }

      public boolean equals(Object var) {
         boolean var = false;
         boolean var;
         if(this != var) {
            var = var;
            if(!(var instanceof a.c)) {
               return var;
            }

            a.c var = (a.c)var;
            boolean var;
            if(this.e == var.e) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }

            var = var;
            if(!l.a(this.f, var.f)) {
               return var;
            }

            if(this.g == var.g) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }

            var = var;
            if(!l.a(this.h, var.h)) {
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = 0;
         int var = this.e;
         String var = this.f;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         int var = this.g;
         var = this.h;
         if(var != null) {
            var = var.hashCode();
         }

         return ((var + var * 31) * 31 + var) * 31 + var;
      }

      public String toString() {
         return "Share(number=" + this.e + ", each=" + this.f + ", limit=" + this.g + ", limitValue=" + this.h + ")";
      }

      public void writeToParcel(Parcel var, int var) {
         l.b(var, "dest");
         var.writeInt(this.e);
         var.writeString(this.f);
         var.writeInt(this.g);
         var.writeString(this.h);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Share$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "OVER_LIMIT", "", "UNDER_LIMIT", "WITHIN_LIMIT", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
