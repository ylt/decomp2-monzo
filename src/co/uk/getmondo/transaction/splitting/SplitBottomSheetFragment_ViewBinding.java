package co.uk.getmondo.transaction.splitting;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SplitBottomSheetFragment_ViewBinding implements Unbinder {
   private SplitBottomSheetFragment a;
   private View b;

   public SplitBottomSheetFragment_ViewBinding(final SplitBottomSheetFragment var, View var) {
      this.a = var;
      var.shareTextViews = (ViewGroup)Utils.findRequiredViewAsType(var, 2131821202, "field 'shareTextViews'", ViewGroup.class);
      var = Utils.findRequiredView(var, 2131821207, "method 'onCustomiseAmount'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCustomiseAmount();
         }
      });
   }

   public void unbind() {
      SplitBottomSheetFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.shareTextViews = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
