package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.ApiAttachmentResponse;
import co.uk.getmondo.api.model.ApiTransactionResponse;
import co.uk.getmondo.api.model.identity_verification.ApiUploadContainer;
import co.uk.getmondo.d.aj;
import io.realm.av;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class a {
   private final MonzoApi a;
   private final j b;
   private final ae c;
   private final co.uk.getmondo.d.a.b d = new co.uk.getmondo.d.a.b();

   public a(MonzoApi var, j var, ae var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.d a(a var, ApiAttachmentResponse var) throws Exception {
      return var.d.a(var.a());
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var, String var, co.uk.getmondo.d.d var) throws Exception {
      return var.b.a(var, var);
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var, String var, String var, ApiTransactionResponse var) throws Exception {
      return var.b.a(var, var);
   }

   private io.reactivex.v a() {
      return io.reactivex.v.a(this.b.a(), this.b.b(), new a.c());
   }

   private io.reactivex.v a(co.uk.getmondo.d.aa var) {
      return this.b.d(var.a()).d((io.reactivex.c.h)(new a.b()));
   }

   private io.reactivex.v a(co.uk.getmondo.payments.send.data.a.a var) {
      return this.b.b(var.b(), var.c()).d((io.reactivex.c.h)(new a.b()));
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, InputStream var, long var, ApiUploadContainer var) throws Exception {
      return var.c.a(var.b(), var, var, "image/jpeg").a((Object)var);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var, String var, String var, ApiUploadContainer var) throws Exception {
      return var.a.registerAttachment(var, var, var.a());
   }

   // $FF: synthetic method
   static void a(String var, co.uk.getmondo.d.h var, av var) {
      ((aj)var.a(aj.class).a("id", var).h()).a(var.f());
   }

   // $FF: synthetic method
   static void b(String var, co.uk.getmondo.d.h var) throws Exception {
      av var = av.n();
      Object var = null;

      label126: {
         Throwable var;
         try {
            var.a(h.a(var, var));
            break label126;
         } catch (Throwable var) {
            var = var;
         } finally {
            ;
         }

         try {
            throw var;
         } catch (Throwable var) {
            if(var != null) {
               if(var != null) {
                  try {
                     var.close();
                  } catch (Throwable var) {
                     ;
                  }
               } else {
                  var.close();
               }
            }

            throw var;
         }
      }

      if(var != null) {
         if(false) {
            try {
               var.close();
            } catch (Throwable var) {
               ;
            }
         } else {
            var.close();
         }
      }

   }

   private io.reactivex.v d(String var) {
      return io.reactivex.v.a(this.b.e(var), this.b.f(var), new a.a());
   }

   private io.reactivex.v e(String var) {
      return io.reactivex.v.a(this.b.g(var), this.b.h(var), new a.c());
   }

   public aj a(String var) {
      return this.b.a(var);
   }

   public io.reactivex.b a(String var, co.uk.getmondo.d.h var) {
      return this.a.updateTransactionCategory(var, var.f()).b((io.reactivex.d)io.reactivex.b.a(b.a(var, var)));
   }

   public io.reactivex.b a(String var, InputStream var, long var, String var) {
      return this.a.createAttachmentUploadUrl(var, UUID.randomUUID().toString(), "image/jpeg").a(c.a(this, var, var)).a(d.a(this, var, var)).d(e.a(this)).c(f.a(this, var));
   }

   public io.reactivex.b a(String var, String var) {
      return this.a.updateNotes(var, var).c(g.a(this, var, var));
   }

   public io.reactivex.v a(aj var) {
      io.reactivex.v var;
      if(var.z()) {
         var = this.a().d((io.reactivex.c.h)(new a.d()));
      } else if(var.r()) {
         var = this.a(var.C());
      } else if(var.u()) {
         var = this.d(var.G());
      } else if(var.q()) {
         var = this.a(var.B());
      } else if(var.f() != null) {
         var = this.e(var.f().h()).d((io.reactivex.c.h)(new a.d()));
      } else {
         var = io.reactivex.v.a((Object)co.uk.getmondo.transaction.details.c.d.a);
      }

      return var;
   }

   public io.reactivex.n b(String var) {
      return this.b.b(var);
   }

   public io.reactivex.b c(String var) {
      return this.a.deregisterAttachment(var).b((io.reactivex.d)this.b.c(var));
   }

   private static class a implements io.reactivex.c.c {
      private a() {
      }

      // $FF: synthetic method
      a(Object var) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.a a(Long var, Long var) throws Exception {
         co.uk.getmondo.transaction.details.c.a var;
         if(var.longValue() == 0L) {
            var = new co.uk.getmondo.transaction.details.c.a(0L, new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a));
         } else {
            long var = var.longValue() / var.longValue();
            var = new co.uk.getmondo.transaction.details.c.a(var.longValue(), new co.uk.getmondo.d.c(var, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(var.longValue(), co.uk.getmondo.common.i.c.a));
         }

         return var;
      }
   }

   private static class b implements io.reactivex.c.h {
      private b() {
      }

      // $FF: synthetic method
      b(Object var) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.b a(List var) throws Exception {
         co.uk.getmondo.common.i.a var = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
         co.uk.getmondo.common.i.a var = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
         Iterator var = var.iterator();

         while(var.hasNext()) {
            aj var = (aj)var.next();
            if(var.g().a()) {
               var.a(var.g());
            } else {
               var.a(var.g());
            }
         }

         return new co.uk.getmondo.transaction.details.c.b((long)var.b(), var.a(), (long)var.b(), var.a());
      }
   }

   private static class c implements io.reactivex.c.c {
      private c() {
      }

      // $FF: synthetic method
      c(Object var) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.e a(Long var, Long var) throws Exception {
         co.uk.getmondo.transaction.details.c.e var;
         if(var.longValue() == 0L) {
            var = new co.uk.getmondo.transaction.details.c.e(new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), 0L);
         } else {
            long var = var.longValue() / var.longValue();
            var = new co.uk.getmondo.transaction.details.c.e(new co.uk.getmondo.d.c(var.longValue(), co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(var, co.uk.getmondo.common.i.c.a), var.longValue());
         }

         return var;
      }
   }

   private static class d implements io.reactivex.c.h {
      private d() {
      }

      // $FF: synthetic method
      d(Object var) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.f a(co.uk.getmondo.transaction.details.c.e var) throws Exception {
         return new co.uk.getmondo.transaction.details.c.a(var.c(), var.b(), var.a());
      }
   }
}
