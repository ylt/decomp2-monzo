package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.model.ApiTransactionResponse;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final a a;
   private final String b;
   private final String c;

   private g(a var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(a var, String var, String var) {
      return new g(var, var, var);
   }

   public Object a(Object var) {
      return a.a(this.a, this.b, this.c, (ApiTransactionResponse)var);
   }
}
