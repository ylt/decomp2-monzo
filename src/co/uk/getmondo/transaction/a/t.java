package co.uk.getmondo.transaction.a;

import java.util.Date;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class t implements Callable {
   private final Date a;
   private final Date b;

   private t(Date var, Date var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(Date var, Date var) {
      return new t(var, var);
   }

   public Object call() {
      return j.a(this.a, this.b);
   }
}
