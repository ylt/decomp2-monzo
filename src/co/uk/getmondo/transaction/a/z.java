package co.uk.getmondo.transaction.a;

import io.realm.av;

// $FF: synthetic class
final class z implements kotlin.d.a.b {
   private final String a;
   private final String b;

   private z(String var, String var) {
      this.a = var;
      this.b = var;
   }

   public static kotlin.d.a.b a(String var, String var) {
      return new z(var, var);
   }

   public Object a(Object var) {
      return j.a(this.a, this.b, (av)var);
   }
}
