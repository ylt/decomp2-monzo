package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.ae;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new i(var, var, var);
   }

   public a a() {
      return new a((MonzoApi)this.b.b(), (j)this.c.b(), (ae)this.d.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
