package co.uk.getmondo.transaction.a;

import co.uk.getmondo.d.aj;
import io.realm.av;
import io.realm.bb;
import io.realm.bc;
import io.realm.bg;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

public class j {
   // $FF: synthetic method
   static bg a(String var, av var) {
      return var.a(aj.class).a("peer.userId", var).g();
   }

   // $FF: synthetic method
   static bg a(String var, String var, av var) {
      return var.a(aj.class).b("bankDetails").a("bankDetails.sortCode", var).a("bankDetails.accountNumber", var).g();
   }

   // $FF: synthetic method
   static List a(Date param0, Date param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static void a(co.uk.getmondo.d.d var, String var, av var) {
      var = (co.uk.getmondo.d.d)var.b((bb)var);
      ((aj)var.a(aj.class).a("id", var).h()).A().a((bb)var);
   }

   // $FF: synthetic method
   static void b(String var, av var) {
      co.uk.getmondo.d.d var = (co.uk.getmondo.d.d)var.a(co.uk.getmondo.d.d.class).a("id", var).h();
      if(bc.b(var)) {
         bc.a(var);
      }

   }

   // $FF: synthetic method
   static void b(String var, String var, av var) {
      ((aj)var.a(aj.class).a("id", var).h()).b(var);
   }

   // $FF: synthetic method
   static bg c(String var, av var) {
      return var.a(aj.class).a("id", var).g();
   }

   // $FF: synthetic method
   static aj d() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long e() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long f() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long i(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long j(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long k(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long l(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   aj a(String param1) {
      // $FF: Couldn't be decompiled
   }

   io.reactivex.b a(co.uk.getmondo.d.d var, String var) {
      return co.uk.getmondo.common.j.g.a(w.a(var, var));
   }

   io.reactivex.b a(String var, String var) {
      return co.uk.getmondo.common.j.g.a(y.a(var, var));
   }

   io.reactivex.v a() {
      return io.reactivex.v.c(o.a());
   }

   public io.reactivex.v a(LocalDateTime var, LocalDateTime var) {
      return io.reactivex.v.c(t.a(DateTimeUtils.a(var.a(ZoneId.a()).j()), DateTimeUtils.a(var.a(ZoneId.a()).j())));
   }

   public io.reactivex.n b(String var) {
      return co.uk.getmondo.common.j.g.a(k.a(var)).filter(u.a()).map(v.a());
   }

   io.reactivex.v b() {
      return io.reactivex.v.c(p.a());
   }

   io.reactivex.v b(String var, String var) {
      return co.uk.getmondo.common.j.g.a(z.a(var, var)).map(aa.a()).first(Collections.emptyList());
   }

   io.reactivex.b c(String var) {
      return co.uk.getmondo.common.j.g.a(x.a(var));
   }

   public io.reactivex.v c() {
      return io.reactivex.v.c(s.a());
   }

   io.reactivex.v d(String var) {
      return co.uk.getmondo.common.j.g.a(ab.a(var)).map(l.a()).first(Collections.emptyList());
   }

   io.reactivex.v e(String var) {
      return io.reactivex.v.c(m.a(var));
   }

   io.reactivex.v f(String var) {
      return io.reactivex.v.c(n.a(var));
   }

   io.reactivex.v g(String var) {
      return io.reactivex.v.c(q.a(var));
   }

   io.reactivex.v h(String var) {
      return io.reactivex.v.c(r.a(var));
   }
}
