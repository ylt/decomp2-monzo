package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.model.identity_verification.ApiUploadContainer;
import java.io.InputStream;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final a a;
   private final InputStream b;
   private final long c;

   private c(a var, InputStream var, long var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(a var, InputStream var, long var) {
      return new c(var, var, var);
   }

   public Object a(Object var) {
      return a.a(this.a, this.b, this.c, (ApiUploadContainer)var);
   }
}
