package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.model.identity_verification.ApiUploadContainer;

// $FF: synthetic class
final class d implements io.reactivex.c.h {
   private final a a;
   private final String b;
   private final String c;

   private d(a var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(a var, String var, String var) {
      return new d(var, var, var);
   }

   public Object a(Object var) {
      return a.a(this.a, this.b, this.c, (ApiUploadContainer)var);
   }
}
