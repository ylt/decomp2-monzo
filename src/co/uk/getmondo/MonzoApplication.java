package co.uk.getmondo;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import co.uk.getmondo.common.f;
import co.uk.getmondo.common.g;
import co.uk.getmondo.common.m;
import co.uk.getmondo.common.q;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.common.h.a.o;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.h;
import io.realm.av;
import io.realm.ay;

public class MonzoApplication extends android.support.e.b {
   d a;
   co.uk.getmondo.b.a b;
   co.uk.getmondo.common.a.c c;
   q d;
   m e;
   g f;
   co.uk.getmondo.adjust.a g;
   private final co.uk.getmondo.common.h.a.a h = this.a();

   public static MonzoApplication a(Context var) {
      Application var;
      if(var instanceof Activity) {
         var = ((Activity)var).getApplication();
      } else if(var instanceof Service) {
         var = ((Service)var).getApplication();
      } else {
         var = (Application)var.getApplicationContext();
      }

      return (MonzoApplication)var;
   }

   protected co.uk.getmondo.common.h.a.a a() {
      return o.Q().a(new co.uk.getmondo.common.h.a.b(this)).a();
   }

   public co.uk.getmondo.common.h.a.a b() {
      return this.h;
   }

   public void onCreate() {
      super.onCreate();
      this.b().a(this);
      io.fabric.sdk.android.c.a((Context)this, (h[])(new h[]{new Crashlytics()}));
      com.squareup.a.a.a(this);
      com.b.c.a.a((Application)this);
      av.a((Context)this);
      d.a.a.a((d.a.a.b)(new f()));
      ak var = this.a.b();
      if(var != null) {
         String var;
         if(var.d() != null) {
            var = var.d().b();
         } else {
            var = var.b();
         }

         if(p.c(var)) {
            this.f.a(var);
            this.d.a(var);
         }
      }

      this.b.a(this);
      this.registerActivityLifecycleCallbacks(this.b().i());
      this.g.a();
      av.b((new ay.a()).a().b());
      this.c.a();
      android.support.text.emoji.a.a(new android.support.text.emoji.a.a(this.getApplicationContext()));
   }

   public void onTerminate() {
      this.unregisterActivityLifecycleCallbacks(this.b().i());
      super.onTerminate();
   }
}
