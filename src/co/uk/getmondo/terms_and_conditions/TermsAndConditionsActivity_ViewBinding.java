package co.uk.getmondo.terms_and_conditions;

import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class TermsAndConditionsActivity_ViewBinding implements Unbinder {
   private TermsAndConditionsActivity a;

   public TermsAndConditionsActivity_ViewBinding(TermsAndConditionsActivity var, View var) {
      this.a = var;
      var.termsWebView = (WebView)Utils.findRequiredViewAsType(var, 2131821115, "field 'termsWebView'", WebView.class);
      var.nextPageButton = (Button)Utils.findRequiredViewAsType(var, 2131821116, "field 'nextPageButton'", Button.class);
   }

   public void unbind() {
      TermsAndConditionsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.termsWebView = null;
         var.nextPageButton = null;
      }
   }
}
