package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;

// $FF: synthetic class
final class b implements ValueCallback {
   private final TermsAndConditionsActivity a;

   private b(TermsAndConditionsActivity var) {
      this.a = var;
   }

   public static ValueCallback a(TermsAndConditionsActivity var) {
      return new b(var);
   }

   public void onReceiveValue(Object var) {
      TermsAndConditionsActivity.a(this.a, (String)var);
   }
}
