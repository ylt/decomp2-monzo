package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;
import io.reactivex.o;

// $FF: synthetic class
final class e implements ValueCallback {
   private final TermsAndConditionsActivity a;
   private final o b;

   private e(TermsAndConditionsActivity var, o var) {
      this.a = var;
      this.b = var;
   }

   public static ValueCallback a(TermsAndConditionsActivity var, o var) {
      return new e(var, var);
   }

   public void onReceiveValue(Object var) {
      TermsAndConditionsActivity.a(this.a, this.b, (String)var);
   }
}
