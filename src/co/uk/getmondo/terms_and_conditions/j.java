package co.uk.getmondo.terms_and_conditions;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(b.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(b.a var) {
      return new j(var);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
