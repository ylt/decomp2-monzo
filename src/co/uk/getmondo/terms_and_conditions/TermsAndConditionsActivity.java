package co.uk.getmondo.terms_and_conditions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;

public class TermsAndConditionsActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131821116)
   Button nextPageButton;
   @BindView(2131821115)
   WebView termsWebView;

   public static Intent a(Context var) {
      return new Intent(var, TermsAndConditionsActivity.class);
   }

   public static Intent a(Context var, String var) {
      Intent var = new Intent(var, TermsAndConditionsActivity.class);
      var.putExtra("EXTRA_OVERRIDE_URL", var);
      return var;
   }

   // $FF: synthetic method
   static r a(TermsAndConditionsActivity var, Object var) throws Exception {
      return n.create(d.a(var));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var, o var) throws Exception {
      var.termsWebView.evaluateJavascript("window.isLastArticle()", e.a(var, var));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var, o var, String var) {
      var.a(Boolean.valueOf(var.a(var)));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var, String var) {
      var.termsWebView.evaluateJavascript("window.isLastArticle()", c.a(var));
   }

   private boolean a(String var) {
      return var.equals("true");
   }

   // $FF: synthetic method
   static void b(TermsAndConditionsActivity var, String var) {
      if(var.a(var)) {
         var.nextPageButton.setText(2131362761);
      }

   }

   public n a() {
      return com.b.a.c.c.a(this.nextPageButton).flatMap(a.a(this));
   }

   public void b() {
      this.termsWebView.evaluateJavascript("window.nextArticle()", b.a(this));
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034216);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.getSupportActionBar().b(false);
      this.termsWebView.getSettings().setJavaScriptEnabled(true);
      this.termsWebView.setWebViewClient(new WebViewClient() {
         public boolean shouldOverrideUrlLoading(WebView var, String var) {
            Uri var = Uri.parse(var);
            (new android.support.b.a.a()).a(android.support.v4.content.a.c(TermsAndConditionsActivity.this, 2131689487)).a().a(TermsAndConditionsActivity.this, var);
            return true;
         }
      });
      String var = "https://monzo.com/-webviews/terms-and-conditions/";
      if(this.getIntent().hasExtra("EXTRA_OVERRIDE_URL")) {
         var = this.getIntent().getStringExtra("EXTRA_OVERRIDE_URL");
      }

      this.termsWebView.loadUrl(var);
      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
