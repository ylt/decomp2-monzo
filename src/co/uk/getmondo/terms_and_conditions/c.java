package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;

// $FF: synthetic class
final class c implements ValueCallback {
   private final TermsAndConditionsActivity a;

   private c(TermsAndConditionsActivity var) {
      this.a = var;
   }

   public static ValueCallback a(TermsAndConditionsActivity var) {
      return new c(var);
   }

   public void onReceiveValue(Object var) {
      TermsAndConditionsActivity.b(this.a, (String)var);
   }
}
