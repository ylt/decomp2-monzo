package co.uk.getmondo.settings;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class OpenSourceLicensesActivity extends co.uk.getmondo.common.activities.b {
   private static final AtomicInteger a = new AtomicInteger(0);
   @BindView(2131821005)
   AppBarLayout appBarLayout;
   @BindView(2131821006)
   CollapsingToolbarLayout collapsingToolbarLayout;
   @BindView(2131821007)
   TextView descriptionTextView;
   @BindView(2131821008)
   RecyclerView recyclerView;

   public static void a(Context var) {
      var.startActivity(new Intent(var, OpenSourceLicensesActivity.class));
   }

   // $FF: synthetic method
   static void a(OpenSourceLicensesActivity var, AppBarLayout var, int var) {
      float var = Math.abs((float)var / (float)var.getTotalScrollRange());
      var.descriptionTextView.setAlpha(1.0F - 1.5F * var);
      var = android.support.v4.content.a.c(var, 17170445);
      int var = android.support.v4.content.a.c(var, 2131689706);
      Object var = (new ArgbEvaluator()).evaluate(var, Integer.valueOf(var), Integer.valueOf(var));
      var.collapsingToolbarLayout.setCollapsedTitleTextColor(((Integer)var).intValue());
      var.collapsingToolbarLayout.setExpandedTitleColor(((Integer)var).intValue());
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034190);
      ButterKnife.bind((Activity)this);
      this.descriptionTextView.setText(this.getString(2131362684, new Object[]{co.uk.getmondo.common.k.e.b()}));
      OpenSourceLicensesActivity.d var = new OpenSourceLicensesActivity.d();
      this.recyclerView.setHasFixedSize(true);
      this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
      this.recyclerView.setAdapter(var);
      this.recyclerView.a(new a.a.a.a.a.b(var));
      this.appBarLayout.a(r.a(this));
   }

   static class HeaderViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821599)
      TextView titleTextView;

      HeaderViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      public void a(OpenSourceLicensesActivity.c var) {
         this.titleTextView.setText(var.c.z);
         this.titleTextView.setVisibility(0);
      }
   }

   static class OpenSourceLibraryHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821605)
      TextView libraryLicenseTextView;
      @BindView(2131821604)
      TextView libraryNameTextView;

      private OpenSourceLibraryHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      // $FF: synthetic method
      OpenSourceLibraryHolder(View var, Object var) {
         this(var);
      }

      public void a(OpenSourceLicensesActivity.c var) {
         this.libraryNameTextView.setText(var.a);
         this.libraryLicenseTextView.setText(var.b.e);
      }
   }

   private static enum a {
      a("Amulya Khare"),
      b("Android"),
      c("Artem Zinnatullin"),
      d("Chris Banes"),
      e("Eduardo Barrenechea"),
      f("Esko Luontola and contributors"),
      g("Facebook"),
      h("Google"),
      i("Jake Wharton"),
      j("Airbnb"),
      k("memoizr"),
      l("Michael Rozumyanskiy"),
      m("Monzo"),
      n("ReactiveX"),
      o("relex"),
      p("Square"),
      q("Stripe"),
      r("sannies"),
      s("JUnit"),
      t("AssertJ contributors"),
      u("Mockito contributors"),
      v("LinkedIn"),
      w("Realm"),
      x("JodaOrg");

      private final int y;
      private final String z;

      private a(String var) {
         this.y = OpenSourceLicensesActivity.a.getAndIncrement();
         this.z = var;
      }
   }

   private static enum b {
      a("Apache 2.0"),
      b("MIT"),
      c("Facebook Platform License"),
      d("Eclipse Public License 1.0");

      private final String e;

      private b(String var) {
         this.e = var;
      }
   }

   private static class c {
      private final String a;
      private final OpenSourceLicensesActivity.b b;
      private final OpenSourceLicensesActivity.a c;

      c(String var, OpenSourceLicensesActivity.b var, OpenSourceLicensesActivity.a var) {
         this.a = var;
         this.b = var;
         this.c = var;
      }
   }

   private static class d extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
      private final List a;

      d() {
         this.a = Arrays.asList(new OpenSourceLicensesActivity.c[]{new OpenSourceLicensesActivity.c("AppCompat", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("AOSP", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("CardView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Constraint Layout", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Custom Tabs", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Design", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("ExifInterface", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("MultiDex", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("RecyclerView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("ButterKnife", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("Glide", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("PhotoView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.d), new OpenSourceLicensesActivity.c("CircleIndicator", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.o), new OpenSourceLicensesActivity.c("TextDrawable", OpenSourceLicensesActivity.b.b, OpenSourceLicensesActivity.a.a), new OpenSourceLicensesActivity.c("power-optional", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.k), new OpenSourceLicensesActivity.c("header-decor", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.e), new OpenSourceLicensesActivity.c("Gson", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("Retrofit 2", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Retrofit 2 Converter (Gson)", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Retrofit 2 Adapter RxJava", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp Logging Interceptor", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp MockWebServer", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Okio", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Leak Canary", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Facebook SDK", OpenSourceLicensesActivity.b.c, OpenSourceLicensesActivity.a.g), new OpenSourceLicensesActivity.c("Stripe", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.q), new OpenSourceLicensesActivity.c("libphonenumber", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.l), new OpenSourceLicensesActivity.c("Retrolambda", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.f), new OpenSourceLicensesActivity.c("RxJava", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.n), new OpenSourceLicensesActivity.c("RxAndroid", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.n), new OpenSourceLicensesActivity.c("RxJava Proguard Rules", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.c), new OpenSourceLicensesActivity.c("Timber", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("RxBinding", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("ThreeTenAbp", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("Dagger 2", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("cameraview", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.m), new OpenSourceLicensesActivity.c("mp4parser", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.r), new OpenSourceLicensesActivity.c("JUnit", OpenSourceLicensesActivity.b.d, OpenSourceLicensesActivity.a.s), new OpenSourceLicensesActivity.c("AssertJ", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.t), new OpenSourceLicensesActivity.c("Espresso", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("Dexmaker", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.v), new OpenSourceLicensesActivity.c("Realm Java", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.w), new OpenSourceLicensesActivity.c("Joda Money", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.x), new OpenSourceLicensesActivity.c("Lottie", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.j)});
         Collections.sort(this.a, s.a());
      }

      // $FF: synthetic method
      static int a(OpenSourceLicensesActivity.c var, OpenSourceLicensesActivity.c var) {
         int var;
         if(var.c.y == var.c.y) {
            var = var.a.compareTo(var.a);
         } else {
            var = var.c.z.compareTo(var.c.z);
         }

         return var;
      }

      public long a(int var) {
         return (long)((OpenSourceLicensesActivity.c)this.a.get(var)).c.y;
      }

      // $FF: synthetic method
      public android.support.v7.widget.RecyclerView.w a(ViewGroup var) {
         return this.b(var);
      }

      public OpenSourceLicensesActivity.OpenSourceLibraryHolder a(ViewGroup var, int var) {
         return new OpenSourceLicensesActivity.OpenSourceLibraryHolder(LayoutInflater.from(var.getContext()).inflate(2131034374, var, false));
      }

      public void a(OpenSourceLicensesActivity.HeaderViewHolder var, int var) {
         var.a((OpenSourceLicensesActivity.c)this.a.get(var));
      }

      public void a(OpenSourceLicensesActivity.OpenSourceLibraryHolder var, int var) {
         var.a((OpenSourceLicensesActivity.c)this.a.get(var));
      }

      public OpenSourceLicensesActivity.HeaderViewHolder b(ViewGroup var) {
         return new OpenSourceLicensesActivity.HeaderViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034371, var, false));
      }

      public int getItemCount() {
         return this.a.size();
      }

      // $FF: synthetic method
      public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var, int var) {
         this.a((OpenSourceLicensesActivity.OpenSourceLibraryHolder)var, var);
      }

      // $FF: synthetic method
      public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var, int var) {
         return this.a(var, var);
      }
   }
}
