package co.uk.getmondo.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import java.util.Set;

public class k {
   private SharedPreferences a;

   public k(Context var) {
      this.a = var.getSharedPreferences("local_user_settings", 0);
   }

   // $FF: synthetic method
   static void a(k var, OnSharedPreferenceChangeListener var) throws Exception {
      var.a.unregisterOnSharedPreferenceChangeListener(var);
   }

   // $FF: synthetic method
   static void a(k var, io.reactivex.o var) throws Exception {
      OnSharedPreferenceChangeListener var = m.a(var);
      var.a.registerOnSharedPreferenceChangeListener(var);
      var.a(n.a(var, var));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var, SharedPreferences var, String var) {
      if(var.equals("KEY_NOTIFICATIONS_ENABLED")) {
         var.a(Boolean.valueOf(var.getBoolean(var, true)));
      }

   }

   void a(boolean var) {
      this.a.edit().putBoolean("KEY_NOTIFICATIONS_ENABLED", var).apply();
   }

   public boolean a() {
      return this.a.getBoolean("KEY_NOTIFICATIONS_ENABLED", true);
   }

   public boolean a(String var) {
      Set var = this.a.getStringSet("KEY_SEEN_NEWS_ITEM", (Set)null);
      boolean var;
      if(var != null && var.contains(var)) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   io.reactivex.n b() {
      return io.reactivex.n.create(l.a(this));
   }

   public void b(String var) {
      android.support.v4.g.b var = new android.support.v4.g.b(this.a.getStringSet("KEY_SEEN_NEWS_ITEM", new android.support.v4.g.b()));
      var.add(var);
      this.a.edit().putStringSet("KEY_SEEN_NEWS_ITEM", var).apply();
   }

   public void b(boolean var) {
      this.a.edit().putBoolean("MONZOME_ONBOARDING_KEY", var).apply();
   }

   public void c(boolean var) {
      this.a.edit().putBoolean("KEY_CARD_ON_ITS_WAY", var).apply();
   }

   public boolean c() {
      return this.a.getBoolean("MONZOME_ONBOARDING_KEY", false);
   }

   public boolean d() {
      return this.a.getBoolean("KEY_CARD_ON_ITS_WAY", false);
   }

   public void e() {
      this.a.edit().clear().apply();
   }
}
