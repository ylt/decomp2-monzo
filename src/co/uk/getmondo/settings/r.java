package co.uk.getmondo.settings;

import android.support.design.widget.AppBarLayout;

// $FF: synthetic class
final class r implements android.support.design.widget.AppBarLayout.b {
   private final OpenSourceLicensesActivity a;

   private r(OpenSourceLicensesActivity var) {
      this.a = var;
   }

   public static android.support.design.widget.AppBarLayout.b a(OpenSourceLicensesActivity var) {
      return new r(var);
   }

   public void onOffsetChanged(AppBarLayout var, int var) {
      OpenSourceLicensesActivity.a(this.a, var, var);
   }
}
