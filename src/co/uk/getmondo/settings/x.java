package co.uk.getmondo.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/settings/UpdateAddressDialogFragment;", "Landroid/support/v4/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class x extends android.support.v4.app.i {
   public static final x.a a = new x.a((kotlin.d.b.i)null);
   private HashMap b;

   public void a() {
      if(this.b != null) {
         this.b.clear();
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      final android.support.v4.app.j var = this.getActivity();
      AlertDialog var = (new Builder((Context)var, 2131493136)).setTitle(2131362696).setMessage(2131362695).setPositiveButton((CharSequence)this.getString(2131362666), (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int varx) {
            co.uk.getmondo.common.h.a.a var = MonzoApplication.a((Context)var).b();
            var.p().a();
            var.h().a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.ADDRESS_CHANGE, (String)null, (String)null, 6, (Object)null));
         }
      })).setNegativeButton((CharSequence)this.getString(2131362499), (OnClickListener)null).create();
      kotlin.d.b.l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.a();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/settings/UpdateAddressDialogFragment$Companion;", "", "()V", "newInstance", "Lco/uk/getmondo/settings/UpdateAddressDialogFragment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final x a() {
         return new x();
      }
   }
}
