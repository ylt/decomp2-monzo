package co.uk.getmondo.settings;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.PaymentLimitsApi;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new j(var, var, var, var, var, var, var, var, var);
   }

   public h a() {
      return (h)b.a.c.a(this.b, new h((io.reactivex.u)this.c.b(), (io.reactivex.u)this.d.b(), (co.uk.getmondo.common.accounts.d)this.e.b(), (MonzoApi)this.f.b(), (PaymentLimitsApi)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (co.uk.getmondo.common.e.a)this.i.b(), (co.uk.getmondo.signup.identity_verification.a.f)this.j.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
