package co.uk.getmondo.settings;

import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

// $FF: synthetic class
final class n implements io.reactivex.c.f {
   private final k a;
   private final OnSharedPreferenceChangeListener b;

   private n(k var, OnSharedPreferenceChangeListener var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.f a(k var, OnSharedPreferenceChangeListener var) {
      return new n(var, var);
   }

   public void a() {
      k.a(this.a, this.b);
   }
}
