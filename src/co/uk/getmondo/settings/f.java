package co.uk.getmondo.settings;

import android.content.res.Resources;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import co.uk.getmondo.api.model.VerificationType;
import co.uk.getmondo.d.y;
import co.uk.getmondo.d.z;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.ab;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\b\u0012\u0004\u0012\u00020\u00040\u0003:\u0005,-./0B\u0019\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u0012\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0014H\u0016J \u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u001e\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020!H\u0016J\u0018\u0010\"\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u0010#\u001a\u00020\u0014H\u0016J\u0016\u0010$\u001a\u00020\b2\u0006\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020(J\u0016\u0010)\u001a\u00020\b2\u0006\u0010*\u001a\u00020+2\u0006\u0010'\u001a\u00020(R\u0018\u0010\n\u001a\f\u0012\b\u0012\u00060\fR\u00020\u00000\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00061"},
   d2 = {"Lco/uk/getmondo/settings/LimitsAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/settings/LimitsAdapter$LimitItemHolder;", "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;", "Lco/uk/getmondo/settings/LimitsAdapter$HeaderViewHolder;", "onLimitDetailsClicked", "Lkotlin/Function1;", "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "", "(Lkotlin/jvm/functions/Function1;)V", "limits", "", "Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;", "amountToString", "", "amount", "Lco/uk/getmondo/model/Amount;", "getHeaderId", "", "position", "", "getItemCount", "makeSubstringClickable", "Landroid/text/SpannableString;", "spannable", "substring", "span", "Landroid/text/style/ClickableSpan;", "onBindHeaderViewHolder", "holder", "onBindViewHolder", "onCreateHeaderViewHolder", "parent", "Landroid/view/ViewGroup;", "onCreateViewHolder", "viewType", "setBalanceLimitPrepaid", "limit", "Lco/uk/getmondo/model/BalanceLimit;", "resources", "Landroid/content/res/Resources;", "setPaymentLimits", "paymentLimits", "Lco/uk/getmondo/model/PaymentLimits;", "HeaderViewHolder", "LimitDetails", "LimitItem", "LimitItemHolder", "NoUnderlineClickableSpan", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
   private final List a;
   private final kotlin.d.a.b b;

   public f(kotlin.d.a.b var) {
      kotlin.d.b.l.b(var, "onLimitDetailsClicked");
      super();
      this.b = var;
      this.a = (List)(new ArrayList());
   }

   private final SpannableString a(SpannableString var, String var, ClickableSpan var) {
      int var = kotlin.h.j.a((CharSequence)var, var, 0, false, 6, (Object)null);
      if(var > -1) {
         var.setSpan(var, var, var.length() + var, 33);
      }

      return var;
   }

   private final String a(co.uk.getmondo.d.c var) {
      String var;
      if(var != null) {
         var = var.toString();
         if(var != null) {
            return var;
         }
      }

      var = "";
      return var;
   }

   public long a(int var) {
      return (long)((f.c)this.a.get(var)).b().hashCode();
   }

   // $FF: synthetic method
   public android.support.v7.widget.RecyclerView.w a(ViewGroup var) {
      return (android.support.v7.widget.RecyclerView.w)this.b(var);
   }

   public f.d a(ViewGroup var, int var) {
      kotlin.d.b.l.b(var, "parent");
      View var = LayoutInflater.from(var.getContext()).inflate(2131034373, var, false);
      kotlin.d.b.l.a(var, "layoutInflater.inflate(R…em_limits, parent, false)");
      return new f.d(var);
   }

   public final void a(co.uk.getmondo.d.e var, Resources var) {
      kotlin.d.b.l.b(var, "limit");
      kotlin.d.b.l.b(var, "resources");
      String var = var.getString(2131362398);
      String var = var.getString(2131362395);
      String var = var.getString(2131362394);
      String var = var.getString(2131362397);
      String var = var.getString(2131362396);
      List var = this.a;
      kotlin.d.b.l.a(var, "headerUsingYourCard");
      String var = var.getString(2131362387);
      kotlin.d.b.l.a(var, "resources.getString(R.st…g.limits_max_single_card)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.b()), (String)null, (f.b)null, 48, (kotlin.d.b.i)null));
      List var = this.a;
      kotlin.d.b.l.a(var, "headerBalanceAndTopups");
      String var = var.getString(2131362384);
      kotlin.d.b.l.a(var, "resources.getString(R.string.limits_max_balance)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.c()), (String)null, (f.b)null, 48, (kotlin.d.b.i)null));
      var = this.a;
      var = var.getString(2131362377);
      kotlin.d.b.l.a(var, "resources.getString(R.string.limits_daily_top_up)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.d()), this.a(var.e()), (f.b)null, 32, (kotlin.d.b.i)null));
      var = this.a;
      var = var.getString(2131362385);
      kotlin.d.b.l.a(var, "resources.getString(R.st….limits_max_over_30_days)");
      CharSequence var = (CharSequence)var;
      String var = this.a(var.f());
      var = this.a(var.g());
      var = var.getString(2131362382, new Object[]{var.f()});
      kotlin.d.b.l.a(var, "resources.getString(R.st…limit.maxTopUpOver30Days)");
      var.add(new f.c(var, var, var, var, var, new f.b(2131362381, var)));
      var = this.a;
      var = var.getString(2131362383);
      kotlin.d.b.l.a(var, "resources.getString(R.st…limits_max_annual_top_up)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.h()), this.a(var.i()), (f.b)null, 32, (kotlin.d.b.i)null));
      List var = this.a;
      kotlin.d.b.l.a(var, "headerAtmWithdrawals");
      var = var.getString(2131362378);
      kotlin.d.b.l.a(var, "resources.getString(R.st….limits_daily_withdrawal)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.j()), this.a(var.k()), (f.b)null, 32, (kotlin.d.b.i)null));
      var = this.a;
      var = var.getString(2131362390);
      kotlin.d.b.l.a(var, "resources.getString(R.st…g.limits_rolling_30_days)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.l()), this.a(var.m()), (f.b)null, 32, (kotlin.d.b.i)null));
      var = this.a;
      var = var.getString(2131362376);
      kotlin.d.b.l.a(var, "resources.getString(R.st…limits_annual_withdrawal)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.n()), this.a(var.o()), (f.b)null, 32, (kotlin.d.b.i)null));
      List var = this.a;
      kotlin.d.b.l.a(var, "headerPayments");
      var = var.getString(2131362388);
      kotlin.d.b.l.a(var, "resources.getString(R.st…imits_max_single_payment)");
      var.add(new f.c(var, var, (CharSequence)var, this.a(var.p()), (String)null, (f.b)null, 48, (kotlin.d.b.i)null));
      var = this.a;
      var = var.getString(2131362386);
      kotlin.d.b.l.a(var, "resources.getString(R.st…ax_received_over_30_days)");
      CharSequence var = (CharSequence)var;
      var = this.a(var.q());
      var = this.a(var.r());
      var = var.getString(2131362380, new Object[]{var.q()});
      kotlin.d.b.l.a(var, "resources.getString(R.st…ntsMaxReceivedOver30Days)");
      var.add(new f.c(var, var, var, var, var, new f.b(2131362381, var)));
      if(var.a() == VerificationType.FULL || var.a() == VerificationType.EXTENDED) {
         List var = this.a;
         kotlin.d.b.l.a(var, "headerMonzoMe");
         var = var.getString(2131362388);
         kotlin.d.b.l.a(var, "resources.getString(R.st…imits_max_single_payment)");
         var.add(new f.c(var, var, (CharSequence)var, this.a(var.s()), (String)null, (f.b)null, 48, (kotlin.d.b.i)null));
         var = this.a;
         var = var.getString(2131362386);
         kotlin.d.b.l.a(var, "resources.getString(R.st…ax_received_over_30_days)");
         CharSequence var = (CharSequence)var;
         var = this.a(var.t());
         var = this.a(var.u());
         String var = var.getString(2131362379, new Object[]{var.t()});
         kotlin.d.b.l.a(var, "resources.getString(R.st…oMeMaxReceivedOver30Days)");
         var.add(new f.c(var, var, var, var, var, new f.b(2131362381, var)));
      }

      this.notifyDataSetChanged();
   }

   public final void a(y var, Resources var) {
      kotlin.d.b.l.b(var, "paymentLimits");
      kotlin.d.b.l.b(var, "resources");
      Iterator var = var.a().iterator();

      while(var.hasNext()) {
         z var = (z)var.next();
         Iterable var = (Iterable)var.b();
         Collection var = (Collection)this.a;

         f.c var;
         for(Iterator var = var.iterator(); var.hasNext(); var.add(var)) {
            co.uk.getmondo.d.x var = (co.uk.getmondo.d.x)var.next();
            if(var instanceof co.uk.getmondo.d.x.a) {
               co.uk.getmondo.d.c var = ((co.uk.getmondo.d.x.a)var).b().b(((co.uk.getmondo.d.x.a)var).c().k());
               var = new f.c(var, var.a(), (CharSequence)((co.uk.getmondo.d.x.a)var).a(), ((co.uk.getmondo.d.x.a)var).b().toString(), var.toString(), (f.b)null, 32, (kotlin.d.b.i)null);
            } else if(var instanceof co.uk.getmondo.d.x.b) {
               long var = ((co.uk.getmondo.d.x.b)var).b();
               long var = ((co.uk.getmondo.d.x.b)var).c();
               var = new f.c(var, var.a(), (CharSequence)((co.uk.getmondo.d.x.b)var).a(), String.valueOf(((co.uk.getmondo.d.x.b)var).b()), String.valueOf(var - var), (f.b)null, 32, (kotlin.d.b.i)null);
            } else {
               if(!(var instanceof co.uk.getmondo.d.x.c)) {
                  throw new NoWhenBranchMatchedException();
               }

               String var = var.a();
               CharSequence var = (CharSequence)((co.uk.getmondo.d.x.c)var).a();
               String var = var.getString(2131362399);
               kotlin.d.b.l.a(var, "resources.getString(R.string.limits_unlimited)");
               var = new f.c(var, var, var, var, (String)null, (f.b)null, 32, (kotlin.d.b.i)null);
            }
         }
      }

      this.notifyDataSetChanged();
   }

   public void a(f.a var, int var) {
      kotlin.d.b.l.b(var, "holder");
      var.a((f.c)this.a.get(var));
   }

   public void a(f.d var, int var) {
      kotlin.d.b.l.b(var, "holder");
      var.a((f.c)this.a.get(var));
   }

   public f.a b(ViewGroup var) {
      kotlin.d.b.l.b(var, "parent");
      View var = LayoutInflater.from(var.getContext()).inflate(2131034371, var, false);
      kotlin.d.b.l.a(var, "LayoutInflater.from(pare…em_header, parent, false)");
      return new f.a(var);
   }

   public int getItemCount() {
      return this.a.size();
   }

   // $FF: synthetic method
   public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var, int var) {
      this.a((f.d)var, var);
   }

   // $FF: synthetic method
   public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var, int var) {
      return (android.support.v7.widget.RecyclerView.w)this.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0007\u001a\u00020\b2\n\u0010\t\u001a\u00060\nR\u00020\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/settings/LimitsAdapter$HeaderViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "limitItem", "Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;", "Lco/uk/getmondo/settings/LimitsAdapter;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends android.support.v7.widget.RecyclerView.w {
      private final View a;

      public a(View var) {
         kotlin.d.b.l.b(var, "view");
         super(var);
         this.a = var;
      }

      public final void a(f.c var) {
         kotlin.d.b.l.b(var, "limitItem");
         ((TextView)this.a.findViewById(co.uk.getmondo.c.a.headerTextView)).setText((CharSequence)var.b());
         ((TextView)this.a.findViewById(co.uk.getmondo.c.a.headerTextView)).setVisibility(0);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0080\b\u0018\u00002\u00020\u0001B\u0017\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "", "title", "", "description", "", "(ILjava/lang/String;)V", "getDescription", "()Ljava/lang/String;", "getTitle", "()I", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private final int a;
      private final String b;

      public b(int var, String var) {
         kotlin.d.b.l.b(var, "description");
         super();
         this.a = var;
         this.b = var;
      }

      public final int a() {
         return this.a;
      }

      public final String b() {
         return this.b;
      }

      public boolean equals(Object var) {
         boolean var = false;
         boolean var;
         if(this != var) {
            var = var;
            if(!(var instanceof f.b)) {
               return var;
            }

            f.b var = (f.b)var;
            boolean var;
            if(this.a == var.a) {
               var = true;
            } else {
               var = false;
            }

            var = var;
            if(!var) {
               return var;
            }

            var = var;
            if(!kotlin.d.b.l.a(this.b, var.b)) {
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         int var = this.a;
         String var = this.b;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var + var * 31;
      }

      public String toString() {
         return "LimitDetails(title=" + this.a + ", description=" + this.b + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\b\u0080\u0004\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;", "", "resources", "Landroid/content/res/Resources;", "category", "", "name", "", "limit", "amountRemaining", "limitDetails", "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "(Lco/uk/getmondo/settings/LimitsAdapter;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;)V", "getAmountRemaining", "()Ljava/lang/String;", "getCategory", "getLimit", "getLimitDetails", "()Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "getName", "()Ljava/lang/CharSequence;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class c {
      private final CharSequence b;
      private final String c;
      private final String d;
      private final String e;
      private final f.b f;

      public c(Resources var, String var, CharSequence var, String var, String var, f.b var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "category");
         kotlin.d.b.l.b(var, "name");
         kotlin.d.b.l.b(var, "limit");
         super();
         this.c = var;
         this.d = var;
         this.e = var;
         this.f = var;
         if(this.f != null) {
            SpannableString var = new SpannableString(var);
            String var = var.getString(2131362375);
            kotlin.d.b.l.a(var, "resources.getString(R.st…ng.limit_details_matcher)");
            this.b = (CharSequence)f.this.a(var, var, (ClickableSpan)(new f.e() {
               public void onClick(View var) {
                  kotlin.d.b.l.b(var, "widget");
                  f.this.b.a(c.this.e());
               }
            }));
         } else {
            this.b = var;
         }

      }

      // $FF: synthetic method
      public c(Resources var, String var, CharSequence var, String var, String var, f.b var, int var, kotlin.d.b.i var) {
         if((var & 16) != 0) {
            var = (String)null;
         }

         if((var & 32) != 0) {
            var = (f.b)null;
         }

         this(var, var, var, var, var, var);
      }

      public final CharSequence a() {
         return this.b;
      }

      public final String b() {
         return this.c;
      }

      public final String c() {
         return this.d;
      }

      public final String d() {
         return this.e;
      }

      public final f.b e() {
         return this.f;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0007\u001a\u00020\b2\n\u0010\t\u001a\u00060\nR\u00020\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/settings/LimitsAdapter$LimitItemHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "limitItem", "Lco/uk/getmondo/settings/LimitsAdapter$LimitItem;", "Lco/uk/getmondo/settings/LimitsAdapter;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class d extends android.support.v7.widget.RecyclerView.w {
      private final View a;

      public d(View var) {
         kotlin.d.b.l.b(var, "view");
         super(var);
         this.a = var;
      }

      public final void a(f.c var) {
         kotlin.d.b.l.b(var, "limitItem");
         ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitNameTextView)).setText(var.a());
         ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitAmountTextView)).setText((CharSequence)var.c());
         if(var.d() == null) {
            ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitAmountRemainingTextView)).setVisibility(8);
         } else {
            String var = this.a.getResources().getString(2131362401);
            ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitAmountRemainingTextView)).setVisibility(0);
            TextView var = (TextView)this.a.findViewById(co.uk.getmondo.c.a.limitAmountRemainingTextView);
            ab var = ab.a;
            kotlin.d.b.l.a(var, "remainingFormat");
            Object[] var = new Object[]{var.d()};
            var = String.format(var, Arrays.copyOf(var, var.length));
            kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
            var.setText((CharSequence)var);
         }

         if(var.e() != null) {
            ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitNameTextView)).setMovementMethod(LinkMovementMethod.getInstance());
         } else {
            ((TextView)this.a.findViewById(co.uk.getmondo.c.a.limitNameTextView)).setMovementMethod((MovementMethod)null);
         }

      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b \u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/settings/LimitsAdapter$NoUnderlineClickableSpan;", "Landroid/text/style/ClickableSpan;", "()V", "updateDrawState", "", "ds", "Landroid/text/TextPaint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public abstract static class e extends ClickableSpan {
      public void updateDrawState(TextPaint var) {
         kotlin.d.b.l.b(var, "ds");
         var.setColor(var.linkColor);
         var.setUnderlineText(false);
      }
   }
}
