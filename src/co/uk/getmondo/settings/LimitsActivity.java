package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.d.y;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0013\u001a\u00020\bH\u0016J\u0012\u0010\u0014\u001a\u00020\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u0017\u001a\u00020\bH\u0014J\b\u0010\u0018\u001a\u00020\bH\u0016J\u0010\u0010\u0019\u001a\u00020\b2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\bH\u0016J\u0010\u0010 \u001a\u00020\b2\u0006\u0010!\u001a\u00020\"H\u0002J\b\u0010#\u001a\u00020\bH\u0016J\b\u0010$\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\nR\u001e\u0010\r\u001a\u00020\u000e8\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006&"},
   d2 = {"Lco/uk/getmondo/settings/LimitsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/settings/LimitsPresenter$View;", "()V", "adapter", "Lco/uk/getmondo/settings/LimitsAdapter;", "onRequestHigherLimitsClicked", "Lio/reactivex/Observable;", "", "getOnRequestHigherLimitsClicked", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "presenter", "Lco/uk/getmondo/settings/LimitsPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/settings/LimitsPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/settings/LimitsPresenter;)V", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "openKycOnboarding", "setBalanceLimitPrepaid", "balanceLimit", "Lco/uk/getmondo/model/BalanceLimit;", "setPaymentLimits", "paymentLimits", "Lco/uk/getmondo/model/PaymentLimits;", "showError", "showLimitDetails", "limitDetails", "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "showLoading", "showRequestHigherLimitsInfo", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LimitsActivity extends co.uk.getmondo.common.activities.b implements h.a {
   public static final LimitsActivity.a b = new LimitsActivity.a((kotlin.d.b.i)null);
   public h a;
   private f c;
   private HashMap e;

   public static final void a(Context var) {
      kotlin.d.b.l.b(var, "context");
      b.a(var);
   }

   private final void a(f.b var) {
      g.a.a(var).show(this.getFragmentManager(), "TAG_LIMITS_DETAILS");
   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.limitsRequestHigherButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(co.uk.getmondo.d.e var) {
      kotlin.d.b.l.b(var, "balanceLimit");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.limitsSubtitleTextView)));
      ((TextView)this.a(co.uk.getmondo.c.a.limitsSubtitleTextView)).setText(var.a().b());
      f var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("adapter");
      }

      Resources var = this.getResources();
      kotlin.d.b.l.a(var, "resources");
      var.a(var, var);
   }

   public void a(y var) {
      kotlin.d.b.l.b(var, "paymentLimits");
      f var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("adapter");
      }

      Resources var = this.getResources();
      kotlin.d.b.l.a(var, "resources");
      var.a(var, var);
   }

   public io.reactivex.n b() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView)).c();
   }

   public void c() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.limitsProgressBar)));
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView));
   }

   public void d() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.limitsProgressBar));
   }

   public void e() {
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView)));
   }

   public void f() {
      ae.a((View)((LinearLayout)this.a(co.uk.getmondo.c.a.limitsRequestHigherViewGroup)));
   }

   public void g() {
      this.startActivity(IdentityVerificationActivity.a.a(IdentityVerificationActivity.g, (Context)this, co.uk.getmondo.signup.identity_verification.a.j.a, Impression.KycFrom.LIMITS, SignupSource.LEGACY_PREPAID, (co.uk.getmondo.signup.j)null, (String)null, 48, (Object)null));
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034184);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      h var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((h.a)this);
      this.c = new f((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final void a(f.b var) {
            kotlin.d.b.l.b(var, "it");
            LimitsActivity.this.a(var);
         }
      }));
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      Context var = (Context)this;
      f var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("adapter");
      }

      var.a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.h(var, (a.a.a.a.a.a)var, 0)));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView)).setHasFixedSize(true);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)(new LinearLayoutManager((Context)this)));
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      f var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("adapter");
      }

      var.setAdapter((android.support.v7.widget.RecyclerView.a)var);
      var = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("adapter");
      }

      var.a((android.support.v7.widget.RecyclerView.g)(new a.a.a.a.a.b((a.a.a.a.a.a)var)));
   }

   protected void onDestroy() {
      h var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/settings/LimitsActivity$Companion;", "", "()V", "TAG_LIMITS_DETAILS", "", "start", "", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final void a(Context var) {
         kotlin.d.b.l.b(var, "context");
         var.startActivity(new Intent(var, LimitsActivity.class));
      }
   }
}
