package co.uk.getmondo.settings;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class q implements OnClickListener {
   private final p a;

   private q(p var) {
      this.a = var;
   }

   public static OnClickListener a(p var) {
      return new q(var);
   }

   public void onClick(DialogInterface var, int var) {
      p.a(this.a, var, var);
   }
}
