package co.uk.getmondo.settings;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SwitchView_ViewBinding implements Unbinder {
   private SwitchView a;

   public SwitchView_ViewBinding(SwitchView var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821758, "field 'titleTextView'", TextView.class);
      var.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821761, "field 'descriptionTextView'", TextView.class);
      var.settingSwitch = (Switch)Utils.findRequiredViewAsType(var, 2131821760, "field 'settingSwitch'", Switch.class);
      var.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821759, "field 'progressBar'", ProgressBar.class);
   }

   public void unbind() {
      SwitchView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
         var.descriptionTextView = null;
         var.settingSwitch = null;
         var.progressBar = null;
      }
   }
}
