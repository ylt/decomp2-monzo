package co.uk.getmondo.settings;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SwitchView extends LinearLayout {
   @BindView(2131821761)
   TextView descriptionTextView;
   @BindView(2131821759)
   ProgressBar progressBar;
   @BindView(2131821760)
   Switch settingSwitch;
   @BindView(2131821758)
   TextView titleTextView;

   public SwitchView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public SwitchView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public SwitchView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      LayoutInflater.from(this.getContext()).inflate(2131034437, this);
      this.setOrientation(0);
      this.setLayoutTransition(new LayoutTransition());
      ButterKnife.bind((View)this);
      TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.SwitchView, 0, 0);

      try {
         this.titleTextView.setText(var.getString(0));
      } finally {
         var.recycle();
      }

      this.settingSwitch.setClickable(false);
   }

   public void a() {
      this.settingSwitch.setVisibility(8);
      this.progressBar.setVisibility(0);
   }

   public void b() {
      this.settingSwitch.setVisibility(0);
      this.progressBar.setVisibility(8);
   }

   public void c() {
      this.settingSwitch.setChecked(true);
   }

   public void d() {
      this.settingSwitch.setChecked(false);
   }

   public boolean e() {
      return this.settingSwitch.isChecked();
   }

   public void setDescription(String var) {
      this.descriptionTextView.setVisibility(0);
      this.descriptionTextView.setText(var);
   }
}
