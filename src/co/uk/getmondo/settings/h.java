package co.uk.getmondo.settings;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.PaymentLimitsApi;
import co.uk.getmondo.api.model.VerificationType;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.y;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/settings/LimitsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/settings/LimitsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "paymentLimitsApi", "Lco/uk/getmondo/api/PaymentLimitsApi;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "identityVerificationStatus", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/PaymentLimitsApi;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class h extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final MonzoApi f;
   private final PaymentLimitsApi g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.signup.identity_verification.a.f j;

   public h(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, MonzoApi var, PaymentLimitsApi var, co.uk.getmondo.common.a var, co.uk.getmondo.common.e.a var, co.uk.getmondo.signup.identity_verification.a.f var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "accountService");
      kotlin.d.b.l.b(var, "monzoApi");
      kotlin.d.b.l.b(var, "paymentLimitsApi");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "identityVerificationStatus");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   public void a(final h.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      ak var = this.e.b();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      final co.uk.getmondo.d.a var = var.c();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      kotlin.d.a.b var;
      io.reactivex.b.a var;
      io.reactivex.n var;
      io.reactivex.c.g var;
      Object var;
      io.reactivex.b.b var;
      if(kotlin.d.b.l.a(var.d(), co.uk.getmondo.d.a.b.PREPAID)) {
         var = this.b;
         var = var.b().startWith((Object)kotlin.n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n varx) {
               kotlin.d.b.l.b(varx, "it");
               return co.uk.getmondo.common.j.f.a(h.this.f.balanceLimits(var.a()).d((io.reactivex.c.h)null.a).b(h.this.d).a(h.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b varx) {
                     var.c();
                  }
               })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
                  public final void a(co.uk.getmondo.d.e varx, Throwable var) {
                     var.d();
                  }
               })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable varx) {
                     co.uk.getmondo.common.e.a var = h.this.i;
                     kotlin.d.b.l.a(varx, "it");
                     var.a(varx, (co.uk.getmondo.common.e.a.a)var);
                     var.e();
                  }
               })));
            }
         }));
         var = (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.e varx) {
               Impression var = Impression.Companion.a(varx.a());
               h.this.h.a(var);
               boolean var;
               if(!kotlin.d.b.l.a(varx.a(), VerificationType.STANDARD) && !kotlin.d.b.l.a(varx.a(), VerificationType.BLOCKED)) {
                  var = false;
               } else {
                  var = true;
               }

               if(var && !h.this.j.a()) {
                  var.f();
               }

               h.a var = var;
               kotlin.d.b.l.a(varx, "balanceLimits");
               var.a(varx);
            }
         });
         var = (kotlin.d.a.b)null.a;
         var = var;
         if(var != null) {
            var = new i(var);
         }

         var = var.subscribe(var, (io.reactivex.c.g)var);
         kotlin.d.b.l.a(var, "view.onRetryClicked\n    …            }, Timber::e)");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      } else {
         this.h.a(Impression.Companion.aq());
         var = this.b;
         var = var.b().startWith((Object)kotlin.n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n varx) {
               kotlin.d.b.l.b(varx, "it");
               return co.uk.getmondo.common.j.f.a(h.this.g.paymentLimits(var.a()).d((io.reactivex.c.h)null.a).b(h.this.d).a(h.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b varx) {
                     var.c();
                  }
               })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
                  public final void a(y varx, Throwable var) {
                     var.d();
                  }
               })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable varx) {
                     co.uk.getmondo.common.e.a var = h.this.i;
                     kotlin.d.b.l.a(varx, "it");
                     var.a(varx, (co.uk.getmondo.common.e.a.a)var);
                     var.e();
                  }
               })));
            }
         }));
         var = (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(y varx) {
               h.a var = var;
               kotlin.d.b.l.a(varx, "paymentLimits");
               var.a(varx);
            }
         });
         var = (kotlin.d.a.b)null.a;
         var = var;
         if(var != null) {
            var = new i(var);
         }

         var = var.subscribe(var, (io.reactivex.c.g)var);
         kotlin.d.b.l.a(var, "view.onRetryClicked\n    …            }, Timber::e)");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      }

      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.g();
         }
      }));
      kotlin.d.b.l.a(var, "view.onRequestHigherLimi…iew.openKycOnboarding() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b`\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\n\u001a\u00020\u0005H&J\b\u0010\u000b\u001a\u00020\u0005H&J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH&J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\b\u0010\u0012\u001a\u00020\u0005H&J\b\u0010\u0013\u001a\u00020\u0005H&J\b\u0010\u0014\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/settings/LimitsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onRequestHigherLimitsClicked", "Lio/reactivex/Observable;", "", "getOnRequestHigherLimitsClicked", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "hideLoading", "openKycOnboarding", "setBalanceLimitPrepaid", "balanceLimit", "Lco/uk/getmondo/model/BalanceLimit;", "setPaymentLimits", "paymentLimits", "Lco/uk/getmondo/model/PaymentLimits;", "showError", "showLoading", "showRequestHigherLimitsInfo", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.e var);

      void a(y var);

      io.reactivex.n b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
