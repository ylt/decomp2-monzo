package co.uk.getmondo.settings;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity.OpenSourceLibraryHolder a;

   public OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding(OpenSourceLicensesActivity.OpenSourceLibraryHolder var, View var) {
      this.a = var;
      var.libraryNameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821604, "field 'libraryNameTextView'", TextView.class);
      var.libraryLicenseTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821605, "field 'libraryLicenseTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity.OpenSourceLibraryHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.libraryNameTextView = null;
         var.libraryLicenseTextView = null;
      }
   }
}
