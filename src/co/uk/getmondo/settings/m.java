package co.uk.getmondo.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

// $FF: synthetic class
final class m implements OnSharedPreferenceChangeListener {
   private final io.reactivex.o a;

   private m(io.reactivex.o var) {
      this.a = var;
   }

   public static OnSharedPreferenceChangeListener a(io.reactivex.o var) {
      return new m(var);
   }

   public void onSharedPreferenceChanged(SharedPreferences var, String var) {
      k.a(this.a, var, var);
   }
}
