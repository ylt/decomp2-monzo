package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final Activity a;

   private d(Activity var) {
      this.a = var;
   }

   public static OnClickListener a(Activity var) {
      return new d(var);
   }

   public void onClick(DialogInterface var, int var) {
      c.a(this.a, var, var);
   }
}
