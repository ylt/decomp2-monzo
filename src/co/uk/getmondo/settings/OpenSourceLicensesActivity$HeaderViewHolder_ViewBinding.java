package co.uk.getmondo.settings;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity$HeaderViewHolder_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity.HeaderViewHolder a;

   public OpenSourceLicensesActivity$HeaderViewHolder_ViewBinding(OpenSourceLicensesActivity.HeaderViewHolder var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821599, "field 'titleTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity.HeaderViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
      }
   }
}
