package co.uk.getmondo.settings;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiAccountSettings;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ak;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001$Bk\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019¢\u0006\u0002\u0010\u001aJ\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0002H\u0016J\u0018\u0010!\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006%"},
   d2 = {"Lco/uk/getmondo/settings/SettingsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/settings/SettingsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "userAccessTokenManager", "Lco/uk/getmondo/common/accounts/UserAccessTokenManager;", "localUserSettingStorage", "Lco/uk/getmondo/settings/LocalUserSettingStorage;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/accounts/UserAccessTokenManager;Lco/uk/getmondo/settings/LocalUserSettingStorage;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "magStripeInitialised", "", "paymentsInitialised", "register", "", "view", "updateMagStripeDescription", "accountSettings", "Lco/uk/getmondo/api/model/ApiAccountSettings;", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class u extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private boolean d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.payments.send.data.h i;
   private final MonzoApi j;
   private final co.uk.getmondo.common.e.a k;
   private final co.uk.getmondo.payments.send.data.p l;
   private final co.uk.getmondo.common.accounts.o m;
   private final k n;
   private final co.uk.getmondo.api.b.a o;
   private final co.uk.getmondo.common.o p;

   public u(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.a var, co.uk.getmondo.payments.send.data.h var, MonzoApi var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.send.data.p var, co.uk.getmondo.common.accounts.o var, k var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.o var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "accountService");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "userSettingsRepository");
      kotlin.d.b.l.b(var, "monzoApi");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "userSettingsStorage");
      kotlin.d.b.l.b(var, "userAccessTokenManager");
      kotlin.d.b.l.b(var, "localUserSettingStorage");
      kotlin.d.b.l.b(var, "userInteractor");
      kotlin.d.b.l.b(var, "featureFlagsStorage");
      super();
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
      this.n = var;
      this.o = var;
      this.p = var;
   }

   private final void a(u.a var, ApiAccountSettings var) {
      if(var.a()) {
         int var = (int)TimeUnit.SECONDS.toHours(var.b());
         var.a(var, (int)TimeUnit.SECONDS.toMinutes(var.b() - TimeUnit.HOURS.toSeconds((long)var)));
      } else {
         var.L();
      }

   }

   public void a(final u.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      final ak var = this.g.b();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      final co.uk.getmondo.d.a var = var.c();
      ak var = this.g.b();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      final ac var = var.d();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      if(var == null) {
         kotlin.d.b.l.a();
      }

      var.a(var, var.b());
      this.h.a(Impression.Companion.ap());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.ah());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.A();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onNameClicked()\n   …on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.c().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.ah());
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            ac varx = var.d();
            if(varx != null && varx.i()) {
               var.C();
            } else {
               var.B();
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new v(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onAddressClicked()\n…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.n.b().startWith((Object)Boolean.valueOf(this.n.a())).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            kotlin.d.b.l.a(varx, "notificationsEnabled");
            if(varx.booleanValue()) {
               var.V();
            } else {
               var.W();
            }

         }
      }));
      kotlin.d.b.l.a(var, "localUserSettingStorage.…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.w().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            u.this.h.a(Impression.Companion.c(var));
         }
      })).observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            k var = u.this.n;
            kotlin.d.b.l.a(var, "it");
            var.a(var.booleanValue());
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onNotificationsStat…(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.d().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.ai());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.G();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onAboutMonzoClicked…zo() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.e().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.aj());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.H();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onTermsAndCondition…ns() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.f().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.ak());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.I();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onPrivacyPolicyClic…cy() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.g().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.al());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.J();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onFscsProtectionCli…on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var.L();
      var = this.b;
      var = this.j.accountSettings(var.a()).b(this.f).a(this.e).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(ApiAccountSettings varx) {
            u var = u.this;
            u.a var = var;
            kotlin.d.b.l.a(varx, "accountSettings");
            var.a(var, varx);
            u.this.c = true;
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            u.this.c = true;
            co.uk.getmondo.common.e.a var = u.this.k;
            kotlin.d.b.l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      kotlin.d.b.l.a(var, "monzoApi.accountSettings… view)\n                })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.x().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var) {
            kotlin.d.b.l.b(var, "it");
            return u.this.c;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            u.this.h.a(Impression.Companion.a(var));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            var.M();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(Boolean varx) {
            kotlin.d.b.l.b(varx, "state");
            return co.uk.getmondo.common.j.f.a(u.this.j.updateSettings(var.a(), varx.booleanValue()).b(u.this.f).a(u.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.N();
                  co.uk.getmondo.common.e.a var = u.this.k;
                  kotlin.d.b.l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(ApiAccountSettings varx) {
            var.N();
            u var = u.this;
            u.a var = var;
            kotlin.d.b.l.a(varx, "accountSettings");
            var.a(var, varx);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onMagStripeStateTog…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      if(kotlin.d.b.l.a(var.d(), co.uk.getmondo.d.a.b.PREPAID)) {
         var.X();
      } else {
         var.Y();
         final String var = ((ad)var).j();
         final String var = ((ad)var).g();
         var.a(var, var);
         io.reactivex.b.a var = this.b;
         var = this.p.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.l varx) {
               if(varx.a()) {
                  var.O();
               } else {
                  var.P();
               }

            }
         }));
         kotlin.d.b.l.a(var, "featureFlagsStorage.feat…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = var.z().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n varx) {
               var.a(var.c(), var, var);
            }
         }));
         kotlin.d.b.l.a(var, "view.onShareAccountDetai…de)\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      }

      var = this.b;
      var = var.h().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.F();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onLimitsClicked().s…ts() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.i.b().map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.send.data.a.d varx) {
            if(kotlin.d.b.l.a(varx, co.uk.getmondo.payments.send.data.a.d.a)) {
               var.R();
            } else {
               var.S();
            }

            u.this.d = true;
         }
      }));
      kotlin.d.b.l.a(var, "userSettingsRepository.o… = true\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.y().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var) {
            kotlin.d.b.l.b(var, "it");
            return u.this.d;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            u.this.h.a(Impression.Companion.b(var));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            kotlin.d.b.l.a(varx, "state");
            if(varx.booleanValue() && kotlin.d.b.l.a(u.this.l.a(), co.uk.getmondo.payments.send.data.a.d.c)) {
               var.Q();
            }

         }
      })).filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var) {
            kotlin.d.b.l.b(var, "it");
            return kotlin.d.b.l.a(u.this.l.a(), co.uk.getmondo.payments.send.data.a.d.c) ^ true;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            var.T();
         }
      })).switchMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.v a(Boolean varx) {
            kotlin.d.b.l.b(varx, "state");
            return u.this.i.a(varx.booleanValue()).d((io.reactivex.c.h)null.a).b(u.this.f).a(u.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = u.this.k;
                  kotlin.d.b.l.a(varx, "error");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).a(io.reactivex.v.a((Object)kotlin.n.a));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.U();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onPaymentsStateTogg…ng() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.i().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.am());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.D();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onOpenSourceLicense…es() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.K();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onLogOutClicked()\n …on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.k().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.an());
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            return io.reactivex.h.a((Callable)(new Callable() {
               public final String a() {
                  return u.this.m.a();
               }

               // $FF: synthetic method
               public Object call() {
                  return this.a();
               }
            })).b(u.this.f);
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var) {
            if(!co.uk.getmondo.a.b.booleanValue()) {
               u.this.o.b(var).b(u.this.f).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
            }

         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var) {
            u.this.g.e();
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            var.u();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onConfirmLogOutClic…sh() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.v().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            u.this.h.a(Impression.Companion.ao());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.E();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onCloseAccountClick…nt() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000e\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&J\b\u0010\u001b\u001a\u00020\u0004H&J\b\u0010\u001c\u001a\u00020\u0004H&J\b\u0010\u001d\u001a\u00020\u0004H&J\u0018\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 H&J\u001a\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&H&J \u0010'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020 2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 H&J\b\u0010)\u001a\u00020\u0004H&J\b\u0010*\u001a\u00020\u0004H&J\b\u0010+\u001a\u00020\u0004H&J\b\u0010,\u001a\u00020\u0004H&J\u0018\u0010-\u001a\u00020\u00042\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/H&J\b\u00101\u001a\u00020\u0004H&J\b\u00102\u001a\u00020\u0004H&J\b\u00103\u001a\u00020\u0004H&J\b\u00104\u001a\u00020\u0004H&J\b\u00105\u001a\u00020\u0004H&J\b\u00106\u001a\u00020\u0004H&J\b\u00107\u001a\u00020\u0004H&J\b\u00108\u001a\u00020\u0004H&J\b\u00109\u001a\u00020\u0004H&J\b\u0010:\u001a\u00020\u0004H&J\b\u0010;\u001a\u00020\u0004H&J\b\u0010<\u001a\u00020\u0004H&¨\u0006="},
      d2 = {"Lco/uk/getmondo/settings/SettingsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideMagStripeLoading", "", "hidePaymentsLoading", "hidePaymentsUi", "onAboutMonzoClicked", "Lio/reactivex/Observable;", "onAddressClicked", "onCloseAccountClicked", "onConfirmLogOutClicked", "onFscsProtectionClicked", "onLimitsClicked", "onLogOutClicked", "onMagStripeStateToggled", "", "onNameClicked", "onNotificationsStateToggled", "onOpenSourceLicensesClicked", "onPaymentsStateToggled", "onPrivacyPolicyClicked", "onShareAccountDetailsClicked", "onTermsAndConditionsClicked", "openAboutMonzo", "openFscsProtection", "openLimits", "openPrivacyPolicy", "openTermsAndConditions", "openUpdateAddress", "setAccountInformation", "accountNumber", "", "sortCode", "setProfileInformation", "profile", "Lco/uk/getmondo/model/Profile;", "created", "Lorg/threeten/bp/LocalDateTime;", "shareAccountDetails", "profileName", "showChangeInformation", "showCloseAccount", "showLogoutConfirmation", "showMagStripeDisabled", "showMagStripeEnabled", "hours", "", "minutes", "showMagStripeLoading", "showNotificationsChecked", "showNotificationsUnchecked", "showOpenSourceLicenses", "showPaymentsBlocked", "showPaymentsDisabled", "showPaymentsEnabled", "showPaymentsLoading", "showPaymentsUi", "showPrepaidUi", "showRetailUi", "showUpdateAddressSupport", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      void D();

      void E();

      void F();

      void G();

      void H();

      void I();

      void J();

      void K();

      void L();

      void M();

      void N();

      void O();

      void P();

      void Q();

      void R();

      void S();

      void T();

      void U();

      void V();

      void W();

      void X();

      void Y();

      void a(int var, int var);

      void a(ac var, LocalDateTime var);

      void a(String var, String var);

      void a(String var, String var, String var);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();

      io.reactivex.n x();

      io.reactivex.n y();

      io.reactivex.n z();
   }
}
