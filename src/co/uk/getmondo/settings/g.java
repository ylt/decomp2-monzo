package co.uk.getmondo.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/settings/LimitsDetailsDialogFragment;", "Landroid/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends DialogFragment {
   public static final g.a a = new g.a((kotlin.d.b.i)null);
   private HashMap b;

   public void a() {
      if(this.b != null) {
         this.b.clear();
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      AlertDialog var = (new Builder((Context)this.getActivity())).setTitle(this.getArguments().getInt("KEY_TITLE")).setMessage((CharSequence)this.getArguments().getString("KEY_DETAILS")).setPositiveButton(17039370, (OnClickListener)null).create();
      kotlin.d.b.l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.a();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/settings/LimitsDetailsDialogFragment$Companion;", "", "()V", "KEY_DETAILS", "", "KEY_TITLE", "newInstance", "Lco/uk/getmondo/settings/LimitsDetailsDialogFragment;", "limitDetails", "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final g a(f.b var) {
         kotlin.d.b.l.b(var, "limitDetails");
         g var = new g();
         Bundle var = new Bundle();
         var.putInt("KEY_TITLE", var.a());
         var.putString("KEY_DETAILS", var.b());
         var.setArguments(var);
         return var;
      }
   }
}
