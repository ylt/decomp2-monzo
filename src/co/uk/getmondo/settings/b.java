package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final Activity a;

   private b(Activity var) {
      this.a = var;
   }

   public static OnClickListener a(Activity var) {
      return new b(var);
   }

   public void onClick(DialogInterface var, int var) {
      a.a(this.a, var, var);
   }
}
