package co.uk.getmondo.settings;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import co.uk.getmondo.MonzoApplication;

public class c extends DialogFragment {
   public static c a() {
      return new c();
   }

   // $FF: synthetic method
   static void a(Activity var, DialogInterface var, int var) {
      MonzoApplication.a(var).b().p().a();
   }

   public Dialog onCreateDialog(Bundle var) {
      Activity var = this.getActivity();
      return (new Builder(var)).setTitle(2131362669).setMessage(2131362668).setPositiveButton(this.getString(2131362666), d.a(var)).setNegativeButton(this.getString(2131362499), (OnClickListener)null).create();
   }
}
