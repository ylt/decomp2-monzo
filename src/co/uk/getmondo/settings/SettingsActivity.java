package co.uk.getmondo.settings;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.profile.address.SelectAddressActivity;
import co.uk.getmondo.terms_and_conditions.TermsAndConditionsActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.d.b.ab;
import kotlin.d.b.y;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000f\u0018\u0000 U2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001UB\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0015\u001a\u00020\rH\u0016J\b\u0010\u0016\u001a\u00020\rH\u0016J\b\u0010\u0017\u001a\u00020\rH\u0016J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u0012\u0010\u001d\u001a\u00020\r2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010 \u001a\u00020\rH\u0014J\u000e\u0010!\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010\"\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010#\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\b\u0010$\u001a\u00020\rH\u0016J\u000e\u0010%\u001a\b\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010'\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010(\u001a\b\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010)\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010*\u001a\b\u0012\u0004\u0012\u00020&0\u0019H\u0016J\u000e\u0010+\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010,\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\u000e\u0010-\u001a\b\u0012\u0004\u0012\u00020\r0\u0019H\u0016J\b\u0010.\u001a\u00020\rH\u0016J\b\u0010/\u001a\u00020\rH\u0016J\b\u00100\u001a\u00020\rH\u0016J\b\u00101\u001a\u00020\rH\u0016J\b\u00102\u001a\u00020\rH\u0016J\b\u00103\u001a\u00020\rH\u0016J\u0010\u00104\u001a\u00020\r2\u0006\u00105\u001a\u000206H\u0002J\u0018\u00107\u001a\u00020\r2\u0006\u00108\u001a\u0002062\u0006\u00109\u001a\u000206H\u0016J\u001a\u0010:\u001a\u00020\r2\u0006\u0010;\u001a\u00020<2\b\u0010=\u001a\u0004\u0018\u00010>H\u0016J \u0010?\u001a\u00020\r2\u0006\u0010@\u001a\u0002062\u0006\u00108\u001a\u0002062\u0006\u00109\u001a\u000206H\u0016J\b\u0010A\u001a\u00020\rH\u0016J\b\u0010B\u001a\u00020\rH\u0016J\b\u0010C\u001a\u00020\rH\u0016J\b\u0010D\u001a\u00020\rH\u0016J\u0018\u0010E\u001a\u00020\r2\u0006\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020GH\u0016J\b\u0010I\u001a\u00020\rH\u0016J\b\u0010J\u001a\u00020\rH\u0016J\b\u0010K\u001a\u00020\rH\u0016J\b\u0010L\u001a\u00020\rH\u0016J\b\u0010M\u001a\u00020\rH\u0016J\b\u0010N\u001a\u00020\rH\u0016J\b\u0010O\u001a\u00020\rH\u0016J\b\u0010P\u001a\u00020\rH\u0016J\b\u0010Q\u001a\u00020\rH\u0016J\b\u0010R\u001a\u00020\rH\u0016J\b\u0010S\u001a\u00020\rH\u0016J\b\u0010T\u001a\u00020\rH\u0016R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR2\u0010\u000b\u001a&\u0012\f\u0012\n \u000e*\u0004\u0018\u00010\r0\r \u000e*\u0012\u0012\f\u0012\n \u000e*\u0004\u0018\u00010\r0\r\u0018\u00010\f0\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u00020\u00108\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006V"},
   d2 = {"Lco/uk/getmondo/settings/SettingsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/settings/SettingsPresenter$View;", "Lco/uk/getmondo/settings/LogOutConfirmationDialogFragment$OnLogOutConfirmedListener;", "()V", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "getAvatarGenerator", "()Lco/uk/getmondo/common/ui/AvatarGenerator;", "avatarGenerator$delegate", "Lkotlin/Lazy;", "logOutConfirmationRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "presenter", "Lco/uk/getmondo/settings/SettingsPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/settings/SettingsPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/settings/SettingsPresenter;)V", "hideMagStripeLoading", "hidePaymentsLoading", "hidePaymentsUi", "onAboutMonzoClicked", "Lio/reactivex/Observable;", "onAddressClicked", "onCloseAccountClicked", "onConfirmLogOutClicked", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onFscsProtectionClicked", "onLimitsClicked", "onLogOutClicked", "onLogOutConfirmed", "onMagStripeStateToggled", "", "onNameClicked", "onNotificationsStateToggled", "onOpenSourceLicensesClicked", "onPaymentsStateToggled", "onPrivacyPolicyClicked", "onShareAccountDetailsClicked", "onTermsAndConditionsClicked", "openAboutMonzo", "openFscsProtection", "openLimits", "openPrivacyPolicy", "openTermsAndConditions", "openUpdateAddress", "openUrl", "url", "", "setAccountInformation", "accountNumber", "sortCode", "setProfileInformation", "profile", "Lco/uk/getmondo/model/Profile;", "created", "Lorg/threeten/bp/LocalDateTime;", "shareAccountDetails", "profileName", "showChangeInformation", "showCloseAccount", "showLogoutConfirmation", "showMagStripeDisabled", "showMagStripeEnabled", "hours", "", "minutes", "showMagStripeLoading", "showNotificationsChecked", "showNotificationsUnchecked", "showOpenSourceLicenses", "showPaymentsBlocked", "showPaymentsDisabled", "showPaymentsEnabled", "showPaymentsLoading", "showPaymentsUi", "showPrepaidUi", "showRetailUi", "showUpdateAddressSupport", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SettingsActivity extends co.uk.getmondo.common.activities.b implements p.a, u.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new kotlin.d.b.w(y.a(SettingsActivity.class), "avatarGenerator", "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;"))};
   public static final SettingsActivity.a c = new SettingsActivity.a((kotlin.d.b.i)null);
   private static final String g = "TAG_CHANGE_PERSONAL_INFORMATION";
   private static final String h = "TAG_ERROR_P2P_BLOCKED";
   private static final String i = "TAG_CLOSE_ACCOUNT";
   private static final String j = "TAG_LOG_OUT_CONFIRMATION";
   private static final String k = "TAG_UPDATE_ADDRESS";
   public u b;
   private final com.b.b.c e = com.b.b.c.a();
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.common.ui.a b() {
         return co.uk.getmondo.common.ui.a.a.a((Context)SettingsActivity.this);
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap l;

   private final void a(String var) {
      (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)this, 2131689487)).a().a((Context)this, Uri.parse(var));
   }

   private final co.uk.getmondo.common.ui.a ae() {
      kotlin.c var = this.f;
      kotlin.reflect.l var = a[0];
      return (co.uk.getmondo.common.ui.a)var.a();
   }

   public void A() {
      a.a().show(this.getSupportFragmentManager(), c.a());
   }

   public void B() {
      x.a.a().show(this.getSupportFragmentManager(), c.e());
   }

   public void C() {
      this.startActivity(SelectAddressActivity.b.a((Context)this));
   }

   public void D() {
      OpenSourceLicensesActivity.a((Context)this);
   }

   public void E() {
      c.a().show(this.getFragmentManager(), c.c());
   }

   public void F() {
      LimitsActivity.b.a((Context)this);
   }

   public void G() {
      this.a("https://monzo.com/about");
   }

   public void H() {
      this.startActivity(TermsAndConditionsActivity.a((Context)this));
   }

   public void I() {
      this.a("https://monzo.com/privacy");
   }

   public void J() {
      this.a("https://monzo.com/fscs-information");
   }

   public void K() {
      p var = p.a();
      var.a((p.a)this);
      var.show(this.getFragmentManager(), c.d());
   }

   public void L() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).setDescription(this.getString(2131362677));
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).d();
   }

   public void M() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).a();
   }

   public void N() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).b();
   }

   public void O() {
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.paymentsTitleTextView)));
      ae.a((View)((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)));
   }

   public void P() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.paymentsTitleTextView));
      ae.b((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView));
   }

   public void Q() {
      co.uk.getmondo.common.d.e.a().show(this.getFragmentManager(), c.b());
   }

   public void R() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)).c();
   }

   public void S() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)).d();
   }

   public void T() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)).a();
   }

   public void U() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)).b();
   }

   public void V() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.notificationsSwitchView)).c();
   }

   public void W() {
      ((SwitchView)this.a(co.uk.getmondo.c.a.notificationsSwitchView)).d();
   }

   public void X() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.fscsProtectionView));
      ae.b(this.a(co.uk.getmondo.c.a.fscsProtectionSeparatorView));
      ae.b(this.a(co.uk.getmondo.c.a.bankAccountInfoViewGroup));
   }

   public void Y() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.paymentsTitleTextView));
      ae.b((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView));
   }

   public View a(int var) {
      if(this.l == null) {
         this.l = new HashMap();
      }

      View var = (View)this.l.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.l.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void a() {
      this.e.a((Object)kotlin.n.a);
   }

   public void a(int var, int var) {
      String var = this.getResources().getQuantityString(2131886086, var, new Object[]{Integer.valueOf(var)});
      String var = this.getResources().getQuantityString(2131886087, var, new Object[]{Integer.valueOf(var)});
      var = this.getResources().getString(2131362248, new Object[]{var, var});
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).setDescription(this.getString(2131362678, new Object[]{var}));
      ((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).c();
   }

   public void a(ac var, LocalDateTime var) {
      kotlin.d.b.l.b(var, "profile");
      ((TextView)this.a(co.uk.getmondo.c.a.nameTextView)).setText((CharSequence)var.a());
      ((TextView)this.a(co.uk.getmondo.c.a.emailTextView)).setText((CharSequence)var.d());
      ((TextView)this.a(co.uk.getmondo.c.a.addressTextView)).setText((CharSequence)co.uk.getmondo.common.k.a.b(var.h()));
      ab var = ab.a;
      String var = this.getString(2131362681);
      kotlin.d.b.l.a(var, "getString(R.string.settings_monzo_user_format)");
      Object[] var = new Object[]{var.g()};
      String var = String.format(var, Arrays.copyOf(var, var.length));
      kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
      Integer var = var.g();
      int var;
      if(var != null) {
         var = var.intValue();
      } else {
         var = 0;
      }

      String var = co.uk.getmondo.common.k.e.a(var);
      CharSequence var = (CharSequence)var;
      boolean var;
      if(var != null && !kotlin.h.j.a(var)) {
         var = false;
      } else {
         var = true;
      }

      if(!var) {
         var = var + " " + var;
      }

      ((TextView)this.a(co.uk.getmondo.c.a.monzoUserNumberTextView)).setText((CharSequence)var);
      if(var != null) {
         var = var.d().a(TextStyle.a, Locale.ENGLISH);
         TextView var = (TextView)this.a(co.uk.getmondo.c.a.memberSinceTextView);
         ab var = ab.a;
         var = this.getString(2131362680);
         kotlin.d.b.l.a(var, "getString(R.string.settings_member_since_format)");
         Object[] var = new Object[]{var, Integer.valueOf(var.b())};
         String var = String.format(var, Arrays.copyOf(var, var.length));
         kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
         var.setText((CharSequence)var);
      }

      var = (int)TypedValue.applyDimension(2, 18.0F, this.getResources().getDisplayMetrics());
      ((ImageView)this.a(co.uk.getmondo.c.a.userImageView)).setImageDrawable(co.uk.getmondo.common.ui.a.b.a(this.ae().a(var.c()), var, (Typeface)null, false, 6, (Object)null));
      ((TextView)this.a(co.uk.getmondo.c.a.appVersionTextView)).setText((CharSequence)this.getString(2131362657, new Object[]{"1.14.1"}));
   }

   public void a(String var, String var) {
      kotlin.d.b.l.b(var, "accountNumber");
      kotlin.d.b.l.b(var, "sortCode");
      ((TextView)this.a(co.uk.getmondo.c.a.accountNumberTextView)).setText((CharSequence)var);
      ((TextView)this.a(co.uk.getmondo.c.a.sortCodeTextView)).setText((CharSequence)var);
   }

   public void a(String var, String var, String var) {
      kotlin.d.b.l.b(var, "profileName");
      kotlin.d.b.l.b(var, "accountNumber");
      kotlin.d.b.l.b(var, "sortCode");
      this.startActivity(co.uk.getmondo.common.k.j.a(this.getString(2131362689, new Object[]{var, var, var})));
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((LinearLayout)this.a(co.uk.getmondo.c.a.nameViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n c() {
      io.reactivex.n var = com.b.a.c.c.a((LinearLayout)this.a(co.uk.getmondo.c.a.addressViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n d() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.aboutMonzoView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n e() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.termsAndConditionsView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n f() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.privacyPolicyView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n g() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.fscsProtectionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n h() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.limitsTextView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n i() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.openSourceLicensesView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n j() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.logOutView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n k() {
      com.b.b.c var = this.e;
      kotlin.d.b.l.a(var, "logOutConfirmationRelay");
      return (io.reactivex.n)var;
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034205);
      this.l().a(this);
      u var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((u.a)this);
   }

   protected void onDestroy() {
      u var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   public io.reactivex.n v() {
      io.reactivex.n var = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.closeAccountView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n w() {
      io.reactivex.n var = com.b.a.c.c.a((SwitchView)this.a(co.uk.getmondo.c.a.notificationsSwitchView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.a((kotlin.n)var));
         }

         public final boolean a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!((SwitchView)SettingsActivity.this.a(co.uk.getmondo.c.a.notificationsSwitchView)).e()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      }));
      kotlin.d.b.l.a(var, "notificationsSwitchView.…onsSwitchView.isChecked }");
      return var;
   }

   public io.reactivex.n x() {
      io.reactivex.n var = com.b.a.c.c.a((SwitchView)this.a(co.uk.getmondo.c.a.magStripeSwitchView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.a((kotlin.n)var));
         }

         public final boolean a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!((SwitchView)SettingsActivity.this.a(co.uk.getmondo.c.a.magStripeSwitchView)).e()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      }));
      kotlin.d.b.l.a(var, "magStripeSwitchView.clic…ipeSwitchView.isChecked }");
      return var;
   }

   public io.reactivex.n y() {
      io.reactivex.n var = com.b.a.c.c.a((SwitchView)this.a(co.uk.getmondo.c.a.paymentsSwitchView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.a((kotlin.n)var));
         }

         public final boolean a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!((SwitchView)SettingsActivity.this.a(co.uk.getmondo.c.a.paymentsSwitchView)).e()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      }));
      kotlin.d.b.l.a(var, "paymentsSwitchView.click…ntsSwitchView.isChecked }");
      return var;
   }

   public io.reactivex.n z() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.shareAccountDetailsButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\u0012H\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/settings/SettingsActivity$Companion;", "", "()V", "TAG_CHANGE_PERSONAL_INFORMATION", "", "getTAG_CHANGE_PERSONAL_INFORMATION", "()Ljava/lang/String;", "TAG_CLOSE_ACCOUNT", "getTAG_CLOSE_ACCOUNT", "TAG_ERROR_P2P_BLOCKED", "getTAG_ERROR_P2P_BLOCKED", "TAG_LOG_OUT_CONFIRMATION", "getTAG_LOG_OUT_CONFIRMATION", "TAG_UPDATE_ADDRESS", "getTAG_UPDATE_ADDRESS", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "start", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      private final String a() {
         return SettingsActivity.g;
      }

      private final String b() {
         return SettingsActivity.h;
      }

      private final String c() {
         return SettingsActivity.i;
      }

      private final String d() {
         return SettingsActivity.j;
      }

      private final String e() {
         return SettingsActivity.k;
      }

      public final void a(Context var) {
         kotlin.d.b.l.b(var, "context");
         var.startActivity(new Intent(var, SettingsActivity.class));
      }

      public final Intent b(Context var) {
         kotlin.d.b.l.b(var, "context");
         return new Intent(var, SettingsActivity.class);
      }
   }
}
