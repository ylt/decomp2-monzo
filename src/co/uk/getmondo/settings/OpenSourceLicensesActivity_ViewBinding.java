package co.uk.getmondo.settings;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity a;

   public OpenSourceLicensesActivity_ViewBinding(OpenSourceLicensesActivity var, View var) {
      this.a = var;
      var.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131821008, "field 'recyclerView'", RecyclerView.class);
      var.appBarLayout = (AppBarLayout)Utils.findRequiredViewAsType(var, 2131821005, "field 'appBarLayout'", AppBarLayout.class);
      var.collapsingToolbarLayout = (CollapsingToolbarLayout)Utils.findRequiredViewAsType(var, 2131821006, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
      var.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821007, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.recyclerView = null;
         var.appBarLayout = null;
         var.collapsingToolbarLayout = null;
         var.descriptionTextView = null;
      }
   }
}
