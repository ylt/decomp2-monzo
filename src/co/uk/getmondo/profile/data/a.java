package co.uk.getmondo.profile.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiPostcodeResponse;
import co.uk.getmondo.api.model.ApiProfile;
import co.uk.getmondo.api.model.LegacyApiAddress;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.s;
import co.uk.getmondo.d.a.i;
import co.uk.getmondo.d.a.p;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.d.b.y;
import kotlin.reflect.e;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0018R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/profile/data/ProfileManager;", "", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "profileApi", "Lco/uk/getmondo/profile/data/MonzoProfileApi;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "idempotencyGenerator", "Lco/uk/getmondo/topup/util/IdempotencyGenerator;", "(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/topup/util/IdempotencyGenerator;)V", "addressMapper", "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;", "lookupPostcode", "Lio/reactivex/Single;", "", "Lco/uk/getmondo/model/LegacyAddress;", "postcode", "", "syncProfile", "Lio/reactivex/Completable;", "updateAddress", "address", "challenge", "Lco/uk/getmondo/common/pin/data/model/Challenge;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final i a;
   private final MonzoApi b;
   private final MonzoProfileApi c;
   private final d d;
   private final co.uk.getmondo.topup.a.a e;

   public a(MonzoApi var, MonzoProfileApi var, d var, co.uk.getmondo.topup.a.a var) {
      l.b(var, "monzoApi");
      l.b(var, "profileApi");
      l.b(var, "accountService");
      l.b(var, "idempotencyGenerator");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.a = new i();
   }

   public final io.reactivex.b a() {
      io.reactivex.b var = this.c.profile().d((h)(new b((kotlin.d.a.b)(new kotlin.d.a.b(new p()) {
         public final ac a(ApiProfile var) {
            l.b(var, "p1");
            return ((p)this.b).a(var);
         }

         public final e a() {
            return y.a(p.class);
         }

         public final String b() {
            return "apply";
         }

         public final String c() {
            return "apply(Lco/uk/getmondo/api/model/ApiProfile;)Lco/uk/getmondo/model/Profile;";
         }
      })))).c((g)(new g() {
         public final void a(ac var) {
            a.this.d.a(var);
         }
      })).c();
      l.a(var, "profileApi.profile()\n   …         .toCompletable()");
      return var;
   }

   public final io.reactivex.b a(final s var, final co.uk.getmondo.common.pin.a.a.a var) {
      l.b(var, "address");
      l.b(var, "challenge");
      io.reactivex.b var = this.d.c().d((h)null.a).c((h)(new h() {
         public final io.reactivex.b a(String varx) {
            l.b(varx, "userId");
            MonzoProfileApi var = a.this.c;
            String[] var = var.h();
            String var = var.i();
            String var = var.b();
            String varx = var.c();
            String var = var.j();
            String var = var.b();
            String var = var.a().a();
            String var = a.this.e.a(var);
            l.a(var, "idempotencyGenerator.getKey(address)");
            return var.updateAddress(varx, var, var, var, varx, var, var, var, var);
         }
      })).b((io.reactivex.d)this.a());
      l.a(var, "accountService.user()\n  …  .andThen(syncProfile())");
      return var;
   }

   public final v a(String var) {
      l.b(var, "postcode");
      v var = this.b.lookupPostcode(var, co.uk.getmondo.d.i.Companion.a().e()).a((h)(new h() {
         public final v a(ApiPostcodeResponse var) {
            l.b(var, "it");
            return n.fromIterable((Iterable)var.a()).map((h)(new h() {
               public final s a(LegacyApiAddress var) {
                  l.b(var, "it");
                  return a.this.a.a(var);
               }
            })).toList();
         }
      }));
      l.a(var, "monzoApi.lookupPostcode(…oList()\n                }");
      return var;
   }
}
