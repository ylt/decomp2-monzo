package co.uk.getmondo.profile.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.common.accounts.d;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new c(var, var, var, var);
   }

   public a a() {
      return new a((MonzoApi)this.b.b(), (MonzoProfileApi)this.c.b(), (d)this.d.b(), (co.uk.getmondo.topup.a.a)this.e.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
