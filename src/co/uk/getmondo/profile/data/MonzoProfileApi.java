package co.uk.getmondo.profile.data;

import io.reactivex.v;
import kotlin.Metadata;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\t\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H'Jw\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\b2\u0010\b\u0001\u0010\t\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\n2\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\b2\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\b2\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\b2\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\b2\b\b\u0001\u0010\u000f\u001a\u00020\b2\b\b\u0001\u0010\u0010\u001a\u00020\b2\b\b\u0001\u0010\u0011\u001a\u00020\bH'¢\u0006\u0002\u0010\u0012¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/profile/data/MonzoProfileApi;", "", "profile", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/ApiProfile;", "updateAddress", "Lio/reactivex/Completable;", "userId", "", "addressStreetLines", "", "addressLocality", "addressAdminArea", "addressPostalCode", "addressCountry", "challenge", "challengeType", "idempotencyKey", "(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Completable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface MonzoProfileApi {
   @GET("profile")
   v profile();

   @FormUrlEncoded
   @POST("profile/update_address")
   io.reactivex.b updateAddress(@Field("user_id") String var, @Field("address[street_address][]") String[] var, @Field("address[locality]") String var, @Field("address[administrative_area]") String var, @Field("address[postal_code]") String var, @Field("address[country]") String var, @Field("challenge") String var, @Field("challenge_type") String var, @Field("idempotency_key") String var);

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class DefaultImpls {
      // $FF: synthetic method
      @FormUrlEncoded
      @POST("profile/update_address")
      public static io.reactivex.b updateAddress$default(MonzoProfileApi var, String var, String[] var, String var, String var, String var, String var, String var, String var, String var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: updateAddress");
         } else {
            if((var & 8) != 0) {
               var = "";
            }

            return var.updateAddress(var, var, var, var, var, var, var, var, var);
         }
      }
   }
}
