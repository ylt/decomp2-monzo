package co.uk.getmondo.profile.address;

import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.ProgressButton;
import io.reactivex.p;
import io.reactivex.c.q;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B%\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$J\f\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$J\f\u0010'\u001a\b\u0012\u0004\u0012\u00020(0$J\b\u0010)\u001a\u00020%H\u0016J\b\u0010*\u001a\u00020%H\u0016J\b\u0010+\u001a\u00020%H\u0016J\b\u0010,\u001a\u00020%H\u0014J\b\u0010-\u001a\u00020%H\u0014J\u000e\u0010.\u001a\b\u0012\u0004\u0012\u00020/0$H\u0016J\u000e\u00100\u001a\b\u0012\u0004\u0012\u00020%0$H\u0016J\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u00170$J\f\u00102\u001a\b\u0012\u0004\u0012\u00020%0$J\u0006\u00103\u001a\u00020%J\b\u00104\u001a\u00020%H\u0016J\b\u00105\u001a\u00020%H\u0016J\u0014\u00106\u001a\u00020%2\f\u00107\u001a\b\u0012\u0004\u0012\u00020(08J\u0006\u00109\u001a\u00020%R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R$\u0010\u0012\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R$\u0010\u0014\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011R\u0011\u0010\u0016\u001a\u00020\u00178F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u001c\u001a\u00020\u00172\u0006\u0010\f\u001a\u00020\u00178F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001d\u0010\u0019\"\u0004\b\u001e\u0010\u001fR$\u0010 \u001a\u00020\u00172\u0006\u0010\f\u001a\u00020\u00178F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b!\u0010\u0019\"\u0004\b\"\u0010\u001f¨\u0006:"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressSelectionView;", "Landroid/widget/LinearLayout;", "Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "addressAdapter", "Lco/uk/getmondo/profile/address/AddressAdapter;", "value", "", "isAddressLoading", "()Z", "setAddressLoading", "(Z)V", "isPrimaryButtonEnabled", "setPrimaryButtonEnabled", "isPrimaryButtonVisible", "setPrimaryButtonVisible", "postcode", "", "getPostcode", "()Ljava/lang/String;", "presenter", "Lco/uk/getmondo/profile/address/AddressSelectionPresenter;", "primaryButtonText", "getPrimaryButtonText", "setPrimaryButtonText", "(Ljava/lang/String;)V", "title", "getTitle", "setTitle", "addressNotInListButtonClicks", "Lio/reactivex/Observable;", "", "addressResidenceButtonClicks", "addressSelected", "Lco/uk/getmondo/api/model/Address;", "clearPostcodeError", "hideAddressNotInListButton", "hideAddressPicker", "onAttachedToWindow", "onDetachedFromWindow", "onPostcodeChanged", "", "onViewedAddresses", "postcodeReady", "primaryButtonClicks", "showAddressNotFoundError", "showAddressNotInListButton", "showAddressPicker", "showAddresses", "addresses", "", "showInvalidPostcodeError", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressSelectionView extends LinearLayout implements e.a {
   private final a a;
   private final e b;
   private HashMap c;

   public AddressSelectionView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AddressSelectionView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (kotlin.d.b.i)null);
   }

   public AddressSelectionView(Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      this.a = new a((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
      this.b = new e();
      this.setOrientation(1);
      LayoutInflater.from(var).inflate(2131034428, (ViewGroup)this);
      LinearLayoutManager var = new LinearLayoutManager(var);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)(new LinearLayoutManager(var)));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.a);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.e(var, var.h(), (Drawable)null, this.getResources().getDimensionPixelSize(2131427603), 0, 0, 0, 116, (kotlin.d.b.i)null)));
      if(var != null) {
         TypedArray var = var.obtainStyledAttributes(var, co.uk.getmondo.c.b.AddressSelectionView, 0, 0);
         String var = var.getString(1);
         if(var == null) {
            var = "";
         }

         this.setPrimaryButtonText(var);
         var = var.getString(0);
         if(var == null) {
            var = "";
         }

         this.setTitle(var);
         var.recycle();
      }

   }

   // $FF: synthetic method
   public AddressSelectionView(Context var, AttributeSet var, int var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      com.b.a.a var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      io.reactivex.n var = var.b();
      kotlin.d.b.l.a(var, "addressPostcodeEditText.…nges().skipInitialValue()");
      return var;
   }

   public final void a(final List var) {
      kotlin.d.b.l.b(var, "addresses");
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).clearFocus();
      ae.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      this.postDelayed((Runnable)(new Runnable() {
         public final void run() {
            if(AddressSelectionView.this.isAttachedToWindow()) {
               AddressSelectionView.this.a.a(var);
               ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).a(0);
               AddressSelectionView.this.n();
            }

         }
      }), 300L);
   }

   public io.reactivex.n b() {
      io.reactivex.n var = io.reactivex.n.create((p)(new p() {
         public final void a(final io.reactivex.o var) {
            kotlin.d.b.l.b(var, "emitter");
            ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).setOnScrollListener((android.support.v7.widget.RecyclerView.m)(new android.support.v7.widget.RecyclerView.m() {
               public void onScrollStateChanged(RecyclerView varx, int var) {
                  super.onScrollStateChanged(varx, var);
                  var.a(kotlin.n.a);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).setOnScrollListener((android.support.v7.widget.RecyclerView.m)null);
               }
            }));
         }
      })).filter((q)(new q() {
         public final boolean a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).canScrollVertically(130)) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "Observable.create<Unit> …\n                .map { }");
      return var;
   }

   public void c() {
      if(((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).getVisibility() != 0) {
         final Context var = this.getContext();
         if(var instanceof Activity) {
            ((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).post((Runnable)(new Runnable() {
               public final void run() {
                  ((Activity)var).getWindow().setSoftInputMode(36);
               }
            }));
         }

         ae.a((View)((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)));
      }

   }

   public void d() {
      if(((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).getVisibility() == 0) {
         ae.b((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton));
         final Context var = this.getContext();
         if(var instanceof Activity) {
            ((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).post((Runnable)(new Runnable() {
               public final void run() {
                  ((Activity)var).getWindow().setSoftInputMode(20);
               }
            }));
         }
      }

   }

   public void e() {
      float var = (float)this.getResources().getDimensionPixelSize(2131427426);
      if(((ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout)).getTranslationY() != var) {
         ScrollView var = (ScrollView)this.a(co.uk.getmondo.c.a.addressScrollView);
         LayoutParams var = new LayoutParams(-1, 0);
         var.weight = 1.0F;
         var.setLayoutParams((android.view.ViewGroup.LayoutParams)var);
         ((ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout)).setLayoutParams((android.view.ViewGroup.LayoutParams)(new LayoutParams(-1, -2)));
         ConstraintLayout var = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
         ae.b((View)var);
         var.animate().translationY(var).setInterpolator((TimeInterpolator)(new android.support.v4.view.b.b()));
      }

   }

   public void f() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)null);
   }

   public final io.reactivex.n g() {
      io.reactivex.n var = com.b.a.c.c.a((TextInputEditText)this.a(co.uk.getmondo.c.a.addressResidenceEditText)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public final String getPostcode() {
      return ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).getText().toString();
   }

   public final String getPrimaryButtonText() {
      return ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).getText().toString();
   }

   public final String getTitle() {
      return ((TextView)this.a(co.uk.getmondo.c.a.addressBodyText)).getText().toString();
   }

   public final io.reactivex.n h() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public final io.reactivex.n i() {
      io.reactivex.n var = this.h().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            String var = AddressSelectionView.this.getPostcode();
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            } else {
               return kotlin.h.j.b((CharSequence)var).toString();
            }
         }
      }));
      kotlin.d.b.l.a(var, "primaryButtonClicks().map { postcode.trim() }");
      return var;
   }

   public final io.reactivex.n j() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public final io.reactivex.n k() {
      io.reactivex.n var = io.reactivex.n.create((p)(new p() {
         public final void a(final io.reactivex.o var) {
            kotlin.d.b.l.b(var, "emitter");
            AddressSelectionView.this.a.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(Address varx) {
                  kotlin.d.b.l.b(varx, "it");
                  var.a(varx);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  AddressSelectionView.this.a.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…stener = null }\n        }");
      return var;
   }

   public final void l() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)this.getResources().getString(2131362495));
   }

   public final void m() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)this.getResources().getString(2131362005));
   }

   public void n() {
      ScrollView var = (ScrollView)this.a(co.uk.getmondo.c.a.addressScrollView);
      LayoutParams var = new LayoutParams(-1, -2);
      var.weight = 0.0F;
      var.setLayoutParams((android.view.ViewGroup.LayoutParams)var);
      ConstraintLayout var = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
      var = new LayoutParams(-1, 0);
      var.weight = 1.0F;
      var.topMargin = this.getResources().getDimensionPixelSize(2131427605);
      var.setLayoutParams((android.view.ViewGroup.LayoutParams)var);
      ConstraintLayout var = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
      ae.a((View)var);
      var.animate().translationY(0.0F).setInterpolator((TimeInterpolator)(new android.support.v4.view.b.b()));
   }

   protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.b.a((e.a)this);
      this.post((Runnable)(new Runnable() {
         public final void run() {
            ae.a((EditText)((TextInputEditText)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressPostcodeEditText)));
         }
      }));
   }

   protected void onDetachedFromWindow() {
      this.b.b();
      ae.c(this);
      super.onDetachedFromWindow();
   }

   public void setAddressLoading(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setLoading(var);
   }

   public void setPrimaryButtonEnabled(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setEnabled(var);
   }

   public final void setPrimaryButtonText(String var) {
      kotlin.d.b.l.b(var, "value");
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setText((CharSequence)var);
   }

   public void setPrimaryButtonVisible(boolean var) {
      ProgressButton var = (ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton);
      byte var;
      if(var) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }

   public final void setTitle(String var) {
      kotlin.d.b.l.b(var, "value");
      ((TextView)this.a(co.uk.getmondo.c.a.addressBodyText)).setText((CharSequence)var);
   }
}
