package co.uk.getmondo.profile.address;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.ApiAddress;
import co.uk.getmondo.d.s;
import io.reactivex.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u001a\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u0015R(\u0010\u001d\u001a\u0004\u0018\u00010\n2\b\u0010\u001c\u001a\u0004\u0018\u00010\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\f\"\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\"\u0010\u0015¨\u0006#"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressInputView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "address", "Lco/uk/getmondo/api/model/Address;", "getAddress", "()Lco/uk/getmondo/api/model/Address;", "addressInputValid", "Lio/reactivex/Observable;", "", "getAddressInputValid", "()Lio/reactivex/Observable;", "city", "", "getCity", "()Ljava/lang/String;", "legacyAddress", "Lco/uk/getmondo/model/LegacyAddress;", "getLegacyAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "postcode", "getPostcode", "value", "preFilledAddress", "getPreFilledAddress", "setPreFilledAddress", "(Lco/uk/getmondo/api/model/Address;)V", "street", "getStreet", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressInputView extends LinearLayout {
   private Address a;
   private HashMap b;

   public AddressInputView(Context var) {
      this(var, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AddressInputView(Context var, AttributeSet var) {
      this(var, var, 0, 4, (kotlin.d.b.i)null);
   }

   public AddressInputView(Context var, AttributeSet var, int var) {
      kotlin.d.b.l.b(var, "context");
      super(var, var, var);
      this.setOrientation(1);
      this.setFocusable(true);
      this.setFocusableInTouchMode(true);
      LayoutInflater.from(var).inflate(2131034427, (ViewGroup)this);
   }

   // $FF: synthetic method
   public AddressInputView(Context var, AttributeSet var, int var, int var, kotlin.d.b.i var) {
      if((var & 2) != 0) {
         var = (AttributeSet)null;
      }

      if((var & 4) != 0) {
         var = 0;
      }

      this(var, var, var);
   }

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final Address getAddress() {
      Iterable var = (Iterable)kotlin.a.m.a((Collection)kotlin.h.j.b((CharSequence)this.getStreet(), new String[]{","}, false, 0, 6, (Object)null), this.getCity());
      Collection var = (Collection)(new ArrayList(kotlin.a.m.a(var, 10)));
      Iterator var = var.iterator();

      while(var.hasNext()) {
         String var = (String)var.next();
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
         }

         var.add(kotlin.h.j.b((CharSequence)var).toString());
      }

      var = (Iterable)((List)var);
      var = (Collection)(new ArrayList());
      var = var.iterator();

      while(var.hasNext()) {
         Object var = var.next();
         boolean var;
         if(!kotlin.h.j.a((CharSequence)((String)var))) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            var.add(var);
         }
      }

      List var = (List)var;
      Address var = this.a;
      if(var == null || !(var instanceof ApiAddress) || !kotlin.d.b.l.a(var.a(), var) || !kotlin.d.b.l.a(((ApiAddress)var).c(), this.getPostcode())) {
         var = (Address)(new ApiAddress((String)null, var, "", this.getPostcode(), co.uk.getmondo.d.i.UNITED_KINGDOM.e()));
      }

      return var;
   }

   public final io.reactivex.n getAddressInputValid() {
      com.b.a.a var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      r var = (r)var;
      com.b.a.a var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      r var = (r)var;
      var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      io.reactivex.n var = io.reactivex.n.combineLatest(var, var, (r)var, (io.reactivex.c.i)null.a).distinctUntilChanged();
      kotlin.d.b.l.a(var, "Observable.combineLatest…  .distinctUntilChanged()");
      return var;
   }

   public final String getCity() {
      String var = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText)).getText().toString();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var).toString();
      }
   }

   public final s getLegacyAddress() {
      return new s(this.getPostcode(), co.uk.getmondo.common.k.a.a(this.getStreet()), this.getCity(), co.uk.getmondo.d.i.UNITED_KINGDOM.e(), (String)null, 16, (kotlin.d.b.i)null);
   }

   public final String getPostcode() {
      String var = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).getText().toString();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var).toString();
      }
   }

   public final Address getPreFilledAddress() {
      return this.a;
   }

   public final String getStreet() {
      String var = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).getText().toString();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var).toString();
      }
   }

   public final void setPreFilledAddress(Address var) {
      this.a = var;
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressStreetInputLayout)).setHintAnimationEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressCityInputLayout)).setHintAnimationEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setHintAnimationEnabled(false);
      String var;
      if(var instanceof ApiAddress) {
         var = (String)kotlin.a.m.g(var.a());
      } else if(var instanceof s) {
         var = ((s)var).i();
      } else {
         var = "";
      }

      List var;
      if(var instanceof ApiAddress) {
         var = kotlin.a.m.d(var.a(), 1);
      } else {
         label44: {
            if(var != null) {
               List var = var.a();
               var = var;
               if(var != null) {
                  break label44;
               }
            }

            var = kotlin.a.m.a();
         }
      }

      Iterable var = (Iterable)var;
      Collection var = (Collection)(new ArrayList());
      Iterator var = var.iterator();

      while(var.hasNext()) {
         Object var = var.next();
         boolean var;
         if(!kotlin.h.j.a((CharSequence)((String)var))) {
            var = true;
         } else {
            var = false;
         }

         if(var) {
            var.add(var);
         }
      }

      String var = kotlin.a.m.a((Iterable)((List)var), (CharSequence)", ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null);
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).setText((CharSequence)var);
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText)).setText((CharSequence)var);
      TextInputEditText var = (TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText);
      if(var != null) {
         var = var.c();
      } else {
         var = null;
      }

      var.setText((CharSequence)var);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressStreetInputLayout)).setHintAnimationEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressCityInputLayout)).setHintAnimationEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setHintAnimationEnabled(true);
      if(var != null) {
         this.requestFocus();
      } else {
         ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).requestFocus();
      }

   }
}
