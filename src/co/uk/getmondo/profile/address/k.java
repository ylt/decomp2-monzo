package co.uk.getmondo.profile.address;

import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/profile/address/SelectAddressPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/profile/address/SelectAddressPresenter$View;", "profileManager", "Lco/uk/getmondo/profile/data/ProfileManager;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analytics", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/profile/data/ProfileManager;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.profile.data.a c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public k(co.uk.getmondo.profile.data.a var, u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "profileManager");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analytics");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   public void a(final k.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.Companion.aN());
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.a();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.e();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new l(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onResidenceClicked(…howUkOnly() }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.b().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(k.this.c.a(varx).b(k.this.d).a(k.this.e).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.a(false);
                  var.f();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(List varx, Throwable var) {
                  var.a(true);
                  var.g();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = k.this.f;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })));
         }
      }));
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List varx) {
            k.this.g.a(Impression.Companion.aO());
            var.h();
            if(varx.isEmpty()) {
               var.j();
               var.i();
            } else {
               k.a var = var;
               kotlin.d.b.l.a(varx, "it");
               var.a(varx);
            }

         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new l(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onContinueClicked()…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.n var = var.c();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Address varx) {
            var.a(varx);
         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new l(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onAddressSelected()…Address(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.d();
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.a((Address)null);
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new l(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onAddressNotInListC…dress(null) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\b\u0010\u0007\u001a\u00020\u0004H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\tH&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\tH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\tH&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\tH&J\u0012\u0010\u000f\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000bH&J\u0010\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0013H&J\b\u0010\u0014\u001a\u00020\u0004H&J\b\u0010\u0015\u001a\u00020\u0004H&J\b\u0010\u0016\u001a\u00020\u0004H&J\u0016\u0010\u0017\u001a\u00020\u00042\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0019H&J\b\u0010\u001a\u001a\u00020\u0004H&J\b\u0010\u001b\u001a\u00020\u0004H&¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/profile/address/SelectAddressPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideAddressLoading", "", "hideAddressNotFound", "hideAddressNotInList", "hideContinue", "onAddressNotInListClicked", "Lio/reactivex/Observable;", "onAddressSelected", "Lco/uk/getmondo/api/model/Address;", "onContinueClicked", "", "onResidenceClicked", "openConfirmAddress", "address", "setContinueEnabled", "enabled", "", "showAddressLoading", "showAddressNotFound", "showAddressNotInList", "showAddresses", "addresses", "", "showContinue", "showUkOnly", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Address var);

      void a(List var);

      void a(boolean var);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
