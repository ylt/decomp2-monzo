package co.uk.getmondo.profile.address;

import co.uk.getmondo.api.model.Address;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressModule;", "", "address", "Lco/uk/getmondo/api/model/Address;", "(Lco/uk/getmondo/api/model/Address;)V", "getAddress", "()Lco/uk/getmondo/api/model/Address;", "provideAddress", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final Address a;

   public c(Address var) {
      this.a = var;
   }

   public final Address a() {
      return this.a;
   }
}
