package co.uk.getmondo.profile.address;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.pin.PinEntryActivity;
import co.uk.getmondo.d.s;
import co.uk.getmondo.settings.SettingsActivity;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005¢\u0006\u0002\u0010\u0003J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00050\u0012H\u0016J\u0012\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u001a\u001a\u00020\u00162\b\u0010\u0004\u001a\u0004\u0018\u00010\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/profile/address/EnterAddressActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;", "()V", "address", "Lco/uk/getmondo/model/LegacyAddress;", "kotlin.jvm.PlatformType", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "address$delegate", "Lkotlin/Lazy;", "presenter", "Lco/uk/getmondo/profile/address/EnterAddressPresenter;", "getPresenter", "()Lco/uk/getmondo/profile/address/EnterAddressPresenter;", "setPresenter", "(Lco/uk/getmondo/profile/address/EnterAddressPresenter;)V", "onAddressInputValid", "Lio/reactivex/Observable;", "", "onConfirmAddress", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "openPinEntry", "setAddress", "Lco/uk/getmondo/api/model/Address;", "setConfirmEnabled", "enabled", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class EnterAddressActivity extends co.uk.getmondo.common.activities.b implements g.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(EnterAddressActivity.class), "address", "getAddress()Lco/uk/getmondo/model/LegacyAddress;"))};
   public static final EnterAddressActivity.a c = new EnterAddressActivity.a((kotlin.d.b.i)null);
   public g b;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final s b() {
         return (s)EnterAddressActivity.this.getIntent().getParcelableExtra("KEY_ADDRESS");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap f;

   private final s c() {
      kotlin.c var = this.e;
      kotlin.reflect.l var = a[0];
      return (s)var.a();
   }

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.f.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      return ((AddressInputView)this.a(co.uk.getmondo.c.a.profileAddressInputView)).getAddressInputValid();
   }

   public void a(Address var) {
      ((AddressInputView)this.a(co.uk.getmondo.c.a.profileAddressInputView)).setPreFilledAddress(var);
   }

   public void a(s var) {
      kotlin.d.b.l.b(var, "address");
      Intent var = SettingsActivity.c.b((Context)this).addFlags(67108864);
      PinEntryActivity.a var = PinEntryActivity.c;
      Context var = (Context)this;
      kotlin.d.b.l.a(var, "confirmationIntent");
      this.startActivity(var.a(var, (co.uk.getmondo.common.pin.a.a.b)(new co.uk.getmondo.common.pin.a.a.b.a(var, var, (Impression.PinFrom)null, 4, (kotlin.d.b.i)null))));
   }

   public void a(boolean var) {
      ((Button)this.a(co.uk.getmondo.c.a.addressConfirmButton)).setEnabled(var);
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.addressConfirmButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final s a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            return ((AddressInputView)EnterAddressActivity.this.a(co.uk.getmondo.c.a.profileAddressInputView)).getLegacyAddress();
         }
      }));
      kotlin.d.b.l.a(var, "addressConfirmButton.cli…InputView.legacyAddress }");
      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034165);
      this.l().a(new c((Address)this.c())).a(this);
      g var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/profile/address/EnterAddressActivity$Companion;", "", "()V", "KEY_ADDRESS", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "address", "Lco/uk/getmondo/api/model/Address;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, Address var) {
         kotlin.d.b.l.b(var, "context");
         Intent var = (new Intent(var, EnterAddressActivity.class)).putExtra("KEY_ADDRESS", (Parcelable)var);
         kotlin.d.b.l.a(var, "Intent(context, EnterAdd…tra(KEY_ADDRESS, address)");
         return var;
      }
   }
}
