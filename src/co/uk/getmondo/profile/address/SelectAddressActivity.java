package co.uk.getmondo.profile.address;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import co.uk.getmondo.api.model.Address;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0004\u0018\u0000 &2\u00020\u00012\u00020\u0002:\u0001&B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0016J\b\u0010\r\u001a\u00020\u000bH\u0016J\b\u0010\u000e\u001a\u00020\u000bH\u0016J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0010H\u0016J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u0010H\u0016J\u0012\u0010\u0015\u001a\u00020\u000b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u0012\u0010\u0019\u001a\u00020\u000b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0012H\u0016J\u0010\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\b\u0010\u001e\u001a\u00020\u000bH\u0016J\b\u0010\u001f\u001a\u00020\u000bH\u0016J\b\u0010 \u001a\u00020\u000bH\u0016J\u0016\u0010!\u001a\u00020\u000b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120#H\u0016J\b\u0010$\u001a\u00020\u000bH\u0016J\b\u0010%\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006'"},
   d2 = {"Lco/uk/getmondo/profile/address/SelectAddressActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/profile/address/SelectAddressPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/profile/address/SelectAddressPresenter;", "getPresenter", "()Lco/uk/getmondo/profile/address/SelectAddressPresenter;", "setPresenter", "(Lco/uk/getmondo/profile/address/SelectAddressPresenter;)V", "hideAddressLoading", "", "hideAddressNotFound", "hideAddressNotInList", "hideContinue", "onAddressNotInListClicked", "Lio/reactivex/Observable;", "onAddressSelected", "Lco/uk/getmondo/api/model/Address;", "onContinueClicked", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResidenceClicked", "openConfirmAddress", "address", "setContinueEnabled", "enabled", "", "showAddressLoading", "showAddressNotFound", "showAddressNotInList", "showAddresses", "addresses", "", "showContinue", "showUkOnly", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SelectAddressActivity extends co.uk.getmondo.common.activities.b implements k.a {
   public static final SelectAddressActivity.a b = new SelectAddressActivity.a((kotlin.d.b.i)null);
   public k a;
   private HashMap c;

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).g();
   }

   public void a(Address var) {
      this.startActivity(EnterAddressActivity.c.a((Context)this, var));
   }

   public void a(List var) {
      kotlin.d.b.l.b(var, "addresses");
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).a(var);
   }

   public void a(boolean var) {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).setPrimaryButtonEnabled(var);
   }

   public io.reactivex.n b() {
      io.reactivex.n var = ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).h().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            return ((AddressSelectionView)SelectAddressActivity.this.a(co.uk.getmondo.c.a.addressSelectionView)).getPostcode();
         }
      }));
      kotlin.d.b.l.a(var, "addressSelectionView.pri…sSelectionView.postcode }");
      return var;
   }

   public io.reactivex.n c() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).k();
   }

   public io.reactivex.n d() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).j();
   }

   public void e() {
      n.c.a().show(this.getSupportFragmentManager(), n.class.getName());
   }

   public void f() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).setAddressLoading(true);
   }

   public void g() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).setAddressLoading(false);
   }

   public void h() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).setPrimaryButtonVisible(false);
   }

   public void i() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).c();
   }

   public void j() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.addressSelectionView)).l();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034203);
      this.l().a(this);
      this.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.selectAddressToolbar));
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(true);
      }

      k var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((k.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/profile/address/SelectAddressActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var) {
         kotlin.d.b.l.b(var, "context");
         return new Intent(var, SelectAddressActivity.class);
      }
   }
}
