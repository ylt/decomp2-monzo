package co.uk.getmondo.profile.address;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.q;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u0012\u0010\u0013\u001a\u00020\u00142\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/profile/address/UkOnlyDialogFragment;", "Landroid/support/v4/app/DialogFragment;", "()V", "analytics", "Lco/uk/getmondo/common/AnalyticsService;", "getAnalytics", "()Lco/uk/getmondo/common/AnalyticsService;", "setAnalytics", "(Lco/uk/getmondo/common/AnalyticsService;)V", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateDialog", "Landroid/app/Dialog;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class n extends android.support.v4.app.i {
   public static final n.a c = new n.a((kotlin.d.b.i)null);
   public q a;
   public co.uk.getmondo.common.a b;
   private HashMap d;

   public final q a() {
      q var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("intercomService");
      }

      return var;
   }

   public final co.uk.getmondo.common.a b() {
      co.uk.getmondo.common.a var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("analytics");
      }

      return var;
   }

   public void c() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a((Context)this.getActivity()).b()).a(new co.uk.getmondo.common.h.b.c((Activity)this.getActivity())).a().a(this);
   }

   public Dialog onCreateDialog(Bundle var) {
      AlertDialog var = (new Builder((Context)this.getActivity(), 2131493136)).setTitle(2131362003).setMessage(2131362002).setPositiveButton((CharSequence)this.getString(2131362666), (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            n.this.a().a();
            n.this.b().a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.ADDRESS_CHANGE, (String)null, (String)null, 6, (Object)null));
         }
      })).setNegativeButton((CharSequence)this.getString(2131362499), (OnClickListener)null).create();
      kotlin.d.b.l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.c();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/profile/address/UkOnlyDialogFragment$Companion;", "", "()V", "newInstance", "Lco/uk/getmondo/profile/address/UkOnlyDialogFragment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final n a() {
         return new n();
      }
   }
}
