package co.uk.getmondo.adjust;

import android.content.Context;
import android.content.SharedPreferences;

public class g {
   private SharedPreferences a;

   public g(Context var) {
      this.a = var.getSharedPreferences("adjust_storage", 0);
   }

   void a(boolean var) {
      this.a.edit().putBoolean("KEY_TRACKED", var).apply();
   }

   boolean a() {
      return this.a.getBoolean("KEY_TRACKED", false);
   }
}
