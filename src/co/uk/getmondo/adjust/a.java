package co.uk.getmondo.adjust;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import co.uk.getmondo.api.AnalyticsApi;
import co.uk.getmondo.api.model.tracking.Impression;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import io.reactivex.u;
import io.reactivex.v;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.threeten.bp.LocalDateTime;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class a {
   private final Context a;
   private final u b;
   private final g c;
   private final AnalyticsApi d;
   private final AdjustApi e;

   a(Context var, u var, String var, g var, OkHttpClient var, AnalyticsApi var, HttpLoggingInterceptor var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      OkHttpClient var = var.newBuilder().addInterceptor(var).build();
      this.e = (AdjustApi)(new Retrofit.Builder()).baseUrl(var).addConverterFactory(GsonConverterFactory.create((new com.google.gson.g()).a())).client(var).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build().create(AdjustApi.class);
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var, Map var) throws Exception {
      Impression var = Impression.j((String)var.get("gps_adid"));
      return var.d.trackEvent(var).b((io.reactivex.d)var.e.trackSession(var));
   }

   // $FF: synthetic method
   static Map a(a var) throws Exception {
      Resources var = var.a.getResources();
      android.support.v4.g.a var = new android.support.v4.g.a();
      var.put("app_token", "fe40w1xej1ts");
      var.put("environment", "production");
      var.put("device_type", "phone");
      var.put("device_manufacturer", Build.MANUFACTURER);
      var.put("os_build", Build.ID);
      var.put("api_level", String.valueOf(VERSION.SDK_INT));
      var.put("os_name", "android");
      var.put("display_width", String.valueOf(var.getDisplayMetrics().widthPixels));
      var.put("display_height", String.valueOf(var.getDisplayMetrics().heightPixels));
      var.put("package_name", var.a.getPackageName());
      var.put("app_version", var.a.getPackageManager().getPackageInfo(var.a.getPackageName(), 0).versionName);
      var.put("country", Locale.getDefault().getCountry());
      var.put("language", Locale.getDefault().getLanguage());
      var.put("device_name", Build.DEVICE);
      var.put("os_version", VERSION.RELEASE);
      String var = var.d();
      if(var != null) {
         var.put("fb_id", var);
      }

      var.put("needs_response_details", "0");
      var.put("event_buffering_enabled", "0");
      var.put("android_uuid", UUID.randomUUID().toString());
      var.put("gps_adid", AdvertisingIdClient.getAdvertisingIdInfo(var.a).getId());
      String var = LocalDateTime.a().toString();
      var.put("created_at", var);
      var.put("sent_at", var);
      return var;
   }

   // $FF: synthetic method
   static void b() throws Exception {
      d.a.a.b("App install tracked with Adjust", new Object[0]);
   }

   // $FF: synthetic method
   static void b(a var) throws Exception {
      var.c.a(true);
   }

   private v c() {
      return v.c(f.a(this));
   }

   private String d() {
      // $FF: Couldn't be decompiled
   }

   public void a() {
      if(!co.uk.getmondo.a.b.booleanValue() && !this.c.a()) {
         this.c().c(b.a(this)).b(c.a(this)).b(this.b).a(d.b(), e.a());
      }

   }
}
