package co.uk.getmondo.adjust;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class f implements Callable {
   private final a a;

   private f(a var) {
      this.a = var;
   }

   public static Callable a(a var) {
      return new f(var);
   }

   public Object call() {
      return a.a(this.a);
   }
}
