package co.uk.getmondo.adjust;

import java.util.Map;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

interface AdjustApi {
   @FormUrlEncoded
   @Headers({"Client-SDK: android4.10.4"})
   @POST("session")
   io.reactivex.b trackSession(@FieldMap Map var);
}
