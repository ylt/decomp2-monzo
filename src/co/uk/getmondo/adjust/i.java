package co.uk.getmondo.adjust;

import android.content.Context;
import co.uk.getmondo.api.AnalyticsApi;
import io.reactivex.u;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new i(var, var, var, var, var, var, var);
   }

   public a a() {
      return new a((Context)this.b.b(), (u)this.c.b(), (String)this.d.b(), (g)this.e.b(), (OkHttpClient)this.f.b(), (AnalyticsApi)this.g.b(), (HttpLoggingInterceptor)this.h.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
