package co.uk.getmondo.top;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopActivity_ViewBinding implements Unbinder {
   private TopActivity a;
   private View b;

   public TopActivity_ViewBinding(final TopActivity var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var = Utils.findRequiredView(var, 2131821118, "field 'getCardButton' and method 'onGetCardClick'");
      var.getCardButton = (Button)Utils.castView(var, 2131821118, "field 'getCardButton'", Button.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onGetCardClick();
         }
      });
   }

   public void unbind() {
      TopActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
         var.getCardButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
