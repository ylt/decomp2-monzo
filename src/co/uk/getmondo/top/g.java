package co.uk.getmondo.top;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import io.reactivex.u;
import io.reactivex.v;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.waitlist.i i;
   private final co.uk.getmondo.create_account.topup.c j;

   g(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.a var, co.uk.getmondo.waitlist.i var, co.uk.getmondo.create_account.topup.c var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   // $FF: synthetic method
   static void a(g var, android.support.v4.g.j var) throws Exception {
      ((g.a)var.a).g();
      switch(null.a[((ak)var.a).a().ordinal()]) {
      case 1:
         ((g.a)var.a).b();
         break;
      case 2:
         ((g.a)var.a).a((co.uk.getmondo.d.c)var.b);
         break;
      case 3:
         ((g.a)var.a).c();
         break;
      case 4:
         ((g.a)var.a).d();
         break;
      case 5:
         ((g.a)var.a).e();
      }

   }

   // $FF: synthetic method
   static void a(g var, Throwable var) throws Exception {
      var.f.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   private void c() {
      ((g.a)this.a).f();
      this.a((io.reactivex.b.b)v.a(this.g.a(), this.j.b(), h.a()).b(this.d).a(this.c).a(i.a(this), j.a(this)));
   }

   void a() {
      ((g.a)this.a).a();
   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.i());
      var.a(p.b(this.e.b().d().c()));
      this.c();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.c var);

      void a(String var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
