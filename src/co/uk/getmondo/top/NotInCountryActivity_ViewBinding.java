package co.uk.getmondo.top;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class NotInCountryActivity_ViewBinding implements Unbinder {
   private NotInCountryActivity a;
   private View b;
   private View c;

   public NotInCountryActivity_ViewBinding(final NotInCountryActivity var, View var) {
      this.a = var;
      var.image = (ImageView)Utils.findRequiredViewAsType(var, 2131820750, "field 'image'", ImageView.class);
      var.notified = (TextView)Utils.findRequiredViewAsType(var, 2131820861, "field 'notified'", TextView.class);
      View var = Utils.findRequiredView(var, 2131820860, "field 'notifyMe' and method 'onNotify'");
      var.notifyMe = (Button)Utils.castView(var, 2131820860, "field 'notifyMe'", Button.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onNotify();
         }
      });
      var.desc = (TextView)Utils.findRequiredViewAsType(var, 2131820842, "field 'desc'", TextView.class);
      var = Utils.findRequiredView(var, 2131820830, "method 'onShare'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onShare();
         }
      });
   }

   public void unbind() {
      NotInCountryActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.image = null;
         var.notified = null;
         var.notifyMe = null;
         var.desc = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
