package co.uk.getmondo.top;

import android.app.Activity;
import android.content.Intent;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.s;
import co.uk.getmondo.d.ak;
import io.reactivex.u;

public class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final s g;
   private final co.uk.getmondo.api.b.a h;
   private final co.uk.getmondo.common.a i;

   b(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, s var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   private static String a(ak var) {
      String var;
      if(var != null && var.d() != null && var.d().h() != null && var.d().h().j() != null) {
         co.uk.getmondo.d.i var = co.uk.getmondo.d.i.a(var.d().h().j());
         if(var == null) {
            var = "";
         } else {
            var = var.c();
         }
      } else {
         var = "";
      }

      return var;
   }

   // $FF: synthetic method
   static void a(b.a var, ak var) throws Exception {
      var.a(a(var));
      if(var.c() != null) {
         var.a();
      }

   }

   // $FF: synthetic method
   static void a(b var, b.a var, Throwable var) throws Exception {
      var.f.a(var, var);
   }

   public void a(Activity var) {
      String var = var.getString(2131362012);
      Intent.createChooser(co.uk.getmondo.common.k.j.a(this.e.b().e().g()), var);
   }

   public void a(b.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.j());
      var.a(a(this.e.b()));
      this.a((io.reactivex.b.b)this.h.a().b(this.c).a(this.d).a(c.a(var), d.a(this, var)));
   }

   boolean a() {
      return this.g.g();
   }

   void c() {
      this.g.b(true);
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var);
   }
}
