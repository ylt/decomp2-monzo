package co.uk.getmondo.top;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.address.SelectAddressActivity;
import co.uk.getmondo.create_account.topup.InitialTopupActivity;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup_old.CreateProfileActivity;

public class TopActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131821118)
   Button getCardButton;
   @BindView(2131821026)
   TextView titleTextView;

   public static void a(Context var) {
      Intent var = new Intent(var, TopActivity.class);
      var.addFlags(268533760);
      var.startActivity(var);
   }

   public void a() {
      SelectAddressActivity.a(this, co.uk.getmondo.common.activities.b.a.a, true);
   }

   public void a(co.uk.getmondo.d.c var) {
      InitialTopupActivity.a(this, var);
   }

   public void a(String var) {
      this.titleTextView.setText(this.getString(2131362904, new Object[]{var, co.uk.getmondo.common.k.e.a()}));
   }

   public void b() {
      CardShippedActivity.a(this, true);
   }

   public void c() {
      HomeActivity.a((Context)this);
   }

   public void d() {
      NotInCountryActivity.a((Context)this);
   }

   public void e() {
      CreateProfileActivity.a((Context)this);
   }

   public void f() {
      this.getCardButton.setEnabled(false);
   }

   public void g() {
      this.getCardButton.setEnabled(true);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034219);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnClick({2131821118})
   void onGetCardClick() {
      this.a.a();
   }
}
