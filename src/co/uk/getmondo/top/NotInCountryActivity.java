package co.uk.getmondo.top;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.main.HomeActivity;

public class NotInCountryActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;
   @BindView(2131820842)
   TextView desc;
   @BindView(2131820750)
   ImageView image;
   @BindView(2131820861)
   TextView notified;
   @BindView(2131820860)
   Button notifyMe;

   public static void a(Context var) {
      Intent var = new Intent(var, NotInCountryActivity.class);
      var.addFlags(268533760);
      var.startActivity(var);
   }

   private void b() {
      if(this.a.a()) {
         this.notifyMe.setVisibility(8);
         this.notified.setVisibility(0);
      } else {
         this.notifyMe.setVisibility(0);
         this.notified.setVisibility(8);
      }

   }

   public static void b(Context var) {
      var.startActivity(new Intent(var, NotInCountryActivity.class));
   }

   public void a() {
      HomeActivity.a((Context)this);
   }

   public void a(String var) {
      this.desc.setText(this.getString(2131362081, new Object[]{var}));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034155);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((b.a)this);
      this.image.setImageResource(2130838029);
      this.b();
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnClick({2131820860})
   void onNotify() {
      this.a.c();
      this.b();
   }

   @OnClick({2131820830})
   void onShare() {
      this.a.a((Activity)this);
   }
}
