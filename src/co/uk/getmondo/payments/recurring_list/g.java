package co.uk.getmondo.payments.recurring_list;

import co.uk.getmondo.payments.a.i;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(b.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var) {
      return new g(var, var);
   }

   public f a() {
      return (f)b.a.c.a(this.b, new f((i)this.c.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
