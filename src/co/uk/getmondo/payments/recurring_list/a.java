package co.uk.getmondo.payments.recurring_list;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.common.ui.AmountView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001&B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u001c\u0010\u001d\u001a\u00020\u000f2\n\u0010\u001e\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001f\u001a\u00020\u001cH\u0016J\u001c\u0010 \u001a\u00060\u0002R\u00020\u00002\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001cH\u0016J\u0014\u0010$\u001a\u00020\u000f2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0%R\u001b\u0010\u0006\u001a\u00020\u00078FX\u0086\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R(\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178FX\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u000b\u001a\u0004\b\u0018\u0010\u0019¨\u0006'"},
   d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter$ViewHolder;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "getAvatarGenerator", "()Lco/uk/getmondo/common/ui/AvatarGenerator;", "avatarGenerator$delegate", "Lkotlin/Lazy;", "recurringPaymentClicked", "Lkotlin/Function1;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "", "getRecurringPaymentClicked", "()Lkotlin/jvm/functions/Function1;", "setRecurringPaymentClicked", "(Lkotlin/jvm/functions/Function1;)V", "recurringPayments", "", "scheduleFormatter", "Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "getScheduleFormatter", "()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "scheduleFormatter$delegate", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setRecurringPayments", "", "ViewHolder", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends android.support.v7.widget.RecyclerView.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(a.class), "avatarGenerator", "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;")), (l)y.a(new w(y.a(a.class), "scheduleFormatter", "getScheduleFormatter()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;"))};
   private kotlin.d.a.b b;
   private final kotlin.c c;
   private final kotlin.c d;
   private final List e;
   private final Context f;

   public a(Context var) {
      kotlin.d.b.l.b(var, "context");
      super();
      this.f = var;
      this.c = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final co.uk.getmondo.common.ui.a b() {
            return co.uk.getmondo.common.ui.a.a.a(a.this.f);
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.d = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final co.uk.getmondo.payments.send.a b() {
            return new co.uk.getmondo.payments.send.a(a.this.f);
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.e = (List)(new ArrayList());
   }

   public a.a a(ViewGroup var, int var) {
      kotlin.d.b.l.b(var, "parent");
      View var = LayoutInflater.from(var.getContext()).inflate(2131034376, var, false);
      kotlin.d.b.l.a(var, "view");
      return new a.a(var);
   }

   public final kotlin.d.a.b a() {
      return this.b;
   }

   public void a(a.a var, int var) {
      kotlin.d.b.l.b(var, "holder");
      var.a((co.uk.getmondo.payments.a.a.f)this.e.get(var));
   }

   public final void a(List var) {
      kotlin.d.b.l.b(var, "recurringPayments");
      this.e.clear();
      this.e.addAll((Collection)var);
      this.notifyDataSetChanged();
   }

   public final void a(kotlin.d.a.b var) {
      this.b = var;
   }

   public final co.uk.getmondo.common.ui.a b() {
      kotlin.c var = this.c;
      l var = a[0];
      return (co.uk.getmondo.common.ui.a)var.a();
   }

   public final co.uk.getmondo.payments.send.a c() {
      kotlin.c var = this.d;
      l var = a[1];
      return (co.uk.getmondo.payments.send.a)var.a();
   }

   public int getItemCount() {
      return this.e.size();
   }

   // $FF: synthetic method
   public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var, int var) {
      this.a((a.a)var, var);
   }

   // $FF: synthetic method
   public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var, int var) {
      return (android.support.v7.widget.RecyclerView.w)this.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter$ViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class a extends android.support.v7.widget.RecyclerView.w {
      private final View b;

      public a(View var) {
         kotlin.d.b.l.b(var, "view");
         super(var);
         this.b = var;
         ((ImageView)this.b.findViewById(co.uk.getmondo.c.a.recurringPaymentImage)).setClipToOutline(true);
         ((ImageView)this.b.findViewById(co.uk.getmondo.c.a.recurringPaymentImage)).setBackgroundResource(2130838008);
         this.itemView.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               kotlin.d.a.b var = a.this.a();
               if(var != null) {
                  n var = (n)var.a(a.this.e.get(a.this.getAdapterPosition()));
               }

            }
         }));
      }

      public final void a(co.uk.getmondo.payments.a.a.f var) {
         kotlin.d.b.l.b(var, "recurringPayment");
         ((TextView)this.b.findViewById(co.uk.getmondo.c.a.paymentTitle)).setText((CharSequence)var.d());
         TextView var = (TextView)this.b.findViewById(co.uk.getmondo.c.a.paymentSubtitle);
         CharSequence var;
         if(var instanceof co.uk.getmondo.payments.a.a.e) {
            var = (CharSequence)this.b.getContext().getString(2131362610);
         } else if(var instanceof co.uk.getmondo.payments.a.a.a) {
            var = (CharSequence)this.b.getContext().getString(2131362609);
         } else {
            var = (CharSequence)"";
         }

         var.setText(var);
         if(var instanceof co.uk.getmondo.payments.a.a.e) {
            ((AmountView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmount)).setAmount(((co.uk.getmondo.payments.a.a.e)var).c());
            ((AmountView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmount)).setVisibility(0);
            String var = a.this.c().b(((co.uk.getmondo.payments.a.a.e)var).e());
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmountSubtitle)).setText((CharSequence)var);
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmountSubtitle)).setVisibility(0);
            Drawable var = co.uk.getmondo.common.ui.a.b.a(a.this.b().a(var.d()), 0, (Typeface)null, false, 7, (Object)null);
            ((ImageView)this.b.findViewById(co.uk.getmondo.c.a.recurringPaymentImage)).setImageDrawable(var);
         } else {
            ((ImageView)this.b.findViewById(co.uk.getmondo.c.a.recurringPaymentImage)).setImageResource(2130837844);
            ((AmountView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmount)).setVisibility(8);
            ((TextView)this.b.findViewById(co.uk.getmondo.c.a.paymentAmountSubtitle)).setVisibility(8);
         }

      }
   }
}
