package co.uk.getmondo.payments.recurring_list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.al;
import android.support.v7.widget.RecyclerView.h;
import android.view.View;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0014J\b\u0010\u0014\u001a\u00020\u0011H\u0014J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0010\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0016\u0010\u001a\u001a\u00020\u00112\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00170\u001cH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter$View;", "()V", "adapter", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;", "getAdapter", "()Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;", "setAdapter", "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;)V", "presenter", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;", "getPresenter", "()Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;", "setPresenter", "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onRecurringPaymentClicked", "Lio/reactivex/Observable;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "showRecurringPaymentDetails", "recurringPayment", "showRecurringPayments", "recurringPayments", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class RecurringPaymentsActivity extends co.uk.getmondo.common.activities.b implements f.a {
   public f a;
   public a b;
   private HashMap c;

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            l.b(var, "emitter");
            android.support.v7.widget.RecyclerView.a var = ((RecyclerView)RecurringPaymentsActivity.this.a(co.uk.getmondo.c.a.recurringRecyclerView)).getAdapter();
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.recurring_list.RecurringPaymentAdapter");
            } else {
               final a var = (a)var;
               var.a((kotlin.d.a.b)(new kotlin.d.a.b() {
                  public final void a(co.uk.getmondo.payments.a.a.f varx) {
                     l.b(varx, "it");
                     var.a(varx);
                  }
               }));
               var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
                  public final void a() {
                     var.a((kotlin.d.a.b)null);
                  }
               }));
            }
         }
      }));
      l.a(var, "Observable.create<Recurr…licked = null }\n        }");
      return var;
   }

   public void a(co.uk.getmondo.payments.a.a.f var) {
      l.b(var, "recurringPayment");
      c.b.a(var).show(this.getSupportFragmentManager(), c.class.getName());
   }

   public void a(List var) {
      l.b(var, "recurringPayments");
      android.support.v7.widget.RecyclerView.a var = ((RecyclerView)this.a(co.uk.getmondo.c.a.recurringRecyclerView)).getAdapter();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.recurring_list.RecurringPaymentAdapter");
      } else {
         ((a)var).a(var);
      }
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034202);
      this.l().a(this);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.recurringRecyclerView)).setLayoutManager((h)(new LinearLayoutManager((Context)this)));
      RecyclerView var = (RecyclerView)this.a(co.uk.getmondo.c.a.recurringRecyclerView);
      a var = this.b;
      if(var == null) {
         l.b("adapter");
      }

      var.setAdapter((android.support.v7.widget.RecyclerView.a)var);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.recurringRecyclerView)).setHasFixedSize(true);
      al var = new al((Context)this, 1);
      var.a(android.support.v4.content.a.a((Context)this, 2130837968));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.recurringRecyclerView)).a((android.support.v7.widget.RecyclerView.g)var);
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((f.a)this);
   }

   protected void onDestroy() {
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }
}
