package co.uk.getmondo.payments.recurring_cancellation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.PinEntryView;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0016J\b\u0010\r\u001a\u00020\u000bH\u0016J\u0012\u0010\u000e\u001a\u00020\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\b\u0010\u0011\u001a\u00020\u000bH\u0014J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\b\u0010\u0015\u001a\u00020\u000bH\u0016J\b\u0010\u0016\u001a\u00020\u000bH\u0016J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\b\u0010\u001a\u001a\u00020\u000bH\u0016J\u0018\u0010\u001b\u001a\u00020\u000b2\u000e\u0010\u001c\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00190\u001dH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;", "getPresenter", "()Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;", "setPresenter", "(Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;)V", "clearPinEntryView", "", "close", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onPinEntered", "Lio/reactivex/Observable;", "", "showIncorrectPinError", "showLoading", "showPaymentDescription", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "showPinBlockedError", "showTitleAction", "paymentType", "Ljava/lang/Class;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class RecurringPaymentCancelActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final RecurringPaymentCancelActivity.a b = new RecurringPaymentCancelActivity.a((i)null);
   public b a;
   private HashMap c;

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            l.b(var, "emitter");
            ((PinEntryView)RecurringPaymentCancelActivity.this.a(co.uk.getmondo.c.a.pinEntryView)).setOnPinEnteredListener((PinEntryView.a)(new PinEntryView.a() {
               public final void a(String varx) {
                  var.a(varx);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((PinEntryView)RecurringPaymentCancelActivity.this.a(co.uk.getmondo.c.a.pinEntryView)).setOnPinEnteredListener((PinEntryView.a)null);
               }
            }));
         }
      }));
      l.a(var, "Observable.create { emit…istener(null) }\n        }");
      return var;
   }

   public void a(co.uk.getmondo.payments.a.a.f var) {
      l.b(var, "recurringPayment");
      TextView var = (TextView)this.a(co.uk.getmondo.c.a.paymentCancellationSubtitle);
      CharSequence var;
      if(var instanceof co.uk.getmondo.payments.a.a.e) {
         var = (CharSequence)this.getString(2131362544, new Object[]{((co.uk.getmondo.payments.a.a.e)var).c(), var.d()});
      } else {
         var = (CharSequence)var.d();
      }

      var.setText(var);
   }

   public void a(Class var) {
      l.b(var, "paymentType");
      TextView var = (TextView)this.a(co.uk.getmondo.c.a.paymentCancellationTitle);
      CharSequence var;
      if(l.a(var, co.uk.getmondo.payments.a.a.e.class)) {
         var = (CharSequence)this.getString(2131362543);
      } else {
         var = (CharSequence)this.getString(2131362145);
      }

      var.setText(var);
   }

   public void b() {
      ae.a(this.a(co.uk.getmondo.c.a.progressBarOverlay));
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.progressBar)));
   }

   public void c() {
      ae.b(this.a(co.uk.getmondo.c.a.progressBarOverlay));
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.progressBar));
   }

   public void d() {
      ((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntryView)).a();
   }

   public void e() {
      co.uk.getmondo.common.ui.i.a((Context)this, this.m(), this.getString(2131362178), 0, false).c();
   }

   public void f() {
      co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), co.uk.getmondo.common.d.e.class.getName());
   }

   public void g() {
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034201);
      co.uk.getmondo.payments.a.a.f var = (co.uk.getmondo.payments.a.a.f)this.getIntent().getParcelableExtra("KEY_RECURRING_PAYMENT");
      co.uk.getmondo.common.h.b.b var = this.l();
      l.a(var, "recurringPayment");
      var.a(new f(var)).a(this);
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$Companion;", "", "()V", "KEY_RECURRING_PAYMENT", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, co.uk.getmondo.payments.a.a.f var) {
         l.b(var, "context");
         l.b(var, "recurringPayment");
         Intent var = (new Intent(var, RecurringPaymentCancelActivity.class)).putExtra("KEY_RECURRING_PAYMENT", (Parcelable)var);
         l.a(var, "Intent(context, Recurrin…AYMENT, recurringPayment)");
         return var;
      }
   }
}
