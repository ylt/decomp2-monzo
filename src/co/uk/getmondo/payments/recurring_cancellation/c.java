package co.uk.getmondo.payments.recurring_cancellation;

import co.uk.getmondo.payments.a.h;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class c {
   // $FF: synthetic field
   public static final int[] a = new int[h.values().length];

   static {
      a[h.a.ordinal()] = 1;
      a[h.b.ordinal()] = 2;
   }
}
