package co.uk.getmondo.payments.recurring_cancellation;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
   d2 = {"Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentModule;", "", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "(Lco/uk/getmondo/payments/data/model/RecurringPayment;)V", "provideRecurringPayment", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f {
   private final co.uk.getmondo.payments.a.a.f a;

   public f(co.uk.getmondo.payments.a.a.f var) {
      l.b(var, "recurringPayment");
      super();
      this.a = var;
   }

   public final co.uk.getmondo.payments.a.a.f a() {
      return this.a;
   }
}
