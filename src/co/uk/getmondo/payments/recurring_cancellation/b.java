package co.uk.getmondo.payments.recurring_cancellation;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.payments.a.h;
import co.uk.getmondo.payments.a.i;
import io.reactivex.n;
import io.reactivex.u;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "manager", "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/payments/data/RecurringPaymentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/data/model/RecurringPayment;)V", "handleError", "", "exception", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final i e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.payments.a.a.f g;

   public b(u var, u var, i var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.a.a.f var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "manager");
      l.b(var, "apiErrorHandler");
      l.b(var, "recurringPayment");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(Throwable var, b.a var) {
      if(var instanceof ApiException) {
         co.uk.getmondo.common.e.f[] var = (co.uk.getmondo.common.e.f[])h.values();
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         String var;
         if(var != null) {
            var = var.a();
         } else {
            var = null;
         }

         h var = (h)co.uk.getmondo.common.e.d.a(var, var);
         if(var != null) {
            switch(c.a[var.ordinal()]) {
            case 1:
               var.f();
               return;
            case 2:
               var.e();
               return;
            default:
               return;
            }
         }
      }

      if(!this.f.a(var, (co.uk.getmondo.common.e.a.a)var)) {
         var.b(2131362198);
      }

   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.g.getClass());
      var.a(this.g);
      final String var = UUID.randomUUID().toString();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String varx) {
            l.b(varx, "pin");
            i var = b.this.e;
            co.uk.getmondo.payments.a.a.f var = b.this.g;
            String varx = var;
            l.a(varx, "idempotencyKey");
            return co.uk.getmondo.common.j.f.a(var.a(var, varx, varx).b(b.this.c).a(b.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.b();
               }
            })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.c();
                  var.d();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  b var = b.this;
                  l.a(varx, "it");
                  var.a(varx, var);
               }
            })).a((Object)b.this.g));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.f varx) {
            var.g();
         }
      }));
      l.a(var, "view.onPinEntered()\n    …ubscribe { view.close() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH&J\b\u0010\n\u001a\u00020\u0004H&J\b\u0010\u000b\u001a\u00020\u0004H&J\u0010\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH&J\b\u0010\u000f\u001a\u00020\u0004H&J\u0018\u0010\u0010\u001a\u00020\u00042\u000e\u0010\u0011\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000e0\u0012H&¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "clearPinEntryView", "", "close", "hideLoading", "onPinEntered", "Lio/reactivex/Observable;", "", "showIncorrectPinError", "showLoading", "showPaymentDescription", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "showPinBlockedError", "showTitleAction", "paymentType", "Ljava/lang/Class;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a();

      void a(co.uk.getmondo.payments.a.a.f var);

      void a(Class var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
