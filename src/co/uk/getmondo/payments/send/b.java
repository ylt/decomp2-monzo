package co.uk.getmondo.payments.send;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class b {
   // $FF: synthetic field
   public static final int[] a = new int[co.uk.getmondo.payments.a.a.d.c.values().length];

   static {
      a[co.uk.getmondo.payments.a.a.d.c.a.ordinal()] = 1;
      a[co.uk.getmondo.payments.a.a.d.c.b.ordinal()] = 2;
      a[co.uk.getmondo.payments.a.a.d.c.c.ordinal()] = 3;
      a[co.uk.getmondo.payments.a.a.d.c.d.ordinal()] = 4;
      a[co.uk.getmondo.payments.a.a.d.c.e.ordinal()] = 5;
   }
}
