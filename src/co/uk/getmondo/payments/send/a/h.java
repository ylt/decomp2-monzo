package co.uk.getmondo.payments.send.a;

import android.database.ContentObserver;

// $FF: synthetic class
final class h implements io.reactivex.c.f {
   private final e a;
   private final ContentObserver b;

   private h(e var, ContentObserver var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.f a(e var, ContentObserver var) {
      return new h(var, var);
   }

   public void a() {
      e.a(this.a, this.b);
   }
}
