package co.uk.getmondo.payments.send.a;

import co.uk.getmondo.common.k.p;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b¢\u0006\u0002\u0010\tJ\b\u0010\u0013\u001a\u00020\u0014H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\rR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\r¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;", "Lco/uk/getmondo/payments/send/contacts/BaseContact;", "id", "", "name", "", "photo", "phoneNumbers", "", "(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getId", "()J", "getName", "()Ljava/lang/String;", "phoneNumber", "getPhoneNumber", "getPhoneNumbers", "()Ljava/util/List;", "getPhoto", "hasPhoto", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends a {
   private final long a;
   private final String b;
   private final String c;
   private final List d;

   public d(long var, String var, String var, List var) {
      l.b(var, "phoneNumbers");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public long a() {
      return this.a;
   }

   public String b() {
      return this.b;
   }

   public String c() {
      return (String)this.d.get(0);
   }

   public boolean d() {
      return p.c(this.e());
   }

   public String e() {
      return this.c;
   }

   public final List f() {
      return this.d;
   }
}
