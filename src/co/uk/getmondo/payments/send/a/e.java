package co.uk.getmondo.payments.send.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;
import io.reactivex.u;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class e {
   private final Context a;
   private final u b;
   private final io.michaelrocks.libphonenumber.android.h c;
   private final ContentResolver d;
   private final io.reactivex.i.c e;
   private final n f;

   public e(Context var, u var, u var, io.michaelrocks.libphonenumber.android.h var) {
      this.d = var.getContentResolver();
      this.a = var;
      this.b = var;
      this.c = var;
      this.e = io.reactivex.i.a.a();
      this.f = n.merge(this.e, this.d()).switchMap(f.a(this, var)).replay(1).b();
   }

   // $FF: synthetic method
   static r a(e var, u var, Object var) throws Exception {
      return n.fromCallable(i.a(var)).subscribeOn(var);
   }

   private String a(String var) {
      try {
         io.michaelrocks.libphonenumber.android.j.a var = this.c.a((CharSequence)var, (String)"GB");
         var = this.c.a(var, io.michaelrocks.libphonenumber.android.h.a.a);
      } catch (NumberParseException var) {
         var = "";
      }

      return var;
   }

   // $FF: synthetic method
   static Map a(e var) {
      return var.c();
   }

   // $FF: synthetic method
   static void a(e var, ContentObserver var) throws Exception {
      var.d.unregisterContentObserver(var);
   }

   // $FF: synthetic method
   static void a(final e var, final o var) throws Exception {
      ContentObserver var = new ContentObserver(new Handler()) {
         public void onChange(boolean varx) {
            var.a(co.uk.getmondo.common.b.a.a);
         }
      };
      var.d.registerContentObserver(Contacts.CONTENT_URI, false, var);
      var.a(h.a(var, var));
      var.a(co.uk.getmondo.common.b.a.a);
   }

   private Map c() {
      boolean var;
      if(android.support.v4.content.a.b(this.a, "android.permission.READ_CONTACTS") == 0) {
         var = true;
      } else {
         var = false;
      }

      d.a.a.a("Loading contacts from device", new Object[0]);
      Map var;
      if(var) {
         var = this.e();
      } else {
         d.a.a.a("Missing contacts permission. Returning an empty map instead", new Object[0]);
         var = Collections.emptyMap();
      }

      return var;
   }

   private n d() {
      return n.create(g.a(this)).subscribeOn(this.b).throttleFirst(5L, TimeUnit.SECONDS, this.b);
   }

   private Map e() {
      // $FF: Couldn't be decompiled
   }

   public n a() {
      return this.f;
   }

   public void b() {
      this.e.onNext(co.uk.getmondo.common.b.a.a);
   }
}
