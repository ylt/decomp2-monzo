package co.uk.getmondo.payments.send.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class i implements Callable {
   private final e a;

   private i(e var) {
      this.a = var;
   }

   public static Callable a(e var) {
      return new i(var);
   }

   public Object call() {
      return e.a(this.a);
   }
}
