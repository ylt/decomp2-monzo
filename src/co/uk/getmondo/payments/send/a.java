package co.uk.getmondo.payments.send;

import android.content.Context;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Year;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ,\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\rJ\u000e\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "format", "", "schedule", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "formatDate", "localDate", "Lorg/threeten/bp/LocalDate;", "monthBeforeDay", "", "monthTextStyle", "Lorg/threeten/bp/format/TextStyle;", "showYearIfDifferent", "formatShort", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final Context a;

   public a(Context var) {
      kotlin.d.b.l.b(var, "context");
      super();
      this.a = var;
   }

   public final String a(co.uk.getmondo.payments.a.a.d var) {
      String var;
      if(var != null && !var.a()) {
         String var = a(this, var.b(), false, (TextStyle)null, true, 6, (Object)null);
         String var = this.a.getString(var.c().b());
         if(var.c() == co.uk.getmondo.payments.a.a.d.c.a) {
            var = var;
         } else if(var.d() == null) {
            var = this.a.getString(2131362539, new Object[]{var, var});
            kotlin.d.b.l.a(var, "context.getString(R.stri…Name, startDateFormatted)");
         } else {
            var = this.a.getString(2131362537, new Object[]{var, var, a(this, var.d(), false, (TextStyle)null, false, 14, (Object)null)});
            kotlin.d.b.l.a(var, "context.getString(R.stri…atDate(schedule.endDate))");
         }
      } else {
         var = "";
      }

      return var;
   }

   public final String a(LocalDate var, boolean var, TextStyle var, boolean var) {
      kotlin.d.b.l.b(var, "localDate");
      kotlin.d.b.l.b(var, "monthTextStyle");
      String var = var.f().a(var, Locale.ENGLISH);
      if(var) {
         var = "" + var + ' ' + co.uk.getmondo.common.c.c.a(var);
      } else {
         var = "" + co.uk.getmondo.common.c.c.a(var) + ' ' + var;
      }

      String var = var;
      if(var) {
         var = var;
         if(var.d() > Year.a().b()) {
            var = "" + var + ' ' + var.d();
         }
      }

      return var;
   }

   public final String b(co.uk.getmondo.payments.a.a.d var) {
      kotlin.d.b.l.b(var, "schedule");
      co.uk.getmondo.payments.a.a.d.c var = var.c();
      String var;
      switch(b.a[var.ordinal()]) {
      case 1:
         var = this.a(var.b(), true, TextStyle.a, true);
         var = this.a.getString(2131362540, new Object[]{var});
         kotlin.d.b.l.a(var, "context.getString(R.stri…edule_pattern_once, date)");
         break;
      case 2:
         var = this.a.getString(var.c().c());
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
         }

         var = var.toLowerCase();
         kotlin.d.b.l.a(var, "(this as java.lang.String).toLowerCase()");
         break;
      case 3:
         var = var.b().i().a(TextStyle.a, Locale.ENGLISH);
         var = this.a.getString(2131362541, new Object[]{var});
         kotlin.d.b.l.a(var, "context.getString(R.stri…attern_weekly, dayOfWeek)");
         break;
      case 4:
         var = this.a.getString(2131362538, new Object[]{co.uk.getmondo.common.c.c.a(var.b())});
         kotlin.d.b.l.a(var, "context.getString(R.stri…ate.dayOfMonthWithSuffix)");
         break;
      case 5:
         var = a(this, var.b(), true, TextStyle.a, false, 8, (Object)null);
         var = this.a.getString(2131362542, new Object[]{var});
         kotlin.d.b.l.a(var, "context.getString(R.stri…ule_pattern_yearly, date)");
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var;
   }
}
