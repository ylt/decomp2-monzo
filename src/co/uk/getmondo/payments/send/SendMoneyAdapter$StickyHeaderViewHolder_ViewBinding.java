package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$StickyHeaderViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.StickyHeaderViewHolder a;

   public SendMoneyAdapter$StickyHeaderViewHolder_ViewBinding(SendMoneyAdapter.StickyHeaderViewHolder var, View var) {
      this.a = var;
      var.headerTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821599, "field 'headerTextView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.StickyHeaderViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.headerTextView = null;
      }
   }
}
