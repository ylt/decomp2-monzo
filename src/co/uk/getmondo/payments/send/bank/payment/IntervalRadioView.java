package co.uk.getmondo.payments.send.bank.payment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class IntervalRadioView extends LinearLayout implements Checkable {
   private co.uk.getmondo.payments.a.a.d.c a;
   @BindView(2131821744)
   TextView descriptionTextView;
   @BindView(2131821743)
   RadioButton radioButton;

   public IntervalRadioView(Context var) {
      super(var);
      this.a();
   }

   private void a() {
      ButterKnife.bind(this, (View)LayoutInflater.from(this.getContext()).inflate(2131034433, this));
   }

   private void b() {
      if(this.isChecked() && this.a.d() != null) {
         this.descriptionTextView.setVisibility(0);
      } else {
         this.descriptionTextView.setVisibility(8);
      }

   }

   public boolean isChecked() {
      return this.radioButton.isChecked();
   }

   public void setChecked(boolean var) {
      this.radioButton.setChecked(var);
      this.b();
   }

   public void setInterval(co.uk.getmondo.payments.a.a.d.c var) {
      this.a = var;
      this.radioButton.setText(var.c());
      if(var.d() != null) {
         this.descriptionTextView.setText(var.d().intValue());
      }

      this.b();
   }

   public void toggle() {
      this.radioButton.toggle();
      this.b();
   }
}
