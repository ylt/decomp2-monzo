package co.uk.getmondo.payments.send.bank.payment;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class BankPaymentDetailsActivity_ViewBinding implements Unbinder {
   private BankPaymentDetailsActivity a;

   public BankPaymentDetailsActivity_ViewBinding(BankPaymentDetailsActivity var, View var) {
      this.a = var;
      var.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var, 2131820817, "field 'amountInputView'", AmountInputView.class);
      var.referenceInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820818, "field 'referenceInputLayout'", TextInputLayout.class);
      var.referenceEditText = (EditText)Utils.findRequiredViewAsType(var, 2131820819, "field 'referenceEditText'", EditText.class);
      var.nextStepButton = (Button)Utils.findRequiredViewAsType(var, 2131820827, "field 'nextStepButton'", Button.class);
      var.schedulePaymentSwitch = (Switch)Utils.findRequiredViewAsType(var, 2131820820, "field 'schedulePaymentSwitch'", Switch.class);
      var.schedulePaymentIntervalRow = (ViewGroup)Utils.findRequiredViewAsType(var, 2131820823, "field 'schedulePaymentIntervalRow'", ViewGroup.class);
      var.schedulePaymentStartRow = (ViewGroup)Utils.findRequiredViewAsType(var, 2131820821, "field 'schedulePaymentStartRow'", ViewGroup.class);
      var.schedulePaymentStartTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820822, "field 'schedulePaymentStartTextView'", TextView.class);
      var.schedulePaymentIntervalTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820824, "field 'schedulePaymentIntervalTextView'", TextView.class);
      var.schedulePaymentEndRow = (ViewGroup)Utils.findRequiredViewAsType(var, 2131820825, "field 'schedulePaymentEndRow'", ViewGroup.class);
      var.schedulePaymentEndTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820826, "field 'schedulePaymentEndTextView'", TextView.class);
   }

   public void unbind() {
      BankPaymentDetailsActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountInputView = null;
         var.referenceInputLayout = null;
         var.referenceEditText = null;
         var.nextStepButton = null;
         var.schedulePaymentSwitch = null;
         var.schedulePaymentIntervalRow = null;
         var.schedulePaymentStartRow = null;
         var.schedulePaymentStartTextView = null;
         var.schedulePaymentIntervalTextView = null;
         var.schedulePaymentEndRow = null;
         var.schedulePaymentEndTextView = null;
      }
   }
}
