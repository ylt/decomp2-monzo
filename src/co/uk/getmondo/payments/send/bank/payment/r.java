package co.uk.getmondo.payments.send.bank.payment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class r implements OnClickListener {
   private final p a;

   private r(p var) {
      this.a = var;
   }

   public static OnClickListener a(p var) {
      return new r(var);
   }

   public void onClick(DialogInterface var, int var) {
      p.a(this.a, var, var);
   }
}
