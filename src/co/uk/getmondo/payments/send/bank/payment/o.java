package co.uk.getmondo.payments.send.bank.payment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

// $FF: synthetic class
final class o implements OnShowListener {
   private final DatePickerDialog a;

   private o(DatePickerDialog var) {
      this.a = var;
   }

   public static OnShowListener a(DatePickerDialog var) {
      return new o(var);
   }

   public void onShow(DialogInterface var) {
      m.a(this.a, var);
   }
}
