package co.uk.getmondo.payments.send.bank.payment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class p extends android.support.v4.app.i {
   private p.a a;
   private co.uk.getmondo.payments.a.a.d.c b;

   public p() {
      this.b = co.uk.getmondo.payments.a.a.d.c.a;
   }

   public static p a(co.uk.getmondo.payments.a.a.d.c var) {
      Bundle var = new Bundle();
      var.putSerializable("KEY_DEFAULT_INTERVAL", var);
      p var = new p();
      var.setArguments(var);
      return var;
   }

   // $FF: synthetic method
   static void a(p var, DialogInterface var, int var) {
      var.a.a(var.b);
   }

   // $FF: synthetic method
   static void a(p var, LinearLayout var, View var) {
      var.b = (co.uk.getmondo.payments.a.a.d.c)var.getTag();

      for(int var = 0; var < var.getChildCount(); ++var) {
         Checkable var = (Checkable)var.getChildAt(var);
         var.setChecked(var.equals(var));
      }

   }

   public void onAttach(Context var) {
      super.onAttach(var);
      if(var instanceof p.a) {
         this.a = (p.a)var;
      } else {
         throw new ClassCastException(var.toString() + " must implement IntervalSelectedListener");
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      if(this.getArguments() != null && this.getArguments().containsKey("KEY_DEFAULT_INTERVAL")) {
         this.b = (co.uk.getmondo.payments.a.a.d.c)this.getArguments().getSerializable("KEY_DEFAULT_INTERVAL");
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      int var = this.getResources().getDimensionPixelSize(2131427603);
      int var = this.getResources().getDimensionPixelSize(2131427605);
      LinearLayout var = new LinearLayout(this.getActivity());
      var.setOrientation(1);
      var.setPadding(var, var, var, var);
      OnClickListener var = q.a(this, var);
      co.uk.getmondo.payments.a.a.d.c[] var = co.uk.getmondo.payments.a.a.d.c.values();
      int var = var.length;

      for(int var = 0; var < var; ++var) {
         co.uk.getmondo.payments.a.a.d.c var = var[var];
         IntervalRadioView var = new IntervalRadioView(this.getActivity());
         var.setPaddingRelative(var, var, var, var);
         var.setInterval(var);
         var.setTag(var);
         var.setOnClickListener(var);
         var.addView(var);
         if(var == this.b) {
            var.setChecked(true);
         }
      }

      ScrollView var = new ScrollView(this.getActivity());
      var.addView(var);
      return (new android.support.v7.app.d.a(this.getActivity())).a(2131362292).b(var).a(2131362291, r.a(this)).b();
   }

   @FunctionalInterface
   interface a {
      void a(co.uk.getmondo.payments.a.a.d.c var);
   }
}
