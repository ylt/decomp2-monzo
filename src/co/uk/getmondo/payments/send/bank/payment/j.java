package co.uk.getmondo.payments.send.bank.payment;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final i b;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(i var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(i var) {
      return new j(var);
   }

   public co.uk.getmondo.payments.send.data.a.b a() {
      return (co.uk.getmondo.payments.send.data.a.b)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
