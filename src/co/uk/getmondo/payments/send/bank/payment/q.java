package co.uk.getmondo.payments.send.bank.payment;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

// $FF: synthetic class
final class q implements OnClickListener {
   private final p a;
   private final LinearLayout b;

   private q(p var, LinearLayout var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(p var, LinearLayout var) {
      return new q(var, var);
   }

   public void onClick(View var) {
      p.a(this.a, this.b, var);
   }
}
