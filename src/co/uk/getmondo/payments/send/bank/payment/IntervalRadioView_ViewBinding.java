package co.uk.getmondo.payments.send.bank.payment;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class IntervalRadioView_ViewBinding implements Unbinder {
   private IntervalRadioView a;

   public IntervalRadioView_ViewBinding(IntervalRadioView var, View var) {
      this.a = var;
      var.radioButton = (RadioButton)Utils.findRequiredViewAsType(var, 2131821743, "field 'radioButton'", RadioButton.class);
      var.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821744, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      IntervalRadioView var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.radioButton = null;
         var.descriptionTextView = null;
      }
   }
}
