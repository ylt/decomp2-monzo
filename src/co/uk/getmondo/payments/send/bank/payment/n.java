package co.uk.getmondo.payments.send.bank.payment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class n implements OnClickListener {
   private final m a;

   private n(m var) {
      this.a = var;
   }

   public static OnClickListener a(m var) {
      return new n(var);
   }

   public void onClick(DialogInterface var, int var) {
      m.a(this.a, var, var);
   }
}
