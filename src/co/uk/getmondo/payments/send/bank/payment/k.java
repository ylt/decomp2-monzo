package co.uk.getmondo.payments.send.bank.payment;

import io.reactivex.u;
import kotlin.Metadata;
import kotlin.TypeCastException;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB;\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0016J\u0018\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J \u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "bankPayee", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/model/BankPayee;)V", "referenceRegex", "Lkotlin/text/Regex;", "isValidBankPaymentAmount", "", "amount", "Lco/uk/getmondo/model/Amount;", "register", "", "view", "validateFinal", "formData", "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;", "validateWhileTyping", "amountString", "", "reference", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends co.uk.getmondo.common.ui.b {
   private final kotlin.h.g c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.a.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.payments.send.data.a.b i;

   public k(u var, u var, co.uk.getmondo.a.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.send.data.a.b var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "balanceRepository");
      kotlin.d.b.l.b(var, "accountService");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "bankPayee");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.c = new kotlin.h.g("^[a-zA-Z0-9 /?:().,+#=!\"%&*<>;{}@_\\-]+$");
   }

   private final void a(k.a var, s var) {
      co.uk.getmondo.d.c.a var = co.uk.getmondo.d.c.Companion;
      String var = var.a();
      co.uk.getmondo.common.i.c var = co.uk.getmondo.common.i.c.a;
      kotlin.d.b.l.a(var, "Currency.GBP");
      co.uk.getmondo.d.c var = var.a(var, var);
      if(!this.a(var)) {
         d.a.a.a((Throwable)(new RuntimeException("Balance string is not a valid number")));
      } else {
         co.uk.getmondo.payments.send.data.a.c var;
         if(var.c()) {
            LocalDate var;
            if(var.e() != co.uk.getmondo.payments.a.a.d.c.a) {
               var = var.f();
            } else {
               var = null;
            }

            if(var == null) {
               kotlin.d.b.l.a();
            }

            var = new co.uk.getmondo.payments.send.data.a.c(var, var.b(), this.i, new co.uk.getmondo.payments.a.a.d(var.d(), var.e(), var, (LocalDate)null, 8, (kotlin.d.b.i)null));
         } else {
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var = new co.uk.getmondo.payments.send.data.a.c(var, var.b(), this.i, (co.uk.getmondo.payments.a.a.d)null, 8, (kotlin.d.b.i)null);
         }

         var.a(var);
      }

   }

   private final void a(k.a var, String var, String var) {
      co.uk.getmondo.d.c.a var = co.uk.getmondo.d.c.Companion;
      co.uk.getmondo.common.i.c var = co.uk.getmondo.common.i.c.a;
      kotlin.d.b.l.a(var, "Currency.GBP");
      co.uk.getmondo.d.c var = var.a(var, var);
      kotlin.h.g var = this.c;
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         boolean var;
         label29: {
            var = var.a((CharSequence)kotlin.h.j.b((CharSequence)var).toString());
            if(!var) {
               boolean var;
               if(((CharSequence)var).length() == 0) {
                  var = true;
               } else {
                  var = false;
               }

               if(!var) {
                  var.k();
                  break label29;
               }
            }

            var.v();
         }

         if(this.a(var) && var) {
            var = true;
         } else {
            var = false;
         }

         var.b(var);
      }
   }

   private final boolean a(co.uk.getmondo.d.c var) {
      boolean var;
      if(var != null && var.k() > 0L) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(final k.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.i.b());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.g.d().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String var) {
            kotlin.d.b.l.b(var, "it");
            return k.this.f.b(var);
         }
      })).b(this.e).a(this.d).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = k.this.h;
            kotlin.d.b.l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      kotlin.d.b.l.a(var, "accountService.accountId…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.n var = this.g.d().b((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var) {
            kotlin.d.b.l.b(var, "it");
            return k.this.f.a(var);
         }
      })).map((io.reactivex.c.h)null.a).distinctUntilChanged();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.c varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "balance.observeOn(uiSche…(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = co.uk.getmondo.common.j.f.a(var.b(), var.a()).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            String var = (String)varx.c();
            String var = (String)varx.d();
            k.this.a(var, var, var);
         }
      }));
      kotlin.d.b.l.a(var, "combineLatest(view.onAmo…ng(view, first, second) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(s varx) {
            k var = k.this;
            k.a var = var;
            kotlin.d.b.l.a(varx, "formData");
            var.a(var, varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onNextStepClicked()…teFinal(view, formData) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.c(varx.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var, "view.onScheduleToggled()…eduleOptionsVisible(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(LocalDate varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.b(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onStartDateClicked(…openStartDatePicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(LocalDate varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onStartDateSelected…owSelectedStartDate(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.g().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.d.c varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.b(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onIntervalClicked()….openIntervalPicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.h().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.d.c varx) {
            k.a var = var;
            boolean var;
            if(varx != co.uk.getmondo.payments.a.a.d.c.a) {
               var = true;
            } else {
               var = false;
            }

            var.a(var);
            var = var;
            kotlin.d.b.l.a(varx, "interval");
            var.c(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onIntervalSelected(…terval)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.i().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onEndDateClicked()\n…w.openEndDatePicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b varx) {
            k.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.b(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onEndDateSelected()…showSelectedEndDate(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\b`\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0014\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006H&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u0006H&J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0006H&J\u0016\u0010\u0016\u001a\u00020\u00042\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\rH&J\u0010\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\nH&J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0013H&J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\b\u0010'\u001a\u00020\u0004H&J\u0010\u0010(\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u0007H&J\u0016\u0010*\u001a\u00020\u00042\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\rH&J\u0010\u0010.\u001a\u00020\u00042\u0006\u0010/\u001a\u00020\nH&¨\u00060"},
      d2 = {"Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "clearInvalidReferenceError", "", "onAmountChanged", "Lio/reactivex/Observable;", "", "onEndDateClicked", "Lcom/memoizrlabs/poweroptional/Optional;", "Lorg/threeten/bp/LocalDate;", "onEndDateSelected", "onIntervalClicked", "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "onIntervalSelected", "onNextStepClicked", "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;", "onReferenceChanged", "onScheduleToggled", "", "onStartDateClicked", "onStartDateSelected", "openEndDatePicker", "optionalDefaultDate", "openIntervalPicker", "defaultInterval", "openPaymentAuthentication", "bankPayment", "Lco/uk/getmondo/payments/send/data/model/BankPayment;", "openStartDatePicker", "defaultDate", "setEndDateVisible", "visible", "setNextStepButtonEnabled", "enabled", "setScheduleOptionsVisible", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showInvalidReference", "showRecipientName", "name", "showSelectedEndDate", "optionalEndDate", "showSelectedInterval", "interval", "showSelectedStartDate", "startDate", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var);

      void a(co.uk.getmondo.payments.send.data.a.c var);

      void a(com.c.b.b var);

      void a(String var);

      void a(LocalDate var);

      void a(boolean var);

      io.reactivex.n b();

      void b(co.uk.getmondo.payments.a.a.d.c var);

      void b(com.c.b.b var);

      void b(LocalDate var);

      void b(boolean var);

      io.reactivex.n c();

      void c(co.uk.getmondo.payments.a.a.d.c var);

      void c(boolean var);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      void k();

      void v();
   }
}
