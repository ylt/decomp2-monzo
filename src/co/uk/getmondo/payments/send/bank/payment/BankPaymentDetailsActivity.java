package co.uk.getmondo.payments.send.bank.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountInputView;
import co.uk.getmondo.payments.send.authentication.PaymentAuthenticationActivity;
import java.util.Locale;
import org.threeten.bp.LocalDate;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.format.DateTimeFormatter;

public class BankPaymentDetailsActivity extends co.uk.getmondo.common.activities.b implements k.a, m.a, p.a {
   k a;
   @BindView(2131820817)
   AmountInputView amountInputView;
   private final com.b.b.b b = com.b.b.b.b((Object)LocalDate.a());
   private final com.b.b.b c = com.b.b.b.b((Object)com.c.b.b.c());
   private final com.b.b.b e;
   private final DateTimeFormatter f;
   @BindView(2131820827)
   Button nextStepButton;
   @BindView(2131820819)
   EditText referenceEditText;
   @BindView(2131820818)
   TextInputLayout referenceInputLayout;
   @BindView(2131820825)
   ViewGroup schedulePaymentEndRow;
   @BindView(2131820826)
   TextView schedulePaymentEndTextView;
   @BindView(2131820823)
   ViewGroup schedulePaymentIntervalRow;
   @BindView(2131820824)
   TextView schedulePaymentIntervalTextView;
   @BindView(2131820821)
   ViewGroup schedulePaymentStartRow;
   @BindView(2131820822)
   TextView schedulePaymentStartTextView;
   @BindView(2131820820)
   Switch schedulePaymentSwitch;

   public BankPaymentDetailsActivity() {
      this.e = com.b.b.b.b((Object)co.uk.getmondo.payments.a.a.d.c.a);
      this.f = DateTimeFormatter.a("EEE, LLL d", Locale.ENGLISH);
   }

   public static Intent a(Context var, co.uk.getmondo.payments.send.data.a.b var) {
      return (new Intent(var, BankPaymentDetailsActivity.class)).putExtra("KEY_RECIPIENT_BANK_ACCOUNT", var);
   }

   // $FF: synthetic method
   static com.c.b.b a(BankPaymentDetailsActivity var, Object var) throws Exception {
      return (com.c.b.b)var.c.b();
   }

   // $FF: synthetic method
   static co.uk.getmondo.payments.a.a.d.c b(BankPaymentDetailsActivity var, Object var) throws Exception {
      return (co.uk.getmondo.payments.a.a.d.c)var.e.b();
   }

   // $FF: synthetic method
   static LocalDate c(BankPaymentDetailsActivity var, Object var) throws Exception {
      return (LocalDate)var.b.b();
   }

   // $FF: synthetic method
   static s d(BankPaymentDetailsActivity var, Object var) throws Exception {
      return new s(var.amountInputView.getAmountText().toString(), var.referenceEditText.getText().toString(), var.schedulePaymentSwitch.isChecked(), (LocalDate)var.b.b(), (co.uk.getmondo.payments.a.a.d.c)var.e.b(), (LocalDate)((com.c.b.b)var.c.b()).a((Object)null));
   }

   private void w() {
      String var = this.getString(2131362621);
      int var = var.indexOf(10);
      int var = var.length();
      int var = android.support.v4.content.a.c(this, 2131689526);
      int var = android.support.v4.content.a.c(this, 2131689527);
      SpannableString var = new SpannableString(var);
      var.setSpan(new ForegroundColorSpan(var), 0, var, 17);
      var.setSpan(new AbsoluteSizeSpan(co.uk.getmondo.common.k.n.b(12)), var, var, 34);
      var.setSpan(new ForegroundColorSpan(var), var, var, 34);
      this.schedulePaymentSwitch.setText(var);
   }

   public io.reactivex.n a() {
      return com.b.a.d.e.c(this.referenceEditText).map(a.a());
   }

   public void a(int var) {
      if(var == 101) {
         this.c.a((Object)com.c.b.b.c());
      }

   }

   public void a(co.uk.getmondo.d.c var) {
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(String.format(this.getString(2131362021), new Object[]{var.toString()}));
      }

   }

   public void a(co.uk.getmondo.payments.a.a.d.c var) {
      this.e.a((Object)var);
   }

   public void a(co.uk.getmondo.payments.send.data.a.c var) {
      this.startActivity(PaymentAuthenticationActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var));
   }

   public void a(com.c.b.b var) {
      LocalDate var = ((LocalDate)this.b.b()).e(1L);
      m.a(101, (LocalDate)var.a((Object)var), var, var.b()).show(this.getSupportFragmentManager(), "DATE_PICKER_FRAGMENT_TAG");
   }

   public void a(String var) {
      this.setTitle(var);
   }

   public void a(LocalDate var) {
      String var;
      if(var.d((ChronoLocalDate)LocalDate.a())) {
         var = this.getString(2131362623);
      } else {
         var = var.a(this.f);
      }

      this.schedulePaymentStartTextView.setText(var);
   }

   public void a(LocalDate var, int var) {
      switch(var) {
      case 100:
         this.b.a((Object)var);
         break;
      case 101:
         this.c.a((Object)com.c.b.b.b(var));
      }

   }

   public void a(boolean var) {
      ViewGroup var = this.schedulePaymentEndRow;
      byte var;
      if(var) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }

   public io.reactivex.n b() {
      return this.amountInputView.a().map(b.a());
   }

   public void b(co.uk.getmondo.payments.a.a.d.c var) {
      p.a(var).show(this.getSupportFragmentManager(), "INTERVAL_PICKER_FRAGMENT_TAG");
   }

   public void b(com.c.b.b var) {
      if(var.b()) {
         this.schedulePaymentEndTextView.setText(((LocalDate)var.a()).a(this.f));
      } else {
         this.schedulePaymentEndTextView.setText(2131362619);
      }

   }

   public void b(LocalDate var) {
      m.a(100, var, LocalDate.a(), false).show(this.getSupportFragmentManager(), "DATE_PICKER_FRAGMENT_TAG");
   }

   public void b(boolean var) {
      this.nextStepButton.setEnabled(var);
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.nextStepButton).map(c.a(this));
   }

   public void c(co.uk.getmondo.payments.a.a.d.c var) {
      this.schedulePaymentIntervalTextView.setText(var.c());
   }

   public void c(boolean var) {
      byte var;
      if(var) {
         var = 0;
      } else {
         var = 8;
      }

      this.schedulePaymentIntervalRow.setVisibility(var);
      this.schedulePaymentStartRow.setVisibility(var);
      if(var && this.e.b() != co.uk.getmondo.payments.a.a.d.c.a) {
         this.schedulePaymentEndRow.setVisibility(0);
      } else if(!var) {
         this.schedulePaymentEndRow.setVisibility(8);
      }

      if(var) {
         this.a((View)this.schedulePaymentSwitch);
      }

   }

   public io.reactivex.n d() {
      return com.b.a.d.d.a(this.schedulePaymentSwitch);
   }

   public io.reactivex.n e() {
      return com.b.a.c.c.a(this.schedulePaymentStartRow).map(d.a(this));
   }

   public io.reactivex.n f() {
      return this.b;
   }

   public io.reactivex.n g() {
      return com.b.a.c.c.a(this.schedulePaymentIntervalRow).map(e.a(this));
   }

   public io.reactivex.n h() {
      return this.e;
   }

   public io.reactivex.n i() {
      return com.b.a.c.c.a(this.schedulePaymentEndRow).map(f.a(this));
   }

   public io.reactivex.n j() {
      return this.c;
   }

   public void k() {
      this.referenceInputLayout.setError(this.getString(2131362027));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034147);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.b var = (co.uk.getmondo.payments.send.data.a.b)this.getIntent().getParcelableExtra("KEY_RECIPIENT_BANK_ACCOUNT");
      if(var == null) {
         throw new RuntimeException("This activity requires a Payee");
      } else {
         this.l().a(new i(var)).a(this);
         this.a.a((k.a)this);
         this.w();
      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   public void v() {
      this.referenceInputLayout.setError("");
   }
}
