package co.uk.getmondo.payments.send.bank.payment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

public class m extends android.support.v4.app.i implements OnDateSetListener {
   private m.a a;
   private int b;

   public static m a(int var, LocalDate var, LocalDate var, boolean var) {
      Bundle var = new Bundle();
      var.putInt("KEY_REQUEST_CODE", var);
      var.putSerializable("KEY_DEFAULT_DATE", var);
      var.putSerializable("KEY_MIN_DATE", var);
      var.putBoolean("KEY_CAN_REMOVE_DATE", var);
      m var = new m();
      var.setArguments(var);
      return var;
   }

   // $FF: synthetic method
   static void a(DatePickerDialog var, DialogInterface var) {
      var.getButton(-3).setTextColor(-65536);
   }

   // $FF: synthetic method
   static void a(m var, DialogInterface var, int var) {
      var.a.a(var.b);
   }

   public void onAttach(Context var) {
      super.onAttach(var);
      if(var instanceof m.a) {
         this.a = (m.a)var;
      } else {
         throw new ClassCastException(var.toString() + " must implement DateListener");
      }
   }

   public Dialog onCreateDialog(Bundle var) {
      this.b = this.getArguments().getInt("KEY_REQUEST_CODE");
      LocalDate var = (LocalDate)this.getArguments().getSerializable("KEY_MIN_DATE");
      boolean var = this.getArguments().getBoolean("KEY_CAN_REMOVE_DATE", false);
      LocalDate var = (LocalDate)this.getArguments().get("KEY_DEFAULT_DATE");
      DatePickerDialog var = new DatePickerDialog(this.getActivity(), this, var.d(), var.e() - 1, var.g());
      if(var) {
         var.setButton(-3, this.getString(2131362100), n.a(this));
         var.setOnShowListener(o.a(var));
      }

      Instant var = ZonedDateTime.a(var, LocalTime.a(0, 0), ZoneId.a()).j();
      var.getDatePicker().setMinDate(var.c());
      return var;
   }

   public void onDateSet(DatePicker var, int var, int var, int var) {
      this.a.a(LocalDate.a(var, var + 1, var), this.b);
   }

   interface a {
      void a(int var);

      void a(LocalDate var, int var);
   }
}
