package co.uk.getmondo.payments.send.bank;

import io.reactivex.u;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(b.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new g(var, var, var, var);
   }

   public b a() {
      return (b)b.a.c.a(this.b, new b((u)this.c.b(), (co.uk.getmondo.a.a)this.d.b(), (co.uk.getmondo.common.accounts.d)this.e.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
