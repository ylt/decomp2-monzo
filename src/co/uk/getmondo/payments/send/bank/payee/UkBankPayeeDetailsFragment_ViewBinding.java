package co.uk.getmondo.payments.send.bank.payee;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class UkBankPayeeDetailsFragment_ViewBinding implements Unbinder {
   private UkBankPayeeDetailsFragment a;

   public UkBankPayeeDetailsFragment_ViewBinding(UkBankPayeeDetailsFragment var, View var) {
      this.a = var;
      var.recipientInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821398, "field 'recipientInputLayout'", TextInputLayout.class);
      var.recipientEditText = (EditText)Utils.findRequiredViewAsType(var, 2131821399, "field 'recipientEditText'", EditText.class);
      var.sortCodeInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821400, "field 'sortCodeInputLayout'", TextInputLayout.class);
      var.sortCodeEditText = (EditText)Utils.findRequiredViewAsType(var, 2131821401, "field 'sortCodeEditText'", EditText.class);
      var.accountNumberInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821402, "field 'accountNumberInputLayout'", TextInputLayout.class);
      var.accountNumberEditText = (EditText)Utils.findRequiredViewAsType(var, 2131821403, "field 'accountNumberEditText'", EditText.class);
      var.nextButton = (Button)Utils.findRequiredViewAsType(var, 2131821404, "field 'nextButton'", Button.class);
      var.progress = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821405, "field 'progress'", ProgressBar.class);
   }

   public void unbind() {
      UkBankPayeeDetailsFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.recipientInputLayout = null;
         var.recipientEditText = null;
         var.sortCodeInputLayout = null;
         var.sortCodeEditText = null;
         var.accountNumberInputLayout = null;
         var.accountNumberEditText = null;
         var.nextButton = null;
         var.progress = null;
      }
   }
}
