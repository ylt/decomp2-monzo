package co.uk.getmondo.payments.send.bank.payee;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final UkBankPayeeDetailsFragment a;

   private e(UkBankPayeeDetailsFragment var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(UkBankPayeeDetailsFragment var) {
      return new e(var);
   }

   public Object a(Object var) {
      return UkBankPayeeDetailsFragment.a(this.a, var);
   }
}
