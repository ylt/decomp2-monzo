package co.uk.getmondo.payments.send.bank.payee;

import co.uk.getmondo.api.ApiException;
import io.reactivex.z;

public class g extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.payments.send.data.a c;
   private final co.uk.getmondo.common.e.a d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;

   g(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.payments.send.data.a var, co.uk.getmondo.common.e.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   // $FF: synthetic method
   static z a(g var, g.a var, co.uk.getmondo.payments.send.data.a.b var) throws Exception {
      return var.c.a(var).b(var.e).a(var.f).c(k.a(var)).b(l.a(var)).a(m.a(var, var)).a((Object)var).a(io.reactivex.v.o_());
   }

   // $FF: synthetic method
   static void a(g.a var, android.support.v4.g.j var) throws Exception {
      boolean var;
      if(!((Boolean)var.a).booleanValue()) {
         var = true;
      } else {
         var = false;
      }

      if(var && !co.uk.getmondo.payments.send.data.a.b.a.c((String)var.b)) {
         var.b();
      } else {
         var.n();
      }

   }

   // $FF: synthetic method
   static void a(g.a var, co.uk.getmondo.payments.send.data.a.b var) throws Exception {
      var.a(var.c());
   }

   // $FF: synthetic method
   static void a(g.a var, io.reactivex.b.b var) throws Exception {
      var.o();
   }

   // $FF: synthetic method
   static void a(g.a var, Throwable var) throws Exception {
      var.p();
   }

   // $FF: synthetic method
   static void a(g var, g.a var, Throwable var) throws Exception {
      var.b(var, var);
   }

   // $FF: synthetic method
   static void b(g.a var, android.support.v4.g.j var) throws Exception {
      boolean var;
      if(!((Boolean)var.a).booleanValue()) {
         var = true;
      } else {
         var = false;
      }

      if(var && !co.uk.getmondo.payments.send.data.a.b.a.b((String)var.b)) {
         var.a();
      } else {
         var.m();
      }

   }

   private void b(g.a var, Throwable var) {
      var.n();
      var.m();
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         if(var != null) {
            co.uk.getmondo.payments.a.a.c var = (co.uk.getmondo.payments.a.a.c)co.uk.getmondo.common.e.d.a(co.uk.getmondo.payments.a.a.c.values(), var.a());
            if(var != null) {
               var.a(var);
               return;
            }
         }
      }

      if(!this.d.a(var, var)) {
         var.b(2131362198);
      }

   }

   // $FF: synthetic method
   static void c(g.a var, android.support.v4.g.j var) throws Exception {
      boolean var;
      if(!((Boolean)var.a).booleanValue()) {
         var = true;
      } else {
         var = false;
      }

      if(var && !co.uk.getmondo.payments.send.data.a.b.a.a((String)var.b)) {
         var.k();
      } else {
         var.l();
      }

   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.a((io.reactivex.b.b)io.reactivex.n.combineLatest(var.d(), var.e(), var.f(), h.a()).subscribe(n.a(var)));
      this.a((io.reactivex.b.b)var.g().withLatestFrom((io.reactivex.r)var.d(), (io.reactivex.c.c)o.a()).subscribe(p.a(var)));
      this.a((io.reactivex.b.b)var.h().withLatestFrom((io.reactivex.r)var.e(), (io.reactivex.c.c)q.a()).subscribe(r.a(var)));
      this.a((io.reactivex.b.b)var.i().withLatestFrom((io.reactivex.r)var.f(), (io.reactivex.c.c)s.a()).subscribe(t.a(var)));
      io.reactivex.n var = var.j().filter(u.a()).flatMapSingle(i.a(this, var));
      var.getClass();
      this.a((io.reactivex.b.b)var.subscribe(j.a(var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, a {
      void a(co.uk.getmondo.payments.send.data.a.b var);

      void a(boolean var);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();
   }
}
