package co.uk.getmondo.payments.send.bank.payee;

public final class v implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!v.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public v(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new v(var, var, var, var, var);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g((io.reactivex.u)this.c.b(), (io.reactivex.u)this.d.b(), (co.uk.getmondo.payments.send.data.a)this.e.b(), (co.uk.getmondo.common.e.a)this.f.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
