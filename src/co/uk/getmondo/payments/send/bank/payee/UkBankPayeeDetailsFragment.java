package co.uk.getmondo.payments.send.bank.payee;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.payments.send.bank.payment.BankPaymentDetailsActivity;

public class UkBankPayeeDetailsFragment extends co.uk.getmondo.common.f.a implements g.a {
   g a;
   @BindView(2131821403)
   EditText accountNumberEditText;
   @BindView(2131821402)
   TextInputLayout accountNumberInputLayout;
   private Unbinder c;
   @BindView(2131821404)
   Button nextButton;
   @BindView(2131821405)
   ProgressBar progress;
   @BindView(2131821399)
   EditText recipientEditText;
   @BindView(2131821398)
   TextInputLayout recipientInputLayout;
   @BindView(2131821401)
   EditText sortCodeEditText;
   @BindView(2131821400)
   TextInputLayout sortCodeInputLayout;

   // $FF: synthetic method
   static co.uk.getmondo.payments.send.data.a.b a(UkBankPayeeDetailsFragment var, Object var) throws Exception {
      return new co.uk.getmondo.payments.send.data.a.b(var.recipientEditText.getText().toString(), var.sortCodeEditText.getText().toString().replace("-", ""), var.accountNumberEditText.getText().toString());
   }

   // $FF: synthetic method
   static String a(CharSequence var) throws Exception {
      return var.toString().replace("-", "");
   }

   public void a() {
      this.sortCodeInputLayout.setError(this.getString(2131362853));
   }

   public void a(co.uk.getmondo.payments.send.data.a.b var) {
      this.startActivity(BankPaymentDetailsActivity.a((Context)this.getActivity(), (co.uk.getmondo.payments.send.data.a.b)var));
   }

   public void a(boolean var) {
      this.nextButton.setEnabled(var);
   }

   public void b() {
      this.accountNumberInputLayout.setError(this.getString(2131362850));
   }

   public void c() {
      this.b(2131362851);
   }

   public io.reactivex.n d() {
      return com.b.a.d.e.c(this.recipientEditText).map(b.a());
   }

   public io.reactivex.n e() {
      return com.b.a.d.e.c(this.sortCodeEditText).map(c.a());
   }

   public io.reactivex.n f() {
      return com.b.a.d.e.c(this.accountNumberEditText).map(d.a());
   }

   public io.reactivex.n g() {
      return com.b.a.c.c.b(this.recipientEditText).b();
   }

   public io.reactivex.n h() {
      return com.b.a.c.c.b(this.sortCodeEditText).b();
   }

   public io.reactivex.n i() {
      return com.b.a.c.c.b(this.accountNumberEditText).b();
   }

   public io.reactivex.n j() {
      return io.reactivex.n.merge(com.b.a.c.c.a(this.nextButton), com.b.a.d.e.b(this.accountNumberEditText)).map(e.a(this));
   }

   public void k() {
      this.recipientInputLayout.setError(this.getString(2131362852));
   }

   public void l() {
      this.recipientInputLayout.setError("");
   }

   public void m() {
      this.sortCodeInputLayout.setError("");
   }

   public void n() {
      this.accountNumberInputLayout.setError("");
   }

   public void o() {
      this.nextButton.setEnabled(false);
      this.progress.setVisibility(0);
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034289, var, false);
   }

   public void onDestroyView() {
      this.c.unbind();
      this.a.b();
      super.onDestroyView();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.c = ButterKnife.bind(this, (View)var);
      this.a.a((g.a)this);
      co.uk.getmondo.common.k.i var = co.uk.getmondo.common.k.i.a('-').a((int)2);
      this.sortCodeEditText.addTextChangedListener(var.a());
   }

   public void p() {
      this.nextButton.setEnabled(true);
      this.progress.setVisibility(8);
   }
}
