package co.uk.getmondo.payments.send.bank;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class BankPaymentActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;

   public static Intent a(Context var) {
      return new Intent(var, BankPaymentActivity.class);
   }

   public void a(co.uk.getmondo.d.c var) {
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(String.format(this.getString(2131362021), new Object[]{var.toString()}));
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034146);
      this.l().a(this);
      this.a.a((b.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
