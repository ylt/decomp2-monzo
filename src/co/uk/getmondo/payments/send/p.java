package co.uk.getmondo.payments.send;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import io.reactivex.u;

public final class p implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;
   private final javax.a.a n;

   static {
      boolean var;
      if(!p.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public p(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                                 if(!a && var == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var;
                                    if(!a && var == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var;
                                       if(!a && var == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var;
                                          if(!a && var == null) {
                                             throw new AssertionError();
                                          } else {
                                             this.n = var;
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new p(var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public o a() {
      return (o)b.a.c.a(this.b, new o((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.common.e.a)this.e.b(), (co.uk.getmondo.common.accounts.d)this.f.b(), (co.uk.getmondo.common.k.f)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (MonzoApi)this.i.b(), (co.uk.getmondo.payments.send.a.e)this.j.b(), (PeerToPeerRepository)this.k.b(), (co.uk.getmondo.payments.send.data.a)this.l.b(), (co.uk.getmondo.common.accounts.b)this.m.b(), (co.uk.getmondo.common.o)this.n.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
