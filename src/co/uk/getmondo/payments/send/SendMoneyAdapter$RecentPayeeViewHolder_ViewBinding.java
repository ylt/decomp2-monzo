package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$RecentPayeeViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.RecentPayeeViewHolder a;

   public SendMoneyAdapter$RecentPayeeViewHolder_ViewBinding(SendMoneyAdapter.RecentPayeeViewHolder var, View var) {
      this.a = var;
      var.payeeImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821606, "field 'payeeImageView'", ImageView.class);
      var.payeeNameView = (TextView)Utils.findRequiredViewAsType(var, 2131821607, "field 'payeeNameView'", TextView.class);
      var.payeeIdentifierView = (TextView)Utils.findRequiredViewAsType(var, 2131821608, "field 'payeeIdentifierView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.RecentPayeeViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.payeeImageView = null;
         var.payeeNameView = null;
         var.payeeIdentifierView = null;
      }
   }
}
