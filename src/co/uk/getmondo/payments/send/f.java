package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class f implements OnClickListener {
   private final SendMoneyAdapter.RecentPayeeViewHolder a;
   private final SendMoneyAdapter.f b;

   private f(SendMoneyAdapter.RecentPayeeViewHolder var, SendMoneyAdapter.f var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(SendMoneyAdapter.RecentPayeeViewHolder var, SendMoneyAdapter.f var) {
      return new f(var, var);
   }

   public void onClick(View var) {
      SendMoneyAdapter.RecentPayeeViewHolder.a(this.a, this.b, var);
   }
}
