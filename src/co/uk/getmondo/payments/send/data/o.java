package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.common.x;

public final class o implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!o.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public o(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new o(var, var, var, var);
   }

   public h a() {
      return new h((MonzoApi)this.b.b(), (p)this.c.b(), (co.uk.getmondo.common.o)this.d.b(), (x)this.e.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
