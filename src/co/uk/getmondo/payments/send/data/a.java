package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.PaymentsApi;

public class a {
   private final PaymentsApi a;
   private final co.uk.getmondo.common.accounts.d b;
   private final co.uk.getmondo.payments.a.i c;
   private final f d;

   public a(PaymentsApi var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.payments.a.i var, f var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   private io.reactivex.b a(co.uk.getmondo.payments.send.data.a.c var, String var, String var) {
      return this.b.d().c(c.a(this, var, var, var));
   }

   private io.reactivex.b a(String var, co.uk.getmondo.payments.send.data.a.g var, String var, String var) {
      co.uk.getmondo.d.c var = var.a();
      String var = var.c().d();
      String var = "phone";
      String var = var;
      if(co.uk.getmondo.common.k.p.d(var)) {
         var = var.c().e();
         var = "username";
      }

      io.reactivex.b var;
      if(co.uk.getmondo.common.k.p.d(var)) {
         var = io.reactivex.b.a((Throwable)(new IllegalArgumentException("Peer must have a phone number or username")));
      } else {
         var = this.a.transferMoneyToPeer(var, var, var, -var.k(), var.l().b(), "pin", var, var, var.b());
      }

      return var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var, co.uk.getmondo.payments.send.data.a.c var, String var, String var, String var) throws Exception {
      co.uk.getmondo.d.c var = var.a();
      co.uk.getmondo.payments.send.data.a.b var = var.c();
      co.uk.getmondo.payments.a.a.d var = var.e();
      io.reactivex.b var;
      if(var != null && !var.a()) {
         var = var.c.a(var, var, var);
      } else {
         var = var.a.transferMoneyToBank(var, var.k(), var.l().b(), var.e(), var.d(), var.b(), var, "pin", var, var.b());
      }

      return var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var, co.uk.getmondo.payments.send.data.a.f var, String var, String var, String var) throws Exception {
      io.reactivex.b var;
      if(var instanceof co.uk.getmondo.payments.send.data.a.g) {
         var = var.a(var, (co.uk.getmondo.payments.send.data.a.g)var, var, var);
      } else {
         if(!(var instanceof co.uk.getmondo.payments.send.data.a.c)) {
            throw new RuntimeException("Unsupported payment type");
         }

         var = var.a((co.uk.getmondo.payments.send.data.a.c)var, var, var);
      }

      return var;
   }

   public io.reactivex.b a(co.uk.getmondo.payments.send.data.a.b var) {
      return this.a.validatePayee(var.e(), var.d());
   }

   public io.reactivex.b a(String var, co.uk.getmondo.payments.send.data.a.f var, String var, String var) {
      return io.reactivex.b.a(b.a(this, var, var, var, var));
   }

   public io.reactivex.n a() {
      return this.d.a();
   }
}
