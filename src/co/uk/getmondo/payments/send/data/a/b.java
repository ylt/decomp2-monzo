package co.uk.getmondo.payments.send.data.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import co.uk.getmondo.common.k.o;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001d\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J'\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\u0006\u0010\u001b\u001a\u00020\u0017J\t\u0010\u001c\u001a\u00020\u0006HÖ\u0001J\u001a\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u00032\u0006\u0010 \u001a\u00020\u0015H\u0016R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u000e\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u000b¨\u0006\""},
   d2 = {"Lco/uk/getmondo/payments/send/data/model/BankPayee;", "Lco/uk/getmondo/payments/send/data/model/Payee;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "name", "", "sortCode", "accountNumber", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "getName", "getSortCode", "sortCodeWithDashes", "getSortCodeWithDashes", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "isValid", "toString", "writeToParcel", "", "dest", "flags", "Validator", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements e {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public b a(Parcel var) {
         l.b(var, "source");
         return new b(var);
      }

      public b[] a(int var) {
         return new b[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final b.a a = new b.a((i)null);
   private static final kotlin.h.g e = new kotlin.h.g("^[0-9]{2}[0-9]{2}[0-9]{2}$");
   private static final kotlin.h.g f = new kotlin.h.g("^[0-9]{6,10}$");
   private final String b;
   private final String c;
   private final String d;

   public b(Parcel var) {
      l.b(var, "source");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      this(var, var, var);
   }

   public b(String var, String var, String var) {
      l.b(var, "name");
      l.b(var, "sortCode");
      l.b(var, "accountNumber");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final String a() {
      return o.a(this.c);
   }

   public String b() {
      return this.b;
   }

   public final boolean c() {
      boolean var;
      if(a.a(this.b()) && a.b(this.c) && a.c(this.d)) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final String d() {
      return this.c;
   }

   public int describeContents() {
      return 0;
   }

   public final String e() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label30: {
            if(var instanceof b) {
               b var = (b)var;
               if(l.a(this.b(), var.b()) && l.a(this.c, var.c) && l.a(this.d, var.d)) {
                  break label30;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.b();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + var * 31) * 31 + var;
   }

   public String toString() {
      return "BankPayee(name=" + this.b() + ", sortCode=" + this.c + ", accountNumber=" + this.d + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      if(var != null) {
         var.writeString(this.b());
      }

      if(var != null) {
         var.writeString(this.c);
      }

      if(var != null) {
         var.writeString(this.d);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u000fJ\u000e\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u000fR\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\t¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/payments/send/data/model/BankPayee$Validator;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "accountNumberRegex", "Lkotlin/text/Regex;", "getAccountNumberRegex", "()Lkotlin/text/Regex;", "sortCodeRegex", "getSortCodeRegex", "isValidAccountNumber", "", "accountNumber", "", "isValidRecipientName", "recipientName", "isValidSortCode", "sortCode", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      private final kotlin.h.g a() {
         return b.e;
      }

      private final kotlin.h.g b() {
         return b.f;
      }

      public final boolean a(String var) {
         l.b(var, "recipientName");
         boolean var;
         if(!j.a((CharSequence)var)) {
            var = true;
         } else {
            var = false;
         }

         return var;
      }

      public final boolean b(String var) {
         l.b(var, "sortCode");
         CharSequence var = (CharSequence)var;
         return ((b.a)this).a().a(var);
      }

      public final boolean c(String var) {
         l.b(var, "accountNumber");
         CharSequence var = (CharSequence)var;
         return ((b.a)this).b().a(var);
      }
   }
}
