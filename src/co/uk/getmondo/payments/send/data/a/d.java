package co.uk.getmondo.payments.send.data.a;

public enum d {
   a("enabled"),
   b("disabled"),
   c("blocked");

   public final String d;

   private d(String var) {
      this.d = var;
   }

   public static d a(String var) {
      d[] var = values();
      int var = var.length;

      for(int var = 0; var < var; ++var) {
         d var = var[var];
         if(var.d.equals(var)) {
            return var;
         }
      }

      throw new IllegalArgumentException("P2pStatus id " + var + " not found");
   }

   public String a() {
      return this.d;
   }
}
