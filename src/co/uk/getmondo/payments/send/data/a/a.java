package co.uk.getmondo.payments.send.data.a;

import io.realm.bc;
import io.realm.internal.l;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006B\u0005¢\u0006\u0002\u0010\u0007J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0096\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u0003H\u0016R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001e\u0010\f\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\t\"\u0004\b\u000e\u0010\u000bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\u000bR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\t\"\u0004\b\u0012\u0010\u000b¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/payments/send/data/model/BankDetails;", "Lio/realm/RealmObject;", "name", "", "sortCode", "accountNumber", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "()V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "id", "getId", "setId", "getName", "setName", "getSortCode", "setSortCode", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class a extends bc implements io.realm.f {
   private String a;
   private String b;
   private String c;
   private String d;

   public a() {
      if(this instanceof l) {
         ((l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
      this.d("");
   }

   public a(String var, String var, String var) {
      kotlin.d.b.l.b(var, "name");
      kotlin.d.b.l.b(var, "sortCode");
      kotlin.d.b.l.b(var, "accountNumber");
      this();
      if(this instanceof l) {
         ((l)this).u_();
      }

      this.a("" + var + ':' + var);
      this.b(var);
      this.c(var);
      this.d(var);
   }

   public final String a() {
      return this.e();
   }

   public void a(String var) {
      this.a = var;
   }

   public final String b() {
      return this.f();
   }

   public void b(String var) {
      this.b = var;
   }

   public final String c() {
      return this.g();
   }

   public void c(String var) {
      this.c = var;
   }

   public String d() {
      return this.a;
   }

   public void d(String var) {
      this.d = var;
   }

   public String e() {
      return this.b;
   }

   public boolean equals(Object var) {
      boolean var;
      if((a)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(kotlin.d.b.l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.send.data.model.BankDetails");
            }

            a var = (a)var;
            if(kotlin.d.b.l.a(this.e(), ((a)var).e()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.f(), ((a)var).f()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.g(), ((a)var).g()) ^ true) {
               var = false;
            } else if(kotlin.d.b.l.a(this.d(), ((a)var).d()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public String f() {
      return this.c;
   }

   public String g() {
      return this.d;
   }

   public int hashCode() {
      return ((this.e().hashCode() * 31 + this.f().hashCode()) * 31 + this.g().hashCode()) * 31 + this.d().hashCode();
   }

   public String toString() {
      return "BankDetails(name='" + this.e() + "', sortCode='" + this.f() + "', accountId='" + this.g() + "', id='" + this.d() + "')";
   }
}
