package co.uk.getmondo.payments.send.data.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 '2\u00020\u0001:\u0001'B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B+\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rJ\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0017\u001a\u00020\bHÆ\u0003J\t\u0010\u0018\u001a\u00020\nHÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\fHÆ\u0003J3\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fHÆ\u0001J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\u001cHÖ\u0001J\t\u0010\"\u001a\u00020\bHÖ\u0001J\u001a\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u00032\u0006\u0010&\u001a\u00020\u001cH\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\t\u001a\u00020\nX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006("},
   d2 = {"Lco/uk/getmondo/payments/send/data/model/BankPayment;", "Lco/uk/getmondo/payments/send/data/model/Payment;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "amount", "Lco/uk/getmondo/model/Amount;", "reference", "", "payee", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "schedule", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "(Lco/uk/getmondo/model/Amount;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/model/BankPayee;Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V", "getAmount", "()Lco/uk/getmondo/model/Amount;", "getPayee", "()Lco/uk/getmondo/payments/send/data/model/BankPayee;", "getReference", "()Ljava/lang/String;", "getSchedule", "()Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements f {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public c a(Parcel var) {
         l.b(var, "source");
         return new c(var);
      }

      public c[] a(int var) {
         return new c[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final c.a a = new c.a((i)null);
   private final co.uk.getmondo.d.c b;
   private final String c;
   private final b d;
   private final co.uk.getmondo.payments.a.a.d e;

   public c(Parcel var) {
      l.b(var, "source");
      Parcelable var = var.readParcelable(co.uk.getmondo.d.c.class.getClassLoader());
      l.a(var, "source.readParcelable<Am…::class.java.classLoader)");
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)var;
      String var = var.readString();
      l.a(var, "source.readString()");
      Parcelable var = var.readParcelable(b.class.getClassLoader());
      l.a(var, "source.readParcelable<Ba…::class.java.classLoader)");
      this(var, var, (b)var, (co.uk.getmondo.payments.a.a.d)var.readParcelable(co.uk.getmondo.payments.a.a.d.class.getClassLoader()));
   }

   public c(co.uk.getmondo.d.c var, String var, b var, co.uk.getmondo.payments.a.a.d var) {
      l.b(var, "amount");
      l.b(var, "reference");
      l.b(var, "payee");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   // $FF: synthetic method
   public c(co.uk.getmondo.d.c var, String var, b var, co.uk.getmondo.payments.a.a.d var, int var, i var) {
      if((var & 8) != 0) {
         var = (co.uk.getmondo.payments.a.a.d)null;
      }

      this(var, var, var, var);
   }

   public co.uk.getmondo.d.c a() {
      return this.b;
   }

   public final String b() {
      return this.c;
   }

   public b c() {
      return this.d;
   }

   // $FF: synthetic method
   public e d() {
      return (e)this.c();
   }

   public int describeContents() {
      return 0;
   }

   public final co.uk.getmondo.payments.a.a.d e() {
      return this.e;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof c) {
               c var = (c)var;
               if(l.a(this.a(), var.a()) && l.a(this.c, var.c) && l.a(this.c(), var.c()) && l.a(this.e, var.e)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      co.uk.getmondo.d.c var = this.a();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      b var = this.c();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      co.uk.getmondo.payments.a.a.d var = this.e;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "BankPayment(amount=" + this.a() + ", reference=" + this.c + ", payee=" + this.c() + ", schedule=" + this.e + ")";
   }

   public void writeToParcel(Parcel var, int var) {
      if(var != null) {
         var.writeParcelable((Parcelable)this.a(), 0);
      }

      if(var != null) {
         var.writeString(this.c);
      }

      if(var != null) {
         var.writeParcelable((Parcelable)this.c(), 0);
      }

      if(var != null) {
         var.writeParcelable((Parcelable)this.e, 0);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/send/data/model/BankPayment$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/send/data/model/BankPayment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
