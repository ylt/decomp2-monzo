package co.uk.getmondo.payments.send.data;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class j implements Callable {
   private final h a;

   private j(h var) {
      this.a = var;
   }

   public static Callable a(h var) {
      return new j(var);
   }

   public Object call() {
      return h.a(this.a);
   }
}
