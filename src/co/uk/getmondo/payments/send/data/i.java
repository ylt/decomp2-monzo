package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.model.ApiUserSettings;

// $FF: synthetic class
final class i implements io.reactivex.c.h {
   private final h a;

   private i(h var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(h var) {
      return new i(var);
   }

   public Object a(Object var) {
      return h.a(this.a, (ApiUserSettings)var);
   }
}
