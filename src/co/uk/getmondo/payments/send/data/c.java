package co.uk.getmondo.payments.send.data;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final a a;
   private final co.uk.getmondo.payments.send.data.a.c b;
   private final String c;
   private final String d;

   private c(a var, co.uk.getmondo.payments.send.data.a.c var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public static io.reactivex.c.h a(a var, co.uk.getmondo.payments.send.data.a.c var, String var, String var) {
      return new c(var, var, var, var);
   }

   public Object a(Object var) {
      return a.a(this.a, this.b, this.c, this.d, (String)var);
   }
}
