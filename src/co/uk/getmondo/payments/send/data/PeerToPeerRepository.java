package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.PaymentsApi;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.al;
import io.reactivex.v;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\r\u001a\u00020\u0006J.\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\r\u001a\u00020\u00062\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "", "paymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "(Lco/uk/getmondo/api/PaymentsApi;)V", "USER_KEY", "", "userByContact", "Lio/reactivex/Single;", "Lco/uk/getmondo/model/Peer;", "contact", "Lco/uk/getmondo/payments/send/contacts/Contact;", "userByUsername", "username", "verifyContactIsOnMonzo", "userResponse", "Lco/uk/getmondo/model/UserResponse;", "UserHasP2pDisabledException", "UserNotOnMonzoException", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class PeerToPeerRepository {
   private final String a;
   private final PaymentsApi b;

   public PeerToPeerRepository(PaymentsApi var) {
      kotlin.d.b.l.b(var, "paymentsApi");
      super();
      this.b = var;
      this.a = "0";
   }

   private final v a(String var, co.uk.getmondo.payments.send.a.b var, al var) {
      String var;
      String var;
      label48: {
         var = null;
         if(var != null) {
            var = var.b();
            if(var != null) {
               break label48;
            }
         }

         var = var;
      }

      v var;
      if(var != null && var.a() != null && var.a().containsKey(this.a)) {
         al.a var = (al.a)var.a().get(this.a);
         if(var != null && !var.a()) {
            String var = var.b();
            String var = var.c();
            String var;
            if(var != null) {
               var = var.c();
            } else {
               var = null;
            }

            if(var != null) {
               var = var.b();
            } else {
               var = null;
            }

            if(var != null) {
               var = var.e();
            }

            boolean var;
            if(var != null) {
               var = true;
            } else {
               var = false;
            }

            var = v.a((Object)(new aa(var, var, var, var, var, var, var)));
            kotlin.d.b.l.a(var, "Single.just(Peer(user.us…        contact != null))");
         } else {
            var = v.a((Throwable)(new PeerToPeerRepository.UserHasP2pDisabledException(var)));
            kotlin.d.b.l.a(var, "Single.error(UserHasP2pDisabledException(name))");
         }
      } else {
         var = v.a((Throwable)(new PeerToPeerRepository.a(var)));
         kotlin.d.b.l.a(var, "Single.error(UserNotOnMonzoException(name))");
      }

      return var;
   }

   public final v a(final co.uk.getmondo.payments.send.a.b var) {
      kotlin.d.b.l.b(var, "contact");
      PaymentsApi var = this.b;
      String var = var.c();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      v var = var.userByPhoneNumber(var).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(al varx) {
            kotlin.d.b.l.b(varx, "userResponse");
            return PeerToPeerRepository.a(PeerToPeerRepository.this, (String)null, var, varx, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var, "paymentsApi.userByPhoneN…esponse = userResponse) }");
      return var;
   }

   public final v a(final String var) {
      kotlin.d.b.l.b(var, "username");
      v var = this.b.userByUsername(var).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(al varx) {
            kotlin.d.b.l.b(varx, "userResponse");
            return PeerToPeerRepository.a(PeerToPeerRepository.this, var, (co.uk.getmondo.payments.send.a.b)null, varx, 2, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var, "paymentsApi.userByUserna…esponse = userResponse) }");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u0015\u0010\t\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0004HÖ\u0001R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "user", "", "(Ljava/lang/String;)V", "getUser", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class UserHasP2pDisabledException extends Exception {
      private final String a;

      public UserHasP2pDisabledException(String var) {
         this.a = var;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label26: {
               if(var instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                  PeerToPeerRepository.UserHasP2pDisabledException var = (PeerToPeerRepository.UserHasP2pDisabledException)var;
                  if(kotlin.d.b.l.a(this.a, var.a)) {
                     break label26;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         String var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var;
      }

      public String toString() {
         return "UserHasP2pDisabledException(user=" + this.a + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserNotOnMonzoException;", "", "user", "", "(Ljava/lang/String;)V", "getUser", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends Throwable {
      private final String a;

      public a(String var) {
         this.a = var;
      }

      public final String a() {
         return this.a;
      }

      public boolean equals(Object var) {
         boolean var;
         if(this != var) {
            label26: {
               if(var instanceof PeerToPeerRepository.a) {
                  PeerToPeerRepository.a var = (PeerToPeerRepository.a)var;
                  if(kotlin.d.b.l.a(this.a, var.a)) {
                     break label26;
                  }
               }

               var = false;
               return var;
            }
         }

         var = true;
         return var;
      }

      public int hashCode() {
         String var = this.a;
         int var;
         if(var != null) {
            var = var.hashCode();
         } else {
            var = 0;
         }

         return var;
      }

      public String toString() {
         return "UserNotOnMonzoException(user=" + this.a + ")";
      }
   }
}
