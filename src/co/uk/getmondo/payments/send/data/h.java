package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiConfig;
import co.uk.getmondo.api.model.ApiUserSettings;
import co.uk.getmondo.common.x;
import co.uk.getmondo.d.am;
import io.reactivex.v;
import io.realm.av;
import io.realm.bg;

public class h {
   private final MonzoApi a;
   private final p b;
   private final co.uk.getmondo.common.o c;
   private final x d;

   public h(MonzoApi var, p var, co.uk.getmondo.common.o var, x var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   static io.reactivex.d a(h var, ApiUserSettings var) throws Exception {
      return var.a();
   }

   // $FF: synthetic method
   static bg a(av var) {
      return var.a(am.class).g();
   }

   // $FF: synthetic method
   static Boolean a(h var) throws Exception {
      boolean var;
      if(var.b.a() == co.uk.getmondo.payments.send.data.a.d.a) {
         var = true;
      } else {
         var = false;
      }

      return Boolean.valueOf(var);
   }

   // $FF: synthetic method
   static void a(h var, ApiConfig var) throws Exception {
      co.uk.getmondo.payments.send.data.a.d var = co.uk.getmondo.payments.send.data.a.d.a(var.a().a());
      String var;
      if(var.b() == null) {
         var = "";
      } else {
         var = var.b().a();
      }

      co.uk.getmondo.d.p var = (new co.uk.getmondo.d.a.h()).a(var.d());
      var.d.a(var.e());
      var.b.a(new am(var.a(), var, var, var.f()));
      var.c.a(new co.uk.getmondo.d.l(var.c().a(), var.c().b()));
   }

   public io.reactivex.b a() {
      return this.a.config().c(k.a(this)).c();
   }

   public v a(boolean var) {
      return this.a.optInToPeerToPeer(var).c(i.a(this)).b(j.a(this));
   }

   public io.reactivex.n b() {
      return co.uk.getmondo.common.j.g.a(l.a()).filter(m.a()).map(n.a());
   }

   public String c() {
      av var = av.n();
      am var = (am)var.a(am.class).h();
      String var;
      if(var != null) {
         var = var.b();
      } else {
         var = "";
      }

      var.close();
      return var;
   }

   public Boolean d() {
      return Boolean.valueOf(this.b.c());
   }

   public co.uk.getmondo.d.p e() {
      return this.b.b();
   }
}
