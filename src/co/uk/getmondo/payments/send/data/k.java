package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.model.ApiConfig;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final h a;

   private k(h var) {
      this.a = var;
   }

   public static io.reactivex.c.g a(h var) {
      return new k(var);
   }

   public void a(Object var) {
      h.a(this.a, (ApiConfig)var);
   }
}
