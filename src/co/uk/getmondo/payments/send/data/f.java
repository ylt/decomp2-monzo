package co.uk.getmondo.payments.send.data;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/payments/send/data/RecentPayeesStorage;", "", "()V", "recentPayees", "Lio/reactivex/Observable;", "", "Lco/uk/getmondo/payments/send/data/model/Payee;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f {
   public final io.reactivex.n a() {
      io.reactivex.n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).map((io.reactivex.c.h)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "RxRealm.asObservable { r…      }\n                }");
      return var;
   }
}
