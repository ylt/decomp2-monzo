package co.uk.getmondo.payments.send.data;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final a a;
   private final co.uk.getmondo.payments.send.data.a.f b;
   private final String c;
   private final String d;
   private final String e;

   private b(a var, co.uk.getmondo.payments.send.data.a.f var, String var, String var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public static Callable a(a var, co.uk.getmondo.payments.send.data.a.f var, String var, String var, String var) {
      return new b(var, var, var, var, var);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c, this.d, this.e);
   }
}
