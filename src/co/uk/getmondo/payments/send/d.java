package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final SendMoneyAdapter.a a;

   private d(SendMoneyAdapter.a var) {
      this.a = var;
   }

   public static OnClickListener a(SendMoneyAdapter.a var) {
      return new d(var);
   }

   public void onClick(View var) {
      SendMoneyAdapter.b.a(this.a, var);
   }
}
