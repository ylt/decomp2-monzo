package co.uk.getmondo.payments.send;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiContactDiscovery;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.q;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001.Bk\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019¢\u0006\u0002\u0010\u001aJB\u0010\u001d\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\u001f0\u001e2\u0018\u0010\"\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020$0\u001e0#2\u0006\u0010\u0010\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020$2\u0006\u0010'\u001a\u00020$H\u0002J\u0012\u0010(\u001a\u00020$2\b\u0010)\u001a\u0004\u0018\u00010$H\u0002J\u0010\u0010*\u001a\u00020$2\u0006\u0010'\u001a\u00020$H\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0002H\u0016R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006/"},
   d2 = {"Lco/uk/getmondo/payments/send/SendMoneyPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "hashSHA256", "Lco/uk/getmondo/common/utils/HashSHA256;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "contacts", "Lco/uk/getmondo/payments/send/contacts/RxContacts;", "peerToPeerRepository", "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "paymentsManager", "Lco/uk/getmondo/payments/send/data/PaymentsManager;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/utils/HashSHA256;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/contacts/RxContacts;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/data/PaymentsManager;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "contactNameComparator", "Lco/uk/getmondo/payments/send/contacts/ContactNameComparator;", "findContactsOnMonzo", "Lkotlin/Pair;", "", "Lco/uk/getmondo/payments/send/contacts/Contact;", "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;", "pairs", "", "", "Lco/uk/getmondo/api/model/ApiContactDiscovery;", "firstPartOfHashedNumber", "number", "hash", "toHash", "nextDigitsOfHashedNumber", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.payments.send.a.c c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.k.f h;
   private final co.uk.getmondo.common.a i;
   private final MonzoApi j;
   private final co.uk.getmondo.payments.send.a.e k;
   private final PeerToPeerRepository l;
   private final co.uk.getmondo.payments.send.data.a m;
   private final co.uk.getmondo.common.accounts.b n;
   private final co.uk.getmondo.common.o o;

   public o(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.k.f var, co.uk.getmondo.common.a var, MonzoApi var, co.uk.getmondo.payments.send.a.e var, PeerToPeerRepository var, co.uk.getmondo.payments.send.data.a var, co.uk.getmondo.common.accounts.b var, co.uk.getmondo.common.o var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "accountService");
      kotlin.d.b.l.b(var, "hashSHA256");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "monzoApi");
      kotlin.d.b.l.b(var, "contacts");
      kotlin.d.b.l.b(var, "peerToPeerRepository");
      kotlin.d.b.l.b(var, "paymentsManager");
      kotlin.d.b.l.b(var, "accountManager");
      kotlin.d.b.l.b(var, "featureFlagsStorage");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
      this.n = var;
      this.o = var;
      this.c = new co.uk.getmondo.payments.send.a.c();
   }

   private final String a(String var) {
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
      } else {
         var = var.substring(5, 12);
         kotlin.d.b.l.a(var, "(this as java.lang.Strin…ing(startIndex, endIndex)");
         return var;
      }
   }

   private final kotlin.h a(List var, ApiContactDiscovery var) {
      ArrayList var = new ArrayList();
      android.support.v4.g.a var = new android.support.v4.g.a(var.size());
      Iterator var = var.iterator();

      while(var.hasNext()) {
         boolean var;
         kotlin.h var;
         String var;
         label47: {
            var = (kotlin.h)var.next();
            var = this.b((String)var.b());
            if(var.a() != null && var.a().containsKey(var)) {
               Object var = var.a().get(var);
               if(var == null) {
                  kotlin.d.b.l.a();
               }

               Iterator var = ((List)var).iterator();

               while(var.hasNext()) {
                  String var = (String)var.next();
                  if(kotlin.d.b.l.a(this.a((String)var.b()), var)) {
                     var = true;
                     break label47;
                  }
               }
            }

            var = false;
         }

         co.uk.getmondo.payments.send.a.b var = (co.uk.getmondo.payments.send.a.b)var.a();
         long var = var.a();
         if(var) {
            var.add(var);
         } else if(var.containsKey(Long.valueOf(var))) {
            co.uk.getmondo.payments.send.a.d var = (co.uk.getmondo.payments.send.a.d)var.get(Long.valueOf(var));
            if(var != null) {
               List var = var.f();
               if(var != null) {
                  String var = var.c();
                  if(var == null) {
                     kotlin.d.b.l.a();
                  }

                  var.add(var);
               }
            }
         } else {
            ArrayList var = new ArrayList();
            var = var.c();
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var.add(var);
            var.put(Long.valueOf(var), new co.uk.getmondo.payments.send.a.d(var, var.b(), var.e(), (List)var));
         }
      }

      Collections.sort((List)var, (Comparator)this.c);
      ArrayList var = new ArrayList(var.values());
      Collections.sort((List)var, (Comparator)this.c);
      return new kotlin.h(var, var);
   }

   private final String b(String var) {
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
      } else {
         var = var.substring(0, 5);
         kotlin.d.b.l.a(var, "(this as java.lang.Strin…ing(startIndex, endIndex)");
         return var;
      }
   }

   private final String c(String var) {
      try {
         var = this.h.a(var);
         kotlin.d.b.l.a(var, "hashSHA256.hash(toHash)");
      } catch (NoSuchAlgorithmException var) {
         var = "";
      }

      return var;
   }

   public void a(final o.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var) {
            o.this.k.b();
         }
      }));
      io.reactivex.n var;
      if(var.i()) {
         var = io.reactivex.n.just(kotlin.n.a);
      } else {
         var = var.b();
      }

      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = io.reactivex.n.merge((r)var, (r)var.g()).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return this.b(var);
         }

         public final io.reactivex.n b(Object var) {
            kotlin.d.b.l.b(var, "it");
            return o.this.k.a();
         }
      })).observeOn(this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Map varx) {
            var.l();
         }
      })).observeOn(this.e).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Map var) {
            ak var = o.this.g.b();
            if(var == null) {
               kotlin.d.b.l.a();
            }

            ac var = var.d();
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var.remove(var.f());
         }
      })).flatMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(Map var) {
            kotlin.d.b.l.b(var, "contactsMap");
            return io.reactivex.n.fromIterable((Iterable)var.values()).map((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(co.uk.getmondo.payments.send.a.b var) {
                  kotlin.d.b.l.b(var, "contact");
                  return new kotlin.h(var, o.this.c(o.this.c(var.c())));
               }
            })).toList();
         }
      })).flatMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(final List var) {
            kotlin.d.b.l.b(var, "pairs");
            return io.reactivex.n.fromIterable((Iterable)var).map((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final String a(kotlin.h var) {
                  kotlin.d.b.l.b(var, "contactAndHashedNumber");
                  return o.this.b((String)var.b());
               }
            })).distinct().toList().a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final v a(List var) {
                  kotlin.d.b.l.b(var, "it");
                  return o.this.j.queryContacts(var);
               }
            })).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(ApiContactDiscovery varx) {
                  kotlin.d.b.l.b(varx, "contacts");
                  o var = o.this;
                  List var = var;
                  kotlin.d.b.l.a(var, "pairs");
                  return var.a(var, varx);
               }
            }));
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            Collection var = (Collection)varx.c();
            Collection var = (Collection)varx.d();
            var.m();
            var.a(var, var);
            o.this.i.a(Impression.Companion.a(var.size(), var.size()));
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            var.m();
            co.uk.getmondo.common.e.a var = o.this.f;
            kotlin.d.b.l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      kotlin.d.b.l.a(var, "Observable.merge(permiss… view)\n                })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var;
      if(this.n.b()) {
         var.h();
         var = this.b;
         io.reactivex.b.b var = this.m.a().filter((q)(new q() {
            public final boolean a(List var) {
               kotlin.d.b.l.b(var, "it");
               return o.this.n.b();
            }
         })).observeOn(this.e).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(List varx) {
               o.a var = var;
               kotlin.d.b.l.a(varx, "it");
               var.a((Collection)varx);
            }
         }), (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(Throwable varx) {
               co.uk.getmondo.common.e.a var = o.this.f;
               kotlin.d.b.l.a(varx, "error");
               var.a(varx, (co.uk.getmondo.common.e.a.a)var);
            }
         }));
         kotlin.d.b.l.a(var, "paymentsManager.recentPa…ndleError(error, view) })");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = var.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.payments.send.data.a.e varx) {
               if(varx instanceof co.uk.getmondo.payments.send.data.a.b) {
                  var.a((co.uk.getmondo.payments.send.data.a.b)varx);
               } else {
                  if(!(varx instanceof aa)) {
                     throw (Throwable)(new RuntimeException("Unexpected payee type"));
                  }

                  var.a((aa)varx);
               }

            }
         }));
         kotlin.d.b.l.a(var, "view.onRecentPayeeClicke…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = this.o.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.l varx) {
               if(varx.a()) {
                  if(!var.i()) {
                     var.j();
                  }

                  var.k();
               }

            }
         }));
         kotlin.d.b.l.a(var, "featureFlagsStorage.feat…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      } else {
         if(!var.i()) {
            var.j();
         }

         var.k();
      }

      var = this.b;
      var = var.c().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.send.a.b varx) {
            var.n();
         }
      })).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(co.uk.getmondo.payments.send.a.b varx) {
            kotlin.d.b.l.b(varx, "contact");
            return o.this.l.a(varx).f().subscribeOn(o.this.e).observeOn(o.this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(aa varx) {
                  var.o();
               }
            })).doOnError((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.o();
                  if(varx instanceof PeerToPeerRepository.a) {
                     var.b(2131362637);
                     var.a(((PeerToPeerRepository.a)varx).a());
                     o.this.i.a(Impression.Companion.Z());
                  } else if(varx instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                     var.b(2131362636);
                  } else {
                     co.uk.getmondo.common.e.a var = o.this.f;
                     kotlin.d.b.l.a(varx, "error");
                     if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                        var.b(2131362642);
                     }
                  }

               }
            })).onErrorResumeNext((r)io.reactivex.n.empty());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(aa varx) {
            o.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onContactWithMonzoC… view.openSendMoney(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.d().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var) {
            o.this.i.a(Impression.Companion.aa());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.onContactNotOnMonzo… view.inviteContact(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object varx) {
            var.p();
            o.this.i.a(Impression.Companion.ab());
         }
      }));
      kotlin.d.b.l.a(var, "view.onAddContactClicked…tact())\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\b\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\tH&J\b\u0010\u000b\u001a\u00020\tH&J\b\u0010\f\u001a\u00020\tH&J\u0012\u0010\r\u001a\u00020\t2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006H&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0006H&J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\b\u0010\u0017\u001a\u00020\tH&J\u0010\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH&J\u0010\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001dH&J\b\u0010\u001e\u001a\u00020\tH&J\b\u0010\u001f\u001a\u00020\tH&J$\u0010 \u001a\u00020\t2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\"2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\"H&J\b\u0010%\u001a\u00020\tH&J\u0016\u0010&\u001a\u00020\t2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00150\"H&¨\u0006("},
      d2 = {"Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "checkContactsPermission", "", "contactsPermissionAcceptClicks", "Lio/reactivex/Observable;", "", "enableBankPayments", "", "enableContactsRefresh", "hideContactLoading", "hideListLoading", "inviteContact", "name", "", "onAddContactClicked", "onContactNotOnMonzoClicked", "onContactWithMonzoClicked", "Lco/uk/getmondo/payments/send/contacts/Contact;", "onRecentPayeeClicked", "Lco/uk/getmondo/payments/send/data/model/Payee;", "onRefreshAction", "openAddContact", "openBankTransfer", "bankPayee", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "openSendMoney", "peer", "Lco/uk/getmondo/model/Peer;", "requestContactsPermission", "showContactLoading", "showContacts", "contactsOnMonzo", "", "allContacts", "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;", "showListLoading", "showRecentPayees", "recentPayees", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(aa var);

      void a(co.uk.getmondo.payments.send.data.a.b var);

      void a(String var);

      void a(Collection var);

      void a(Collection var, Collection var);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      void h();

      boolean i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();
   }
}
