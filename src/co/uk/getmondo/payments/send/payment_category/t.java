package co.uk.getmondo.payments.send.payment_category;

import io.reactivex.u;

public final class t implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!t.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public t(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new t(var, var, var, var, var);
   }

   public o a() {
      return (o)b.a.c.a(this.b, new o((u)this.c.b(), (b.a)this.d.b(), (co.uk.getmondo.common.a)this.e.b(), (co.uk.getmondo.payments.send.data.a.f)this.f.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
