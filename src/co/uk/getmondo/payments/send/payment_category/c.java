package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.feed.ApiFeedItem;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class c implements Callable {
   private final b a;
   private final ApiFeedItem b;

   private c(b var, ApiFeedItem var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(b var, ApiFeedItem var) {
      return new c(var, var);
   }

   public Object call() {
      return b.a(this.a, this.b);
   }
}
