package co.uk.getmondo.payments.send.payment_category;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.main.HomeActivity;

public class PaymentCategoryActivity extends co.uk.getmondo.common.activities.b implements o.a {
   o a;
   @BindView(2131821019)
   AmountView amountView;
   @BindView(2131821022)
   CategoryView categoryView;
   @BindView(2131821020)
   TextView confirmationTextView;

   public static Intent a(Context var, co.uk.getmondo.payments.send.data.a.f var) {
      return (new Intent(var, PaymentCategoryActivity.class)).putExtra("KEY_PAYMENT", var);
   }

   // $FF: synthetic method
   static void a(PaymentCategoryActivity var) throws Exception {
      var.categoryView.setCategoryClickListener((CategoryView.a)null);
   }

   // $FF: synthetic method
   static void a(PaymentCategoryActivity var, io.reactivex.o var) throws Exception {
      CategoryView var = var.categoryView;
      var.getClass();
      var.setCategoryClickListener(i.a(var));
      var.a(j.a(var));
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(h.a(this));
   }

   public void a(co.uk.getmondo.d.c var) {
      this.amountView.setAmount(var);
   }

   public void a(String var) {
      this.confirmationTextView.setText(this.getString(2131362635, new Object[]{var}));
   }

   public void b() {
      HomeActivity.a((Context)this);
   }

   public void onBackPressed() {
      super.onBackPressed();
      this.b();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034193);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.f var = (co.uk.getmondo.payments.send.data.a.f)this.getIntent().getParcelableExtra("KEY_PAYMENT");
      this.l().a(new m(var)).a(this);
      this.a.a((o.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
