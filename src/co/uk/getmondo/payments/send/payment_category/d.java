package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.feed.ApiFeedItem;

// $FF: synthetic class
final class d implements io.reactivex.c.g {
   private final b a;

   private d(b var) {
      this.a = var;
   }

   public static io.reactivex.c.g a(b var) {
      return new d(var);
   }

   public void a(Object var) {
      b.b(this.a, (ApiFeedItem)var);
   }
}
