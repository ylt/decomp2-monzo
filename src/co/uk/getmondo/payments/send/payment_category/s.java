package co.uk.getmondo.payments.send.payment_category;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class s implements Callable {
   private final o a;
   private final co.uk.getmondo.d.h b;

   private s(o var, co.uk.getmondo.d.h var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(o var, co.uk.getmondo.d.h var) {
      return new s(var, var);
   }

   public Object call() {
      return o.a(this.a, this.b);
   }
}
