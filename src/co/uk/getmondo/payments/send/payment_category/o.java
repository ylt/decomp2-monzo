package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;

class o extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.common.a d;
   private final b.a e;
   private final co.uk.getmondo.payments.send.data.a.f f;

   o(u var, b.a var, co.uk.getmondo.common.a var, co.uk.getmondo.payments.send.data.a.f var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.h a(o var, co.uk.getmondo.d.h var) throws Exception {
      var.e.a(var.f());
      return var;
   }

   private io.reactivex.n a(co.uk.getmondo.d.h var) {
      return io.reactivex.n.fromCallable(s.a(this, var)).subscribeOn(this.c);
   }

   // $FF: synthetic method
   static void a(o var, o.a var, co.uk.getmondo.d.h var) throws Exception {
      var.d.a(Impression.a(var));
      var.b();
   }

   // $FF: synthetic method
   static io.reactivex.n b(o var, co.uk.getmondo.d.h var) {
      return var.a(var);
   }

   public void a(o.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.f.a());
      var.a(this.f.d().b());
      this.a((io.reactivex.b.b)var.a().switchMap(p.a(this)).subscribe(q.a(this, var), r.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var);

      void a(String var);

      void b();
   }
}
