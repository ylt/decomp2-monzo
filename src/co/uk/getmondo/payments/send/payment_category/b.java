package co.uk.getmondo.payments.send.payment_category;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.feed.ApiFeedItem;
import co.uk.getmondo.api.model.feed.ApiTransaction;
import io.reactivex.v;
import io.reactivex.z;

public class b {
   private final MonzoApi a;
   private final b.a b;

   public b(MonzoApi var, b.a var) {
      this.a = var;
      this.b = var;
   }

   private ApiFeedItem a(ApiFeedItem var, String var) {
      ApiTransaction var = var.f().a(var);
      return new ApiFeedItem(var.a(), var.b(), var.c(), var.d(), var.e(), var, (ApiFeedItem.Params)null, var.h(), var.i());
   }

   // $FF: synthetic method
   static z a(b var, ApiFeedItem var) throws Exception {
      ApiTransaction var = var.f();
      v var;
      if(var != null && var.d() && var.i() < 0L && a(var) && var.b.a() != null) {
         String var = var.b.a();
         var = var.a.updateTransactionCategory(var.g(), var).a((Object)var.a(var, var)).c(d.a(var)).d(e.a()).b((Object)var);
      } else {
         var = v.a((Object)var);
      }

      return var;
   }

   private static boolean a(ApiTransaction var) {
      boolean var;
      if(var.t() == null || !var.t().isEmpty() && !"mondo".equalsIgnoreCase(var.t()) && !"monzo".equalsIgnoreCase(var.t())) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   // $FF: synthetic method
   static void b(b var, ApiFeedItem var) throws Exception {
      var.b.b();
   }

   public v a(ApiFeedItem var) {
      return v.a(c.a(this, var));
   }

   public static class a {
      private final SharedPreferences a;

      public a(Context var) {
         this.a = var.getSharedPreferences("p2p_category_hack", 0);
      }

      String a() {
         return this.a.getString("KEY_CATEGORY", (String)null);
      }

      void a(String var) {
         this.a.edit().putString("KEY_CATEGORY", var).apply();
      }

      public void b() {
         this.a.edit().remove("KEY_CATEGORY").apply();
      }
   }
}
