package co.uk.getmondo.payments.send.payment_category;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class PaymentCategoryActivity_ViewBinding implements Unbinder {
   private PaymentCategoryActivity a;

   public PaymentCategoryActivity_ViewBinding(PaymentCategoryActivity var, View var) {
      this.a = var;
      var.amountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821019, "field 'amountView'", AmountView.class);
      var.categoryView = (CategoryView)Utils.findRequiredViewAsType(var, 2131821022, "field 'categoryView'", CategoryView.class);
      var.confirmationTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821020, "field 'confirmationTextView'", TextView.class);
   }

   public void unbind() {
      PaymentCategoryActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountView = null;
         var.categoryView = null;
         var.confirmationTextView = null;
      }
   }
}
