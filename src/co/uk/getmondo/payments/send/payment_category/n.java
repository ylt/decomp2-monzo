package co.uk.getmondo.payments.send.payment_category;

public final class n implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final m b;

   static {
      boolean var;
      if(!n.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public n(m var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(m var) {
      return new n(var);
   }

   public co.uk.getmondo.payments.send.data.a.f a() {
      return (co.uk.getmondo.payments.send.data.a.f)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
