package co.uk.getmondo.payments.send.payment_category;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridLayout.LayoutParams;

public class CategoryView extends GridLayout {
   private CategoryView.a a;

   public CategoryView(Context var) {
      super(var);
      this.a();
   }

   public CategoryView(Context var, AttributeSet var) {
      super(var, var);
      this.a();
   }

   public CategoryView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.a();
   }

   private View a(co.uk.getmondo.d.h var) {
      View var = LayoutInflater.from(this.getContext()).inflate(2131034356, this, false);
      TextView var = (TextView)var.findViewById(2131821568);
      ImageView var = (ImageView)var.findViewById(2131821567);
      var.setText(var.a());
      var.setTextColor(android.support.v4.content.a.c(var.getContext(), var.b()));
      var.setImageResource(var.e());
      var.setOnClickListener(a.a(this, var));
      return var;
   }

   private void a() {
      this.setColumnCount(this.getResources().getInteger(2131558402));
      co.uk.getmondo.d.h[] var = co.uk.getmondo.d.h.values();
      int var = var.length;

      for(int var = 0; var < var; ++var) {
         View var = this.a(var[var]);
         LayoutParams var = new LayoutParams();
         var.setGravity(17);
         var.columnSpec = GridLayout.spec(Integer.MIN_VALUE, GridLayout.FILL, 1.0F);
         var.setLayoutParams(var);
         var.setMinimumWidth(co.uk.getmondo.common.k.n.b(80));
         this.addView(var);
      }

   }

   // $FF: synthetic method
   static void a(CategoryView var, co.uk.getmondo.d.h var, View var) {
      if(var.a != null) {
         var.a.a(var);
      }

   }

   public void setCategoryClickListener(CategoryView.a var) {
      this.a = var;
   }

   interface a {
      void a(co.uk.getmondo.d.h var);
   }
}
