package co.uk.getmondo.payments.send.payment_category;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final CategoryView a;
   private final co.uk.getmondo.d.h b;

   private a(CategoryView var, co.uk.getmondo.d.h var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(CategoryView var, co.uk.getmondo.d.h var) {
      return new a(var, var);
   }

   public void onClick(View var) {
      CategoryView.a(this.a, this.b, var);
   }
}
