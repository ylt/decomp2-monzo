package co.uk.getmondo.payments.send.authentication;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.PinEntryView;

public class PaymentAuthenticationActivity_ViewBinding implements Unbinder {
   private PaymentAuthenticationActivity a;

   public PaymentAuthenticationActivity_ViewBinding(PaymentAuthenticationActivity var, View var) {
      this.a = var;
      var.contactImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821011, "field 'contactImageView'", ImageView.class);
      var.contactNameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821012, "field 'contactNameTextView'", TextView.class);
      var.amountView = (AmountView)Utils.findRequiredViewAsType(var, 2131821013, "field 'amountView'", AmountView.class);
      var.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821018, "field 'progressBar'", ProgressBar.class);
      var.progressBarOverlay = Utils.findRequiredView(var, 2131821017, "field 'progressBarOverlay'");
      var.pinEntryView = (PinEntryView)Utils.findRequiredViewAsType(var, 2131821682, "field 'pinEntryView'", PinEntryView.class);
      var.accountNumberTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821014, "field 'accountNumberTextView'", TextView.class);
      var.scheduleDescriptionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821015, "field 'scheduleDescriptionTextView'", TextView.class);
   }

   public void unbind() {
      PaymentAuthenticationActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.contactImageView = null;
         var.contactNameTextView = null;
         var.amountView = null;
         var.progressBar = null;
         var.progressBarOverlay = null;
         var.pinEntryView = null;
         var.accountNumberTextView = null;
         var.scheduleDescriptionTextView = null;
      }
   }
}
