package co.uk.getmondo.payments.send.authentication;

import co.uk.getmondo.common.ui.PinEntryView;

// $FF: synthetic class
final class b implements PinEntryView.a {
   private final io.reactivex.o a;

   private b(io.reactivex.o var) {
      this.a = var;
   }

   public static PinEntryView.a a(io.reactivex.o var) {
      return new b(var);
   }

   public void a(String var) {
      this.a.a(var);
   }
}
