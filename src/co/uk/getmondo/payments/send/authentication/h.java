package co.uk.getmondo.payments.send.authentication;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.z;
import java.util.UUID;

class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.a f;
   private final co.uk.getmondo.common.a g;
   private final co.uk.getmondo.payments.send.data.a.f h;
   private final co.uk.getmondo.common.accounts.b i;

   h(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.send.data.a var, co.uk.getmondo.common.a var, co.uk.getmondo.payments.send.data.a.f var, co.uk.getmondo.common.accounts.b var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static z a(h var, String var, h.a var, String var) throws Exception {
      return var.f.a(var.i.c(), var.h, var, var).b(var.d).a(var.c).a(n.a(var, var)).a((Object)Boolean.valueOf(true)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(h.a var, String var) throws Exception {
      var.f();
      var.c();
   }

   private void a(h.a var, Throwable var) {
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         if(var != null) {
            p var = (p)co.uk.getmondo.common.e.d.a(p.values(), var.a());
            if(var != null) {
               var.a(var, this.h);
               if(var == p.d) {
                  this.g.a(Impression.R());
               }

               return;
            }
         }
      }

      if(!this.e.a(var, var)) {
         var.b();
      }

   }

   // $FF: synthetic method
   static void a(h var, h.a var, Boolean var) throws Exception {
      if(var.h instanceof co.uk.getmondo.payments.send.data.a.g) {
         var.g.a(Impression.h(var.booleanValue()));
      }

      if(var.booleanValue()) {
         var.a(var.h);
      } else {
         var.e();
         var.g();
         var.d();
      }

   }

   // $FF: synthetic method
   static void a(h var, h.a var, Throwable var) throws Exception {
      var.a(var, var);
   }

   // $FF: synthetic method
   static boolean a(String var) throws Exception {
      boolean var;
      if(var.length() == 4) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(h.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      if(this.h instanceof co.uk.getmondo.payments.send.data.a.g) {
         var.a((co.uk.getmondo.payments.send.data.a.g)this.h);
      } else if(this.h instanceof co.uk.getmondo.payments.send.data.a.c) {
         var.a((co.uk.getmondo.payments.send.data.a.c)this.h);
      }

      String var = UUID.randomUUID().toString();
      this.a((io.reactivex.b.b)var.a().filter(i.a()).doOnNext(j.a(var)).flatMapSingle(k.a(this, var, var)).subscribe(l.a(this, var), m.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(p var, co.uk.getmondo.payments.send.data.a.f var);

      void a(co.uk.getmondo.payments.send.data.a.c var);

      void a(co.uk.getmondo.payments.send.data.a.f var);

      void a(co.uk.getmondo.payments.send.data.a.g var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
