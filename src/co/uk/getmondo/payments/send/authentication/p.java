package co.uk.getmondo.payments.send.authentication;

import android.content.res.Resources;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH&R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getPrefix", "()Ljava/lang/String;", "getDisplayMessage", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "BLOCKED", "PIN_INCORRECT", "INSUFFICIENT_FUNDS", "RECIPIENT_NOT_ENABLED", "RECIPIENT_NOT_ACTIVE", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum p implements co.uk.getmondo.common.e.f {
   a,
   b,
   c,
   d,
   e;

   private final String g;

   static {
      p.a var = new p.a("BLOCKED", 0);
      a = var;
      p.c var = new p.c("PIN_INCORRECT", 1);
      b = var;
      p.b var = new p.b("INSUFFICIENT_FUNDS", 2);
      c = var;
      p.e var = new p.e("RECIPIENT_NOT_ENABLED", 3);
      d = var;
      p.d var = new p.d("RECIPIENT_NOT_ACTIVE", 4);
      e = var;
   }

   protected p(String var) {
      kotlin.d.b.l.b(var, "prefix");
      super(var, var);
      this.g = var;
   }

   public String a() {
      return this.g;
   }

   public abstract String a(Resources var, co.uk.getmondo.payments.send.data.a.f var);

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError$BLOCKED;", "Lco/uk/getmondo/payments/send/authentication/PaymentError;", "(Ljava/lang/String;I)V", "getDisplayMessage", "", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends p {
      a(String var, int var) {
         super("forbidden");
      }

      public String a(Resources var, co.uk.getmondo.payments.send.data.a.f var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "payment");
         String var = var.getString(2131362562);
         kotlin.d.b.l.a(var, "resources.getString(R.st….pin_blocked_description)");
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError$INSUFFICIENT_FUNDS;", "Lco/uk/getmondo/payments/send/authentication/PaymentError;", "(Ljava/lang/String;I)V", "getDisplayMessage", "", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends p {
      b(String var, int var) {
         super("bad_request.insufficient_funds");
      }

      public String a(Resources var, co.uk.getmondo.payments.send.data.a.f var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "payment");
         String var = var.getString(2131362534);
         kotlin.d.b.l.a(var, "resources.getString(R.st…yment_insufficient_funds)");
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError$PIN_INCORRECT;", "Lco/uk/getmondo/payments/send/authentication/PaymentError;", "(Ljava/lang/String;I)V", "getDisplayMessage", "", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends p {
      c(String var, int var) {
         super("bad_request.bad_param.pin");
      }

      public String a(Resources var, co.uk.getmondo.payments.send.data.a.f var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "payment");
         String var = var.getString(2131362178);
         kotlin.d.b.l.a(var, "resources.getString(R.string.error_pin_incorrect)");
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError$RECIPIENT_NOT_ACTIVE;", "Lco/uk/getmondo/payments/send/authentication/PaymentError;", "(Ljava/lang/String;I)V", "getDisplayMessage", "", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class d extends p {
      d(String var, int var) {
         super("bad_request.recipient_not_active");
      }

      public String a(Resources var, co.uk.getmondo.payments.send.data.a.f var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "payment");
         String var = var.getString(2131362653, new Object[]{var.d().b()});
         kotlin.d.b.l.a(var, "resources.getString(R.st…tive, payment.payee.name)");
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/send/authentication/PaymentError$RECIPIENT_NOT_ENABLED;", "Lco/uk/getmondo/payments/send/authentication/PaymentError;", "(Ljava/lang/String;I)V", "getDisplayMessage", "", "resources", "Landroid/content/res/Resources;", "payment", "Lco/uk/getmondo/payments/send/data/model/Payment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class e extends p {
      e(String var, int var) {
         super("bad_request.recipient_not_enabled");
      }

      public String a(Resources var, co.uk.getmondo.payments.send.data.a.f var) {
         kotlin.d.b.l.b(var, "resources");
         kotlin.d.b.l.b(var, "payment");
         String var = var.getString(2131362654, new Object[]{var.d().b()});
         kotlin.d.b.l.a(var, "resources.getString(R.st…bled, payment.payee.name)");
         return var;
      }
   }
}
