package co.uk.getmondo.payments.send.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.PinEntryView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.payment_category.PaymentCategoryActivity;

public class PaymentAuthenticationActivity extends co.uk.getmondo.common.activities.b implements h.a {
   h a;
   @BindView(2131821014)
   TextView accountNumberTextView;
   @BindView(2131821013)
   AmountView amountView;
   co.uk.getmondo.payments.send.a b;
   private co.uk.getmondo.common.ui.a c;
   @BindView(2131821011)
   ImageView contactImageView;
   @BindView(2131821012)
   TextView contactNameTextView;
   @BindView(2131821682)
   PinEntryView pinEntryView;
   @BindView(2131821018)
   ProgressBar progressBar;
   @BindView(2131821017)
   View progressBarOverlay;
   @BindView(2131821015)
   TextView scheduleDescriptionTextView;

   public static Intent a(Context var, co.uk.getmondo.payments.send.data.a.f var) {
      return (new Intent(var, PaymentAuthenticationActivity.class)).putExtra("KEY_PAYMENT", var);
   }

   // $FF: synthetic method
   static void a(PaymentAuthenticationActivity var) throws Exception {
      var.pinEntryView.setOnPinEnteredListener((PinEntryView.a)null);
   }

   // $FF: synthetic method
   static void a(PaymentAuthenticationActivity var, io.reactivex.o var) throws Exception {
      PinEntryView var = var.pinEntryView;
      var.getClass();
      var.setOnPinEnteredListener(b.a(var));
      var.a(c.a(var));
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(a.a(this));
   }

   public void a(p var, co.uk.getmondo.payments.send.data.a.f var) {
      boolean var = true;
      if(var == p.a) {
         co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), "tag_error_dialog");
      } else {
         String var = var.a(this.getResources(), var);
         if(var == p.b) {
            var = false;
         }

         co.uk.getmondo.common.d.a.a(this.getString(2131362641), var, var).show(this.getSupportFragmentManager(), "tag_error_dialog");
      }

   }

   public void a(co.uk.getmondo.payments.send.data.a.c var) {
      byte var = 8;
      co.uk.getmondo.payments.send.data.a.b var = var.c();
      this.contactImageView.setVisibility(8);
      this.contactNameTextView.setText(var.b());
      this.amountView.setAmount(var.a());
      String var = var.a();
      String var = var.e();
      String var = var.b();
      this.accountNumberTextView.setText(TextUtils.join("  •  ", new String[]{var, var, var}));
      this.accountNumberTextView.setVisibility(0);
      String var = this.b.a(var.e());
      this.scheduleDescriptionTextView.setText(var);
      TextView var = this.scheduleDescriptionTextView;
      if(!var.isEmpty()) {
         var = 0;
      }

      var.setVisibility(var);
   }

   public void a(co.uk.getmondo.payments.send.data.a.f var) {
      this.finish();
      this.startActivity(PaymentCategoryActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var));
   }

   public void a(co.uk.getmondo.payments.send.data.a.g var) {
      aa var = var.c();
      if(var.f()) {
         com.bumptech.glide.g.b(this.contactImageView.getContext()).a(Uri.parse(var.g())).h().a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.contactImageView)));
      } else {
         int var = (int)TypedValue.applyDimension(2, 26.0F, this.getResources().getDisplayMetrics());
         this.contactImageView.setImageDrawable(this.c.a(var.b()).a(var));
      }

      this.contactImageView.setVisibility(0);
      this.contactNameTextView.setText(var.b());
      this.amountView.setAmount(var.a());
   }

   public void b() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362641), this.getString(2131362642), false).show(this.getSupportFragmentManager(), "tag_error_dialog");
   }

   public void c() {
      this.progressBar.setVisibility(0);
      this.progressBarOverlay.setVisibility(0);
   }

   public void d() {
      this.progressBar.setVisibility(8);
      this.progressBarOverlay.setVisibility(8);
   }

   public void e() {
      this.pinEntryView.a();
   }

   public void f() {
      this.pinEntryView.b();
   }

   public void g() {
      this.pinEntryView.c();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034192);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.f var = (co.uk.getmondo.payments.send.data.a.f)this.getIntent().getParcelableExtra("KEY_PAYMENT");
      if(var == null) {
         throw new RuntimeException("Activity requires a payment");
      } else {
         co.uk.getmondo.common.ui.a var = this.c;
         this.c = co.uk.getmondo.common.ui.a.a((Context)this);
         this.l().a(new f(var)).a(this);
         this.a.a((h.a)this);
      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
