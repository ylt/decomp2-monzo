package co.uk.getmondo.payments.send.authentication;

// $FF: synthetic class
final class k implements io.reactivex.c.h {
   private final h a;
   private final String b;
   private final h.a c;

   private k(h var, String var, h.a var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(h var, String var, h.a var) {
      return new k(var, var, var);
   }

   public Object a(Object var) {
      return h.a(this.a, this.b, this.c, (String)var);
   }
}
