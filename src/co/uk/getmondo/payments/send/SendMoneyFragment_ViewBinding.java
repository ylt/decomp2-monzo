package co.uk.getmondo.payments.send;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyFragment_ViewBinding implements Unbinder {
   private SendMoneyFragment a;

   public SendMoneyFragment_ViewBinding(SendMoneyFragment var, View var) {
      this.a = var;
      var.contactsRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131821363, "field 'contactsRecyclerView'", RecyclerView.class);
      var.contactsProgressBar = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821361, "field 'contactsProgressBar'", ProgressBar.class);
      var.swipeRefreshLayout = (SwipeRefreshLayout)Utils.findRequiredViewAsType(var, 2131821362, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
   }

   public void unbind() {
      SendMoneyFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.contactsRecyclerView = null;
         var.contactsProgressBar = null;
         var.swipeRefreshLayout = null;
      }
   }
}
