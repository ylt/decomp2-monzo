package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final b a;
   private final b.a b;

   private k(b var, b.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(b var, b.a var) {
      return new k(var, var);
   }

   public void a(Object var) {
      b.a(this.a, this.b, (Throwable)var);
   }
}
