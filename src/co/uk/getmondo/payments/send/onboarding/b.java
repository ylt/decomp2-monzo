package co.uk.getmondo.payments.send.onboarding;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;

class b extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.h f;
   private final co.uk.getmondo.common.a g;

   b(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.send.data.h var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   // $FF: synthetic method
   static z a(b var, b.a var, Object var) throws Exception {
      return var.f.a(true).b(var.d).a(var.c).b(i.a(var)).a(j.a(var)).d(k.a(var, var)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(b.a var, io.reactivex.b.b var) throws Exception {
      var.b(false);
   }

   // $FF: synthetic method
   static void a(b.a var, Boolean var, Throwable var) throws Exception {
      var.b(true);
   }

   // $FF: synthetic method
   static void a(b.a var, Object var) throws Exception {
      var.d();
   }

   // $FF: synthetic method
   static void a(b var, b.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   // $FF: synthetic method
   static boolean a(Boolean var) throws Exception {
      return var.booleanValue();
   }

   public void a(b.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.J());
      io.reactivex.n var = var.a().switchMapSingle(c.a(this, var)).filter(d.a());
      var.getClass();
      this.a((io.reactivex.b.b)var.subscribe(e.a(var), f.a()));
      this.a((io.reactivex.b.b)var.b().subscribe(g.a(var)));
      var = var.c();
      var.getClass();
      this.a((io.reactivex.b.b)var.subscribe(h.a(var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(boolean var);

      io.reactivex.n b();

      void b(boolean var);

      io.reactivex.n c();

      void d();
   }
}
