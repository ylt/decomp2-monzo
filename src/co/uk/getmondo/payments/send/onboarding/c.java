package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final b a;
   private final b.a b;

   private c(b var, b.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.h a(b var, b.a var) {
      return new c(var, var);
   }

   public Object a(Object var) {
      return b.a(this.a, this.b, var);
   }
}
