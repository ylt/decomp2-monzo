package co.uk.getmondo.payments.send.onboarding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PeerToPeerMoreInfoActivity extends co.uk.getmondo.common.activities.b implements PeerToPeerMoreInfoPresenter.a {
   PeerToPeerMoreInfoPresenter a;
   private final io.reactivex.i.a b = io.reactivex.i.a.a();
   @BindView(2131820798)
   Toolbar toolbar;
   @BindView(2131821029)
   WebView webView;

   public static void a(Activity var, int var) {
      var.startActivityForResult(new Intent(var, PeerToPeerMoreInfoActivity.class), var);
   }

   private void a(boolean var) {
      Intent var = new Intent();
      var.putExtra("KEY_RESULT_ENABLED", var);
      this.setResult(-1, var);
      this.finish();
   }

   public static boolean a(Intent var) {
      return var.getBooleanExtra("KEY_RESULT_ENABLED", false);
   }

   public io.reactivex.n a() {
      return this.b;
   }

   public void b() {
      this.a(false);
   }

   public void c() {
      this.a(true);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034196);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.toolbar.setNavigationIcon(2130837837);
      this.webView.getSettings().setJavaScriptEnabled(true);
      this.webView.setWebViewClient(new WebViewClient());
      this.webView.addJavascriptInterface(new PeerToPeerMoreInfoActivity.a(this.b), "mondo");
      this.webView.loadUrl("https://monzo.com/content/p2p/android");
      this.a.a((PeerToPeerMoreInfoPresenter.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   private static class a {
      private final io.reactivex.i.a a;

      a(io.reactivex.i.a var) {
         this.a = var;
      }

      @JavascriptInterface
      public void postMessage(String var) {
         this.a.onNext(var);
      }
   }
}
