package co.uk.getmondo.payments.send.onboarding;

import android.support.annotation.Keep;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;

class PeerToPeerMoreInfoPresenter extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.h f;
   private final co.uk.getmondo.common.a g;

   PeerToPeerMoreInfoPresenter(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.payments.send.data.h var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   // $FF: synthetic method
   static PeerToPeerMoreInfoPresenter.MessageFromWeb a(String var) throws Exception {
      return (PeerToPeerMoreInfoPresenter.MessageFromWeb)(new com.google.gson.f()).a(var, PeerToPeerMoreInfoPresenter.MessageFromWeb.class);
   }

   // $FF: synthetic method
   static io.reactivex.r a(PeerToPeerMoreInfoPresenter.a var, PeerToPeerMoreInfoPresenter.MessageFromWeb var) throws Exception {
      io.reactivex.n var;
      if(var.proceed()) {
         var = io.reactivex.n.just(var);
      } else if(var.dismiss()) {
         var.b();
         var = io.reactivex.n.empty();
      } else {
         var = io.reactivex.n.empty();
      }

      return var;
   }

   // $FF: synthetic method
   static z a(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var, PeerToPeerMoreInfoPresenter.MessageFromWeb var) throws Exception {
      return var.f.a(true).b(var.d).a(var.c).d(t.a(var, var)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(PeerToPeerMoreInfoPresenter.a var, Boolean var) throws Exception {
      var.c();
   }

   // $FF: synthetic method
   static void a(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   // $FF: synthetic method
   static boolean a(Boolean var) throws Exception {
      return var.booleanValue();
   }

   public void a(PeerToPeerMoreInfoPresenter.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.K());
      this.a((io.reactivex.b.b)var.a().map(n.a()).switchMap(o.a(var)).switchMapSingle(p.a(this, var)).filter(q.a()).subscribe(r.a(var), s.a()));
   }

   @Keep
   private static class MessageFromWeb {
      private final String action;

      public MessageFromWeb(String var) {
         this.action = var;
      }

      boolean dismiss() {
         return this.action.equals("dismiss");
      }

      boolean proceed() {
         return this.action.equals("request_permission");
      }
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void b();

      void c();
   }
}
