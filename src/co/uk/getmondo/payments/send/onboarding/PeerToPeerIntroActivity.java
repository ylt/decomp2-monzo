package co.uk.getmondo.payments.send.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PeerToPeerIntroActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;
   private final com.b.b.c b = com.b.b.c.a();
   @BindView(2131821027)
   Button enablePeerToPeerButton;
   @BindView(2131821028)
   Button moreInfoButton;
   @BindView(2131820798)
   Toolbar toolbar;

   public static Intent a(Context var, co.uk.getmondo.common.t var) {
      return (new Intent(var, PeerToPeerIntroActivity.class)).putExtra("KEY_NAV_FLOW", var);
   }

   public static Intent a(Context var, co.uk.getmondo.common.t var, co.uk.getmondo.payments.send.data.a.g var) {
      return a(var, var).putExtra("KEY_PENDING_PAYMENT", var);
   }

   public static co.uk.getmondo.common.t a(Intent var) {
      return (co.uk.getmondo.common.t)var.getSerializableExtra("KEY_NAV_FLOW");
   }

   public static co.uk.getmondo.payments.send.data.a.g b(Intent var) {
      return (co.uk.getmondo.payments.send.data.a.g)var.getParcelableExtra("KEY_PENDING_PAYMENT");
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.enablePeerToPeerButton);
   }

   public void a(boolean var) {
      Intent var = new Intent();
      var.putExtras(this.getIntent());
      byte var;
      if(var) {
         var = -1;
      } else {
         var = 0;
      }

      this.setResult(var, var);
      this.finish();
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.moreInfoButton);
   }

   public void b(boolean var) {
      this.enablePeerToPeerButton.setEnabled(var);
   }

   public io.reactivex.n c() {
      return this.b;
   }

   public void d() {
      PeerToPeerMoreInfoActivity.a(this, 1);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1 && var == -1) {
         this.b.a((Object)Boolean.valueOf(PeerToPeerMoreInfoActivity.a(var)));
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034195);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.toolbar.setNavigationIcon(2130837837);
      this.a.a((b.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
