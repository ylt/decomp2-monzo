package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class p implements io.reactivex.c.h {
   private final PeerToPeerMoreInfoPresenter a;
   private final PeerToPeerMoreInfoPresenter.a b;

   private p(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.h a(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var) {
      return new p(var, var);
   }

   public Object a(Object var) {
      return PeerToPeerMoreInfoPresenter.a(this.a, this.b, (PeerToPeerMoreInfoPresenter.MessageFromWeb)var);
   }
}
