package co.uk.getmondo.payments.send.onboarding;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class PeerToPeerIntroActivity_ViewBinding implements Unbinder {
   private PeerToPeerIntroActivity a;

   public PeerToPeerIntroActivity_ViewBinding(PeerToPeerIntroActivity var, View var) {
      this.a = var;
      var.enablePeerToPeerButton = (Button)Utils.findRequiredViewAsType(var, 2131821027, "field 'enablePeerToPeerButton'", Button.class);
      var.moreInfoButton = (Button)Utils.findRequiredViewAsType(var, 2131821028, "field 'moreInfoButton'", Button.class);
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      PeerToPeerIntroActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.enablePeerToPeerButton = null;
         var.moreInfoButton = null;
         var.toolbar = null;
      }
   }
}
