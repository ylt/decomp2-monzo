package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class t implements io.reactivex.c.g {
   private final PeerToPeerMoreInfoPresenter a;
   private final PeerToPeerMoreInfoPresenter.a b;

   private t(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(PeerToPeerMoreInfoPresenter var, PeerToPeerMoreInfoPresenter.a var) {
      return new t(var, var);
   }

   public void a(Object var) {
      PeerToPeerMoreInfoPresenter.a(this.a, this.b, (Throwable)var);
   }
}
