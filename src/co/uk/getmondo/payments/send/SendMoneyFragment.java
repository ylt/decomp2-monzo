package co.uk.getmondo.payments.send;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.bank.BankPaymentActivity;
import co.uk.getmondo.payments.send.bank.payment.BankPaymentDetailsActivity;
import co.uk.getmondo.payments.send.peer.PeerPaymentActivity;
import java.util.Collection;

public class SendMoneyFragment extends co.uk.getmondo.common.f.a implements o.a {
   o a;
   co.uk.getmondo.common.a c;
   @BindView(2131821361)
   ProgressBar contactsProgressBar;
   @BindView(2131821363)
   RecyclerView contactsRecyclerView;
   SendMoneyAdapter d;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private final com.b.b.c g = com.b.b.c.a();
   private final com.b.b.c h = com.b.b.c.a();
   private FloatingActionButton i;
   private Unbinder j;
   private boolean k = false;
   @BindView(2131821362)
   SwipeRefreshLayout swipeRefreshLayout;

   public static Fragment a() {
      return new SendMoneyFragment();
   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var) throws Exception {
      var.swipeRefreshLayout.setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)null);
   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var, co.uk.getmondo.payments.send.a.a var, boolean var) {
      if(var) {
         var.f.a((Object)((co.uk.getmondo.payments.send.a.b)var));
      } else {
         var.g.a((Object)var.b());
      }

   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var, io.reactivex.o var) throws Exception {
      var.swipeRefreshLayout.setOnRefreshListener(l.a(var));
      var.a(m.a(var));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var) {
      var.a(co.uk.getmondo.common.b.a.a);
   }

   // $FF: synthetic method
   static void b(SendMoneyFragment var) {
      var.startActivity(BankPaymentActivity.a((Context)var.getActivity()));
   }

   public void a(aa var) {
      this.startActivity(PeerPaymentActivity.a((Context)this.getActivity(), (aa)var, (Impression.PaymentFlowFrom)Impression.PaymentFlowFrom.FROM_CONTACT_DISCOVERY));
   }

   public void a(co.uk.getmondo.payments.send.data.a.b var) {
      this.startActivity(BankPaymentDetailsActivity.a((Context)this.getActivity(), (co.uk.getmondo.payments.send.data.a.b)var));
   }

   public void a(String var) {
      var = this.getString(2131362646, new Object[]{var});
      String var = this.getString(2131362645);
      this.startActivity(co.uk.getmondo.common.k.j.a(this.getActivity(), var, var, co.uk.getmondo.api.model.tracking.a.INVITE_CONTACT_DISCOVERY));
   }

   public void a(Collection var) {
      this.d.a(var);
   }

   public void a(Collection var, Collection var) {
      this.d.a(var, var);
   }

   public io.reactivex.n b() {
      return this.e;
   }

   public io.reactivex.n c() {
      return this.f;
   }

   public io.reactivex.n d() {
      return this.g;
   }

   public io.reactivex.n e() {
      return com.b.a.c.c.a(this.i);
   }

   public io.reactivex.n f() {
      return this.h;
   }

   public io.reactivex.n g() {
      return io.reactivex.n.create(k.a(this));
   }

   public void h() {
      this.d.a(true);
   }

   public boolean i() {
      boolean var;
      if(android.support.v4.content.a.b(this.getActivity(), "android.permission.READ_CONTACTS") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void j() {
      this.c.a(Impression.L());
      this.requestPermissions(new String[]{"android.permission.READ_CONTACTS"}, 0);
   }

   public void k() {
      this.swipeRefreshLayout.setEnabled(true);
   }

   public void l() {
      if(this.d.getItemCount() <= 1) {
         this.contactsProgressBar.setVisibility(0);
      } else {
         this.swipeRefreshLayout.setRefreshing(true);
      }

   }

   public void m() {
      this.contactsProgressBar.setVisibility(8);
      this.swipeRefreshLayout.setRefreshing(false);
   }

   public void n() {
      this.contactsProgressBar.setVisibility(0);
   }

   public void o() {
      this.contactsProgressBar.setVisibility(8);
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034278, var, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.j.unbind();
      super.onDestroyView();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      boolean var = true;
      if(var == 0) {
         boolean var;
         if(var.length > 0 && var[0] == 0) {
            this.e.a((Object)co.uk.getmondo.common.b.a.a);
            var = true;
         } else {
            var = false;
         }

         if(var) {
            var = false;
         }

         this.k = var;
         this.c.a(Impression.g(var));
      } else {
         super.onRequestPermissionsResult(var, var, var);
      }

   }

   public void onResume() {
      super.onResume();
      if(this.k) {
         co.uk.getmondo.common.d.a.a("", this.getString(2131362640), false).show(this.getChildFragmentManager(), "error_dialog");
         this.k = false;
      }

   }

   public void onViewCreated(View var, Bundle var) {
      super.onCreate(var);
      this.j = ButterKnife.bind(this, (View)var);
      this.i = (FloatingActionButton)this.getActivity().findViewById(2131821306);
      this.swipeRefreshLayout.setColorSchemeResources(new int[]{2131689558});
      this.swipeRefreshLayout.setEnabled(false);
      this.d.a(h.a(this));
      this.d.a(false);
      SendMoneyAdapter var = this.d;
      com.b.b.c var = this.h;
      var.getClass();
      var.a(i.a(var));
      this.d.a(j.a(this));
      this.contactsRecyclerView.setHasFixedSize(true);
      this.contactsRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
      this.contactsRecyclerView.setAdapter(this.d);
      int var = this.getResources().getDimensionPixelSize(2131427609);
      this.contactsRecyclerView.a(new co.uk.getmondo.common.ui.h(this.getActivity(), this.d, var));
      this.contactsRecyclerView.a(new a.a.a.a.a.b(this.d));
      this.a.a((o.a)this);
   }

   public void p() {
      Intent var = new Intent("android.intent.action.INSERT");
      var.setType("vnd.android.cursor.dir/raw_contact");
      var.putExtra("finishActivityOnSaveCompleted", true);
      this.startActivity(var);
   }
}
