package co.uk.getmondo.payments.send;

// $FF: synthetic class
final class h implements SendMoneyAdapter.c {
   private final SendMoneyFragment a;

   private h(SendMoneyFragment var) {
      this.a = var;
   }

   public static SendMoneyAdapter.c a(SendMoneyFragment var) {
      return new h(var);
   }

   public void a(co.uk.getmondo.payments.send.a.a var, boolean var) {
      SendMoneyFragment.a(this.a, var, var);
   }
}
