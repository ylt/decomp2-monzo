package co.uk.getmondo.payments.send.peer;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class PeerPaymentActivity_ViewBinding implements Unbinder {
   private PeerPaymentActivity a;

   public PeerPaymentActivity_ViewBinding(PeerPaymentActivity var, View var) {
      this.a = var;
      var.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var, 2131821023, "field 'amountInputView'", AmountInputView.class);
      var.notesEditText = (EditText)Utils.findRequiredViewAsType(var, 2131821024, "field 'notesEditText'", EditText.class);
      var.sendMoneyButton = (Button)Utils.findRequiredViewAsType(var, 2131821025, "field 'sendMoneyButton'", Button.class);
   }

   public void unbind() {
      PeerPaymentActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountInputView = null;
         var.notesEditText = null;
         var.sendMoneyButton = null;
      }
   }
}
