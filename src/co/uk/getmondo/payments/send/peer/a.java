package co.uk.getmondo.payments.send.peer;

// $FF: synthetic class
final class a implements Runnable {
   private final PeerPaymentActivity a;
   private final android.support.v7.app.a b;
   private final co.uk.getmondo.d.c c;

   private a(PeerPaymentActivity var, android.support.v7.app.a var, co.uk.getmondo.d.c var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static Runnable a(PeerPaymentActivity var, android.support.v7.app.a var, co.uk.getmondo.d.c var) {
      return new a(var, var, var);
   }

   public void run() {
      PeerPaymentActivity.a(this.a, this.b, this.c);
   }
}
