package co.uk.getmondo.payments.send.peer;

import co.uk.getmondo.d.aa;
import io.reactivex.u;

class j extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final aa g;
   private final co.uk.getmondo.d.c h;
   private final String i;
   private final co.uk.getmondo.a.a j;
   private co.uk.getmondo.d.c k;

   j(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, aa var, co.uk.getmondo.d.c var, String var, co.uk.getmondo.a.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   // $FF: synthetic method
   static void a() throws Exception {
      d.a.a.b("Balance synced", new Object[0]);
   }

   // $FF: synthetic method
   static void a(j var, j.a var, android.support.v4.g.j var) throws Exception {
      co.uk.getmondo.d.c var = co.uk.getmondo.d.c.a((String)var.a, co.uk.getmondo.common.i.c.a);
      if(!var.a(var)) {
         d.a.a.a((Throwable)(new RuntimeException("Balance string is not a valid number")));
      } else if(var.k == null || !var.k.a() && var.k() <= var.k.k()) {
         var.a(new co.uk.getmondo.payments.send.data.a.g(var, (String)var.b, var.g));
      } else {
         var.b(var.k);
      }

   }

   // $FF: synthetic method
   static void a(j var, j.a var, co.uk.getmondo.d.c var) throws Exception {
      var.k = var;
      var.a(var);
   }

   // $FF: synthetic method
   static void a(j var, j.a var, String var) throws Exception {
      var.a(var.a(co.uk.getmondo.d.c.a(var, co.uk.getmondo.common.i.c.a)));
   }

   // $FF: synthetic method
   static void a(j var, j.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   private boolean a(co.uk.getmondo.d.c var) {
      boolean var;
      if(var != null && var.f() > 0.0D) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(j.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.g.b());
      var.a(this.h, this.i);
      String var = this.f.b().c().a();
      this.a((io.reactivex.b.b)this.j.a(var).observeOn(this.c).map(k.a()).subscribe(l.a(this, var), m.a()));
      this.a((io.reactivex.b.b)this.j.b(var).b(this.d).a(this.c).a(n.b(), o.a(this, var)));
      this.a((io.reactivex.b.b)var.a().subscribe(p.a(this, var)));
      this.a((io.reactivex.b.b)var.b().subscribe(q.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var);

      void a(co.uk.getmondo.d.c var, String var);

      void a(co.uk.getmondo.payments.send.data.a.g var);

      void a(String var);

      void a(boolean var);

      io.reactivex.n b();

      void b(co.uk.getmondo.d.c var);
   }
}
