package co.uk.getmondo.payments.send.peer;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final PeerPaymentActivity a;

   private c(PeerPaymentActivity var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(PeerPaymentActivity var) {
      return new c(var);
   }

   public Object a(Object var) {
      return PeerPaymentActivity.a(this.a, var);
   }
}
