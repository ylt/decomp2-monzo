package co.uk.getmondo.payments.send.peer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountInputView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.authentication.PaymentAuthenticationActivity;

public class PeerPaymentActivity extends co.uk.getmondo.common.activities.b implements j.a {
   j a;
   @BindView(2131821023)
   AmountInputView amountInputView;
   co.uk.getmondo.common.a b;
   private final Handler c = new Handler();
   @BindView(2131821024)
   EditText notesEditText;
   @BindView(2131821025)
   Button sendMoneyButton;

   public static Intent a(Context var, aa var, Impression.PaymentFlowFrom var) {
      return (new Intent(var, PeerPaymentActivity.class)).putExtra("key_peer", var).putExtra("key_entry_point", var);
   }

   public static Intent a(Context var, co.uk.getmondo.payments.send.data.a.g var, Impression.PaymentFlowFrom var) {
      return (new Intent(var, PeerPaymentActivity.class)).putExtra("key_peer", var.c()).putExtra("key_notes", var.b()).putExtra("key_amount", var.a()).putExtra("key_entry_point", var);
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(PeerPaymentActivity var, Object var) throws Exception {
      return android.support.v4.g.j.a(var.amountInputView.getAmountText().toString(), var.notesEditText.getText().toString());
   }

   // $FF: synthetic method
   static void a(PeerPaymentActivity var, android.support.v7.app.a var, co.uk.getmondo.d.c var) {
      var.b(String.format(var.getString(2131362021), new Object[]{var.toString()}));
   }

   public io.reactivex.n a() {
      return this.amountInputView.a().map(b.a());
   }

   public void a(co.uk.getmondo.d.c var) {
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         this.c.post(a.a(this, var, var));
      }

   }

   public void a(co.uk.getmondo.d.c var, String var) {
      if(var != null && var.k() != 0L) {
         this.amountInputView.setDefaultAmount(var);
      }

      this.notesEditText.setText(var);
   }

   public void a(co.uk.getmondo.payments.send.data.a.g var) {
      this.startActivity(PaymentAuthenticationActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var));
   }

   public void a(String var) {
      this.setTitle(var);
   }

   public void a(boolean var) {
      this.sendMoneyButton.setEnabled(var);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.sendMoneyButton).map(c.a(this));
   }

   public void b(co.uk.getmondo.d.c var) {
      co.uk.getmondo.common.d.a.a(this.getString(2131362535), this.getString(2131362536, new Object[]{var.toString()}), false).show(this.getSupportFragmentManager(), "ERROR_FRAGMENT_TAG");
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034194);
      ButterKnife.bind((Activity)this);
      aa var = (aa)this.getIntent().getParcelableExtra("key_peer");
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("key_amount");
      String var = this.getIntent().getStringExtra("key_notes");
      this.l().a(new f(var, var, var)).a(this);
      this.a.a((j.a)this);
      Impression.PaymentFlowFrom var = (Impression.PaymentFlowFrom)this.getIntent().getSerializableExtra("key_entry_point");
      this.b.a(Impression.a(var));
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
