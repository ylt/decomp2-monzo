package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$ContactViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.ContactViewHolder a;

   public SendMoneyAdapter$ContactViewHolder_ViewBinding(SendMoneyAdapter.ContactViewHolder var, View var) {
      this.a = var;
      var.contactImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821011, "field 'contactImageView'", ImageView.class);
      var.nameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821012, "field 'nameTextView'", TextView.class);
      var.numberTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821592, "field 'numberTextView'", TextView.class);
      var.actionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821593, "field 'actionTextView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.ContactViewHolder var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.contactImageView = null;
         var.nameTextView = null;
         var.numberTextView = null;
         var.actionTextView = null;
      }
   }
}
