package co.uk.getmondo.payments.send;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.w;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.aa;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class SendMoneyAdapter extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
   private final List a = new ArrayList();
   private final Collection b = new ArrayList();
   private final List c = new ArrayList();
   private boolean d = false;
   private final List e = new ArrayList();
   private SendMoneyAdapter.c f;
   private SendMoneyAdapter.a g;
   private SendMoneyAdapter.f h;
   private boolean i = false;
   private co.uk.getmondo.common.ui.a j;

   SendMoneyAdapter(Context var) {
      this.j = co.uk.getmondo.common.ui.a.a(var);
   }

   private void a() {
      ArrayList var = new ArrayList();
      if(this.i) {
         var.add("BANK_PAYMENTS_ITEM");
         var.addAll(this.c);
      }

      var.addAll(this.a);
      var.addAll(this.b);
      if(this.d) {
         var.add("FOOTER_ITEM");
      }

      android.support.v7.f.b.b var = android.support.v7.f.b.a(new SendMoneyAdapter.d(this.e, var));
      this.e.clear();
      this.e.addAll(var);
      var.a(this);
   }

   public long a(int var) {
      Object var = this.e.get(var);
      long var;
      if(!var.equals("BANK_PAYMENTS_ITEM") && !var.equals("FOOTER_ITEM")) {
         if(var instanceof co.uk.getmondo.payments.send.a.b) {
            var = 10L;
         } else if(var instanceof co.uk.getmondo.payments.send.data.a.e) {
            var = 11L;
         } else {
            var = 12L;
         }
      } else {
         var = -1L;
      }

      return var;
   }

   // $FF: synthetic method
   public w a(ViewGroup var) {
      return this.b(var);
   }

   public void a(SendMoneyAdapter.StickyHeaderViewHolder var, int var) {
      Resources var = var.headerTextView.getResources();
      String var;
      switch((int)this.a(var)) {
      case 10:
         var = var.getString(2131362638);
         break;
      case 11:
         var = var.getString(2131362652);
         break;
      default:
         var = var.getString(2131362633);
      }

      var.a(var);
   }

   void a(SendMoneyAdapter.a var) {
      this.g = var;
   }

   void a(SendMoneyAdapter.c var) {
      this.f = var;
   }

   void a(SendMoneyAdapter.f var) {
      this.h = var;
   }

   void a(Collection var) {
      this.c.clear();
      this.c.addAll(var);
      this.a();
   }

   void a(Collection var, Collection var) {
      this.a.clear();
      this.b.clear();
      this.a.addAll(var);
      this.b.addAll(var);
      this.d = true;
      this.a();
   }

   void a(boolean var) {
      this.i = var;
      this.a();
   }

   public SendMoneyAdapter.StickyHeaderViewHolder b(ViewGroup var) {
      return new SendMoneyAdapter.StickyHeaderViewHolder(LayoutInflater.from(var.getContext()).inflate(2131034371, var, false));
   }

   public int getItemCount() {
      return this.e.size();
   }

   public int getItemViewType(int var) {
      Object var = this.e.get(var);
      byte var;
      if(var.equals("BANK_PAYMENTS_ITEM")) {
         var = 2;
      } else if(var.equals("FOOTER_ITEM")) {
         var = 1;
      } else if(var instanceof co.uk.getmondo.payments.send.data.a.e) {
         var = 3;
      } else {
         var = 0;
      }

      return var;
   }

   public void onBindViewHolder(w var, int var) {
      if(var instanceof SendMoneyAdapter.ContactViewHolder) {
         ((SendMoneyAdapter.ContactViewHolder)var).a((co.uk.getmondo.payments.send.a.a)this.e.get(var));
      }

      if(var instanceof SendMoneyAdapter.RecentPayeeViewHolder) {
         ((SendMoneyAdapter.RecentPayeeViewHolder)var).a((co.uk.getmondo.payments.send.data.a.e)this.e.get(var));
      }

   }

   public w onCreateViewHolder(ViewGroup var, int var) {
      LayoutInflater var = LayoutInflater.from(var.getContext());
      Object var;
      switch(var) {
      case 0:
         var = new SendMoneyAdapter.ContactViewHolder(var.inflate(2131034367, var, false), this.f);
         break;
      case 1:
         var = new SendMoneyAdapter.e(var.inflate(2131034368, var, false));
         break;
      case 2:
         var = new SendMoneyAdapter.b(var.inflate(2131034363, var, false), this.g);
         break;
      case 3:
         var = new SendMoneyAdapter.RecentPayeeViewHolder(var.inflate(2131034375, var, false), this.h);
         break;
      default:
         throw new RuntimeException("Unexpected viewType: " + var);
      }

      return (w)var;
   }

   class ContactViewHolder extends w {
      @BindView(2131821593)
      TextView actionTextView;
      private int b;
      @BindView(2131821011)
      ImageView contactImageView;
      @BindView(2131821012)
      TextView nameTextView;
      @BindView(2131821592)
      TextView numberTextView;

      ContactViewHolder(View var, SendMoneyAdapter.c var) {
         super(var);
         ButterKnife.bind(this, (View)var);
         this.b = (int)TypedValue.applyDimension(2, 20.0F, this.nameTextView.getResources().getDisplayMetrics());
         this.itemView.setOnClickListener(e.a(this, var));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.ContactViewHolder var, SendMoneyAdapter.c var, View var) {
         Object var = SendMoneyAdapter.this.e.get(var.getAdapterPosition());
         if(var != null && var != null && var instanceof co.uk.getmondo.payments.send.a.a) {
            var.a((co.uk.getmondo.payments.send.a.a)var, var instanceof co.uk.getmondo.payments.send.a.b);
         }

      }

      void a(co.uk.getmondo.payments.send.a.a var) {
         Resources var = this.nameTextView.getResources();
         if(var instanceof co.uk.getmondo.payments.send.a.b) {
            this.actionTextView.setText(2131362632);
            this.actionTextView.setTextColor(android.support.v4.content.a.c(this.nameTextView.getContext(), 2131689477));
            this.numberTextView.setVisibility(8);
         } else {
            this.actionTextView.setText(2131362631);
            this.actionTextView.setTextColor(android.support.v4.content.a.c(this.nameTextView.getContext(), 2131689649));
            this.numberTextView.setVisibility(0);
            if(((co.uk.getmondo.payments.send.a.d)var).f().size() > 1) {
               this.numberTextView.setText(var.getString(2131362650, new Object[]{var.c(), Integer.valueOf(((co.uk.getmondo.payments.send.a.d)var).f().size() - 1)}));
            } else {
               this.numberTextView.setText(var.c());
            }
         }

         this.nameTextView.setText(var.b());
         if(var.d()) {
            com.bumptech.glide.g.b(this.contactImageView.getContext()).a(Uri.parse(var.e())).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.contactImageView)));
         } else if(var.b() != null) {
            this.contactImageView.setImageDrawable(SendMoneyAdapter.this.j.a(var.b()).a(this.b));
         }

      }
   }

   class RecentPayeeViewHolder extends w {
      private int b;
      @BindView(2131821608)
      TextView payeeIdentifierView;
      @BindView(2131821606)
      ImageView payeeImageView;
      @BindView(2131821607)
      TextView payeeNameView;

      RecentPayeeViewHolder(View var, SendMoneyAdapter.f var) {
         super(var);
         ButterKnife.bind(this, (View)var);
         this.b = (int)TypedValue.applyDimension(2, 20.0F, var.getResources().getDisplayMetrics());
         var.setOnClickListener(f.a(this, var));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.RecentPayeeViewHolder var, SendMoneyAdapter.f var, View var) {
         int var = var.getAdapterPosition();
         if(var != -1) {
            var.a((co.uk.getmondo.payments.send.data.a.e)SendMoneyAdapter.this.e.get(var));
         }

      }

      void a(co.uk.getmondo.payments.send.data.a.e var) {
         String var = var.b();
         String var;
         String var;
         if(var instanceof co.uk.getmondo.payments.send.data.a.b) {
            co.uk.getmondo.payments.send.data.a.b var = (co.uk.getmondo.payments.send.data.a.b)var;
            var = String.format("%s  •  %s", new Object[]{var.a(), var.e()});
            var = null;
         } else {
            if(!(var instanceof aa)) {
               throw new RuntimeException("Unexpected payee type");
            }

            aa var = (aa)var;
            var = var.d();
            var = var.g();
         }

         if(var == null) {
            throw new RuntimeException("Payee is missing information");
         } else {
            if(TextUtils.isEmpty(var)) {
               this.payeeIdentifierView.setVisibility(8);
            } else {
               this.payeeIdentifierView.setText(var);
               var = var;
            }

            if(var != null && !var.isEmpty()) {
               com.bumptech.glide.g.b(this.payeeImageView.getContext()).a(Uri.parse(var)).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.payeeImageView)));
            } else {
               this.payeeImageView.setImageDrawable(SendMoneyAdapter.this.j.a(var).a(this.b));
            }

            this.payeeNameView.setText(var);
         }
      }
   }

   static class StickyHeaderViewHolder extends w {
      @BindView(2131821599)
      TextView headerTextView;

      StickyHeaderViewHolder(View var) {
         super(var);
         ButterKnife.bind(this, (View)var);
      }

      void a(String var) {
         this.headerTextView.setText(var);
      }
   }

   @FunctionalInterface
   interface a {
      void a();
   }

   private class b extends w {
      b(View var, SendMoneyAdapter.a var) {
         super(var);
         var.setOnClickListener(d.a(var));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.a var, View var) {
         var.a();
      }
   }

   @FunctionalInterface
   interface c {
      void a(co.uk.getmondo.payments.send.a.a var, boolean var);
   }

   private static class d extends android.support.v7.f.b.a {
      private final List a;
      private final List b;

      private d(List var, List var) {
         this.a = var;
         this.b = var;
      }

      // $FF: synthetic method
      d(List var, List var, Object var) {
         this(var, var);
      }

      public int a() {
         return this.a.size();
      }

      public boolean a(int var, int var) {
         Object var = this.a.get(var);
         Object var = this.b.get(var);
         boolean var;
         if(var instanceof co.uk.getmondo.payments.send.a.a && var instanceof co.uk.getmondo.payments.send.a.a) {
            if(((co.uk.getmondo.payments.send.a.a)var).a() == ((co.uk.getmondo.payments.send.a.a)var).a()) {
               var = true;
            } else {
               var = false;
            }
         } else {
            var = var.equals(var);
         }

         return var;
      }

      public int b() {
         return this.b.size();
      }

      public boolean b(int var, int var) {
         return this.a.get(var).equals(this.b.get(var));
      }
   }

   private class e extends w {
      e(View var) {
         super(var);
      }
   }

   @FunctionalInterface
   interface f {
      void a(co.uk.getmondo.payments.send.data.a.e var);
   }
}
