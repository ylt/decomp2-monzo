package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final SendMoneyAdapter.ContactViewHolder a;
   private final SendMoneyAdapter.c b;

   private e(SendMoneyAdapter.ContactViewHolder var, SendMoneyAdapter.c var) {
      this.a = var;
      this.b = var;
   }

   public static OnClickListener a(SendMoneyAdapter.ContactViewHolder var, SendMoneyAdapter.c var) {
      return new e(var, var);
   }

   public void onClick(View var) {
      SendMoneyAdapter.ContactViewHolder.a(this.a, this.b, var);
   }
}
