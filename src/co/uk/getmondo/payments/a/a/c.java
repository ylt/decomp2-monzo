package co.uk.getmondo.payments.a.a;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B#\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u000e\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/payments/data/model/PayeeValidationError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "showError", "Lkotlin/Function1;", "Lco/uk/getmondo/payments/send/bank/payee/PayeeValidationErrorView;", "", "(Ljava/lang/String;ILjava/lang/String;Lkotlin/jvm/functions/Function1;)V", "getPrefix", "()Ljava/lang/String;", "getShowError", "()Lkotlin/jvm/functions/Function1;", "displayError", "view", "BAD_ACCOUNT_NUMBER", "BAD_SORT_CODE", "MODULUS_CHECK_FAILED", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum c implements co.uk.getmondo.common.e.f {
   a,
   b,
   c;

   private final String e;
   private final kotlin.d.a.b f;

   static {
      c var = new c("BAD_ACCOUNT_NUMBER", 0, "bad_request.bad_param.account_number", (kotlin.d.a.b)null.a);
      a = var;
      c var = new c("BAD_SORT_CODE", 1, "bad_request.bad_param.sort_code", (kotlin.d.a.b)null.a);
      b = var;
      c var = new c("MODULUS_CHECK_FAILED", 2, "bad_request.bad_param.modulus_check_failed", (kotlin.d.a.b)null.a);
      c = var;
   }

   protected c(String var, kotlin.d.a.b var) {
      l.b(var, "prefix");
      l.b(var, "showError");
      super(var, var);
      this.e = var;
      this.f = var;
   }

   public String a() {
      return this.e;
   }

   public final void a(co.uk.getmondo.payments.send.bank.payee.a var) {
      l.b(var, "view");
      this.f.a(var);
   }
}
