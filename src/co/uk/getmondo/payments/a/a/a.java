package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import io.realm.bc;
import io.realm.n;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 A2\u00020\u00012\u00020\u0002:\u0001ABa\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\b\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0006\u0010\n\u001a\u00020\u0004\u0012\u0006\u0010\u000b\u001a\u00020\u0004\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0002\u0010\u0011B\u000f\b\u0016\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014B\u0005¢\u0006\u0002\u0010\u0015J\b\u00107\u001a\u000208H\u0016J\u0013\u00109\u001a\u00020\r2\b\u0010:\u001a\u0004\u0018\u00010;H\u0096\u0002J\b\u0010<\u001a\u000208H\u0016J\u0018\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u00132\u0006\u0010@\u001a\u000208H\u0016R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\f\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR$\u0010\u000e\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\u000f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u0014\u0010%\u001a\u00020\u00048VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b&\u0010\u0019R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010\u0019\"\u0004\b(\u0010\u001bR(\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010 \u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b)\u0010\"\"\u0004\b*\u0010$R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0019\"\u0004\b,\u0010\u001bR\u001a\u0010\b\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0019\"\u0004\b.\u0010\u001bR\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\u0019\"\u0004\b0\u0010\u001bR\u001a\u0010\u000b\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0019\"\u0004\b2\u0010\u001bR\u001a\u0010\n\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u0019\"\u0004\b4\u0010\u001bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\u0019\"\u0004\b6\u0010\u001b¨\u0006B"},
   d2 = {"Lco/uk/getmondo/payments/data/model/DirectDebit;", "Lio/realm/RealmObject;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "id", "", "accountId", "payerSortCode", "payerAccountNumber", "payerName", "serviceUserNumber", "serviceUserName", "reference", "active", "", "created", "Lorg/threeten/bp/LocalDateTime;", "lastCollected", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "_created", "_lastCollected", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "getActive", "()Z", "setActive", "(Z)V", "value", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "setCreated", "(Lorg/threeten/bp/LocalDateTime;)V", "displayName", "getDisplayName", "getId", "setId", "getLastCollected", "setLastCollected", "getPayerAccountNumber", "setPayerAccountNumber", "getPayerName", "setPayerName", "getPayerSortCode", "setPayerSortCode", "getReference", "setReference", "getServiceUserName", "setServiceUserName", "getServiceUserNumber", "setServiceUserNumber", "describeContents", "", "equals", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class a extends bc implements f, n {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public a a(Parcel var) {
         l.b(var, "source");
         return new a(var);
      }

      public a[] a(int var) {
         return new a[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final a.a a = new a.a((i)null);
   private String b;
   private String c;
   private String d;
   private String e;
   private String f;
   private String g;
   private String h;
   private String i;
   private boolean j;
   private String k;
   private String l;

   public a() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
      this.d("");
      this.e("");
      this.f("");
      this.g("");
      this.h("");
      this.i("");
   }

   public a(Parcel var) {
      boolean var = true;
      l.b(var, "source");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      if(var.readInt() != 1) {
         var = false;
      }

      Serializable var = var.readSerializable();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.LocalDateTime");
      } else {
         this(var, var, var, var, var, var, var, var, var, (LocalDateTime)var, (LocalDateTime)var.readSerializable());
         if(this instanceof io.realm.internal.l) {
            ((io.realm.internal.l)this).u_();
         }

      }
   }

   public a(String var, String var, String var, String var, String var, String var, String var, String var, boolean var, LocalDateTime var, LocalDateTime var) {
      l.b(var, "id");
      l.b(var, "accountId");
      l.b(var, "payerSortCode");
      l.b(var, "payerAccountNumber");
      l.b(var, "payerName");
      l.b(var, "serviceUserNumber");
      l.b(var, "serviceUserName");
      l.b(var, "reference");
      l.b(var, "created");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
      this.g(var);
      this.h(var);
      this.a(var);
      this.a(var);
      this.b(var);
   }

   public final String a() {
      return this.e();
   }

   public void a(String var) {
      this.b = var;
   }

   public final void a(LocalDateTime var) {
      l.b(var, "value");
      String var = var.toString();
      l.a(var, "value.toString()");
      this.i(var);
   }

   public void a(boolean var) {
      this.j = var;
   }

   public final LocalDateTime b() {
      LocalDateTime var = LocalDateTime.a((CharSequence)this.n());
      l.a(var, "LocalDateTime.parse(_created)");
      return var;
   }

   public void b(String var) {
      this.c = var;
   }

   public final void b(LocalDateTime var) {
      String var;
      if(var != null) {
         var = var.toString();
      } else {
         var = null;
      }

      this.j(var);
   }

   public final LocalDateTime c() {
      LocalDateTime var;
      if(this.o() != null) {
         var = LocalDateTime.a((CharSequence)this.o());
      } else {
         var = null;
      }

      return var;
   }

   public void c(String var) {
      this.d = var;
   }

   public String d() {
      return this.t_();
   }

   public void d(String var) {
      this.e = var;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.b;
   }

   public void e(String var) {
      this.f = var;
   }

   public boolean equals(Object var) {
      boolean var;
      if((a)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.DirectDebit");
            }

            a var = (a)var;
            if(l.a(this.e(), ((a)var).e()) ^ true) {
               var = false;
            } else if(l.a(this.f(), ((a)var).f()) ^ true) {
               var = false;
            } else if(l.a(this.g(), ((a)var).g()) ^ true) {
               var = false;
            } else if(l.a(this.h(), ((a)var).h()) ^ true) {
               var = false;
            } else if(l.a(this.i(), ((a)var).i()) ^ true) {
               var = false;
            } else if(l.a(this.j(), ((a)var).j()) ^ true) {
               var = false;
            } else if(l.a(this.t_(), ((a)var).t_()) ^ true) {
               var = false;
            } else if(l.a(this.l(), ((a)var).l()) ^ true) {
               var = false;
            } else if(this.m() != ((a)var).m()) {
               var = false;
            } else if(l.a(this.n(), ((a)var).n()) ^ true) {
               var = false;
            } else if(l.a(this.o(), ((a)var).o()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public String f() {
      return this.c;
   }

   public void f(String var) {
      this.g = var;
   }

   public String g() {
      return this.d;
   }

   public void g(String var) {
      this.h = var;
   }

   public String h() {
      return this.e;
   }

   public void h(String var) {
      this.i = var;
   }

   public int hashCode() {
      int var = this.e().hashCode();
      int var = this.f().hashCode();
      int var = this.g().hashCode();
      int var = this.h().hashCode();
      int var = this.i().hashCode();
      int var = this.j().hashCode();
      int var = this.t_().hashCode();
      int var = this.l().hashCode();
      int var = Boolean.valueOf(this.m()).hashCode();
      int var = this.n().hashCode();
      String var = this.o();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      return var + (((((((((var * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31;
   }

   public String i() {
      return this.f;
   }

   public void i(String var) {
      this.k = var;
   }

   public String j() {
      return this.g;
   }

   public void j(String var) {
      this.l = var;
   }

   public String l() {
      return this.i;
   }

   public boolean m() {
      return this.j;
   }

   public String n() {
      return this.k;
   }

   public String o() {
      return this.l;
   }

   public String t_() {
      return this.h;
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeString(this.e());
      var.writeString(this.f());
      var.writeString(this.g());
      var.writeString(this.h());
      var.writeString(this.i());
      var.writeString(this.j());
      var.writeString(this.t_());
      var.writeString(this.l());
      byte var;
      if(this.m()) {
         var = 1;
      } else {
         var = 0;
      }

      var.writeInt(var);
      var.writeSerializable((Serializable)this.b());
      var.writeSerializable((Serializable)this.c());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/DirectDebit$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
