package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.ar;
import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u001d\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 ;2\u00020\u00012\u00020\u0002:\u0001;B7\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rB\u000f\b\u0016\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010B\u0005¢\u0006\u0002\u0010\u0011J\b\u00100\u001a\u000201H\u0016J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u000105H\u0096\u0002J\b\u00106\u001a\u000201H\u0016J\u0018\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u000f2\u0006\u0010:\u001a\u000201H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR$\u0010\t\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\"\u0010\u001aR\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001a\"\u0004\b$\u0010\u001cR$\u0010\u000b\u001a\u00020\f2\u0006\u0010%\u001a\u00020\f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u001a\"\u0004\b/\u0010\u001c¨\u0006<"},
   d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSeries;", "Lio/realm/RealmObject;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "id", "", "accountId", "scheme", "schemaData", "Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "amount", "Lco/uk/getmondo/model/Amount;", "paymentSchedule", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/data/model/FpsSchemaData;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "_amount", "", "_currency", "_endDate", "_intervalType", "_nextIterationDate", "_startDate", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "getAmount", "()Lco/uk/getmondo/model/Amount;", "setAmount", "(Lco/uk/getmondo/model/Amount;)V", "displayName", "getDisplayName", "getId", "setId", "value", "getPaymentSchedule", "()Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "setPaymentSchedule", "(Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V", "getSchemaData", "()Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "setSchemaData", "(Lco/uk/getmondo/payments/data/model/FpsSchemaData;)V", "getScheme", "setScheme", "describeContents", "", "equals", "", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class e extends bc implements f, ar {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public e a(Parcel var) {
         l.b(var, "source");
         return new e(var);
      }

      public e[] a(int var) {
         return new e[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final e.a a = new e.a((i)null);
   private String b;
   private String c;
   private String d;
   private b e;
   private long f;
   private String g;
   private String h;
   private String i;
   private String j;
   private String k;

   public e() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
      this.a(new b((String)null, (String)null, (String)null, (String)null, (String)null, 31, (i)null));
      this.d("");
      this.e("");
      this.h("");
   }

   public e(Parcel var) {
      l.b(var, "source");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      Parcelable var = var.readParcelable(b.class.getClassLoader());
      l.a(var, "source.readParcelable(Fp…::class.java.classLoader)");
      b var = (b)var;
      Parcelable var = var.readParcelable(co.uk.getmondo.d.c.class.getClassLoader());
      l.a(var, "source.readParcelable(Am…::class.java.classLoader)");
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)var;
      Parcelable var = var.readParcelable(d.class.getClassLoader());
      l.a(var, "source.readParcelable(Pa…::class.java.classLoader)");
      this(var, var, var, var, var, (d)var);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public e(String var, String var, String var, b var, co.uk.getmondo.d.c var, d var) {
      l.b(var, "id");
      l.b(var, "accountId");
      l.b(var, "scheme");
      l.b(var, "schemaData");
      l.b(var, "amount");
      l.b(var, "paymentSchedule");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var);
      this.b(var);
      this.c(var);
      this.a(var);
      this.a(var);
      this.a(var);
   }

   public final String a() {
      return this.f();
   }

   public void a(long var) {
      this.f = var;
   }

   public final void a(co.uk.getmondo.d.c var) {
      l.b(var, "amount");
      this.a(var.k());
      String var = var.l().b();
      l.a(var, "amount.currency.currencyCode");
      this.d(var);
   }

   public void a(b var) {
      this.e = var;
   }

   public final void a(d var) {
      Object var = null;
      l.b(var, "value");
      String var = var.b().toString();
      l.a(var, "value.startDate.toString()");
      this.e(var);
      LocalDate var = var.d();
      if(var != null) {
         var = var.toString();
      } else {
         var = null;
      }

      this.g(var);
      this.h(var.c().a());
      var = var.e();
      String var = (String)var;
      if(var != null) {
         var = var.toString();
      }

      this.f(var);
   }

   public void a(String var) {
      this.b = var;
   }

   public final b b() {
      return this.i();
   }

   public void b(String var) {
      this.c = var;
   }

   public final co.uk.getmondo.d.c c() {
      return new co.uk.getmondo.d.c(this.j(), this.k());
   }

   public void c(String var) {
      this.d = var;
   }

   public final String d() {
      return this.i().d();
   }

   public void d(String var) {
      this.g = var;
   }

   public int describeContents() {
      return 0;
   }

   public final d e() {
      LocalDate var = null;
      LocalDate var = LocalDate.a((CharSequence)this.l());
      l.a(var, "LocalDate.parse(_startDate)");
      d.c var = d.c.f.a(this.o());
      LocalDate var;
      if(this.n() != null) {
         var = LocalDate.a((CharSequence)this.n());
      } else {
         var = null;
      }

      if(this.m() != null) {
         var = LocalDate.a((CharSequence)this.m());
      }

      return new d(var, var, var, var);
   }

   public void e(String var) {
      this.h = var;
   }

   public boolean equals(Object var) {
      boolean var;
      if((e)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.PaymentSeries");
            }

            e var = (e)var;
            if(l.a(this.f(), ((e)var).f()) ^ true) {
               var = false;
            } else if(l.a(this.g(), ((e)var).g()) ^ true) {
               var = false;
            } else if(l.a(this.h(), ((e)var).h()) ^ true) {
               var = false;
            } else if(l.a(this.i(), ((e)var).i()) ^ true) {
               var = false;
            } else if(this.j() != ((e)var).j()) {
               var = false;
            } else if(l.a(this.k(), ((e)var).k()) ^ true) {
               var = false;
            } else if(l.a(this.l(), ((e)var).l()) ^ true) {
               var = false;
            } else if(l.a(this.m(), ((e)var).m()) ^ true) {
               var = false;
            } else if(l.a(this.n(), ((e)var).n()) ^ true) {
               var = false;
            } else if(l.a(this.o(), ((e)var).o()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public String f() {
      return this.b;
   }

   public void f(String var) {
      this.i = var;
   }

   public String g() {
      return this.c;
   }

   public void g(String var) {
      this.j = var;
   }

   public String h() {
      return this.d;
   }

   public void h(String var) {
      this.k = var;
   }

   public int hashCode() {
      int var = 0;
      int var = this.f().hashCode();
      int var = this.g().hashCode();
      int var = this.h().hashCode();
      int var = this.i().hashCode();
      int var = Long.valueOf(this.j()).hashCode();
      int var = this.k().hashCode();
      int var = this.l().hashCode();
      String var = this.m();
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.n();
      if(var != null) {
         var = var.hashCode();
      }

      return ((var + ((((((var * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31 + var) * 31) * 31 + var) * 31 + this.o().hashCode();
   }

   public b i() {
      return this.e;
   }

   public long j() {
      return this.f;
   }

   public String k() {
      return this.g;
   }

   public String l() {
      return this.h;
   }

   public String m() {
      return this.i;
   }

   public String n() {
      return this.j;
   }

   public String o() {
      return this.k;
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeString(this.f());
      var.writeString(this.g());
      var.writeString(this.h());
      var.writeParcelable((Parcelable)this.i(), var);
      var.writeParcelable((Parcelable)this.c(), var);
      var.writeParcelable((Parcelable)this.e(), var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSeries$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
