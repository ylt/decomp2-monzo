package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.bc;
import io.realm.x;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B7\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0007¢\u0006\u0002\u0010\fJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0096\u0002J\b\u0010\u001f\u001a\u00020\u001aH\u0016J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u001aH\u0016R\u001a\u0010\t\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u000b\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0010R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000e\"\u0004\b\u0014\u0010\u0010R\u001a\u0010\b\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000e\"\u0004\b\u0016\u0010\u0010R\u001a\u0010\n\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u000e\"\u0004\b\u0018\u0010\u0010¨\u0006%"},
   d2 = {"Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "Lio/realm/RealmObject;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "reference", "accountNumber", "sortCode", "customerName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "getCustomerName", "setCustomerName", "getId", "setId", "getReference", "setReference", "getSortCode", "setSortCode", "describeContents", "", "equals", "", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class b extends bc implements Parcelable, x {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public b a(Parcel var) {
         l.b(var, "source");
         return new b(var);
      }

      public b[] a(int var) {
         return new b[var];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var) {
         return this.a(var);
      }

      // $FF: synthetic method
      public Object[] newArray(int var) {
         return (Object[])this.a(var);
      }
   });
   public static final b.a a = new b.a((i)null);
   private String b;
   private String c;
   private String d;
   private String e;
   private String f;

   public b() {
      this((String)null, (String)null, (String)null, (String)null, (String)null, 31, (i)null);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(Parcel var) {
      l.b(var, "source");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      String var = var.readString();
      l.a(var, "source.readString()");
      this(var, var, var, var, var);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(String var, String var, String var, String var, String var) {
      l.b(var, "id");
      l.b(var, "reference");
      l.b(var, "accountNumber");
      l.b(var, "sortCode");
      l.b(var, "customerName");
      super();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.b(var);
      this.c(var);
      this.d(var);
      this.e(var);
      this.f(var);
   }

   // $FF: synthetic method
   public b(String var, String var, String var, String var, String var, int var, i var) {
      if((var & 1) != 0) {
         var = "";
      }

      if((var & 2) != 0) {
         var = "";
      }

      if((var & 4) != 0) {
         var = "";
      }

      if((var & 8) != 0) {
         var = "";
      }

      if((var & 16) != 0) {
         var = "";
      }

      this(var, var, var, var, var);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public final String a() {
      return this.f();
   }

   public final void a(String var) {
      l.b(var, "<set-?>");
      this.b(var);
   }

   public final String b() {
      return this.g();
   }

   public void b(String var) {
      this.b = var;
   }

   public final String c() {
      return this.h();
   }

   public void c(String var) {
      this.c = var;
   }

   public final String d() {
      return this.i();
   }

   public void d(String var) {
      this.d = var;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.b;
   }

   public void e(String var) {
      this.e = var;
   }

   public boolean equals(Object var) {
      boolean var;
      if((b)this == var) {
         var = true;
      } else {
         Class var;
         if(var != null) {
            var = var.getClass();
         } else {
            var = null;
         }

         if(l.a(var, this.getClass()) ^ true) {
            var = false;
         } else {
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.FpsSchemaData");
            }

            b var = (b)var;
            if(l.a(this.e(), ((b)var).e()) ^ true) {
               var = false;
            } else if(l.a(this.f(), ((b)var).f()) ^ true) {
               var = false;
            } else if(l.a(this.g(), ((b)var).g()) ^ true) {
               var = false;
            } else if(l.a(this.h(), ((b)var).h()) ^ true) {
               var = false;
            } else if(l.a(this.i(), ((b)var).i()) ^ true) {
               var = false;
            } else {
               var = true;
            }
         }
      }

      return var;
   }

   public String f() {
      return this.c;
   }

   public void f(String var) {
      this.f = var;
   }

   public String g() {
      return this.d;
   }

   public String h() {
      return this.e;
   }

   public int hashCode() {
      return (((this.e().hashCode() * 31 + this.f().hashCode()) * 31 + this.g().hashCode()) * 31 + this.h().hashCode()) * 31 + this.i().hashCode();
   }

   public String i() {
      return this.f;
   }

   public void writeToParcel(Parcel var, int var) {
      l.b(var, "dest");
      var.writeString(this.e());
      var.writeString(this.f());
      var.writeString(this.g());
      var.writeString(this.h());
      var.writeString(this.i());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/FpsSchemaData$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
