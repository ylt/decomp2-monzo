package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.model.payments.ApiPaymentSeries;
import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/payments/data/PaymentSeriesMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e implements co.uk.getmondo.d.a.j {
   public static final e a;

   static {
      new e();
   }

   private e() {
      a = (e)this;
   }

   public co.uk.getmondo.payments.a.a.e a(ApiPaymentSeries var) {
      l.b(var, "apiValue");
      co.uk.getmondo.payments.a.a.b var = d.a.a(var.f());
      var.a(var.a());
      LocalDate var = var.g();
      co.uk.getmondo.payments.a.a.d.c var = co.uk.getmondo.payments.a.a.d.c.f.a(var.h());
      LocalDate var = var.i();
      co.uk.getmondo.payments.a.a.d var = new co.uk.getmondo.payments.a.a.d(var, var, var.j(), var);
      return new co.uk.getmondo.payments.a.a.e(var.a(), var.b(), var.e(), var, new co.uk.getmondo.d.c(var.c(), var.d()), var);
   }
}
