package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.PaymentsApi;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new j(var, var, var, var);
   }

   public i a() {
      return new i((f)this.b.b(), (b)this.c.b(), (PaymentsApi)this.d.b(), (co.uk.getmondo.common.accounts.d)this.e.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
