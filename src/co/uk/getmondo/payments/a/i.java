package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.PaymentsApi;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.v;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 '2\u00020\u0001:\u0001'B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fJ\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\r0\fJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\r0\fJ&\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J&\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u001e\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0018\u0010\u001e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 0\u001f0\fJ\u001e\u0010!\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0006\u0010$\u001a\u00020\u0014J\u0006\u0010%\u001a\u00020\u0014J\u0006\u0010&\u001a\u00020\u0014R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006("},
   d2 = {"Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "", "paymentSeriesStorage", "Lco/uk/getmondo/payments/data/PaymentSeriesStorage;", "directDebitsStorage", "Lco/uk/getmondo/payments/data/DirectDebitsStorage;", "paymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Lco/uk/getmondo/payments/data/PaymentSeriesStorage;Lco/uk/getmondo/payments/data/DirectDebitsStorage;Lco/uk/getmondo/api/PaymentsApi;Lco/uk/getmondo/common/accounts/AccountService;)V", "allDirectDebits", "Lio/reactivex/Observable;", "", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "allPaymentSeries", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "allRecurringPayments", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "cancelDirectDebit", "Lio/reactivex/Completable;", "directDebitId", "", "accountId", "pin", "idempotencyKey", "cancelPaymentSeries", "paymentSeriesId", "cancelRecurringPayment", "recurringPayment", "counts", "Lkotlin/Pair;", "", "scheduleBankPayment", "bankPayment", "Lco/uk/getmondo/payments/send/data/model/BankPayment;", "syncAll", "syncDirectDebits", "syncPaymentSeries", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i {
   public static final i.a a = new i.a((kotlin.d.b.i)null);
   private final f b;
   private final b c;
   private final PaymentsApi d;
   private final co.uk.getmondo.common.accounts.d e;

   public i(f var, b var, PaymentsApi var, co.uk.getmondo.common.accounts.d var) {
      l.b(var, "paymentSeriesStorage");
      l.b(var, "directDebitsStorage");
      l.b(var, "paymentsApi");
      l.b(var, "accountService");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public final io.reactivex.b a() {
      io.reactivex.b var = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var) {
            l.b(var, "accountId");
            return i.this.d.scheduledPaymentsSeries(var);
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(List var) {
            l.b(var, "it");
            return i.this.b.a(var);
         }
      }));
      l.a(var, "accountService.accountId…riesStorage.saveAll(it) }");
      return var;
   }

   public final io.reactivex.b a(final co.uk.getmondo.payments.a.a.f var, final String var, final String var) {
      l.b(var, "recurringPayment");
      l.b(var, "pin");
      l.b(var, "idempotencyKey");
      io.reactivex.b var = this.e.d().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.d a(String varx) {
            l.b(varx, "accountId");
            co.uk.getmondo.payments.a.a.f varx = var;
            io.reactivex.d varx;
            if(varx instanceof co.uk.getmondo.payments.a.a.a) {
               varx = (io.reactivex.d)i.this.b(((co.uk.getmondo.payments.a.a.a)var).a(), varx, var, var);
            } else if(varx instanceof co.uk.getmondo.payments.a.a.e) {
               varx = (io.reactivex.d)i.this.a(((co.uk.getmondo.payments.a.a.e)var).a(), varx, var, var);
            } else {
               varx = (io.reactivex.d)io.reactivex.b.a((Throwable)(new IllegalArgumentException("Unsupported payment type " + var.getClass())));
            }

            return varx;
         }
      }));
      l.a(var, "accountService.accountId…      }\n                }");
      return var;
   }

   public final io.reactivex.b a(final co.uk.getmondo.payments.send.data.a.c var, final String var, final String var) {
      l.b(var, "bankPayment");
      l.b(var, "pin");
      l.b(var, "idempotencyKey");
      io.reactivex.b var = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String varx) {
            l.b(varx, "accountId");
            if(var.e() == null) {
               v.a((Throwable)(new IllegalStateException("Payment must contain scheduling data " + var)));
            }

            PaymentsApi var = i.this.d;
            long varx = var.a().k();
            String var = var.a().l().b();
            l.a(var, "bankPayment.amount.currency.currencyCode");
            String var = var.c().e();
            String var = var.c().d();
            String var = var.c().b();
            String var = var;
            String var = var;
            String var = var.b();
            co.uk.getmondo.payments.a.a.d var = var.e();
            if(var == null) {
               l.a();
            }

            return var.scheduleBankPayment(varx, varx, var, var, var, var, var, "pin", var, var, var.b(), var.e().c().a(), var.e().d());
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.e var) {
            f var = i.this.b;
            l.a(var, "it");
            var.a(var);
         }
      })).c();
      l.a(var, "accountService.accountId…         .toCompletable()");
      return var;
   }

   public final io.reactivex.b a(String var, String var, String var, String var) {
      l.b(var, "paymentSeriesId");
      l.b(var, "accountId");
      l.b(var, "pin");
      l.b(var, "idempotencyKey");
      io.reactivex.b var = this.d.cancelScheduledPaymentSeries(var, var, "pin", var, var).b((io.reactivex.d)this.b.a(var));
      l.a(var, "paymentsApi\n            ….delete(paymentSeriesId))");
      return var;
   }

   public final io.reactivex.b b() {
      io.reactivex.b var = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var) {
            l.b(var, "accountId");
            return i.this.d.bacsDirectDebits(var);
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(List var) {
            l.b(var, "it");
            return i.this.c.a(var);
         }
      }));
      l.a(var, "accountService.accountId…bitsStorage.saveAll(it) }");
      return var;
   }

   public final io.reactivex.b b(String var, String var, String var, String var) {
      l.b(var, "directDebitId");
      l.b(var, "accountId");
      l.b(var, "pin");
      l.b(var, "idempotencyKey");
      io.reactivex.b var = this.d.cancelBacsDirectDebit(var, var, "pin", var, var).b((io.reactivex.d)this.c.a(var));
      l.a(var, "paymentsApi.cancelBacsDi…ge.delete(directDebitId))");
      return var;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var = io.reactivex.b.a((Iterable)m.b(new io.reactivex.b[]{this.a(), this.b()}));
      l.a(var, "Completable.mergeDelayEr…s(), syncDirectDebits()))");
      return var;
   }

   public final n d() {
      n var = this.b.a().map((io.reactivex.c.h)null.a);
      l.a(var, "paymentSeriesStorage.all…      .map { it.results }");
      return var;
   }

   public final n e() {
      n var = this.c.a().map((io.reactivex.c.h)null.a);
      l.a(var, "directDebitsStorage.allD…      .map { it.results }");
      return var;
   }

   public final n f() {
      n var = n.combineLatest((r)this.d(), (r)this.e(), (io.reactivex.c.c)null.a);
      l.a(var, "Observable.combineLatest…hat makes sense\n        )");
      return var;
   }

   public final n g() {
      return co.uk.getmondo.common.j.f.a(this.c.b(), this.b.b());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/payments/data/RecurringPaymentsManager$Companion;", "", "()V", "CHALLENGE_TYPE_PIN", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
