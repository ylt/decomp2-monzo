package co.uk.getmondo.payments.a;

import io.reactivex.n;
import io.realm.av;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0014\u0010\r\u001a\u00020\n2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/payments/data/DirectDebitsStorage;", "", "()V", "allDirectDebits", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/data/QueryResults;", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "count", "", "delete", "Lio/reactivex/Completable;", "directDebitId", "", "saveAll", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   public final io.reactivex.b a(final String var) {
      l.b(var, "directDebitId");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            varx.a(co.uk.getmondo.payments.a.a.a.class).a("id", var).f().b();
         }
      }));
   }

   public final io.reactivex.b a(final List var) {
      l.b(var, "allDirectDebits");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av varx) {
            if(var.isEmpty()) {
               varx.a(co.uk.getmondo.payments.a.a.a.class).f().b();
            } else {
               varx.a((Collection)var);
               Iterable var = (Iterable)var;
               Collection var = (Collection)(new ArrayList(m.a(var, 10)));
               Iterator var = var.iterator();

               while(var.hasNext()) {
                  var.add(((co.uk.getmondo.payments.a.a.a)var.next()).a());
               }

               var = (Collection)((List)var);
               Object[] var = var.toArray(new String[var.size()]);
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               String[] var = (String[])var;
               varx.a(co.uk.getmondo.payments.a.a.a.class).d().a("id", var).f().b();
            }

         }
      }));
   }

   public final n a() {
      n var = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).map((io.reactivex.c.h)null.a);
      l.a(var, "RxRealm.asObservable { r… .map { it.queryResults }");
      return var;
   }

   public final n b() {
      n var = this.a().map((io.reactivex.c.h)null.a).distinctUntilChanged();
      l.a(var, "allDirectDebits()\n      …  .distinctUntilChanged()");
      return var;
   }
}
