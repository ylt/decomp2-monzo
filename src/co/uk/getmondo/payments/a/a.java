package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.model.payments.ApiDirectDebit;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/payments/data/DirectDebitMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/payments/ApiDirectDebit;", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements co.uk.getmondo.d.a.j {
   public static final a a;

   static {
      new a();
   }

   private a() {
      a = (a)this;
   }

   public co.uk.getmondo.payments.a.a.a a(ApiDirectDebit var) {
      l.b(var, "apiValue");
      String var = var.a();
      String var = var.b();
      String var = var.c();
      String var = var.d();
      String var = var.e();
      String var = var.g();
      return new co.uk.getmondo.payments.a.a.a(var, var, var, var, var, var.f(), var, var.h(), var.i(), var.j(), var.k());
   }
}
