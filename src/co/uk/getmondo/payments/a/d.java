package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.model.payments.ApiPaymentSeries;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/payments/data/FpsSchemaDataMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements co.uk.getmondo.d.a.j {
   public static final d a;

   static {
      new d();
   }

   private d() {
      a = (d)this;
   }

   public co.uk.getmondo.payments.a.a.b a(ApiPaymentSeries.FpsSchemeData var) {
      l.b(var, "apiValue");
      return new co.uk.getmondo.payments.a.a.b((String)null, var.a(), var.b(), var.c(), var.d(), 1, (kotlin.d.b.i)null);
   }
}
