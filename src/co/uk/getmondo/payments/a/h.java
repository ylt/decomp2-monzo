package co.uk.getmondo.payments.a;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/payments/data/PinEntryError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getPrefix", "()Ljava/lang/String;", "BLOCKED", "PIN_INCORRECT", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum h implements co.uk.getmondo.common.e.f {
   a,
   b;

   private final String d;

   static {
      h var = new h("BLOCKED", 0, "forbidden");
      a = var;
      h var = new h("PIN_INCORRECT", 1, "bad_request.bad_param.pin");
      b = var;
   }

   protected h(String var) {
      l.b(var, "prefix");
      super(var, var);
      this.d = var;
   }

   public String a() {
      return this.d;
   }
}
