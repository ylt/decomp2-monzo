package co.uk.getmondo.signup.rejected;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import co.uk.getmondo.c;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.activities.b;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/signup/rejected/SignupRejectedActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "()V", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "getAnalyticsService", "()Lco/uk/getmondo/common/AnalyticsService;", "setAnalyticsService", "(Lco/uk/getmondo/common/AnalyticsService;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignupRejectedActivity extends b {
   public static final SignupRejectedActivity.a b = new SignupRejectedActivity.a((i)null);
   public co.uk.getmondo.common.a a;
   private HashMap c;

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034209);
      this.l().a(this);
      co.uk.getmondo.common.a var = this.a;
      if(var == null) {
         l.b("analyticsService");
      }

      var.a(Impression.Companion.aW());
      ((Button)this.a(c.a.closeMonzoButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            SignupRejectedActivity.this.setResult(0);
            SignupRejectedActivity.this.finish();
         }
      }));
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/rejected/SignupRejectedActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, SignupRejectedActivity.class);
      }
   }
}
