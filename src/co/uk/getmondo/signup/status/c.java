package co.uk.getmondo.signup.status;

import co.uk.getmondo.api.MigrationApi;
import co.uk.getmondo.api.SignupApi;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new c(var, var, var, var, var);
   }

   public b a() {
      return new b((SignupApi)this.b.b(), (MigrationApi)this.c.b(), (co.uk.getmondo.api.b.a)this.d.b(), (g)this.e.b(), (co.uk.getmondo.migration.d)this.f.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
