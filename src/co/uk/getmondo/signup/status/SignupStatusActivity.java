package co.uk.getmondo.signup.status;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.j;
import co.uk.getmondo.signup.card_activation.CardOnItsWayActivity;
import co.uk.getmondo.signup.card_ordering.OrderCardActivity;
import co.uk.getmondo.signup.documents.LegalDocumentsActivity;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import co.uk.getmondo.signup.marketing_opt_in.MarketingOptInActivity;
import co.uk.getmondo.signup.pending.SignupPendingActivity;
import co.uk.getmondo.signup.phone_verification.PhoneVerificationActivity;
import co.uk.getmondo.signup.profile.ProfileCreationActivity;
import co.uk.getmondo.signup.rejected.SignupRejectedActivity;
import co.uk.getmondo.signup.tax_residency.TaxResidencyActivity;
import co.uk.getmondo.signup_old.CreateProfileActivity;
import co.uk.getmondo.waitlist.WaitlistActivity;
import io.reactivex.n;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 32\u00020\u00012\u00020\u0002:\u00013B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0014\u001a\u00020\u0012H\u0016J\"\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\u0012\u0010\u001b\u001a\u00020\u00122\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0014J\b\u0010\u001e\u001a\u00020\u0012H\u0014J\u000e\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00120 H\u0016J\u000e\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00120 H\u0016J\b\u0010\"\u001a\u00020\u0012H\u0016J\b\u0010#\u001a\u00020\u0012H\u0016J\b\u0010$\u001a\u00020\u0012H\u0016J\b\u0010%\u001a\u00020\u0012H\u0016J\b\u0010&\u001a\u00020\u0012H\u0016J\b\u0010'\u001a\u00020\u0012H\u0016J\b\u0010(\u001a\u00020\u0012H\u0016J\b\u0010)\u001a\u00020\u0012H\u0016J\b\u0010*\u001a\u00020\u0012H\u0016J\b\u0010+\u001a\u00020\u0012H\u0016J\b\u0010,\u001a\u00020\u0012H\u0016J\b\u0010-\u001a\u00020\u0012H\u0016J\b\u0010.\u001a\u00020\u0012H\u0016J\u0010\u0010/\u001a\u00020\u00122\u0006\u00100\u001a\u000201H\u0016J\b\u00102\u001a\u00020\u0012H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR2\u0010\u0010\u001a&\u0012\f\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012 \u0013*\u0012\u0012\f\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00110\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00064"},
   d2 = {"Lco/uk/getmondo/signup/status/SignupStatusActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;", "()V", "entryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "entryPoint$delegate", "Lkotlin/Lazy;", "presenter", "Lco/uk/getmondo/signup/status/SignupStatusPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/status/SignupStatusPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/status/SignupStatusPresenter;)V", "stageCompletedRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "hideLoading", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onRetryClicked", "Lio/reactivex/Observable;", "onStageCompleted", "openCardActivation", "openCardOrdering", "openHome", "openIdentityVerification", "openLegalDocuments", "openMarketingOptIn", "openPhoneVerification", "openProfileCreation", "openSignUpPending", "openSignUpRejected", "openTaxResidency", "openWaitingList", "openWaitingListSignup", "showError", "message", "", "showLoading", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignupStatusActivity extends co.uk.getmondo.common.activities.b implements d.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(SignupStatusActivity.class), "entryPoint", "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"))};
   public static final SignupStatusActivity.a c = new SignupStatusActivity.a((i)null);
   public d b;
   private final com.b.b.c e = com.b.b.c.a();
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final j b() {
         Serializable var = SignupStatusActivity.this.getIntent().getSerializableExtra("KEY_ENTRY_POINT");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.SignupEntryPoint");
         } else {
            return (j)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap g;

   public static final Intent a(Context var, j var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "signupEntryPoint");
      return c.a(var, var);
   }

   public void A() {
      this.startActivityForResult(WaitlistActivity.a((Context)this), 1);
   }

   public void B() {
      this.startActivityForResult(CreateProfileActivity.b((Context)this), 1);
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public j a() {
      kotlin.c var = this.f;
      l var = a[0];
      return (j)var.a();
   }

   public n b() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.signupStatusErrorView)).c();
   }

   public void b(String var) {
      kotlin.d.b.l.b(var, "message");
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.signupStatusErrorView)).setMessage(var);
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.signupStatusErrorView)));
   }

   public n c() {
      com.b.b.c var = this.e;
      kotlin.d.b.l.a(var, "stageCompletedRelay");
      return (n)var;
   }

   public void d() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.signupStatusProgress)));
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.signupStatusErrorView));
   }

   public void e() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.signupStatusProgress));
   }

   public void f() {
      this.startActivity(HomeActivity.f.b((Context)this));
      this.finishAffinity();
   }

   public void g() {
      this.startActivityForResult(SignupRejectedActivity.b.a((Context)this), 1);
   }

   public void h() {
      this.startActivityForResult(ProfileCreationActivity.a.a((Context)this, this.a()), 1);
   }

   public void i() {
      this.startActivityForResult(PhoneVerificationActivity.a.a((Context)this, this.a()), 1);
   }

   public void j() {
      this.startActivityForResult(MarketingOptInActivity.a.a((Context)this, this.a()), 1);
   }

   public void k() {
      this.startActivityForResult(LegalDocumentsActivity.a.a((Context)this, this.a()), 1);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1) {
         switch(var) {
         case -1:
            this.e.a((Object)kotlin.n.a);
            break;
         case 0:
            this.finish();
         }
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034210);
      this.l().a(this);
      d var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((d.a)this);
   }

   protected void onDestroy() {
      d var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   public void v() {
      this.startActivityForResult(IdentityVerificationActivity.a.a(IdentityVerificationActivity.g, (Context)this, co.uk.getmondo.signup.identity_verification.a.j.b, Impression.KycFrom.SIGNUP, SignupSource.PERSONAL_ACCOUNT, this.a(), (String)null, 32, (Object)null), 1);
   }

   public void w() {
      this.startActivityForResult(TaxResidencyActivity.b.a((Context)this, this.a()), 1);
   }

   public void x() {
      this.startActivityForResult(OrderCardActivity.a.a((Context)this, this.a()), 1);
   }

   public void y() {
      this.startActivityForResult(CardOnItsWayActivity.g.a((Context)this, this.a()), 1);
   }

   public void z() {
      this.startActivityForResult(SignupPendingActivity.b.a((Context)this, this.a()), 1);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/signup/status/SignupStatusActivity$Companion;", "", "()V", "KEY_ENTRY_POINT", "", "REQUEST_CODE_STAGE", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, j var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, SignupStatusActivity.class)).putExtra("KEY_ENTRY_POINT", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, SignupSt…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
