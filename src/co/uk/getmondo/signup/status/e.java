package co.uk.getmondo.signup.status;

import co.uk.getmondo.api.model.signup.SignupInfo;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class e {
   // $FF: synthetic field
   public static final int[] a = new int[SignupInfo.Status.values().length];
   // $FF: synthetic field
   public static final int[] b;
   // $FF: synthetic field
   public static final int[] c;

   static {
      a[SignupInfo.Status.COMPLETED.ordinal()] = 1;
      a[SignupInfo.Status.IN_PROGRESS.ordinal()] = 2;
      a[SignupInfo.Status.APPROVED.ordinal()] = 3;
      a[SignupInfo.Status.REJECTED.ordinal()] = 4;
      a[SignupInfo.Status.NOT_STARTED.ordinal()] = 5;
      b = new int[SignupInfo.Stage.values().length];
      b[SignupInfo.Stage.WAIT_LIST_SIGNUP.ordinal()] = 1;
      b[SignupInfo.Stage.WAIT_LIST.ordinal()] = 2;
      c = new int[SignupInfo.Stage.values().length];
      c[SignupInfo.Stage.PROFILE_DETAILS.ordinal()] = 1;
      c[SignupInfo.Stage.PHONE_VERIFICATION.ordinal()] = 2;
      c[SignupInfo.Stage.MARKETING_OPT_IN.ordinal()] = 3;
      c[SignupInfo.Stage.LEGAL_DOCUMENTS.ordinal()] = 4;
      c[SignupInfo.Stage.IDENTITY_VERIFICATION.ordinal()] = 5;
      c[SignupInfo.Stage.TAX_RESIDENCY.ordinal()] = 6;
      c[SignupInfo.Stage.CARD_ORDER.ordinal()] = 7;
      c[SignupInfo.Stage.CARD_ACTIVATION.ordinal()] = 8;
      c[SignupInfo.Stage.PENDING.ordinal()] = 9;
      c[SignupInfo.Stage.DEVICE_AUTHENTICATION_ENROLMENT.ordinal()] = 10;
      c[SignupInfo.Stage.NONE.ordinal()] = 11;
      c[SignupInfo.Stage.DONE.ordinal()] = 12;
      c[SignupInfo.Stage.WAIT_LIST.ordinal()] = 13;
      c[SignupInfo.Stage.WAIT_LIST_SIGNUP.ordinal()] = 14;
   }
}
