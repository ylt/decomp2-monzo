package co.uk.getmondo.signup.status;

import co.uk.getmondo.api.model.signup.SignupInfo;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.signup.j;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u000fH\u0002J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\r\u001a\u00020\u000e*\u00020\u000f8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0010¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/signup/status/SignupStatusPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "signupStatusManager", "Lco/uk/getmondo/signup/status/SignupStatusManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/status/SignupStatusManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/api/interactors/UserInteractor;)V", "isReadyToStart", "", "Lco/uk/getmondo/api/model/signup/SignupInfo;", "(Lco/uk/getmondo/api/model/signup/SignupInfo;)Z", "processStage", "", "stage", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "processStageWhenStatusNotStarted", "signupInfo", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final b e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;

   public d(u var, u var, b var, co.uk.getmondo.common.e.a var, co.uk.getmondo.api.b.a var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "signupStatusManager");
      l.b(var, "apiErrorHandler");
      l.b(var, "userInteractor");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(SignupInfo.Stage var) {
      switch(e.c[var.ordinal()]) {
      case 1:
         ((d.a)this.a).h();
         break;
      case 2:
         ((d.a)this.a).i();
         break;
      case 3:
         ((d.a)this.a).j();
         break;
      case 4:
         ((d.a)this.a).k();
         break;
      case 5:
         ((d.a)this.a).v();
         break;
      case 6:
         ((d.a)this.a).w();
         break;
      case 7:
         ((d.a)this.a).x();
         break;
      case 8:
         ((d.a)this.a).y();
         break;
      case 9:
         ((d.a)this.a).z();
         break;
      case 10:
         throw (Throwable)(new IllegalStateException("Unexpected stage! Should have been skipped"));
      case 11:
         throw (Throwable)(new IllegalStateException("Stage is missing when status is in-progress"));
      case 12:
         throw (Throwable)(new IllegalStateException("Unexpected stage! Should be Status.COMPLETED"));
      case 13:
      case 14:
         throw (Throwable)(new IllegalStateException("" + var + " stage is only allowed if status is NOT_STARTED"));
      }

   }

   private final void a(SignupInfo var) {
      SignupInfo.Stage var = var.b();
      switch(e.b[var.ordinal()]) {
      case 1:
         ((d.a)this.a).B();
         break;
      case 2:
         ((d.a)this.a).A();
         break;
      default:
         throw (Throwable)(new IllegalStateException("Unexpected stage " + var.b() + " when status is NOT_STARTED"));
      }

   }

   private final boolean b(SignupInfo var) {
      boolean var;
      if(l.a(var.a(), SignupInfo.Status.NOT_STARTED) && l.a(var.b(), SignupInfo.Stage.WAIT_LIST) ^ true && l.a(var.b(), SignupInfo.Stage.WAIT_LIST_SIGNUP) ^ true) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(final d.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = n.merge((r)var.b(), (r)var.c()).startWith((Object)kotlin.n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n varx) {
            l.b(varx, "it");
            boolean var = l.a(var.a(), j.b);
            return co.uk.getmondo.common.j.f.a(d.this.e.a(var).a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final v a(final SignupInfo varx) {
                  l.b(varx, "it");
                  v var;
                  if(d.this.b(varx)) {
                     var = d.this.e.b();
                  } else if(l.a(varx.a(), SignupInfo.Status.IN_PROGRESS) && l.a(varx.b(), SignupInfo.Stage.DEVICE_AUTHENTICATION_ENROLMENT)) {
                     var = d.this.e.c();
                  } else if(l.a(varx.a(), SignupInfo.Status.COMPLETED)) {
                     var = d.this.g.a().d((io.reactivex.c.h)(new io.reactivex.c.h() {
                        public final SignupInfo a(ak varxx) {
                           l.b(varxx, "<anonymous parameter 0>");
                           return varx;
                        }
                     }));
                  } else {
                     var = v.a((Object)varx);
                  }

                  return var;
               }
            })).b(d.this.d).a(d.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.d();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(SignupInfo varx, Throwable var) {
                  var.e();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = d.this.f;
                  l.a(varx, "it");
                  if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(SignupInfo varx) {
            SignupInfo.Status var = varx.a();
            switch(e.a[var.ordinal()]) {
            case 1:
               var.f();
               break;
            case 2:
            case 3:
               d.this.a(varx.b());
               break;
            case 4:
               var.g();
               break;
            case 5:
               d var = d.this;
               l.a(varx, "it");
               var.a(varx);
            }

         }
      }));
      l.a(var, "Observable.merge(view.on…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0007\u001a\u00020\bH&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\nH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\nH&J\b\u0010\f\u001a\u00020\bH&J\b\u0010\r\u001a\u00020\bH&J\b\u0010\u000e\u001a\u00020\bH&J\b\u0010\u000f\u001a\u00020\bH&J\b\u0010\u0010\u001a\u00020\bH&J\b\u0010\u0011\u001a\u00020\bH&J\b\u0010\u0012\u001a\u00020\bH&J\b\u0010\u0013\u001a\u00020\bH&J\b\u0010\u0014\u001a\u00020\bH&J\b\u0010\u0015\u001a\u00020\bH&J\b\u0010\u0016\u001a\u00020\bH&J\b\u0010\u0017\u001a\u00020\bH&J\b\u0010\u0018\u001a\u00020\bH&J\b\u0010\u0019\u001a\u00020\bH&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/signup/status/SignupStatusPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "entryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "hideLoading", "", "onRetryClicked", "Lio/reactivex/Observable;", "onStageCompleted", "openCardActivation", "openCardOrdering", "openHome", "openIdentityVerification", "openLegalDocuments", "openMarketingOptIn", "openPhoneVerification", "openProfileCreation", "openSignUpPending", "openSignUpRejected", "openTaxResidency", "openWaitingList", "openWaitingListSignup", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      j a();

      n b();

      n c();

      void d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();

      void k();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
