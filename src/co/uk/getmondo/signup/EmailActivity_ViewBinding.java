package co.uk.getmondo.signup;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class EmailActivity_ViewBinding implements Unbinder {
   private EmailActivity a;
   private View b;
   private View c;

   public EmailActivity_ViewBinding(final EmailActivity var, View var) {
      this.a = var;
      var.content = (TextView)Utils.findRequiredViewAsType(var, 2131820654, "field 'content'", TextView.class);
      View var = Utils.findRequiredView(var, 2131820890, "method 'openMail'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.openMail();
         }
      });
      var = Utils.findRequiredView(var, 2131820891, "method 'onNoEmailClick'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onNoEmailClick();
         }
      });
   }

   public void unbind() {
      EmailActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.content = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
