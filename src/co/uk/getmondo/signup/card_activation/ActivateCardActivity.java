package co.uk.getmondo.signup.card_activation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.q;
import io.reactivex.n;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001cH\u0016J\u0012\u0010\u001e\u001a\u00020\u001c2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\b\u0010!\u001a\u00020\u001cH\u0014J\b\u0010\"\u001a\u00020\u001cH\u0016J\b\u0010#\u001a\u00020\u001cH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\rR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00168\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a¨\u0006%"},
   d2 = {"Lco/uk/getmondo/signup/card_activation/ActivateCardActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter$View;", "()V", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "maxLength", "", "getMaxLength", "()I", "maxLength$delegate", "Lkotlin/Lazy;", "onCardPanEntered", "Lio/reactivex/Observable;", "", "getOnCardPanEntered", "()Lio/reactivex/Observable;", "presenter", "Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;)V", "cardActivated", "", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "reloadSignupStatus", "showLoading", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ActivateCardActivity extends co.uk.getmondo.common.activities.b implements b.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(ActivateCardActivity.class), "maxLength", "getMaxLength()I"))};
   public static final ActivateCardActivity.a e = new ActivateCardActivity.a((i)null);
   public b b;
   public q c;
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final int b() {
         return ActivateCardActivity.this.getResources().getInteger(2131558409);
      }

      // $FF: synthetic method
      public Object v_() {
         return Integer.valueOf(this.b());
      }
   }));
   private HashMap g;

   private final int f() {
      kotlin.c var = this.f;
      l var = a[0];
      return ((Number)var.a()).intValue();
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      com.b.a.a var = com.b.a.d.e.d((EditText)this.a(co.uk.getmondo.c.a.activateCardEditText));
      kotlin.d.b.l.a(var, "RxTextView.afterTextChangeEvents(this)");
      n var = var.filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(com.b.a.d.f var) {
            kotlin.d.b.l.b(var, "it");
            Editable var = var.b();
            boolean var;
            if(var != null && var.length() == ActivateCardActivity.this.f()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "activateCardEditText.aft…t.editable().toString() }");
      return var;
   }

   public void b() {
      this.setResult(-1);
      this.finish();
   }

   public void c() {
      this.setResult(-1);
      this.finish();
   }

   public void d() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.activateCardProgressBar)));
      ae.a(this.a(co.uk.getmondo.c.a.activateCardOverlayView));
   }

   public void e() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.activateCardProgressBar));
      ae.b(this.a(co.uk.getmondo.c.a.activateCardOverlayView));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034140);
      co.uk.getmondo.common.k.i var = co.uk.getmondo.common.k.i.a(' ').a((int)4);
      ((EditText)this.a(co.uk.getmondo.c.a.activateCardEditText)).addTextChangedListener(var.a());
      this.l().a(this);
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/card_activation/ActivateCardActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         kotlin.d.b.l.b(var, "context");
         return new Intent(var, ActivateCardActivity.class);
      }
   }
}
