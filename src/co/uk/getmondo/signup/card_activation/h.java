package co.uk.getmondo.signup.card_activation;

import co.uk.getmondo.settings.k;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!h.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public h(b.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var) {
      return new h(var, var, var);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g((co.uk.getmondo.common.a)this.c.b(), (k)this.d.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
