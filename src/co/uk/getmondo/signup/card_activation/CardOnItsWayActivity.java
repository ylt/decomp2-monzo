package co.uk.getmondo.signup.card_activation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.j;
import io.reactivex.n;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0001)B\u0005¢\u0006\u0002\u0010\u0003J\"\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0012\u0010 \u001a\u00020\f2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0014J\b\u0010#\u001a\u00020\fH\u0014J\b\u0010$\u001a\u00020\fH\u0014J\b\u0010%\u001a\u00020\fH\u0016J\b\u0010&\u001a\u00020\fH\u0016J\b\u0010'\u001a\u00020\fH\u0016J\b\u0010(\u001a\u00020\fH\u0016R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000eR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R2\u0010\u0017\u001a&\u0012\f\u0012\n \u0019*\u0004\u0018\u00010\f0\f \u0019*\u0012\u0012\f\u0012\n \u0019*\u0004\u0018\u00010\f0\f\u0018\u00010\u00180\u0018X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006*"},
   d2 = {"Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter$View;", "()V", "entryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "entryPoint$delegate", "Lkotlin/Lazy;", "onMyCardArrivedClicked", "Lio/reactivex/Observable;", "", "getOnMyCardArrivedClicked", "()Lio/reactivex/Observable;", "onRefresh", "getOnRefresh", "presenter", "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;)V", "resumeRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "kotlin.jvm.PlatformType", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onResume", "openCardActivation", "openHome", "showBackToMonzoFeed", "showMyCardArrived", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CardOnItsWayActivity extends co.uk.getmondo.signup.a implements g.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(CardOnItsWayActivity.class), "entryPoint", "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"))};
   public static final CardOnItsWayActivity.a g = new CardOnItsWayActivity.a((i)null);
   public g b;
   private final com.b.b.c h = com.b.b.c.a();
   private final kotlin.c i = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final j b() {
         return CardOnItsWayActivity.this.a();
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap j;

   public View a(int var) {
      if(this.j == null) {
         this.j = new HashMap();
      }

      View var = (View)this.j.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.j.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public j c() {
      kotlin.c var = this.i;
      l var = a[0];
      return (j)var.a();
   }

   public n d() {
      com.b.b.c var = this.h;
      kotlin.d.b.l.a(var, "resumeRelay");
      return (n)var;
   }

   public n e() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.cardOnItsWayButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void f() {
      ae.a((View)((Button)this.a(co.uk.getmondo.c.a.cardOnItsWayButton)));
   }

   public void g() {
      ((Button)this.a(co.uk.getmondo.c.a.cardOnItsWayButton)).setText((CharSequence)this.getString(2131362442));
      ae.a((View)((Button)this.a(co.uk.getmondo.c.a.cardOnItsWayButton)));
   }

   public void h() {
      this.startActivityForResult(ActivateCardActivity.e.a((Context)this), 1001);
   }

   public void i() {
      HomeActivity.f.a((Context)this);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1001) {
         if(var == -1) {
            this.setResult(-1);
            this.finish();
         }
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034149);
      this.l().a(this);
      g var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   protected void onDestroy() {
      g var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   protected void onResume() {
      super.onResume();
      this.h.a((Object)kotlin.n.a);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/signup/card_activation/CardOnItsWayActivity$Companion;", "", "()V", "REQUEST_CODE_ACTIVATE_CARD", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, j var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, CardOnItsWayActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, CardOnIt…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
