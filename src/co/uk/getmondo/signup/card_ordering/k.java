package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import io.reactivex.v;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/OrderCardManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "(Lco/uk/getmondo/api/SignupApi;)V", "cardOrderOptions", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/order_card/CardOrderOptions;", "createCardOrder", "Lio/reactivex/Completable;", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "pin", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k {
   private final SignupApi a;

   public k(SignupApi var) {
      kotlin.d.b.l.b(var, "signupApi");
      super();
      this.a = var;
   }

   public final io.reactivex.b a(CardOrderName.Type var, String var) {
      kotlin.d.b.l.b(var, "nameType");
      kotlin.d.b.l.b(var, "pin");
      return this.a.createCardOrder(var.a(), "specified", var);
   }

   public final v a() {
      return this.a.cardOrderOptions();
   }
}
