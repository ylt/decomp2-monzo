package co.uk.getmondo.signup.card_ordering;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.PinEntryView;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import io.reactivex.c.q;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 62\u00020\u00012\u00020\u0002:\u000267B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u001eH\u0016J\b\u0010 \u001a\u00020\u001eH\u0016J\b\u0010!\u001a\u00020\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0016J\u0012\u0010%\u001a\u00020\u001e2\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J$\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010-2\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J\b\u0010.\u001a\u00020\u001eH\u0016J\u0010\u0010/\u001a\u00020\u001e2\u0006\u00100\u001a\u00020\u0005H\u0016J\u001a\u00101\u001a\u00020\u001e2\u0006\u00102\u001a\u00020)2\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J\b\u00103\u001a\u00020\u001eH\u0016J\b\u00104\u001a\u00020\u001eH\u0016J\b\u00105\u001a\u00020\u001eH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0014\u001a\u00020\u00158\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001a\u001a\u0004\u0018\u00010\u00058VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\n\u001a\u0004\b\u001b\u0010\b¨\u00068"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;", "()V", "nameOnCard", "", "kotlin.jvm.PlatformType", "getNameOnCard", "()Ljava/lang/String;", "nameOnCard$delegate", "Lkotlin/Lazy;", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "getNameType", "()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameType$delegate", "onPinEntered", "Lio/reactivex/Observable;", "getOnPinEntered", "()Lio/reactivex/Observable;", "presenter", "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;)V", "previousPin", "getPreviousPin", "previousPin$delegate", "cardOrdered", "", "clearPin", "hideKeyboard", "hideLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onPinEnteredFirstTime", "pin", "onViewCreated", "view", "reloadSignupStatus", "showKeyboard", "showLoading", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends co.uk.getmondo.common.f.a implements c.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(a.class), "nameOnCard", "getNameOnCard()Ljava/lang/String;")), (kotlin.reflect.l)y.a(new w(y.a(a.class), "nameType", "getNameType()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;")), (kotlin.reflect.l)y.a(new w(y.a(a.class), "previousPin", "getPreviousPin()Ljava/lang/String;"))};
   public static final a.a d = new a.a((kotlin.d.b.i)null);
   public c c;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return a.this.getArguments().getString("EXTRA_NAME_ON_CARD");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final CardOrderName.Type b() {
         Serializable var = a.this.getArguments().getSerializable("EXTRA_NAME_TYPE");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.order_card.CardOrderName.Type");
         } else {
            return (CardOrderName.Type)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return a.this.getArguments().getString("EXTRA_PREVIOUS_PIN");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap h;

   private final String l() {
      kotlin.c var = this.e;
      kotlin.reflect.l var = a[0];
      return (String)var.a();
   }

   public View a(int var) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var = (View)this.h.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.h.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public CardOrderName.Type a() {
      kotlin.c var = this.f;
      kotlin.reflect.l var = a[1];
      return (CardOrderName.Type)var.a();
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "pin");
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ChoosePinFragment.StepListener");
      } else {
         a.b var = (a.b)var;
         CardOrderName.Type var = this.a();
         String var = this.l();
         kotlin.d.b.l.a(var, "nameOnCard");
         var.a(var, var, var);
      }
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public String c() {
      kotlin.c var = this.g;
      kotlin.reflect.l var = a[2];
      return (String)var.a();
   }

   public n d() {
      n var = n.create((p)(new p() {
         public final void a(final o var) {
            kotlin.d.b.l.b(var, "emitter");
            ((PinEntryView)a.this.a(co.uk.getmondo.c.a.choosePinEntryView)).setOnPinEnteredListener((PinEntryView.a)(new PinEntryView.a() {
               public final void a(String varx) {
                  var.a(varx);
               }
            }));
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((PinEntryView)a.this.a(co.uk.getmondo.c.a.choosePinEntryView)).setOnPinEnteredListener((PinEntryView.a)null);
               }
            }));
         }
      })).filter((q)(new q() {
         public final boolean a(String var) {
            kotlin.d.b.l.b(var, "<anonymous parameter 0>");
            return a.this.isResumed();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create<String…filter { _ -> isResumed }");
      return var;
   }

   public void e() {
      ((PinEntryView)this.a(co.uk.getmondo.c.a.choosePinEntryView)).requestFocus();
      ae.a((EditText)((PinEntryView)this.a(co.uk.getmondo.c.a.choosePinEntryView)));
   }

   public void f() {
      ae.c((PinEntryView)this.a(co.uk.getmondo.c.a.choosePinEntryView));
   }

   public void g() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.choosePinProgressBar)));
      ae.a(this.a(co.uk.getmondo.c.a.choosePinOverlayView));
   }

   public void h() {
      ae.b(this.a(co.uk.getmondo.c.a.choosePinOverlayView));
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.choosePinProgressBar));
   }

   public void i() {
      ((PinEntryView)this.a(co.uk.getmondo.c.a.choosePinEntryView)).a();
   }

   public void j() {
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ChoosePinFragment.StepListener");
      } else {
         ((a.b)var).d_();
      }
   }

   public void k() {
      if(this.h != null) {
         this.h.clear();
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof a.b)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + a.b.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      this.getActivity().getWindow().setSoftInputMode(36);
      View var = var.inflate(2131034267, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…se_pin, container, false)");
      return var;
   }

   public void onDestroyView() {
      c var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.k();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      c var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((c.a)this);
      ((TextView)this.a(co.uk.getmondo.c.a.choosePinNameOnCardTextView)).setText((CharSequence)this.l());
      if(this.c() != null) {
         ((TextView)this.a(co.uk.getmondo.c.a.choosePinTitleTextView)).setText(2131362518);
         ((TextView)this.a(co.uk.getmondo.c.a.choosePinSubtitleTextView)).setText(2131362517);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment$Companion;", "", "()V", "EXTRA_NAME_ON_CARD", "", "EXTRA_NAME_TYPE", "EXTRA_PREVIOUS_PIN", "newInstance", "Landroid/support/v4/app/Fragment;", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "previousPin", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Fragment a(CardOrderName.Type var, String var, String var) {
         kotlin.d.b.l.b(var, "nameType");
         kotlin.d.b.l.b(var, "nameOnCard");
         a var = new a();
         Bundle var = new Bundle();
         var.putString("EXTRA_NAME_ON_CARD", var);
         var.putSerializable("EXTRA_NAME_TYPE", (Serializable)var);
         var.putString("EXTRA_PREVIOUS_PIN", var);
         var.setArguments(var);
         return (Fragment)var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J \u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH&¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment$StepListener;", "", "onCardOrdered", "", "onPinEntered", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "pin", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void a(CardOrderName.Type var, String var, String var);

      void d_();
   }
}
