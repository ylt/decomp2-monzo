package co.uk.getmondo.signup.card_ordering;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import io.reactivex.n;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\u0018\u0000 62\u00020\u00012\u00020\u0002:\u000267B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0015\u001a\u00020\u0006H\u0016J\b\u0010\u0016\u001a\u00020\u0006H\u0016J\u0010\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0012\u0010\u001a\u001a\u00020\u00062\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J$\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010#\u001a\u00020\u0006H\u0016J\u001a\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u001e2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0018\u0010&\u001a\u00020\u00062\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J\b\u0010+\u001a\u00020\u0006H\u0016J\u0010\u0010,\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\u0010\u0010.\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\u0010\u0010/\u001a\u00020\u00062\u0006\u00100\u001a\u00020*H\u0016J\u0010\u00101\u001a\u00020\u00062\u0006\u00102\u001a\u00020*H\u0016J\u0010\u00103\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\b\u00104\u001a\u00020\u0006H\u0016J\u0010\u00105\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u00068"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "()V", "onLegalNameSelected", "Lio/reactivex/Observable;", "", "getOnLegalNameSelected", "()Lio/reactivex/Observable;", "onPreferredNameSelected", "getOnPreferredNameSelected", "onRetryClicked", "getOnRetryClicked", "onSendCardClicked", "getOnSendCardClicked", "presenter", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;)V", "enableChoosingAName", "hideLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "openChooseYourPin", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "reloadSignupStatus", "setLegalNameSelected", "name", "setPreferredNameSelected", "showAddress", "address", "showError", "message", "showLegalName", "showLoading", "showPreferredName", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   public static final e.a c = new e.a((kotlin.d.b.i)null);
   public g a;
   private HashMap d;

   public View a(int var) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var = (View)this.d.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.d.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public n a() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)).c();
   }

   public void a(CardOrderName.Type var, String var) {
      kotlin.d.b.l.b(var, "nameType");
      kotlin.d.b.l.b(var, "nameOnCard");
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ConfirmCardDetailsFragment.StepListener");
      } else {
         ((e.b)var).a(var, var);
      }
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "name");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTitleTextView)));
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTextView)));
      TextView var = (TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTextView);
      Resources var = this.getResources();
      Locale var = Locale.ENGLISH;
      kotlin.d.b.l.a(var, "Locale.ENGLISH");
      String var = var.toUpperCase(var);
      kotlin.d.b.l.a(var, "(this as java.lang.String).toUpperCase(locale)");
      var.setText((CharSequence)var.getString(2131362512, new Object[]{var}));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public void b(String var) {
      kotlin.d.b.l.b(var, "name");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTitleTextView)));
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTextView)));
      TextView var = (TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTextView);
      Resources var = this.getResources();
      Locale var = Locale.ENGLISH;
      kotlin.d.b.l.a(var, "Locale.ENGLISH");
      String var = var.toUpperCase(var);
      kotlin.d.b.l.a(var, "(this as java.lang.String).toUpperCase(locale)");
      var.setText((CharSequence)var.getString(2131362514, new Object[]{var}));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var);
   }

   public n c() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.orderCardNextButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void c(String var) {
      kotlin.d.b.l.b(var, "address");
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardAddressTextView)).setText((CharSequence)var);
      ae.a((View)((LinearLayout)this.a(co.uk.getmondo.c.a.confirmCardDetailsContainer)));
   }

   public n d() {
      n var = com.b.a.c.c.a((RelativeLayout)this.a(co.uk.getmondo.c.a.orderCardLegalNameViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void d(String var) {
      kotlin.d.b.l.b(var, "message");
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)).setMessage(var);
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)));
   }

   public n e() {
      n var = com.b.a.c.c.a((RelativeLayout)this.a(co.uk.getmondo.c.a.orderCardPreferredNameViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void e(String var) {
      kotlin.d.b.l.b(var, "name");
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameImageView));
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.orderCardLegalNameImageView)));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var);
   }

   public void f() {
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView));
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.orderCardProgressBar)));
   }

   public void f(String var) {
      kotlin.d.b.l.b(var, "name");
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.orderCardLegalNameImageView));
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameImageView)));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var);
   }

   public void g() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.orderCardProgressBar));
   }

   public void h() {
      ae.a(this.a(co.uk.getmondo.c.a.orderCardDividerView));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardTitleTextView)).setText((CharSequence)this.getResources().getString(2131362508));
   }

   public void i() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof e.b)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + e.b.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034268, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…etails, container, false)");
      return var;
   }

   public void onDestroyView() {
      g var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.i();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      g var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$Companion;", "", "()V", "newInstance", "Landroid/support/v4/app/Fragment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Fragment a() {
         return (Fragment)(new e());
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$StepListener;", "", "onCardDetailsConfirmed", "", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void a(CardOrderName.Type var, String var);
   }
}
