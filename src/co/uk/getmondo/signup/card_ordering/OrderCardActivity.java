package co.uk.getmondo.signup.card_ordering;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u0012B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0006H\u0016J\u0012\u0010\f\u001a\u00020\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J \u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\nH\u0016J\"\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\nH\u0002¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/OrderCardActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$StepListener;", "Lco/uk/getmondo/signup/card_ordering/ChoosePinFragment$StepListener;", "()V", "onCardDetailsConfirmed", "", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "onCardOrdered", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onPinEntered", "pin", "showChoosePinFragment", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class OrderCardActivity extends co.uk.getmondo.signup.a implements a.b, e.b {
   public static final OrderCardActivity.a a = new OrderCardActivity.a((kotlin.d.b.i)null);
   private HashMap b;

   private final void b(CardOrderName.Type var, String var, String var) {
      this.getSupportFragmentManager().a().b(2131821009, a.d.a(var, var, var)).a((String)null).a(4097).c();
   }

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void a(CardOrderName.Type var, String var) {
      kotlin.d.b.l.b(var, "nameType");
      kotlin.d.b.l.b(var, "nameOnCard");
      this.b(var, var, (String)null);
   }

   public void a(CardOrderName.Type var, String var, String var) {
      kotlin.d.b.l.b(var, "nameType");
      kotlin.d.b.l.b(var, "nameOnCard");
      kotlin.d.b.l.b(var, "pin");
      this.b(var, var, var);
   }

   public void d_() {
      this.setResult(-1);
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034191);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(false);
      }

      var = this.getSupportActionBar();
      if(var != null) {
         var.d(false);
      }

      if(var == null) {
         this.getSupportFragmentManager().a().a(2131821009, e.c.a()).c();
      }

      this.l().a(this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/OrderCardActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, co.uk.getmondo.signup.j var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, OrderCardActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, OrderCar…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
