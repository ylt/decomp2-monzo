package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.c.q;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "orderCardManager", "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "view", "it", "", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final k e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public c(u var, u var, k var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "orderCardManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(c.a var, Throwable var) {
      var.h();
      var.f();
      if(!co.uk.getmondo.signup.i.a.a(var, (co.uk.getmondo.signup.i.a)var) && !this.f.a(var, (co.uk.getmondo.common.e.a.a)var)) {
         var.b(2131362198);
      }

   }

   public void a(final c.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      var.e();
      if(var.c() == null) {
         this.g.a(Impression.Companion.aS());
      } else {
         this.g.a(Impression.Companion.aT());
      }

      n var = var.d().share();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.filter((q)(new q() {
         public final boolean a(String varx) {
            kotlin.d.b.l.b(varx, "<anonymous parameter 0>");
            boolean var;
            if(var.c() == null) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            c.a var = var;
            kotlin.d.b.l.a(varx, "pin");
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "onPinEntered\n           …inEnteredFirstTime(pin) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.filter((q)(new q() {
         public final boolean a(String varx) {
            kotlin.d.b.l.b(varx, "pin");
            boolean var;
            if(var.c() != null && kotlin.d.b.l.a(var.c(), varx) ^ true) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            c.this.g.a(Impression.Companion.aU());
            var.i();
            var.b(2131362516);
         }
      }));
      kotlin.d.b.l.a(var, "onPinEntered\n           …ct_pin)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.filter((q)(new q() {
         public final boolean a(String varx) {
            kotlin.d.b.l.b(varx, "pin");
            return kotlin.d.b.l.a(var.c(), varx);
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String varx) {
            kotlin.d.b.l.b(varx, "pin");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)c.this.e.a(var.a(), varx).b(c.this.c).a(c.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.g();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  c var = c.this;
                  c.a var = var;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(var, varx);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.j();
         }
      }));
      kotlin.d.b.l.a(var, "onPinEntered\n           …be { view.cardOrdered() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\b\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0010\u001a\u00020\u0011H&J\b\u0010\u0012\u001a\u00020\u0011H&J\b\u0010\u0013\u001a\u00020\u0011H&J\b\u0010\u0014\u001a\u00020\u0011H&J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\nH&J\b\u0010\u0017\u001a\u00020\u0011H&J\b\u0010\u0018\u001a\u00020\u0011H&R\u0012\u0010\u0004\u001a\u00020\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u0004\u0018\u00010\nX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "getNameType", "()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "onPinEntered", "Lio/reactivex/Observable;", "", "getOnPinEntered", "()Lio/reactivex/Observable;", "previousPin", "getPreviousPin", "()Ljava/lang/String;", "cardOrdered", "", "clearPin", "hideKeyboard", "hideLoading", "onPinEnteredFirstTime", "pin", "showKeyboard", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      CardOrderName.Type a();

      void a(String var);

      String c();

      n d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
