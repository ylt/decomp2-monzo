package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.model.ApiAddress;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.api.model.order_card.CardOrderOptions;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.c.q;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "orderCardManager", "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "canChooseName", "", "legalName", "", "preferredName", "selectedNameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "register", "", "view", "setLegalNameSelected", "setPreferredNameSelected", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private String c;
   private String d;
   private boolean e;
   private CardOrderName.Type f;
   private final u g;
   private final u h;
   private final k i;
   private final co.uk.getmondo.common.e.a j;
   private final co.uk.getmondo.common.a k;

   public g(u var, u var, k var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "orderCardManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
   }

   private final void b(g.a var) {
      this.f = CardOrderName.Type.PREFERRED;
      String var = this.d;
      if(var == null) {
         kotlin.d.b.l.a();
      }

      var.f(var);
   }

   private final void c(g.a var) {
      this.f = CardOrderName.Type.LEGAL;
      String var = this.c;
      if(var == null) {
         kotlin.d.b.l.a();
      }

      var.e(var);
   }

   // $FF: synthetic method
   public static final CardOrderName.Type f(g var) {
      CardOrderName.Type var = var.f;
      if(var == null) {
         kotlin.d.b.l.b("selectedNameType");
      }

      return var;
   }

   public void a(final g.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.k.a(Impression.Companion.aQ());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().startWith((Object)n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(n varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(g.this.i.a().b(g.this.g).a(g.this.h).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.f();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(CardOrderOptions varx, Throwable var) {
                  var.g();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.signup.i var = co.uk.getmondo.signup.i.a;
                  kotlin.d.b.l.a(varx, "it");
                  if(!var.a(varx, (co.uk.getmondo.signup.i.a)var) && !g.this.j.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CardOrderOptions varx) {
            List var = varx.a();
            ApiAddress var = varx.b();
            Iterator var = ((Iterable)var).iterator();

            while(var.hasNext()) {
               CardOrderName var = (CardOrderName)var.next();
               CardOrderName.Type var = var.a();
               String var = var.b();
               switch(h.a[var.ordinal()]) {
               case 1:
                  g.this.c = var;
                  g.this.f = CardOrderName.Type.LEGAL;
                  var.a(var);
                  break;
               case 2:
                  g.this.d = var;
                  g.this.f = CardOrderName.Type.PREFERRED;
                  var.b(var);
               }
            }

            if(var.size() > 1) {
               var.h();
               g.this.c(var);
               g.this.e = true;
            }

            var.c(var.e());
         }
      }));
      kotlin.d.b.l.a(var, "view.onRetryClicked\n    …Line())\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.d().skipWhile((q)(new q() {
         public final boolean a(n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!g.this.e) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            g.this.c(var);
         }
      }));
      kotlin.d.b.l.a(var, "view.onLegalNameSelected…LegalNameSelected(view) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.e().skipWhile((q)(new q() {
         public final boolean a(n var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!g.this.e) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            g.this.b(var);
         }
      }));
      kotlin.d.b.l.a(var, "view.onPreferredNameSele…erredNameSelected(view) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            String var;
            String var;
            if(kotlin.d.b.l.a(g.f(g.this), CardOrderName.Type.LEGAL)) {
               var = g.this.c;
               var = var;
               if(var == null) {
                  kotlin.d.b.l.a();
                  var = var;
               }
            } else {
               var = g.this.d;
               var = var;
               if(var == null) {
                  kotlin.d.b.l.a();
                  var = var;
               }
            }

            g.this.k.a(Impression.Companion.p(g.f(g.this).a()));
            var.a(g.f(g.this), var);
         }
      }));
      kotlin.d.b.l.a(var, "view.onSendCardClicked\n …OnCard)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000f\u001a\u00020\u0006H&J\b\u0010\u0010\u001a\u00020\u0006H&J\u0018\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\u0010\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u0015H&J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\b\u0010\u001c\u001a\u00020\u0006H&J\u0010\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u0018\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\b¨\u0006\u001e"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onLegalNameSelected", "Lio/reactivex/Observable;", "", "getOnLegalNameSelected", "()Lio/reactivex/Observable;", "onPreferredNameSelected", "getOnPreferredNameSelected", "onRetryClicked", "getOnRetryClicked", "onSendCardClicked", "getOnSendCardClicked", "enableChoosingAName", "hideLoading", "openChooseYourPin", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "setLegalNameSelected", "name", "setPreferredNameSelected", "showAddress", "address", "showLegalName", "showLoading", "showPreferredName", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(CardOrderName.Type var, String var);

      void a(String var);

      void b(String var);

      io.reactivex.n c();

      void c(String var);

      io.reactivex.n d();

      io.reactivex.n e();

      void e(String var);

      void f();

      void f(String var);

      void g();

      void h();
   }
}
