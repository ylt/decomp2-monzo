package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.SignupApi;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!l.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public l(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new l(var);
   }

   public k a() {
      return new k((SignupApi)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
