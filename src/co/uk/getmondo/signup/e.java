package co.uk.getmondo.signup;

import co.uk.getmondo.common.k;
import co.uk.getmondo.common.s;

public final class e implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var, javax.a.a var) {
      return new e(var, var, var);
   }

   public void a(EmailActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (k)this.b.b();
         var.b = (s)this.c.b();
         var.c = (f)this.d.b();
      }
   }
}
