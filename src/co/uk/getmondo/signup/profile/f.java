package co.uk.getmondo.signup.profile;

public final class f implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a a(javax.a.a var) {
      return new f(var);
   }

   public void a(e var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.c = (g)this.b.b();
      }
   }
}
