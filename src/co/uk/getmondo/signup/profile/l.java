package co.uk.getmondo.signup.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.ProgressButton;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u000b\u0018\u00002\u00020\u00012\u00020\u0002:\u00010B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J$\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u000e\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\rH\u0016J\b\u0010\u001c\u001a\u00020\u000bH\u0016J\u000e\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001b0\rH\u0016J\u000e\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\rH\u0016J\u001a\u0010 \u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u00152\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\b\u0010\"\u001a\u00020\u000bH\u0016J\b\u0010#\u001a\u00020\u000bH\u0016J\u0010\u0010$\u001a\u00020\u000b2\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010'\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020&H\u0016J\u0012\u0010)\u001a\u00020\u000b2\b\u0010*\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010+\u001a\u00020\u000bH\u0016J\b\u0010,\u001a\u00020\u000bH\u0016J\b\u0010-\u001a\u00020\u000bH\u0016J\b\u0010.\u001a\u00020\u000bH\u0016J\b\u0010/\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u00061"},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;)V", "clearDateOfBirthError", "", "onAddPreferredNameClicked", "Lio/reactivex/Observable;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDateOfBirthTextChange", "", "onDestroyView", "onLegalNameTextChange", "onNextClicked", "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;", "onViewCreated", "view", "profileDetailsSubmitted", "reloadSignupStatus", "setNextButtonEnabled", "enabled", "", "setNextButtonLoading", "loading", "showForm", "profileData", "showInvalidDateOfBirthError", "showLegalNameUnsupportedCharError", "showPreferredNameField", "showPreferredNameUnsupportedCharError", "showUnderAgeError", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l extends co.uk.getmondo.common.f.a implements n.a {
   public n a;
   private HashMap c;

   public View a(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.c.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.addPreferredNameButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(k var) {
      if(var != null) {
         ((EditText)this.a(co.uk.getmondo.c.a.legalNameEditText)).setText((CharSequence)var.a());
         ((EditText)this.a(co.uk.getmondo.c.a.dateOfBirthEditText)).setText((CharSequence)var.c());
         if(var.b() != null) {
            ((EditText)this.a(co.uk.getmondo.c.a.preferredNameEditText)).setText((CharSequence)var.b());
            this.f();
         }
      }

      ((EditText)this.a(co.uk.getmondo.c.a.legalNameEditText)).requestFocus();
      ae.a((View)((ScrollView)this.a(co.uk.getmondo.c.a.profileDetailsFormContainer)));
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.profileDetailsProgress));
   }

   public void a(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.profileDetailsNextButton)).setEnabled(var);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public void b(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.profileDetailsNextButton)).setLoading(var);
   }

   public io.reactivex.n c() {
      com.b.a.a var = com.b.a.d.e.c((EditText)this.a(co.uk.getmondo.c.a.dateOfBirthEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      return (io.reactivex.n)var;
   }

   public io.reactivex.n d() {
      com.b.a.a var = com.b.a.d.e.c((EditText)this.a(co.uk.getmondo.c.a.legalNameEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      return (io.reactivex.n)var;
   }

   public io.reactivex.n e() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.profileDetailsNextButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      io.reactivex.r var = (io.reactivex.r)var;
      var = com.b.a.d.e.a((EditText)this.a(co.uk.getmondo.c.a.dateOfBirthEditText));
      kotlin.d.b.l.a(var, "RxTextView.editorActions(this)");
      var = io.reactivex.n.merge(var, (io.reactivex.r)var).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return this.b(var);
         }

         public final k b(Object var) {
            kotlin.d.b.l.b(var, "it");
            return new k(((EditText)l.this.a(co.uk.getmondo.c.a.legalNameEditText)).getText().toString(), ((EditText)l.this.a(co.uk.getmondo.c.a.preferredNameEditText)).getText().toString(), ((EditText)l.this.a(co.uk.getmondo.c.a.dateOfBirthEditText)).getText().toString());
         }
      }));
      kotlin.d.b.l.a(var, "Observable.merge(profile…      )\n                }");
      return var;
   }

   public void f() {
      ae.b((Button)this.a(co.uk.getmondo.c.a.addPreferredNameButton));
      ae.a((View)((TextInputLayout)this.a(co.uk.getmondo.c.a.preferredNameInputLayout)));
      EditText var = ((TextInputLayout)this.a(co.uk.getmondo.c.a.preferredNameInputLayout)).getEditText();
      if(var != null) {
         var.requestFocus();
      }

   }

   public void g() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.dateOfBirthInputLayout)).setError((CharSequence)this.getString(2131362595));
   }

   public void h() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.dateOfBirthInputLayout)).setError((CharSequence)"");
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.dateOfBirthInputLayout)).setErrorEnabled(false);
   }

   public void i() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.dateOfBirthInputLayout)).setError((CharSequence)this.getString(2131362602));
   }

   public void j() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362596), this.getString(2131362603), false).show(this.getFragmentManager(), co.uk.getmondo.common.d.a.class.getName());
   }

   public void k() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362600), this.getString(2131362603), false).show(this.getFragmentManager(), co.uk.getmondo.common.d.a.class.getName());
   }

   public void l() {
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.profile.ProfileDetailsFragment.StepListener");
      } else {
         ((l.a)var).c();
      }
   }

   public void m() {
      if(this.c != null) {
         this.c.clear();
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof l.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + l.a.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034276, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…etails, container, false)");
      return var;
   }

   public void onDestroyView() {
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.m();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      ((EditText)this.a(co.uk.getmondo.c.a.dateOfBirthEditText)).addTextChangedListener((TextWatcher)(new co.uk.getmondo.common.k.d()));
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((n.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsFragment$StepListener;", "", "onProfileDetailsSubmitted", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a {
      void c();
   }
}
