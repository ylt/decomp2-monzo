package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.ApiAddress;
import io.reactivex.v;
import java.util.List;
import kotlin.Metadata;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J$\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u00062\b\b\u0002\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fJ\"\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/signup/profile/SignupProfileManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "(Lco/uk/getmondo/api/SignupApi;)V", "existingProfile", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/signup/SignUpProfile;", "searchAddress", "", "Lco/uk/getmondo/api/model/Address;", "countryCode", "", "postalCode", "submitProfile", "Lio/reactivex/Completable;", "legalName", "preferredName", "dateOfBirth", "Lorg/threeten/bp/LocalDate;", "submitProfileAddressAndCommit", "address", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q {
   private final SignupApi a;

   public q(SignupApi var) {
      kotlin.d.b.l.b(var, "signupApi");
      super();
      this.a = var;
   }

   public final io.reactivex.b a(Address var) {
      kotlin.d.b.l.b(var, "address");
      SignupApi var = this.a;
      Address var;
      if(!(var instanceof ApiAddress)) {
         var = null;
      } else {
         var = var;
      }

      ApiAddress var = (ApiAddress)var;
      String var;
      if(var != null) {
         var = var.f();
      } else {
         var = null;
      }

      List var = var.a();
      String var = var.b();
      if(var == null) {
         var = "";
      }

      String var = var.c();
      if(var == null) {
         var = "";
      }

      io.reactivex.b var = var.submitProfileAddress(var, var, var, var, var.d()).b((io.reactivex.d)this.a.commitProfile());
      kotlin.d.b.l.a(var, "signupApi.submitProfileA…ignupApi.commitProfile())");
      return var;
   }

   public final io.reactivex.b a(String var, String var, LocalDate var) {
      kotlin.d.b.l.b(var, "legalName");
      kotlin.d.b.l.b(var, "dateOfBirth");
      return this.a.submitProfile(var, var, var);
   }

   public final v a() {
      return this.a.profile();
   }

   public final v a(String var, String var) {
      kotlin.d.b.l.b(var, "countryCode");
      kotlin.d.b.l.b(var, "postalCode");
      v var = this.a.searchAddress(var, var).d((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "signupApi.searchAddress(…ode).map { it.addresses }");
      return var;
   }
}
