package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.model.signup.SignUpProfile;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeParseException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0002J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "signupProfileManager", "Lco/uk/getmondo/signup/profile/SignupProfileManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "dateFormatter", "Lorg/threeten/bp/format/DateTimeFormatter;", "getDateFormatter", "()Lorg/threeten/bp/format/DateTimeFormatter;", "dateFormatter$delegate", "Lkotlin/Lazy;", "dateLength", "", "handleError", "", "throwable", "", "view", "parseDate", "Lorg/threeten/bp/LocalDate;", "date", "", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class n extends co.uk.getmondo.common.ui.b {
   // $FF: synthetic field
   static final kotlin.reflect.l[] c = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(n.class), "dateFormatter", "getDateFormatter()Lorg/threeten/bp/format/DateTimeFormatter;"))};
   private final kotlin.c d;
   private final int e;
   private final u f;
   private final u g;
   private final q h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.common.a j;

   public n(u var, u var, q var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "signupProfileManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.d = kotlin.d.a((kotlin.d.a.a)null.a);
      this.e = 10;
   }

   private final LocalDate a(String var) {
      LocalDate var;
      try {
         var = LocalDate.a((CharSequence)var, this.a());
      } catch (DateTimeParseException var) {
         var = null;
      }

      return var;
   }

   private final DateTimeFormatter a() {
      kotlin.c var = this.d;
      kotlin.reflect.l var = c[0];
      return (DateTimeFormatter)var.a();
   }

   private final void a(Throwable var, n.a var) {
      var.b(false);
      j var = (j)co.uk.getmondo.common.e.c.a(var, (co.uk.getmondo.common.e.f[])j.values());
      if(var != null) {
         switch(o.a[var.ordinal()]) {
         case 1:
            var.j();
            break;
         case 2:
            var.k();
            break;
         case 3:
            var.g();
         }
      } else if(!co.uk.getmondo.signup.i.a.a(var, (co.uk.getmondo.signup.i.a)var) && !this.i.a(var, (co.uk.getmondo.common.e.a.a)var)) {
         var.b(2131362198);
      }

   }

   public void a(final n.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.j.a(Impression.Companion.f());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.f();
         }
      }));
      kotlin.d.b.l.a(var, "view.onAddPreferredNameC…howPreferredNameField() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.h.a().b(this.g).a(this.f).d((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final k a(SignUpProfile var) {
            kotlin.d.b.l.b(var, "it");
            String var = var.a();
            String var;
            if(var.c()) {
               var = null;
            } else {
               var = var.b();
            }

            LocalDate var = var.d();
            String var;
            if(var != null) {
               var = var.a(n.this.a());
               if(var != null) {
                  return new k(var, var, var);
               }
            }

            var = "";
            return new k(var, var, var);
         }
      })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(k varx) {
            var.a(varx);
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = n.this.i;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
            n.a.a(var, (k)null, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var, "signupProfileManager.exi…      }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.n var = var.d().map((io.reactivex.c.h)null.a);
      io.reactivex.n var = var.c().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.a((CharSequence)var));
         }

         public final boolean a(CharSequence var) {
            kotlin.d.b.l.b(var, "dateText");
            boolean var;
            if(var.length() == n.this.e && co.uk.getmondo.common.c.c.a(n.this.a(var.toString()), 18L)) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      }));
      var = this.b;
      kotlin.d.b.l.a(var, "legalNameValid");
      kotlin.d.b.l.a(var, "dateOfBirthValid");
      var = co.uk.getmondo.common.j.f.a(var, var).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            boolean var;
            n.a var;
            label12: {
               Boolean var = (Boolean)varx.c();
               Boolean var = (Boolean)varx.d();
               var = var;
               kotlin.d.b.l.a(var, "legalNameValid");
               if(var.booleanValue()) {
                  kotlin.d.b.l.a(var, "dateOfBirthValid");
                  if(var.booleanValue()) {
                     var = true;
                     break label12;
                  }
               }

               var = false;
            }

            var.a(var);
         }
      }));
      kotlin.d.b.l.a(var, "combineLatest(legalNameV…hValid)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CharSequence varx) {
            if(varx.length() != n.this.e) {
               var.h();
            } else {
               LocalDate var = n.this.a(varx.toString());
               if(var == null) {
                  var.g();
               } else if(!co.uk.getmondo.common.c.c.a(var, 18L)) {
                  var.i();
               }
            }

         }
      }));
      kotlin.d.b.l.a(var, "view.onDateOfBirthTextCh…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.e().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(k var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!kotlin.h.j.a((CharSequence)var.a())) {
               var = true;
            } else {
               var = false;
            }

            boolean var;
            if(var) {
               if(!kotlin.h.j.a((CharSequence)var.c())) {
                  var = true;
               } else {
                  var = false;
               }

               if(var && co.uk.getmondo.common.c.c.a(n.this.a(var.c()), 18L)) {
                  var = true;
                  return var;
               }
            }

            var = false;
            return var;
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(k varx) {
            kotlin.d.b.l.b(varx, "it");
            LocalDate var = n.this.a(varx.c());
            if(var == null) {
               kotlin.d.b.l.a();
            }

            q var = n.this.h;
            String var = varx.a();
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            } else {
               var = kotlin.h.j.b((CharSequence)var).toString();
               String var = varx.b();
               if(var != null) {
                  if(var == null) {
                     throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                  }

                  var = kotlin.h.j.b((CharSequence)var).toString();
               } else {
                  var = null;
               }

               return co.uk.getmondo.common.j.f.a((io.reactivex.b)var.a(var, var, var).b(n.this.g).a(n.this.f).c((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b varx) {
                     var.b(true);
                  }
               })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable varx) {
                     n var = n.this;
                     kotlin.d.b.l.a(varx, "it");
                     var.a(varx, var);
                  }
               })), (Object)kotlin.n.a);
            }
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.l();
         }
      }));
      kotlin.d.b.l.a(var, "view.onNextClicked()\n   …itted()\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007H&J\b\u0010\r\u001a\u00020\u0005H&J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0010H&J\u0014\u0010\u0013\u001a\u00020\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\fH&J\b\u0010\u0015\u001a\u00020\u0005H&J\b\u0010\u0016\u001a\u00020\u0005H&J\b\u0010\u0017\u001a\u00020\u0005H&J\b\u0010\u0018\u001a\u00020\u0005H&J\b\u0010\u0019\u001a\u00020\u0005H&¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "clearDateOfBirthError", "", "onAddPreferredNameClicked", "Lio/reactivex/Observable;", "onDateOfBirthTextChange", "", "onLegalNameTextChange", "onNextClicked", "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;", "profileDetailsSubmitted", "setNextButtonEnabled", "enabled", "", "setNextButtonLoading", "loading", "showForm", "profileData", "showInvalidDateOfBirthError", "showLegalNameUnsupportedCharError", "showPreferredNameField", "showPreferredNameUnsupportedCharError", "showUnderAgeError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(k var);

      void a(boolean var);

      void b(boolean var);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();

      void k();

      void l();
   }

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      // $FF: synthetic method
      public static void a(n.a var, k var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showForm");
         } else {
            if((var & 1) != 0) {
               var = (k)null;
            }

            var.a(var);
         }
      }
   }
}
