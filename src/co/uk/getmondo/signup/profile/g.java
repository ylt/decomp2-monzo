package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "signupProfileManager", "Lco/uk/getmondo/signup/profile/SignupProfileManager;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final q f;
   private final co.uk.getmondo.common.a g;

   public g(u var, u var, co.uk.getmondo.common.e.a var, q var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "signupProfileManager");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   public void a(final g.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.Companion.y());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.f();
         }
      }));
      kotlin.d.b.l.a(var, "view.residencyClicked\n  …ibe { view.showUkOnly() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(q.a(g.this.f, (String)null, varx, 1, (Object)null).b(g.this.c).a(g.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.a(true);
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(List varx, Throwable var) {
                  var.a(false);
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  g var = g.this;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, var);
               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List varx) {
            var.g();
            if(varx.isEmpty()) {
               var.i();
               var.h();
            } else {
               g.a var = var;
               kotlin.d.b.l.a(varx, "it");
               var.a(varx);
            }

         }
      }));
      kotlin.d.b.l.a(var, "view.postcodeReady\n     …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Address varx) {
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "view.addressSelected\n   …AddressConfirmation(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            g.a.a(var, (Address)null, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var, "view.addressNotInListCli…enAddressConfirmation() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   public final void a(Throwable var, g.a var) {
      kotlin.d.b.l.b(var, "throwable");
      kotlin.d.b.l.b(var, "view");
      if(kotlin.h.j.a(co.uk.getmondo.common.e.c.a(var), "bad_request.bad_param.postal_code", false, 2, (Object)null)) {
         var.j();
      } else if(!co.uk.getmondo.signup.i.a.a(var, (co.uk.getmondo.signup.i.a)var) && !this.e.a(var, (co.uk.getmondo.common.e.a.a)var)) {
         var.b(2131362198);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0011\u001a\u00020\u0006H&J\u0014\u0010\u0012\u001a\u00020\u00062\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\nH&J\u0010\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0016H&J\b\u0010\u0019\u001a\u00020\u0006H&J\b\u0010\u001a\u001a\u00020\u0006H&J\u0016\u0010\u001b\u001a\u00020\u00062\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\n0\u001dH&J\b\u0010\u001e\u001a\u00020\u0006H&J\b\u0010\u001f\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\b¨\u0006 "},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "addressNotInListClicked", "Lio/reactivex/Observable;", "", "getAddressNotInListClicked", "()Lio/reactivex/Observable;", "addressSelected", "Lco/uk/getmondo/api/model/Address;", "getAddressSelected", "postcodeReady", "", "getPostcodeReady", "residencyClicked", "getResidencyClicked", "hideContinue", "openAddressConfirmation", "address", "setAddressLoading", "loading", "", "setContinueEnabled", "enabled", "showAddressNotFound", "showAddressNotInListButton", "showAddresses", "addresses", "", "showInvalidPostcodeError", "showUkOnly", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(Address var);

      void a(List var);

      void a(boolean var);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      // $FF: synthetic method
      public static void a(g.a var, Address var, int var, Object var) {
         if(var != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: openAddressConfirmation");
         } else {
            if((var & 1) != 0) {
               var = (Address)null;
            }

            var.a(var);
         }
      }
   }
}
