package co.uk.getmondo.signup.profile;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class o {
   // $FF: synthetic field
   public static final int[] a = new int[j.values().length];

   static {
      a[j.a.ordinal()] = 1;
      a[j.b.ordinal()] = 2;
      a[j.c.ordinal()] = 3;
   }
}
