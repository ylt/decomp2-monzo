package co.uk.getmondo.signup.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.profile.address.AddressSelectionView;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0005\u0018\u0000 C2\u00020\u00012\u00020\u0002:\u0002CDB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001f\u001a\u00020\bH\u0016J\"\u0010 \u001a\u00020\b2\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020\u00052\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0010\u0010%\u001a\u00020\b2\u0006\u0010&\u001a\u00020'H\u0016J\u0012\u0010(\u001a\u00020\b2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J$\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\b\u0010/\u001a\u0004\u0018\u0001002\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u00101\u001a\u00020\bH\u0016J\u001a\u00102\u001a\u00020\b2\u0006\u00103\u001a\u00020,2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0012\u00104\u001a\u00020\b2\b\u00105\u001a\u0004\u0018\u00010\fH\u0016J\b\u00106\u001a\u00020\bH\u0016J\u0010\u00107\u001a\u00020\b2\u0006\u00108\u001a\u000209H\u0016J\u0010\u0010:\u001a\u00020\b2\u0006\u0010;\u001a\u000209H\u0016J\b\u0010<\u001a\u00020\bH\u0016J\b\u0010=\u001a\u00020\bH\u0016J\u0016\u0010>\u001a\u00020\b2\f\u0010?\u001a\b\u0012\u0004\u0012\u00020\f0@H\u0016J\b\u0010A\u001a\u00020\bH\u0016J\b\u0010B\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\nR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\nR\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\n¨\u0006E"},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;", "()V", "REQUEST_CONFIRM_ADDRESS", "", "addressNotInListClicked", "Lio/reactivex/Observable;", "", "getAddressNotInListClicked", "()Lio/reactivex/Observable;", "addressSelected", "Lco/uk/getmondo/api/model/Address;", "getAddressSelected", "entryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "entryPoint$delegate", "Lkotlin/Lazy;", "postcodeReady", "", "getPostcodeReady", "presenter", "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;)V", "residencyClicked", "getResidencyClicked", "hideContinue", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "openAddressConfirmation", "address", "reloadSignupStatus", "setAddressLoading", "loading", "", "setContinueEnabled", "enabled", "showAddressNotFound", "showAddressNotInListButton", "showAddresses", "addresses", "", "showInvalidPostcodeError", "showUkOnly", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(e.class), "entryPoint", "getEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"))};
   public static final e.a d = new e.a((kotlin.d.b.i)null);
   public g c;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.signup.j b() {
         Serializable var = e.this.getArguments().getSerializable("KEY_SIGNUP_ENTRY_POINT");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.SignupEntryPoint");
         } else {
            return (co.uk.getmondo.signup.j)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final int f = 1;
   private HashMap g;

   private final co.uk.getmondo.signup.j l() {
      kotlin.c var = this.e;
      kotlin.reflect.l var = a[0];
      return (co.uk.getmondo.signup.j)var.a();
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.g.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public io.reactivex.n a() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).g();
   }

   public void a(Address var) {
      AddressConfirmationActivity.a var = AddressConfirmationActivity.g;
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      this.startActivityForResult(var.a((Context)var, var, this.l()), this.f);
   }

   public void a(List var) {
      kotlin.d.b.l.b(var, "addresses");
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).a(var);
   }

   public void a(boolean var) {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).setAddressLoading(var);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public io.reactivex.n c() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).i();
   }

   public io.reactivex.n d() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).j();
   }

   public io.reactivex.n e() {
      return ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).k();
   }

   public void f() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362599), this.getString(2131362598), false).show(this.getFragmentManager(), co.uk.getmondo.common.d.a.class.getName());
   }

   public void g() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).setPrimaryButtonVisible(false);
   }

   public void h() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).c();
   }

   public void i() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).l();
   }

   public void j() {
      ((AddressSelectionView)this.a(co.uk.getmondo.c.a.signupAddressSelectionView)).m();
   }

   public void k() {
      if(this.g != null) {
         this.g.clear();
      }

   }

   public void onActivityResult(int var, int var, Intent var) {
      if(var == this.f) {
         if(var == -1) {
            android.support.v4.app.j var = this.getActivity();
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.profile.ProfileAddressFragment.StepListener");
            }

            ((e.b)var).f_();
         }
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof e.b)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + e.b.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must extend " + co.uk.getmondo.signup.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034275, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…ddress, container, false)");
      return var;
   }

   public void onDestroyView() {
      g var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.k();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      g var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressFragment$Companion;", "", "()V", "newInstance", "Lco/uk/getmondo/signup/profile/ProfileAddressFragment;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final e a(co.uk.getmondo.signup.j var) {
         kotlin.d.b.l.b(var, "signupEntryPoint");
         e var = new e();
         Bundle var = new Bundle();
         var.putSerializable("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         var.setArguments(var);
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressFragment$StepListener;", "", "onProfileReady", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void f_();
   }
}
