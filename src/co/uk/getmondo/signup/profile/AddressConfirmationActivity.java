package co.uk.getmondo.signup.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.profile.address.AddressInputView;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.w;
import kotlin.d.b.y;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 !2\u00020\u00012\u00020\u0002:\u0001!B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0018\u001a\u00020\u0019H\u0016J\u0012\u0010\u001a\u001a\u00020\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\rH\u0016J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020\rH\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u000fR\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017¨\u0006\""},
   d2 = {"Lco/uk/getmondo/signup/profile/AddressConfirmationActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter$View;", "()V", "address", "Lco/uk/getmondo/api/model/Address;", "kotlin.jvm.PlatformType", "getAddress", "()Lco/uk/getmondo/api/model/Address;", "address$delegate", "Lkotlin/Lazy;", "addressInputValid", "Lio/reactivex/Observable;", "", "getAddressInputValid", "()Lio/reactivex/Observable;", "confirmAddressClicked", "getConfirmAddressClicked", "presenter", "Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/profile/AddressConfirmationPresenter;)V", "finishProfileStage", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setConfirmButtonEnabled", "enabled", "setConfirmButtonLoading", "loading", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressConfirmationActivity extends co.uk.getmondo.signup.a implements b.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(AddressConfirmationActivity.class), "address", "getAddress()Lco/uk/getmondo/api/model/Address;"))};
   public static final AddressConfirmationActivity.a g = new AddressConfirmationActivity.a((kotlin.d.b.i)null);
   public b b;
   private final kotlin.c h = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final Address b() {
         return (Address)AddressConfirmationActivity.this.getIntent().getParcelableExtra("KEY_ADDRESS");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap i;

   private final Address f() {
      kotlin.c var = this.h;
      kotlin.reflect.l var = a[0];
      return (Address)var.a();
   }

   public View a(int var) {
      if(this.i == null) {
         this.i = new HashMap();
      }

      View var = (View)this.i.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.i.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void a(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.signupAddressConfirmButton)).setEnabled(var);
   }

   public void b(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.signupAddressConfirmButton)).setLoading(var);
   }

   public io.reactivex.n c() {
      return ((AddressInputView)this.a(co.uk.getmondo.c.a.signupAddressInputView)).getAddressInputValid();
   }

   public io.reactivex.n d() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.signupAddressConfirmButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = var.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final Address a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            return ((AddressInputView)AddressConfirmationActivity.this.a(co.uk.getmondo.c.a.signupAddressInputView)).getAddress();
         }
      }));
      kotlin.d.b.l.a(var, "signupAddressConfirmButt…ddressInputView.address }");
      return var;
   }

   public void e() {
      this.setResult(-1);
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034142);
      this.l().a(this);
      ((AddressInputView)this.a(co.uk.getmondo.c.a.signupAddressInputView)).setPreFilledAddress(this.f());
      b var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((b.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/signup/profile/AddressConfirmationActivity$Companion;", "", "()V", "KEY_ADDRESS", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "address", "Lco/uk/getmondo/api/model/Address;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, Address var, co.uk.getmondo.signup.j var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, AddressConfirmationActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var).putExtra("KEY_ADDRESS", (Parcelable)var);
         kotlin.d.b.l.a(var, "Intent(context, AddressC…tra(KEY_ADDRESS, address)");
         return var;
      }
   }
}
