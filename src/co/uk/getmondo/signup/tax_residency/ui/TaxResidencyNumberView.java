package co.uk.getmondo.signup.tax_residency.ui;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.k.e;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014J\u000e\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010\f\u001a\u00020\b2\b\u0010\r\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u000b¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/ui/TaxResidencyNumberView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "onFinishInflate", "", "setAltTinType", "altTinType", "", "setFlag", "countryCode", "setLabel", "tinType", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TaxResidencyNumberView extends ConstraintLayout {
   private HashMap c;

   public TaxResidencyNumberView(Context var, AttributeSet var) {
      super(var, var);
   }

   public View b(int var) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var = (View)this.c.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.c.put(Integer.valueOf(var), var);
      }

      return var;
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      View.inflate(this.getContext(), 2131034438, (ViewGroup)this);
   }

   public final void setAltTinType(String var) {
      l.b(var, "altTinType");
      ((Button)this.b(c.a.altTinTypeText)).setText((CharSequence)this.getContext().getString(2131362861, new Object[]{var}));
      Button var = (Button)this.b(c.a.altTinTypeText);
      boolean var;
      if(j.a((CharSequence)var)) {
         var = true;
      } else {
         var = false;
      }

      byte var;
      if(!var) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }

   public final void setFlag(String var) {
      ((TextView)this.b(c.a.flagTextView)).setText((CharSequence)e.a(var));
   }

   public final void setLabel(String var) {
      l.b(var, "tinType");
      ((TextInputLayout)this.b(c.a.taxNumberInputLayout)).setHint((CharSequence)var);
   }
}
