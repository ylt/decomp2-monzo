package co.uk.getmondo.signup.tax_residency.a;

import co.uk.getmondo.api.TaxResidencyApi;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.api.model.tax_residency.TaxResidencyInfo;
import io.reactivex.v;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\"\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\nH\u0002J(\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006J$\u0010\u0014\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e0\u00062\u0006\u0010\u0011\u001a\u00020\u0012J\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006J\u001e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001aJ\u0006\u0010\u001c\u001a\u00020\u0017J\u001c\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u0011\u001a\u00020\u00122\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\r0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "", "api", "Lco/uk/getmondo/api/TaxResidencyApi;", "(Lco/uk/getmondo/api/TaxResidencyApi;)V", "checkIsLastJurisdiction", "Lio/reactivex/Single;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "jurisdictions", "", "generateTaxCountriesList", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "Lkotlin/collections/ArrayList;", "taxResidency", "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;", "isUsTaxResident", "", "getStatus", "getTaxCountries", "nextJurisdictionOrSubmit", "reportTin", "Lio/reactivex/Completable;", "jurisdiction", "tinId", "", "tinValue", "submitUkTaxResidentOnly", "updateSelfCertification", "countries", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   public static final c.a a = new c.a((i)null);
   private final TaxResidencyApi b;

   public c(TaxResidencyApi var) {
      l.b(var, "api");
      super();
      this.b = var;
   }

   private final v a(List var) {
      v var;
      if(var.isEmpty()) {
         var = this.b.submit().a((Object)com.c.b.b.c());
         l.a(var, "api.submit()\n           …Default(Optional.empty())");
      } else {
         var = v.a((Object)com.c.b.b.b(var.get(0)));
         l.a(var, "Single.just(Optional.optionOf(jurisdictions[0]))");
      }

      return var;
   }

   private final ArrayList a(TaxResidencyInfo var, boolean var) {
      ArrayList var = new ArrayList();
      Iterator var = co.uk.getmondo.d.i.Companion.b().iterator();
      int var = 0;

      while(var.hasNext()) {
         co.uk.getmondo.d.i var = (co.uk.getmondo.d.i)var.next();
         String var = var.f();
         String var = var.g();
         String var = var.h();
         boolean var;
         if(!l.a(var, co.uk.getmondo.d.i.UNITED_KINGDOM.e()) && (!l.a(var, co.uk.getmondo.d.i.UNITED_STATES.e()) || !var)) {
            var = false;
         } else {
            var = true;
         }

         List var = var.d();
         boolean var;
         if(var != null) {
            Iterable var = (Iterable)var;
            if(var instanceof Collection && ((Collection)var).isEmpty()) {
               var = false;
            } else {
               Iterator var = var.iterator();

               while(true) {
                  if(!var.hasNext()) {
                     var = false;
                     break;
                  }

                  if(l.a(((Jurisdiction)var.next()).e(), var)) {
                     var = true;
                     break;
                  }
               }
            }
         } else {
            var = false;
         }

         a var = new a(var, var, var, var, var);
         if(var.c().contains(var)) {
            var.add(var, var);
            ++var;
         } else {
            var.add(var);
         }
      }

      return var;
   }

   public final io.reactivex.b a(Jurisdiction var, String var, String var) {
      l.b(var, "jurisdiction");
      l.b(var, "tinId");
      l.b(var, "tinValue");
      io.reactivex.b var;
      if(l.a(var, "NO_TIN") && var.h()) {
         var = this.b.noTin(var.e());
      } else {
         var = this.b.updateTin(var.e(), var, var);
      }

      return var;
   }

   public final io.reactivex.b a(boolean var, List var) {
      l.b(var, "countries");
      Iterable var = (Iterable)var;
      Collection var = (Collection)(new ArrayList(m.a(var, 10)));
      Iterator var = var.iterator();

      while(var.hasNext()) {
         var.add(((a)var.next()).c());
      }

      var = (Collection)((List)var);
      Object[] var = var.toArray(new String[var.size()]);
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
      } else {
         String[] var = (String[])var;
         return this.b.updateSelfCertification(false, var, var);
      }
   }

   public final v a() {
      return this.b.status();
   }

   public final v a(final boolean var) {
      v var = this.a().d((h)(new h() {
         public final ArrayList a(TaxResidencyInfo varx) {
            l.b(varx, "taxResidencyInfo");
            return c.this.a(varx, var);
         }
      }));
      l.a(var, "getStatus()\n            …yInfo, isUsTaxResident) }");
      return var;
   }

   public final v b() {
      v var = this.a().d((h)null.a).a((h)(new h() {
         public final v a(List var) {
            l.b(var, "it");
            return c.this.a(var);
         }
      }));
      l.a(var, "getStatus()\n            …kIsLastJurisdiction(it) }");
      return var;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var = this.b.updateSelfCertification(true, false, (String[])((Object[])(new String[]{co.uk.getmondo.d.i.UNITED_KINGDOM.e()}))).b((io.reactivex.d)this.b.submit());
      l.a(var, "api.updateSelfCertificat…   .andThen(api.submit())");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager$Companion;", "", "()V", "NO_TIN", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }
   }
}
