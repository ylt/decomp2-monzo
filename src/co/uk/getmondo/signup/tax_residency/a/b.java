package co.uk.getmondo.signup.tax_residency.a;

import co.uk.getmondo.common.e.f;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\b¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getPrefix", "()Ljava/lang/String;", "BAD_TIN_VALUE", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum b implements f {
   a;

   private final String c;

   static {
      b var = new b("BAD_TIN_VALUE", 0, "bad_request.bad_param.value");
      a = var;
   }

   protected b(String var) {
      l.b(var, "prefix");
      super(var, var);
      this.c = var;
   }

   public String a() {
      return this.c;
   }
}
