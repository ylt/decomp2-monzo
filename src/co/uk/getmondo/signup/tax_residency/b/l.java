package co.uk.getmondo.signup.tax_residency.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.signup.tax_residency.ui.TaxResidencyNumberView;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 @2\u00020\u00012\u00020\u0002:\u0002@AB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001d\u001a\u00020\fH\u0016J\u0010\u0010\u001e\u001a\u00020\f2\u0006\u0010\u001f\u001a\u00020\u0005H\u0016J\b\u0010 \u001a\u00020\fH\u0016J\b\u0010!\u001a\u00020\fH\u0016J\b\u0010\"\u001a\u00020\fH\u0016J\u0010\u0010#\u001a\u00020\f2\u0006\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020\f2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J&\u0010)\u001a\u0004\u0018\u00010*2\u0006\u0010+\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010/\u001a\u00020\fH\u0016J\u001c\u00100\u001a\u00020\f2\b\u00101\u001a\u0004\u0018\u00010*2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\u0010\u00102\u001a\u00020\f2\u0006\u00103\u001a\u000204H\u0016J\u0010\u00105\u001a\u00020\f2\u0006\u00106\u001a\u000204H\u0016J\u0010\u00107\u001a\u00020\f2\u0006\u00106\u001a\u000204H\u0016J\u0010\u00108\u001a\u00020\f2\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010;\u001a\u00020\f2\u0006\u0010<\u001a\u00020:H\u0016J\u0010\u0010=\u001a\u00020\f2\u0006\u0010>\u001a\u000204H\u0016J\u0010\u0010?\u001a\u00020\f2\u0006\u0010<\u001a\u00020:H\u0016R\u0014\u0010\u0004\u001a\u00020\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u000eR\u001a\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u000eR\u001a\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u000b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u000eR\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c¨\u0006B"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;", "()V", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "getJurisdiction", "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "listener", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$StepListener;", "onAltTinClicked", "Lio/reactivex/Observable;", "", "getOnAltTinClicked", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onNoTinClicked", "getOnNoTinClicked", "onTinChanges", "", "getOnTinChanges", "presenter", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;)V", "completeStage", "goToNextStep", "nextJurisdiction", "hideAltTinType", "hideInvalidTinError", "hideNoTin", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "setContinueButtonEnabled", "enabled", "", "setContinueButtonLoading", "loading", "setNoTinButtonLoading", "showAltTinType", "altTinType", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "showInvalidTinError", "tinType", "showNoTin", "isPlural", "showTinType", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l extends co.uk.getmondo.common.f.a implements n.a {
   public static final l.a c = new l.a((kotlin.d.b.i)null);
   public n a;
   private l.b d;
   private HashMap e;

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.e.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public Jurisdiction a() {
      Parcelable var = this.getArguments().getParcelable("ARG_JURISDICTION");
      kotlin.d.b.l.a(var, "arguments.getParcelable(ARG_JURISDICTION)");
      return (Jurisdiction)var;
   }

   public void a(Jurisdiction.TinType var) {
      kotlin.d.b.l.b(var, "tinType");
      ((TaxResidencyNumberView)this.a(co.uk.getmondo.c.a.taxNumberView)).setLabel(var.b());
      ((TextInputEditText)((TaxResidencyNumberView)this.a(co.uk.getmondo.c.a.taxNumberView)).b(co.uk.getmondo.c.a.taxNumberEditText)).setText((CharSequence)"");
   }

   public void a(Jurisdiction var) {
      kotlin.d.b.l.b(var, "nextJurisdiction");
      l.b var = this.d;
      if(var == null) {
         kotlin.d.b.l.b("listener");
      }

      var.b(var);
   }

   public void a(boolean var) {
      byte var;
      if(var) {
         var = 2;
      } else {
         var = 1;
      }

      ((ProgressButton)this.a(co.uk.getmondo.c.a.noTinButton)).setText((CharSequence)this.getResources().getQuantityString(2131886080, var));
      ((ProgressButton)this.a(co.uk.getmondo.c.a.noTinButton)).setVisibility(0);
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.altTinTypeText)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void b(Jurisdiction.TinType var) {
      kotlin.d.b.l.b(var, "altTinType");
      ((TaxResidencyNumberView)this.a(co.uk.getmondo.c.a.taxNumberView)).setAltTinType(var.b());
   }

   public void b(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setEnabled(var);
   }

   public io.reactivex.n c() {
      com.b.a.a var = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.taxNumberEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      return (io.reactivex.n)var;
   }

   public void c(Jurisdiction.TinType var) {
      kotlin.d.b.l.b(var, "tinType");
      TextInputLayout var = (TextInputLayout)this.a(co.uk.getmondo.c.a.taxNumberInputLayout);
      String var = var.b();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
      } else {
         var = var.toLowerCase();
         kotlin.d.b.l.a(var, "(this as java.lang.String).toLowerCase()");
         var.setError((CharSequence)this.getString(2131362756, new Object[]{var, var.c()}));
      }
   }

   public void c(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(var);
   }

   public io.reactivex.n d() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      io.reactivex.n var = com.b.a.d.e.a((TextInputEditText)this.a(co.uk.getmondo.c.a.taxNumberEditText));
      kotlin.d.b.l.a(var, "RxTextView.editorActions(this)");
      var = var.mergeWith((io.reactivex.r)var.map((io.reactivex.c.h)null.a)).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var) {
            kotlin.d.b.l.b(var, "it");
            return ((TextInputEditText)((TaxResidencyNumberView)l.this.a(co.uk.getmondo.c.a.taxNumberView)).b(co.uk.getmondo.c.a.taxNumberEditText)).getText().toString();
         }
      }));
      kotlin.d.b.l.a(var, "continueButton.clicks()\n…ditText.text.toString() }");
      return var;
   }

   public void d(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.noTinButton)).setLoading(var);
   }

   public io.reactivex.n e() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.noTinButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void f() {
      ((Button)((TaxResidencyNumberView)this.a(co.uk.getmondo.c.a.taxNumberView)).b(co.uk.getmondo.c.a.altTinTypeText)).setVisibility(8);
   }

   public void g() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.noTinButton)).setVisibility(8);
   }

   public void h() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.taxNumberInputLayout)).setError((CharSequence)"");
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.taxNumberInputLayout)).setErrorEnabled(false);
   }

   public void i() {
      l.b var = this.d;
      if(var == null) {
         kotlin.d.b.l.b("listener");
      }

      var.j();
   }

   public void j() {
      if(this.e != null) {
         this.e.clear();
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(var instanceof l.b) {
         this.d = (l.b)var;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement TaxResidencyTinEntryFragment.StepListener"));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      return var.inflate(2131034286, var, false);
   }

   public void onDestroyView() {
      super.onDestroyView();
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      this.j();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      ((TaxResidencyNumberView)this.a(co.uk.getmondo.c.a.taxNumberView)).setFlag(this.a().a());
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((n.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$Companion;", "", "()V", "ARG_JURISDICTION", "", "newInstance", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment;", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final l a(Jurisdiction var) {
         kotlin.d.b.l.b(var, "jurisdiction");
         l var = new l();
         Bundle var = new Bundle();
         var.putParcelable("ARG_JURISDICTION", (Parcelable)var);
         var.setArguments(var);
         return var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyStageListener;", "onTinEntryComplete", "", "nextJurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b extends co.uk.getmondo.signup.tax_residency.f {
      void b(Jurisdiction var);
   }
}
