package co.uk.getmondo.signup.tax_residency.b;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u001a\u0010\u0014\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0002H\u0016J \u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "currentTinType", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "displayJurisdiction", "", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "view", "getAltTinType", "tinType", "register", "updateTinTypes", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class n extends co.uk.getmondo.common.ui.b {
   private Jurisdiction.TinType c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.signup.tax_residency.a.c f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.common.a h;

   public n(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.signup.tax_residency.a.c var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "taxResidencyManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   private final Jurisdiction.TinType a(Jurisdiction.TinType var, Jurisdiction var) {
      if(kotlin.d.b.l.a(var, var.c())) {
         var = var.d();
      } else {
         var = var.c();
      }

      return var;
   }

   // $FF: synthetic method
   public static final Jurisdiction.TinType a(n var) {
      Jurisdiction.TinType var = var.c;
      if(var == null) {
         kotlin.d.b.l.b("currentTinType");
      }

      return var;
   }

   private final void a(Jurisdiction.TinType var, Jurisdiction var, n.a var) {
      var.a(var);
      var = this.a(var, var);
      if(var != null) {
         var.b(var);
      } else {
         var.f();
      }

   }

   private final void a(Jurisdiction var, n.a var) {
      this.c = var.c();
      Jurisdiction.TinType var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("currentTinType");
      }

      this.a(var, var, var);
      if(var.h()) {
         var.a(var.b());
      } else {
         var.g();
      }

   }

   public void a(final n.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.Companion.aG());
      final Jurisdiction var = var.a();
      this.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.c();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CharSequence varx) {
            boolean var = true;
            n.a var = var;
            boolean var;
            if(varx != null && !kotlin.h.j.a(varx)) {
               var = false;
            } else {
               var = true;
            }

            if(var) {
               var = false;
            }

            var.b(var);
            var.h();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new o(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onTinChanges\n      …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.n var = var.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            n var = n.this;
            Jurisdiction.TinType var = n.this.a(n.a(n.this), var);
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var.c = var;
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            n.this.a(n.a(n.this), var, var);
         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new o(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onAltTinClicked\n   …tion, view) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.d().mergeWith((io.reactivex.r)var.e().map((io.reactivex.c.h)null.a)).filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(final String varx) {
            kotlin.d.b.l.b(varx, "tinValue");
            return co.uk.getmondo.common.j.f.a(n.this.f.a(var, n.a(n.this).a(), varx).a((z)n.this.f.b()).b(n.this.d).a(n.this.e).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varxx) {
                  if(kotlin.d.b.l.a(varx, "NO_TIN") ^ true) {
                     var.c(true);
                  } else {
                     var.d(true);
                  }

               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.c(false);
                  var.d(false);
                  if(varx instanceof ApiException) {
                     co.uk.getmondo.common.e.f[] var = (co.uk.getmondo.common.e.f[])co.uk.getmondo.signup.tax_residency.a.b.values();
                     co.uk.getmondo.api.model.b var = ((ApiException)varx).e();
                     String varx;
                     if(var != null) {
                        varx = var.a();
                     } else {
                        varx = null;
                     }

                     if(kotlin.d.b.l.a((co.uk.getmondo.signup.tax_residency.a.b)co.uk.getmondo.common.e.d.a(var, varx), co.uk.getmondo.signup.tax_residency.a.b.a)) {
                        var.c(n.a(n.this));
                        return;
                     }
                  }

                  co.uk.getmondo.common.e.a var = n.this.g;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })));
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b varx) {
            if(varx.d()) {
               var.i();
            } else {
               n.a var = var;
               Object var = varx.a();
               kotlin.d.b.l.a(var, "nextJurisdiction.get()");
               var.a((Jurisdiction)var);
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new o(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onContinueClicked\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0014\u001a\u00020\tH&J\u0010\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0004H&J\b\u0010\u0017\u001a\u00020\tH&J\b\u0010\u0018\u001a\u00020\tH&J\b\u0010\u0019\u001a\u00020\tH&J\u0010\u0010\u001a\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010\u001f\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010 \u001a\u00020\t2\u0006\u0010!\u001a\u00020\"H&J\u0010\u0010#\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&J\u0010\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u001cH&J\u0010\u0010'\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000bR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000bR\u0018\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u000b¨\u0006("},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "getJurisdiction", "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "onAltTinClicked", "Lio/reactivex/Observable;", "", "getOnAltTinClicked", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onNoTinClicked", "getOnNoTinClicked", "onTinChanges", "", "getOnTinChanges", "completeStage", "goToNextStep", "nextJurisdiction", "hideAltTinType", "hideInvalidTinError", "hideNoTin", "setContinueButtonEnabled", "enabled", "", "setContinueButtonLoading", "loading", "setNoTinButtonLoading", "showAltTinType", "altTinType", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "showInvalidTinError", "tinType", "showNoTin", "isPlural", "showTinType", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      Jurisdiction a();

      void a(Jurisdiction.TinType var);

      void a(Jurisdiction var);

      void a(boolean var);

      io.reactivex.n b();

      void b(Jurisdiction.TinType var);

      void b(boolean var);

      io.reactivex.n c();

      void c(Jurisdiction.TinType var);

      void c(boolean var);

      io.reactivex.n d();

      void d(boolean var);

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();
   }
}
