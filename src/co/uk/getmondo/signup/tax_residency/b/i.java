package co.uk.getmondo.signup.tax_residency.b;

import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.signup.tax_residency.a.c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public i(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.signup.tax_residency.a.c var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "taxResidencyManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   public void a(final i.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.Companion.aD());
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)i.this.e.c().b(i.this.c).a(i.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.a(true);
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.a(false);
                  co.uk.getmondo.common.e.a var = i.this.f;
                  kotlin.d.b.l.a(varx, "it");
                  if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362198);
                  }

               }
            })), (Object)kotlin.n.a);
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.d();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new j(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onUkOnlyClicked\n   …leteStage() }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.b();
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.c();
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new j(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "view.onOtherCountriesCli…oNextStep() }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\n\u001a\u00020\u0005H&J\b\u0010\u000b\u001a\u00020\u0005H&J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onOtherCountriesClicked", "Lio/reactivex/Observable;", "", "getOnOtherCountriesClicked", "()Lio/reactivex/Observable;", "onUkOnlyClicked", "getOnUkOnlyClicked", "completeStage", "goToNextStep", "setButtonLoading", "loading", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(boolean var);

      io.reactivex.n b();

      void c();

      void d();
   }
}
