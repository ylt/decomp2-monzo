package co.uk.getmondo.signup.tax_residency.b;

import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0004\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001fB-\u0012\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007¢\u0006\u0002\u0010\tJ\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u001c\u0010\u0014\u001a\u00020\b2\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u001c\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0011H\u0016J\u0014\u0010\u001a\u001a\u00020\b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00050\u001cJ\u000e\u0010\u001d\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\u0005R(\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006 "},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter$TaxCountryViewHolder;", "taxCountries", "", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "clickListener", "Lkotlin/Function1;", "", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "setClickListener", "(Lkotlin/jvm/functions/Function1;)V", "getTaxCountries", "()Ljava/util/List;", "getItemCount", "", "getItemViewType", "position", "onBindViewHolder", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setCountries", "countries", "", "updateCountry", "taxCountry", "TaxCountryViewHolder", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends android.support.v7.widget.RecyclerView.a {
   private final List a;
   private kotlin.d.a.b b;

   public a() {
      this((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
   }

   public a(List var, kotlin.d.a.b var) {
      kotlin.d.b.l.b(var, "taxCountries");
      super();
      this.a = var;
      this.b = var;
   }

   // $FF: synthetic method
   public a(List var, kotlin.d.a.b var, int var, kotlin.d.b.i var) {
      if((var & 1) != 0) {
         var = (List)(new ArrayList());
      }

      if((var & 2) != 0) {
         var = (kotlin.d.a.b)null;
      }

      this(var, var);
   }

   public a.a a(ViewGroup var, int var) {
      kotlin.d.b.l.b(var, "parent");
      View var = LayoutInflater.from(var.getContext()).inflate(var, var, false);
      kotlin.d.b.l.a(var, "view");
      return new a.a(var);
   }

   public final List a() {
      return this.a;
   }

   public final void a(co.uk.getmondo.signup.tax_residency.a.a var) {
      kotlin.d.b.l.b(var, "taxCountry");
      Iterator var = this.a.iterator();
      int var = 0;

      while(true) {
         if(!var.hasNext()) {
            var = -1;
            break;
         }

         if(kotlin.d.b.l.a(((co.uk.getmondo.signup.tax_residency.a.a)var.next()).b(), var.b())) {
            break;
         }

         ++var;
      }

      this.a.set(var, var);
      this.notifyItemChanged(var);
   }

   public void a(a.a var, int var) {
      kotlin.d.b.l.b(var, "holder");
      var.a((co.uk.getmondo.signup.tax_residency.a.a)this.a.get(var));
   }

   public final void a(List var) {
      kotlin.d.b.l.b(var, "countries");
      this.a.clear();
      this.a.addAll((Collection)var);
      this.notifyDataSetChanged();
   }

   public final void a(kotlin.d.a.b var) {
      this.b = var;
   }

   public final kotlin.d.a.b b() {
      return this.b;
   }

   public int getItemCount() {
      return this.a.size();
   }

   public int getItemViewType(int var) {
      if(((co.uk.getmondo.signup.tax_residency.a.a)this.a.get(var)).d()) {
         var = 2131034379;
      } else {
         var = 2131034369;
      }

      return var;
   }

   // $FF: synthetic method
   public void onBindViewHolder(w var, int var) {
      this.a((a.a)var, var);
   }

   // $FF: synthetic method
   public w onCreateViewHolder(ViewGroup var, int var) {
      return (w)this.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter$TaxCountryViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter;Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "country", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class a extends w {
      private final View b;

      public a(View var) {
         kotlin.d.b.l.b(var, "view");
         super(var);
         this.b = var;
         this.b.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               kotlin.d.a.b var = a.this.b();
               if(var != null) {
                  kotlin.n var = (kotlin.n)var.a(a.this.a().get(a.this.getAdapterPosition()));
               }

            }
         }));
      }

      public final void a(co.uk.getmondo.signup.tax_residency.a.a var) {
         boolean var = false;
         kotlin.d.b.l.b(var, "country");
         ((TextView)this.b.findViewById(co.uk.getmondo.c.a.countryNameTextView)).setText((CharSequence)var.a());
         int var;
         if(var.e()) {
            var = 2130837888;
         } else {
            var = 0;
         }

         ((TextView)this.b.findViewById(co.uk.getmondo.c.a.countryNameTextView)).setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, var, 0);
         View var = this.b;
         if(!var.d()) {
            var = true;
         }

         var.setEnabled(var);
      }
   }
}
