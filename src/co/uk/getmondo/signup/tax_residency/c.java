package co.uk.getmondo.signup.tax_residency;

import co.uk.getmondo.api.model.tax_residency.TaxResidencyInfo;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class c {
   // $FF: synthetic field
   public static final int[] a = new int[TaxResidencyInfo.Status.values().length];

   static {
      a[TaxResidencyInfo.Status.NOT_STARTED.ordinal()] = 1;
      a[TaxResidencyInfo.Status.IN_PROGRESS.ordinal()] = 2;
      a[TaxResidencyInfo.Status.SUBMITTED.ordinal()] = 3;
   }
}
