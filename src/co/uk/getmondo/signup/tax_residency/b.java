package co.uk.getmondo.signup.tax_residency;

import co.uk.getmondo.api.model.tax_residency.TaxResidencyInfo;
import co.uk.getmondo.signup.i;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B+\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0010\u0010\u0010\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "handleStatusError", "", "it", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.signup.tax_residency.a.c e;
   private final co.uk.getmondo.common.e.a f;

   public b(u var, u var, co.uk.getmondo.signup.tax_residency.a.c var, co.uk.getmondo.common.e.a var) {
      l.b(var, "ioScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "taxResidencyManager");
      l.b(var, "apiErrorHandler");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   private final void a(Throwable var, final b.a var) {
      if(!i.a.a(var, (i.a)var) && !this.f.a(var, (co.uk.getmondo.common.e.a.a)var, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var) {
            this.a(((Number)var).intValue());
            return n.a;
         }

         public final void a(int var) {
            var.d(var);
         }
      }))) {
         var.b(2131362198);
      }

   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = var.c().startWith((Object)n.a).flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(n varx) {
            l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(b.this.e.a().b(b.this.c).a(b.this.d).b((g)(new g() {
               public final void a(io.reactivex.b.b varx) {
                  var.d();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(TaxResidencyInfo varx, Throwable var) {
                  var.e();
               }
            })).d((g)(new g() {
               public final void a(Throwable varx) {
                  b var = b.this;
                  l.a(varx, "it");
                  var.a(varx, var);
               }
            })));
         }
      }));
      g var = (g)(new g() {
         public final void a(TaxResidencyInfo varx) {
            TaxResidencyInfo.Status var = varx.a();
            switch(c.a[var.ordinal()]) {
            case 1:
            case 2:
               if(varx.b()) {
                  var.f();
               } else {
                  var.g();
               }
               break;
            case 3:
               var.h();
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new d(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (g)var);
      l.a(var, "view.onRetryClicked\n    …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\t\u001a\u00020\u0006H&J\b\u0010\n\u001a\u00020\u0006H&J\u0012\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0006H&J\b\u0010\u000f\u001a\u00020\u0006H&J\b\u0010\u0010\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onRetryClicked", "Lio/reactivex/Observable;", "", "getOnRetryClicked", "()Lio/reactivex/Observable;", "finishStage", "hideLoading", "showFullScreenError", "errorMessage", "", "showLoading", "showSummaryScreen", "showUsTaxResidentScreen", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, i.a {
      io.reactivex.n c();

      void d();

      void d(int var);

      void e();

      void f();

      void g();

      void h();
   }
}
