package co.uk.getmondo.signup.tax_residency;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.t;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.signup.j;
import co.uk.getmondo.signup.tax_residency.b.g;
import co.uk.getmondo.signup.tax_residency.b.l;
import co.uk.getmondo.signup.tax_residency.b.q;
import io.reactivex.n;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u0000 02\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u00010B\u0005¢\u0006\u0002\u0010\u0007J\b\u0010\u0013\u001a\u00020\nH\u0016J\b\u0010\u0014\u001a\u00020\nH\u0016J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\n2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\b\u0010\u001b\u001a\u00020\nH\u0014J\b\u0010\u001c\u001a\u00020\nH\u0016J\b\u0010\u001d\u001a\u00020\nH\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0017H\u0016J\u001e\u0010 \u001a\u00020\n2\u0006\u0010!\u001a\u00020\"2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$H\u0016J\u001a\u0010&\u001a\u00020\n2\u0006\u0010'\u001a\u00020(2\b\b\u0002\u0010)\u001a\u00020\"H\u0002J\u0010\u0010*\u001a\u00020\n2\u0006\u0010+\u001a\u00020,H\u0016J\b\u0010-\u001a\u00020\nH\u0016J\b\u0010.\u001a\u00020\nH\u0016J\b\u0010/\u001a\u00020\nH\u0016R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u00061"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryFragment$StepListener;", "()V", "onRetryClicked", "Lio/reactivex/Observable;", "", "getOnRetryClicked", "()Lio/reactivex/Observable;", "presenter", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;)V", "finishStage", "hideLoading", "onCountrySelectionComplete", "firstJurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onStageComplete", "onSummaryStepComplete", "onTinEntryComplete", "nextJurisdiction", "onUsResidentStepCompleted", "isUsTaxResident", "", "taxCountries", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "showFragment", "fragment", "Landroid/support/v4/app/Fragment;", "addToBackStack", "showFullScreenError", "errorMessage", "", "showLoading", "showSummaryScreen", "showUsTaxResidentScreen", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TaxResidencyActivity extends co.uk.getmondo.signup.a implements b.a, co.uk.getmondo.signup.tax_residency.b.b.b, g.a, l.b, q.a {
   public static final TaxResidencyActivity.a b = new TaxResidencyActivity.a((i)null);
   public b a;
   private HashMap g;

   private final void a(Fragment var, boolean var) {
      t var = this.getSupportFragmentManager().a().b(2131821112, var);
      if(var) {
         var.a((String)null).a(4097);
      }

      var.d();
   }

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void a(Jurisdiction var) {
      kotlin.d.b.l.b(var, "firstJurisdiction");
      a(this, (Fragment)l.c.a(var), false, 2, (Object)null);
   }

   public void a(boolean var, ArrayList var) {
      kotlin.d.b.l.b(var, "taxCountries");
      a(this, (Fragment)co.uk.getmondo.signup.tax_residency.b.b.c.a(var, var), false, 2, (Object)null);
   }

   public void b(Jurisdiction var) {
      kotlin.d.b.l.b(var, "nextJurisdiction");
      a(this, (Fragment)l.c.a(var), false, 2, (Object)null);
   }

   public n c() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.taxResidencyErrorView)).c();
   }

   public void d() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.taxResidencyProgress)));
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.taxResidencyErrorView));
      ae.b((FrameLayout)this.a(co.uk.getmondo.c.a.taxResidencyFragmentContainer));
   }

   public void d(int var) {
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.taxResidencyErrorView)).setMessage(this.getString(var));
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.taxResidencyErrorView)));
      ae.b((FrameLayout)this.a(co.uk.getmondo.c.a.taxResidencyFragmentContainer));
   }

   public void e() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.taxResidencyProgress));
      ae.a((View)((FrameLayout)this.a(co.uk.getmondo.c.a.taxResidencyFragmentContainer)));
   }

   public void f() {
      this.a((Fragment)(new g()), false);
   }

   public void g() {
      this.a((Fragment)(new q()), false);
   }

   public void h() {
      this.setResult(-1);
      this.finish();
   }

   public void i() {
      this.a((Fragment)(new q()), true);
   }

   public void j() {
      this.h();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034215);
      this.l().a(this);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(false);
      }

      b var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, j var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, TaxResidencyActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, TaxResid…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
