package co.uk.getmondo.signup.phone_verification;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import co.uk.getmondo.common.ui.ProgressButton;
import io.reactivex.r;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\f\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u0002:;B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0018\u001a\u00020\rH\u0016J\b\u0010\u0019\u001a\u00020\u0006H\u0016J\b\u0010\u001a\u001a\u00020\u0006H\u0016J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0012\u0010\u001e\u001a\u00020\u00062\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J$\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u0010'\u001a\u00020\u0006H\u0016J\u0010\u0010(\u001a\u00020\u00062\u0006\u0010)\u001a\u00020\nH\u0016J+\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020,2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\n0.2\u0006\u0010/\u001a\u000200H\u0016¢\u0006\u0002\u00101J\u001a\u00102\u001a\u00020\u00062\u0006\u00103\u001a\u00020\"2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u00104\u001a\u00020\u0006H\u0016J\b\u00105\u001a\u00020\u0006H\u0016J\u0010\u00106\u001a\u00020\u00062\u0006\u00107\u001a\u00020\rH\u0016J\b\u00108\u001a\u00020\u0006H\u0016J\b\u00109\u001a\u00020\u0006H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\bR2\u0010\u000f\u001a&\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\r0\r \u0011*\u0012\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\r0\r\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017¨\u0006<"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;", "()V", "onContinueClicked", "Lio/reactivex/Observable;", "", "getOnContinueClicked", "()Lio/reactivex/Observable;", "onPhoneNumberChanged", "", "getOnPhoneNumberChanged", "onSmsPermissionsGranted", "", "getOnSmsPermissionsGranted", "permissionsResultRelay", "Lcom/jakewharton/rxrelay2/BehaviorRelay;", "kotlin.jvm.PlatformType", "presenter", "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;)V", "checkSmsPermissions", "hideInvalidPhoneNumberError", "hideLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onPhoneNumberSent", "phoneNumber", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onViewCreated", "view", "reloadSignupStatus", "requestSmsPermissions", "setContinueButtonEnabled", "enabled", "showInvalidPhoneNumberError", "showLoading", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   public static final e.a c = new e.a((kotlin.d.b.i)null);
   public g a;
   private final com.b.b.b d = com.b.b.b.a();
   private final io.reactivex.n e;
   private HashMap f;

   public e() {
      com.b.b.b var = this.d;
      kotlin.d.b.l.a(var, "permissionsResultRelay");
      this.e = (io.reactivex.n)var;
   }

   public View a(int var) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var = (View)this.f.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.f.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.d.e.a((EditText)this.a(co.uk.getmondo.c.a.phoneNumberEditText));
      kotlin.d.b.l.a(var, "RxTextView.editorActions(this)");
      r var = (r)var;
      var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = io.reactivex.n.merge(var, (r)var).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "Observable.merge(phoneNu…on.clicks()).map { Unit }");
      return var;
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "phoneNumber");
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsSendFragment.StepListener");
      } else {
         ((e.b)var).a(var);
      }
   }

   public void a(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setEnabled(var);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public io.reactivex.n c() {
      com.b.a.a var = com.b.a.d.e.c((EditText)this.a(co.uk.getmondo.c.a.phoneNumberEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      io.reactivex.n var = var.map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "phoneNumberEditText.text…s().map { it.toString() }");
      return var;
   }

   public void d() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(true);
   }

   public void e() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(false);
   }

   public void f() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.phoneNumberInputLayout)).setError((CharSequence)this.getString(2131362714));
   }

   public void g() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.phoneNumberInputLayout)).setError((CharSequence)null);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.phoneNumberInputLayout)).setErrorEnabled(false);
   }

   public boolean h() {
      co.uk.getmondo.common.k.k.a var = co.uk.getmondo.common.k.k.a;
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      return var.a((Context)var);
   }

   public io.reactivex.n i() {
      return this.e;
   }

   public void j() {
      this.requestPermissions((String[])((Object[])(new String[]{"android.permission.RECEIVE_SMS", "android.permission.READ_SMS"})), 1001);
   }

   public void k() {
      if(this.f != null) {
         this.f.clear();
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof e.b)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + e.b.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034279, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…s_send, container, false)");
      return var;
   }

   public void onDestroyView() {
      g var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.k();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      boolean var = true;
      kotlin.d.b.l.b(var, "permissions");
      kotlin.d.b.l.b(var, "grantResults");
      if(var == 1001) {
         if(var.length <= 1 || var[0] != 0 || var[1] != 0) {
            var = false;
         }

         this.d.a((Object)Boolean.valueOf(var));
      } else {
         super.onRequestPermissionsResult(var, var, var);
      }

   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      g var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendFragment$Companion;", "", "()V", "REQUEST_SMS_PERMISSIONS", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendFragment$StepListener;", "", "onPhoneNumberSent", "", "phoneNumber", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void a(String var);
   }
}
