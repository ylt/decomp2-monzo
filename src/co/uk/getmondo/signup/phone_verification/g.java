package co.uk.getmondo.signup.phone_verification;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.r;
import io.reactivex.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "phoneVerificationManager", "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public g(u var, u var, c var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "phoneVerificationManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(Throwable var, g.a var) {
      boolean var;
      label27: {
         var.e();
         b var = (b)co.uk.getmondo.common.e.c.a(var, (co.uk.getmondo.common.e.f[])b.values());
         if(var != null) {
            switch(h.a[var.ordinal()]) {
            case 1:
               var.f();
               var = true;
               break label27;
            }
         }

         var = false;
      }

      boolean var = var;
      if(!var) {
         var = co.uk.getmondo.signup.i.a.a(var, (co.uk.getmondo.signup.i.a)var);
      }

      var = var;
      if(!var) {
         var = this.f.a(var, (co.uk.getmondo.common.e.a.a)var);
      }

      if(!var) {
         var.b(2131362198);
      }

   }

   public void a(final g.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.n var = var.c().share();
      io.reactivex.n var = var.a().share();
      this.g.a(Impression.Companion.A());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            g.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var, "phoneNumberChanges\n     …ntinueButtonEnabled(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            if(!var.h()) {
               var.j();
            }

         }
      }));
      kotlin.d.b.l.a(var, "continueClicks\n         …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            var.g();
         }
      }));
      kotlin.d.b.l.a(var, "phoneNumberChanges\n     …validPhoneNumberError() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.n var;
      if(!var.h()) {
         var = var.i();
      } else {
         var = io.reactivex.n.just(Boolean.valueOf(true));
      }

      kotlin.d.b.l.a(var, "continueClicks");
      kotlin.d.b.l.a(var, "onSmsPermissionGranted");
      var = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      kotlin.d.b.l.a(var, "phoneNumberChanges");
      io.reactivex.b.b var = co.uk.getmondo.common.j.f.a(var, (r)var).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.h varx) {
            kotlin.d.b.l.b(varx, "<name for destructuring parameter 0>");
            String var = (String)varx.d();
            c var = g.this.e;
            kotlin.d.b.l.a(var, "phoneNumber");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)var.a(var).b(g.this.d).a(g.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.d();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  g var = g.this;
                  kotlin.d.b.l.a(varx, "error");
                  var.a(varx, var);
               }
            })), (Object)var);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            g.a var = var;
            kotlin.d.b.l.a(varx, "phoneNumber");
            var.a(varx);
         }
      }));
      kotlin.d.b.l.a(var, "continueClicksWithPermis…NumberSent(phoneNumber) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000f\u001a\u00020\rH&J\b\u0010\u0010\u001a\u00020\u0006H&J\b\u0010\u0011\u001a\u00020\u0006H&J\u0010\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\nH&J\b\u0010\u0014\u001a\u00020\u0006H&J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\rH&J\b\u0010\u0017\u001a\u00020\u0006H&J\b\u0010\u0018\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\b¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onContinueClicked", "Lio/reactivex/Observable;", "", "getOnContinueClicked", "()Lio/reactivex/Observable;", "onPhoneNumberChanged", "", "getOnPhoneNumberChanged", "onSmsPermissionsGranted", "", "getOnSmsPermissionsGranted", "checkSmsPermissions", "hideInvalidPhoneNumberError", "hideLoading", "onPhoneNumberSent", "phoneNumber", "requestSmsPermissions", "setContinueButtonEnabled", "enabled", "showInvalidPhoneNumberError", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(String var);

      void a(boolean var);

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();

      boolean h();

      io.reactivex.n i();

      void j();
   }
}
