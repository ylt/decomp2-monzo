package co.uk.getmondo.signup.phone_verification;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/PhoneVerificationError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getPrefix", "()Ljava/lang/String;", "INVALID_PHONE_NUMBER", "EXPIRED_CODE", "INCORRECT_CODE", "TOO_MANY_ATTEMPTS", "PHONE_NUMBER_ALREADY_IN_USE", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum b implements co.uk.getmondo.common.e.f {
   a,
   b,
   c,
   d,
   e;

   private final String g;

   static {
      b var = new b("INVALID_PHONE_NUMBER", 0, "bad_request.bad_param.phone_number");
      a = var;
      b var = new b("EXPIRED_CODE", 1, "bad_request.code.expired");
      b = var;
      b var = new b("INCORRECT_CODE", 2, "bad_request.code.incorrect");
      c = var;
      b var = new b("TOO_MANY_ATTEMPTS", 3, "bad_request.too_many_attempts");
      d = var;
      b var = new b("PHONE_NUMBER_ALREADY_IN_USE", 4, "bad_request.phone_number.in_use");
      e = var;
   }

   protected b(String var) {
      kotlin.d.b.l.b(var, "prefix");
      super(var, var);
      this.g = var;
   }

   public String a() {
      return this.g;
   }
}
