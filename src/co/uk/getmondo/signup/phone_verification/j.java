package co.uk.getmondo.signup.phone_verification;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.create_account.phone_number.ag;
import io.reactivex.o;
import io.reactivex.p;
import io.reactivex.r;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u000b\u0018\u0000 52\u00020\u00012\u00020\u0002:\u000556789B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\b\u0010\u0019\u001a\u00020\nH\u0016J\b\u0010\u001a\u001a\u00020\nH\u0016J\b\u0010\u001b\u001a\u00020\nH\u0016J\u0010\u0010\u001c\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0012\u0010\u001f\u001a\u00020\n2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J$\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010'2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\b\u0010(\u001a\u00020\nH\u0016J\b\u0010)\u001a\u00020\nH\u0016J\u001a\u0010*\u001a\u00020\n2\u0006\u0010+\u001a\u00020#2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\b\u0010,\u001a\u00020\nH\u0016J\u0010\u0010-\u001a\u00020\n2\u0006\u0010.\u001a\u00020/H\u0016J\b\u00100\u001a\u00020\nH\u0016J\b\u00101\u001a\u00020\nH\u0016J\b\u00102\u001a\u00020\nH\u0016J\b\u00103\u001a\u00020\nH\u0016J\b\u00104\u001a\u00020\nH\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\bR\u0014\u0010\u000e\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006:"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;", "()V", "onCodeChanged", "Lio/reactivex/Observable;", "", "getOnCodeChanged", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onSmsCodeReceived", "getOnSmsCodeReceived", "phoneNumber", "getPhoneNumber", "()Ljava/lang/String;", "presenter", "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;)V", "fillCode", "code", "hideIncorrectCodeError", "hideLoading", "navigateToSmsSendScreen", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onPhoneNumberVerified", "onViewCreated", "view", "reloadSignupStatus", "setContinueButtonEnabled", "enabled", "", "showExpiredCodeError", "showIncorrectCodeError", "showLoading", "showPhoneNumberAlreadyInUseError", "showTooManyAttemptsError", "Companion", "ExpiredCodeDialog", "PhoneNumberAlreadyInUseDialog", "StepListener", "TooManyAttemptsDialog", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class j extends co.uk.getmondo.common.f.a implements l.a {
   public static final j.a c = new j.a((kotlin.d.b.i)null);
   public l a;
   private HashMap d;

   public View a(int var) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var = (View)this.d.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.d.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public final l a() {
      l var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      return var;
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "code");
      ((EditText)this.a(co.uk.getmondo.c.a.verificationCodeEditText)).setText((CharSequence)var);
   }

   public void a(boolean var) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setEnabled(var);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public io.reactivex.n c() {
      io.reactivex.n var = io.reactivex.n.create((p)(new p() {
         public final void a(final o var) {
            kotlin.d.b.l.b(var, "emitter");
            final ag var = new ag();
            co.uk.getmondo.create_account.phone_number.a var = (co.uk.getmondo.create_account.phone_number.a)(new co.uk.getmondo.create_account.phone_number.a() {
               public final void a(String varx) {
                  var.a(varx);
               }
            });
            ag.a var = ag.a;
            Context var = j.this.getContext();
            kotlin.d.b.l.a(var, "context");
            var.a(var, var, "\\d{4,8}", (String[])((Object[])(new String[]{"Monzo"})), var);
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ag.a var = ag.a;
                  Context var = j.this.getContext();
                  kotlin.d.b.l.a(var, "context");
                  var.a(var, var);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…CodeReceiver) }\n        }");
      return var;
   }

   public io.reactivex.n d() {
      io.reactivex.n var = com.b.a.d.e.a((EditText)this.a(co.uk.getmondo.c.a.verificationCodeEditText));
      kotlin.d.b.l.a(var, "RxTextView.editorActions(this)");
      r var = (r)var;
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      var = io.reactivex.n.merge(var, (r)var).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "Observable.merge(verific…on.clicks()).map { Unit }");
      return var;
   }

   public io.reactivex.n e() {
      com.b.a.a var = com.b.a.d.e.c((EditText)this.a(co.uk.getmondo.c.a.verificationCodeEditText));
      kotlin.d.b.l.a(var, "RxTextView.textChanges(this)");
      io.reactivex.n var = var.map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "verificationCodeEditText…s().map { it.toString() }");
      return var;
   }

   public void f() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(true);
   }

   public void g() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(false);
   }

   public String h() {
      String var = this.getArguments().getString("phoneNumber");
      kotlin.d.b.l.a(var, "arguments.getString(EXTRA_PHONE_NUMBER)");
      return var;
   }

   public void i() {
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment.StepListener");
      } else {
         ((j.d)var).d();
      }
   }

   public void j() {
      j.b var = new j.b();
      var.setTargetFragment((Fragment)this, 0);
      var.show(this.getFragmentManager(), "errorDialog");
   }

   public void k() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.verificationCodeInputLayout)).setError((CharSequence)this.getString(2131362719));
   }

   public void l() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.verificationCodeInputLayout)).setError((CharSequence)null);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.verificationCodeInputLayout)).setErrorEnabled(false);
   }

   public void m() {
      j.e var = new j.e();
      var.setTargetFragment((Fragment)this, 0);
      var.show(this.getFragmentManager(), "errorDialog");
   }

   public void n() {
      (new j.c()).show(this.getFragmentManager(), "errorDialog");
   }

   public void o() {
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment.StepListener");
      } else {
         ((j.d)var).c();
      }
   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof j.d)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + j.d.class.getSimpleName()).toString()));
      } else if(!(var instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      if(this.h() == null) {
         throw (Throwable)(new IllegalStateException("Required value was null.".toString()));
      } else {
         this.B().a(this);
      }
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034280, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…cation, container, false)");
      return var;
   }

   public void onDestroyView() {
      l var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.p();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      l var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((l.a)this);
      ae.a((EditText)this.a(co.uk.getmondo.c.a.verificationCodeEditText));
   }

   public void p() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$Companion;", "", "()V", "EXTRA_PHONE_NUMBER", "", "REGEX_PATTERN_CODE", "TAG_ERROR_DIALOG_FRAGMENT", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$ExpiredCodeDialog;", "Landroid/support/v4/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends android.support.v4.app.i {
      private HashMap a;

      public void a() {
         if(this.a != null) {
            this.a.clear();
         }

      }

      public Dialog onCreateDialog(Bundle var) {
         AlertDialog var = (new Builder(this.getContext())).setTitle(2131362722).setMessage(2131362718).setNegativeButton(2131362726, (OnClickListener)null.a).setPositiveButton(2131362728, (OnClickListener)(new OnClickListener() {
            public final void onClick(DialogInterface var, int var) {
               Fragment var = b.this.getTargetFragment();
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment");
               } else {
                  ((j)var).a().a();
                  var.dismiss();
               }
            }
         })).create();
         kotlin.d.b.l.a(var, "AlertDialog.Builder(cont…                .create()");
         return (Dialog)var;
      }

      // $FF: synthetic method
      public void onDestroyView() {
         super.onDestroyView();
         this.a();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$PhoneNumberAlreadyInUseDialog;", "Landroid/support/v4/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends android.support.v4.app.i {
      private HashMap a;

      public void a() {
         if(this.a != null) {
            this.a.clear();
         }

      }

      public Dialog onCreateDialog(Bundle var) {
         AlertDialog var = (new Builder(this.getContext())).setTitle(2131362723).setMessage(2131362720).setNegativeButton(2131362725, (OnClickListener)(new OnClickListener() {
            public final void onClick(DialogInterface var, int var) {
               MonzoApplication.a((Context)c.this.getActivity()).b().p().a();
               var.dismiss();
            }
         })).setPositiveButton(2131362727, (OnClickListener)null.a).create();
         kotlin.d.b.l.a(var, "AlertDialog.Builder(cont…                .create()");
         return (Dialog)var;
      }

      // $FF: synthetic method
      public void onDestroyView() {
         super.onDestroyView();
         this.a();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$StepListener;", "", "navigateToSmsSendScreen", "", "onPhoneNumberVerified", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface d {
      void c();

      void d();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$TooManyAttemptsDialog;", "Landroid/support/v4/app/DialogFragment;", "()V", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class e extends android.support.v4.app.i {
      private HashMap a;

      public void a() {
         if(this.a != null) {
            this.a.clear();
         }

      }

      public Dialog onCreateDialog(Bundle var) {
         AlertDialog var = (new Builder(this.getContext())).setTitle(2131362724).setMessage(2131362721).setNegativeButton(2131362726, (OnClickListener)null.a).setPositiveButton(2131362728, (OnClickListener)(new OnClickListener() {
            public final void onClick(DialogInterface var, int var) {
               Fragment var = e.this.getTargetFragment();
               if(var == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.phone_verification.SmsVerificationFragment");
               } else {
                  ((j)var).a().a();
                  var.dismiss();
               }
            }
         })).create();
         kotlin.d.b.l.a(var, "AlertDialog.Builder(cont…                .create()");
         return (Dialog)var;
      }

      // $FF: synthetic method
      public void onDestroyView() {
         super.onDestroyView();
         this.a();
      }
   }
}
