package co.uk.getmondo.signup.phone_verification;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.r;
import io.reactivex.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0006\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "phoneVerificationManager", "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "getNewCode", "", "handleError", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public l(u var, u var, c var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "phoneVerificationManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(Throwable var, l.a var) {
      boolean var;
      label30: {
         var.g();
         b var = (b)co.uk.getmondo.common.e.c.a(var, (co.uk.getmondo.common.e.f[])b.values());
         if(var != null) {
            switch(m.a[var.ordinal()]) {
            case 1:
               var.j();
               var = true;
               break label30;
            case 2:
               var.k();
               var = true;
               break label30;
            case 3:
               var.m();
               var = true;
               break label30;
            case 4:
               var.n();
               var.o();
               var = true;
               break label30;
            }
         }

         var = false;
      }

      boolean var = var;
      if(!var) {
         var = co.uk.getmondo.signup.i.a.a(var, (co.uk.getmondo.signup.i.a)var);
      }

      var = var;
      if(!var) {
         var = this.f.a(var, (co.uk.getmondo.common.e.a.a)var);
      }

      if(!var) {
         var.b(2131362198);
      }

   }

   // $FF: synthetic method
   public static final l.a d(l var) {
      return (l.a)var.a;
   }

   public final void a() {
      ((l.a)this.a).f();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = this.e.a(((l.a)this.a).h()).b(this.d).a(this.c).a((io.reactivex.c.a)(new io.reactivex.c.a() {
         public final void a() {
            l.d(l.this).g();
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var) {
            l var = l.this;
            kotlin.d.b.l.a(var, "it");
            l.a var = l.d(l.this);
            kotlin.d.b.l.a(var, "view");
            var.a(var, var);
         }
      }));
      kotlin.d.b.l.a(var, "phoneVerificationManager…view) }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   public void a(final l.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.Companion.B());
      io.reactivex.n var = var.e().share();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            l.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var, "codeChanges\n            …ntinueButtonEnabled(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            var.l();
         }
      }));
      kotlin.d.b.l.a(var, "codeChanges\n            …ideIncorrectCodeError() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.n var = var.d();
      kotlin.d.b.l.a(var, "codeChanges");
      var = co.uk.getmondo.common.j.f.a(var, (r)var).map((io.reactivex.c.h)null.a);
      var = var.c().share();
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.observeOn(this.c).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String varx) {
            l.a var = var;
            kotlin.d.b.l.a(varx, "it");
            var.a(varx);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "smsCodeReceived\n        …(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = io.reactivex.n.merge((r)var, (r)var).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String varx) {
            kotlin.d.b.l.b(varx, "code");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)l.this.e.a(var.h(), varx).b(l.this.d).a(l.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.f();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  l var = l.this;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, var);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.i();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.merge(manualS…onPhoneNumberVerified() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0006H&J\b\u0010\u0013\u001a\u00020\nH&J\b\u0010\u0014\u001a\u00020\nH&J\b\u0010\u0015\u001a\u00020\nH&J\b\u0010\u0016\u001a\u00020\nH&J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\u0019H&J\b\u0010\u001a\u001a\u00020\nH&J\b\u0010\u001b\u001a\u00020\nH&J\b\u0010\u001c\u001a\u00020\nH&J\b\u0010\u001d\u001a\u00020\nH&J\b\u0010\u001e\u001a\u00020\nH&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\bR\u0012\u0010\u000e\u001a\u00020\u0006X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onCodeChanged", "Lio/reactivex/Observable;", "", "getOnCodeChanged", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onSmsCodeReceived", "getOnSmsCodeReceived", "phoneNumber", "getPhoneNumber", "()Ljava/lang/String;", "fillCode", "code", "hideIncorrectCodeError", "hideLoading", "navigateToSmsSendScreen", "onPhoneNumberVerified", "setContinueButtonEnabled", "enabled", "", "showExpiredCodeError", "showIncorrectCodeError", "showLoading", "showPhoneNumberAlreadyInUseError", "showTooManyAttemptsError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      void a(String var);

      void a(boolean var);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      String h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();
   }
}
