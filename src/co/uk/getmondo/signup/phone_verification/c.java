package co.uk.getmondo.signup.phone_verification;

import co.uk.getmondo.api.SignupApi;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "(Lco/uk/getmondo/api/SignupApi;)V", "sendPhoneNumber", "Lio/reactivex/Completable;", "phoneNumber", "", "verifyPhoneNumber", "code", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final SignupApi a;

   public c(SignupApi var) {
      kotlin.d.b.l.b(var, "signupApi");
      super();
      this.a = var;
   }

   public final io.reactivex.b a(String var) {
      kotlin.d.b.l.b(var, "phoneNumber");
      return this.a.sendPhoneNumber(var);
   }

   public final io.reactivex.b a(String var, String var) {
      kotlin.d.b.l.b(var, "phoneNumber");
      kotlin.d.b.l.b(var, "code");
      return this.a.verifyPhoneNumber(var, var);
   }
}
