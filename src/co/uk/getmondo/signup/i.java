package co.uk.getmondo.signup;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/signup/SignupApiErrorHandler;", "", "()V", "handleError", "", "throwable", "", "view", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i {
   public static final i a;

   static {
      new i();
   }

   private i() {
      a = (i)this;
   }

   public final boolean a(Throwable var, i.a var) {
      boolean var = false;
      l.b(var, "throwable");
      l.b(var, "view");
      String var = co.uk.getmondo.common.e.c.a(var);
      if(kotlin.h.j.a(var, "precondition_failed.signup.stage", false, 2, (Object)null) || kotlin.h.j.a(var, "precondition_failed.signup.status", false, 2, (Object)null)) {
         var.b();
         var = true;
      }

      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "Lco/uk/getmondo/common/errors/ErrorView;", "reloadSignupStatus", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.e {
      void b();
   }
}
