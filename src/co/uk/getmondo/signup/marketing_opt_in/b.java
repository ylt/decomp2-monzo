package co.uk.getmondo.signup.marketing_opt_in;

import co.uk.getmondo.api.SignupApi;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "(Lco/uk/getmondo/api/SignupApi;)V", "subscribeToMarketing", "Lio/reactivex/Completable;", "optIn", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final SignupApi a;

   public b(SignupApi var) {
      l.b(var, "signupApi");
      super();
      this.a = var;
   }

   public final io.reactivex.b a(boolean var) {
      SignupApi var = this.a;
      boolean var;
      if(!var) {
         var = true;
      } else {
         var = false;
      }

      return var.subscribeToMarketing(var, var);
   }
}
