package co.uk.getmondo.signup.marketing_opt_in;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.i;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.c.h;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "marketingOptInManager", "Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/marketing_opt_in/MarketingOptInManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "handleError", "", "throwable", "", "handleSubscribe", "optIn", "", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.a e;
   private final b f;
   private final co.uk.getmondo.common.e.a g;

   public f(u var, u var, co.uk.getmondo.common.a var, b var, co.uk.getmondo.common.e.a var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "analyticsService");
      l.b(var, "marketingOptInManager");
      l.b(var, "apiErrorHandler");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(Throwable var) {
      ((f.a)this.a).e();
      ((f.a)this.a).g();
      ((f.a)this.a).a(true);
      ((f.a)this.a).b(true);
      i var = i.a;
      co.uk.getmondo.common.ui.f var = this.a;
      l.a(var, "view");
      if(!var.a(var, (i.a)var)) {
         co.uk.getmondo.common.e.a var = this.g;
         co.uk.getmondo.common.ui.f var = this.a;
         l.a(var, "view");
         if(!var.a(var, (co.uk.getmondo.common.e.a.a)var)) {
            ((f.a)this.a).b(2131362198);
         }
      }

   }

   private final void a(boolean var) {
      if(var) {
         ((f.a)this.a).d();
      } else {
         ((f.a)this.a).f();
      }

      ((f.a)this.a).a(false);
      ((f.a)this.a).b(false);
   }

   public void a(final f.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.e.a(Impression.Companion.aH());
      n var = var.a().map((h)null.a);
      n var = var.c().map((h)null.a);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.mergeWith((r)var).throttleFirst(100L, TimeUnit.MILLISECONDS).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            co.uk.getmondo.common.a var = f.this.e;
            Impression.Companion var = Impression.Companion;
            l.a(var, "optIn");
            var.a(var.l(var.booleanValue()));
         }
      })).flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(final Boolean var) {
            l.b(var, "optIn");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)f.this.f.a(var.booleanValue()).b(f.this.d).a(f.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  f var = f.this;
                  Boolean var = var;
                  l.a(var, "optIn");
                  var.a(var.booleanValue());
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var) {
                  f var = f.this;
                  l.a(var, "it");
                  var.a(var);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.h();
         }
      }));
      l.a(var, "optInClicks\n            … { view.completeStage() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000b\u001a\u00020\u0006H&J\b\u0010\f\u001a\u00020\u0006H&J\b\u0010\r\u001a\u00020\u0006H&J\u0010\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H&J\b\u0010\u0012\u001a\u00020\u0006H&J\b\u0010\u0013\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\b¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onOptInClicked", "Lio/reactivex/Observable;", "", "getOnOptInClicked", "()Lio/reactivex/Observable;", "onOptOutClicked", "getOnOptOutClicked", "completeStage", "hideOptInLoading", "hideOptOutLoading", "setOptInButtonEnabled", "enabled", "", "setOptOutButtonEnabled", "showOptInLoading", "showOptOutLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, i.a {
      n a();

      void a(boolean var);

      void b(boolean var);

      n c();

      void d();

      void e();

      void f();

      void g();

      void h();
   }
}
