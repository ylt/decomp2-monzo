package co.uk.getmondo.signup.marketing_opt_in;

import co.uk.getmondo.api.SignupApi;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new c(var);
   }

   public b a() {
      return new b((SignupApi)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
