package co.uk.getmondo.signup.documents;

import co.uk.getmondo.api.SignupApi;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new e(var);
   }

   public d a() {
      return new d((SignupApi)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
