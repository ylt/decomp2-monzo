package co.uk.getmondo.signup.documents;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import co.uk.getmondo.signup.j;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \t2\u00020\u00012\u00020\u0002:\u0001\tB\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0014J\b\u0010\b\u001a\u00020\u0005H\u0016¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/documents/LegalDocumentsFragment$StepListener;", "()V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onMonzoDocsCompleted", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LegalDocumentsActivity extends co.uk.getmondo.signup.a implements b.a {
   public static final LegalDocumentsActivity.a a = new LegalDocumentsActivity.a((i)null);
   private HashMap b;

   public View a(int var) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var = (View)this.b.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.b.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void c() {
      this.setResult(-1);
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034183);
      this.l().a(this);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(false);
      }

      var = this.getSupportActionBar();
      if(var != null) {
         var.d(false);
      }

      if(var == null) {
         this.getSupportFragmentManager().a().a(2131820984, (Fragment)(new b())).c();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, j var) {
         l.b(var, "context");
         l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, LegalDocumentsActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         l.a(var, "Intent(context, LegalDoc…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
