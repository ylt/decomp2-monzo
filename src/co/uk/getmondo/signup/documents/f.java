package co.uk.getmondo.signup.documents;

import co.uk.getmondo.api.model.signup.SignUpDocumentUrls;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.i;
import io.reactivex.u;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "legalDocumentsManager", "Lco/uk/getmondo/signup/documents/LegalDocumentsManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/documents/LegalDocumentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "handleAcceptError", "", "throwable", "", "handleGetUrlsError", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.a e;
   private final d f;
   private final co.uk.getmondo.common.e.a g;

   public f(u var, u var, co.uk.getmondo.common.a var, d var, co.uk.getmondo.common.e.a var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "analyticsService");
      l.b(var, "legalDocumentsManager");
      l.b(var, "apiErrorHandler");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   private final void a(Throwable var) {
      ((f.a)this.a).i();
      i var = i.a;
      co.uk.getmondo.common.ui.f var = this.a;
      l.a(var, "view");
      if(!var.a(var, (i.a)var)) {
         co.uk.getmondo.common.e.a var = this.g;
         var = this.a;
         l.a(var, "view");
         if(!var.a(var, (co.uk.getmondo.common.e.a.a)var)) {
            ((f.a)this.a).b(2131362198);
         }
      }

   }

   private final void b(Throwable var) {
      ((f.a)this.a).k();
      i var = i.a;
      co.uk.getmondo.common.ui.f var = this.a;
      l.a(var, "view");
      if(!var.a(var, (i.a)var)) {
         co.uk.getmondo.common.e.a var = this.g;
         var = this.a;
         l.a(var, "view");
         if(!var.a(var, (co.uk.getmondo.common.e.a.a)var)) {
            ((f.a)this.a).b(2131362198);
         }
      }

   }

   public void a(final f.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.e.a(Impression.Companion.aI());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().startWith((Object)n.a).flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(n varx) {
            l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(f.this.f.a().b(f.this.d).a(f.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.h();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  f var = f.this;
                  l.a(varx, "it");
                  var.a(varx);
               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(SignUpDocumentUrls varx) {
            var.i();
            var.a(varx);
         }
      }));
      l.a(var, "view.onRefreshUrlsClicke… = urls\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            f.this.e.a(Impression.Companion.aJ());
            var.l();
         }
      }));
      l.a(var, "view.onTermsAndCondition…tions()\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            f.this.e.a(Impression.Companion.aK());
            var.m();
         }
      }));
      l.a(var, "view.onPrivacyPolicyClic…olicy()\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            f.this.e.a(Impression.Companion.aL());
            var.n();
         }
      }));
      l.a(var, "view.onFscsProtectionCli…ction()\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.f().flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(n varx) {
            l.b(varx, "it");
            SignUpDocumentUrls var = var.g();
            if(var == null) {
               l.a();
            }

            String var = var.b();
            SignUpDocumentUrls var = var.g();
            if(var == null) {
               l.a();
            }

            String var = var.d();
            SignUpDocumentUrls var = var.g();
            if(var == null) {
               l.a();
            }

            String var = var.f();
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)f.this.f.a(var, var, var).b(f.this.d).a(f.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.j();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  f var = f.this;
                  l.a(varx, "it");
                  var.b(varx);
               }
            })), (Object)n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n varx) {
            var.o();
         }
      }));
      l.a(var, "view.onContinueClicked\n … { view.completeStage() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\r\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0017\u001a\u00020\u0006H&J\b\u0010\u0018\u001a\u00020\u0006H&J\b\u0010\u0019\u001a\u00020\u0006H&J\b\u0010\u001a\u001a\u00020\u0006H&J\b\u0010\u001b\u001a\u00020\u0006H&J\b\u0010\u001c\u001a\u00020\u0006H&J\b\u0010\u001d\u001a\u00020\u0006H&J\b\u0010\u001e\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u0018\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\bR\u001a\u0010\u0011\u001a\u0004\u0018\u00010\u0012X¦\u000e¢\u0006\f\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onContinueClicked", "Lio/reactivex/Observable;", "", "getOnContinueClicked", "()Lio/reactivex/Observable;", "onFscsProtectionClicked", "getOnFscsProtectionClicked", "onPrivacyPolicyClicked", "getOnPrivacyPolicyClicked", "onRefreshUrlsClicked", "getOnRefreshUrlsClicked", "onTermsAndConditionsClicked", "getOnTermsAndConditionsClicked", "urls", "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "getUrls", "()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "setUrls", "(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V", "completeStage", "hideAcceptLoading", "hideUrlsLoading", "openFscsProtection", "openPrivacyPolicy", "openTermsAndConditions", "showAcceptLoading", "showUrlsLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, i.a {
      io.reactivex.n a();

      void a(SignUpDocumentUrls var);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      SignUpDocumentUrls g();

      void h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();
   }
}
