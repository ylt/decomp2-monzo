package co.uk.getmondo.signup.documents;

import co.uk.getmondo.api.SignupApi;
import io.reactivex.v;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bJ\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "(Lco/uk/getmondo/api/SignupApi;)V", "acceptLegalDocuments", "Lio/reactivex/Completable;", "termsAndConditionsVersion", "", "privacyPolicyVersion", "fscsInformationSheetVersion", "legalDocuments", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d {
   private final SignupApi a;

   public d(SignupApi var) {
      l.b(var, "signupApi");
      super();
      this.a = var;
   }

   public final io.reactivex.b a(String var, String var, String var) {
      l.b(var, "termsAndConditionsVersion");
      l.b(var, "privacyPolicyVersion");
      l.b(var, "fscsInformationSheetVersion");
      return this.a.acceptLegalDocuments(true, var, true, var, true, var);
   }

   public final v a() {
      return this.a.legalDocuments();
   }
}
