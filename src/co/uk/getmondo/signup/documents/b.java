package co.uk.getmondo.signup.documents;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import co.uk.getmondo.api.model.signup.SignUpDocumentUrls;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.signup.i;
import co.uk.getmondo.terms_and_conditions.TermsAndConditionsActivity;
import io.reactivex.n;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002:\u00019B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001e\u001a\u00020\u0006H\u0016J\b\u0010\u001f\u001a\u00020\u0006H\u0016J\b\u0010 \u001a\u00020\u0006H\u0016J\u0010\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u00020\u00062\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J$\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010,2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u0010-\u001a\u00020\u0006H\u0016J\u001c\u0010.\u001a\u00020\u00062\b\u0010/\u001a\u0004\u0018\u00010(2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u00100\u001a\u00020\u0006H\u0016J\b\u00101\u001a\u00020\u0006H\u0016J\b\u00102\u001a\u00020\u0006H\u0016J\b\u00103\u001a\u00020\u0006H\u0016J\b\u00104\u001a\u00020\u0006H\u0016J\u0012\u00105\u001a\u00020\u00062\b\u00106\u001a\u0004\u0018\u000107H\u0016J\b\u00108\u001a\u00020\u0006H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\bR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R(\u0010\u0019\u001a\u0004\u0018\u00010\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018@VX\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001d¨\u0006:"},
   d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter$View;", "()V", "onContinueClicked", "Lio/reactivex/Observable;", "", "getOnContinueClicked", "()Lio/reactivex/Observable;", "onFscsProtectionClicked", "getOnFscsProtectionClicked", "onPrivacyPolicyClicked", "getOnPrivacyPolicyClicked", "onRefreshUrlsClicked", "getOnRefreshUrlsClicked", "onTermsAndConditionsClicked", "getOnTermsAndConditionsClicked", "presenter", "Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/documents/LegalDocumentsPresenter;)V", "value", "Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "urls", "getUrls", "()Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "setUrls", "(Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;)V", "completeStage", "hideAcceptLoading", "hideUrlsLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "openFscsProtection", "openPrivacyPolicy", "openTermsAndConditions", "reloadSignupStatus", "showAcceptLoading", "showError", "message", "", "showUrlsLoading", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.f.a implements f.a {
   public f a;
   private SignUpDocumentUrls c;
   private HashMap d;

   public View a(int var) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var = (View)this.d.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.d.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((LoadingErrorView)this.a(co.uk.getmondo.c.a.monzoDocsErrorView)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(SignUpDocumentUrls var) {
      this.c = var;
      ae.a((View)((ConstraintLayout)this.a(co.uk.getmondo.c.a.monzoDocsContent)));
   }

   public void b() {
      i.a var = (i.a)this.getActivity();
      if(var != null) {
         var.b();
      }

   }

   public n c() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.docsTermsAndConditionsButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public n d() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.docsPrivacyPolicyButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void d(String var) {
      if(((ConstraintLayout)this.a(co.uk.getmondo.c.a.monzoDocsContent)).getVisibility() != 0) {
         ((LoadingErrorView)this.a(co.uk.getmondo.c.a.monzoDocsErrorView)).setVisibility(0);
         ((LoadingErrorView)this.a(co.uk.getmondo.c.a.monzoDocsErrorView)).setMessage(var);
      } else {
         super.d(var);
      }

   }

   public n e() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.docsFscsProtectionButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public n f() {
      n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.docsContinueButton)).map((h)com.b.a.a.d.a);
      l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public SignUpDocumentUrls g() {
      return this.c;
   }

   public void h() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.monzoDocsProgress)));
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.monzoDocsErrorView));
      ae.b((ConstraintLayout)this.a(co.uk.getmondo.c.a.monzoDocsContent));
   }

   public void i() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.monzoDocsProgress));
   }

   public void j() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.docsContinueButton)).setLoading(true);
   }

   public void k() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.docsContinueButton)).setLoading(false);
   }

   public void l() {
      Context var = (Context)this.getActivity();
      SignUpDocumentUrls var = this.g();
      if(var == null) {
         l.a();
      }

      this.startActivity(TermsAndConditionsActivity.a(var, var.a()));
   }

   public void m() {
      android.support.b.a var = (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)this.getActivity(), 2131689487)).a();
      Context var = (Context)this.getActivity();
      SignUpDocumentUrls var = this.g();
      if(var == null) {
         l.a();
      }

      var.a(var, Uri.parse(var.c()));
   }

   public void n() {
      android.support.b.a var = (new android.support.b.a.a()).a(android.support.v4.content.a.c((Context)this.getActivity(), 2131689487)).a();
      Context var = (Context)this.getActivity();
      SignUpDocumentUrls var = this.g();
      if(var == null) {
         l.a();
      }

      var.a(var, Uri.parse(var.e()));
   }

   public void o() {
      j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.documents.LegalDocumentsFragment.StepListener");
      } else {
         ((b.a)var).c();
      }
   }

   public void onAttach(Context var) {
      l.b(var, "context");
      super.onAttach(var);
      if(!(var instanceof b.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + b.a.class.getSimpleName()).toString()));
      } else if(!(var instanceof i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      l.b(var, "inflater");
      View var = var.inflate(2131034273, var, false);
      l.a(var, "inflater.inflate(R.layou…o_docs, container, false)");
      return var;
   }

   public void onDestroyView() {
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.p();
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      f var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((f.a)this);
   }

   public void p() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/documents/LegalDocumentsFragment$StepListener;", "", "onMonzoDocsCompleted", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a {
      void c();
   }
}
