package co.uk.getmondo.signup.pending;

import co.uk.getmondo.api.model.signup.SignupInfo;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.f;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/signup/pending/SignupPendingPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/pending/SignupPendingPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "signupStatusManager", "Lco/uk/getmondo/signup/status/SignupStatusManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/status/SignupStatusManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.signup.status.b e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public b(u var, u var, co.uk.getmondo.signup.status.b var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var) {
      l.b(var, "uiScheduler");
      l.b(var, "ioScheduler");
      l.b(var, "signupStatusManager");
      l.b(var, "apiErrorHandler");
      l.b(var, "analyticsService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((f)var);
      this.g.a(Impression.Companion.aV());
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = n.interval(2L, 15L, TimeUnit.SECONDS).flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(Long varx) {
            l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(co.uk.getmondo.signup.status.b.a(b.this.e, false, 1, (Object)null).b(b.this.d).a(b.this.c).d((g)(new g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = b.this.f;
                  l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })));
         }
      })).subscribe((g)(new g() {
         public final void a(SignupInfo varx) {
            if(l.a(varx.a(), SignupInfo.Status.IN_PROGRESS) ^ true && l.a(varx.a(), SignupInfo.Status.APPROVED) ^ true || l.a(varx.b(), SignupInfo.Stage.PENDING) ^ true) {
               var.c();
            }

         }
      }));
      l.a(var, "Observable.interval(2, 1…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/pending/SignupPendingPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "finishStage", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      void c();
   }
}
