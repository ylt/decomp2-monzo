package co.uk.getmondo.signup.pending;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.signup.j;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00112\u00020\u00012\u00020\u0002:\u0001\u0011B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\u0012\u0010\f\u001a\u00020\u000b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u000bH\u0014J\b\u0010\u0010\u001a\u00020\u000bH\u0014R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/signup/pending/SignupPendingActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/pending/SignupPendingPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/signup/pending/SignupPendingPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/pending/SignupPendingPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/pending/SignupPendingPresenter;)V", "finishStage", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onStop", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignupPendingActivity extends co.uk.getmondo.signup.a implements b.a {
   public static final SignupPendingActivity.a b = new SignupPendingActivity.a((i)null);
   public b a;
   private HashMap g;

   public View a(int var) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var = (View)this.g.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.g.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public void c() {
      this.setResult(-1);
      this.finish();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034208);
      this.l().a(this);
      if(l.a(this.a(), j.a)) {
         ae.a((View)((Button)this.a(co.uk.getmondo.c.a.signupPendingBackButton)));
         ((Button)this.a(co.uk.getmondo.c.a.signupPendingBackButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var) {
               SignupPendingActivity.this.finish();
            }
         }));
      }

   }

   protected void onResume() {
      super.onResume();
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onStop() {
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onStop();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/pending/SignupPendingActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var, j var) {
         l.b(var, "context");
         l.b(var, "signupEntryPoint");
         Intent var = (new Intent(var, SignupPendingActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var);
         l.a(var, "Intent(context, SignupPe…_POINT, signupEntryPoint)");
         return var;
      }
   }
}
