package co.uk.getmondo.signup.identity_verification.chat_with_us;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import io.reactivex.n;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00162\u00020\u00012\u00020\u0002:\u0001\u0016B\u0005¢\u0006\u0002\u0010\u0003J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u0012\u0010\u0010\u001a\u00020\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\b\u0010\u0013\u001a\u00020\fH\u0014J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u000eH\u0016J\b\u0010\u0015\u001a\u00020\fH\u0014R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter$View;", "()V", "presenter", "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;)V", "resumeRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "onChatClicked", "Lio/reactivex/Observable;", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onRefresh", "onResume", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ChatWithUsActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final ChatWithUsActivity.a b = new ChatWithUsActivity.a((i)null);
   public b a;
   private final com.b.b.c c;
   private HashMap e;

   public ChatWithUsActivity() {
      com.b.b.c var = com.b.b.c.a();
      l.a(var, "PublishRelay.create()");
      this.c = var;
   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public n a() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.chatWithUsButton));
      l.a(var, "RxView.clicks(chatWithUsButton)");
      return var;
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034153);
      this.l().a(this);
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.a((b.a)this);
   }

   protected void onDestroy() {
      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   protected void onResume() {
      super.onResume();
      this.c.a((Object)kotlin.n.a);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final Intent a(Context var) {
         l.b(var, "context");
         return new Intent(var, ChatWithUsActivity.class);
      }
   }
}
