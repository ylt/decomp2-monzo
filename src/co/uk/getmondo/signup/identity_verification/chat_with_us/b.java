package co.uk.getmondo.signup.identity_verification.chat_with_us;

import co.uk.getmondo.common.q;
import co.uk.getmondo.common.ui.f;
import io.reactivex.n;
import io.reactivex.c.g;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u000f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter$View;", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "(Lco/uk/getmondo/common/IntercomService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final q c;

   public b(q var) {
      l.b(var, "intercomService");
      super();
      this.c = var;
   }

   public void a(b.a var) {
      l.b(var, "view");
      super.a((f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.a().subscribe((g)(new g() {
         public final void a(Object var) {
            b.this.c.a();
         }
      }), (g)null.a);
      l.a(var, "view.onChatClicked()\n   …om() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u000e\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004H&¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/chat_with_us/ChatWithUsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onChatClicked", "Lio/reactivex/Observable;", "", "onRefresh", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      n a();
   }
}
