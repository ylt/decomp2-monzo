package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.BitmapFactory.Options;
import android.util.SizeF;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.InputStream;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\"\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\f0\u00182\u0006\u0010\u0015\u001a\u00020\u0016J\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\f0\u00182\u0006\u0010\u0012\u001a\u00020\u0013¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;", "", "()V", "calculateRotationMatrix", "Landroid/graphics/Matrix;", "exifInterface", "Landroid/support/media/ExifInterface;", "calculateScalingOptions", "Landroid/graphics/BitmapFactory$Options;", "size", "Landroid/util/SizeF;", "cropAndRotate", "Landroid/graphics/Bitmap;", "bitmap", "shouldCrop", "", "rotationMatrix", "extractPictureSizeFromByteArray", "pictureData", "", "extractPictureSizeFromFile", "file", "Ljava/io/File;", "prepareFallbackImage", "Lio/reactivex/Observable;", "prepareImage", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class u {
   public static final u.a a = new u.a((kotlin.d.b.i)null);

   private final Bitmap a(Bitmap var, boolean var, Matrix var) {
      int var = var.getWidth();
      int var = var.getHeight();
      Bitmap var;
      if(var == var) {
         var = var;
         if(var == null) {
            return var;
         }
      }

      int var = var;
      int var = var;
      if(var) {
         var = Math.min(var, var);
         var = var;
      }

      if(var == null) {
         var = Bitmap.createBitmap(var, 0, 0, var, var);
         kotlin.d.b.l.a(var, "Bitmap.createBitmap(bitmap, 0, 0, width, height)");
      } else {
         var = Bitmap.createBitmap(var, 0, 0, var, var, var, true);
         kotlin.d.b.l.a(var, "Bitmap.createBitmap(bitm…ht, rotationMatrix, true)");
      }

      return var;
   }

   private final Options a(SizeF var) {
      double var = Math.sqrt(7000000.0D) * 2.0D * (double)var.getWidth() / (double)(var.getWidth() + var.getHeight());
      Options var = new Options();
      if((double)var.getWidth() > var) {
         var.inScaled = true;
         var.inDensity = (int)var.getWidth();
         if(Math.floor((double)var.getWidth() / var) >= (double)2) {
            var.inSampleSize = 2;
            var.inTargetDensity = (int)var * var.inSampleSize;
         } else {
            var.inTargetDensity = (int)var;
         }
      }

      return var;
   }

   private final Matrix a(android.support.d.a var) {
      short var;
      switch(var.a("Orientation", 1)) {
      case 3:
         var = 180;
         break;
      case 4:
      case 5:
      case 7:
      default:
         var = 0;
         break;
      case 6:
         var = 90;
         break;
      case 8:
         var = 270;
      }

      Matrix var;
      if(var > 0) {
         var = new Matrix();
         var.postRotate((float)var);
      } else {
         var = null;
      }

      return var;
   }

   private final SizeF b(File var) {
      Options var = new Options();
      var.inJustDecodeBounds = true;
      BitmapFactory.decodeFile(var.getPath(), var);
      return new SizeF((float)var.outWidth, (float)var.outHeight);
   }

   private final SizeF b(byte[] var) {
      Options var = new Options();
      var.inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray(var, 0, var.length, var);
      return new SizeF((float)var.outWidth, (float)var.outHeight);
   }

   public final io.reactivex.n a(final File var) {
      kotlin.d.b.l.b(var, "file");
      io.reactivex.n var = io.reactivex.n.fromCallable((Callable)(new Callable() {
         public final Bitmap a() {
            SizeF varx = u.this.b(var);
            Bitmap var = BitmapFactory.decodeFile(var.getPath(), u.this.a(varx));
            Matrix var = u.this.a(new android.support.d.a(var.getPath()));
            u var = u.this;
            kotlin.d.b.l.a(var, "bitmap");
            return var.a(var, false, var);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.fromCallable … false, matrix)\n        }");
      return var;
   }

   public final io.reactivex.n a(final byte[] var) {
      kotlin.d.b.l.b(var, "pictureData");
      io.reactivex.n var = io.reactivex.n.fromCallable((Callable)(new Callable() {
         public final Bitmap a() {
            boolean varx = false;
            Options var = u.this.a(u.this.b(var));
            Bitmap var = BitmapFactory.decodeByteArray(var, 0, var.length, var);
            Matrix var = (Matrix)null;
            Closeable var = (Closeable)(new ByteArrayInputStream(var));
            boolean var = false;

            Matrix var;
            label102: {
               Exception var;
               try {
                  var = true;
                  ByteArrayInputStream var = (ByteArrayInputStream)var;
                  android.support.d.a var = new android.support.d.a((InputStream)var);
                  var = u.this.a(var);
                  kotlin.n var = kotlin.n.a;
                  var = false;
                  break label102;
               } catch (Exception var) {
                  var = var;
                  var = false;
               } finally {
                  if(var) {
                     if(!varx) {
                        var.close();
                     }

                  }
               }

               try {
                  try {
                     var.close();
                  } catch (Exception var) {
                     ;
                  }

                  throw (Throwable)var;
               } finally {
                  ;
               }
            }

            var.close();
            u var = u.this;
            kotlin.d.b.l.a(var, "bitmap");
            return var.a(var, true, var);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.fromCallable …rotationMatrix)\n        }");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer$Companion;", "", "()V", "MAX_IMAGE_PIXEL", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
