package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final f b;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(f var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(f var) {
      return new i(var);
   }

   public IdentityDocumentType a() {
      return (IdentityDocumentType)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
