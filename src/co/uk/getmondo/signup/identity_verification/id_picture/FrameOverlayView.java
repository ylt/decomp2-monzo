package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class FrameOverlayView extends View {
   private final int a;
   private final int b;
   private final int c;
   private final int d;
   private final int e;
   private final int f;
   private final Paint g;
   private final Paint h;
   private final Paint i;
   private final Path j;
   private IdentityDocumentType k;
   private boolean l;

   public FrameOverlayView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public FrameOverlayView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public FrameOverlayView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.a = co.uk.getmondo.common.k.n.b(16);
      this.b = co.uk.getmondo.common.k.n.b(62);
      this.c = co.uk.getmondo.common.k.n.b(2);
      this.d = co.uk.getmondo.common.k.n.b(75);
      this.e = co.uk.getmondo.common.k.n.b(16);
      this.f = co.uk.getmondo.common.k.n.b(3);
      this.j = new Path();
      this.k = IdentityDocumentType.PASSPORT;
      this.l = false;
      this.g = new Paint(1);
      this.g.setStyle(Style.STROKE);
      this.g.setColor(-1);
      this.g.setStrokeWidth((float)this.c);
      this.g.setPathEffect(new DashPathEffect(new float[]{20.0F, 10.0F}, 0.0F));
      this.i = new Paint();
      this.i.setAntiAlias(true);
      this.i.setStyle(Style.STROKE);
      this.h = new Paint();
      this.h.setColor(android.support.v4.content.a.c(var, 2131689526));
      this.h.setStyle(Style.FILL);
   }

   private void a(Canvas var) {
      if(this.l) {
         this.a(var, this.b, this.a);
      }

      this.b(var, this.b, this.a);
      var.drawPath(this.j, this.g);
   }

   private void a(Canvas var, int var, int var) {
      float var = (float)this.c / 2.0F;
      var.drawRect(0.0F, 0.0F, (float)var + var, (float)this.getHeight(), this.h);
      var.drawRect((float)(this.getWidth() - var) - var, 0.0F, (float)this.getWidth(), (float)this.getHeight(), this.h);
      var.drawRect((float)var, 0.0F, (float)(this.getWidth() - var), (float)var, this.h);
      var.drawRect((float)var, (float)(this.getHeight() - var), (float)(this.getWidth() - var), (float)this.getHeight(), this.h);
   }

   private void b(Canvas var) {
      if(this.l) {
         this.a(var, this.e, this.d);
      }

      this.b(var, this.e, this.d);
   }

   private void b(Canvas var, int var, int var) {
      var.drawRoundRect((float)var, (float)var, (float)(this.getWidth() - var), (float)(this.getHeight() - var), (float)this.f, (float)this.f, this.i);
   }

   protected void onDraw(Canvas var) {
      super.onDraw(var);
      if(this.k == IdentityDocumentType.PASSPORT) {
         this.a(var);
      } else {
         this.b(var);
      }

   }

   protected void onLayout(boolean var, int var, int var, int var, int var) {
      super.onLayout(var, var, var, var, var);
      if(this.k == IdentityDocumentType.PASSPORT) {
         this.j.reset();
         this.j.moveTo((float)this.b, (float)(this.getHeight() / 2));
         this.j.lineTo((float)(this.getWidth() - this.b), (float)(this.getHeight() / 2));
      }

   }

   public void setFrameIdType(IdentityDocumentType var) {
      this.k = var;
      this.i.setColor(-1);
      this.i.setStrokeWidth((float)this.c);
   }

   public void setOpaqueFrame(boolean var) {
      if(this.l != var) {
         this.l = var;
         this.invalidate();
      }

   }
}
