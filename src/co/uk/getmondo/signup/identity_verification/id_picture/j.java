package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import android.net.Uri;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;

public class j extends co.uk.getmondo.common.ui.b {
   private Uri c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final s f;
   private final io.reactivex.u g;
   private final co.uk.getmondo.signup.identity_verification.a.e h;
   private final co.uk.getmondo.common.a i;
   private final u j;
   private final co.uk.getmondo.signup.identity_verification.a.j k;
   private final IdentityDocumentType l;
   private final SignupSource m;
   private final String n;
   private final co.uk.getmondo.d.i o;

   j(io.reactivex.u var, io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.signup.identity_verification.a.e var, s var, co.uk.getmondo.common.a var, u var, co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var, String var) {
      this.d = var;
      this.e = var;
      this.g = var;
      this.h = var;
      this.f = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
      this.n = var;
      this.o = var;
   }

   // $FF: synthetic method
   static io.reactivex.r a(j var, j.a var, boolean var, Bitmap var) throws Exception {
      return var.a(var, var).subscribeOn(var.e);
   }

   // $FF: synthetic method
   static io.reactivex.r a(j var, byte[] var) throws Exception {
      return var.j.a(var).subscribeOn(var.g);
   }

   // $FF: synthetic method
   static void a(j.a var, Object var) throws Exception {
      var.b(false);
      var.c(false);
      var.h();
      var.j();
   }

   // $FF: synthetic method
   static void a(j var, j.a var, Bitmap var) throws Exception {
      var.i();
      var.a(var);
      var.e();
      var.a(var.f.a());
   }

   // $FF: synthetic method
   static void a(j var, j.a var, Uri var) throws Exception {
      var.c = var;
      var.c(true);
   }

   // $FF: synthetic method
   static void a(j var, j.a var, Object var) throws Exception {
      if(var.k == co.uk.getmondo.signup.identity_verification.a.j.b && var.l.b() && var.n == null) {
         var.a(var.k, var.l, var.o, var.m, var.c.getPath());
      } else {
         String var;
         if(var.n == null) {
            var = var.c.getPath();
         } else {
            var = var.n;
         }

         String var;
         if(var.n != null) {
            var = var.c.getPath();
         } else {
            var = null;
         }

         var.h.a(new co.uk.getmondo.signup.identity_verification.a.a.b(var, var.l, var.o, var), false);
         var.i.a(Impression.h(var.l.a()));
      }

      var.finish();
   }

   // $FF: synthetic method
   static void a(j var, j.a var, boolean var, Boolean var) throws Exception {
      if(var.booleanValue()) {
         var.g();
         var.f();
         var.b(true);
         var.a(var.f.a(var));
         if(var.n != null) {
            var.d(var.n);
         }
      } else {
         var.k();
      }

   }

   public void a(j.a var) {
      boolean var = true;
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.a(this.l.a(), false));
      boolean var;
      if(this.n == null) {
         var = true;
      } else {
         var = false;
      }

      var.f();
      var.a(this.f.a(var));
      if(!var) {
         var.d(this.n);
      }

      if(this.k == co.uk.getmondo.signup.identity_verification.a.j.b && this.l.b()) {
         if(this.n != null) {
            var = false;
         }

         var.a(var);
      }

      this.a((io.reactivex.b.b)var.b().subscribe(k.a(var)));
      this.a((io.reactivex.b.b)var.d().concatMap(l.a(this)).observeOn(this.d).doOnNext(m.a(this, var)).concatMap(n.a(this, var, var)).observeOn(this.d).subscribe(o.a(this, var)));
      this.a((io.reactivex.b.b)var.c().subscribe(p.a(this, var)));
      this.a((io.reactivex.b.b)var.a().subscribe(q.a(this, var, var)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      io.reactivex.n a(Bitmap var, boolean var);

      void a(Bitmap var);

      void a(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var, String var);

      void a(String var);

      void a(boolean var);

      io.reactivex.n b();

      void b(boolean var);

      io.reactivex.n c();

      void c(boolean var);

      io.reactivex.n d();

      void d(String var);

      void e();

      void f();

      void finish();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
