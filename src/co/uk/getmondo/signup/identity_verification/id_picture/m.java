package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;

// $FF: synthetic class
final class m implements io.reactivex.c.g {
   private final j a;
   private final j.a b;

   private m(j var, j.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(j var, j.a var) {
      return new m(var, var);
   }

   public void a(Object var) {
      j.a(this.a, this.b, (Bitmap)var);
   }
}
