package co.uk.getmondo.signup.identity_verification.id_picture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import com.google.android.cameraview.CameraView;
import java.io.File;
import java.io.FileOutputStream;

public class DocumentCameraActivity extends co.uk.getmondo.common.activities.b implements j.a {
   j a;
   co.uk.getmondo.signup.identity_verification.a.a b;
   private com.b.b.c c = com.b.b.c.a();
   @BindView(2131820977)
   ProgressBar cameraProgress;
   @BindView(2131821105)
   CameraView cameraView;
   @BindView(2131821108)
   TextView documentPositionTextView;
   private boolean e = false;
   @BindView(2131821107)
   FrameOverlayView frameOverlayView;
   @BindView(2131821104)
   TextView instructionsView;
   @BindView(2131821106)
   ImageView previewImage;
   @BindView(2131821109)
   ImageButton takePictureButton;
   @BindView(2131821111)
   ImageView thumbnailImageView;
   @BindView(2131821110)
   Button usePictureButton;

   public static Intent a(Context var, co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var) {
      return (new Intent(var, DocumentCameraActivity.class)).putExtra("KEY_VERSION", var).putExtra("KEY_ID_DOCUMENT_TYPE", var).putExtra("KEY_COUNTRY_CODE", var.e()).putExtra("KEY_SIGNUP_SOURCE", var);
   }

   public static Intent a(Context var, co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var, String var) {
      return a(var, var, var, var, var).putExtra("KEY_FRONT_PHOTO_PATH", var);
   }

   // $FF: synthetic method
   static Uri a(DocumentCameraActivity var, boolean var, Bitmap var) throws Exception {
      File var;
      if(var) {
         var = var.b.b();
      } else {
         var = var.b.c();
      }

      FileOutputStream var = new FileOutputStream(var);
      Object var = null;

      label141: {
         Throwable var;
         try {
            var.compress(CompressFormat.JPEG, 90, var);
            break label141;
         } catch (Throwable var) {
            var = var;
         } finally {
            ;
         }

         try {
            throw var;
         } finally {
            if(var != null) {
               if(var != null) {
                  try {
                     var.close();
                  } catch (Throwable var) {
                     ;
                  }
               } else {
                  var.close();
               }
            }

            throw var;
         }
      }

      if(var != null) {
         if(false) {
            try {
               var.close();
            } catch (Throwable var) {
               ;
            }
         } else {
            var.close();
         }
      }

      return Uri.fromFile(var);
   }

   // $FF: synthetic method
   static void a(DocumentCameraActivity var, com.google.android.cameraview.CameraView.a var) throws Exception {
      var.cameraView.b(var);
   }

   // $FF: synthetic method
   static void a(final DocumentCameraActivity var, final io.reactivex.o var) throws Exception {
      com.google.android.cameraview.CameraView.a var = new com.google.android.cameraview.CameraView.a() {
         public void a(CameraView varx, byte[] var) {
            var.a(var);
         }
      };
      var.cameraView.a(var);
      var.a(c.a(var, var));
   }

   private boolean v() {
      boolean var;
      if(android.support.v4.content.a.b(this, "android.permission.CAMERA") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public io.reactivex.n a() {
      return this.c;
   }

   public io.reactivex.n a(Bitmap var, boolean var) {
      return io.reactivex.n.fromCallable(b.a(this, var, var));
   }

   public void a(Bitmap var) {
      this.thumbnailImageView.setVisibility(8);
      this.previewImage.setImageBitmap(var);
      this.previewImage.setVisibility(0);
      this.frameOverlayView.setOpaqueFrame(true);
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var, String var) {
      this.startActivity(a(this, var, var, var, var, var));
   }

   public void a(String var) {
      this.instructionsView.setText(var);
   }

   public void a(boolean var) {
      TextView var = this.documentPositionTextView;
      int var;
      if(var) {
         var = 2131362262;
      } else {
         var = 2131362261;
      }

      var.setText(var);
      this.documentPositionTextView.setVisibility(0);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.takePictureButton);
   }

   public void b(boolean var) {
      this.takePictureButton.setEnabled(var);
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.usePictureButton);
   }

   public void c(boolean var) {
      this.usePictureButton.setEnabled(var);
   }

   public io.reactivex.n d() {
      return io.reactivex.n.create(a.a(this));
   }

   public void d(String var) {
      File var = new File(var);
      com.bumptech.glide.g.a((android.support.v4.app.j)this).a(var).a(new com.bumptech.glide.h.b(String.valueOf(var.lastModified()))).a(0.2F).a(this.thumbnailImageView);
      this.thumbnailImageView.setVisibility(0);
   }

   public void e() {
      this.usePictureButton.setVisibility(0);
      this.takePictureButton.setVisibility(8);
   }

   public void f() {
      this.usePictureButton.setVisibility(8);
      this.takePictureButton.setVisibility(0);
   }

   public void g() {
      this.previewImage.setVisibility(8);
      this.frameOverlayView.setOpaqueFrame(false);
   }

   public void h() {
      this.cameraProgress.setVisibility(0);
   }

   public void i() {
      this.cameraProgress.setVisibility(8);
   }

   public void j() {
      this.cameraView.g();
   }

   public void k() {
      super.onBackPressed();
   }

   public void onBackPressed() {
      boolean var;
      if(this.previewImage.getVisibility() == 0) {
         var = true;
      } else {
         var = false;
      }

      this.c.a((Object)Boolean.valueOf(var));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034214);
      ButterKnife.bind((Activity)this);
      this.thumbnailImageView.setBackgroundResource(2130838017);
      this.thumbnailImageView.setClipToOutline(true);
      android.support.v7.app.a var = this.getSupportActionBar();
      if(var != null) {
         var.b(true);
      }

      co.uk.getmondo.signup.identity_verification.a.j var = (co.uk.getmondo.signup.identity_verification.a.j)this.getIntent().getSerializableExtra("KEY_VERSION");
      if(var == null) {
         throw new RuntimeException("Identity verification flow version required");
      } else {
         co.uk.getmondo.d.i var = co.uk.getmondo.d.i.a(this.getIntent().getStringExtra("KEY_COUNTRY_CODE"));
         if(var == null) {
            var = co.uk.getmondo.d.i.UNITED_KINGDOM;
         }

         IdentityDocumentType var = IdentityDocumentType.PASSPORT;
         if(this.getIntent().hasExtra("KEY_ID_DOCUMENT_TYPE")) {
            var = (IdentityDocumentType)this.getIntent().getSerializableExtra("KEY_ID_DOCUMENT_TYPE");
         }

         SignupSource var = (SignupSource)this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
         String var = this.getIntent().getStringExtra("KEY_FRONT_PHOTO_PATH");
         this.l().a(new co.uk.getmondo.signup.identity_verification.p(var, var)).a(new f(var, var, var)).a(this);
         this.a.a((j.a)this);
         this.frameOverlayView.setFrameIdType(var);
         if(!this.v()) {
            android.support.v4.app.a.a(this, new String[]{"android.permission.CAMERA"}, 1);
         }

      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   public void onPause() {
      this.cameraView.c();
      super.onPause();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      if(var == 1) {
         if(var.length != 1 || var[0] != 0) {
            this.e = true;
         }
      } else {
         super.onRequestPermissionsResult(var, var, var);
      }

   }

   public void onResume() {
      super.onResume();
      if(this.v()) {
         this.cameraView.a();
      }

   }

   protected void onResumeFragments() {
      super.onResumeFragments();
      if(this.e) {
         co.uk.getmondo.common.d.a.a(this.getString(2131362302)).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
         this.e = false;
      }

   }
}
