package co.uk.getmondo.signup.identity_verification.id_picture;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final f b;

   static {
      boolean var;
      if(!h.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public h(f var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(f var) {
      return new h(var);
   }

   public String a() {
      return this.b.c();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
