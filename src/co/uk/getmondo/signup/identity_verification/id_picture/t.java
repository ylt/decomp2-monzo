package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public final class t implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!t.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public t(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new t(var, var);
   }

   public s a() {
      return new s((Context)this.b.b(), (IdentityDocumentType)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
