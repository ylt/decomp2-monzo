package co.uk.getmondo.signup.identity_verification.id_picture;

// $FF: synthetic class
final class c implements io.reactivex.c.f {
   private final DocumentCameraActivity a;
   private final com.google.android.cameraview.CameraView.a b;

   private c(DocumentCameraActivity var, com.google.android.cameraview.CameraView.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.f a(DocumentCameraActivity var, com.google.android.cameraview.CameraView.a var) {
      return new c(var, var);
   }

   public void a() {
      DocumentCameraActivity.a(this.a, this.b);
   }
}
