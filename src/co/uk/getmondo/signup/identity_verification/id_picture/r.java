package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;
   private final javax.a.a n;

   static {
      boolean var;
      if(!r.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public r(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                        if(!a && var == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var;
                           if(!a && var == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var;
                              if(!a && var == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var;
                                 if(!a && var == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var;
                                    if(!a && var == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var;
                                       if(!a && var == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var;
                                          if(!a && var == null) {
                                             throw new AssertionError();
                                          } else {
                                             this.n = var;
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new r(var, var, var, var, var, var, var, var, var, var, var, var, var);
   }

   public j a() {
      return (j)b.a.c.a(this.b, new j((io.reactivex.u)this.c.b(), (io.reactivex.u)this.d.b(), (io.reactivex.u)this.e.b(), (co.uk.getmondo.signup.identity_verification.a.e)this.f.b(), (s)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (u)this.i.b(), (co.uk.getmondo.signup.identity_verification.a.j)this.j.b(), (IdentityDocumentType)this.k.b(), (co.uk.getmondo.d.i)this.l.b(), (SignupSource)this.m.b(), (String)this.n.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
