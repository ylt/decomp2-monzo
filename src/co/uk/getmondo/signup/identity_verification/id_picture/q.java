package co.uk.getmondo.signup.identity_verification.id_picture;

// $FF: synthetic class
final class q implements io.reactivex.c.g {
   private final j a;
   private final j.a b;
   private final boolean c;

   private q(j var, j.a var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.g a(j var, j.a var, boolean var) {
      return new q(var, var, var);
   }

   public void a(Object var) {
      j.a(this.a, this.b, this.c, (Boolean)var);
   }
}
