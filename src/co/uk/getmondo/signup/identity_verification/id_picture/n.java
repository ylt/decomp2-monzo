package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final j a;
   private final j.a b;
   private final boolean c;

   private n(j var, j.a var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(j var, j.a var, boolean var) {
      return new n(var, var, var);
   }

   public Object a(Object var) {
      return j.a(this.a, this.b, this.c, (Bitmap)var);
   }
}
