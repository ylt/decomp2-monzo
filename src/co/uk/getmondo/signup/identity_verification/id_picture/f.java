package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class f {
   private final IdentityDocumentType a;
   private final co.uk.getmondo.d.i b;
   private final String c;

   public f(IdentityDocumentType var, co.uk.getmondo.d.i var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   IdentityDocumentType a() {
      return this.a;
   }

   co.uk.getmondo.d.i b() {
      return this.b;
   }

   String c() {
      return this.c;
   }
}
