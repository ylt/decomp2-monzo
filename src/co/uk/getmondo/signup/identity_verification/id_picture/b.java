package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final DocumentCameraActivity a;
   private final boolean b;
   private final Bitmap c;

   private b(DocumentCameraActivity var, boolean var, Bitmap var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static Callable a(DocumentCameraActivity var, boolean var, Bitmap var) {
      return new b(var, var, var);
   }

   public Object call() {
      return DocumentCameraActivity.a(this.a, this.b, this.c);
   }
}
