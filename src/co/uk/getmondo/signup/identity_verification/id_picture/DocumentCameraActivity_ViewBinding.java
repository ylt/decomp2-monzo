package co.uk.getmondo.signup.identity_verification.id_picture;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.cameraview.CameraView;

public class DocumentCameraActivity_ViewBinding implements Unbinder {
   private DocumentCameraActivity a;

   public DocumentCameraActivity_ViewBinding(DocumentCameraActivity var, View var) {
      this.a = var;
      var.cameraView = (CameraView)Utils.findRequiredViewAsType(var, 2131821105, "field 'cameraView'", CameraView.class);
      var.previewImage = (ImageView)Utils.findRequiredViewAsType(var, 2131821106, "field 'previewImage'", ImageView.class);
      var.takePictureButton = (ImageButton)Utils.findRequiredViewAsType(var, 2131821109, "field 'takePictureButton'", ImageButton.class);
      var.instructionsView = (TextView)Utils.findRequiredViewAsType(var, 2131821104, "field 'instructionsView'", TextView.class);
      var.frameOverlayView = (FrameOverlayView)Utils.findRequiredViewAsType(var, 2131821107, "field 'frameOverlayView'", FrameOverlayView.class);
      var.usePictureButton = (Button)Utils.findRequiredViewAsType(var, 2131821110, "field 'usePictureButton'", Button.class);
      var.cameraProgress = (ProgressBar)Utils.findRequiredViewAsType(var, 2131820977, "field 'cameraProgress'", ProgressBar.class);
      var.thumbnailImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821111, "field 'thumbnailImageView'", ImageView.class);
      var.documentPositionTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821108, "field 'documentPositionTextView'", TextView.class);
   }

   public void unbind() {
      DocumentCameraActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.cameraView = null;
         var.previewImage = null;
         var.takePictureButton = null;
         var.instructionsView = null;
         var.frameOverlayView = null;
         var.usePictureButton = null;
         var.cameraProgress = null;
         var.thumbnailImageView = null;
         var.documentPositionTextView = null;
      }
   }
}
