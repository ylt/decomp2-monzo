package co.uk.getmondo.signup.identity_verification.id_picture;

public final class d implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var) {
      return new d(var, var);
   }

   public void a(DocumentCameraActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (j)this.b.b();
         var.b = (co.uk.getmondo.signup.identity_verification.a.a)this.c.b();
      }
   }
}
