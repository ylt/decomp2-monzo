package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class s {
   private Context a;
   private final IdentityDocumentType b;

   s(Context var, IdentityDocumentType var) {
      this.a = var;
      this.b = var;
   }

   private String b() {
      return this.a.getString(this.b.c());
   }

   String a() {
      return this.a.getString(2131362256, new Object[]{this.b()});
   }

   String a(boolean var) {
      String var;
      if(!this.b.b()) {
         var = this.a.getString(2131362254, new Object[]{this.b()});
      } else {
         Context var = this.a;
         int var;
         if(var) {
            var = 2131362262;
         } else {
            var = 2131362261;
         }

         var = var.getString(var).toLowerCase();
         var = this.a.getString(2131362255, new Object[]{var, this.b()});
      }

      return var;
   }
}
