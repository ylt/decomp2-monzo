package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.d.ak;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class ab {
   // $FF: synthetic field
   public static final int[] a = new int[ak.a.values().length];
   // $FF: synthetic field
   public static final int[] b;

   static {
      a[ak.a.HAS_ACCOUNT.ordinal()] = 1;
      a[ak.a.INITIAL_TOPUP_REQUIRED.ordinal()] = 2;
      b = new int[IdentityVerification.Status.values().length];
      b[IdentityVerification.Status.NOT_STARTED.ordinal()] = 1;
      b[IdentityVerification.Status.PENDING_SUBMISSION.ordinal()] = 2;
      b[IdentityVerification.Status.APPROVED.ordinal()] = 3;
      b[IdentityVerification.Status.NOT_REQUIRED.ordinal()] = 4;
      b[IdentityVerification.Status.BLOCKED.ordinal()] = 5;
      b[IdentityVerification.Status.PENDING_REVIEW.ordinal()] = 6;
   }
}
