package co.uk.getmondo.signup.identity_verification;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.cameraview.CameraView;

public class SquareCameraView extends CameraView {
   public SquareCameraView(Context var) {
      super(var);
   }

   public SquareCameraView(Context var, AttributeSet var) {
      super(var, var);
   }

   public SquareCameraView(Context var, AttributeSet var, int var) {
      super(var, var, var);
   }

   protected void onMeasure(int var, int var) {
      super.onMeasure(var, var);
   }
}
