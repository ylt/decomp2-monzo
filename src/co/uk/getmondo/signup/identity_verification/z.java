package co.uk.getmondo.signup.identity_verification;

public final class z implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!z.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public z(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a a(javax.a.a var) {
      return new z(var);
   }

   public void a(VerificationPendingActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.b = (aa)this.b.b();
      }
   }
}
