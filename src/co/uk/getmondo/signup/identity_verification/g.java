package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB]\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "fileValidator", "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "rejectionReason", "", "allowSystemCamera", "", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/data/FileValidator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V", "pageViewTracked", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.signup.identity_verification.a.e g;
   private final co.uk.getmondo.signup.identity_verification.a.c h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.signup.identity_verification.a.j j;
   private final Impression.KycFrom k;
   private final String l;
   private boolean m;

   public g(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.signup.identity_verification.a.e var, co.uk.getmondo.signup.identity_verification.a.c var, co.uk.getmondo.common.a var, co.uk.getmondo.signup.identity_verification.a.j var, Impression.KycFrom var, String var, boolean var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "verificationManager");
      kotlin.d.b.l.b(var, "fileValidator");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "version");
      kotlin.d.b.l.b(var, "from");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
   }

   public void a(final g.a var) {
      final boolean var = false;
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      CharSequence var = (CharSequence)this.l;
      boolean var;
      if(var != null && !kotlin.h.j.a(var)) {
         var = false;
      } else {
         var = true;
      }

      if(!var) {
         var = true;
      }

      if(var) {
         String var = this.l;
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var.a(var);
      }

      io.reactivex.b.a var;
      io.reactivex.b.b var;
      io.reactivex.b.a var;
      io.reactivex.b.b var;
      if(this.j == co.uk.getmondo.signup.identity_verification.a.j.b) {
         var = this.b;
         var = var.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n varx) {
               kotlin.d.b.l.b(varx, "it");
               return var.g();
            }
         })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(Boolean varx) {
               kotlin.d.b.l.b(varx, "hasPassport");
               io.reactivex.h var;
               if(varx.booleanValue()) {
                  var = io.reactivex.h.a((Object)IdentityDocumentType.PASSPORT);
               } else {
                  var = var.h();
               }

               return var;
            }
         })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(final IdentityDocumentType varx) {
               kotlin.d.b.l.b(varx, "documentType");
               io.reactivex.h var;
               if(varx == IdentityDocumentType.NATIONAL_ID) {
                  var = io.reactivex.h.a((Object)(new kotlin.h(varx, Boolean.valueOf(false))));
               } else {
                  var = var.a(varx).c((io.reactivex.c.h)(new io.reactivex.c.h() {
                     public final kotlin.h a(Boolean varxx) {
                        kotlin.d.b.l.b(varxx, "isUk");
                        return new kotlin.h(varx, varxx);
                     }
                  }));
               }

               return var;
            }
         })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h varx) {
               IdentityDocumentType var = (IdentityDocumentType)varx.c();
               g.a var;
               if(((Boolean)varx.d()).booleanValue()) {
                  co.uk.getmondo.signup.identity_verification.a.j var;
                  if(g.this.m) {
                     var = var;
                     var = g.this.j;
                     kotlin.d.b.l.a(var, "documentType");
                     var.b(var, var, co.uk.getmondo.d.i.UNITED_KINGDOM);
                  } else {
                     var = var;
                     var = g.this.j;
                     kotlin.d.b.l.a(var, "documentType");
                     var.a(var, var, co.uk.getmondo.d.i.UNITED_KINGDOM);
                  }
               } else {
                  var = var;
                  kotlin.d.b.l.a(var, "documentType");
                  var.a(var, g.this.m);
               }

            }
         }), (io.reactivex.c.g)null.a);
         kotlin.d.b.l.a(var, "view.onTakePhotoClicked(…     }, { Timber.e(it) })");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h varx) {
               co.uk.getmondo.d.i var = (co.uk.getmondo.d.i)varx.c();
               IdentityDocumentType var = (IdentityDocumentType)varx.d();
               if(g.this.m) {
                  var.b(g.this.j, var, var);
               } else {
                  var.a(g.this.j, var, var);
               }

            }
         }));
         kotlin.d.b.l.a(var, "view.onDocumentCountrySe…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      } else {
         var = this.b;
         var = var.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n varx) {
               kotlin.d.b.l.b(varx, "it");
               return var.f();
            }
         })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(IdentityDocumentType varx) {
               g.a var = var;
               co.uk.getmondo.signup.identity_verification.a.j var = g.this.j;
               kotlin.d.b.l.a(varx, "idDocumentType");
               var.a(var, varx, co.uk.getmondo.d.i.UNITED_KINGDOM);
            }
         }), (io.reactivex.c.g)null.a);
         kotlin.d.b.l.a(var, "view.onTakePhotoClicked(…     }, { Timber.e(it) })");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      }

      var = this.b;
      var = var.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            if(g.this.m) {
               var.i();
            } else {
               var.a(g.this.j);
            }

         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onTakeVideoClicked(…en take video screen\") })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = co.uk.getmondo.common.j.f.a(this.g.b(), this.g.c()).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h varx) {
            boolean var = false;
            com.c.b.b var = (com.c.b.b)varx.c();
            com.c.b.b var = (com.c.b.b)varx.d();
            boolean var;
            if(var.b() && ((co.uk.getmondo.signup.identity_verification.a.a.b)var.a()).a(g.this.h, g.this.j)) {
               var = true;
            } else {
               var = false;
            }

            boolean varx = var;
            if(var.b()) {
               varx = var;
               if(g.this.h.a((String)var.a())) {
                  varx = true;
               }
            }

            if(var && varx) {
               var.l();
            } else {
               var.m();
            }

            if(var) {
               g.a var = var;
               Object var = var.a();
               kotlin.d.b.l.a(var, "identityDocument.get()");
               var.a((co.uk.getmondo.signup.identity_verification.a.a.b)var);
            } else {
               var.n();
            }

            if(varx) {
               g.a var = var;
               Object var = var.a();
               kotlin.d.b.l.a(var, "videoPath.get()");
               var.b((String)var);
            } else {
               var.o();
            }

            if(!g.this.c) {
               g.this.i.a(Impression.Companion.a(var, varx, var));
            }

         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "combineLatest(verificati…ate of submit button\") })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.c().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(kotlin.n varx) {
            kotlin.d.b.l.b(varx, "it");
            return var.d();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object varx) {
            return this.b(varx);
         }

         public final io.reactivex.h b(Object varx) {
            kotlin.d.b.l.b(varx, "it");
            return g.this.g.a().e().b(g.this.d).a(g.this.e).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.j();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = g.this.f;
                  kotlin.d.b.l.a(varx, "throwable");
                  if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362331);
                  }

               }
            })).f().a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(Boolean varx, Throwable var) {
                  var.k();
               }
            }));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            co.uk.getmondo.common.a var = g.this.i;
            Impression.Companion var = Impression.Companion;
            Impression.KycFrom var = g.this.k;
            if(var == null) {
               kotlin.d.b.l.a();
            }

            var.a(var.a(var, var.booleanValue()));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            var.a(g.this.j, g.this.k);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var, "view.onSubmitVerificatio…rification documents\") })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u001a\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\bH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u0018\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015H&J \u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J \u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H&J\u0018\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 H&J\b\u0010!\u001a\u00020\u0004H&J\u000e\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00150#H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\b\u0010'\u001a\u00020\u0004H&J\u0016\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00150#2\u0006\u0010\u0013\u001a\u00020\u000bH&J\u000e\u0010)\u001a\b\u0012\u0004\u0012\u00020\u000b0#H&J\b\u0010*\u001a\u00020\u0004H&J\u000e\u0010+\u001a\b\u0012\u0004\u0012\u00020\u000b0#H&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020.H&J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020.H&J\b\u00101\u001a\u00020\u0004H&¨\u00062"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "disableSubmitVerificationButton", "", "enableSubmitVerificationButton", "hideLoading", "onDocumentCountrySelected", "Lio/reactivex/Observable;", "Lkotlin/Pair;", "Lco/uk/getmondo/model/Country;", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "onPhotoIdTypeChosen", "onSubmitVerificationClicked", "onSubmitVerificationConfirmationClicked", "", "onTakePhotoClicked", "onTakeVideoClicked", "openCountrySelection", "documentType", "useSystemCamera", "", "openDocumentCamera", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "idDocumentType", "country", "openDocumentCameraFallback", "openTakeVideo", "openVerificationPending", "identityVerificationVersion", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "openVideoFallback", "showDoYouHavePassport", "Lio/reactivex/Maybe;", "showIdentityDocComplete", "identityDocument", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "showIdentityDocIncomplete", "showIsDocumentUk", "showLegacyIdentityDocumentSelection", "showLoading", "showOtherIdentityDocumentsSelection", "showRejectedReason", "message", "", "showVideoComplete", "videoPath", "showVideoIncomplete", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.h a(IdentityDocumentType var);

      io.reactivex.n a();

      void a(IdentityDocumentType var, boolean var);

      void a(co.uk.getmondo.signup.identity_verification.a.a.b var);

      void a(co.uk.getmondo.signup.identity_verification.a.j var);

      void a(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var);

      void a(co.uk.getmondo.signup.identity_verification.a.j var, Impression.KycFrom var);

      void a(String var);

      io.reactivex.n b();

      void b(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var);

      void b(String var);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.h f();

      io.reactivex.h g();

      io.reactivex.h h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();
   }
}
