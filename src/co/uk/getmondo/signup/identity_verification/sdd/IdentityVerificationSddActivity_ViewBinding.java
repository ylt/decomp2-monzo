package co.uk.getmondo.signup.identity_verification.sdd;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class IdentityVerificationSddActivity_ViewBinding implements Unbinder {
   private IdentityVerificationSddActivity a;

   public IdentityVerificationSddActivity_ViewBinding(IdentityVerificationSddActivity var, View var) {
      this.a = var;
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
      var.sddTitleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820970, "field 'sddTitleTextView'", TextView.class);
      var.sddBodyTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820971, "field 'sddBodyTextView'", TextView.class);
      var.sddVerifyButton = (Button)Utils.findRequiredViewAsType(var, 2131820972, "field 'sddVerifyButton'", Button.class);
      var.sddNotNowButton = (Button)Utils.findRequiredViewAsType(var, 2131820973, "field 'sddNotNowButton'", Button.class);
   }

   public void unbind() {
      IdentityVerificationSddActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.toolbar = null;
         var.sddTitleTextView = null;
         var.sddBodyTextView = null;
         var.sddVerifyButton = null;
         var.sddNotNowButton = null;
      }
   }
}
