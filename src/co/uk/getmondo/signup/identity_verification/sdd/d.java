package co.uk.getmondo.signup.identity_verification.sdd;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(c var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(c var) {
      return new d(var);
   }

   public String a() {
      return this.b.b();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
