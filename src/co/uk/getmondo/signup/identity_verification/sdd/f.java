package co.uk.getmondo.signup.identity_verification.sdd;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import io.reactivex.n;

class f extends co.uk.getmondo.common.ui.b {
   private final j c;
   private final co.uk.getmondo.common.a d;
   private final String e;

   f(j var, co.uk.getmondo.common.a var, String var) {
      this.c = var;
      this.d = var;
      this.e = var;
   }

   // $FF: synthetic method
   static void a(f var, f.a var, Object var) throws Exception {
      var.d.a(Impression.a(var.c));
      var.a("https://monzo.com/-webviews/identity-upgrade");
   }

   // $FF: synthetic method
   static void b(f var, f.a var, Object var) throws Exception {
      if(p.d(var.e)) {
         var.a(var.c.c());
      } else {
         var.b(var.c.c());
      }

   }

   public void a(f.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.d.a(this.c.a());
      if(this.c == j.b) {
         var.c();
      } else if(this.c == j.c) {
         var.d();
      }

      this.a((io.reactivex.b.b)var.a().subscribe(g.a(this, var)));
      this.a((io.reactivex.b.b)var.b().subscribe(h.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      n a();

      void a(Impression.KycFrom var);

      void a(String var);

      n b();

      void b(Impression.KycFrom var);

      void c();

      void d();
   }
}
