package co.uk.getmondo.signup.identity_verification.sdd;

public final class a implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!a.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public a(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a a(javax.a.a var) {
      return new a(var);
   }

   public void a(IdentityVerificationSddActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (f)this.b.b();
      }
   }
}
