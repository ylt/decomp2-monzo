package co.uk.getmondo.signup.identity_verification.sdd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import io.reactivex.n;

public class IdentityVerificationSddActivity extends co.uk.getmondo.common.activities.b implements f.a {
   f a;
   @BindView(2131820971)
   TextView sddBodyTextView;
   @BindView(2131820973)
   Button sddNotNowButton;
   @BindView(2131820970)
   TextView sddTitleTextView;
   @BindView(2131820972)
   Button sddVerifyButton;
   @BindView(2131820798)
   Toolbar toolbar;

   public static Intent a(Context var, j var) {
      return (new Intent(var, IdentityVerificationSddActivity.class)).putExtra("KEY_EXTRA_SDD_UPGRADE_TYPE", var);
   }

   public static Intent a(Context var, j var, String var) {
      return a(var, var).putExtra("KEY_EXTRA_REJECTION_NOTE", var);
   }

   public n a() {
      return com.b.a.c.c.a(this.sddVerifyButton);
   }

   public void a(Impression.KycFrom var) {
      this.startActivity(IdentityVerificationActivity.a(this, co.uk.getmondo.signup.identity_verification.a.j.b, var, SignupSource.SDD_MIGRATION));
   }

   public void a(String var) {
      (new android.support.b.a.a()).a(android.support.v4.content.a.c(this, 2131689487)).a().a(this, Uri.parse(var));
   }

   public n b() {
      return com.b.a.c.c.a(this.sddNotNowButton);
   }

   public void b(Impression.KycFrom var) {
      this.startActivity(IdentityVerificationActivity.a(this, co.uk.getmondo.signup.identity_verification.a.j.b, var, SignupSource.SDD_MIGRATION));
   }

   public void c() {
      this.sddBodyTextView.setText(2131362266);
   }

   public void d() {
      this.toolbar.setVisibility(8);
      this.sddTitleTextView.setText(2131362270);
      this.sddBodyTextView.setText(2131362267);
   }

   protected void onCreate(Bundle var) {
      j var = (j)this.getIntent().getSerializableExtra("KEY_EXTRA_SDD_UPGRADE_TYPE");
      int var;
      if(var == j.a) {
         var = 2131493156;
      } else {
         var = 2131493159;
      }

      this.setTheme(var);
      super.onCreate(var);
      this.setContentView(2131034180);
      ButterKnife.bind((Activity)this);
      if(var == null) {
         throw new IllegalArgumentException("IdentityVerificationSddActivity requires an SddUpgradeLevelType");
      } else {
         String var = this.getIntent().getStringExtra("KEY_EXTRA_REJECTION_NOTE");
         this.l().a(new c(var, var)).a(this);
         this.a.a((f.a)this);
      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
