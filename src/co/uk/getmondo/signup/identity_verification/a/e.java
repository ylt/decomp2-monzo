package co.uk.getmondo.signup.identity_verification.a;

import io.reactivex.n;
import io.reactivex.v;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007H&J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\nH&J\u0018\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\nH&J\u0014\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00040\u0003H&¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "", "identityDocument", "Lio/reactivex/Observable;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "status", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "submitEvidence", "", "useIdentityDocument", "", "evidence", "usedSystemCamera", "useVideoSelfie", "videoPath", "", "videoSelfiePath", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface e {
   v a();

   void a(co.uk.getmondo.signup.identity_verification.a.a.b var, boolean var);

   void a(String var, boolean var);

   n b();

   n c();

   v d();
}
