package co.uk.getmondo.signup.identity_verification.a;

import android.content.Context;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!i.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public i(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new i(var, var);
   }

   public h a() {
      return new h((Context)this.b.b(), (com.google.gson.f)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
