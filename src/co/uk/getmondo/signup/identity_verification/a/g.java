package co.uk.getmondo.signup.identity_verification.a;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new g(var);
   }

   public f a() {
      return new f((h)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
