package co.uk.getmondo.signup.identity_verification.a;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.identity_verification.ContentType;
import co.uk.getmondo.api.model.identity_verification.FileType;
import co.uk.getmondo.api.model.identity_verification.FileUpload;
import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.api.model.signup.SignupSource;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.z;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ4\u0010\r\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0014\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\u0014H\u0016J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u000eH\u0016J\u000e\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u000eH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0002J\u001e\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00120\u000e2\u0006\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00162\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0018\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0014\u0010'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00150\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006("},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/NewIdentityVerificationManager;", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "storage", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;", "api", "Lco/uk/getmondo/api/IdentityVerificationApi;", "fileUploader", "Lco/uk/getmondo/api/FileUploader;", "deleteFeedItemStorage", "Lco/uk/getmondo/common/DeleteFeedItemStorage;", "source", "Lco/uk/getmondo/api/model/signup/SignupSource;", "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/FileUploader;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/api/model/signup/SignupSource;)V", "fileWrapper", "Lio/reactivex/Single;", "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;", "kotlin.jvm.PlatformType", "path", "", "identityDocument", "Lio/reactivex/Observable;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "status", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "submitEvidence", "", "uploadDocuments", "Lio/reactivex/Completable;", "uploadFile", "filePath", "fileType", "Lco/uk/getmondo/api/model/identity_verification/FileType;", "useIdentityDocument", "", "evidence", "usedSystemCamera", "useVideoSelfie", "videoPath", "videoSelfiePath", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l implements e {
   private final h a;
   private final IdentityVerificationApi b;
   private final ae c;
   private final co.uk.getmondo.common.i d;
   private final SignupSource e;

   public l(h var, IdentityVerificationApi var, ae var, co.uk.getmondo.common.i var, SignupSource var) {
      kotlin.d.b.l.b(var, "storage");
      kotlin.d.b.l.b(var, "api");
      kotlin.d.b.l.b(var, "fileUploader");
      kotlin.d.b.l.b(var, "deleteFeedItemStorage");
      kotlin.d.b.l.b(var, "source");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   private final v a(final String var) {
      return v.c((Callable)(new Callable() {
         public final co.uk.getmondo.signup.identity_verification.a.a.a a() {
            return co.uk.getmondo.signup.identity_verification.a.a.a.a.a(new File(var));
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
   }

   private final v a(String var, final FileType var) {
      v var = this.a(var).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(co.uk.getmondo.signup.identity_verification.a.a.a var) {
            kotlin.d.b.l.b(var, "<name for destructuring parameter 0>");
            final FileInputStream var = var.c();
            String var = var.d();
            final long varx = var.e();
            final ContentType var = var.f();
            return l.this.b.requestFileUpload(l.this.e, var, var, var, varx).a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final v a(FileUpload var) {
                  kotlin.d.b.l.b(var, "<name for destructuring parameter 0>");
                  String varxx = var.a();
                  String var = var.b();
                  return l.this.c.a(var, (InputStream)var, varx, var.a()).a((Object)varxx);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "fileWrapper(filePath)\n  …      }\n                }");
      return var;
   }

   private final io.reactivex.b e() {
      final co.uk.getmondo.signup.identity_verification.a.a.b var = this.a.a();
      String var = this.a.c();
      io.reactivex.b var;
      if(var != null && var != null) {
         if(var.d() == null) {
            var = this.a(var.a(), FileType.IDENTITY_DOCUMENT).c((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(String varx) {
                  kotlin.d.b.l.b(varx, "fileId");
                  return IdentityVerificationApi.DefaultImpls.registerIdentityDocument$default(l.this.b, l.this.e, var.b(), var.c().e(), l.this.a.b(), varx, (String)null, 32, (Object)null);
               }
            }));
         } else {
            var = co.uk.getmondo.common.j.f.a(this.a(var.a(), FileType.IDENTITY_DOCUMENT), (z)this.a(var.d(), FileType.IDENTITY_DOCUMENT)).c((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(kotlin.h varx) {
                  kotlin.d.b.l.b(varx, "<name for destructuring parameter 0>");
                  String var = (String)varx.c();
                  String var = (String)varx.d();
                  return l.this.b.registerIdentityDocument(l.this.e, var.b(), var.c().e(), l.this.a.b(), var, var);
               }
            }));
         }

         var = var.d((io.reactivex.d)this.a(var, FileType.SELFIE_VIDEO).c((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.b a(String var) {
               kotlin.d.b.l.b(var, "it");
               return l.this.b.registerSelfieVideo(l.this.e, var, l.this.a.d());
            }
         }))).b((io.reactivex.d)this.b.submit(this.e));
         kotlin.d.b.l.a(var, "idEvidenceUpload.mergeWi…dThen(api.submit(source))");
      } else {
         var = io.reactivex.b.a((Throwable)(new IllegalStateException("Cannot submit evidence because ID evidence or video " + "path is missing | idEvidence: " + var + " videoPath " + var)));
         kotlin.d.b.l.a(var, "Completable.error(Illega…e videoPath $videoPath\"))");
      }

      return var;
   }

   public v a() {
      io.reactivex.b var = this.e();
      boolean var;
      if(this.a.b() && this.a.d()) {
         var = true;
      } else {
         var = false;
      }

      v var = var.a((Object)Boolean.valueOf(var)).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            h var = l.this.a;
            String var = UUID.randomUUID().toString();
            kotlin.d.b.l.a(var, "UUID.randomUUID().toString()");
            var.a(var);
            l.this.a.h();
            l.this.d.a();
         }
      }));
      kotlin.d.b.l.a(var, "uploadDocuments()\n      …tFlag()\n                }");
      return var;
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.a.b var, boolean var) {
      kotlin.d.b.l.b(var, "evidence");
      this.a.a(var, var);
   }

   public void a(String var, boolean var) {
      kotlin.d.b.l.b(var, "videoPath");
      this.a.a(var, var);
   }

   public n b() {
      return this.a.f();
   }

   public n c() {
      return this.a.g();
   }

   public v d() {
      v var = this.b.status(this.e).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(IdentityVerification var) {
            h var = l.this.a;
            kotlin.d.b.l.a(var, "it");
            var.a(var);
         }
      }));
      kotlin.d.b.l.a(var, "api.status(source).doOnS…dentityVerification(it) }");
      return var;
   }
}
