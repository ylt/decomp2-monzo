package co.uk.getmondo.signup.identity_verification.a;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "", "(Ljava/lang/String;I)V", "LEGACY", "NEW", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum j {
   a,
   b;

   static {
      j var = new j("LEGACY", 0);
      a = var;
      j var = new j("NEW", 1);
      b = var;
   }
}
