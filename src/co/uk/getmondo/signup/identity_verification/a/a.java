package co.uk.getmondo.signup.identity_verification.a;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public class a {
   private final Context a;

   a(Context var) {
      this.a = var;
   }

   public File a() {
      return new File(this.a.getFilesDir(), "selfie-video.mp4");
   }

   public File b() {
      return new File(this.a.getFilesDir(), "identity-document-primary.jpg");
   }

   public File c() {
      return new File(this.a.getFilesDir(), "identity-document-secondary.jpg");
   }

   public File d() throws IOException {
      return File.createTempFile("selfie-video", ".mp4", this.a.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
   }

   public File e() throws IOException {
      return File.createTempFile("identity-document", ".jpg", this.a.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
   }
}
