package co.uk.getmondo.signup.identity_verification.a;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.identity_verification.ApiUploadContainer;
import co.uk.getmondo.api.model.identity_verification.LegacyIdType;
import io.reactivex.n;
import io.reactivex.v;
import java.io.File;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u0000 &2\u00020\u0001:\u0001&B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011H\u0016J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\fH\u0016J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\fH\u0016J\b\u0010\u0018\u001a\u00020\u0019H\u0002J(\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u000fH\u0002J\u0018\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u0017H\u0016J\u0018\u0010#\u001a\u00020 2\u0006\u0010$\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020\u0017H\u0016J\u0014\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u00120\u0011H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006'"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/LegacyIdentityVerificationManager;", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "storage", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;", "identityVerificationApi", "Lco/uk/getmondo/api/IdentityVerificationApi;", "fileUploader", "Lco/uk/getmondo/api/FileUploader;", "deleteFeedItemStorage", "Lco/uk/getmondo/common/DeleteFeedItemStorage;", "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/FileUploader;Lco/uk/getmondo/common/DeleteFeedItemStorage;)V", "fileWrapper", "Lio/reactivex/Single;", "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;", "path", "", "identityDocument", "Lio/reactivex/Observable;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "status", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "submitEvidence", "", "uploadDocuments", "Lio/reactivex/Completable;", "uploadEvidence", "wrapper", "groupId", "mediaType", "evidenceType", "useIdentityDocument", "", "evidence", "usedSystemCamera", "useVideoSelfie", "videoPath", "videoSelfiePath", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k implements e {
   public static final k.a a = new k.a((kotlin.d.b.i)null);
   private final h b;
   private final IdentityVerificationApi c;
   private final ae d;
   private final co.uk.getmondo.common.i e;

   public k(h var, IdentityVerificationApi var, ae var, co.uk.getmondo.common.i var) {
      kotlin.d.b.l.b(var, "storage");
      kotlin.d.b.l.b(var, "identityVerificationApi");
      kotlin.d.b.l.b(var, "fileUploader");
      kotlin.d.b.l.b(var, "deleteFeedItemStorage");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   private final io.reactivex.b a(final co.uk.getmondo.signup.identity_verification.a.a.a var, final String var, final String var, final String var) {
      IdentityVerificationApi var = this.c;
      String var = UUID.randomUUID().toString();
      kotlin.d.b.l.a(var, "UUID.randomUUID().toString()");
      io.reactivex.b var = var.createKYCUploadUrl(var, var).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(ApiUploadContainer varx) {
            kotlin.d.b.l.b(varx, "upload");
            return k.this.d.a(varx.b(), (InputStream)var.a(), var.b(), var).b((io.reactivex.d)k.this.c.registerKYCUrl(varx.a(), var, var));
         }
      }));
      kotlin.d.b.l.a(var, "identityVerificationApi.…eType))\n                }");
      return var;
   }

   private final v a(final String var) {
      v var = v.c((Callable)(new Callable() {
         public final co.uk.getmondo.signup.identity_verification.a.a.a a() {
            return co.uk.getmondo.signup.identity_verification.a.a.a.a.a(new File(var));
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Single.fromCallable { Fi…er.fromFile(File(path)) }");
      return var;
   }

   private final io.reactivex.b e() {
      final co.uk.getmondo.signup.identity_verification.a.a.b var = this.b.a();
      String var = this.b.c();
      io.reactivex.b var;
      if(var != null && var != null) {
         v var = this.a(var.a());
         v var = this.a(var);
         final String var = UUID.randomUUID().toString();
         var = var.c((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.b a(co.uk.getmondo.signup.identity_verification.a.a.a varx) {
               kotlin.d.b.l.b(varx, "fileWrapper");
               k var = k.this;
               String var = var;
               kotlin.d.b.l.a(var, "groupId");
               return var.a(varx, var, "image/jpeg", LegacyIdType.Companion.a(var.b()).a());
            }
         })).d((io.reactivex.d)var.c((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.b a(co.uk.getmondo.signup.identity_verification.a.a.a var) {
               kotlin.d.b.l.b(var, "fileWrapper");
               k var = k.this;
               String var = var;
               kotlin.d.b.l.a(var, "groupId");
               return var.a(var, var, "video/mp4", "kyc_video_face");
            }
         }))).b((io.reactivex.c.a)(new io.reactivex.c.a() {
            public final void a() {
               h var = k.this.b;
               String var = var;
               kotlin.d.b.l.a(var, "groupId");
               var.a(var);
            }
         }));
         kotlin.d.b.l.a(var, "photoUpload.mergeWith(vi…ge.saveGroupId(groupId) }");
      } else {
         var = io.reactivex.b.a((Throwable)(new IllegalStateException("Cannot submit evidence because identity doc or video " + "path are missing | idEvidence: " + var + " videoPath " + var)));
         kotlin.d.b.l.a(var, "Completable.error(Illega…e videoPath $videoPath\"))");
      }

      return var;
   }

   public v a() {
      v var = this.e().a((Object)Boolean.valueOf(false)).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var) {
            k.this.b.h();
            k.this.e.a();
         }
      }));
      kotlin.d.b.l.a(var, "uploadDocuments()\n      …tFlag()\n                }");
      return var;
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.a.b var, boolean var) {
      kotlin.d.b.l.b(var, "evidence");
      this.b.a(var, false);
   }

   public void a(String var, boolean var) {
      kotlin.d.b.l.b(var, "videoPath");
      this.b.a(var, false);
   }

   public n b() {
      return this.b.f();
   }

   public n c() {
      return this.b.g();
   }

   public v d() {
      v var = this.c.kycStatus().d((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "identityVerificationApi.…oIdentityVerification() }");
      return var;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/data/LegacyIdentityVerificationManager$Companion;", "", "()V", "EVIDENCE_TYPE_VIDEO", "", "MIME_TYPE_IMAGE", "MIME_TYPE_VIDEO", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
