package co.uk.getmondo.signup.identity_verification.a;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import io.reactivex.n;
import java.io.File;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u0000 02\u00020\u0001:\u00010B\u0019\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\"\u001a\u00020#J\r\u0010$\u001a\u00020#H\u0000¢\u0006\u0002\b%J\b\u0010&\u001a\u00020#H\u0002J\u0012\u0010'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u00110(J\u000e\u0010)\u001a\u00020#2\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010*\u001a\u00020#2\u0006\u0010+\u001a\u00020\f2\u0006\u0010,\u001a\u00020\u0014J\u000e\u0010-\u001a\u00020#2\u0006\u0010\u0017\u001a\u00020\u0018J\u0016\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u00020\b2\u0006\u0010,\u001a\u00020\u0014J\u0012\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00110(R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b8F¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f8F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eRJ\u0010\u000f\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\f \u0012*\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00110\u0011 \u0012*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\f \u0012*\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0013\u001a\u00020\u00148F¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u00188F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\b8F¢\u0006\u0006\u001a\u0004\b\u001e\u0010\nRJ\u0010\u001f\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b \u0012*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00110\u0011 \u0012*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b \u0012*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010 \u001a\u00020\u00148F¢\u0006\u0006\u001a\u0004\b!\u0010\u0016¨\u00061"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;", "", "context", "Landroid/content/Context;", "gson", "Lcom/google/gson/Gson;", "(Landroid/content/Context;Lcom/google/gson/Gson;)V", "groupId", "", "getGroupId", "()Ljava/lang/String;", "identityDocument", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "getIdentityDocument", "()Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "identityDocumentRelay", "Lcom/jakewharton/rxrelay2/BehaviorRelay;", "Lcom/memoizrlabs/poweroptional/Optional;", "kotlin.jvm.PlatformType", "identityDocumentUsedSystemCamera", "", "getIdentityDocumentUsedSystemCamera", "()Z", "identityVerification", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "getIdentityVerification", "()Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "sharedPreferences", "Landroid/content/SharedPreferences;", "videoPath", "getVideoPath", "videoPathRelay", "videoUsedSystemCamera", "getVideoUsedSystemCamera", "clearAll", "", "deleteDocuments", "deleteDocuments$app_monzoPrepaidRelease", "deleteFiles", "identityDocumentEvidence", "Lio/reactivex/Observable;", "saveGroupId", "saveIdentityDocumentEvidence", "evidence", "usedSystemCamera", "saveIdentityVerification", "saveVideoEvidence", "path", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class h {
   public static final h.a a = new h.a((kotlin.d.b.i)null);
   private final com.b.b.b b;
   private final com.b.b.b c;
   private final SharedPreferences d;
   private final Context e;
   private final com.google.gson.f f;

   public h(Context var, com.google.gson.f var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "gson");
      super();
      this.e = var;
      this.f = var;
      this.b = com.b.b.b.a();
      this.c = com.b.b.b.a();
      SharedPreferences var = this.e.getSharedPreferences("identity_verification", 0);
      kotlin.d.b.l.a(var, "context.getSharedPrefere…ME, Context.MODE_PRIVATE)");
      this.d = var;
      co.uk.getmondo.signup.identity_verification.a.a.b var = this.a();
      String var = this.c();
      this.b.a((Object)com.c.b.b.b(var));
      this.c.a((Object)com.c.b.b.b(var));
   }

   private final void j() {
      co.uk.getmondo.signup.identity_verification.a.a.b var = this.a();
      if(var != null) {
         (new File(var.a())).delete();
         if(var.d() != null) {
            (new File(var.d())).delete();
         }
      }

      String var = this.c();
      if(var != null) {
         (new File(var)).delete();
      }

   }

   public final co.uk.getmondo.signup.identity_verification.a.a.b a() {
      Object var = null;
      int var = this.d.getInt("photo_id_type", -1);
      if(var >= 0) {
         this.d.edit().putString("identity_document_type", IdentityDocumentType.values()[var].a()).remove("photo_id_type").apply();
      }

      String var = this.d.getString("photo_id_path", (String)null);
      IdentityDocumentType var = IdentityDocumentType.Companion.a(this.d.getString("identity_document_type", (String)null));
      String var = this.d.getString("secondary_photo_id_path", (String)null);
      co.uk.getmondo.d.i var = co.uk.getmondo.d.i.Companion.a(this.d.getString("identity_document_country", (String)null));
      co.uk.getmondo.signup.identity_verification.a.a.b var = (co.uk.getmondo.signup.identity_verification.a.a.b)var;
      if(var != null) {
         var = (co.uk.getmondo.signup.identity_verification.a.a.b)var;
         if(var != null) {
            if(var == null) {
               var = (co.uk.getmondo.signup.identity_verification.a.a.b)var;
            } else {
               var = new co.uk.getmondo.signup.identity_verification.a.a.b(var, var, var, var);
            }
         }
      }

      return var;
   }

   public final void a(IdentityVerification var) {
      kotlin.d.b.l.b(var, "identityVerification");
      this.d.edit().putString("identity_verification", this.f.a(var)).apply();
   }

   public final void a(co.uk.getmondo.signup.identity_verification.a.a.b var, boolean var) {
      kotlin.d.b.l.b(var, "evidence");
      this.d.edit().putString("photo_id_path", var.a()).putString("identity_document_country", var.c().e()).putString("identity_document_type", var.b().a()).putString("secondary_photo_id_path", var.d()).putBoolean("id_used_system_camera", var).apply();
      this.b.a((Object)com.c.b.b.b(var));
   }

   public final void a(String var) {
      kotlin.d.b.l.b(var, "groupId");
      this.d.edit().putString("group_id", var).apply();
   }

   public final void a(String var, boolean var) {
      kotlin.d.b.l.b(var, "path");
      this.d.edit().putString("video_id_path", var).putBoolean("video_used_system_camera", var).apply();
      this.c.a((Object)com.c.b.b.b(var));
   }

   public final boolean b() {
      return this.d.getBoolean("id_used_system_camera", false);
   }

   public final String c() {
      return this.d.getString("video_id_path", (String)null);
   }

   public final boolean d() {
      return this.d.getBoolean("video_used_system_camera", false);
   }

   public final String e() {
      return this.d.getString("group_id", (String)null);
   }

   public final n f() {
      com.b.b.b var = this.b;
      kotlin.d.b.l.a(var, "identityDocumentRelay");
      return (n)var;
   }

   public final n g() {
      com.b.b.b var = this.c;
      kotlin.d.b.l.a(var, "videoPathRelay");
      return (n)var;
   }

   public final void h() {
      this.j();
      this.d.edit().remove("photo_id_path").remove("secondary_photo_id_path").remove("identity_document_type").remove("identity_document_country").remove("id_used_system_camera").remove("video_id_path").remove("video_used_system_camera").remove("photo_id_type").apply();
   }

   public final void i() {
      this.d.edit().clear().apply();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage$Companion;", "", "()V", "KEY_GROUP_ID", "", "KEY_IDENTITY_DOCUMENT_COUNTRY", "KEY_IDENTITY_DOCUMENT_TYPE", "KEY_IDENTITY_DOCUMENT_USED_SYSTEM_CAMERA", "KEY_IDENTITY_VERIFICATION", "KEY_LEGACY_PHOTO_ID_TYPE_ORDINAL", "KEY_PHOTO_ID_PATH", "KEY_SECONDARY_PHOTO_ID_PATH", "KEY_VIDEO_ID_PATH", "KEY_VIDEO_USED_SYSTEM_CAMERA", "NAME", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }
   }
}
