package co.uk.getmondo.signup.identity_verification.a.a;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.d.i;
import co.uk.getmondo.signup.identity_verification.a.c;
import co.uk.getmondo.signup.identity_verification.a.j;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003HÆ\u0003J3\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\u0016\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fJ\t\u0010 \u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006!"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "", "primaryPhotoPath", "", "type", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "country", "Lco/uk/getmondo/model/Country;", "secondaryPhotoPath", "(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/model/Country;Ljava/lang/String;)V", "getCountry", "()Lco/uk/getmondo/model/Country;", "getPrimaryPhotoPath", "()Ljava/lang/String;", "getSecondaryPhotoPath", "getType", "()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "isValid", "fileValidator", "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final String a;
   private final IdentityDocumentType b;
   private final i c;
   private final String d;

   public b(String var, IdentityDocumentType var, i var, String var) {
      l.b(var, "primaryPhotoPath");
      l.b(var, "type");
      l.b(var, "country");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
   }

   public final String a() {
      return this.a;
   }

   public final boolean a(c var, j var) {
      l.b(var, "fileValidator");
      l.b(var, "version");
      boolean var;
      if(l.a(var, j.a)) {
         var = var.a(this.a);
      } else if(l.a(this.b, IdentityDocumentType.PASSPORT)) {
         var = var.a(this.a);
      } else if(var.a(this.a) && var.a(this.d)) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final IdentityDocumentType b() {
      return this.b;
   }

   public final i c() {
      return this.c;
   }

   public final String d() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label32: {
            if(var instanceof b) {
               b var = (b)var;
               if(l.a(this.a, var.a) && l.a(this.b, var.b) && l.a(this.c, var.c) && l.a(this.d, var.d)) {
                  break label32;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      IdentityDocumentType var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      i var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + var * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "IdentityDocument(primaryPhotoPath=" + this.a + ", type=" + this.b + ", country=" + this.c + ", secondaryPhotoPath=" + this.d + ")";
   }
}
