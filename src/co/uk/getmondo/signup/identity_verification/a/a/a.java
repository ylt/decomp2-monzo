package co.uk.getmondo.signup.identity_verification.a.a;

import co.uk.getmondo.api.model.identity_verification.ContentType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0016\u001a\u00020\tHÆ\u0003J1\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;", "", "inputStream", "Ljava/io/FileInputStream;", "name", "", "length", "", "contentType", "Lco/uk/getmondo/api/model/identity_verification/ContentType;", "(Ljava/io/FileInputStream;Ljava/lang/String;JLco/uk/getmondo/api/model/identity_verification/ContentType;)V", "getContentType", "()Lco/uk/getmondo/api/model/identity_verification/ContentType;", "getInputStream", "()Ljava/io/FileInputStream;", "getLength", "()J", "getName", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Create", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.a a = new a.a((i)null);
   private final FileInputStream b;
   private final String c;
   private final long d;
   private final ContentType e;

   public a(FileInputStream var, String var, long var, ContentType var) {
      l.b(var, "inputStream");
      l.b(var, "name");
      l.b(var, "contentType");
      super();
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public final FileInputStream a() {
      return this.b;
   }

   public final long b() {
      return this.d;
   }

   public final FileInputStream c() {
      return this.b;
   }

   public final String d() {
      return this.c;
   }

   public final long e() {
      return this.d;
   }

   public boolean equals(Object var) {
      boolean var = false;
      boolean var;
      if(this != var) {
         var = var;
         if(!(var instanceof a)) {
            return var;
         }

         a var = (a)var;
         var = var;
         if(!l.a(this.b, var.b)) {
            return var;
         }

         var = var;
         if(!l.a(this.c, var.c)) {
            return var;
         }

         boolean var;
         if(this.d == var.d) {
            var = true;
         } else {
            var = false;
         }

         var = var;
         if(!var) {
            return var;
         }

         var = var;
         if(!l.a(this.e, var.e)) {
            return var;
         }
      }

      var = true;
      return var;
   }

   public final ContentType f() {
      return this.e;
   }

   public int hashCode() {
      int var = 0;
      FileInputStream var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      String var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      long var = this.d;
      int var = (int)(var ^ var >>> 32);
      ContentType var = this.e;
      if(var != null) {
         var = var.hashCode();
      }

      return ((var + var * 31) * 31 + var) * 31 + var;
   }

   public String toString() {
      return "FileWrapper(inputStream=" + this.b + ", name=" + this.c + ", length=" + this.d + ", contentType=" + this.e + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper$Create;", "", "()V", "fromFile", "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;", "file", "Ljava/io/File;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final a a(File var) throws FileNotFoundException {
         l.b(var, "file");
         FileInputStream var = new FileInputStream(var);
         String var = var.getName();
         l.a(var, "file.name");
         return new a(var, var, var.length(), ContentType.Companion.a(kotlin.io.b.a(var)));
      }
   }
}
