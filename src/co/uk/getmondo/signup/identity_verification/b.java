package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

// $FF: synthetic class
final class b implements kotlin.d.a.b {
   private final CountrySelectionActivity a;
   private final IdentityDocumentType b;

   private b(CountrySelectionActivity var, IdentityDocumentType var) {
      this.a = var;
      this.b = var;
   }

   public static kotlin.d.a.b a(CountrySelectionActivity var, IdentityDocumentType var) {
      return new b(var, var);
   }

   public Object a(Object var) {
      return CountrySelectionActivity.a(this.a, this.b, (co.uk.getmondo.d.i)var);
   }
}
