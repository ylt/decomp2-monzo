package co.uk.getmondo.signup.identity_verification;

import android.view.View;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class IdentityApprovedActivity_ViewBinding implements Unbinder {
   private IdentityApprovedActivity a;
   private View b;

   public IdentityApprovedActivity_ViewBinding(final IdentityApprovedActivity var, View var) {
      this.a = var;
      var = Utils.findRequiredView(var, 2131820965, "method 'onReturnToHomeClicked'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onReturnToHomeClicked();
         }
      });
   }

   public void unbind() {
      if(this.a == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
