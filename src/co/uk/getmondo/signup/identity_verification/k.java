package co.uk.getmondo.signup.identity_verification;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final j b;

   static {
      boolean var;
      if(!k.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public k(j var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(j var) {
      return new k(var);
   }

   public Boolean a() {
      return (Boolean)b.a.d.a(Boolean.valueOf(this.b.c()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
