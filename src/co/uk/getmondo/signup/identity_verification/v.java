package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "profileManager", "Lco/uk/getmondo/profile/data/ProfileManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/profile/data/ProfileManager;)V", "allowSystemCamera", "", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class v extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.signup.identity_verification.a.e g;
   private final co.uk.getmondo.profile.data.a h;

   public v(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.signup.identity_verification.a.e var, co.uk.getmondo.profile.data.a var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "verificationManager");
      kotlin.d.b.l.b(var, "profileManager");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   public void a(final v.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.c().startWith((Object)kotlin.n.a).doOnSubscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(io.reactivex.b.b varx) {
            var.f();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n varx) {
            kotlin.d.b.l.b(varx, "it");
            return co.uk.getmondo.common.j.f.a(v.this.g.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.v a(IdentityVerification varx) {
                  kotlin.d.b.l.b(varx, "it");
                  io.reactivex.v var;
                  if(kotlin.d.b.l.a(varx.b(), IdentityVerification.Status.NOT_STARTED)) {
                     var = v.this.h.a().a((Object)varx);
                  } else {
                     var = io.reactivex.v.a((Object)varx);
                  }

                  return var;
               }
            })).b(v.this.d).a(v.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.g();
                  co.uk.getmondo.signup.i var = co.uk.getmondo.signup.i.a;
                  kotlin.d.b.l.a(varx, "it");
                  if(!var.a(varx, (co.uk.getmondo.signup.i.a)var) && !v.this.f.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(IdentityVerification varx) {
            var.g();
            v.this.c = varx.c();
            IdentityVerification.Status var = varx.b();
            if(var != null) {
               switch(w.a[var.ordinal()]) {
               case 1:
                  var.h();
                  return;
               case 2:
                  var.a(varx.c(), varx.a());
                  return;
               case 3:
                  var.i();
                  return;
               }
            }

            var.j();
         }
      }));
      kotlin.d.b.l.a(var, "view.onRetryClicked\n    …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.a(v.this.c, (String)null);
         }
      }));
      kotlin.d.b.l.a(var, "view.onOnboardingComplet…llowSystemCamera, null) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000b\u001a\u00020\u0006H&J\b\u0010\f\u001a\u00020\u0006H&J\b\u0010\r\u001a\u00020\u0006H&J\u001a\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H&J\b\u0010\u0013\u001a\u00020\u0006H&J\b\u0010\u0014\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\b¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onOnboardingComplete", "Lio/reactivex/Observable;", "", "getOnOnboardingComplete", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "hideLoading", "openBlocked", "openGenericError", "showIdentityDocuments", "allowSystemCamera", "", "rejectionNote", "", "showLoading", "showOnboarding", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      void a(boolean var, String var);

      io.reactivex.n c();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
