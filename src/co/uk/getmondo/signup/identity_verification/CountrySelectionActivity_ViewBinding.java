package co.uk.getmondo.signup.identity_verification;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CountrySelectionActivity_ViewBinding implements Unbinder {
   private CountrySelectionActivity a;

   public CountrySelectionActivity_ViewBinding(CountrySelectionActivity var, View var) {
      this.a = var;
      var.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var, 2131820862, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      CountrySelectionActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.recyclerView = null;
      }
   }
}
