package co.uk.getmondo.signup.identity_verification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.signup.identity_verification.fallback.DocumentFallbackActivity;
import co.uk.getmondo.signup.identity_verification.fallback.VideoFallbackActivity;
import co.uk.getmondo.signup.identity_verification.id_picture.DocumentCameraActivity;
import co.uk.getmondo.signup.identity_verification.video.VideoRecordingActivity;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Ä\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 p2\u00020\u00012\u00020\u0002:\u0002pqB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010/\u001a\u000200H\u0016J\b\u00101\u001a\u000200H\u0016J\b\u00102\u001a\u000200H\u0016J\u001a\u00103\u001a\u0002002\b\u00104\u001a\u0004\u0018\u00010$2\u0006\u00105\u001a\u000206H\u0002J\"\u00107\u001a\u0002002\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u0002092\b\u0010;\u001a\u0004\u0018\u00010<H\u0016J\u0010\u0010=\u001a\u0002002\u0006\u0010>\u001a\u00020?H\u0016J\u0012\u0010@\u001a\u0002002\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J$\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020F2\b\u0010G\u001a\u0004\u0018\u00010H2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\b\u0010I\u001a\u000200H\u0016J\u001a\u0010J\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\f0KH\u0016J\u000e\u0010L\u001a\b\u0012\u0004\u0012\u00020\u000e0KH\u0016J\b\u0010M\u001a\u000200H\u0016J\u000e\u0010N\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010O\u001a\b\u0012\u0004\u0012\u00020P0KH\u0016J\u000e\u0010Q\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010R\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u001a\u0010S\u001a\u0002002\u0006\u0010T\u001a\u00020D2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0018\u0010U\u001a\u0002002\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010W\u001a\u00020\u0005H\u0016J \u0010X\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010Z\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J \u0010\\\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J\u0010\u0010]\u001a\u0002002\u0006\u0010Y\u001a\u00020\u0011H\u0016J\u0018\u0010^\u001a\u0002002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010_\u001a\u00020\u0016H\u0016J\b\u0010`\u001a\u000200H\u0016J\u000e\u0010a\u001a\b\u0012\u0004\u0012\u00020\u00050bH\u0017J\u0010\u0010c\u001a\u0002002\u0006\u0010d\u001a\u00020eH\u0016J\b\u0010f\u001a\u000200H\u0016J\u0016\u0010g\u001a\b\u0012\u0004\u0012\u00020\u00050b2\u0006\u0010V\u001a\u00020\u000eH\u0016J\u000e\u0010h\u001a\b\u0012\u0004\u0012\u00020\u000e0bH\u0017J\b\u0010i\u001a\u000200H\u0016J\u000e\u0010j\u001a\b\u0012\u0004\u0012\u00020\u000e0bH\u0017J\u0010\u0010k\u001a\u0002002\u0006\u0010l\u001a\u00020$H\u0016J\u0010\u0010m\u001a\u0002002\u0006\u0010n\u001a\u00020$H\u0016J\b\u0010o\u001a\u000200H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007Rb\u0010\n\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f0\f \u000f**\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f0\f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\t\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\t\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.¢\u0006\u0002\n\u0000R2\u0010\u001c\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u001d\u001a\u00020\u001e8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001d\u0010#\u001a\u0004\u0018\u00010$8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010\t\u001a\u0004\b%\u0010&R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u0082\u000e¢\u0006\u0002\n\u0000R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010\t\u001a\u0004\b,\u0010-¨\u0006r"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "()V", "allowSystemCamera", "", "getAllowSystemCamera", "()Z", "allowSystemCamera$delegate", "Lkotlin/Lazy;", "documentCountryRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lkotlin/Pair;", "Lco/uk/getmondo/model/Country;", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "kotlin.jvm.PlatformType", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "getIdentityVerificationVersion", "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "identityVerificationVersion$delegate", "kycFrom", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "getKycFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "kycFrom$delegate", "listener", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;", "photoIdRelay", "presenter", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;)V", "rejectedReason", "", "getRejectedReason", "()Ljava/lang/String;", "rejectedReason$delegate", "rejectedReasonSnackbar", "Landroid/support/design/widget/Snackbar;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "getSignupSource", "()Lco/uk/getmondo/api/model/signup/SignupSource;", "signupSource$delegate", "disableSubmitVerificationButton", "", "enableSubmitVerificationButton", "hideLoading", "loadThumbnail", "path", "imageView", "Landroid/widget/ImageView;", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onDocumentCountrySelected", "Lio/reactivex/Observable;", "onPhotoIdTypeChosen", "onStop", "onSubmitVerificationClicked", "onSubmitVerificationConfirmationClicked", "", "onTakePhotoClicked", "onTakeVideoClicked", "onViewCreated", "view", "openCountrySelection", "documentType", "useSystemCamera", "openDocumentCamera", "version", "idDocumentType", "country", "openDocumentCameraFallback", "openTakeVideo", "openVerificationPending", "from", "openVideoFallback", "showDoYouHavePassport", "Lio/reactivex/Maybe;", "showIdentityDocComplete", "identityDocument", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "showIdentityDocIncomplete", "showIsDocumentUk", "showLegacyIdentityDocumentSelection", "showLoading", "showOtherIdentityDocumentsSelection", "showRejectedReason", "message", "showVideoComplete", "videoPath", "showVideoIncomplete", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "identityVerificationVersion", "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "kycFrom", "getKycFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "rejectedReason", "getRejectedReason()Ljava/lang/String;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "allowSystemCamera", "getAllowSystemCamera()Z")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "signupSource", "getSignupSource()Lco/uk/getmondo/api/model/signup/SignupSource;"))};
   public static final e.a d = new e.a((kotlin.d.b.i)null);
   public g c;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private final kotlin.c g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.signup.identity_verification.a.j b() {
         Serializable var = e.this.getArguments().getSerializable("KEY_VERSION");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
         } else {
            return (co.uk.getmondo.signup.identity_verification.a.j)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c h = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final Impression.KycFrom b() {
         Serializable var = e.this.getArguments().getSerializable("KEY_ENTRY_POINT");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.tracking.Impression.KycFrom");
         } else {
            return (Impression.KycFrom)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c i = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return e.this.getArguments().getString("KEY_REJECTION_REASON");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c j = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final boolean b() {
         return e.this.getArguments().getBoolean("KEY_ALLOW_SYSTEM_CAMERA", false);
      }

      // $FF: synthetic method
      public Object v_() {
         return Boolean.valueOf(this.b());
      }
   }));
   private final kotlin.c k = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final SignupSource b() {
         Serializable var = e.this.getArguments().getSerializable("KEY_SIGNUP_SOURCE");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
         } else {
            return (SignupSource)var;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private e.b l;
   private Snackbar m;
   private HashMap n;

   private final void a(String var, ImageView var) {
      File var = new File(var);
      com.bumptech.glide.g.a((Fragment)this).a(var).a((com.bumptech.glide.load.c)(new com.bumptech.glide.h.b(String.valueOf(var.lastModified())))).a(0.2F).a(var);
   }

   private final co.uk.getmondo.signup.identity_verification.a.j r() {
      kotlin.c var = this.g;
      kotlin.reflect.l var = a[0];
      return (co.uk.getmondo.signup.identity_verification.a.j)var.a();
   }

   private final Impression.KycFrom s() {
      kotlin.c var = this.h;
      kotlin.reflect.l var = a[1];
      return (Impression.KycFrom)var.a();
   }

   private final String t() {
      kotlin.c var = this.i;
      kotlin.reflect.l var = a[2];
      return (String)var.a();
   }

   private final boolean v() {
      kotlin.c var = this.j;
      kotlin.reflect.l var = a[3];
      return ((Boolean)var.a()).booleanValue();
   }

   private final SignupSource w() {
      kotlin.c var = this.k;
      kotlin.reflect.l var = a[4];
      return (SignupSource)var.a();
   }

   public View a(int var) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var = (View)this.n.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.n.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public io.reactivex.h a(IdentityDocumentType var) {
      kotlin.d.b.l.b(var, "documentType");
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b(this.getString(var.c())) {
         // $FF: synthetic field
         final String b;

         {
            this.b = var;
         }

         public final android.support.v7.app.d a(final kotlin.d.a.b var) {
            kotlin.d.b.l.b(var, "setResult");
            android.support.v7.app.d var = (new android.support.v7.app.d.a(e.this.getContext(), 2131493135)).a((CharSequence)e.this.getString(2131362161, new Object[]{co.uk.getmondo.common.k.p.i(this.b)})).b((CharSequence)e.this.getString(2131362160, new Object[]{this.b})).a(2131362159, (OnClickListener)(new OnClickListener() {
               public final void onClick(DialogInterface varx, int var) {
                  var.a(Boolean.valueOf(true));
               }
            })).b(2131362158, (OnClickListener)(new OnClickListener() {
               public final void onClick(DialogInterface varx, int var) {
                  var.a(Boolean.valueOf(false));
               }
            })).a(false).b();
            kotlin.d.b.l.a(var, "AlertDialog.Builder(cont…                .create()");
            return var;
         }
      }));
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(IdentityDocumentType var, boolean var) {
      kotlin.d.b.l.b(var, "documentType");
      this.startActivityForResult(CountrySelectionActivity.a(this.getContext(), var), 1001);
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.a.b var) {
      kotlin.d.b.l.b(var, "identityDocument");
      ((ImageView)this.a(co.uk.getmondo.c.a.photoCompleteImageView)).setImageResource(2130837954);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setText(2131362298);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setActivated(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setVisibility(0);
      String var = var.a();
      ImageView var = (ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail);
      kotlin.d.b.l.a(var, "primaryDocumentThumbnail");
      this.a(var, var);
      if(var.d() != null) {
         ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(0);
         String var = var.d();
         var = (ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail);
         kotlin.d.b.l.a(var, "secondaryDocumentThumbnail");
         this.a(var, var);
      } else {
         ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(8);
      }

   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var) {
      kotlin.d.b.l.b(var, "version");
      VideoRecordingActivity.a var = VideoRecordingActivity.b;
      Context var = this.getContext();
      kotlin.d.b.l.a(var, "context");
      this.startActivity(var.a(var, var, this.w()));
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var) {
      kotlin.d.b.l.b(var, "version");
      kotlin.d.b.l.b(var, "idDocumentType");
      kotlin.d.b.l.b(var, "country");
      this.startActivity(DocumentCameraActivity.a(this.getContext(), var, var, var, this.w()));
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var, Impression.KycFrom var) {
      kotlin.d.b.l.b(var, "identityVerificationVersion");
      kotlin.d.b.l.b(var, "from");
      e.b var = this.l;
      if(var == null) {
         kotlin.d.b.l.b("listener");
      }

      var.e_();
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "message");
      Snackbar var = co.uk.getmondo.common.ui.i.a(this.getContext(), (RelativeLayout)this.a(co.uk.getmondo.c.a.rootView), var, (int)TimeUnit.MINUTES.toMillis(1L), true).a(2131362202, (android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
         public final void onClick(View var) {
            Snackbar var = e.this.m;
            if(var != null) {
               var.d();
            }

         }
      })).e(android.support.v4.content.a.c(this.getContext(), 2131689706));
      var.c();
      this.m = var;
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void b(co.uk.getmondo.signup.identity_verification.a.j var, IdentityDocumentType var, co.uk.getmondo.d.i var) {
      kotlin.d.b.l.b(var, "version");
      kotlin.d.b.l.b(var, "documentType");
      kotlin.d.b.l.b(var, "country");
      DocumentFallbackActivity.a var = DocumentFallbackActivity.c;
      Context var = this.getContext();
      kotlin.d.b.l.a(var, "context");
      this.startActivity(var.a(var, var, var, this.w()));
   }

   public void b(String var) {
      kotlin.d.b.l.b(var, "videoPath");
      ((ImageView)this.a(co.uk.getmondo.c.a.videoCompleteImageView)).setImageResource(2130837954);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setText(2131362298);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setActivated(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setVisibility(0);
      ImageView var = (ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail);
      kotlin.d.b.l.a(var, "videoThumbnail");
      this.a(var, var);
   }

   public io.reactivex.n c() {
      io.reactivex.n var = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n d() {
      io.reactivex.n var = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var) {
            kotlin.d.b.l.b(var, "emitter");
            final a var = a.a.a();
            var.a((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var.a(kotlin.n.a);
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var.b((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var.a();
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var.show(e.this.getChildFragmentManager(), "TAG_CONFIRM_VERIFY_IDENTITY");
            var.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var.a((kotlin.d.a.a)null);
                  var.b((kotlin.d.a.a)null);
                  var.dismiss();
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "Observable.create { emit…)\n            }\n        }");
      return var;
   }

   public io.reactivex.n e() {
      com.b.b.c var = this.f;
      kotlin.d.b.l.a(var, "documentCountryRelay");
      return (io.reactivex.n)var;
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h f() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var) {
            kotlin.d.b.l.b(var, "setResult");
            ContextThemeWrapper var = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var = e.this.getLayoutInflater().cloneInContext((Context)var).inflate(2131034236, (ViewGroup)null);
            android.support.design.widget.c var = new android.support.design.widget.c(e.this.getContext());
            var.setContentView(var);
            var.findViewById(2131821200).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(IdentityDocumentType.DRIVING_LICENSE);
               }
            }));
            var.findViewById(2131821201).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(IdentityDocumentType.PASSPORT);
               }
            }));
            return var;
         }
      }));
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h g() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var) {
            kotlin.d.b.l.b(var, "setResult");
            ContextThemeWrapper var = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var = e.this.getLayoutInflater().cloneInContext((Context)var).inflate(2131034235, (ViewGroup)null);
            android.support.design.widget.c var = new android.support.design.widget.c(e.this.getContext());
            var.setContentView(var);
            var.findViewById(2131821198).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(Boolean.valueOf(true));
               }
            }));
            var.findViewById(2131821199).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(Boolean.valueOf(false));
               }
            }));
            return var;
         }
      }));
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h h() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var) {
            kotlin.d.b.l.b(var, "setResult");
            ContextThemeWrapper var = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var = e.this.getLayoutInflater().cloneInContext((Context)var).inflate(2131034233, (ViewGroup)null);
            android.support.design.widget.c var = new android.support.design.widget.c(e.this.getContext());
            var.setContentView(var);
            var.findViewById(2131821195).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(IdentityDocumentType.NATIONAL_ID);
               }
            }));
            var.findViewById(2131821194).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View varx) {
                  var.a(IdentityDocumentType.DRIVING_LICENSE);
               }
            }));
            return var;
         }
      }));
   }

   public void i() {
      VideoFallbackActivity.a var = VideoFallbackActivity.e;
      Context var = this.getContext();
      kotlin.d.b.l.a(var, "context");
      this.startActivity(var.a(var, this.w()));
   }

   public void j() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setLoading(true);
   }

   public void k() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setLoading(false);
   }

   public void l() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setEnabled(true);
   }

   public void m() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setEnabled(false);
   }

   public void n() {
      ((ImageView)this.a(co.uk.getmondo.c.a.photoCompleteImageView)).setImageResource(2130837834);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setText(2131362319);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setActivated(false);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setImageBitmap((Bitmap)null);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setImageBitmap((Bitmap)null);
   }

   public void o() {
      ((ImageView)this.a(co.uk.getmondo.c.a.videoCompleteImageView)).setImageResource(2130837835);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setText(2131362332);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setActivated(false);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setImageBitmap((Bitmap)null);
   }

   public void onActivityResult(int var, int var, Intent var) {
      if(var == 1001 && var == -1) {
         co.uk.getmondo.d.i var = CountrySelectionActivity.b(var);
         IdentityDocumentType var = CountrySelectionActivity.a(var);
         this.f.a((Object)(new kotlin.h(var, var)));
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   public void onAttach(Context var) {
      kotlin.d.b.l.b(var, "context");
      super.onAttach(var);
      if(var instanceof e.b) {
         this.l = (e.b)var;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement StepListener"));
      }
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(new p(this.r(), this.w())).a(new j(this.s(), this.t(), this.v())).a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      View var = var.inflate(2131034271, var, false);
      kotlin.d.b.l.a(var, "inflater.inflate(R.layou…uments, container, false)");
      return var;
   }

   public void onDestroyView() {
      g var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroyView();
      this.p();
   }

   public void onStop() {
      Snackbar var = this.m;
      if(var != null) {
         var.d();
      }

      super.onStop();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setClipToOutline(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setClipToOutline(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setClipToOutline(true);
      g var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.a((g.a)this);
   }

   public void p() {
      if(this.n != null) {
         this.n.clear();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J0\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$Companion;", "", "()V", "KEY_ALLOW_SYSTEM_CAMERA", "", "KEY_ENTRY_POINT", "KEY_REJECTION_REASON", "KEY_REQUEST_CODE_COUNTRY", "", "KEY_SIGNUP_SOURCE", "KEY_VERSION", "TAG_CONFIRM_VERIFY_IDENTITY", "newInstance", "Landroid/support/v4/app/Fragment;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "rejectedReason", "allowSystemCamera", "", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Fragment a(co.uk.getmondo.signup.identity_verification.a.j var, Impression.KycFrom var, String var, boolean var, SignupSource var) {
         kotlin.d.b.l.b(var, "version");
         kotlin.d.b.l.b(var, "from");
         kotlin.d.b.l.b(var, "signupSource");
         e var = new e();
         Bundle var = new Bundle();
         var.putSerializable("KEY_VERSION", (Serializable)var);
         var.putSerializable("KEY_ENTRY_POINT", (Serializable)var);
         var.putString("KEY_REJECTION_REASON", var);
         var.putBoolean("KEY_ALLOW_SYSTEM_CAMERA", var);
         var.putSerializable("KEY_SIGNUP_SOURCE", (Serializable)var);
         var.setArguments(var);
         return (Fragment)var;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;", "", "onIdentityVerificationDocumentsSubmitted", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void e_();
   }
}
