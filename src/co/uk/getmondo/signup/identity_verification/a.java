package co.uk.getmondo.signup.identity_verification;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\"\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\t¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/ConfirmIdentityVerificationDialogFragment;", "Landroid/support/v4/app/DialogFragment;", "()V", "closedListener", "Lkotlin/Function0;", "", "getClosedListener", "()Lkotlin/jvm/functions/Function0;", "setClosedListener", "(Lkotlin/jvm/functions/Function0;)V", "confirmListener", "getConfirmListener", "setConfirmListener", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends android.support.v4.app.i {
   public static final a.a a = new a.a((kotlin.d.b.i)null);
   private kotlin.d.a.a b;
   private kotlin.d.a.a c;
   private HashMap d;

   public final kotlin.d.a.a a() {
      return this.b;
   }

   public final void a(kotlin.d.a.a var) {
      this.b = var;
   }

   public void b() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public final void b(kotlin.d.a.a var) {
      this.c = var;
   }

   public Dialog onCreateDialog(Bundle var) {
      android.support.v7.app.d var = (new android.support.v7.app.d.a((Context)this.getActivity(), 2131493138)).a(2131362306).b(2131362305).a((CharSequence)this.getString(2131362304), (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var, int var) {
            kotlin.d.a.a var = a.this.a();
            if(var != null) {
               kotlin.n var = (kotlin.n)var.v_();
            }

         }
      })).b((CharSequence)this.getString(2131362303), (OnClickListener)null).b();
      kotlin.d.b.l.a(var, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.b();
   }

   public void onDismiss(DialogInterface var) {
      kotlin.d.b.l.b(var, "dialog");
      kotlin.d.a.a var = this.c;
      if(var != null) {
         kotlin.n var = (kotlin.n)var.v_();
      }

      super.onDismiss(var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/ConfirmIdentityVerificationDialogFragment$Companion;", "", "()V", "newInstance", "Lco/uk/getmondo/signup/identity_verification/ConfirmIdentityVerificationDialogFragment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final a a() {
         return new a();
      }
   }
}
