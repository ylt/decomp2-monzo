package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.d.ak;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "identityVerificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/accounts/AccountService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class aa extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.signup.identity_verification.a.e e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;

   public aa(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.signup.identity_verification.a.e var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var) {
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "identityVerificationManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "accountService");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   public void a(final aa.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      io.reactivex.b.a var = this.b;
      io.reactivex.n var = io.reactivex.n.merge((io.reactivex.r)var.a(), (io.reactivex.r)io.reactivex.n.interval(20L, TimeUnit.SECONDS)).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object varx) {
            return this.b(varx);
         }

         public final io.reactivex.h b(Object varx) {
            kotlin.d.b.l.b(varx, "it");
            return aa.this.e.d().b(aa.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.b();
               }
            })).b(aa.this.d).a(aa.this.d).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(IdentityVerification varx, Throwable var) {
                  var.c();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = aa.this.f;
                  kotlin.d.b.l.a(varx, "it");
                  var.a(varx, (co.uk.getmondo.common.e.a.a)var);
               }
            })).e().f();
         }
      }));
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(IdentityVerification varx) {
            IdentityVerification.Status var = varx.b();
            if(var != null) {
               switch(ab.b[var.ordinal()]) {
               case 1:
                  var.f();
                  return;
               case 2:
                  var.a(varx.a());
                  return;
               case 3:
               case 4:
                  ak var = aa.this.g.b();
                  ak.a var;
                  if(var != null) {
                     var = var.a();
                  } else {
                     var = null;
                  }

                  if(var != null) {
                     switch(ab.a[var.ordinal()]) {
                     case 1:
                        var.h();
                        return;
                     case 2:
                        var.e();
                        return;
                     }
                  }

                  d.a.a.a((Throwable)(new IllegalStateException("Unexpected user state: " + var)));
                  return;
               case 5:
                  var.g();
                  return;
               case 6:
                  var.d();
                  return;
               }
            }

            d.a.a.a((Throwable)(new IllegalStateException("Unexpected identity verification: " + varx)));
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new ac(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "Observable.merge(view.on…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006H&J\b\u0010\u0007\u001a\u00020\u0004H&J\b\u0010\b\u001a\u00020\u0004H&J\u0012\u0010\t\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u000bH&J\b\u0010\f\u001a\u00020\u0004H&J\b\u0010\r\u001a\u00020\u0004H&J\b\u0010\u000e\u001a\u00020\u0004H&J\b\u0010\u000f\u001a\u00020\u0004H&¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideLoading", "", "onRefresh", "Lio/reactivex/Observable;", "openChat", "openHome", "openIdentityVerification", "rejectionNote", "", "openIdentityVerificationOnboarding", "openTopupInfo", "showLoading", "showVerifyingMessage", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();

      void h();
   }
}
