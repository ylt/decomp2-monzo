package co.uk.getmondo.signup.identity_verification.fallback;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.identity_verification.video.ab;
import io.reactivex.n;
import java.io.File;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\u001f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "stringProvider", "Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V", "videoPath", "", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class h extends co.uk.getmondo.common.ui.b {
   private String c;
   private final co.uk.getmondo.signup.identity_verification.a.e d;
   private final ab e;
   private final co.uk.getmondo.common.a f;

   public h(co.uk.getmondo.signup.identity_verification.a.e var, ab var, co.uk.getmondo.common.a var) {
      l.b(var, "verificationManager");
      l.b(var, "stringProvider");
      l.b(var, "analyticsService");
      super();
      this.d = var;
      this.e = var;
      this.f = var;
   }

   public void a(final h.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.f.a(Impression.Companion.f(true));
      var.a(this.e.b(), this.e.a());
      io.reactivex.b.a var = this.b;
      n var = var.b();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            l.a(varx, "hasCameraPermission");
            if(varx.booleanValue()) {
               String var = h.this.c;
               if(var != null) {
                  h.this.d.a(var, true);
                  var.a(var);
                  var.finish();
               } else {
                  var.g();
               }
            } else {
               var.f();
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new i(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      l.a(var, "view.onMainActionClicked…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      io.reactivex.b.b var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(File varx) {
            if(varx.length() > (long)20971520) {
               var.i();
            } else {
               h.this.c = varx.getPath();
               h.a var = var;
               l.a(varx, "file");
               var.a(varx, h.this.e.c(), h.this.e.a());
            }

         }
      }));
      l.a(var, "view.onVideoTaken()\n    …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            l.a(varx, "isMuted");
            if(varx.booleanValue()) {
               var.k();
            } else {
               var.j();
            }

         }
      }));
      l.a(var, "view.onMuteClicked()\n   …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      n var = var.d();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object varx) {
            h.this.c = (String)null;
            var.h();
            var.a(h.this.e.b(), h.this.e.a());
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new i(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      l.a(var, "view.onRetakeClicked()\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\u0004H&J\b\u0010\b\u001a\u00020\u0004H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\nH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\nH&J\b\u0010\u0011\u001a\u00020\u0004H&J\b\u0010\u0012\u001a\u00020\u0004H&J \u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0006H&J\u0018\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0006H&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "deleteOldVideos", "", "usedSelfiePath", "", "finish", "mute", "onMainActionClicked", "Lio/reactivex/Observable;", "", "onMuteClicked", "onRetakeClicked", "", "onVideoTaken", "Ljava/io/File;", "requestCameraPermission", "resetToExample", "showConfirmation", "videoFile", "sentence", "textToRead", "showInitialInstructions", "showVideoTooLongError", "takeVideo", "unmute", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(File var, String var, String var);

      void a(String var);

      void a(String var, String var);

      n b();

      n c();

      n d();

      n e();

      void f();

      void finish();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
