package co.uk.getmondo.signup.identity_verification.fallback;

import co.uk.getmondo.signup.identity_verification.video.ab;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(b.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new j(var, var, var, var);
   }

   public h a() {
      return (h)b.a.c.a(this.b, new h((co.uk.getmondo.signup.identity_verification.a.e)this.c.b(), (ab)this.d.b(), (co.uk.getmondo.common.a)this.e.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
