package co.uk.getmondo.signup.identity_verification.fallback;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.signup.identity_verification.p;
import io.reactivex.n;
import io.reactivex.r;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 >2\u00020\u00012\u00020\u0002:\u0001>B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\"\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u000f2\b\u0010 \u001a\u0004\u0018\u00010!H\u0014J\b\u0010\"\u001a\u00020\u000fH\u0014J\u000e\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180$H\u0016J\u000e\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0$H\u0016J+\u0010&\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020+H\u0016¢\u0006\u0002\u0010,J\u000e\u0010-\u001a\b\u0012\u0004\u0012\u00020.0$H\u0016J\b\u0010/\u001a\u00020\u000fH\u0016J\b\u00100\u001a\u00020\u000fH\u0016J\b\u00101\u001a\u00020\u000fH\u0016J\u001e\u00102\u001a\b\u0012\u0004\u0012\u0002030$2\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0018H\u0016J\u0018\u00107\u001a\u00020\u000f2\u0006\u00108\u001a\u00020)2\u0006\u00109\u001a\u000203H\u0016J\u0018\u0010:\u001a\u00020\u000f2\u0006\u0010;\u001a\u00020)2\u0006\u0010<\u001a\u00020=H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR2\u0010\n\u001a&\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f \r*\u0012\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R2\u0010\u000e\u001a&\u0012\f\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f \r*\u0012\u0012\f\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\fX\u0082.¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006?"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "()V", "fileGenerator", "Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "getFileGenerator", "()Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "setFileGenerator", "(Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;)V", "imageChangedRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Ljava/io/File;", "kotlin.jvm.PlatformType", "permissionGrantedRelay", "", "photoFile", "presenter", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;)V", "hasCameraPermission", "", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMainActionClicked", "Lio/reactivex/Observable;", "onPictureChanged", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onRetakeClicked", "", "openCamera", "requestCameraPermission", "resetActionButtons", "saveInFile", "Landroid/net/Uri;", "bitmap", "Landroid/graphics/Bitmap;", "isPrimaryFile", "showConfirmation", "title", "uri", "showInstructions", "text", "preview", "Landroid/graphics/drawable/Drawable;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class DocumentFallbackActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final DocumentFallbackActivity.a c = new DocumentFallbackActivity.a((kotlin.d.b.i)null);
   public b a;
   public co.uk.getmondo.signup.identity_verification.a.a b;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private File g;
   private HashMap h;

   private final boolean h() {
      boolean var;
      if(android.support.v4.content.a.b((Context)this, "android.permission.CAMERA") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public View a(int var) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var = (View)this.h.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.h.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final co.uk.getmondo.signup.identity_verification.a.a a() {
      co.uk.getmondo.signup.identity_verification.a.a var = this.b;
      if(var == null) {
         l.b("fileGenerator");
      }

      return var;
   }

   public n a(final Bitmap var, final boolean var) {
      l.b(var, "bitmap");
      n var = n.fromCallable((Callable)(new Callable() {
         public final Uri a() {
            File varx;
            if(var) {
               varx = DocumentFallbackActivity.this.a().b();
            } else {
               varx = DocumentFallbackActivity.this.a().c();
            }

            Closeable var = (Closeable)(new FileOutputStream(varx));

            label123: {
               Exception var;
               try {
                  FileOutputStream var = (FileOutputStream)var;
                  var.compress(CompressFormat.JPEG, 90, (OutputStream)var);
                  break label123;
               } catch (Exception var) {
                  var = var;
               } finally {
                  ;
               }

               boolean varx = true;

               try {
                  try {
                     var.close();
                  } catch (Exception var) {
                     ;
                  }

                  throw (Throwable)var;
               } finally {
                  if(!varx) {
                     var.close();
                  }

                  throw var;
               }
            }

            var.close();
            return Uri.fromFile(varx);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      l.a(var, "Observable.fromCallable ….fromFile(file)\n        }");
      return var;
   }

   public void a(String var, Drawable var) {
      l.b(var, "text");
      l.b(var, "preview");
      ((TextView)this.a(co.uk.getmondo.c.a.fallbackDocumentBodyTextView)).setText((CharSequence)var);
      ((ImageView)this.a(co.uk.getmondo.c.a.fallbackDocumentPreviewImageView)).setImageDrawable(var);
   }

   public void a(String var, Uri var) {
      l.b(var, "title");
      l.b(var, "uri");
      ((TextView)this.a(co.uk.getmondo.c.a.fallbackDocumentBodyTextView)).setText((CharSequence)var);
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362351));
      ((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton)).setVisibility(0);
      File var = new File(var.getPath());
      com.bumptech.glide.g.a((android.support.v4.app.j)this).a(var).a((com.bumptech.glide.load.c)(new com.bumptech.glide.h.b(String.valueOf(var.lastModified())))).a((ImageView)this.a(co.uk.getmondo.c.a.fallbackDocumentPreviewImageView));
   }

   public n b() {
      n var = n.merge((r)this.e, (r)com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.mainActionButton))).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.b(var));
         }

         public final boolean b(Object var) {
            l.b(var, "it");
            return DocumentFallbackActivity.this.h();
         }
      }));
      l.a(var, "Observable.merge(permiss…{ hasCameraPermission() }");
      return var;
   }

   public n c() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton));
      l.a(var, "RxView.clicks(retakePhotoButton)");
      return var;
   }

   public n d() {
      com.b.b.c var = this.f;
      l.a(var, "imageChangedRelay");
      return (n)var;
   }

   public void e() {
      android.support.v4.app.a.a((Activity)this, (String[])((Object[])(new String[]{"android.permission.CAMERA"})), 1000);
   }

   public void f() {
      Intent var = new Intent("android.media.action.IMAGE_CAPTURE");
      if(var.resolveActivity(this.getPackageManager()) != null) {
         Context var = (Context)this;
         String var = this.getString(2131362187);
         File var = this.g;
         if(var == null) {
            l.b("photoFile");
         }

         var.putExtra("output", (Parcelable)FileProvider.a(var, var, var));
         this.startActivityForResult(var, 1001);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Failed to find default camera to open")));
      }

   }

   public void g() {
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362347));
      ((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton)).setVisibility(8);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1001 && var == -1) {
         com.b.b.c var = this.f;
         File var = this.g;
         if(var == null) {
            l.b("photoFile");
         }

         var.a((Object)var);
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034162);
      co.uk.getmondo.d.i var = co.uk.getmondo.d.i.Companion.a(this.getIntent().getStringExtra("KEY_COUNTRY_CODE"));
      if(var == null) {
         var = co.uk.getmondo.d.i.UNITED_KINGDOM;
      }

      IdentityDocumentType var = IdentityDocumentType.PASSPORT;
      if(this.getIntent().hasExtra("KEY_ID_DOCUMENT_TYPE")) {
         Serializable var = this.getIntent().getSerializableExtra("KEY_ID_DOCUMENT_TYPE");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.identity_verification.IdentityDocumentType");
         }

         var = (IdentityDocumentType)var;
      }

      Serializable var = this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
      } else {
         SignupSource var = (SignupSource)var;
         this.l().a(new p(co.uk.getmondo.signup.identity_verification.a.j.b, var)).a(new co.uk.getmondo.signup.identity_verification.id_picture.f(var, var, (String)null)).a(this);
         b var = this.a;
         if(var == null) {
            l.b("presenter");
         }

         var.a((b.a)this);
         co.uk.getmondo.signup.identity_verification.a.a var = this.b;
         if(var == null) {
            l.b("fileGenerator");
         }

         File var = var.e();
         l.a(var, "fileGenerator.tempFallbackIdentityDocFile");
         this.g = var;
      }
   }

   protected void onDestroy() {
      File var = this.g;
      if(var == null) {
         l.b("photoFile");
      }

      if(var.exists()) {
         var = this.g;
         if(var == null) {
            l.b("photoFile");
         }

         var.delete();
      }

      b var = this.a;
      if(var == null) {
         l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      boolean var = true;
      l.b(var, "permissions");
      l.b(var, "grantResults");
      if(var == 1000) {
         boolean var;
         if(var.length == 0) {
            var = true;
         } else {
            var = false;
         }

         if(!var) {
            var = var;
         } else {
            var = false;
         }

         if(var && var[0] == 0) {
            this.e.a((Object)kotlin.n.a);
            return;
         }
      }

      super.onRequestPermissionsResult(var, var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J(\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$Companion;", "", "()V", "KEY_COUNTRY_CODE", "", "KEY_ID_DOCUMENT_TYPE", "KEY_REQUEST_CAMERA_PERMISSION", "", "KEY_REQUEST_TAKE_PHOTO", "KEY_SIGNUP_SOURCE", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "identityDocumentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "country", "Lco/uk/getmondo/model/Country;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, IdentityDocumentType var, co.uk.getmondo.d.i var, SignupSource var) {
         l.b(var, "context");
         l.b(var, "identityDocumentType");
         l.b(var, "country");
         l.b(var, "signupSource");
         Intent var = (new Intent(var, DocumentFallbackActivity.class)).putExtra("KEY_ID_DOCUMENT_TYPE", (Serializable)var).putExtra("KEY_COUNTRY_CODE", var.e()).putExtra("KEY_SIGNUP_SOURCE", (Serializable)var);
         l.a(var, "Intent(context, Document…NUP_SOURCE, signupSource)");
         return var;
      }
   }
}
