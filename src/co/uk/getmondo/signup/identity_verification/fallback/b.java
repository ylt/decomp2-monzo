package co.uk.getmondo.signup.identity_verification.fallback;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;
import io.reactivex.u;
import java.io.File;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001dBK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\b\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016J\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "computationScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "country", "Lco/uk/getmondo/model/Country;", "documentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "imageResizer", "Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;", "stringProvider", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/model/Country;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V", "backPhotoPath", "", "frontPhotoPath", "shownBackInstructions", "", "isFront", "register", "", "view", "showInstructions", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private String c;
   private String d;
   private boolean e;
   private final u f;
   private final u g;
   private final co.uk.getmondo.d.i h;
   private final IdentityDocumentType i;
   private final co.uk.getmondo.signup.identity_verification.a.e j;
   private final co.uk.getmondo.signup.identity_verification.id_picture.u k;
   private final e l;
   private final co.uk.getmondo.common.a m;

   public b(u var, u var, co.uk.getmondo.d.i var, IdentityDocumentType var, co.uk.getmondo.signup.identity_verification.a.e var, co.uk.getmondo.signup.identity_verification.id_picture.u var, e var, co.uk.getmondo.common.a var) {
      l.b(var, "computationScheduler");
      l.b(var, "uiScheduler");
      l.b(var, "country");
      l.b(var, "documentType");
      l.b(var, "verificationManager");
      l.b(var, "imageResizer");
      l.b(var, "stringProvider");
      l.b(var, "analyticsService");
      super();
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
      this.l = var;
      this.m = var;
   }

   private final boolean a() {
      boolean var;
      if(this.i.b() && this.c == null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private final void b(b.a var) {
      kotlin.h var = this.l.a(this.i, this.a());
      var.a((String)var.c(), (Drawable)var.d());
   }

   public void a(final b.a var) {
      l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.m.a(Impression.Companion.a(this.i.a(), true));
      this.b(var);
      io.reactivex.b.a var = this.b;
      n var = var.b();
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean varx) {
            boolean var = false;
            l.a(varx, "hasCameraPermission");
            if(varx.booleanValue()) {
               boolean var;
               if(b.this.c != null) {
                  var = true;
               } else {
                  var = false;
               }

               if(b.this.d != null) {
                  var = true;
               }

               boolean var = b.this.i.b();
               if(var && var || var && !var) {
                  String var = b.this.c;
                  if(var == null) {
                     l.a();
                  }

                  co.uk.getmondo.signup.identity_verification.a.a.b var = new co.uk.getmondo.signup.identity_verification.a.a.b(var, b.this.i, b.this.h, b.this.d);
                  b.this.j.a(var, true);
                  var.finish();
               } else if(var && var && !var && !b.this.e) {
                  b.this.b(var);
                  var.g();
                  b.this.e = true;
               } else {
                  var.f();
               }
            } else {
               var.e();
            }

         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new c(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      l.a(var, "view.onMainActionClicked…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.d().concatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final n a(File var) {
            l.b(var, "file");
            return b.this.k.a(var).subscribeOn(b.this.f);
         }
      })).concatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final n a(Bitmap varx) {
            l.b(varx, "bitmap");
            return var.a(varx, b.this.a()).subscribeOn(b.this.f);
         }
      })).observeOn(this.g);
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Uri varx) {
            String var;
            b.a var;
            if(b.this.c == null) {
               b.this.c = varx.getPath();
               if(!b.this.i.b()) {
                  b.a var = var;
                  String var = b.this.l.a();
                  l.a(varx, "uri");
                  var.a(var, varx);
               } else {
                  var = var;
                  var = b.this.l.b(b.this.i, true);
                  l.a(varx, "uri");
                  var.a(var, varx);
               }
            } else {
               b.this.d = varx.getPath();
               var = var;
               var = b.this.l.b(b.this.i, false);
               l.a(varx, "uri");
               var.a(var, varx);
            }

         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new c(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      l.a(var, "view.onPictureChanged()\n…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      io.reactivex.b.a var = this.b;
      n var = var.c();
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object varx) {
            boolean var = true;
            boolean var;
            if(b.this.c != null) {
               var = true;
            } else {
               var = false;
            }

            if(b.this.d == null) {
               var = false;
            }

            if(var && !var) {
               b.this.c = (String)null;
            } else if(var && var) {
               b.this.d = (String)null;
            }

            b.this.e = false;
            var.g();
            b.this.b(var);
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new c(var);
      }

      io.reactivex.b.b var = var.subscribe(var, (io.reactivex.c.g)var);
      l.a(var, "view.onRetakeClicked()\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0006H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0006H&J\b\u0010\f\u001a\u00020\u0004H&J\b\u0010\r\u001a\u00020\u0004H&J\b\u0010\u000e\u001a\u00020\u0004H&J\u001e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0007H&J\u0018\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0010H&J\u0018\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH&¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "finish", "", "onMainActionClicked", "Lio/reactivex/Observable;", "", "onPictureChanged", "Ljava/io/File;", "onRetakeClicked", "", "openCamera", "requestCameraPermission", "resetActionButtons", "saveInFile", "Landroid/net/Uri;", "bitmap", "Landroid/graphics/Bitmap;", "isPrimaryFile", "showConfirmation", "title", "", "uri", "showInstructions", "text", "preview", "Landroid/graphics/drawable/Drawable;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a(Bitmap var, boolean var);

      void a(String var, Drawable var);

      void a(String var, Uri var);

      n b();

      n c();

      n d();

      void e();

      void f();

      void finish();

      void g();
   }
}
