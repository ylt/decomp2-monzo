package co.uk.getmondo.signup.identity_verification.fallback;

public final class g implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var) {
      return new g(var, var);
   }

   public void a(VideoFallbackActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.b = (h)this.b.b();
         var.c = (co.uk.getmondo.signup.identity_verification.a.a)this.c.b();
      }
   }
}
