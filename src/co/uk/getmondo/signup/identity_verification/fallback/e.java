package co.uk.getmondo.signup.identity_verification.fallback;

import android.content.Context;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\u0006J\"\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "confirmationId", "", "documentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "isFront", "", "confirmationPassport", "getInstructions", "Lkotlin/Pair;", "Landroid/graphics/drawable/Drawable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   private final Context a;

   public e(Context var) {
      l.b(var, "context");
      super();
      this.a = var;
   }

   public final String a() {
      String var = this.a.getString(2131362343);
      l.a(var, "context.getString(R.stri…allback_confirm_passport)");
      return var;
   }

   public final kotlin.h a(IdentityDocumentType var, boolean var) {
      l.b(var, "documentType");
      kotlin.h var;
      if(l.a(var, IdentityDocumentType.PASSPORT)) {
         var = new kotlin.h(this.a.getString(2131362346), this.a.getDrawable(2130837961));
      } else {
         String var = this.a.getString(var.c());
         if(var) {
            var = new kotlin.h(this.a.getString(2131362345, new Object[]{var}), this.a.getDrawable(2130837960));
         } else {
            var = new kotlin.h(this.a.getString(2131362344, new Object[]{var}), this.a.getDrawable(2130837959));
         }
      }

      return var;
   }

   public final String b(IdentityDocumentType var, boolean var) {
      l.b(var, "documentType");
      String var = this.a.getString(var.c());
      if(var) {
         var = this.a.getString(2131362342, new Object[]{var});
         l.a(var, "context.getString(R.stri…_id_front, idDisplayName)");
      } else {
         var = this.a.getString(2131362341, new Object[]{var});
         l.a(var, "context.getString(R.stri…m_id_back, idDisplayName)");
      }

      return var;
   }
}
