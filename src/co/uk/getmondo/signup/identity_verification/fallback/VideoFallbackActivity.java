package co.uk.getmondo.signup.identity_verification.fallback;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.signup.identity_verification.p;
import co.uk.getmondo.signup.identity_verification.video.MuteButton;
import com.google.android.exoplayer2.t;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.s;
import io.reactivex.n;
import io.reactivex.r;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.ab;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 K2\u00020\u00012\u00020\u0002:\u0001KB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010 \u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"H\u0016J\b\u0010#\u001a\u00020$H\u0002J\b\u0010%\u001a\u00020\u0011H\u0016J\"\u0010&\u001a\u00020\u00112\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020(2\b\u0010*\u001a\u0004\u0018\u00010+H\u0014J\u0012\u0010,\u001a\u00020\u00112\b\u0010-\u001a\u0004\u0018\u00010.H\u0014J\b\u0010/\u001a\u00020\u0011H\u0014J\u000e\u00100\u001a\b\u0012\u0004\u0012\u00020$01H\u0016J\u000e\u00102\u001a\b\u0012\u0004\u0012\u00020$01H\u0016J+\u00103\u001a\u00020\u00112\u0006\u0010'\u001a\u00020(2\f\u00104\u001a\b\u0012\u0004\u0012\u00020\"052\u0006\u00106\u001a\u000207H\u0016¢\u0006\u0002\u00108J\u000e\u00109\u001a\b\u0012\u0004\u0012\u00020:01H\u0016J\u000e\u0010;\u001a\b\u0012\u0004\u0012\u00020\f01H\u0016J\u0006\u0010<\u001a\u00020\u0011J\u0010\u0010=\u001a\u00020\u00112\u0006\u0010>\u001a\u00020?H\u0002J\b\u0010@\u001a\u00020\u0011H\u0016J\b\u0010A\u001a\u00020\u0011H\u0016J \u0010B\u001a\u00020\u00112\u0006\u0010C\u001a\u00020\f2\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0016J\u0018\u0010F\u001a\u00020\u00112\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0016J\u0018\u0010G\u001a\u00020\u00112\u0006\u0010D\u001a\u00020\"2\u0006\u0010E\u001a\u00020\"H\u0002J\b\u0010H\u001a\u00020\u0011H\u0016J\b\u0010I\u001a\u00020\u0011H\u0016J\b\u0010J\u001a\u00020\u0011H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R2\u0010\u000f\u001a&\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011 \u0012*\u0012\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R#\u0010\u0019\u001a\n \u0012*\u0004\u0018\u00010\u001a0\u001a8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001b\u0010\u001cR2\u0010\u001f\u001a&\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\f0\f \u0012*\u0012\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\f0\f\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006L"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;", "()V", "fileGenerator", "Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "getFileGenerator", "()Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "setFileGenerator", "(Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;)V", "generatedVideoFiles", "Ljava/util/ArrayList;", "Ljava/io/File;", "Lkotlin/collections/ArrayList;", "latestVideoFile", "permissionGrantedRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "presenter", "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;)V", "simpleExoPlayer", "Lcom/google/android/exoplayer2/SimpleExoPlayer;", "getSimpleExoPlayer", "()Lcom/google/android/exoplayer2/SimpleExoPlayer;", "simpleExoPlayer$delegate", "Lkotlin/Lazy;", "videoTakenRelay", "deleteOldVideos", "usedSelfiePath", "", "hasPermissions", "", "mute", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMainActionClicked", "Lio/reactivex/Observable;", "onMuteClicked", "onRequestPermissionsResult", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onRetakeClicked", "", "onVideoTaken", "playSampleVideo", "playVideoFromPath", "videoUri", "Landroid/net/Uri;", "requestCameraPermission", "resetToExample", "showConfirmation", "videoFile", "sentence", "textToRead", "showInitialInstructions", "showInstructions", "showVideoTooLongError", "takeVideo", "unmute", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class VideoFallbackActivity extends co.uk.getmondo.common.activities.b implements h.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(VideoFallbackActivity.class), "simpleExoPlayer", "getSimpleExoPlayer()Lcom/google/android/exoplayer2/SimpleExoPlayer;"))};
   public static final VideoFallbackActivity.a e = new VideoFallbackActivity.a((kotlin.d.b.i)null);
   public h b;
   public co.uk.getmondo.signup.identity_verification.a.a c;
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final t b() {
         return com.google.android.exoplayer2.f.a((Context)VideoFallbackActivity.this, (com.google.android.exoplayer2.b.g)(new com.google.android.exoplayer2.b.b()), (com.google.android.exoplayer2.l)(new com.google.android.exoplayer2.c()));
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final com.b.b.c g = com.b.b.c.a();
   private final com.b.b.c h = com.b.b.c.a();
   private final ArrayList i = new ArrayList();
   private File j;
   private HashMap k;

   private final void a(Uri var) {
      com.google.android.exoplayer2.source.f var = new com.google.android.exoplayer2.source.f(var, (com.google.android.exoplayer2.upstream.c.a)(new com.google.android.exoplayer2.upstream.h((Context)this, s.a((Context)this, "monzo"), (com.google.android.exoplayer2.upstream.l)null)), (com.google.android.exoplayer2.extractor.h)(new com.google.android.exoplayer2.extractor.c()), (Handler)null, (com.google.android.exoplayer2.source.f.a)null);
      this.v().a((com.google.android.exoplayer2.source.i)(new com.google.android.exoplayer2.source.g((com.google.android.exoplayer2.source.i)var)));
      this.v().a(true);
   }

   private final void b(String var, String var) {
      int var = android.support.v4.content.a.c((Context)this, 2131689714);
      TextView var = (TextView)this.a(co.uk.getmondo.c.a.fallbackVideoInstructionsTextView);
      ab var = ab.a;
      Object[] var = new Object[]{var};
      var = String.format(var, Arrays.copyOf(var, var.length));
      kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
      var.setText((new co.uk.getmondo.common.ui.j(var, var)).a(var, true).a("sans-serif", 1, true).a());
   }

   private final t v() {
      kotlin.c var = this.f;
      l var = a[0];
      return (t)var.a();
   }

   private final boolean w() {
      boolean var;
      if(android.support.v4.content.a.b((Context)this, "android.permission.CAMERA") == 0 && android.support.v4.content.a.b((Context)this, "android.permission.RECORD_AUDIO") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public View a(int var) {
      if(this.k == null) {
         this.k = new HashMap();
      }

      View var = (View)this.k.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.k.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final void a() {
      ((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.fallbackVideoExoPlayer)).setPlayer(this.v());
      final RawResourceDataSource var = new RawResourceDataSource((Context)this);
      var.a(new com.google.android.exoplayer2.upstream.e(RawResourceDataSource.a(2131296262)));
      com.google.android.exoplayer2.source.f var = new com.google.android.exoplayer2.source.f(var.a(), (com.google.android.exoplayer2.upstream.c.a)(new com.google.android.exoplayer2.upstream.c.a() {
         // $FF: synthetic method
         public com.google.android.exoplayer2.upstream.c a() {
            return (com.google.android.exoplayer2.upstream.c)this.b();
         }

         public final RawResourceDataSource b() {
            return var;
         }
      }), (com.google.android.exoplayer2.extractor.h)(new com.google.android.exoplayer2.extractor.c()), (Handler)null, (com.google.android.exoplayer2.source.f.a)null);
      this.v().a((com.google.android.exoplayer2.source.i)(new com.google.android.exoplayer2.source.g((com.google.android.exoplayer2.source.i)var)));
      this.v().a(true);
      this.v().a(0.0F);
   }

   public void a(File var, String var, String var) {
      kotlin.d.b.l.b(var, "videoFile");
      kotlin.d.b.l.b(var, "sentence");
      kotlin.d.b.l.b(var, "textToRead");
      this.b(var, var);
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362352));
      ((Button)this.a(co.uk.getmondo.c.a.retakeVideoButton)).setVisibility(0);
      ((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.fallbackVideoExoPlayer)).setBackground((Drawable)null);
      android.support.constraint.b var = new android.support.constraint.b();
      var.a((ConstraintLayout)this.a(co.uk.getmondo.c.a.fallbackVideoConstraintLayout));
      var.a(((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.fallbackVideoExoPlayer)).getId(), 4, 0, 4, 0);
      var.b((ConstraintLayout)this.a(co.uk.getmondo.c.a.fallbackVideoConstraintLayout));
      Uri var = Uri.fromFile(var);
      kotlin.d.b.l.a(var, "Uri.fromFile(videoFile)");
      this.a(var);
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "usedSelfiePath");
      Iterator var = ((Iterable)this.i).iterator();

      while(var.hasNext()) {
         File var = (File)var.next();
         if(var.exists() && kotlin.d.b.l.a(var.getPath(), var) ^ true) {
            var.delete();
         }
      }

      this.i.clear();
   }

   public void a(String var, String var) {
      kotlin.d.b.l.b(var, "sentence");
      kotlin.d.b.l.b(var, "textToRead");
      this.b(var, var);
   }

   public n b() {
      n var = n.merge((r)this.g, (r)com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.mainActionButton))).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.b(var));
         }

         public final boolean b(Object var) {
            kotlin.d.b.l.b(var, "it");
            return VideoFallbackActivity.this.w();
         }
      }));
      kotlin.d.b.l.a(var, "Observable.merge(permiss….map { hasPermissions() }");
      return var;
   }

   public n c() {
      com.b.b.c var = this.h;
      kotlin.d.b.l.a(var, "videoTakenRelay");
      return (n)var;
   }

   public n d() {
      n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.retakeVideoButton));
      kotlin.d.b.l.a(var, "RxView.clicks(retakeVideoButton)");
      return var;
   }

   public n e() {
      n var = com.b.a.c.c.a((MuteButton)this.a(co.uk.getmondo.c.a.fallbackVideoMuteButton)).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.b(var));
         }

         public final boolean b(Object var) {
            kotlin.d.b.l.b(var, "it");
            return ((MuteButton)VideoFallbackActivity.this.a(co.uk.getmondo.c.a.fallbackVideoMuteButton)).a();
         }
      }));
      kotlin.d.b.l.a(var, "RxView.clicks(fallbackVi…VideoMuteButton.isMuted }");
      return var;
   }

   public void f() {
      android.support.v4.app.a.a((Activity)this, (String[])((Object[])(new String[]{"android.permission.CAMERA", "android.permission.RECORD_AUDIO"})), 1001);
   }

   public void g() {
      co.uk.getmondo.signup.identity_verification.a.a var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("fileGenerator");
      }

      File var = var.d();
      this.j = var;
      this.i.add(var);
      Uri var = FileProvider.a((Context)this, this.getString(2131362187), var);
      Intent var = (new Intent("android.media.action.VIDEO_CAPTURE")).putExtra("android.intent.extra.durationLimit", 15).putExtra("android.intent.extra.sizeLimit", 20971520).putExtra("android.intent.extra.screenOrientation", 1).putExtra("output", (Parcelable)var).putExtra("android.intent.extra.videoQuality", 0).putExtra("android.intent.extras.CAMERA_FACING", 1);
      if(var.resolveActivity(this.getPackageManager()) != null) {
         this.startActivityForResult(var, 1002);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Failed to find default camera to open")));
      }

   }

   public void h() {
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362350));
      ((Button)this.a(co.uk.getmondo.c.a.retakeVideoButton)).setVisibility(8);
      this.a();
      ((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.fallbackVideoExoPlayer)).setBackgroundResource(2130838034);
      android.support.constraint.b var = new android.support.constraint.b();
      var.a((ConstraintLayout)this.a(co.uk.getmondo.c.a.fallbackVideoConstraintLayout));
      var.a(((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.fallbackVideoExoPlayer)).getId(), 4, ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).getId(), 3, 0);
      var.b((ConstraintLayout)this.a(co.uk.getmondo.c.a.fallbackVideoConstraintLayout));
   }

   public void i() {
      String var = this.getString(2131362353);
      final Snackbar var = co.uk.getmondo.common.ui.i.a((Context)this, this.m(), var, (int)TimeUnit.MINUTES.toMillis(1L), true);
      var.a(2131362202, (OnClickListener)(new OnClickListener() {
         public final void onClick(View var) {
            var.d();
         }
      })).e(android.support.v4.content.a.c((Context)this, 2131689706));
      var.c();
   }

   public void j() {
      ((MuteButton)this.a(co.uk.getmondo.c.a.fallbackVideoMuteButton)).b();
      this.v().a(0.0F);
   }

   public void k() {
      ((MuteButton)this.a(co.uk.getmondo.c.a.fallbackVideoMuteButton)).b();
      this.v().a(1.0F);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 1002 && var == -1) {
         com.b.b.c var = this.h;
         File var = this.j;
         if(var == null) {
            kotlin.d.b.l.a();
         }

         var.a((Object)var);
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034228);
      Serializable var = this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
      } else {
         SignupSource var = (SignupSource)var;
         this.l().a(new p(co.uk.getmondo.signup.identity_verification.a.j.b, var)).a(this);
         h var = this.b;
         if(var == null) {
            kotlin.d.b.l.b("presenter");
         }

         var.a((h.a)this);
         this.a();
      }
   }

   protected void onDestroy() {
      Iterator var = ((Iterable)this.i).iterator();

      while(var.hasNext()) {
         File var = (File)var.next();
         if(var.exists()) {
            var.delete();
         }
      }

      this.i.clear();
      this.v().d();
      h var = this.b;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      boolean var = true;
      kotlin.d.b.l.b(var, "permissions");
      kotlin.d.b.l.b(var, "grantResults");
      if(var == 1001) {
         boolean var;
         if(var.length == 0) {
            var = true;
         } else {
            var = false;
         }

         if(!var) {
            var = var;
         } else {
            var = false;
         }

         if(var && var[0] == 0) {
            this.g.a((Object)kotlin.n.a);
            return;
         }
      }

      super.onRequestPermissionsResult(var, var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackActivity$Companion;", "", "()V", "KEY_REQUEST_PERMISSIONS", "", "KEY_REQUEST_TAKE_VIDEO", "KEY_SIGNUP_SOURCE", "", "MEGABYTES_20", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, SignupSource var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "signupSource");
         Intent var = (new Intent(var, VideoFallbackActivity.class)).putExtra("KEY_SIGNUP_SOURCE", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, VideoFal…NUP_SOURCE, signupSource)");
         return var;
      }
   }
}
