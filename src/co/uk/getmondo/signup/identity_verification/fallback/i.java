package co.uk.getmondo.signup.identity_verification.fallback;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
final class i implements io.reactivex.c.g {
   // $FF: synthetic field
   private final kotlin.d.a.b a;

   i(kotlin.d.a.b var) {
      this.a = var;
   }

   // $FF: synthetic method
   public final void a(Object var) {
      l.a(this.a.a(var), "invoke(...)");
   }
}
