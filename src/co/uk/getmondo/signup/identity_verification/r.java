package co.uk.getmondo.signup.identity_verification;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;

   static {
      boolean var;
      if(!r.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public r(p var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(p var) {
      return new r(var);
   }

   public co.uk.getmondo.signup.identity_verification.a.j a() {
      return (co.uk.getmondo.signup.identity_verification.a.j)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
