package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.tracking.Impression;

public class j {
   private final Impression.KycFrom a;
   private final String b;
   private final boolean c;

   j(Impression.KycFrom var, String var, boolean var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   Impression.KycFrom a() {
      return this.a;
   }

   String b() {
      return this.b;
   }

   boolean c() {
      return this.c;
   }
}
