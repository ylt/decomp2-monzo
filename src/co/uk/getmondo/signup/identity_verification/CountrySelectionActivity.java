package co.uk.getmondo.signup.identity_verification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;

public class CountrySelectionActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   @BindView(2131820862)
   RecyclerView recyclerView;

   public static Intent a(Context var, IdentityDocumentType var) {
      return (new Intent(var, CountrySelectionActivity.class)).putExtra("KEY_DOCUMENT_TYPE", var);
   }

   public static IdentityDocumentType a(Intent var) {
      return (IdentityDocumentType)var.getSerializableExtra("KEY_DOCUMENT_TYPE");
   }

   // $FF: synthetic method
   static kotlin.n a(CountrySelectionActivity var, IdentityDocumentType var, co.uk.getmondo.d.i var) {
      var.setResult(-1, (new Intent()).putExtra("KEY_COUNTRY", var).putExtra("KEY_DOCUMENT_TYPE", var));
      var.finish();
      return kotlin.n.a;
   }

   public static co.uk.getmondo.d.i b(Intent var) {
      return (co.uk.getmondo.d.i)var.getParcelableExtra("KEY_COUNTRY");
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034156);
      ButterKnife.bind((Activity)this);
      IdentityDocumentType var = (IdentityDocumentType)this.getIntent().getSerializableExtra("KEY_DOCUMENT_TYPE");
      if(var == null) {
         throw new IllegalStateException("Identity document type is required");
      } else {
         this.l().a(this);
         this.setTitle(this.getString(2131362257, new Object[]{co.uk.getmondo.common.k.p.i(this.getString(var.c()))}));
         co.uk.getmondo.signup_old.a var = new co.uk.getmondo.signup_old.a(co.uk.getmondo.d.i.j());
         this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
         this.recyclerView.setAdapter(var);
         this.recyclerView.setHasFixedSize(true);
         var.a(b.a(this, var));
         this.a.a(Impression.i(var.a()));
      }
   }
}
