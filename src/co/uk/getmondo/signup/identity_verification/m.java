package co.uk.getmondo.signup.identity_verification;

public final class m implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final j b;

   static {
      boolean var;
      if(!m.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public m(j var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(j var) {
      return new m(var);
   }

   public String a() {
      return this.b.b();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
