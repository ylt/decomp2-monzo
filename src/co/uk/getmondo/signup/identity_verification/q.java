package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.signup.SignupSource;

public final class q implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;

   static {
      boolean var;
      if(!q.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public q(p var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(p var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new q(var, var, var, var, var, var);
   }

   public co.uk.getmondo.signup.identity_verification.a.e a() {
      return (co.uk.getmondo.signup.identity_verification.a.e)b.a.d.a(this.b.a((co.uk.getmondo.signup.identity_verification.a.h)this.c.b(), (IdentityVerificationApi)this.d.b(), (ae)this.e.b(), (co.uk.getmondo.common.i)this.f.b(), (SignupSource)this.g.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
