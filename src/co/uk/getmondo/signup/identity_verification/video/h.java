package co.uk.getmondo.signup.identity_verification.video;

import co.uk.getmondo.api.model.tracking.Impression;

class h extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final String d;

   h(co.uk.getmondo.common.a var, String var) {
      this.c = var;
      this.d = var;
   }

   // $FF: synthetic method
   static void a(h.a var, Boolean var) throws Exception {
      if(var.booleanValue()) {
         var.g();
      } else {
         var.f();
      }

   }

   // $FF: synthetic method
   static void a(h.a var, Object var) throws Exception {
      var.d();
   }

   // $FF: synthetic method
   static void a(h var, h.a var, Object var) throws Exception {
      var.c.a(Impression.D());
      var.e();
   }

   public void a(h.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(this.d);
      this.a((io.reactivex.b.b)var.a().subscribe(i.a(var)));
      this.a((io.reactivex.b.b)var.c().subscribe(j.a(var)));
      this.a((io.reactivex.b.b)var.b().subscribe(k.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();
   }
}
