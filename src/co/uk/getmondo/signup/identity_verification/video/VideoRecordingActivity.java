package co.uk.getmondo.signup.identity_verification.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.common.ae;
import com.google.android.cameraview.CameraView;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u001a\u0018\u0000 I2\u00020\u00012\u00020\u0002:\u0001IB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u0018H\u0016J\b\u0010\u001a\u001a\u00020\bH\u0002J\b\u0010\u001b\u001a\u00020\u0018H\u0016J\b\u0010\u001c\u001a\u00020\u0018H\u0016J\b\u0010\u001d\u001a\u00020\u0018H\u0016J\b\u0010\u001e\u001a\u00020\u0018H\u0016J\"\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\b\u0010#\u001a\u0004\u0018\u00010$H\u0014J\u0012\u0010%\u001a\u00020\u00182\b\u0010&\u001a\u0004\u0018\u00010'H\u0014J\b\u0010(\u001a\u00020\u0018H\u0014J\b\u0010)\u001a\u00020\u0018H\u0014J\u000e\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J+\u0010+\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!2\f\u0010,\u001a\b\u0012\u0004\u0012\u00020.0-2\u0006\u0010/\u001a\u000200H\u0016¢\u0006\u0002\u00101J\b\u00102\u001a\u00020\u0018H\u0014J\b\u00103\u001a\u00020\u0018H\u0014J\u000e\u00104\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00105\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00106\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0016J\u000e\u00107\u001a\b\u0012\u0004\u0012\u00020\b0\u0011H\u0016J\u0010\u00108\u001a\u00020\u00182\u0006\u00109\u001a\u00020.H\u0016J\b\u0010:\u001a\u00020\u0018H\u0016J\b\u0010;\u001a\u00020\u0018H\u0016J\b\u0010<\u001a\u00020\u0018H\u0016J\b\u0010=\u001a\u00020\u0018H\u0016J\b\u0010>\u001a\u00020\u0018H\u0016J\b\u0010?\u001a\u00020\u0018H\u0016J\b\u0010@\u001a\u00020\u0018H\u0016J\u0010\u0010A\u001a\u00020\u00182\u0006\u0010B\u001a\u00020.H\u0016J\b\u0010C\u001a\u00020\u0018H\u0016J\b\u0010D\u001a\u00020\u0018H\u0016J\u0010\u0010E\u001a\u00020\u00182\u0006\u0010F\u001a\u00020.H\u0016J\b\u0010G\u001a\u00020\u0018H\u0016J\b\u0010H\u001a\u00020\u0018H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R2\u0010\u0006\u001a&\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b \t*\u0012\u0012\f\u0012\n \t*\u0004\u0018\u00010\b0\b\u0018\u00010\u00070\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u00020\u000b8\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0082.¢\u0006\u0002\n\u0000R2\u0010\u0013\u001a&\u0012\f\u0012\n \t*\u0004\u0018\u00010\u00120\u0012 \t*\u0012\u0012\f\u0012\n \t*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00070\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006J"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter$View;", "()V", "handler", "Landroid/os/Handler;", "playbackResultRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "presenter", "Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingPresenter;)V", "recordButtonClicked", "Lio/reactivex/Observable;", "", "recordingTimedOutRelay", "shouldDisplayPermissionDeniedDialog", "signalRecordingTimedOut", "Ljava/lang/Runnable;", "disableVideoButton", "", "enableVideoButton", "hasCameraAndRecordAudioPermissions", "hideCameraOverlay", "hideSavingVideo", "hideTextToRead", "hideWarmingUpCamera", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onPause", "onRecordingTimedOut", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onResumeFragments", "onStartRecordingClicked", "onStopRecordingClicked", "onTakeLaterClicked", "onVideoPlaybackResult", "playVideo", "videoPath", "showCameraOverlay", "showReadTextBelowText", "showRecordAction", "showRecordingError", "showRecordingInstructions", "showSavingVideo", "showStopAction", "showTextToRead", "text", "showWarmingUpCamera", "startRecordingTimer", "startRecordingVideo", "videoFilePath", "stopRecordingTimer", "stopRecordingVideo", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class VideoRecordingActivity extends co.uk.getmondo.common.activities.b implements n.a {
   public static final VideoRecordingActivity.a b = new VideoRecordingActivity.a((kotlin.d.b.i)null);
   private static final long j;
   public n a;
   private io.reactivex.n c;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private final Handler g = new Handler();
   private final Runnable h = (Runnable)(new Runnable() {
      public final void run() {
         VideoRecordingActivity.this.f.a((Object)co.uk.getmondo.common.b.a.a);
      }
   });
   private boolean i;
   private HashMap k;

   static {
      j = TimeUnit.SECONDS.toMillis((long)15);
   }

   private final boolean I() {
      boolean var;
      if(android.support.v4.content.a.b((Context)this, "android.permission.CAMERA") == 0 && android.support.v4.content.a.b((Context)this, "android.permission.RECORD_AUDIO") == 0) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void A() {
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.maskImageView));
   }

   public void B() {
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.savingTextView)));
   }

   public void C() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.savingTextView));
   }

   public void D() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.textToReadTextView));
      ae.b(this.a(co.uk.getmondo.c.a.cameraOverlayView));
   }

   public void E() {
      ((TextView)this.a(co.uk.getmondo.c.a.descriptionTextView)).setText(2131362324);
   }

   public void F() {
      ((TextView)this.a(co.uk.getmondo.c.a.descriptionTextView)).setText(2131362324);
   }

   public void G() {
      this.a((View)this.n());
      this.d.a((com.c.a.a)(new com.c.a.a() {
         public final void a(co.uk.getmondo.common.f.c var) {
            var.a(VideoRecordingActivity.this.getString(2131362606), VideoRecordingActivity.this.getString(2131362607), (co.uk.getmondo.common.f.c.a)(new co.uk.getmondo.common.f.c.a() {
               public final void a() {
                  VideoRecordingActivity.this.a().a();
               }
            }));
         }
      }));
   }

   public View a(int var) {
      if(this.k == null) {
         this.k = new HashMap();
      }

      View var = (View)this.k.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.k.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final n a() {
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      return var;
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "videoFilePath");
      ((CameraView)this.a(co.uk.getmondo.c.a.cameraView)).a(var);
   }

   public void b() {
      ((CameraView)this.a(co.uk.getmondo.c.a.cameraView)).e();
   }

   public void c() {
      this.g.removeCallbacks(this.h);
      this.g.postDelayed(this.h, b.a());
   }

   public void d() {
      this.g.removeCallbacks(this.h);
   }

   public void d(String var) {
      kotlin.d.b.l.b(var, "videoPath");
      Intent var = VideoPlaybackActivity.b.a((Context)this, var);
      var.addFlags(65536);
      this.startActivityForResult(var, 2);
   }

   public io.reactivex.n e() {
      io.reactivex.n var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("recordButtonClicked");
      }

      var = var.filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Object var) {
            kotlin.d.b.l.b(var, "it");
            boolean var;
            if(!((CameraView)VideoRecordingActivity.this.a(co.uk.getmondo.c.a.cameraView)).f()) {
               var = true;
            } else {
               var = false;
            }

            return var;
         }
      }));
      kotlin.d.b.l.a(var, "recordButtonClicked.filt…raView.isRecordingVideo }");
      return var;
   }

   public void e(String var) {
      kotlin.d.b.l.b(var, "text");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.textToReadTextView)));
      ((TextView)this.a(co.uk.getmondo.c.a.textToReadTextView)).setText((CharSequence)var);
      ae.a(this.a(co.uk.getmondo.c.a.cameraOverlayView));
   }

   public io.reactivex.n f() {
      io.reactivex.n var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("recordButtonClicked");
      }

      var = var.filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Object var) {
            kotlin.d.b.l.b(var, "it");
            return ((CameraView)VideoRecordingActivity.this.a(co.uk.getmondo.c.a.cameraView)).f();
         }
      }));
      kotlin.d.b.l.a(var, "recordButtonClicked.filt…raView.isRecordingVideo }");
      return var;
   }

   public io.reactivex.n g() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.takeLaterButton));
      kotlin.d.b.l.a(var, "RxView.clicks(takeLaterButton)");
      return var;
   }

   public io.reactivex.n h() {
      com.b.b.c var = this.e;
      kotlin.d.b.l.a(var, "playbackResultRelay");
      return (io.reactivex.n)var;
   }

   public io.reactivex.n i() {
      com.b.b.c var = this.f;
      kotlin.d.b.l.a(var, "recordingTimedOutRelay");
      return (io.reactivex.n)var;
   }

   public void j() {
      ((ImageButton)this.a(co.uk.getmondo.c.a.recordVideoButton)).setEnabled(false);
   }

   public void k() {
      ((ImageButton)this.a(co.uk.getmondo.c.a.recordVideoButton)).setImageResource(2130837602);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      if(var == 2) {
         com.b.b.c var = this.e;
         boolean var;
         if(var == -1) {
            var = true;
         } else {
            var = false;
         }

         var.a((Object)Boolean.valueOf(var));
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034200);
      Serializable var = this.getIntent().getSerializableExtra("KEY_VERSION");
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
      } else {
         co.uk.getmondo.signup.identity_verification.a.j var = (co.uk.getmondo.signup.identity_verification.a.j)var;
         Serializable var = this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
         if(var == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
         } else {
            SignupSource var = (SignupSource)var;
            this.l().a(new co.uk.getmondo.signup.identity_verification.p(var, var)).a(this);
            io.reactivex.n var = com.b.a.c.c.a((ImageButton)this.a(co.uk.getmondo.c.a.recordVideoButton)).share();
            kotlin.d.b.l.a(var, "RxView.clicks(recordVideoButton).share()");
            this.c = var;
            n var = this.a;
            if(var == null) {
               kotlin.d.b.l.b("presenter");
            }

            var.a((n.a)this);
            if(!this.I()) {
               android.support.v4.app.a.a((Activity)this, (String[])((Object[])(new String[]{"android.permission.CAMERA", "android.permission.RECORD_AUDIO"})), 1);
            }

         }
      }
   }

   protected void onDestroy() {
      this.g.removeCallbacks(this.h);
      n var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      super.onDestroy();
   }

   protected void onPause() {
      ((CameraView)this.a(co.uk.getmondo.c.a.cameraView)).c();
      super.onPause();
   }

   public void onRequestPermissionsResult(int var, String[] var, int[] var) {
      kotlin.d.b.l.b(var, "permissions");
      kotlin.d.b.l.b(var, "grantResults");
      if(var == 1) {
         if(var.length != 2 || var[0] != 0 || var[1] != 0) {
            this.i = true;
         }
      } else {
         super.onRequestPermissionsResult(var, var, var);
      }

   }

   protected void onResume() {
      super.onResume();
      if(this.I()) {
         ((CameraView)this.a(co.uk.getmondo.c.a.cameraView)).b();
      }

   }

   protected void onResumeFragments() {
      super.onResumeFragments();
      if(this.i) {
         co.uk.getmondo.common.d.a.a(this.getString(2131362605)).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
         this.i = false;
      }

   }

   public void v() {
      ((ImageButton)this.a(co.uk.getmondo.c.a.recordVideoButton)).setImageResource(2130837599);
   }

   public void w() {
      ((ImageButton)this.a(co.uk.getmondo.c.a.recordVideoButton)).setEnabled(true);
   }

   public void x() {
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.warmingUpTextView)));
   }

   public void y() {
      ae.b((TextView)this.a(co.uk.getmondo.c.a.warmingUpTextView));
   }

   public void z() {
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.maskImageView)));
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoRecordingActivity$Companion;", "", "()V", "KEY_SIGNUP_SOURCE", "", "KEY_VERSION", "MAX_RECORDING_TIME", "", "getMAX_RECORDING_TIME", "()J", "MAX_RECORDING_TIME_SECONDS", "", "REQUEST_CAMERA_RECORD_AUDIO_PERMISSION", "REQUEST_CODE_VIDEO_PLAYBACK", "TAG_ERROR_DIALOG_FRAGMENT", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      private final long a() {
         return VideoRecordingActivity.j;
      }

      public final Intent a(Context var, co.uk.getmondo.signup.identity_verification.a.j var, SignupSource var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "version");
         kotlin.d.b.l.b(var, "signupSource");
         Intent var = (new Intent(var, VideoRecordingActivity.class)).putExtra("KEY_VERSION", (Serializable)var).putExtra("KEY_SIGNUP_SOURCE", (Serializable)var);
         kotlin.d.b.l.a(var, "Intent(context, VideoRec…NUP_SOURCE, signupSource)");
         return var;
      }
   }
}
