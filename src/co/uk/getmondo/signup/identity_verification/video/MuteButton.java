package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;
import android.util.AttributeSet;

public class MuteButton extends android.support.v7.widget.o {
   private static final int[] a = new int[]{2130772584};
   private boolean b = true;

   public MuteButton(Context var) {
      super(var);
   }

   public MuteButton(Context var, AttributeSet var) {
      super(var, var);
   }

   public MuteButton(Context var, AttributeSet var, int var) {
      super(var, var, var);
   }

   public boolean a() {
      return this.b;
   }

   public void b() {
      boolean var;
      if(!this.b) {
         var = true;
      } else {
         var = false;
      }

      this.b = var;
   }

   public int[] onCreateDrawableState(int var) {
      int[] var = super.onCreateDrawableState(var + 1);
      if(this.b) {
         mergeDrawableStates(var, a);
      }

      return var;
   }
}
