package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;
import co.uk.getmondo.d.ak;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0019\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\bJ\u0006\u0010\n\u001a\u00020\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;", "", "context", "Landroid/content/Context;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Landroid/content/Context;Lco/uk/getmondo/common/accounts/AccountService;)V", "selfieConfirmation", "", "selfieInstructions", "selfieTextToRead", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ab {
   private final Context a;
   private final co.uk.getmondo.common.accounts.d b;

   public ab(Context var, co.uk.getmondo.common.accounts.d var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "accountService");
      super();
      this.a = var;
      this.b = var;
   }

   public final String a() {
      String var;
      label18: {
         ak var = this.b.b();
         if(var != null) {
            co.uk.getmondo.d.ac var = var.d();
            if(var != null) {
               var = var.c();
               if(var != null) {
                  break label18;
               }
            }
         }

         var = "";
      }

      var = co.uk.getmondo.common.k.p.b(var);
      Boolean var = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var, "BuildConfig.BANK");
      int var;
      if(var.booleanValue()) {
         var = 2131362326;
      } else {
         var = 2131362327;
      }

      var = this.a.getString(var, new Object[]{var});
      kotlin.d.b.l.a(var, "context.getString(textResource, name)");
      return var;
   }

   public final String b() {
      String var = this.a.getString(2131362337);
      kotlin.d.b.l.a(var, "context.getString(R.stri…ra_fallback_instructions)");
      return var;
   }

   public final String c() {
      String var = this.a.getString(2131362336);
      kotlin.d.b.l.a(var, "context.getString(R.stri…ra_fallback_confirmation)");
      return var;
   }
}
