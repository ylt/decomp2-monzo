package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;

public final class ac implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!ac.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public ac(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var) {
      return new ac(var, var);
   }

   public ab a() {
      return new ab((Context)this.b.b(), (co.uk.getmondo.common.accounts.d)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
