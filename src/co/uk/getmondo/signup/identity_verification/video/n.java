package co.uk.getmondo.signup.identity_verification.video;

import co.uk.getmondo.api.model.tracking.Impression;
import java.util.concurrent.TimeUnit;

public class n extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.signup.identity_verification.a.e e;
   private final co.uk.getmondo.common.a f;
   private final co.uk.getmondo.common.q g;
   private final a h;
   private final String i;
   private final ab j;

   n(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.signup.identity_verification.a.e var, co.uk.getmondo.common.a var, co.uk.getmondo.common.q var, a var, co.uk.getmondo.signup.identity_verification.a.a var, ab var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var.a().getAbsolutePath();
      this.j = var;
   }

   // $FF: synthetic method
   static io.reactivex.z a(n var, Object var) throws Exception {
      return var.h.a(var.i).b(var.d).f(r.a(var));
   }

   // $FF: synthetic method
   static io.reactivex.z a(n var, Throwable var) throws Exception {
      boolean var = var instanceof OutOfMemoryError;
      var.f.a(Impression.b(var.getMessage(), var));
      return io.reactivex.v.a((Object)var.i);
   }

   // $FF: synthetic method
   static void a(n.a var, io.reactivex.m var) throws Exception {
      var.C();
   }

   // $FF: synthetic method
   static void a(n.a var, Object var) throws Exception {
      var.finish();
   }

   // $FF: synthetic method
   static void a(n.a var, Throwable var) throws Exception {
      d.a.a.a(var);
      var.G();
   }

   // $FF: synthetic method
   static void a(n var, n.a var, Boolean var) throws Exception {
      if(var.booleanValue()) {
         var.e.a(var.i, false);
         var.finish();
      } else {
         var.b(var);
      }

   }

   // $FF: synthetic method
   static void a(n var, n.a var, Object var) throws Exception {
      var.e(var.j.a());
      var.w();
      var.y();
      var.k();
      var.c();
      var.f.a(Impression.C());
   }

   // $FF: synthetic method
   static void a(n var, n.a var, String var) throws Exception {
      var.d(var.i);
      var.v();
   }

   private void b(n.a var) {
      var.F();
      var.z();
      var.w();
   }

   // $FF: synthetic method
   static void b(n.a var, Object var) throws Exception {
      var.d();
      var.j();
      var.b();
      var.D();
      var.B();
   }

   // $FF: synthetic method
   static void b(n.a var, Throwable var) throws Exception {
      d.a.a.a(var);
      var.G();
   }

   // $FF: synthetic method
   static void b(n var, n.a var, Object var) throws Exception {
      var.A();
      var.j();
      var.E();
      var.x();
      var.a(var.i);
   }

   void a() {
      this.g.a();
   }

   public void a(n.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.f.a(Impression.f(false));
      this.a((io.reactivex.b.b)var.e().doOnNext(o.a(this, var)).delay(1L, TimeUnit.SECONDS, this.d).observeOn(this.c).subscribe(s.a(this, var), t.a(var)));
      this.a((io.reactivex.b.b)io.reactivex.n.merge(var.f(), var.i()).observeOn(this.c).doOnNext(u.a(var)).delay(2L, TimeUnit.SECONDS, this.d).flatMapSingle(v.a(this)).observeOn(this.c).doOnEach(w.a(var)).subscribe(x.a(this, var), y.a(var)));
      this.a((io.reactivex.b.b)var.g().subscribe(z.a(var)));
      this.a((io.reactivex.b.b)var.h().subscribe(p.a(this, var), q.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      void D();

      void E();

      void F();

      void G();

      void a(String var);

      void b();

      void c();

      void d();

      void d(String var);

      io.reactivex.n e();

      void e(String var);

      io.reactivex.n f();

      void finish();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      void j();

      void k();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
