package co.uk.getmondo.signup.identity_verification.video;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final a a;
   private final String b;

   private b(a var, String var) {
      this.a = var;
      this.b = var;
   }

   public static Callable a(a var, String var) {
      return new b(var, var);
   }

   public Object call() {
      return a.a(this.a, this.b);
   }
}
