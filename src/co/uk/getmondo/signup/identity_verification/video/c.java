package co.uk.getmondo.signup.identity_verification.video;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!c.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public c(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new c(var);
   }

   public a a() {
      return new a((co.uk.getmondo.common.a)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
