package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\f\u001a\u00020\rH\u0016J\u0012\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\b\u0010\u0011\u001a\u00020\rH\u0014J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u0013H\u0016J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0013H\u0016J\b\u0010\u0018\u001a\u00020\rH\u0016J\u0010\u0010\u0019\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\rH\u0016J\b\u0010\u001d\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00078\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter$View;", "()V", "exoPlayer", "Lcom/google/android/exoplayer2/SimpleExoPlayer;", "presenter", "Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackPresenter;)V", "mute", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMuteClicked", "Lio/reactivex/Observable;", "", "onUseVideoClicked", "", "onVideoPlayerTouched", "restartVideo", "startVideo", "videoPath", "", "unmute", "useVideo", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class VideoPlaybackActivity extends co.uk.getmondo.common.activities.b implements h.a {
   public static final VideoPlaybackActivity.a b = new VideoPlaybackActivity.a((kotlin.d.b.i)null);
   public h a;
   private com.google.android.exoplayer2.t c;
   private HashMap e;

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.c((SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.exoPlayerView)).filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var, "RxView.touches(exoPlayer…    .map { Event.IGNORE }");
      return var;
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "videoPath");
      com.google.android.exoplayer2.upstream.h var = new com.google.android.exoplayer2.upstream.h((Context)this, com.google.android.exoplayer2.util.s.a((Context)this, "monzo"), (com.google.android.exoplayer2.upstream.l)null);
      com.google.android.exoplayer2.source.f var = new com.google.android.exoplayer2.source.f(Uri.parse(var), (com.google.android.exoplayer2.upstream.c.a)var, (com.google.android.exoplayer2.extractor.h)(new com.google.android.exoplayer2.extractor.c()), (Handler)null, (com.google.android.exoplayer2.source.f.a)null);
      com.google.android.exoplayer2.t var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      var.a((com.google.android.exoplayer2.source.i)var);
      var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      var.a(true);
      if(((MuteButton)this.a(co.uk.getmondo.c.a.muteButton)).a()) {
         var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("exoPlayer");
         }

         var.a(0.0F);
      }

   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.useThisVideoButton));
      kotlin.d.b.l.a(var, "RxView.clicks(useThisVideoButton)");
      return var;
   }

   public io.reactivex.n c() {
      io.reactivex.n var = com.b.a.c.c.a((MuteButton)this.a(co.uk.getmondo.c.a.muteButton)).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var) {
            return Boolean.valueOf(this.b(var));
         }

         public final boolean b(Object var) {
            kotlin.d.b.l.b(var, "it");
            return ((MuteButton)VideoPlaybackActivity.this.a(co.uk.getmondo.c.a.muteButton)).a();
         }
      }));
      kotlin.d.b.l.a(var, "RxView.clicks(muteButton…ap { muteButton.isMuted }");
      return var;
   }

   public void d() {
      com.google.android.exoplayer2.t var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      if(var.a() == 3) {
         var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("exoPlayer");
         }

         if(var.b()) {
            return;
         }
      }

      var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      if(var.a() == 4) {
         var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("exoPlayer");
         }

         var.a(0L);
      }

   }

   public void e() {
      this.setResult(-1);
      this.finish();
   }

   public void f() {
      ((MuteButton)this.a(co.uk.getmondo.c.a.muteButton)).b();
      com.google.android.exoplayer2.t var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      var.a(0.0F);
   }

   public void g() {
      ((MuteButton)this.a(co.uk.getmondo.c.a.muteButton)).b();
      com.google.android.exoplayer2.t var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      var.a(1.0F);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034182);
      String var = this.getIntent().getStringExtra("KEY_VIDEO_PATH");
      if(var != null) {
         this.l().a(new f(var)).a(this);
         com.google.android.exoplayer2.t var = com.google.android.exoplayer2.f.a((Context)this, (com.google.android.exoplayer2.b.g)(new com.google.android.exoplayer2.b.b()), (com.google.android.exoplayer2.l)(new com.google.android.exoplayer2.c()));
         kotlin.d.b.l.a(var, "ExoPlayerFactory.newSimp…(), DefaultLoadControl())");
         this.c = var;
         SimpleExoPlayerView var = (SimpleExoPlayerView)this.a(co.uk.getmondo.c.a.exoPlayerView);
         var = this.c;
         if(var == null) {
            kotlin.d.b.l.b("exoPlayer");
         }

         var.setPlayer(var);
         h var = this.a;
         if(var == null) {
            kotlin.d.b.l.b("presenter");
         }

         var.a((h.a)this);
      } else {
         throw (Throwable)(new RuntimeException("A path to the video file is required"));
      }
   }

   protected void onDestroy() {
      h var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("presenter");
      }

      var.b();
      com.google.android.exoplayer2.t var = this.c;
      if(var == null) {
         kotlin.d.b.l.b("exoPlayer");
      }

      var.d();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoPlaybackActivity$Companion;", "", "()V", "KEY_VIDEO_PATH", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "videoPath", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, String var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "videoPath");
         Intent var = new Intent(var, VideoPlaybackActivity.class);
         var.putExtra("KEY_VIDEO_PATH", var);
         return var;
      }
   }
}
