package co.uk.getmondo.signup.identity_verification.video;

public final class m implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!m.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public m(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a a(javax.a.a var) {
      return new m(var);
   }

   public void a(VideoRecordingActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (n)this.b.b();
      }
   }
}
