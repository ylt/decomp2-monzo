package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.signup.SignupSource;

public final class s implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;

   static {
      boolean var;
      if(!s.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public s(p var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(p var) {
      return new s(var);
   }

   public SignupSource a() {
      return (SignupSource)b.a.d.a(this.b.b(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
