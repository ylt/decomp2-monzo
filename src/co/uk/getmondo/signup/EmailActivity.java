package co.uk.getmondo.signup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.k;
import co.uk.getmondo.common.s;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import io.reactivex.n;
import java.util.NoSuchElementException;

public class EmailActivity extends co.uk.getmondo.common.activities.b implements f.a {
   k a;
   s b;
   f c;
   @BindView(2131820654)
   TextView content;
   private final com.b.b.b e = com.b.b.b.a();

   // $FF: synthetic method
   static void a(int var, String var, co.uk.getmondo.common.f.c var) {
      var.a(var, var);
   }

   public static void a(Activity var) {
      var.startActivity(new Intent(var, EmailActivity.class));
   }

   private void a(Intent var) {
      if(var.getData() != null) {
         String var = var.getData().getQueryParameter("code");
         String var = var.getData().getQueryParameter("state");
         if(p.c(var) && p.c(var)) {
            this.e.a((Object)android.support.v4.g.j.a(var, var));
         }
      }

   }

   private void a(String var) {
      this.a((View)this.n());
      this.d.a(c.a(var));
   }

   // $FF: synthetic method
   static void c(String var, co.uk.getmondo.common.f.c var) {
      var.a(var);
   }

   public void a() {
      this.startActivity(SignupStatusActivity.a(this, j.b));
      this.finishAffinity();
   }

   public void a(int var, String var) {
      this.a((View)this.n());
      this.d.a(d.a(var, var));
   }

   public n b() {
      return this.e;
   }

   public void c() {
      this.startActivity(HomeActivity.b((Context)this));
      this.finishAffinity();
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.l().a(this);
      this.a(this.getIntent());
      this.setContentView(2131034164);
      ButterKnife.bind((Activity)this);
      this.setTitle(2131362708);
      this.content.setText(this.getString(2131362870, new Object[]{this.b.a()}));
      this.c.a((f.a)this);
   }

   protected void onDestroy() {
      this.c.b();
      super.onDestroy();
   }

   protected void onNewIntent(Intent var) {
      super.onNewIntent(var);
      this.setIntent(var);
      this.a(this.getIntent());
   }

   @OnClick({2131820891})
   void onNoEmailClick() {
      this.c.a((Context)this);
   }

   @OnClick({2131820890})
   void openMail() {
      try {
         this.a.a(this);
         this.c.a();
      } catch (NoSuchElementException var) {
         this.a(var.getMessage());
      } catch (Exception var) {
         this.a(this.getString(2131362177));
      }

   }
}
