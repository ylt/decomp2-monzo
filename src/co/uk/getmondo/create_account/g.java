package co.uk.getmondo.create_account;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.s;
import io.reactivex.u;
import java.text.ParseException;
import java.util.Date;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;

   g(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.api.b.a var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static void a(g var) throws Exception {
      ((g.a)var.a).g();
   }

   // $FF: synthetic method
   static void a(g var, Throwable var) throws Exception {
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         d var = (d)co.uk.getmondo.common.e.d.a(d.values(), var.a());
         if(d.a.equals(var)) {
            ((g.a)var.a).c();
            return;
         }
      }

      var.f.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   // $FF: synthetic method
   static void b(g var, Throwable var) throws Exception {
      ((g.a)var.a).h();
   }

   void a() {
      this.h.a(Impression.x());
      s var = this.e.b().d().h();
      ((g.a)this.a).a(var);
   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.u());
      ak var = this.e.b();
      ((g.a)this.a).a(var.d().c());
      ((g.a)this.a).d(b.b(var.d().e()));
      ((g.a)this.a).e(co.uk.getmondo.common.k.a.a(var.d().h()));
   }

   void a(s var) {
      ak var = this.e.b();
      this.e.a(var.d().a(var));
      ((g.a)this.a).e(co.uk.getmondo.common.k.a.a(var));
   }

   void a(String var, String var) {
      ((g.a)this.a).e();
      ((g.a)this.a).f();
      Date var = new Date();

      Date var;
      try {
         var = b.a(var);
      } catch (ParseException var) {
         ((g.a)this.a).b();
         return;
      }

      if(b.b(var, var)) {
         ((g.a)this.a).b();
      } else if(!b.a(var, var)) {
         ((g.a)this.a).a();
      } else if(!p.a(var)) {
         ((g.a)this.a).d();
      } else {
         ((g.a)this.a).i();
         ak var = this.e.b();
         String var = var.d().f();
         String var = var.d().d();
         s var = var.d().h();
         ac var = var.d().a(var, b.c(var));
         this.e.a(var);
         this.a((io.reactivex.b.b)this.g.a(new co.uk.getmondo.create_account.a.a.a(var, var, var, var, var)).b(this.d).a(this.c).b(h.a(this)).a(i.a(this), j.a(this)));
      }

   }

   void c() {
      this.h.a(Impression.v());
   }

   void d() {
      this.h.a(Impression.w());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(s var);

      void a(String var);

      void b();

      void c();

      void d();

      void d(String var);

      void e();

      void e(String var);

      void f();

      void g();

      void h();

      void i();
   }
}
