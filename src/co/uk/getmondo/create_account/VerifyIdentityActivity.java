package co.uk.getmondo.create_account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.address.LegacyEnterAddressActivity;
import co.uk.getmondo.d.s;
import co.uk.getmondo.signup.identity_verification.VerificationPendingActivity;
import co.uk.getmondo.signup_old.r;

public class VerifyIdentityActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131821165)
   EditText addressInput;
   private final Handler b = new Handler();
   private final Runnable c = e.a(this);
   @BindView(2131821164)
   EditText dobInput;
   @BindView(2131821163)
   TextInputLayout dobWrapper;
   @BindView(2131820866)
   EditText nameInput;
   @BindView(2131820865)
   TextInputLayout nameWrapper;

   public static Intent a(Context var) {
      return (new Intent(var, VerifyIdentityActivity.class)).addFlags(268468224);
   }

   private void j() {
      final r var = new r();
      this.dobInput.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable varx) {
         }

         public void beforeTextChanged(CharSequence varx, int var, int var, int var) {
         }

         public void onTextChanged(CharSequence varx, int var, int var, int var) {
            var.a(varx, var, var, this, VerifyIdentityActivity.this.dobInput);
         }
      });
   }

   public void a() {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(this.getString(2131362903));
   }

   public void a(s var) {
      this.startActivityForResult(LegacyEnterAddressActivity.a(this, this.getString(2131362494), co.uk.getmondo.common.activities.b.a.a, var), 1);
   }

   public void a(String var) {
      this.nameInput.setText(var);
   }

   public void b() {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(this.getString(2131362148));
   }

   public void c() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362868), this.getString(2131362162), false).show(this.getSupportFragmentManager(), "DUPLICATE_NUMBER");
   }

   public void d() {
      this.nameWrapper.setErrorEnabled(true);
      this.nameWrapper.setError(this.getString(2131362479));
   }

   public void d(String var) {
      this.dobInput.setText(var);
   }

   public void e() {
      this.dobWrapper.setErrorEnabled(false);
   }

   public void e(String var) {
      this.addressInput.setText(var);
   }

   public void f() {
      this.nameWrapper.setErrorEnabled(false);
   }

   public void g() {
      this.startActivity(VerificationPendingActivity.a(this, co.uk.getmondo.signup.identity_verification.a.j.b, Impression.KycFrom.SIGNUP));
      this.finishAffinity();
   }

   public void h() {
      this.b.removeCallbacks(this.c);
      this.t();
   }

   public void i() {
      this.b.postDelayed(this.c, 300L);
   }

   protected void onActivityResult(int var, int var, Intent var) {
      super.onActivityResult(var, var, var);
      if(var == 1 && var == -1) {
         s var = LegacyEnterAddressActivity.a(var);
         this.a.a(var);
      }

   }

   @OnFocusChange({2131821165})
   void onAddressFocusChange(boolean var) {
      if(var) {
         this.a.a();
         this.addressInput.clearFocus();
      }

   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034227);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.j();
      this.getSupportActionBar().b(false);
      this.setTitle(this.getString(2131362263));
      this.a.a((g.a)this);
   }

   @OnFocusChange({2131821164})
   void onDateFocusChange(boolean var) {
      String var = this.dobInput.getText().toString();
      if(var) {
         this.dobInput.setSelection(var.length());
      }

   }

   @OnFocusChange({2131821164})
   void onDateOfBirthFocusChange(boolean var) {
      if(var) {
         this.a.d();
      }

   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnClick({2131821166})
   void onDetailsCorrectClick() {
      this.a.a(this.nameInput.getText().toString(), this.dobInput.getText().toString());
   }

   @OnFocusChange({2131820866})
   void onNameFocusChange(boolean var) {
      if(var) {
         this.a.c();
      }

   }
}
