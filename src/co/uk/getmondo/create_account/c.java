package co.uk.getmondo.create_account;

import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

public class c {
   private String a() {
      return " / ";
   }

   private void a(CharSequence var, TextWatcher var, EditText var) {
      Object var;
      if(this.a(var)) {
         var = var.subSequence(0, var.length() - 1);
      } else {
         char var = this.b(var);
         var = var.subSequence(0, var.length() - 1) + this.a() + var;
      }

      this.c((CharSequence)var, var, var);
   }

   private boolean a(CharSequence var) {
      char var = this.b(var);
      boolean var;
      if(var != 47 && var != 46 && var != 95 && var != 45) {
         var = false;
      } else {
         var = true;
      }

      return var;
   }

   private char b(CharSequence var) {
      return var.charAt(var.length() - 1);
   }

   private void b(CharSequence var, TextWatcher var, EditText var) {
      Object var;
      if(this.a(var)) {
         var = var.subSequence(0, var.length() - 1);
      } else {
         var = var.subSequence(0, var.length()) + this.a();
      }

      this.c((CharSequence)var, var, var);
   }

   private void c(CharSequence var, TextWatcher var, EditText var) {
      var.removeTextChangedListener(var);
      var.setText(var);
      var.addTextChangedListener(var);
      var.setSelection(var.length());
   }

   private boolean c(CharSequence var) {
      boolean var;
      if(var.length() == 9) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private boolean d(CharSequence var) {
      boolean var;
      if(var.length() == 2) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(CharSequence var, int var, int var, TextWatcher var, EditText var) {
      if(var != null && var.length() != 0) {
         if(var <= var && var.length() <= 9) {
            if(this.d(var) || this.c(var)) {
               this.c(var + this.a(), var, var);
            }

            if(var.length() == 3) {
               this.a(var, var, var);
            } else if(var.length() == 2) {
               this.b(var, var, var);
            } else if(TextUtils.isDigitsOnly(this.b(var) + "")) {
               this.c(var, var, var);
            } else {
               this.c(var.subSequence(0, var.length() - 1), var, var);
            }
         } else if(var.length() == 4) {
            this.c(var.subSequence(0, 2), var, var);
         } else if(var.length() > 9) {
            this.c(var.subSequence(0, 9), var, var);
         }
      }

   }
}
