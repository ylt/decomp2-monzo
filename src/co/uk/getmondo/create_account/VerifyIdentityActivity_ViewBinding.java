package co.uk.getmondo.create_account;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class VerifyIdentityActivity_ViewBinding implements Unbinder {
   private VerifyIdentityActivity a;
   private View b;
   private View c;
   private View d;
   private View e;

   public VerifyIdentityActivity_ViewBinding(final VerifyIdentityActivity var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131820866, "field 'nameInput' and method 'onNameFocusChange'");
      var.nameInput = (EditText)Utils.castView(var, 2131820866, "field 'nameInput'", EditText.class);
      this.b = var;
      var.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View varx, boolean var) {
            var.onNameFocusChange(var);
         }
      });
      var.nameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131820865, "field 'nameWrapper'", TextInputLayout.class);
      var = Utils.findRequiredView(var, 2131821164, "field 'dobInput', method 'onDateFocusChange', and method 'onDateOfBirthFocusChange'");
      var.dobInput = (EditText)Utils.castView(var, 2131821164, "field 'dobInput'", EditText.class);
      this.c = var;
      var.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View varx, boolean var) {
            var.onDateFocusChange(var);
            var.onDateOfBirthFocusChange(var);
         }
      });
      var.dobWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821163, "field 'dobWrapper'", TextInputLayout.class);
      var = Utils.findRequiredView(var, 2131821165, "field 'addressInput' and method 'onAddressFocusChange'");
      var.addressInput = (EditText)Utils.castView(var, 2131821165, "field 'addressInput'", EditText.class);
      this.d = var;
      var.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View varx, boolean var) {
            var.onAddressFocusChange(var);
         }
      });
      var = Utils.findRequiredView(var, 2131821166, "method 'onDetailsCorrectClick'");
      this.e = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onDetailsCorrectClick();
         }
      });
   }

   public void unbind() {
      VerifyIdentityActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.nameInput = null;
         var.nameWrapper = null;
         var.dobInput = null;
         var.dobWrapper = null;
         var.addressInput = null;
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         this.c.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.c = null;
         this.d.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.d = null;
         this.e.setOnClickListener((OnClickListener)null);
         this.e = null;
      }
   }
}
