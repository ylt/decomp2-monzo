package co.uk.getmondo.create_account;

import android.text.TextWatcher;
import android.widget.EditText;

public class a {
   private static String a(CharSequence var) {
      String var = var.toString();
      String var;
      if(var.length() > 19) {
         var = var.substring(0, 19);
      } else {
         char[] var = var.replace(" ", "").toCharArray();
         int var = var.length;
         int var = 0;
         int var = 0;
         var = "";

         while(true) {
            var = var;
            if(var >= var) {
               break;
            }

            char var = var[var];
            var = var + var;
            ++var;
            var = var;
            if(var > 0) {
               var = var;
               if(var < 16) {
                  var = var;
                  if(var % 4 == 0) {
                     var = var + " ";
                  }
               }
            }

            ++var;
         }
      }

      return var;
   }

   public static void a(CharSequence var, int var, EditText var, TextWatcher var) {
      if(var > 0) {
         String var = a(var.toString());
         var.removeTextChangedListener(var);
         var.setText(var);
         var.setSelection(var.length());
         var.addTextChangedListener(var);
      }

   }
}
