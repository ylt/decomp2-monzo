package co.uk.getmondo.create_account.a.a;

import co.uk.getmondo.create_account.b;
import co.uk.getmondo.d.s;
import java.util.Map;
import kotlin.Metadata;
import kotlin.h;
import kotlin.a.ab;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010$\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\bHÆ\u0003J;\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0018J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/create_account/data/model/SignUpRequest;", "", "email", "", "name", "dateOfBirth", "phoneNumber", "address", "Lco/uk/getmondo/model/LegacyAddress;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getDateOfBirth", "()Ljava/lang/String;", "getEmail", "getName", "getPhoneNumber", "component1", "component2", "component3", "component4", "component5", "copy", "createMap", "", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final String a;
   private final String b;
   private final String c;
   private final String d;
   private final s e;

   public a(String var, String var, String var, String var, s var) {
      l.b(var, "email");
      l.b(var, "name");
      l.b(var, "dateOfBirth");
      l.b(var, "phoneNumber");
      l.b(var, "address");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
   }

   public final Map a() {
      String var = this.e.b();
      if(var == null) {
         var = "";
      }

      String var = this.e.i();
      if(var == null) {
         var = "";
      }

      String var = this.e.c();
      if(var == null) {
         var = "";
      }

      String var = this.e.j();
      if(var == null) {
         var = "";
      }

      h var = kotlin.l.a("email", this.a);
      h var = kotlin.l.a("name", this.b);
      String var = b.c(this.c);
      if(var == null) {
         l.a();
      }

      return ab.a(new h[]{var, var, kotlin.l.a("date_of_birth", var), kotlin.l.a("phone_number", this.d), kotlin.l.a("address[locality]", var), kotlin.l.a("address[administrative_area]", var), kotlin.l.a("address[postal_code]", var), kotlin.l.a("address[country]", var)});
   }

   public final s b() {
      return this.e;
   }

   public boolean equals(Object var) {
      boolean var;
      if(this != var) {
         label34: {
            if(var instanceof a) {
               a var = (a)var;
               if(l.a(this.a, var.a) && l.a(this.b, var.b) && l.a(this.c, var.c) && l.a(this.d, var.d) && l.a(this.e, var.e)) {
                  break label34;
               }
            }

            var = false;
            return var;
         }
      }

      var = true;
      return var;
   }

   public int hashCode() {
      int var = 0;
      String var = this.a;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.b;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.c;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      var = this.d;
      int var;
      if(var != null) {
         var = var.hashCode();
      } else {
         var = 0;
      }

      s var = this.e;
      if(var != null) {
         var = var.hashCode();
      }

      return (var + (var + (var + var * 31) * 31) * 31) * 31 + var;
   }

   public String toString() {
      return "SignUpRequest(email=" + this.a + ", name=" + this.b + ", dateOfBirth=" + this.c + ", phoneNumber=" + this.d + ", address=" + this.e + ")";
   }
}
