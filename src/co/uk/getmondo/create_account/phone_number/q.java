package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.model.ApiCodeCheckResult;

// $FF: synthetic class
final class q implements io.reactivex.c.b {
   private final k.a a;

   private q(k.a var) {
      this.a = var;
   }

   public static io.reactivex.c.b a(k.a var) {
      return new q(var);
   }

   public void a(Object var, Object var) {
      k.a(this.a, (ApiCodeCheckResult)var, (Throwable)var);
   }
}
