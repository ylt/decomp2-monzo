package co.uk.getmondo.create_account.phone_number;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class EnterPhoneNumberActivity_ViewBinding implements Unbinder {
   private EnterPhoneNumberActivity a;

   public EnterPhoneNumberActivity_ViewBinding(EnterPhoneNumberActivity var, View var) {
      this.a = var;
      var.phoneNumberInput = (EditText)Utils.findRequiredViewAsType(var, 2131820905, "field 'phoneNumberInput'", EditText.class);
      var.textCodeButton = (Button)Utils.findRequiredViewAsType(var, 2131820906, "field 'textCodeButton'", Button.class);
   }

   public void unbind() {
      EnterPhoneNumberActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.phoneNumberInput = null;
         var.textCodeButton = null;
      }
   }
}
