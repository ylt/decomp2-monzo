package co.uk.getmondo.create_account.phone_number;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class s implements OnEditorActionListener {
   private final EnterPhoneNumberActivity a;

   private s(EnterPhoneNumberActivity var) {
      this.a = var;
   }

   public static OnEditorActionListener a(EnterPhoneNumberActivity var) {
      return new s(var);
   }

   public boolean onEditorAction(TextView var, int var, KeyEvent var) {
      return EnterPhoneNumberActivity.a(this.a, var, var, var);
   }
}
