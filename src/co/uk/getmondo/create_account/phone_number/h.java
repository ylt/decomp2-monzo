package co.uk.getmondo.create_account.phone_number;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class h implements OnEditorActionListener {
   private final io.reactivex.o a;

   private h(io.reactivex.o var) {
      this.a = var;
   }

   public static OnEditorActionListener a(io.reactivex.o var) {
      return new h(var);
   }

   public boolean onEditorAction(TextView var, int var, KeyEvent var) {
      return EnterCodeActivity.a(this.a, var, var, var);
   }
}
