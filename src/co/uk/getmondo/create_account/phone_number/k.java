package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiCodeCheckResult;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class k extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final MonzoApi f;
   private final co.uk.getmondo.common.a g;
   private final co.uk.getmondo.common.e.a h;

   k(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, MonzoApi var, co.uk.getmondo.common.a var, co.uk.getmondo.common.e.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static io.reactivex.l a(k var, ak var, k.a var, String var) throws Exception {
      return var.f.checkVerificationCode(var.d().f(), var).e().b(var.d).a(var.c).b(o.a(var)).a(p.a(var, var)).a((io.reactivex.l)io.reactivex.h.a()).a(q.a(var));
   }

   // $FF: synthetic method
   static void a(k.a var, ApiCodeCheckResult var) throws Exception {
      if(!var.a()) {
         var.b(2131362180);
      } else {
         var.a();
      }

   }

   // $FF: synthetic method
   static void a(k.a var, ApiCodeCheckResult var, Throwable var) throws Exception {
      var.t();
      var.a(true);
   }

   // $FF: synthetic method
   static void a(k.a var, io.reactivex.b.b var) throws Exception {
      var.s();
      var.a(false);
   }

   // $FF: synthetic method
   static void a(k var, k.a var, Throwable var) throws Exception {
      var.h.a(var, var);
   }

   public void a(k.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.g.a(Impression.t());
      ak var = this.e.b();
      this.a((io.reactivex.b.b)var.b().flatMapMaybe(l.a(this, var, var)).observeOn(this.c).subscribe(m.a(var), n.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(boolean var);

      io.reactivex.n b();

      void s();

      void t();
   }
}
