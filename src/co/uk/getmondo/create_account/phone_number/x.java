package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class x extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.a h;

   x(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, MonzoApi var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static io.reactivex.l a(x var, x.a var, String var) throws Exception {
      return var.g.sendVerificationCode(var).a((Object)co.uk.getmondo.common.b.a.a).e().b(var.d).a(var.c).a(ad.a(var, var)).f().a(ae.a(var));
   }

   // $FF: synthetic method
   static void a(x.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.d();
   }

   // $FF: synthetic method
   static void a(x.a var, co.uk.getmondo.common.b.a var, Throwable var) throws Exception {
      var.e();
   }

   // $FF: synthetic method
   static void a(x.a var, Boolean var) throws Exception {
      if(!var.booleanValue()) {
         var.c();
      }

   }

   // $FF: synthetic method
   static void a(x var, x.a var, ak var, String var) throws Exception {
      var.f();
      var.f.a(var.d().a(var));
   }

   // $FF: synthetic method
   static void a(x var, x.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   public void a(x.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.h.a(Impression.s());
      ak var = this.f.b();
      co.uk.getmondo.d.ac var = null;
      if(var != null) {
         var = var.d();
      }

      if(var != null) {
         var.a(var.f());
      }

      this.a((io.reactivex.b.b)var.a().doOnNext(y.a(this, var, var)).flatMapMaybe(z.a(this, var)).subscribe(aa.a(var), ab.a()));
      this.a((io.reactivex.b.b)var.b().subscribe(ac.a(var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var);

      io.reactivex.n b();

      void c();

      void d();

      void e();

      void f();
   }
}
