package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class l implements io.reactivex.c.h {
   private final k a;
   private final ak b;
   private final k.a c;

   private l(k var, ak var, k.a var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.h a(k var, ak var, k.a var) {
      return new l(var, var, var);
   }

   public Object a(Object var) {
      return k.a(this.a, this.b, this.c, (String)var);
   }
}
