package co.uk.getmondo.create_account.phone_number;

// $FF: synthetic class
final class t implements io.reactivex.c.q {
   private final EnterPhoneNumberActivity a;

   private t(EnterPhoneNumberActivity var) {
      this.a = var;
   }

   public static io.reactivex.c.q a(EnterPhoneNumberActivity var) {
      return new t(var);
   }

   public boolean a(Object var) {
      return EnterPhoneNumberActivity.c(this.a, var);
   }
}
