package co.uk.getmondo.create_account.phone_number;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.VerifyIdentityActivity;

public class EnterCodeActivity extends co.uk.getmondo.common.activities.b implements k.a {
   k a;
   @BindView(2131820904)
   Button submitCodeButton;
   @BindView(2131820903)
   EditText verificationCodeInput;

   // $FF: synthetic method
   static String a(EnterCodeActivity var, Object var) throws Exception {
      return var.verificationCodeInput.getText().toString().trim();
   }

   public static void a(Context var) {
      var.startActivity(new Intent(var, EnterCodeActivity.class));
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var) throws Exception {
      var.verificationCodeInput.setOnEditorActionListener((OnEditorActionListener)null);
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var, ag var) throws Exception {
      ag.a((Context)var, (ag)var);
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var, io.reactivex.o var) throws Exception {
      ag var = new ag();
      var.getClass();
      a var = f.a(var);
      ag.a(var, var, "\\d{4,8}", new String[]{"Monzo"}, var);
      var.a(g.a(var, var));
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var, String var) throws Exception {
      var.verificationCodeInput.setText(var);
   }

   // $FF: synthetic method
   static boolean a(io.reactivex.o var, TextView var, int var, KeyEvent var) {
      boolean var;
      if(var == 4) {
         var.a(co.uk.getmondo.common.b.a.a);
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   // $FF: synthetic method
   static void b(EnterCodeActivity var, io.reactivex.o var) throws Exception {
      var.verificationCodeInput.setOnEditorActionListener(h.a(var));
      var.a(i.a(var));
   }

   private io.reactivex.n c() {
      return io.reactivex.n.create(d.a(this));
   }

   private io.reactivex.n d() {
      return io.reactivex.n.create(e.a(this));
   }

   public void a() {
      this.startActivity(VerifyIdentityActivity.a((Context)this));
      this.finish();
   }

   public void a(boolean var) {
      this.submitCodeButton.setEnabled(var);
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a(this.submitCodeButton).mergeWith(this.c());
      io.reactivex.n var;
      if(co.uk.getmondo.common.k.k.a(this)) {
         var = this.d().doOnNext(b.a(this));
      } else {
         var = io.reactivex.n.empty();
      }

      return var.mergeWith(var).map(c.a(this));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034167);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((k.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
