package co.uk.getmondo.create_account.phone_number;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.telephony.SmsMessage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0082\u000e¢\u0006\u0004\n\u0002\u0010\t¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;", "Landroid/content/BroadcastReceiver;", "()V", "listener", "Lco/uk/getmondo/create_account/phone_number/CodeListener;", "regex", "", "sender", "", "[Ljava/lang/String;", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ag extends BroadcastReceiver {
   public static final ag.a a = new ag.a((kotlin.d.b.i)null);
   private String b = "";
   private String[] c = new String[0];
   private a d;

   public static final void a(Context var, ag var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "smsCodeReceiver");
      a.a(var, var);
   }

   public static final void a(Context var, ag var, String var, String[] var, a var) {
      kotlin.d.b.l.b(var, "context");
      kotlin.d.b.l.b(var, "smsCodeReceiver");
      kotlin.d.b.l.b(var, "regex");
      kotlin.d.b.l.b(var, "sender");
      kotlin.d.b.l.b(var, "listener");
      a.a(var, var, var, var, var);
   }

   public void onReceive(Context var, Intent var) {
      Pattern var = null;
      Bundle var;
      if(var != null) {
         var = var.getExtras();
      } else {
         var = null;
      }

      Object var = var;
      if(var != null) {
         var = var.get("pdus");
      }

      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<*>");
      } else {
         Object[] var = (Object[])var;
         String var = var.getString("format");
         var = Pattern.compile(this.b);

         for(int var = 0; var < var.length; ++var) {
            Object var = var[var];
            SmsMessage var;
            if(VERSION.SDK_INT >= 23) {
               var = SmsMessage.createFromPdu((byte[])var, var);
            } else {
               var = SmsMessage.createFromPdu((byte[])var);
            }

            String var = var.getDisplayOriginatingAddress();
            Object[] var = (Object[])this.c;
            int var = 0;

            boolean var;
            while(true) {
               if(var >= var.length) {
                  var = false;
                  break;
               }

               CharSequence var = (CharSequence)((String)var[var]);
               kotlin.d.b.l.a(var, "originatingAddress");
               if(kotlin.h.j.b(var, (CharSequence)var, true)) {
                  var = true;
                  break;
               }

               ++var;
            }

            if(!var) {
               break;
            }

            Matcher var = var.matcher((CharSequence)var.getDisplayMessageBody());
            if(var.find()) {
               String var = var.group();
               a var = this.d;
               if(var != null) {
                  var.a(var);
               }
            }
         }

      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J;\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007¢\u0006\u0002\u0010\u0014J\u0018\u0010\u0015\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver$Companion;", "", "()V", "ACTION", "", "KEY_FORMAT", "KEY_PDUS", "PRIORITY", "", "register", "", "context", "Landroid/content/Context;", "smsCodeReceiver", "Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;", "regex", "sender", "", "listener", "Lco/uk/getmondo/create_account/phone_number/CodeListener;", "(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/CodeListener;)V", "unregister", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final void a(Context var, ag var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "smsCodeReceiver");
         var.unregisterReceiver((BroadcastReceiver)var);
      }

      public final void a(Context var, ag var, String var, String[] var, a var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "smsCodeReceiver");
         kotlin.d.b.l.b(var, "regex");
         kotlin.d.b.l.b(var, "sender");
         kotlin.d.b.l.b(var, "listener");
         var.b = var;
         var.c = var;
         var.d = var;
         IntentFilter var = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
         var.setPriority(999);
         var.registerReceiver((BroadcastReceiver)var, var);
      }
   }
}
