package co.uk.getmondo.create_account.phone_number;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class EnterCodeActivity_ViewBinding implements Unbinder {
   private EnterCodeActivity a;

   public EnterCodeActivity_ViewBinding(EnterCodeActivity var, View var) {
      this.a = var;
      var.verificationCodeInput = (EditText)Utils.findRequiredViewAsType(var, 2131820903, "field 'verificationCodeInput'", EditText.class);
      var.submitCodeButton = (Button)Utils.findRequiredViewAsType(var, 2131820904, "field 'submitCodeButton'", Button.class);
   }

   public void unbind() {
      EnterCodeActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.verificationCodeInput = null;
         var.submitCodeButton = null;
      }
   }
}
