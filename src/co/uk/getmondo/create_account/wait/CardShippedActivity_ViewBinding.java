package co.uk.getmondo.create_account.wait;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class CardShippedActivity_ViewBinding implements Unbinder {
   private CardShippedActivity a;
   private View b;
   private View c;

   public CardShippedActivity_ViewBinding(final CardShippedActivity var, View var) {
      this.a = var;
      var.descriptionView = (TextView)Utils.findRequiredViewAsType(var, 2131820842, "field 'descriptionView'", TextView.class);
      var.descriptionOnceArrivedView = (TextView)Utils.findRequiredViewAsType(var, 2131820847, "field 'descriptionOnceArrivedView'", TextView.class);
      var.animationView = (ImageView)Utils.findRequiredViewAsType(var, 2131820843, "field 'animationView'", ImageView.class);
      var.buttonsContainer = (ViewGroup)Utils.findRequiredViewAsType(var, 2131820844, "field 'buttonsContainer'", ViewGroup.class);
      View var = Utils.findRequiredView(var, 2131820845, "method 'onCardArrivedClick'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCardArrivedClick();
         }
      });
      var = Utils.findRequiredView(var, 2131820846, "method 'onCardDidntArriveClick'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onCardDidntArriveClick();
         }
      });
   }

   public void unbind() {
      CardShippedActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.descriptionView = null;
         var.descriptionOnceArrivedView = null;
         var.animationView = null;
         var.buttonsContainer = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
