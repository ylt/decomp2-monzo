package co.uk.getmondo.create_account.wait;

import co.uk.getmondo.api.model.ApiCardDispatchStatus;
import io.reactivex.c.g;

// $FF: synthetic class
final class d implements g {
   private final c a;
   private final c.a b;

   private d(c var, c.a var) {
      this.a = var;
      this.b = var;
   }

   public static g a(c var, c.a var) {
      return new d(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (ApiCardDispatchStatus)var);
   }
}
