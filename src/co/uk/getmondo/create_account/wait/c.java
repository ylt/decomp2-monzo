package co.uk.getmondo.create_account.wait;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiCardDispatchStatus;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.q;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.waitlist.i;
import io.reactivex.u;

public class c extends co.uk.getmondo.common.ui.b {
   private final q c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final MonzoApi h;
   private final co.uk.getmondo.common.a i;
   private final i j;

   c(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, MonzoApi var, co.uk.getmondo.common.a var, q var, i var) {
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.c = var;
      this.j = var;
   }

   // $FF: synthetic method
   static void a(c var, c.a var, ApiCardDispatchStatus var) throws Exception {
      var.d();
      ac var = var.f.b().d();
      if(var.a() != null) {
         var.a(co.uk.getmondo.common.k.a.a(var.h()), co.uk.getmondo.common.c.a.b(var.a().getTime()));
      } else {
         var.a(co.uk.getmondo.common.k.a.a(var.h()));
      }

   }

   // $FF: synthetic method
   static void a(c var, c.a var, Throwable var) throws Exception {
      var.g.a(var, var);
   }

   void a() {
      ((c.a)this.a).c();
   }

   public void a(c.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.A());
      var.e();
      this.a((io.reactivex.b.b)this.h.getCardDispatchStatus(this.f.b().c().a()).b(this.e).a(this.d).a(d.a(this, var), e.a(this, var)));
      if(this.j.a()) {
         var.a();
      } else {
         var.b();
      }

   }

   void c() {
      this.i.a(Impression.a(Impression.IntercomFrom.GOOD_TO_GO));
      this.c.a();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var);

      void a(String var, String var);

      void b();

      void c();

      void d();

      void e();
   }
}
