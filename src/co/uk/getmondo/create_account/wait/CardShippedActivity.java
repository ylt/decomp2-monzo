package co.uk.getmondo.create_account.wait;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.card.activate.ActivateCardActivity;

public class CardShippedActivity extends co.uk.getmondo.common.activities.b implements c.a {
   c a;
   @BindView(2131820843)
   ImageView animationView;
   private final Handler b = new Handler();
   @BindView(2131820844)
   ViewGroup buttonsContainer;
   private final Runnable c = a.a(this);
   @BindView(2131820847)
   TextView descriptionOnceArrivedView;
   @BindView(2131820842)
   TextView descriptionView;
   private boolean e;

   public static void a(Context var, boolean var) {
      Intent var = new Intent(var, CardShippedActivity.class);
      var.addFlags(268533760);
      var.putExtra("EXTRA_ACTIVATE_CARD_BUTTON", var);
      var.startActivity(var);
   }

   public void a() {
      this.descriptionOnceArrivedView.setVisibility(8);
      this.buttonsContainer.setVisibility(0);
      this.animationView.setImageResource(2130837681);
      ((AnimationDrawable)this.animationView.getDrawable()).start();
   }

   public void a(String var) {
      this.descriptionView.setText(this.getString(2131362124, new Object[]{var}));
   }

   public void a(String var, String var) {
      this.descriptionView.setText(this.getString(2131362125, new Object[]{var, var}));
   }

   public void b() {
      this.descriptionOnceArrivedView.setVisibility(0);
      this.buttonsContainer.setVisibility(8);
      this.animationView.setImageResource(2130837631);
      ((AnimationDrawable)this.animationView.getDrawable()).start();
   }

   public void c() {
      this.startActivity(ActivateCardActivity.a((Context)this));
   }

   public void d() {
      this.b.removeCallbacks(this.c);
      this.t();
   }

   public void e() {
      this.b.postDelayed(this.c, 300L);
   }

   @OnClick({2131820845})
   void onCardArrivedClick() {
      this.a.a();
   }

   @OnClick({2131820846})
   void onCardDidntArriveClick() {
      this.a.c();
   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034151);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.e = this.getIntent().getBooleanExtra("EXTRA_ACTIVATE_CARD_BUTTON", false);
      this.a.a((c.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }

   protected void onPause() {
      super.onPause();
      this.e = true;
   }

   protected void onResume() {
      super.onResume();
      if(this.e) {
         this.a();
      }

   }
}
