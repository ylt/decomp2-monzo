package co.uk.getmondo.create_account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class b {
   private static final SimpleDateFormat a;
   private static final SimpleDateFormat b;

   static {
      a = new SimpleDateFormat("dd / MM / yyyy", Locale.UK);
      b = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
   }

   public static Date a(String var) throws ParseException {
      return a.parse(var);
   }

   public static boolean a(Date var, Date var) {
      boolean var = true;
      Calendar var = Calendar.getInstance();
      var.setTime(var);
      var.add(1, -18);
      if(var.getTimeInMillis() < var.getTime()) {
         var = false;
      }

      return var;
   }

   public static String b(String var) {
      try {
         var = a.format(b.parse(var));
      } catch (ParseException var) {
         var = null;
      }

      return var;
   }

   public static boolean b(Date var, Date var) {
      boolean var = true;
      Calendar var = Calendar.getInstance();
      var.setTime(var);
      var.add(1, -150);
      if(var.getTimeInMillis() <= var.getTime()) {
         var = false;
      }

      return var;
   }

   public static String c(String var) {
      try {
         var = b.format(a.parse(var));
      } catch (ParseException var) {
         var = null;
      }

      return var;
   }
}
