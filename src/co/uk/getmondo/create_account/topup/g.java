package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ab;
import java.util.concurrent.TimeUnit;

public class g extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.q g;
   private final o h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.common.m j;
   private final c k;

   g(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.q var, o var, co.uk.getmondo.common.a var, co.uk.getmondo.common.m var, c var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
   }

   // $FF: synthetic method
   static void a(g var) {
      var.i.a(Impression.a(Impression.IntercomFrom.INITIAL_TOPUP));
      var.g.a();
   }

   // $FF: synthetic method
   static void a(g var, g.a var, Long var) throws Exception {
      var.a(var.h.c(), var.h.b(), m.a(var));
   }

   // $FF: synthetic method
   static void a(g var, Throwable var) throws Exception {
      var.f.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   // $FF: synthetic method
   static void a(g var, boolean var) {
      var.a(var);
   }

   private void a(boolean var) {
      if(var) {
         this.a();
      } else {
         g.a var = (g.a)this.a;
         String var = this.h.a();
         String var = this.h.b();
         co.uk.getmondo.common.q var = this.g;
         var.getClass();
         var.a(var, var, l.a(var));
      }

   }

   void a() {
      ab var = (ab)this.e.b().c();
      this.e.a(var.b(true));
      ((g.a)this.a).a();
      this.j.b();
   }

   public void a(g.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.z());
      this.a((io.reactivex.b.b)io.reactivex.n.timer(30L, TimeUnit.SECONDS).observeOn(this.c).subscribe(h.a(this, var), i.a()));
   }

   boolean a(Throwable var) {
      boolean var;
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         if(var != null) {
            co.uk.getmondo.topup.a.v var = (co.uk.getmondo.topup.a.v)co.uk.getmondo.common.e.d.a(co.uk.getmondo.topup.a.v.values(), var.a());
            if(co.uk.getmondo.topup.a.v.g.equals(var)) {
               this.a((io.reactivex.b.b)this.k.a().b(this.d).a(this.c).a(j.a(this), k.a(this)));
               var = true;
               return var;
            }
         }
      }

      var = false;
      return var;
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var, String var, co.uk.getmondo.common.f.c.a var);
   }
}
