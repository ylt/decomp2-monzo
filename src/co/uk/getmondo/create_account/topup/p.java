package co.uk.getmondo.create_account.topup;

import android.content.Context;

public final class p implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!p.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public p(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new p(var);
   }

   public o a() {
      return new o((Context)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
