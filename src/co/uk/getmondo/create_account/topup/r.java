package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.z;

public class r extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.a f;
   private final co.uk.getmondo.api.b.a g;
   private final c h;
   private co.uk.getmondo.d.c i;

   r(io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.a var, co.uk.getmondo.api.b.a var, c var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static z a(r var, r.a var, ak var) throws Exception {
      io.reactivex.v var;
      switch(null.a[var.a().ordinal()]) {
      case 1:
         var.b();
         var = io.reactivex.v.o_();
         break;
      case 2:
         var.c();
         var = io.reactivex.v.o_();
         break;
      default:
         var = var.h.b().b(var.d).a(var.c);
      }

      return var;
   }

   // $FF: synthetic method
   static void a(r.a var, io.reactivex.b.b var) throws Exception {
      var.a(false);
   }

   // $FF: synthetic method
   static void a(r var, r.a var, co.uk.getmondo.d.c var) throws Exception {
      var.i = var;
      var.b(var.i);
      var.a(true);
   }

   // $FF: synthetic method
   static void a(r var, r.a var, Object var) throws Exception {
      var.a(var.i);
   }

   // $FF: synthetic method
   static void a(r var, r.a var, Throwable var) throws Exception {
      var.e.a(var, var);
   }

   // $FF: synthetic method
   static boolean a(r var, Object var) throws Exception {
      boolean var;
      if(var.i == null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public void a(r.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.f.a(Impression.y());
      this.a((io.reactivex.b.b)var.a().skipWhile(s.a(this)).subscribe(t.a(this, var)));
      this.a((io.reactivex.b.b)this.g.a().b(this.d).a(this.c).b(u.a(var)).a(v.a(this, var)).a(w.a(this, var), x.a(this, var)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var);

      void a(boolean var);

      void b();

      void b(co.uk.getmondo.d.c var);

      void c();
   }
}
