package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.model.ApiInitialTopupStatus;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final c a;

   private e(c var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(c var) {
      return new e(var);
   }

   public Object a(Object var) {
      return c.a(this.a, (ApiInitialTopupStatus)var);
   }
}
