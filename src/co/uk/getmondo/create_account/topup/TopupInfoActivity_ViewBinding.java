package co.uk.getmondo.create_account.topup;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class TopupInfoActivity_ViewBinding implements Unbinder {
   private TopupInfoActivity a;

   public TopupInfoActivity_ViewBinding(TopupInfoActivity var, View var) {
      this.a = var;
      var.titleTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821026, "field 'titleTextView'", TextView.class);
      var.topupButton = Utils.findRequiredView(var, 2131821133, "field 'topupButton'");
   }

   public void unbind() {
      TopupInfoActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.titleTextView = null;
         var.topupButton = null;
      }
   }
}
