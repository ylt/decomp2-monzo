package co.uk.getmondo.create_account.topup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.topup.card.TopUpWithCardFragment;

public class InitialTopupActivity extends co.uk.getmondo.common.activities.b implements g.a, TopUpWithCardFragment.a {
   g a;

   public static void a(Context var, co.uk.getmondo.d.c var) {
      Intent var = new Intent(var, InitialTopupActivity.class);
      var.putExtra("EXTRA_TOPUP_AMOUNT", var);
      var.startActivity(var);
   }

   // $FF: synthetic method
   static void a(String var, String var, co.uk.getmondo.common.f.c.a var, co.uk.getmondo.common.f.c var) {
      var.a(var, var, var);
   }

   public void a() {
      CardShippedActivity.a(this, false);
   }

   public void a(String var, String var, co.uk.getmondo.common.f.c.a var) {
      this.d.a(a.a(var, var, var));
   }

   public boolean a(Throwable var) {
      return this.a.a(var);
   }

   public void b() {
      this.a.a();
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.l().a(this);
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("EXTRA_TOPUP_AMOUNT");
      this.setTitle(this.getString(2131362777, new Object[]{var.toString()}));
      if(this.getSupportFragmentManager().a(16908290) == null) {
         this.getSupportFragmentManager().a().b(16908290, TopUpWithCardFragment.a(var, true)).c();
      }

      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }
}
