package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiInitialTopupStatus;

public class c {
   private static final co.uk.getmondo.d.c a;
   private final co.uk.getmondo.common.accounts.d b;
   private final MonzoApi c;

   static {
      a = new co.uk.getmondo.d.c(10000L, co.uk.getmondo.common.i.c.a);
   }

   public c(co.uk.getmondo.common.accounts.d var, MonzoApi var) {
      this.b = var;
      this.c = var;
   }

   private co.uk.getmondo.d.c a(ApiInitialTopupStatus var) {
      return new co.uk.getmondo.d.c(var.b(), var.c());
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.c a(c var, ApiInitialTopupStatus var) {
      return var.a(var);
   }

   public io.reactivex.v a() {
      io.reactivex.v var;
      if(this.b.b().c() != null) {
         var = this.c.getInitialTopupStatus(this.b.b().c().a()).d(d.a());
      } else {
         var = io.reactivex.v.a((Object)Boolean.valueOf(false));
      }

      return var;
   }

   public io.reactivex.v b() {
      co.uk.getmondo.d.a var = this.b.b().c();
      io.reactivex.v var;
      if(var != null) {
         var = this.c.getInitialTopupStatus(var.a()).d(e.a(this));
      } else {
         var = io.reactivex.v.a((Object)a);
      }

      return var;
   }
}
