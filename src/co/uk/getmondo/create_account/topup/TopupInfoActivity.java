package co.uk.getmondo.create_account.topup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.main.HomeActivity;

public class TopupInfoActivity extends co.uk.getmondo.common.activities.b implements r.a {
   r a;
   @BindView(2131821026)
   TextView titleTextView;
   @BindView(2131821133)
   View topupButton;

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.topupButton);
   }

   public void a(co.uk.getmondo.d.c var) {
      InitialTopupActivity.a(this, var);
   }

   public void a(boolean var) {
      this.topupButton.setEnabled(var);
   }

   public void b() {
      CardShippedActivity.a(this, true);
   }

   public void b(co.uk.getmondo.d.c var) {
      this.titleTextView.setText(this.getString(2131362779, new Object[]{var.toString()}));
   }

   public void c() {
      HomeActivity.a((Context)this);
   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034221);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((r.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }
}
