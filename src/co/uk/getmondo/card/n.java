package co.uk.getmondo.card;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final j a;
   private final j.a b;

   private n(j var, j.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.h a(j var, j.a var) {
      return new n(var, var);
   }

   public Object a(Object var) {
      return j.a(this.a, this.b, var);
   }
}
