package co.uk.getmondo.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import io.reactivex.u;
import java.util.Date;

class j extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final c g;
   private final co.uk.getmondo.common.a h;
   private co.uk.getmondo.d.s i;

   j(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, c var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
   }

   // $FF: synthetic method
   static io.reactivex.l a(j var, j.a var, Object var) throws Exception {
      return var.g.a(var.i).b(var.d).a(var.c).d(q.a(var, var)).e().f();
   }

   private void a() {
      if(this.i != null) {
         ac var = this.e.b().d().a(this.i);
         this.e.a(var);
      }

   }

   // $FF: synthetic method
   static void a(j.a var, Object var) throws Exception {
      var.d();
   }

   // $FF: synthetic method
   static void a(j var, j.a var, co.uk.getmondo.d.s var) throws Exception {
      var.i = var;
      var.a(co.uk.getmondo.common.k.a.b(var));
   }

   // $FF: synthetic method
   static void a(j var, j.a var, com.c.b.b var) throws Exception {
      var.a();
      var.f();
      var.a(true);
      Date var;
      if(var.b()) {
         var = (Date)var.a();
      } else {
         var = null;
      }

      var.a(var);
   }

   // $FF: synthetic method
   static void a(j var, j.a var, Throwable var) throws Exception {
      var.f();
      var.a(true);
      if(!var.f.a(var, var)) {
         var.b(2131362061);
      }

   }

   // $FF: synthetic method
   static void b(j var, j.a var, Object var) throws Exception {
      var.h.a(Impression.H());
      var.e();
      var.a(false);
   }

   public void a(j.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      var.a(co.uk.getmondo.common.k.a.b(this.e.b().d().h()));
      this.a((io.reactivex.b.b)var.b().subscribe(k.a(var)));
      this.a((io.reactivex.b.b)var.c().subscribe(l.a(this, var)));
      this.a((io.reactivex.b.b)var.a().doOnNext(m.a(this, var)).flatMapMaybe(n.a(this, var)).subscribe(o.a(this, var), p.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var);

      void a(Date var);

      void a(boolean var);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();

      void f();
   }
}
