package co.uk.getmondo.card.activate;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class j implements io.reactivex.c.g {
   private final e a;
   private final ak b;

   private j(e var, ak var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(e var, ak var) {
      return new j(var, var);
   }

   public void a(Object var) {
      e.a(this.a, this.b, (co.uk.getmondo.d.g)var);
   }
}
