package co.uk.getmondo.card.activate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.InputFilter.LengthFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.top.TopActivity;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0012\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0014J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\f0\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u000fH\u0014J\b\u0010\u0016\u001a\u00020\u000fH\u0016J\b\u0010\u0017\u001a\u00020\u000fH\u0016J\b\u0010\u0018\u001a\u00020\u000fH\u0016J\b\u0010\u0019\u001a\u00020\u000fH\u0016J\u0018\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\fH\u0016J\b\u0010!\u001a\u00020\u000fH\u0016J\b\u0010\"\u001a\u00020\u000fH\u0016J\b\u0010#\u001a\u00020\u000fH\u0016J\b\u0010$\u001a\u00020\u000fH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR2\u0010\n\u001a&\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f \r*\u0012\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006&"},
   d2 = {"Lco/uk/getmondo/card/activate/ActivateCardActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/card/activate/ActivateCardPresenter$View;", "()V", "activateCardPresenter", "Lco/uk/getmondo/card/activate/ActivateCardPresenter;", "getActivateCardPresenter", "()Lco/uk/getmondo/card/activate/ActivateCardPresenter;", "setActivateCardPresenter", "(Lco/uk/getmondo/card/activate/ActivateCardPresenter;)V", "cardNumberRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "hideLoading", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDebitPanEntered", "Lio/reactivex/Observable;", "onDestroy", "openCardActivatedConfirmation", "openHomeFeed", "openTopOfWaitinglist", "resetTokenInput", "setTextChangeListener", "maxLength", "", "isPrepaid", "", "setToken", "cardToken", "showLoading", "showPrepaidUi", "showRetailUi", "showTokenError", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ActivateCardActivity extends co.uk.getmondo.common.activities.b implements e.a {
   public static final ActivateCardActivity.a b = new ActivateCardActivity.a((kotlin.d.b.i)null);
   public e a;
   private final com.b.b.c c = com.b.b.c.a();
   private HashMap e;

   public static final Intent a(Context var) {
      return ActivateCardActivity.a.a(b, var, false, (co.uk.getmondo.common.activities.b.a)null, 6, (Object)null);
   }

   private final void a(final int var, final boolean var) {
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(Editable varx) {
            kotlin.d.b.l.b(varx, "s");
            if(varx.length() >= var) {
               ((EditText)ActivateCardActivity.this.a(co.uk.getmondo.c.a.tokenEditText)).setEnabled(false);
               if(var) {
                  ActivateCardActivity.this.a().a(varx.toString());
               } else {
                  ActivateCardActivity.this.c.a((Object)varx.toString());
               }
            }

         }

         public void beforeTextChanged(CharSequence varx, int varx, int var, int var) {
            kotlin.d.b.l.b(varx, "s");
         }

         public void onTextChanged(CharSequence varx, int varx, int var, int var) {
            kotlin.d.b.l.b(varx, "s");
            ((TextInputLayout)ActivateCardActivity.this.a(co.uk.getmondo.c.a.tokenTextInputLayout)).setErrorEnabled(false);
         }
      }));
   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.findViewById(var);
         this.e.put(Integer.valueOf(var), var);
      }

      return var;
   }

   public final e a() {
      e var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("activateCardPresenter");
      }

      return var;
   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "cardToken");
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).setText((CharSequence)var);
   }

   public void b() {
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.activateCardImageView));
      ((TextView)this.a(co.uk.getmondo.c.a.activateCardInstructions)).setText(2131361988);
      int var = this.getResources().getInteger(2131558409);
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).setFilters((InputFilter[])((Object[])(new InputFilter[]{(InputFilter)(new LengthFilter(var))})));
      this.a(var, false);
      co.uk.getmondo.common.k.i var = co.uk.getmondo.common.k.i.a(' ').a((int)4);
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).addTextChangedListener(var.a());
   }

   public void c() {
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.activateCardImageView)));
      ((TextView)this.a(co.uk.getmondo.c.a.activateCardInstructions)).setText(2131361990);
      int var = this.getResources().getInteger(2131558413);
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).setFilters((InputFilter[])((Object[])(new InputFilter[]{(InputFilter)(new LengthFilter(var))})));
      this.a(var, true);
   }

   public io.reactivex.n d() {
      com.b.b.c var = this.c;
      kotlin.d.b.l.a(var, "cardNumberRelay");
      return (io.reactivex.n)var;
   }

   public void e() {
      TopActivity.a((Context)this);
   }

   public void f() {
      HomeActivity.f.a((Context)this);
   }

   public void g() {
      Intent var = HomeActivity.f.b((Context)this);
      ConfirmationActivity.a((Context)this, this.getString(2131362047), this.getString(2131362046), var);
   }

   public void h() {
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).setEnabled(true);
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).setText((CharSequence)"");
      ((EditText)this.a(co.uk.getmondo.c.a.tokenEditText)).requestFocus();
   }

   public void i() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.tokenTextInputLayout)).setErrorEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.tokenTextInputLayout)).setError((CharSequence)this.getString(2131362048));
   }

   public void j() {
      this.s();
   }

   public void k() {
      this.t();
   }

   protected void onCreate(Bundle var) {
      co.uk.getmondo.common.activities.b.a var = (co.uk.getmondo.common.activities.b.a)this.getIntent().getSerializableExtra("EXTRA_THEME");
      if(var != null) {
         int var;
         if(kotlin.d.b.l.a(var, co.uk.getmondo.common.activities.b.a.b)) {
            var = 2131493160;
         } else {
            var = 2131493157;
         }

         this.setTheme(var);
      }

      super.onCreate(var);
      this.setContentView(2131034139);
      boolean var = this.getIntent().getBooleanExtra("EXTRA_IS_REPLACEMENT", false);
      this.l().a(new c(var)).a(this);
      e var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("activateCardPresenter");
      }

      var.a((e.a)this);
   }

   protected void onDestroy() {
      e var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("activateCardPresenter");
      }

      var.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000e"},
      d2 = {"Lco/uk/getmondo/card/activate/ActivateCardActivity$Companion;", "", "()V", "EXTRA_IS_REPLACEMENT", "", "EXTRA_THEME", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "isReplacementCard", "", "theme", "Lco/uk/getmondo/common/activities/BaseActivity$Theme;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var) {
         this();
      }

      public final Intent a(Context var, boolean var, co.uk.getmondo.common.activities.b.a var) {
         kotlin.d.b.l.b(var, "context");
         kotlin.d.b.l.b(var, "theme");
         Intent var = new Intent(var, ActivateCardActivity.class);
         var.putExtra("EXTRA_IS_REPLACEMENT", var);
         var.putExtra("EXTRA_THEME", (Serializable)var);
         return var;
      }
   }
}
