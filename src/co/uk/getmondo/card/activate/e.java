package co.uk.getmondo.card.activate;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.u;

public class e extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.card.c g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.waitlist.i i;
   private final boolean j;

   e(u var, u var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.common.e.a var, co.uk.getmondo.card.c var, co.uk.getmondo.common.a var, co.uk.getmondo.waitlist.i var, boolean var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
   }

   // $FF: synthetic method
   static io.reactivex.l a(e var, e.a var, String var) throws Exception {
      return var.g.b(var.replace(" ", "")).b(var.d).a(var.c).b(l.a(var)).d(m.a(var, var)).a(n.a(var)).e().f();
   }

   // $FF: synthetic method
   static void a(e.a var, co.uk.getmondo.d.g var) throws Exception {
      var.f();
   }

   // $FF: synthetic method
   static void a(e.a var, co.uk.getmondo.d.g var, Throwable var) throws Exception {
      var.k();
      var.h();
   }

   // $FF: synthetic method
   static void a(e.a var, io.reactivex.b.b var) throws Exception {
      var.j();
   }

   // $FF: synthetic method
   static void a(e var, e.a var, Throwable var) throws Exception {
      var.f.a(var, var);
   }

   // $FF: synthetic method
   static void a(e var, ak var, co.uk.getmondo.d.g var) throws Exception {
      ((e.a)var.a).k();
      co.uk.getmondo.d.a var = var.c().a(true);
      var.e.a(var);
      if(var.j) {
         ((e.a)var.a).g();
      } else {
         ((e.a)var.a).f();
      }

   }

   // $FF: synthetic method
   static void a(e var, co.uk.getmondo.d.g var) throws Exception {
      var.i.c();
   }

   // $FF: synthetic method
   static void a(e var, Throwable var) throws Exception {
      ((e.a)var.a).k();
      ((e.a)var.a).h();
      if(var instanceof ApiException) {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         if(var != null) {
            p var = (p)co.uk.getmondo.common.e.d.a(p.values(), var.a());
            if(p.a.equals(var)) {
               ((e.a)var.a).i();
               return;
            }

            if(var != null) {
               ((e.a)var.a).b(var.b());
               return;
            }
         }
      }

      var.f.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   public void a(e.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      co.uk.getmondo.common.a var = this.h;
      Impression var;
      if(this.j) {
         var = Impression.I();
      } else {
         var = Impression.B();
      }

      var.a(var);
      ak var = this.e.b();
      if(var.a() == ak.a.NO_CARD && co.uk.getmondo.common.k.p.c(this.i.b())) {
         var.a(this.i.b());
         this.a(this.i.b());
      }

      if(var.c().e()) {
         var.b();
      } else {
         var.c();
      }

      this.a((io.reactivex.b.b)var.d().flatMapMaybe(f.a(this, var)).subscribe(g.a(var), h.a()));
   }

   void a(String var) {
      ak var = this.e.b();
      if(!this.j && var.a() == ak.a.ON_WAITLIST && this.i.a()) {
         this.i.a(var);
         ((e.a)this.a).e();
      } else {
         ((e.a)this.a).j();
         this.a((io.reactivex.b.b)this.g.a(var, this.j).c(i.a(this)).b(this.d).a(this.c).a(j.a(this, var), k.a(this)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(String var);

      void b();

      void c();

      io.reactivex.n d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
