package co.uk.getmondo.card.activate;

enum p implements co.uk.getmondo.common.e.f {
   a("bad_request.bad_param.processor_token", 2131362048),
   b("bad_request.activation_failed", 2131362045);

   public final int c;
   private final String d;

   private p(String var, int var) {
      this.d = var;
      this.c = var;
   }

   public String a() {
      return this.d;
   }

   public int b() {
      return this.c;
   }
}
