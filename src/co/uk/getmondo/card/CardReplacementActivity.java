package co.uk.getmondo.card;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.common.address.SelectAddressActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CardReplacementActivity extends co.uk.getmondo.common.activities.b implements j.a {
   private static final SimpleDateFormat b;
   j a;
   @BindView(2131820840)
   TextView addressTextView;
   private com.b.b.c c = com.b.b.c.a();
   @BindView(2131820841)
   Button editAddressButton;
   @BindView(2131820835)
   Button sendReplacementCardButton;

   static {
      b = new SimpleDateFormat("EEE d", Locale.UK);
   }

   public static Intent a(Context var) {
      return new Intent(var, CardReplacementActivity.class);
   }

   private static String b(Date var) {
      Calendar var = Calendar.getInstance();
      var.setTime(var);
      int var = var.get(5);
      return b.format(var) + co.uk.getmondo.common.c.a.a(var);
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.sendReplacementCardButton);
   }

   public void a(String var) {
      this.addressTextView.setText(var);
   }

   public void a(Date var) {
      String var;
      if(var == null) {
         var = "";
      } else {
         var = this.getString(2131362057, new Object[]{b(var)});
      }

      ConfirmationActivity.a(this, this.getString(2131362058), var);
      this.finish();
   }

   public void a(boolean var) {
      this.sendReplacementCardButton.setEnabled(var);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.editAddressButton);
   }

   public io.reactivex.n c() {
      return this.c;
   }

   public void d() {
      this.startActivityForResult(SelectAddressActivity.b(this, co.uk.getmondo.common.activities.b.a.b, false), 2);
   }

   public void e() {
      this.s();
   }

   public void f() {
      this.t();
   }

   protected void onActivityResult(int var, int var, Intent var) {
      switch(var) {
      case 1:
         this.finish();
         break;
      case 2:
         if(var == -1) {
            co.uk.getmondo.d.s var = SelectAddressActivity.a(var);
            this.c.a((Object)var);
         }
         break;
      default:
         super.onActivityResult(var, var, var);
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034150);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((j.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
