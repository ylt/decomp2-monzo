package co.uk.getmondo.card;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CardReplacementActivity_ViewBinding implements Unbinder {
   private CardReplacementActivity a;

   public CardReplacementActivity_ViewBinding(CardReplacementActivity var, View var) {
      this.a = var;
      var.sendReplacementCardButton = (Button)Utils.findRequiredViewAsType(var, 2131820835, "field 'sendReplacementCardButton'", Button.class);
      var.editAddressButton = (Button)Utils.findRequiredViewAsType(var, 2131820841, "field 'editAddressButton'", Button.class);
      var.addressTextView = (TextView)Utils.findRequiredViewAsType(var, 2131820840, "field 'addressTextView'", TextView.class);
   }

   public void unbind() {
      CardReplacementActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.sendReplacementCardButton = null;
         var.editAddressButton = null;
         var.addressTextView = null;
      }
   }
}
