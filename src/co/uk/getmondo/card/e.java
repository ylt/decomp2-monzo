package co.uk.getmondo.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.w;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001#BS\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016J\u0018\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010!\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010\"\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006$"},
   d2 = {"Lco/uk/getmondo/card/CardPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/card/CardPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "developerOptionsPreferences", "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;", "overdraftManager", "Lco/uk/getmondo/overdraft/data/OverdraftManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;Lco/uk/getmondo/overdraft/data/OverdraftManager;)V", "formatExpirationDate", "", "expirationDate", "onCard", "", "view", "card", "Lco/uk/getmondo/model/Card;", "register", "setActivateReplacementButtonVisible", "visible", "", "setCardReplacementButtonVisible", "updateUiForCard", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.b e;
   private final co.uk.getmondo.common.e.a f;
   private final c g;
   private final co.uk.getmondo.a.a h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.developer_options.a j;
   private final co.uk.getmondo.overdraft.a.c k;

   public e(u var, u var, co.uk.getmondo.common.accounts.b var, co.uk.getmondo.common.e.a var, c var, co.uk.getmondo.a.a var, co.uk.getmondo.common.a var, co.uk.getmondo.developer_options.a var, co.uk.getmondo.overdraft.a.c var) {
      kotlin.d.b.l.b(var, "uiScheduler");
      kotlin.d.b.l.b(var, "ioScheduler");
      kotlin.d.b.l.b(var, "accountManager");
      kotlin.d.b.l.b(var, "apiErrorHandler");
      kotlin.d.b.l.b(var, "cardManager");
      kotlin.d.b.l.b(var, "balanceRepository");
      kotlin.d.b.l.b(var, "analyticsService");
      kotlin.d.b.l.b(var, "developerOptionsPreferences");
      kotlin.d.b.l.b(var, "overdraftManager");
      super();
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
   }

   private final String a(String var) {
      List var = kotlin.h.j.b((CharSequence)var, new String[]{"/"}, false, 0, 6, (Object)null);
      String var = var;
      if(var.size() == 2) {
         var = var;
         if(((String)var.get(1)).length() == 4) {
            StringBuilder var = (new StringBuilder()).append((String)var.get(0)).append("/");
            var = (String)var.get(1);
            if(var == null) {
               throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            var = var.substring(2, 4);
            kotlin.d.b.l.a(var, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            var = var.append(var).toString();
         }
      }

      return var;
   }

   private final void a(e.a var, co.uk.getmondo.d.g var) {
      String var;
      if(!this.e.b()) {
         var = co.uk.getmondo.common.k.p.f(var.e());
         kotlin.d.b.l.a(var, "token");
         var.a(var);
      }

      var = var.c();
      kotlin.d.b.l.a(var, "card.expires");
      var.b(this.a(var));
      var = var.d();
      kotlin.d.b.l.a(var, "card.lastDigits");
      var.c(var);
      this.b(var, var);
   }

   private final void a(boolean var, e.a var) {
      if(var) {
         var.a_();
      } else {
         var.r();
      }

   }

   private final void b(e.a var, co.uk.getmondo.d.g var) {
      boolean var = true;
      boolean var;
      if(!var.f()) {
         var = true;
      } else {
         var = false;
      }

      co.uk.getmondo.api.model.a var = var.g();
      if(var != null) {
         switch(f.a[var.ordinal()]) {
         case 1:
            var.i();
            var.j();
            var.v();
            var = false;
            break;
         case 2:
            var.w();
            var.h();
            var.j();
            break;
         case 3:
            var.w();
            var.h();
            var.k();
         }
      }

      boolean var;
      if(this.e.b() && !this.j.a()) {
         var = false;
      } else {
         var = true;
      }

      this.a(var.f(), var);
      if(!var || !var) {
         var = false;
      }

      this.b(var, var);
   }

   private final void b(boolean var, e.a var) {
      if(var) {
         var.l();
      } else {
         var.m();
      }

   }

   public void a(final e.a var) {
      kotlin.d.b.l.b(var, "view");
      super.a((co.uk.getmondo.common.ui.f)var);
      this.i.a(Impression.Companion.s());
      final boolean var = this.e.b();
      String var = this.e.c();
      if(var == null) {
         kotlin.d.b.l.a();
      }

      io.reactivex.b.a var;
      io.reactivex.b.b var;
      io.reactivex.b.b var;
      io.reactivex.b.a var;
      if(var) {
         var = this.b;
         var = this.k.b(var).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(w varx) {
               if(varx.b()) {
                  var.a(true, varx.g());
               } else if(varx.c()) {
                  var.a(false, varx.g());
               } else {
                  var.x();
               }

            }
         }));
         kotlin.d.b.l.a(var, "overdraftManager.status(…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = var.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n varx) {
               var.y();
            }
         }));
         kotlin.d.b.l.a(var, "view.applyForOverdraftCl…iew.openOverdraftTour() }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
         var = this.b;
         var = var.g().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n varx) {
               var.z();
            }
         }));
         kotlin.d.b.l.a(var, "view.manageOverdraftClic…enOverdraftManagement() }");
         this.b = co.uk.getmondo.common.j.f.a(var, var);
      }

      io.reactivex.b.a var = this.b;
      io.reactivex.n var = this.h.a(this.e.c()).observeOn(this.c);
      io.reactivex.c.g var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.b varx) {
            e.a varx = var;
            co.uk.getmondo.d.c var = varx.a();
            kotlin.d.b.l.a(var, "accountBalance.balance");
            varx.a(var, var);
         }
      });
      kotlin.d.a.b var = (kotlin.d.a.b)null.a;
      Object var = var;
      if(var != null) {
         var = new g(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "balanceRepository.balanc…ailAccount) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.g.c().b(this.d).a(this.c).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = e.this.f;
            kotlin.d.b.l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      kotlin.d.b.l.a(var, "cardManager.syncCard()\n …view) }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.h.b(this.e.c()).b(this.d).a(this.c).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable varx) {
            co.uk.getmondo.common.e.a var = e.this.f;
            kotlin.d.b.l.a(varx, "error");
            var.a(varx, (co.uk.getmondo.common.e.a.a)var);
         }
      }));
      kotlin.d.b.l.a(var, "balanceRepository.refres…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = this.g.a().observeOn(this.c);
      var = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g varx) {
            e var = e.this;
            e.a var = var;
            kotlin.d.b.l.a(varx, "card");
            var.a(var, varx);
         }
      });
      var = (kotlin.d.a.b)null.a;
      var = var;
      if(var != null) {
         var = new g(var);
      }

      var = var.subscribe(var, (io.reactivex.c.g)var);
      kotlin.d.b.l.a(var, "cardManager.card()\n     …view, card) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.b(e.this.e.b());
         }
      }));
      kotlin.d.b.l.a(var, "view.topUpClicks\n       …anager.isRetailAccount) }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.a().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(kotlin.n varx) {
            kotlin.d.b.l.b(varx, "it");
            co.uk.getmondo.d.g var = e.this.g.b();
            if(var == null) {
               kotlin.d.b.l.a();
            }

            co.uk.getmondo.api.model.a var;
            if(kotlin.d.b.l.a(var.g(), co.uk.getmondo.api.model.a.ACTIVE)) {
               var = co.uk.getmondo.api.model.a.INACTIVE;
            } else {
               var = co.uk.getmondo.api.model.a.ACTIVE;
            }

            Impression var;
            if(kotlin.d.b.l.a(var.g(), co.uk.getmondo.api.model.a.ACTIVE)) {
               var = Impression.Companion.t();
            } else {
               var = Impression.Companion.u();
            }

            e.this.i.a(var);
            c var = e.this.g;
            String var = var.a();
            kotlin.d.b.l.a(var, "card.id");
            return var.a(var, var).a(e.this.c).b(e.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b varx) {
                  var.o();
                  var.a(false);
               }
            })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  var.p();
                  var.a(true);
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable varx) {
                  co.uk.getmondo.common.e.a var = e.this.f;
                  kotlin.d.b.l.a(varx, "error");
                  if(!var.a(varx, (co.uk.getmondo.common.e.a.a)var)) {
                     var.b(2131362064);
                  }

               }
            })).b();
         }
      })).c();
      kotlin.d.b.l.a(var, "view.toggleCardClicks\n  …             .subscribe()");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var) {
            e.this.i.a(Impression.Companion.N());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.n();
         }
      }));
      kotlin.d.b.l.a(var, "view.cardReplacementClic…w.openCardReplacement() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      var = var.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            var.s();
         }
      }));
      kotlin.d.b.l.a(var, "view.activateReplacement…view.openActivateCard() }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
      var = this.b;
      io.reactivex.b.b var = var.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n varx) {
            e.this.i.a(Impression.Companion.a(Impression.PinFrom.CARD));
            if(var) {
               var.b_();
            } else {
               var.t();
            }

         }
      }));
      kotlin.d.b.l.a(var, "view.forgotPinClicks\n   …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var, var);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0014\u001a\u00020\u0005H&J\b\u0010\u0015\u001a\u00020\u0005H&J\b\u0010\u0016\u001a\u00020\u0005H&J\b\u0010\u0017\u001a\u00020\u0005H&J\b\u0010\u0018\u001a\u00020\u0005H&J\b\u0010\u0019\u001a\u00020\u0005H&J\b\u0010\u001a\u001a\u00020\u0005H&J\b\u0010\u001b\u001a\u00020\u0005H&J\b\u0010\u001c\u001a\u00020\u0005H&J\b\u0010\u001d\u001a\u00020\u0005H&J\u0010\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020 H&J\b\u0010!\u001a\u00020\u0005H&J\b\u0010\"\u001a\u00020\u0005H&J\u0010\u0010#\u001a\u00020\u00052\u0006\u0010$\u001a\u00020%H&J\u0010\u0010&\u001a\u00020\u00052\u0006\u0010'\u001a\u00020%H&J\u0010\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020 H&J\b\u0010*\u001a\u00020\u0005H&J\u0018\u0010+\u001a\u00020\u00052\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020 H&J\b\u0010/\u001a\u00020\u0005H&J\b\u00100\u001a\u00020\u0005H&J\b\u00101\u001a\u00020\u0005H&J\b\u00102\u001a\u00020\u0005H&J\u0018\u00103\u001a\u00020\u00052\u0006\u00104\u001a\u00020 2\u0006\u00105\u001a\u00020-H&J\u0010\u00106\u001a\u00020\u00052\u0006\u00107\u001a\u00020%H&J\b\u00108\u001a\u00020\u0005H&J\b\u00109\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007R\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007R\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0007R\u0018\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0007R\u0018\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0007¨\u0006:"},
      d2 = {"Lco/uk/getmondo/card/CardPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "activateReplacementCardClicks", "Lio/reactivex/Observable;", "", "getActivateReplacementCardClicks", "()Lio/reactivex/Observable;", "applyForOverdraftClicks", "getApplyForOverdraftClicks", "cardReplacementClicks", "getCardReplacementClicks", "forgotPinClicks", "getForgotPinClicks", "manageOverdraftClicks", "getManageOverdraftClicks", "toggleCardClicks", "getToggleCardClicks", "topUpClicks", "getTopUpClicks", "hideActivateReplacementCardButton", "hideCardLoading", "hideCardReplacementButton", "hideOverdraftAction", "hideToggleButton", "openActivateCard", "openCardReplacement", "openForgotPin", "openOverdraftManagement", "openOverdraftTour", "openTopUp", "isRetail", "", "setCardActive", "setCardInactive", "setExpirationDate", "expires", "", "setLastFourDigits", "lastDigits", "setToggleButtonEnabled", "enabled", "showActivateReplacementCardButton", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showOnToolbar", "showCardFrozen", "showCardLoading", "showCardReplacementButton", "showForgotPinDialog", "showOverdraftAction", "hasOverdraft", "limit", "showProcessorToken", "processorToken", "showTitle", "showToggleButton", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var, boolean var);

      void a(String var);

      void a(boolean var);

      void a(boolean var, co.uk.getmondo.d.c var);

      void a_();

      io.reactivex.n b();

      void b(String var);

      void b(boolean var);

      void b_();

      io.reactivex.n c();

      void c(String var);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      void h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();

      void r();

      void s();

      void t();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
