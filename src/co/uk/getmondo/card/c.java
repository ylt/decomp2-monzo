package co.uk.getmondo.card;

import co.uk.getmondo.api.MonzoApi;
import io.reactivex.v;
import java.util.Date;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u001c\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014J\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0016J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00140\u000e2\u0006\u0010\u0019\u001a\u00020\u0010J\u001e\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001d0\u001c0\u000e2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u001a\u0010 \u001a\u00020!2\u0006\u0010\u0019\u001a\u00020\u00102\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J \u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001d0\u000e2\u0006\u0010\u0019\u001a\u00020\u00102\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J\u0006\u0010#\u001a\u00020!J\u0018\u0010$\u001a\n %*\u0004\u0018\u00010!0!2\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0016\u0010&\u001a\u00020!2\u0006\u0010'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020)R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006*"},
   d2 = {"Lco/uk/getmondo/card/CardManager;", "", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "cardStorage", "Lco/uk/getmondo/card/CardStorage;", "(Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/card/CardStorage;)V", "cachedCard", "Lco/uk/getmondo/model/Card;", "getCachedCard", "()Lco/uk/getmondo/model/Card;", "activateDebitCard", "Lio/reactivex/Single;", "cardPan", "", "activatePrepaidCard", "cardToken", "isReplacementCard", "", "card", "Lio/reactivex/Observable;", "getCardFromApi", "Lio/reactivex/Maybe;", "accountId", "hasCard", "replaceCard", "Lcom/memoizrlabs/poweroptional/Optional;", "Ljava/util/Date;", "address", "Lco/uk/getmondo/model/LegacyAddress;", "replaceCardBank", "Lio/reactivex/Completable;", "replaceCardPrepaid", "syncCard", "syncCardForAccount", "kotlin.jvm.PlatformType", "toggleCard", "cardId", "apiCardStatus", "Lco/uk/getmondo/api/model/ApiCardStatus;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final co.uk.getmondo.common.accounts.b a;
   private final MonzoApi b;
   private final s c;

   public c(co.uk.getmondo.common.accounts.b var, MonzoApi var, s var) {
      kotlin.d.b.l.b(var, "accountManager");
      kotlin.d.b.l.b(var, "monzoApi");
      kotlin.d.b.l.b(var, "cardStorage");
      super();
      this.a = var;
      this.b = var;
      this.c = var;
   }

   private final io.reactivex.b a(final String var, final co.uk.getmondo.d.s var) {
      io.reactivex.b var = v.c((Callable)(new Callable() {
         public final co.uk.getmondo.d.g a() {
            return c.this.c.a(var);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.d a(co.uk.getmondo.d.g var) {
            kotlin.d.b.l.b(var, "card");
            io.reactivex.d varx;
            if(var == null) {
               varx = (io.reactivex.d)c.this.b.orderReplacementCardBank(var.a(), (String[])null, (String)null, (String)null, (String)null, (String)null);
            } else {
               varx = (io.reactivex.d)c.this.b.orderReplacementCardBank(var.a(), var.h(), var.i(), var.b(), var.c(), var.j());
            }

            return varx;
         }
      })).b((io.reactivex.d)this.c(var));
      kotlin.d.b.l.a(var, "Single.fromCallable({ ca…ardForAccount(accountId))");
      return var;
   }

   private final v b(final String var, final co.uk.getmondo.d.s var) {
      v var = v.c((Callable)(new Callable() {
         public final co.uk.getmondo.d.g a() {
            return c.this.c.a(var);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(co.uk.getmondo.d.g var) {
            kotlin.d.b.l.b(var, "card");
            v varx;
            if(var == null) {
               varx = c.this.b.orderReplacementCardPrepaid(var.a(), (String[])null, (String)null, (String)null, (String)null, (String)null);
            } else {
               varx = c.this.b.orderReplacementCardPrepaid(var.a(), var.h(), var.i(), var.b(), var.c(), var.j());
            }

            return varx;
         }
      })).d((io.reactivex.c.h)null.a).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(Date varx) {
            kotlin.d.b.l.b(varx, "it");
            return c.this.c(var).a((Object)varx);
         }
      }));
      kotlin.d.b.l.a(var, "Single.fromCallable { ca…Id).toSingleDefault(it) }");
      return var;
   }

   private final io.reactivex.b c(String var) {
      return this.d(var).c();
   }

   private final io.reactivex.h d(String var) {
      io.reactivex.h var = this.b.cards(var).d((io.reactivex.c.h)null.a).a((io.reactivex.c.q)null.a).c((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var) {
            s var = c.this.c;
            kotlin.d.b.l.a(var, "it");
            var.a(var);
         }
      }));
      kotlin.d.b.l.a(var, "monzoApi.cards(accountId…ardStorage.saveCard(it) }");
      return var;
   }

   public final io.reactivex.b a(final String var, final co.uk.getmondo.api.model.a var) {
      kotlin.d.b.l.b(var, "cardId");
      kotlin.d.b.l.b(var, "apiCardStatus");
      io.reactivex.b var = this.a.e().firstOrError().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String varx) {
            kotlin.d.b.l.b(varx, "accountId");
            return c.this.b.toggleCard(var, var).b((io.reactivex.d)c.this.c(varx));
         }
      }));
      kotlin.d.b.l.a(var, "accountManager.accountId…untId))\n                }");
      return var;
   }

   public final io.reactivex.n a() {
      io.reactivex.n var = this.a.e().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var) {
            kotlin.d.b.l.b(var, "it");
            return c.this.c.b(var);
         }
      }));
      kotlin.d.b.l.a(var, "accountManager.accountId… { cardStorage.card(it) }");
      return var;
   }

   public final v a(final co.uk.getmondo.d.s var) {
      v var = v.a((Callable)(new Callable() {
         public final v a() {
            co.uk.getmondo.d.a varx = c.this.a.a();
            if(varx == null) {
               kotlin.d.b.l.a();
            }

            v var;
            if(varx.e()) {
               var = c.this.a(varx.a(), var).a((Object)com.c.b.b.c());
            } else {
               var = c.this.b(varx.a(), var).d((io.reactivex.c.h)null.a);
            }

            return var;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Single.defer {\n         …}\n            }\n        }");
      return var;
   }

   public final v a(final String var) {
      kotlin.d.b.l.b(var, "accountId");
      v var = v.a((Callable)(new Callable() {
         public final v a() {
            v varx;
            if(c.this.c.a(var) == null) {
               varx = c.this.d(var).d().d((io.reactivex.c.h)null.a);
            } else {
               varx = v.a((Object)Boolean.valueOf(true));
            }

            return varx;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var, "Single.defer {\n         …)\n            }\n        }");
      return var;
   }

   public final v a(final String var, final boolean var) {
      kotlin.d.b.l.b(var, "cardToken");
      v var = v.a((Callable)(new Callable() {
         public final v a() {
            String varx = c.this.a.c();
            if(varx == null) {
               kotlin.d.b.l.a();
            }

            v var;
            if(var) {
               co.uk.getmondo.d.g varx = c.this.c.a(varx);
               if(varx == null) {
                  throw (Throwable)(new RuntimeException("Trying to activate a replacement card but there isn't a previous card saved"));
               }

               var = c.this.b.activateReplacementCard(varx, varx.a(), var).d((io.reactivex.c.h)null.a);
            } else {
               var = c.this.b.activateCardPrepaid(varx, var).d((io.reactivex.c.h)null.a);
            }

            return var;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var) {
            s var = c.this.c;
            kotlin.d.b.l.a(var, "card");
            var.a(var);
         }
      }));
      kotlin.d.b.l.a(var, "activationObservable\n   …dStorage.saveCard(card) }");
      return var;
   }

   public final co.uk.getmondo.d.g b() {
      String var = this.a.c();
      co.uk.getmondo.d.g var;
      if(var != null) {
         var = this.c.a(var);
      } else {
         var = null;
      }

      return var;
   }

   public final v b(final String var) {
      kotlin.d.b.l.b(var, "cardPan");
      v var = this.a.e().firstOrError().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String varx) {
            kotlin.d.b.l.b(varx, "accountId");
            return c.this.b.activateCardBank(varx, var).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(co.uk.getmondo.d.g varx) {
                  s var = c.this.c;
                  kotlin.d.b.l.a(varx, "card");
                  var.a(varx);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var, "accountManager.accountId…card) }\n                }");
      return var;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var = this.a.e().firstOrError().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String var) {
            kotlin.d.b.l.b(var, "it");
            return c.this.c(var);
         }
      }));
      kotlin.d.b.l.a(var, "accountManager.accountId… syncCardForAccount(it) }");
      return var;
   }
}
