package co.uk.getmondo.card;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.card.activate.ActivateCardActivity;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.ActionView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.overdraft.ChangeOverdraftLimitActivity;
import co.uk.getmondo.pin.ForgotPinActivity;
import co.uk.getmondo.topup.TopUpActivity;
import co.uk.getmondo.topup.bank.TopUpInstructionsActivity;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.ab;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001f\u001a\u00020\nH\u0016J\b\u0010 \u001a\u00020\nH\u0016J\b\u0010!\u001a\u00020\nH\u0016J\b\u0010\"\u001a\u00020\nH\u0016J\b\u0010#\u001a\u00020\nH\u0016J\"\u0010$\u001a\u00020\n2\u0006\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u00072\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\u0012\u0010)\u001a\u00020\n2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J&\u0010,\u001a\u0004\u0018\u00010-2\u0006\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u0001012\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\b\u00102\u001a\u00020\nH\u0016J\u001a\u00103\u001a\u00020\n2\u0006\u00104\u001a\u00020-2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\b\u00105\u001a\u00020\nH\u0016J\b\u00106\u001a\u00020\nH\u0016J\b\u00107\u001a\u00020\nH\u0016J\b\u00108\u001a\u00020\nH\u0016J\b\u00109\u001a\u00020\nH\u0016J\u0010\u0010:\u001a\u00020\n2\u0006\u0010;\u001a\u00020<H\u0016J\b\u0010=\u001a\u00020\nH\u0016J\b\u0010>\u001a\u00020\nH\u0016J\u0010\u0010?\u001a\u00020\n2\u0006\u0010@\u001a\u00020\u0005H\u0016J\u0010\u0010A\u001a\u00020\n2\u0006\u0010B\u001a\u00020\u0005H\u0016J\u0010\u0010C\u001a\u00020\n2\u0006\u0010D\u001a\u00020<H\u0016J\b\u0010E\u001a\u00020\nH\u0016J\u0018\u0010F\u001a\u00020\n2\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020<H\u0016J\b\u0010J\u001a\u00020\nH\u0016J\b\u0010K\u001a\u00020\nH\u0016J\b\u0010L\u001a\u00020\nH\u0016J\b\u0010M\u001a\u00020\nH\u0016J\u0018\u0010N\u001a\u00020\n2\u0006\u0010O\u001a\u00020<2\u0006\u0010P\u001a\u00020HH\u0016J\u0010\u0010Q\u001a\u00020\n2\u0006\u0010R\u001a\u00020\u0005H\u0016J\b\u0010S\u001a\u00020\nH\u0016J\b\u0010T\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\fR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\fR\u001a\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\fR\u001a\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\fR\u001a\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\fR\u001a\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\f¨\u0006U"},
   d2 = {"Lco/uk/getmondo/card/CardFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/card/CardPresenter$View;", "()V", "KEY_FORGOT_PIN", "", "REQUEST_CODE_PIN", "", "activateReplacementCardClicks", "Lio/reactivex/Observable;", "", "getActivateReplacementCardClicks", "()Lio/reactivex/Observable;", "applyForOverdraftClicks", "getApplyForOverdraftClicks", "cardPresenter", "Lco/uk/getmondo/card/CardPresenter;", "getCardPresenter", "()Lco/uk/getmondo/card/CardPresenter;", "setCardPresenter", "(Lco/uk/getmondo/card/CardPresenter;)V", "cardReplacementClicks", "getCardReplacementClicks", "forgotPinClicks", "getForgotPinClicks", "manageOverdraftClicks", "getManageOverdraftClicks", "toggleCardClicks", "getToggleCardClicks", "topUpClicks", "getTopUpClicks", "hideActivateReplacementCardButton", "hideCardLoading", "hideCardReplacementButton", "hideOverdraftAction", "hideToggleButton", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "openActivateCard", "openCardReplacement", "openForgotPin", "openOverdraftManagement", "openOverdraftTour", "openTopUp", "isRetail", "", "setCardActive", "setCardInactive", "setExpirationDate", "expires", "setLastFourDigits", "lastDigits", "setToggleButtonEnabled", "enabled", "showActivateReplacementCardButton", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showOnToolbar", "showCardFrozen", "showCardLoading", "showCardReplacementButton", "showForgotPinDialog", "showOverdraftAction", "hasOverdraft", "limit", "showProcessorToken", "processorToken", "showTitle", "showToggleButton", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends co.uk.getmondo.common.f.a implements e.a {
   public e a;
   private final int c = 1;
   private final String d = "KEY_FORGOT_PIN";
   private HashMap e;

   public void A() {
      if(this.e != null) {
         this.e.clear();
      }

   }

   public View a(int var) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var = (View)this.e.get(Integer.valueOf(var));
      View var = var;
      if(var == null) {
         var = this.getView();
         if(var == null) {
            var = null;
         } else {
            var = var.findViewById(var);
            this.e.put(Integer.valueOf(var), var);
         }
      }

      return var;
   }

   public io.reactivex.n a() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void a(co.uk.getmondo.d.c var, boolean var) {
      kotlin.d.b.l.b(var, "balance");
      ab var = ab.a;
      String var = this.getString(2131362050);
      kotlin.d.b.l.a(var, "getString(R.string.card_balance_format)");
      Object[] var = new Object[]{var.toString()};
      String var = String.format(var, Arrays.copyOf(var, var.length));
      kotlin.d.b.l.a(var, "java.lang.String.format(format, *args)");
      if(var) {
         ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setSubtitle((CharSequence)var);
      } else {
         ((ActionView)this.a(co.uk.getmondo.c.a.topupActionView)).setActionSubtitle(var);
      }

   }

   public void a(String var) {
      kotlin.d.b.l.b(var, "processorToken");
      ((TextView)this.a(co.uk.getmondo.c.a.tokenView)).setText((CharSequence)var);
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.tokenView)));
   }

   public void a(boolean var) {
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setEnabled(var);
   }

   public void a(boolean var, co.uk.getmondo.d.c var) {
      kotlin.d.b.l.b(var, "limit");
      if(var) {
         ae.a((View)((ActionView)this.a(co.uk.getmondo.c.a.manageOverdraftActionView)));
         ((ActionView)this.a(co.uk.getmondo.c.a.manageOverdraftActionView)).setActionSubtitle(this.getString(2131362041, new Object[]{var}));
         ae.b((ActionView)this.a(co.uk.getmondo.c.a.applyForOverdraftActionView));
      } else {
         ae.a((View)((ActionView)this.a(co.uk.getmondo.c.a.applyForOverdraftActionView)));
         ae.b((ActionView)this.a(co.uk.getmondo.c.a.manageOverdraftActionView));
      }

   }

   public void a_() {
      ae.a((View)((ActionView)this.a(co.uk.getmondo.c.a.activateCardActionView)));
   }

   public io.reactivex.n b() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.orderReplacementActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void b(String var) {
      kotlin.d.b.l.b(var, "expires");
      ((TextView)this.a(co.uk.getmondo.c.a.expiresView)).setText((CharSequence)var);
   }

   public void b(boolean var) {
      if(var) {
         Context var = this.getContext();
         TopUpInstructionsActivity.a var = TopUpInstructionsActivity.b;
         Context var = this.getContext();
         kotlin.d.b.l.a(var, "context");
         var.startActivity(var.a(var));
      } else {
         TopUpActivity.a((Context)this.getActivity());
      }

   }

   public void b_() {
      (new co.uk.getmondo.common.d.c()).show(this.getFragmentManager(), this.d);
   }

   public io.reactivex.n c() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.activateCardActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void c(String var) {
      kotlin.d.b.l.b(var, "lastDigits");
      ((TextView)this.a(co.uk.getmondo.c.a.cardNumbers4Textview)).setText((CharSequence)var);
   }

   public io.reactivex.n d() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.pinActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n e() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.topupActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n f() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.applyForOverdraftActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public io.reactivex.n g() {
      io.reactivex.n var = com.b.a.c.c.a((ActionView)this.a(co.uk.getmondo.c.a.manageOverdraftActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var, "RxView.clicks(this).map(VoidToUnit)");
      return var;
   }

   public void h() {
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setActionTitle(this.getString(2131362044));
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setActionTitleColor(2131689665);
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setPlaceholderIcon(2130837776);
      ((ImageView)this.a(co.uk.getmondo.c.a.cardView)).setImageDrawable(this.getActivity().getDrawable(2130837636));
   }

   public void i() {
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setActionTitle(this.getString(2131362039));
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setActionTitleColor(0);
      ((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)).setPlaceholderIcon(2130837777);
      ((ImageView)this.a(co.uk.getmondo.c.a.cardView)).setImageDrawable(this.getActivity().getDrawable(2130837635));
   }

   public void j() {
      ae.a((View)((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView)));
   }

   public void k() {
      ae.b((ActionView)this.a(co.uk.getmondo.c.a.toggleCardActionView));
   }

   public void l() {
      ae.a((View)((ActionView)this.a(co.uk.getmondo.c.a.orderReplacementActionView)));
   }

   public void m() {
      ae.b((ActionView)this.a(co.uk.getmondo.c.a.orderReplacementActionView));
   }

   public void n() {
      this.startActivity(CardReplacementActivity.a((Context)this.getActivity()));
   }

   public void o() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.cardProgressbar)));
      ae.a(this.a(co.uk.getmondo.c.a.toggleCardOverlayView));
   }

   public void onActivityResult(int var, int var, Intent var) {
      if(var == this.c) {
         if(var != null && var.hasExtra("pin_error_msg")) {
            this.d(var.getStringExtra("pin_error_msg"));
         }
      } else {
         super.onActivityResult(var, var, var);
      }

   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      kotlin.d.b.l.b(var, "inflater");
      return var.inflate(2131034266, var, false);
   }

   public void onDestroyView() {
      e var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("cardPresenter");
      }

      var.b();
      super.onDestroyView();
      this.A();
   }

   public void onViewCreated(View var, Bundle var) {
      kotlin.d.b.l.b(var, "view");
      super.onViewCreated(var, var);
      e var = this.a;
      if(var == null) {
         kotlin.d.b.l.b("cardPresenter");
      }

      var.a((e.a)this);
      android.support.v4.app.j var = this.getActivity();
      if(var == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var = (HomeActivity)var;
         var.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         kotlin.d.b.l.a(var, "toolbar");
         var.a(var);
      }
   }

   public void p() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.cardProgressbar));
      ae.b(this.a(co.uk.getmondo.c.a.toggleCardOverlayView));
   }

   public void r() {
      ae.b((ActionView)this.a(co.uk.getmondo.c.a.activateCardActionView));
   }

   public void s() {
      ActivateCardActivity.a var = ActivateCardActivity.b;
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      this.startActivity(var.a((Context)var, true, co.uk.getmondo.common.activities.b.a.b));
   }

   public void t() {
      this.startActivityForResult(new Intent((Context)this.getActivity(), ForgotPinActivity.class), this.c);
   }

   public void v() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362480);
   }

   public void w() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362481);
   }

   public void x() {
      ae.b((ActionView)this.a(co.uk.getmondo.c.a.applyForOverdraftActionView));
      ae.b((ActionView)this.a(co.uk.getmondo.c.a.manageOverdraftActionView));
   }

   public void y() {
      ChangeOverdraftLimitActivity.a var = ChangeOverdraftLimitActivity.b;
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      this.startActivity(var.a((Context)var));
   }

   public void z() {
      ChangeOverdraftLimitActivity.a var = ChangeOverdraftLimitActivity.b;
      android.support.v4.app.j var = this.getActivity();
      kotlin.d.b.l.a(var, "activity");
      this.startActivity(var.a((Context)var));
   }
}
