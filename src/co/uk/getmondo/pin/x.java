package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Window;

public class x extends android.support.v4.app.i {
   public static final String a = x.class.getSimpleName();
   private String b;
   private String c;
   private x.a d;

   public static x a(String var, String var) {
      x var = new x();
      Bundle var = new Bundle();
      var.putString("title", var);
      var.putString("message", var);
      var.setArguments(var);
      var.setCancelable(false);
      return var;
   }

   // $FF: synthetic method
   static void a(x var, DialogInterface var, int var) {
      if(var.d != null) {
         var.d.C();
      }

   }

   // $FF: synthetic method
   static void b(x var, DialogInterface var, int var) {
      if(var.d != null) {
         var.d.a();
      }

   }

   public void a(x.a var) {
      this.d = var;
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      if(this.getArguments().containsKey("title")) {
         this.b = this.getArguments().getString("title", "");
      }

      if(this.getArguments().containsKey("message")) {
         this.c = this.getArguments().getString("message", "");
      }

   }

   public Dialog onCreateDialog(Bundle var) {
      AlertDialog var = (new Builder(this.getActivity(), 2131493138)).setTitle(this.b).setMessage(this.c).setNeutralButton(2131362499, y.a(this)).setPositiveButton(2131362091, z.a(this)).create();
      Window var = var.getWindow();
      if(var != null) {
         var.clearFlags(2);
      }

      return var;
   }

   interface a {
      void C();

      void a();
   }
}
