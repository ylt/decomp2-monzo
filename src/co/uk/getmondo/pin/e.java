package co.uk.getmondo.pin;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import java.util.concurrent.TimeUnit;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

class e extends co.uk.getmondo.common.ui.b {
   private final DateTimeFormatter c = DateTimeFormatter.a("dd/MM/yyyy");
   private LocalDate d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;
   private final io.reactivex.u g;
   private final co.uk.getmondo.pin.a.a h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.signup_old.s j;
   private final co.uk.getmondo.common.a k;

   e(io.reactivex.u var, io.reactivex.u var, io.reactivex.u var, co.uk.getmondo.pin.a.a var, co.uk.getmondo.common.e.a var, co.uk.getmondo.signup_old.s var, co.uk.getmondo.common.a var) {
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
      this.j = var;
      this.k = var;
   }

   // $FF: synthetic method
   static String a(String var) throws Exception {
      return var.replace(" ", "");
   }

   // $FF: synthetic method
   static void a(e.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.B();
   }

   // $FF: synthetic method
   static void a(e.a var, io.reactivex.b.b var) throws Exception {
      var.i();
   }

   // $FF: synthetic method
   static void a(e.a var, Boolean var, Throwable var) throws Exception {
      var.k();
   }

   // $FF: synthetic method
   static void a(e.a var, Object var) throws Exception {
      var.B();
   }

   // $FF: synthetic method
   static void a(e var, e.a var, Boolean var) throws Exception {
      if(var.booleanValue()) {
         var.k.a(Impression.M());
         var.z();
      } else {
         var.v();
      }

   }

   // $FF: synthetic method
   static void a(e var, e.a var, Object var) throws Exception {
      var.x();
      var.k.a(Impression.N());
      var.y();
   }

   // $FF: synthetic method
   static void a(e var, e.a var, String var, String var) throws Exception {
      if(var.j.a(var)) {
         var.d = LocalDate.a((CharSequence)var, (DateTimeFormatter)var.c);
         var.w();
         var.k.a(Impression.P());
         var.d(var);
      } else {
         var.g();
      }

   }

   // $FF: synthetic method
   static void a(e var, e.a var, Throwable var) throws Exception {
      var.k();
      if(var.a(var)) {
         var.k.a(Impression.M());
         var.z();
      } else {
         var.i.a(var, var);
      }

   }

   private boolean a(Throwable var) {
      boolean var;
      if(this.b(var) != null) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   private co.uk.getmondo.api.model.pin.a b(Throwable var) {
      Object var = null;
      co.uk.getmondo.api.model.pin.a var;
      if(!(var instanceof ApiException)) {
         var = (co.uk.getmondo.api.model.pin.a)var;
      } else {
         co.uk.getmondo.api.model.b var = ((ApiException)var).e();
         var = (co.uk.getmondo.api.model.pin.a)var;
         if(var != null) {
            var = (co.uk.getmondo.api.model.pin.a)co.uk.getmondo.common.e.d.a(co.uk.getmondo.api.model.pin.a.values(), var.a());
         }
      }

      return var;
   }

   // $FF: synthetic method
   static io.reactivex.r b(e var, e.a var, Object var) throws Exception {
      return var.h.a(var.d).b(var.f).a(var.e).c(m.a(var)).a(n.a(var, var)).a((io.reactivex.r)io.reactivex.n.just(co.uk.getmondo.common.b.a.a));
   }

   // $FF: synthetic method
   static void b(e.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.k();
   }

   // $FF: synthetic method
   static void b(e.a var, io.reactivex.b.b var) throws Exception {
      var.h();
   }

   // $FF: synthetic method
   static void b(e var, e.a var, Throwable var) throws Exception {
      var.i.a(var, var);
   }

   // $FF: synthetic method
   static void c(e.a var, co.uk.getmondo.common.b.a var) throws Exception {
      var.j();
   }

   // $FF: synthetic method
   static void c(e var, e.a var, Object var) throws Exception {
      var.k.a(Impression.O());
      var.x();
   }

   // $FF: synthetic method
   static void d(e var, e.a var, Object var) throws Exception {
      var.k.a(Impression.a(Impression.IntercomFrom.PIN));
      var.A();
      var.B();
   }

   public void a(e.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.a((io.reactivex.b.b)var.b().subscribe(f.a(var)));
      this.a((io.reactivex.b.b)var.f().subscribe(o.a(this, var)));
      String var = this.h.a();
      if(co.uk.getmondo.common.k.p.d(var)) {
         this.k.a(Impression.M());
         var.z();
      } else {
         this.a((io.reactivex.b.b)this.h.b().b(this.f).a(this.e).b(p.a(var)).a(q.a(var)).a(r.a(this, var), s.a(this, var)));
         this.a((io.reactivex.b.b)var.c().map(t.a()).subscribe(u.a(this, var, var)));
         this.a((io.reactivex.b.b)var.d().doOnNext(v.a(this, var)).switchMap(g.a(this, var)).doOnNext(h.a(var)).delay(5L, TimeUnit.SECONDS, this.g).doOnNext(i.a(var)).subscribe(j.a(var), k.a()));
         this.a((io.reactivex.b.b)var.e().subscribe(l.a(this, var)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      void d(String var);

      io.reactivex.n e();

      io.reactivex.n f();

      void g();

      void h();

      void i();

      void j();

      void k();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
