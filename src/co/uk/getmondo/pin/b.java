package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

// $FF: synthetic class
final class b implements OnShowListener {
   private final DateOfBirthDialogFragment a;
   private final AlertDialog b;

   private b(DateOfBirthDialogFragment var, AlertDialog var) {
      this.a = var;
      this.b = var;
   }

   public static OnShowListener a(DateOfBirthDialogFragment var, AlertDialog var) {
      return new b(var, var);
   }

   public void onShow(DialogInterface var) {
      DateOfBirthDialogFragment.a(this.a, this.b, var);
   }
}
