package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.Unbinder;

public class DateOfBirthDialogFragment extends android.support.v4.app.i {
   public static final String a = DateOfBirthDialogFragment.class.getSimpleName();
   private Unbinder b;
   private DateOfBirthDialogFragment.a c;
   @BindView(2131821254)
   EditText dobEditText;
   @BindView(2131821163)
   TextInputLayout dobWrapper;

   public static DateOfBirthDialogFragment a() {
      DateOfBirthDialogFragment var = new DateOfBirthDialogFragment();
      var.setCancelable(false);
      return var;
   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var, AlertDialog var, DialogInterface var) {
      var.getButton(-1).setOnClickListener(c.a(var));
   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var, DialogInterface var, int var) {
      if(var.c != null) {
         var.c.a();
      }

   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var, View var) {
      if(var.c != null) {
         var.c.a(var.dobEditText.getText().toString());
      }

   }

   public void a(DateOfBirthDialogFragment.a var) {
      this.c = var;
   }

   public void a(String var) {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(var);
   }

   public Dialog onCreateDialog(Bundle var) {
      View var = LayoutInflater.from(this.getActivity()).inflate(2131034261, (ViewGroup)null);
      this.b = ButterKnife.bind(this, (View)var);
      final co.uk.getmondo.signup_old.r var = new co.uk.getmondo.signup_old.r();
      this.dobEditText.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var) {
         }

         public void beforeTextChanged(CharSequence var, int varx, int var, int var) {
         }

         public void onTextChanged(CharSequence var, int varx, int var, int var) {
            DateOfBirthDialogFragment.this.dobWrapper.setErrorEnabled(false);
            DateOfBirthDialogFragment.this.dobWrapper.setError((CharSequence)null);
            var.a(var, var, var, this, DateOfBirthDialogFragment.this.dobEditText);
         }
      });
      AlertDialog var = (new Builder(this.getActivity(), 2131493138)).setTitle(2131362563).setView(var).setPositiveButton(this.getString(2131362493), (OnClickListener)null).setNeutralButton(this.getString(2131362499), a.a(this)).setCancelable(false).create();
      var.setOnShowListener(b.a(this, var));
      Window var = var.getWindow();
      if(var != null) {
         var.clearFlags(2);
         var.setSoftInputMode(4);
      }

      return var;
   }

   public void onDestroyView() {
      this.b.unbind();
      super.onDestroyView();
   }

   @OnEditorAction({2131821254})
   protected boolean onNext(int var) {
      boolean var = false;
      boolean var = var;
      if(var == 6) {
         if(this.c == null) {
            var = var;
         } else {
            this.c.a(this.dobEditText.getText().toString());
            var = true;
         }
      }

      return var;
   }

   interface a {
      void a();

      void a(String var);
   }
}
