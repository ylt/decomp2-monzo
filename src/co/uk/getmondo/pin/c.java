package co.uk.getmondo.pin;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class c implements OnClickListener {
   private final DateOfBirthDialogFragment a;

   private c(DateOfBirthDialogFragment var) {
      this.a = var;
   }

   public static OnClickListener a(DateOfBirthDialogFragment var) {
      return new c(var);
   }

   public void onClick(View var) {
      DateOfBirthDialogFragment.a(this.a, var);
   }
}
