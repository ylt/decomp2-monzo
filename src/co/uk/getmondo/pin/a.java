package co.uk.getmondo.pin;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final DateOfBirthDialogFragment a;

   private a(DateOfBirthDialogFragment var) {
      this.a = var;
   }

   public static OnClickListener a(DateOfBirthDialogFragment var) {
      return new a(var);
   }

   public void onClick(DialogInterface var, int var) {
      DateOfBirthDialogFragment.a(this.a, var, var);
   }
}
