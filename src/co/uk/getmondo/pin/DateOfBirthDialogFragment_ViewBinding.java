package co.uk.getmondo.pin;

import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class DateOfBirthDialogFragment_ViewBinding implements Unbinder {
   private DateOfBirthDialogFragment a;
   private View b;

   public DateOfBirthDialogFragment_ViewBinding(final DateOfBirthDialogFragment var, View var) {
      this.a = var;
      var.dobWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var, 2131821163, "field 'dobWrapper'", TextInputLayout.class);
      var = Utils.findRequiredView(var, 2131821254, "field 'dobEditText' and method 'onNext'");
      var.dobEditText = (EditText)Utils.castView(var, 2131821254, "field 'dobEditText'", EditText.class);
      this.b = var;
      ((TextView)var).setOnEditorActionListener(new OnEditorActionListener() {
         public boolean onEditorAction(TextView varx, int var, KeyEvent var) {
            return var.onNext(var);
         }
      });
   }

   public void unbind() {
      DateOfBirthDialogFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.dobWrapper = null;
         var.dobEditText = null;
         ((TextView)this.b).setOnEditorActionListener((OnEditorActionListener)null);
         this.b = null;
      }
   }
}
