package co.uk.getmondo.pin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ForgotPinActivity extends co.uk.getmondo.common.activities.b implements DateOfBirthDialogFragment.a, SmsDialogFragment.a, e.a, x.a {
   e a;
   co.uk.getmondo.common.q b;
   private com.b.b.c c = com.b.b.c.a();
   private com.b.b.c e = com.b.b.c.a();
   private com.b.b.c f = com.b.b.c.a();
   private com.b.b.c g = com.b.b.c.a();
   private com.b.b.c h = com.b.b.c.a();
   private ProgressDialog i;

   private void e(String var) {
      Fragment var = this.getSupportFragmentManager().a(var);
      if(var != null && var instanceof android.support.v4.app.i) {
         ((android.support.v4.app.i)var).dismiss();
      }

   }

   private void f(String var) {
      if(this.i == null) {
         this.i = new ProgressDialog(this);
         this.i.setIndeterminate(true);
      }

      this.i.setMessage(var);
      this.i.show();
   }

   public void A() {
      this.b.a();
   }

   public void B() {
      this.setResult(-1);
      this.finish();
   }

   public void C() {
      this.h.a((Object)co.uk.getmondo.common.b.a.a);
   }

   public void D() {
      this.f.a((Object)co.uk.getmondo.common.b.a.a);
   }

   public void E() {
      this.g.a((Object)co.uk.getmondo.common.b.a.a);
   }

   public void a() {
      this.c.a((Object)co.uk.getmondo.common.b.a.a);
   }

   public void a(String var) {
      this.e.a((Object)var);
   }

   public io.reactivex.n b() {
      return this.c;
   }

   public void b(int var) {
      Intent var = new Intent();
      var.putExtra("pin_error_msg", this.getString(var));
      this.setResult(0, var);
      this.finish();
   }

   public io.reactivex.n c() {
      return this.e;
   }

   public io.reactivex.n d() {
      return this.f;
   }

   public void d(String var) {
      SmsDialogFragment var = SmsDialogFragment.a(var);
      var.a((SmsDialogFragment.a)this);
      var.show(this.getSupportFragmentManager(), SmsDialogFragment.a);
   }

   public io.reactivex.n e() {
      return this.g;
   }

   public io.reactivex.n f() {
      return this.h;
   }

   public void g() {
      Fragment var = this.getSupportFragmentManager().a(DateOfBirthDialogFragment.a);
      if(var != null && var instanceof DateOfBirthDialogFragment) {
         ((DateOfBirthDialogFragment)var).a(this.getString(2131362171));
      }

   }

   public void h() {
      this.f(this.getString(2131362402));
   }

   public void i() {
      this.f(this.getString(2131362574));
   }

   public void j() {
      this.f(this.getString(2131362568));
   }

   public void k() {
      if(this.i != null) {
         this.i.dismiss();
         this.i = null;
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034171);
      this.l().a(this);
      this.a.a((e.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      if(this.i != null && this.i.isShowing()) {
         this.i.dismiss();
      }

      super.onDestroy();
   }

   public void v() {
      DateOfBirthDialogFragment var = DateOfBirthDialogFragment.a();
      var.a((DateOfBirthDialogFragment.a)this);
      var.show(this.getSupportFragmentManager(), DateOfBirthDialogFragment.a);
   }

   public void w() {
      this.e(DateOfBirthDialogFragment.a);
   }

   public void x() {
      this.e(SmsDialogFragment.a);
   }

   public void y() {
      x var = x.a(this.getString(2131362571), this.getString(2131362570));
      var.a(this);
      var.show(this.getSupportFragmentManager(), x.a);
   }

   public void z() {
      x var = x.a(this.getString(2131362573), this.getString(2131362572));
      var.a(this);
      var.show(this.getSupportFragmentManager(), x.a);
   }
}
