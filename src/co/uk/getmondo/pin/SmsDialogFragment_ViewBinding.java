package co.uk.getmondo.pin;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SmsDialogFragment_ViewBinding implements Unbinder {
   private SmsDialogFragment a;
   private View b;
   private View c;
   private View d;

   public SmsDialogFragment_ViewBinding(final SmsDialogFragment var, View var) {
      this.a = var;
      View var = Utils.findRequiredView(var, 2131821255, "field 'textMeButton' and method 'onTextMe'");
      var.textMeButton = (Button)Utils.castView(var, 2131821255, "field 'textMeButton'", Button.class);
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onTextMe();
         }
      });
      var = Utils.findRequiredView(var, 2131821256, "method 'onChangedNumber'");
      this.c = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onChangedNumber();
         }
      });
      var = Utils.findRequiredView(var, 2131821257, "method 'onNotNow'");
      this.d = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.onNotNow();
         }
      });
   }

   public void unbind() {
      SmsDialogFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.textMeButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
