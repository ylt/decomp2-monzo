package co.uk.getmondo.waitlist;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class ShareActivity_ViewBinding implements Unbinder {
   private ShareActivity a;
   private View b;

   public ShareActivity_ViewBinding(final ShareActivity var, View var) {
      this.a = var;
      var.link = (TextView)Utils.findRequiredViewAsType(var, 2131821080, "field 'link'", TextView.class);
      var.description = (TextView)Utils.findRequiredViewAsType(var, 2131821081, "field 'description'", TextView.class);
      var = Utils.findRequiredView(var, 2131821082, "method 'share'");
      this.b = var;
      var.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View varx) {
            var.share();
         }
      });
   }

   public void unbind() {
      ShareActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.link = null;
         var.description = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
