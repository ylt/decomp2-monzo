package co.uk.getmondo.waitlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class ShareActivity extends co.uk.getmondo.common.activities.h {
   co.uk.getmondo.common.a a;
   co.uk.getmondo.common.accounts.d b;
   @BindView(2131821081)
   TextView description;
   @BindView(2131821080)
   TextView link;

   public static void a(Activity var) {
      var.startActivity(new Intent(var, ShareActivity.class));
   }

   private void a(ak var) {
      this.link.setText(var.e().g());
      int var = var.e().h();
      this.description.setText(this.getString(2131362182, new Object[]{Integer.valueOf(var), this.getResources().getQuantityString(2131886081, var)}));
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034206);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a(Impression.g());
      this.a(this.b.b());
   }

   @OnClick({2131821082})
   void share() {
      this.a.a(Impression.h());
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362700), this.link.getText().toString(), co.uk.getmondo.api.model.tracking.a.WAITING_LIST_SHARE));
   }
}
