package co.uk.getmondo.waitlist;

import android.content.Context;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var;
      if(!j.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public j(javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(javax.a.a var) {
      return new j(var);
   }

   public i a() {
      return new i((Context)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
