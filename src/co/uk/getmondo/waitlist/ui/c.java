package co.uk.getmondo.waitlist.ui;

// $FF: synthetic class
final class c implements Runnable {
   private final ParallaxAnimationView a;

   private c(ParallaxAnimationView var) {
      this.a = var;
   }

   public static Runnable a(ParallaxAnimationView var) {
      return new c(var);
   }

   public void run() {
      ParallaxAnimationView.a(this.a);
   }
}
