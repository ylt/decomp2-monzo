package co.uk.getmondo.waitlist.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

public class ParallaxTouchInterceptorView extends FrameLayout {
   private int a;
   private ObjectAnimator b;
   private ParallaxTouchInterceptorView.a c;
   private float d;
   private ParallaxSwipeToRefresh e;
   private int f;
   private int g;
   private int h;
   private int i;

   public ParallaxTouchInterceptorView(Context var) {
      super(var);
      this.a(var, (AttributeSet)null);
   }

   public ParallaxTouchInterceptorView(Context var, AttributeSet var) {
      super(var, var);
      this.a(var, var);
   }

   public ParallaxTouchInterceptorView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.a(var, var);
   }

   private int a(float var) {
      float var;
      int var;
      if(var > 0.0F) {
         var = var;
         if((double)var > (double)this.i * 1.5D) {
            var = (float)this.i * 1.5F;
         }

         var = (int)((1.5D - Math.pow((double)(var / ((float)this.i * 1.5F)), 0.125D)) * (double)var);
      } else {
         var = var;
         if(var < (float)this.h * 1.5F) {
            var = (float)this.h * 1.5F;
         }

         var = (int)((1.5D - Math.pow((double)(var / ((float)this.h * 1.5F)), 0.012500000186264515D)) * (double)var);
      }

      return var;
   }

   private void a(int var, int var, int var) {
      if(this.b != null) {
         this.b.cancel();
      }

      this.b = ObjectAnimator.ofInt(this, "animatedPosition", new int[]{var, var});
      this.b.setInterpolator(new AccelerateDecelerateInterpolator());
      this.b.setDuration((long)var);
      this.b.start();
   }

   private void a(Context var, AttributeSet var) {
      if(var != null) {
         TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.ParallaxTouchInterceptorView, 0, 0);

         try {
            this.f = var.getDimensionPixelSize(2, 0);
            this.g = var.getDimensionPixelSize(3, 0);
            this.h = var.getDimensionPixelSize(0, 0);
            this.i = var.getDimensionPixelSize(1, 0);
         } finally {
            var.recycle();
         }
      }

   }

   public void a() {
      this.a(this.a, this.f, 250);
   }

   public void a(ParallaxSwipeToRefresh var) {
      this.e = var;
   }

   public void b() {
      this.a(this.g, this.f, 3500);
   }

   public boolean onTouchEvent(MotionEvent var) {
      this.e.onTouchEvent(var);
      switch(var.getAction()) {
      case 0:
         this.d = var.getY();
         break;
      case 1:
         this.d = -1.0F;
         this.a();
         break;
      case 2:
         if(this.d != -1.0F) {
            this.setPosition(-(var.getY() - this.d));
         }
         break;
      case 3:
         this.d = -1.0F;
         this.a();
      }

      return true;
   }

   public void setAnimatedPosition(int var) {
      this.a = var;
      this.c.a((float)var);
   }

   public void setInitialPosition(boolean var) {
      if(var) {
         this.setAnimatedPosition(this.g);
      } else {
         this.setAnimatedPosition(this.f);
      }

   }

   public void setOnYAxisScrollChangeListener(ParallaxTouchInterceptorView.a var) {
      this.c = var;
   }

   public void setPosition(float var) {
      float var = (float)this.a(var);
      var = var;
      if(var < (float)this.h) {
         var = (float)this.h;
      }

      var = var;
      if(var > (float)this.i) {
         var = (float)this.i;
      }

      int var = this.f;
      this.a = (int)var + var;
      this.c.a((float)this.a);
   }

   public interface a {
      void a(float var);
   }
}
