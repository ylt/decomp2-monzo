package co.uk.getmondo.waitlist.ui;

// $FF: synthetic class
final class a implements ParallaxTouchInterceptorView.a {
   private final ParallaxAnimationView a;

   private a(ParallaxAnimationView var) {
      this.a = var;
   }

   public static ParallaxTouchInterceptorView.a a(ParallaxAnimationView var) {
      return new a(var);
   }

   public void a(float var) {
      ParallaxAnimationView.a(this.a, var);
   }
}
