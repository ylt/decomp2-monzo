package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import co.uk.getmondo.d.an;

public class ParallaxAnimationView extends FrameLayout {
   private ParallaxImageView a;
   private ParallaxImageView b;
   private ParallaxImageView c;
   private ParallaxImageView d;
   private ParallaxImageView e;
   private ParallaxImageView f;
   private ParallaxLinearLayout g;
   private ParallaxImageView h;
   private ParallaxSwipeToRefresh i;
   private ParallaxTouchInterceptorView j;
   private com.c.b.b k;
   private boolean l;

   public ParallaxAnimationView(Context var) {
      this(var, (AttributeSet)null, 0);
   }

   public ParallaxAnimationView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public ParallaxAnimationView(Context var, AttributeSet var, int var) {
      super(var, var, var);
      this.k = com.c.b.b.c();
      LayoutInflater.from(this.getContext()).inflate(2131034359, this, true);
   }

   private void a(float var) {
      this.h.a(var);
      this.a.a(var);
      this.b.a(var);
      this.c.a(var);
      this.d.a(var);
      this.e.a(var);
      this.f.a(var);
      this.g.a((int)var);
   }

   // $FF: synthetic method
   static void a(ParallaxAnimationView var) {
      if(var.l) {
         var.l = false;
         var.j.b();
      }

   }

   // $FF: synthetic method
   static void a(ParallaxAnimationView var, float var) {
      var.a(var);
   }

   private void b() {
      this.k.a(d.a());
   }

   // $FF: synthetic method
   static void b(ParallaxAnimationView var) {
      var.b();
   }

   public void a() {
      this.j.setInitialPosition(false);
   }

   public void a(int var) {
      this.g.b(var);
   }

   public void a(an var) {
      this.g.a(var);
   }

   public void a(an var, boolean var) {
      this.l = var;
      this.g.a(var);
      this.j.setInitialPosition(var);
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      this.a = (ParallaxImageView)this.findViewById(2131821571);
      this.b = (ParallaxImageView)this.findViewById(2131821572);
      this.c = (ParallaxImageView)this.findViewById(2131821573);
      this.d = (ParallaxImageView)this.findViewById(2131821574);
      this.e = (ParallaxImageView)this.findViewById(2131821575);
      this.f = (ParallaxImageView)this.findViewById(2131821576);
      this.g = (ParallaxLinearLayout)this.findViewById(2131821578);
      this.h = (ParallaxImageView)this.findViewById(2131821570);
      this.i = (ParallaxSwipeToRefresh)this.findViewById(2131821577);
      this.j = (ParallaxTouchInterceptorView)this.findViewById(2131821582);
      this.i.setColorSchemeResources(new int[]{2131689477});
      this.j.setOnYAxisScrollChangeListener(a.a(this));
      this.j.a(this.i);
      this.i.setOnRefreshListener(b.a(this));
      this.postDelayed(c.a(this), 150L);
   }

   public void setParallaxAnimationListener(ParallaxAnimationView.a var) {
      this.i.setColorSchemeResources(new int[]{2131689477});
      this.k = com.c.b.b.b(var);
   }

   public void setRefreshing(boolean var) {
      this.i.setRefreshing(var);
   }

   public interface a {
      void a();
   }
}
