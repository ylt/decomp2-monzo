package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ParallaxSwipeToRefresh extends SwipeRefreshLayout {
   private boolean m;

   public ParallaxSwipeToRefresh(Context var) {
      this(var, (AttributeSet)null);
   }

   public ParallaxSwipeToRefresh(Context var, AttributeSet var) {
      super(var, var);
   }

   public boolean onTouchEvent(MotionEvent var) {
      if(!this.m) {
         this.m = this.onInterceptTouchEvent(var);
      }

      boolean var;
      if(this.m) {
         var = super.onTouchEvent(var);
      } else {
         var = false;
      }

      return var;
   }

   public void setRefreshing(boolean var) {
      super.setRefreshing(var);
      this.m = false;
   }
}
