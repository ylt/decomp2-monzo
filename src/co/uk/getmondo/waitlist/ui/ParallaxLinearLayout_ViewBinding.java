package co.uk.getmondo.waitlist.ui;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class ParallaxLinearLayout_ViewBinding implements Unbinder {
   private ParallaxLinearLayout a;

   public ParallaxLinearLayout_ViewBinding(ParallaxLinearLayout var, View var) {
      this.a = var;
      var.imageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821579, "field 'imageView'", ImageView.class);
      var.ahead = (AnimatedNumberView)Utils.findRequiredViewAsType(var, 2131821580, "field 'ahead'", AnimatedNumberView.class);
      var.behind = (AnimatedNumberView)Utils.findRequiredViewAsType(var, 2131821581, "field 'behind'", AnimatedNumberView.class);
   }

   public void unbind() {
      ParallaxLinearLayout var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.imageView = null;
         var.ahead = null;
         var.behind = null;
      }
   }
}
