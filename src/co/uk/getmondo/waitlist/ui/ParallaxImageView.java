package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ParallaxImageView extends ImageView {
   private int a;
   private float b;

   public ParallaxImageView(Context var) {
      this(var, (AttributeSet)null);
   }

   public ParallaxImageView(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public ParallaxImageView(Context var, AttributeSet var, int var) {
      this(var, var, var, 0);
   }

   public ParallaxImageView(Context var, AttributeSet var, int var, int var) {
      super(var, var, var, var);
      if(var != null) {
         TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.ParallaxImageView, 0, 0);

         try {
            this.b = var.getFloat(0, 0.0F);
            this.a = var.getDimensionPixelSize(1, 0);
         } finally {
            var.recycle();
         }
      }

   }

   public void a(float var) {
      this.scrollTo(0, (int)((float)(-this.a) + this.b * var));
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
   }
}
