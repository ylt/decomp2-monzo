package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.ui.AnimatedNumberView;
import co.uk.getmondo.d.an;

public class ParallaxLinearLayout extends LinearLayout {
   private int a;
   @BindView(2131821580)
   AnimatedNumberView ahead;
   private int b;
   @BindView(2131821581)
   AnimatedNumberView behind;
   @BindView(2131821579)
   ImageView imageView;

   public ParallaxLinearLayout(Context var) {
      this(var, (AttributeSet)null);
   }

   public ParallaxLinearLayout(Context var, AttributeSet var) {
      this(var, var, 0);
   }

   public ParallaxLinearLayout(Context var, AttributeSet var, int var) {
      this(var, var, var, 0);
   }

   public ParallaxLinearLayout(Context var, AttributeSet var, int var, int var) {
      super(var, var, var, var);
      this.a = -1;
      MonzoApplication.a(var).b().a(this);
      this.setOrientation(1);
      if(var != null) {
         TypedArray var = var.getTheme().obtainStyledAttributes(var, co.uk.getmondo.c.b.ParallaxLinearLayout, 0, 0);

         try {
            this.b = var.getDimensionPixelSize(0, 0);
         } finally {
            var.recycle();
         }
      }

   }

   public void a(int var) {
      this.scrollTo(0, Float.valueOf((float)var).intValue());
   }

   public void a(an var) {
      if(var != null) {
         this.ahead.setNumber(var.d());
         this.behind.setNumber(var.e());
         int var = var.b();
         if(this.a != var) {
            this.a = var;
            String var = "waiting_list_" + var;
            this.imageView.setImageResource(this.getResources().getIdentifier(var, "drawable", this.getContext().getPackageName()));
         }
      }

   }

   public void b(int var) {
      this.ahead.a(var);
      this.behind.a(var);
   }

   public boolean canScrollVertically(int var) {
      boolean var;
      if(this.getScrollY() >= this.b) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      ButterKnife.bind((View)this);
   }
}
