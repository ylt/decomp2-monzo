package co.uk.getmondo.waitlist.referral;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import okhttp3.HttpUrl;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0011\u0010\u000b\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\f\u001a\u0004\u0018\u00010\r8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/waitlist/referral/InstallReferrer;", "", "referrerUrl", "Lokhttp3/HttpUrl;", "(Lokhttp3/HttpUrl;)V", "hasUtmContent", "", "getHasUtmContent", "()Z", "hasUtmSource", "getHasUtmSource", "isPlayStoreRedirect", "utmContent", "", "getUtmContent", "()Ljava/lang/String;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.a a = new a.a((i)null);
   private final HttpUrl b;

   private a(HttpUrl var) {
      this.b = var;
   }

   // $FF: synthetic method
   public a(HttpUrl var, i var) {
      this(var);
   }

   public static final a a(String var) throws IllegalArgumentException, UnsupportedEncodingException {
      return a.a(var);
   }

   public final boolean a() {
      return this.b.queryParameterNames().contains("utm_source");
   }

   public final boolean b() {
      boolean var;
      if(this.a() && l.a("play_store_redirect", this.b.queryParameter("utm_source"))) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   public final boolean c() {
      return this.b.queryParameterNames().contains("utm_content");
   }

   public final String d() {
      return this.b.queryParameter("utm_content");
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/waitlist/referral/InstallReferrer$Companion;", "", "()V", "PARAM_PLAY_STORE_REDIRECT", "", "PARAM_UTM_CONTENT", "PARAM_UTM_SOURCE", "URL", "parseReferrer", "Lco/uk/getmondo/waitlist/referral/InstallReferrer;", "referrer", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var) {
         this();
      }

      public final a a(String var) throws IllegalArgumentException, UnsupportedEncodingException {
         HttpUrl var;
         if(var != null) {
            var = HttpUrl.parse("http://www.monzo.com?" + URLDecoder.decode(var, "UTF-8"));
         } else {
            var = null;
         }

         if(var != null) {
            return new a(var, (i)null);
         } else {
            throw (Throwable)(new IllegalArgumentException("Couldn't parse referrer: " + var));
         }
      }
   }
}
