package co.uk.getmondo.waitlist.referral;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;
import java.io.UnsupportedEncodingException;

public class InstallReferrerReceiver extends BroadcastReceiver {
   co.uk.getmondo.common.a a;

   public void onReceive(Context var, Intent var) {
      MonzoApplication.a(var).b().a(this);
      if(var.hasExtra("referrer")) {
         String var = var.getStringExtra("referrer");

         try {
            a var = a.a(var);
            if(var.b() && var.c()) {
               this.a.a(Impression.g(var.d()));
            } else {
               this.a.a(Impression.f(var));
            }
         } catch (IllegalArgumentException var) {
            ;
         } catch (UnsupportedEncodingException var) {
            ;
         }
      }

   }
}
