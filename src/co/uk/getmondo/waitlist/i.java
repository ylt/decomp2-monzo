package co.uk.getmondo.waitlist;

import android.content.Context;
import android.content.SharedPreferences;

public class i {
   private final SharedPreferences a;

   public i(Context var) {
      this.a = var.getSharedPreferences("skip_waitlist", 0);
   }

   public void a(String var) {
      this.a.edit().putString("key_card_token", var).apply();
   }

   public boolean a() {
      return this.a.getBoolean("key_skip_waitlist", false);
   }

   public String b() {
      return this.a.getString("key_card_token", "");
   }

   public void c() {
      this.a.edit().clear().apply();
   }
}
