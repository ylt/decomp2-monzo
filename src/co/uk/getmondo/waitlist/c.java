package co.uk.getmondo.waitlist;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.s;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import co.uk.getmondo.d.k;
import io.reactivex.n;
import io.reactivex.u;

public class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;
   private final s i;
   private final i j;
   private boolean k = false;

   c(u var, u var, co.uk.getmondo.common.e.a var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.api.b.a var, s var, i var, co.uk.getmondo.common.a var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.i = var;
      this.j = var;
      this.h = var;
   }

   // $FF: synthetic method
   static ak a(c var, an var) throws Exception {
      return var.f.b();
   }

   private void a(ak var) {
      if(var == null) {
         ((c.a)this.a).b(2131361985);
      } else if(var.e() != null) {
         if(var.e().f()) {
            ((c.a)this.a).f();
         } else if(var.e().a()) {
            ((c.a)this.a).c();
         } else {
            if(var.e().i()) {
               ((c.a)this.a).a(true);
               ((c.a)this.a).a(2131362119);
            } else if(var.e().d() == 0) {
               ((c.a)this.a).a(false);
               ((c.a)this.a).a(2131362901);
            } else {
               ((c.a)this.a).a(true);
               ((c.a)this.a).a(2131362900);
            }

            if(this.k && this.a(var.e())) {
               ((c.a)this.a).b();
            } else if(!this.k) {
               ((c.a)this.a).a(var.e(), this.i.e());
               this.k = true;
            } else {
               ((c.a)this.a).a(var.e());
            }
         }
      }

   }

   // $FF: synthetic method
   static void a(c.a var, Object var) throws Exception {
      var.e();
   }

   // $FF: synthetic method
   static void a(c var, Throwable var) throws Exception {
      var.e.a(var, (co.uk.getmondo.common.e.a.a)var.a);
   }

   // $FF: synthetic method
   static void a(c var, boolean var, ak var) throws Exception {
      if(var) {
         ((c.a)var.a).t();
      }

      var.a(var);
   }

   private void a(boolean var) {
      if(var) {
         ((c.a)this.a).s();
      }

      this.a((io.reactivex.b.b)this.g.b().d(e.a(this)).b(this.d).a(this.c).a(f.a(this, var), g.a(this)));
   }

   private boolean a(an var) {
      boolean var = false;
      if(var != null) {
         String var = this.i.f();
         k var = var.a(var);
         if(var != null) {
            var = var + "|" + var.b();
            this.i.e(var);
            ((c.a)this.a).a(var);
            var = true;
         }
      }

      return var;
   }

   void a() {
      this.a(true);
   }

   public void a(c.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.a((io.reactivex.b.b)var.d().subscribe(d.a(var)));
      if(this.i.e()) {
         this.h.a(Impression.d());
         this.i.a(false);
      } else {
         this.h.a(Impression.e());
      }

      this.a(this.f.b());
   }

   void c() {
      this.a(false);
   }

   void d() {
      this.a(this.f.b().e());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(int var);

      void a(an var);

      void a(an var, boolean var);

      void a(k var);

      void a(boolean var);

      void b();

      void c();

      n d();

      void e();

      void f();

      void s();

      void t();
   }
}
