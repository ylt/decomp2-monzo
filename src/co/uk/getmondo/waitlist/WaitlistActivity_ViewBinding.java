package co.uk.getmondo.waitlist;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.waitlist.ui.ParallaxAnimationView;

public class WaitlistActivity_ViewBinding implements Unbinder {
   private WaitlistActivity a;

   public WaitlistActivity_ViewBinding(WaitlistActivity var, View var) {
      this.a = var;
      var.parallaxAnimationView = (ParallaxAnimationView)Utils.findRequiredViewAsType(var, 2131821175, "field 'parallaxAnimationView'", ParallaxAnimationView.class);
      var.threeFingerView = (TouchInterceptingRelativeLayout)Utils.findRequiredViewAsType(var, 2131821174, "field 'threeFingerView'", TouchInterceptingRelativeLayout.class);
      var.inviteFriendsTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821177, "field 'inviteFriendsTextView'", TextView.class);
      var.bumpButton = Utils.findRequiredView(var, 2131821178, "field 'bumpButton'");
   }

   public void unbind() {
      WaitlistActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.parallaxAnimationView = null;
         var.threeFingerView = null;
         var.inviteFriendsTextView = null;
         var.bumpButton = null;
      }
   }
}
