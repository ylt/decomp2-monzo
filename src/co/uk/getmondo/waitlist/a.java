package co.uk.getmondo.waitlist;

public final class a implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var;
      if(!a.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public a(javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
         }
      }
   }

   public static b.a a(javax.a.a var, javax.a.a var) {
      return new a(var, var);
   }

   public void a(ShareActivity var) {
      if(var == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var.a = (co.uk.getmondo.common.a)this.b.b();
         var.b = (co.uk.getmondo.common.accounts.d)this.c.b();
      }
   }
}
