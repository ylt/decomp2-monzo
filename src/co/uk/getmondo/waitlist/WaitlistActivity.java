package co.uk.getmondo.waitlist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.bump_up.BumpUpActivity;
import co.uk.getmondo.bump_up.WebEventActivity;
import co.uk.getmondo.d.an;
import co.uk.getmondo.d.k;
import co.uk.getmondo.top.NotInCountryActivity;
import co.uk.getmondo.waitlist.ui.ParallaxAnimationView;
import io.reactivex.n;

public class WaitlistActivity extends co.uk.getmondo.common.activities.b implements android.support.v4.widget.SwipeRefreshLayout.b, c.a, ParallaxAnimationView.a {
   c a;
   @BindView(2131821178)
   View bumpButton;
   @BindView(2131821177)
   TextView inviteFriendsTextView;
   @BindView(2131821175)
   ParallaxAnimationView parallaxAnimationView;
   @BindView(2131821174)
   TouchInterceptingRelativeLayout threeFingerView;

   public static Intent a(Context var) {
      return new Intent(var, WaitlistActivity.class);
   }

   public void a() {
      this.a.a();
   }

   public void a(int var) {
      this.inviteFriendsTextView.setText(var);
   }

   public void a(an var) {
      this.parallaxAnimationView.a(var);
   }

   public void a(an var, boolean var) {
      this.parallaxAnimationView.a(var, var);
   }

   public void a(k var) {
      if(var.a()) {
         WebEventActivity.a(this, var);
      } else {
         BumpUpActivity.a(this, var);
      }

   }

   public void a(boolean var) {
      View var = this.bumpButton;
      byte var;
      if(var) {
         var = 0;
      } else {
         var = 8;
      }

      var.setVisibility(var);
   }

   public void b() {
      this.parallaxAnimationView.a();
   }

   public void c() {
      NotInCountryActivity.b(this);
   }

   public n d() {
      return com.b.a.c.c.a(this.bumpButton);
   }

   public void e() {
      ShareActivity.a((Activity)this);
   }

   public void f() {
      this.setResult(-1);
      this.finish();
   }

   protected void onActivityResult(int var, int var, Intent var) {
      super.onActivityResult(var, var, var);
      if(BumpUpActivity.a(var)) {
         this.a.d();
         this.parallaxAnimationView.a(BumpUpActivity.a(var));
      }

   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034229);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((c.a)this);
      this.parallaxAnimationView.setParallaxAnimationListener(this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   protected void onResume() {
      super.onResume();
      this.a.c();
   }

   public void s() {
      this.parallaxAnimationView.setRefreshing(true);
   }

   public void t() {
      this.parallaxAnimationView.setRefreshing(false);
   }
}
