package co.uk.getmondo.waitlist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

public class TouchInterceptingRelativeLayout extends RelativeLayout {
   private OnTouchListener a;

   public TouchInterceptingRelativeLayout(Context var) {
      super(var);
   }

   public TouchInterceptingRelativeLayout(Context var, AttributeSet var) {
      super(var, var);
   }

   public TouchInterceptingRelativeLayout(Context var, AttributeSet var, int var) {
      super(var, var, var);
   }

   public boolean onInterceptTouchEvent(MotionEvent var) {
      if(this.a != null) {
         this.a.onTouch(this, var);
      }

      return false;
   }

   public void setOnTouchListener(OnTouchListener var) {
      this.a = var;
      super.setOnTouchListener(var);
   }
}
