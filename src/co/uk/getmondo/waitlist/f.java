package co.uk.getmondo.waitlist;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class f implements io.reactivex.c.g {
   private final c a;
   private final boolean b;

   private f(c var, boolean var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(c var, boolean var) {
      return new f(var, var);
   }

   public void a(Object var) {
      c.a(this.a, this.b, (ak)var);
   }
}
