package co.uk.getmondo.monzo.me.deeplink;

import android.net.Uri;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var;
      if(!e.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public e(c var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(c var) {
      return new e(var);
   }

   public Uri a() {
      return this.b.a();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
