package co.uk.getmondo.monzo.me.deeplink;

import co.uk.getmondo.payments.send.data.p;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var;
      if(!d.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public d(c var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
            }
         }
      }
   }

   public static b.a.b a(c var, javax.a.a var, javax.a.a var) {
      return new d(var, var, var);
   }

   public f a() {
      return (f)b.a.d.a(this.b.a((co.uk.getmondo.common.accounts.d)this.c.b(), (p)this.d.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
