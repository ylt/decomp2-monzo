package co.uk.getmondo.monzo.me.deeplink;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import co.uk.getmondo.main.HomeActivity;

public class MonzoMeActivity extends co.uk.getmondo.common.activities.b implements f.a {
   f a;

   public void a() {
      co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), "tag_error_dialog");
   }

   public void a(Uri var) {
      Intent var = new Intent("android.intent.action.VIEW", var);
      var.addFlags(268435456);
      var.setPackage("com.android.chrome");

      try {
         this.startActivity(var);
      } catch (ActivityNotFoundException var) {
         var.setPackage((String)null);
         this.startActivity(var);
      }

   }

   public void a(String var, co.uk.getmondo.d.c var, String var) {
      HomeActivity.a(this, var, var, var);
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.l().a(new c(this.getIntent().getData())).a(this);
      this.a.a((f.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
