package co.uk.getmondo.monzo.me.deeplink;

import android.net.Uri;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.p;
import java.math.BigDecimal;
import java.util.List;

class f extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.accounts.d c;
   private final Uri d;
   private final p e;

   f(co.uk.getmondo.common.accounts.d var, Uri var, p var) {
      this.c = var;
      this.d = var;
      this.e = var;
   }

   private co.uk.getmondo.d.c a(String var) {
      co.uk.getmondo.d.c var;
      try {
         BigDecimal var = new BigDecimal(var);
         var = var.scaleByPowerOfTen(co.uk.getmondo.common.i.c.a.a());
         var = new co.uk.getmondo.d.c(var.longValue(), co.uk.getmondo.common.i.c.a);
      } catch (Exception var) {
         var = null;
      }

      return var;
   }

   public void a(f.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      ak var = this.c.b();
      if(this.d != null && var != null && var.a() == ak.a.HAS_ACCOUNT) {
         if(this.e.a() == co.uk.getmondo.payments.send.data.a.d.c) {
            var.a();
         } else {
            List var = this.d.getPathSegments();
            String var = (String)var.get(0);
            String var;
            if(var.size() > 1) {
               var = (String)var.get(1);
            } else {
               var = "";
            }

            String var = this.d.getQueryParameter("d");
            var.a(var, this.a(var), var);
         }
      } else {
         var.a(this.d);
      }

   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a();

      void a(Uri var);

      void a(String var, co.uk.getmondo.d.c var, String var);
   }
}
