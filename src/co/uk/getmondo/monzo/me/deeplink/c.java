package co.uk.getmondo.monzo.me.deeplink;

import android.net.Uri;
import co.uk.getmondo.payments.send.data.p;

public class c {
   private final Uri a;

   c(Uri var) {
      this.a = var;
   }

   Uri a() {
      return this.a;
   }

   f a(co.uk.getmondo.common.accounts.d var, p var) {
      return new f(var, this.a, var);
   }
}
