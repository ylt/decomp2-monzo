package co.uk.getmondo.monzo.me.request;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.settings.k;
import io.reactivex.n;

class b extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final co.uk.getmondo.payments.send.data.h d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.monzo.me.a f;
   private final k g;

   b(co.uk.getmondo.common.a var, co.uk.getmondo.payments.send.data.h var, co.uk.getmondo.common.accounts.d var, co.uk.getmondo.monzo.me.a var, k var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
   }

   // $FF: synthetic method
   static void a(b.a var, Boolean var) throws Exception {
      var.g();
   }

   // $FF: synthetic method
   static void a(b.a var, Object var) throws Exception {
      var.g();
   }

   // $FF: synthetic method
   static void a(b var, b.a var, String var, Object var) throws Exception {
      var.c.a(Impression.U());
      var.a(var);
   }

   // $FF: synthetic method
   static boolean a(b var, Boolean var) throws Exception {
      boolean var;
      if(var.booleanValue() && !var.g.c()) {
         var = true;
      } else {
         var = false;
      }

      return var;
   }

   // $FF: synthetic method
   static void b(b.a var, Object var) throws Exception {
      var.f();
   }

   public void a(b.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.c.a(Impression.T());
      String var = this.d.c();
      String var = String.format("%s/%s", new Object[]{"monzo.me", var});
      var = this.f.a(var, (String)null, (String)null);
      var.a(this.e.b().d().a(), var);
      this.a((io.reactivex.b.b)var.c().subscribe(c.a(this, var, var)));
      this.a((io.reactivex.b.b)var.d().subscribe(d.a(var)));
      this.a((io.reactivex.b.b)var.b().subscribe(e.a(var)));
      this.a((io.reactivex.b.b)var.e().filter(f.a(this)).subscribe(g.a(var)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(String var);

      void a(String var, String var);

      n b();

      n c();

      n d();

      n e();

      void f();

      void g();
   }
}
