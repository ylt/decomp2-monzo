package co.uk.getmondo.monzo.me.request;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class RequestMoneyFragment_ViewBinding implements Unbinder {
   private RequestMoneyFragment a;

   public RequestMoneyFragment_ViewBinding(RequestMoneyFragment var, View var) {
      this.a = var;
      var.iconImageView = (ImageView)Utils.findRequiredViewAsType(var, 2131821356, "field 'iconImageView'", ImageView.class);
      var.nameTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821357, "field 'nameTextView'", TextView.class);
      var.linkTextView = (TextView)Utils.findRequiredViewAsType(var, 2131821358, "field 'linkTextView'", TextView.class);
      var.shareButton = (Button)Utils.findRequiredViewAsType(var, 2131821359, "field 'shareButton'", Button.class);
      var.customiseAmountButton = (Button)Utils.findRequiredViewAsType(var, 2131821360, "field 'customiseAmountButton'", Button.class);
   }

   public void unbind() {
      RequestMoneyFragment var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.iconImageView = null;
         var.nameTextView = null;
         var.linkTextView = null;
         var.shareButton = null;
         var.customiseAmountButton = null;
      }
   }
}
