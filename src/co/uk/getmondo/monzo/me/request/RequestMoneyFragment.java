package co.uk.getmondo.monzo.me.request;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.j;
import co.uk.getmondo.monzo.me.customise.CustomiseMonzoMeLinkActivity;
import co.uk.getmondo.monzo.me.onboarding.MonzoMeOnboardingActivity;
import io.reactivex.n;

public class RequestMoneyFragment extends co.uk.getmondo.common.f.a implements b.a {
   b a;
   private final io.reactivex.i.a c = io.reactivex.i.a.a();
   @BindView(2131821360)
   Button customiseAmountButton;
   private final io.reactivex.i.a d = io.reactivex.i.a.a();
   private co.uk.getmondo.common.ui.a e;
   private Unbinder f;
   @BindView(2131821356)
   ImageView iconImageView;
   @BindView(2131821358)
   TextView linkTextView;
   @BindView(2131821357)
   TextView nameTextView;
   @BindView(2131821359)
   Button shareButton;

   public static Fragment a() {
      return new RequestMoneyFragment();
   }

   public void a(String var) {
      String var = this.getString(2131362690);
      this.startActivity(j.a(this.getActivity(), var, var, co.uk.getmondo.api.model.tracking.a.REQUEST_MONEY));
   }

   public void a(String var, String var) {
      this.nameTextView.setText(var);
      this.linkTextView.setText(var);
      int var = (int)TypedValue.applyDimension(2, 28.0F, this.getResources().getDisplayMetrics());
      this.iconImageView.setImageDrawable(this.e.a(var).a(var));
   }

   public n b() {
      return this.d;
   }

   public n c() {
      return com.b.a.c.c.a(this.shareButton);
   }

   public n d() {
      return com.b.a.c.c.a(this.customiseAmountButton);
   }

   public n e() {
      return this.c;
   }

   public void f() {
      CustomiseMonzoMeLinkActivity.a(this.getActivity(), (co.uk.getmondo.d.c)null, (String)null, Impression.CustomiseMonzoMeLinkFrom.REQUEST_MONEY);
   }

   public void g() {
      this.startActivity(MonzoMeOnboardingActivity.a((Context)this.getActivity()));
   }

   public void onCreate(Bundle var) {
      super.onCreate(var);
      this.setHasOptionsMenu(true);
      this.e = co.uk.getmondo.common.ui.a.a((Context)this.getActivity());
      this.B().a(this);
   }

   public void onCreateOptionsMenu(Menu var, MenuInflater var) {
      var.inflate(2131951621, var);
      super.onCreateOptionsMenu(var, var);
   }

   public View onCreateView(LayoutInflater var, ViewGroup var, Bundle var) {
      return var.inflate(2131034277, var, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.f.unbind();
      super.onDestroyView();
   }

   public boolean onOptionsItemSelected(MenuItem var) {
      boolean var;
      if(var.getItemId() == 2131821779) {
         this.d.onNext(co.uk.getmondo.common.b.a.a);
         var = true;
      } else {
         var = super.onOptionsItemSelected(var);
      }

      return var;
   }

   public void onViewCreated(View var, Bundle var) {
      super.onViewCreated(var, var);
      this.f = ButterKnife.bind(this, (View)var);
      this.a.a((b.a)this);
   }

   public void setUserVisibleHint(boolean var) {
      super.setUserVisibleHint(var);
      this.c.onNext(Boolean.valueOf(var));
   }
}
