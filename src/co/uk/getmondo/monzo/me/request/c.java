package co.uk.getmondo.monzo.me.request;

// $FF: synthetic class
final class c implements io.reactivex.c.g {
   private final b a;
   private final b.a b;
   private final String c;

   private c(b var, b.a var, String var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   public static io.reactivex.c.g a(b var, b.a var, String var) {
      return new c(var, var, var);
   }

   public void a(Object var) {
      b.a(this.a, this.b, this.c, var);
   }
}
