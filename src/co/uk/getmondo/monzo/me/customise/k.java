package co.uk.getmondo.monzo.me.customise;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final i a;
   private final i.a b;

   private k(i var, i.a var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.g a(i var, i.a var) {
      return new k(var, var);
   }

   public void a(Object var) {
      i.b(this.a, this.b, (android.support.v4.g.j)var);
   }
}
