package co.uk.getmondo.monzo.me.customise;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class CustomiseMonzoMeLinkActivity_ViewBinding implements Unbinder {
   private CustomiseMonzoMeLinkActivity a;

   public CustomiseMonzoMeLinkActivity_ViewBinding(CustomiseMonzoMeLinkActivity var, View var) {
      this.a = var;
      var.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var, 2131820878, "field 'amountInputView'", AmountInputView.class);
      var.notesEditText = (EditText)Utils.findRequiredViewAsType(var, 2131820879, "field 'notesEditText'", EditText.class);
      var.shareButton = (Button)Utils.findRequiredViewAsType(var, 2131820880, "field 'shareButton'", Button.class);
   }

   public void unbind() {
      CustomiseMonzoMeLinkActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.amountInputView = null;
         var.notesEditText = null;
         var.shareButton = null;
      }
   }
}
