package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.payments.send.data.p;

class i extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final co.uk.getmondo.payments.send.data.h d;
   private final co.uk.getmondo.d.c e;
   private final String f;
   private final co.uk.getmondo.monzo.me.a g;
   private final Impression.CustomiseMonzoMeLinkFrom h;
   private final p i;

   i(co.uk.getmondo.common.a var, co.uk.getmondo.payments.send.data.h var, co.uk.getmondo.d.c var, String var, co.uk.getmondo.monzo.me.a var, Impression.CustomiseMonzoMeLinkFrom var, p var) {
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
      this.g = var;
      this.h = var;
      this.i = var;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(Object var, String var, String var) throws Exception {
      return android.support.v4.g.j.a(var, var);
   }

   // $FF: synthetic method
   static void a(i var, i.a var, android.support.v4.g.j var) throws Exception {
      var.c.a(Impression.S());
      String var = var.d.c();
      var.a(var.g.a(var, (String)var.a, (String)var.b));
   }

   // $FF: synthetic method
   static void b(i var, i.a var, android.support.v4.g.j var) throws Exception {
      var.a(false);
      String var = (String)var.a;
      String var = (String)var.b;
      if(co.uk.getmondo.common.k.p.d(var)) {
         var.e();
         if(co.uk.getmondo.common.k.p.c(var)) {
            var.a(true);
         }
      } else {
         co.uk.getmondo.d.p var = var.i.b();
         co.uk.getmondo.d.c var = co.uk.getmondo.d.c.a(var, co.uk.getmondo.common.i.c.a);
         if(var != null) {
            long var = (long)var.b();
            long var = (long)var.a();
            double var = var.f();
            if(var <= 0L) {
               var.d();
            } else if(var >= (double)var && var <= (double)var) {
               var.e();
               var.a(true);
            } else {
               var.a(var.b(), var.a());
            }
         }
      }

   }

   public void a(i.a var) {
      super.a((co.uk.getmondo.common.ui.f)var);
      this.c.a(Impression.a(this.h));
      var.a(this.e, this.f);
      this.a((io.reactivex.b.b)io.reactivex.n.combineLatest(var.a(), var.b(), j.a()).subscribe(k.a(this, var)));
      this.a((io.reactivex.b.b)var.c().withLatestFrom(var.a(), var.b(), l.a()).subscribe(m.a(this, var), n.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(int var, int var);

      void a(co.uk.getmondo.d.c var, String var);

      void a(String var);

      void a(boolean var);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();
   }
}
