package co.uk.getmondo.monzo.me.customise;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountInputView;

public class CustomiseMonzoMeLinkActivity extends co.uk.getmondo.common.activities.b implements i.a {
   i a;
   @BindView(2131820878)
   AmountInputView amountInputView;
   @BindView(2131820879)
   EditText notesEditText;
   @BindView(2131820880)
   Button shareButton;

   public static void a(Context var, co.uk.getmondo.d.c var, String var, Impression.CustomiseMonzoMeLinkFrom var) {
      var.startActivity(b(var, var, var, var));
   }

   public static Intent b(Context var, co.uk.getmondo.d.c var, String var, Impression.CustomiseMonzoMeLinkFrom var) {
      Intent var = new Intent(var, CustomiseMonzoMeLinkActivity.class);
      var.putExtra("key_notes", var);
      var.putExtra("key_amount", var);
      var.putExtra("key_entry_point", var);
      return var;
   }

   public io.reactivex.n a() {
      return this.amountInputView.a().map(a.a());
   }

   public void a(int var, int var) {
      this.amountInputView.setError(this.getString(2131362470, new Object[]{Integer.valueOf(var), Integer.valueOf(var)}));
   }

   public void a(co.uk.getmondo.d.c var, String var) {
      if(var != null) {
         this.amountInputView.setDefaultAmount(var);
      }

      this.notesEditText.setText(var);
   }

   public void a(String var) {
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362690), var, co.uk.getmondo.api.model.tracking.a.REQUEST_MONEY_CUSTOMISE));
   }

   public void a(boolean var) {
      this.shareButton.setEnabled(var);
   }

   public io.reactivex.n b() {
      return com.b.a.d.e.c(this.notesEditText).map(b.a());
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.shareButton);
   }

   public void d() {
      this.amountInputView.setError(this.getString(2131362471));
   }

   public void e() {
      this.amountInputView.setError("");
   }

   protected void onCreate(Bundle var) {
      super.onCreate(var);
      this.setContentView(2131034160);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.d.c var = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("key_amount");
      String var = this.getIntent().getStringExtra("key_notes");
      Impression.CustomiseMonzoMeLinkFrom var = (Impression.CustomiseMonzoMeLinkFrom)this.getIntent().getSerializableExtra("key_entry_point");
      this.l().a(new e(var, var, var)).a(this);
      this.a.a((i.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
