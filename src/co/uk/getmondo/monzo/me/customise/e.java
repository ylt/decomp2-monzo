package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;

public class e {
   private co.uk.getmondo.d.c a;
   private final String b;
   private final Impression.CustomiseMonzoMeLinkFrom c;

   e(co.uk.getmondo.d.c var, String var, Impression.CustomiseMonzoMeLinkFrom var) {
      this.a = var;
      this.b = var;
      this.c = var;
   }

   co.uk.getmondo.d.c a() {
      return this.a;
   }

   String b() {
      return this.b;
   }

   Impression.CustomiseMonzoMeLinkFrom c() {
      return this.c;
   }
}
