package co.uk.getmondo.monzo.me.customise;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final e b;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(e var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(e var) {
      return new f(var);
   }

   public co.uk.getmondo.d.c a() {
      return this.b.a();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
