package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final e b;

   static {
      boolean var;
      if(!g.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public g(e var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
      }
   }

   public static b.a.b a(e var) {
      return new g(var);
   }

   public Impression.CustomiseMonzoMeLinkFrom a() {
      return (Impression.CustomiseMonzoMeLinkFrom)b.a.d.a(this.b.c(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
