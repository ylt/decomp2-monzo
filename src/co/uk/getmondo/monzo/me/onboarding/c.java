package co.uk.getmondo.monzo.me.onboarding;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final MonzoMeOnboardingActivity a;

   private c(MonzoMeOnboardingActivity var) {
      this.a = var;
   }

   public static io.reactivex.c.h a(MonzoMeOnboardingActivity var) {
      return new c(var);
   }

   public Object a(Object var) {
      return MonzoMeOnboardingActivity.a(this.a, (Integer)var);
   }
}
