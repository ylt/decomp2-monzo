package co.uk.getmondo.monzo.me.onboarding;

import io.reactivex.c.q;

// $FF: synthetic class
final class a implements q {
   private final MonzoMeOnboardingActivity a;

   private a(MonzoMeOnboardingActivity var) {
      this.a = var;
   }

   public static q a(MonzoMeOnboardingActivity var) {
      return new a(var);
   }

   public boolean a(Object var) {
      return MonzoMeOnboardingActivity.b(this.a, var);
   }
}
