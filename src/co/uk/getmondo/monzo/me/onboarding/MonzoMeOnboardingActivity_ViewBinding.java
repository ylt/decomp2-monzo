package co.uk.getmondo.monzo.me.onboarding;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import me.relex.circleindicator.CircleIndicator;

public class MonzoMeOnboardingActivity_ViewBinding implements Unbinder {
   private MonzoMeOnboardingActivity a;

   public MonzoMeOnboardingActivity_ViewBinding(MonzoMeOnboardingActivity var, View var) {
      this.a = var;
      var.viewPager = (ViewPager)Utils.findRequiredViewAsType(var, 2131821000, "field 'viewPager'", ViewPager.class);
      var.viewPagerIndicator = (CircleIndicator)Utils.findRequiredViewAsType(var, 2131821001, "field 'viewPagerIndicator'", CircleIndicator.class);
      var.actionButton = (Button)Utils.findRequiredViewAsType(var, 2131821002, "field 'actionButton'", Button.class);
      var.progress = (ProgressBar)Utils.findRequiredViewAsType(var, 2131821003, "field 'progress'", ProgressBar.class);
      var.toolbar = (Toolbar)Utils.findRequiredViewAsType(var, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      MonzoMeOnboardingActivity var = this.a;
      if(var == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var.viewPager = null;
         var.viewPagerIndicator = null;
         var.actionButton = null;
         var.progress = null;
         var.toolbar = null;
      }
   }
}
