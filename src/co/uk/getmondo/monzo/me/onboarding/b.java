package co.uk.getmondo.monzo.me.onboarding;

import io.reactivex.c.q;

// $FF: synthetic class
final class b implements q {
   private final MonzoMeOnboardingActivity a;

   private b(MonzoMeOnboardingActivity var) {
      this.a = var;
   }

   public static q a(MonzoMeOnboardingActivity var) {
      return new b(var);
   }

   public boolean a(Object var) {
      return MonzoMeOnboardingActivity.a(this.a, var);
   }
}
