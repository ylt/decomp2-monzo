package co.uk.getmondo.background_sync;

import io.reactivex.u;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;

   static {
      boolean var;
      if(!f.class.desiredAssertionStatus()) {
         var = true;
      } else {
         var = false;
      }

      a = var;
   }

   public f(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      if(!a && var == null) {
         throw new AssertionError();
      } else {
         this.b = var;
         if(!a && var == null) {
            throw new AssertionError();
         } else {
            this.c = var;
            if(!a && var == null) {
               throw new AssertionError();
            } else {
               this.d = var;
               if(!a && var == null) {
                  throw new AssertionError();
               } else {
                  this.e = var;
                  if(!a && var == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var;
                     if(!a && var == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var;
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var, javax.a.a var) {
      return new f(var, var, var, var, var, var);
   }

   public d a() {
      return new d((u)this.b.b(), (co.uk.getmondo.feed.a.d)this.c.b(), (co.uk.getmondo.a.a)this.d.b(), (co.uk.getmondo.common.accounts.b)this.e.b(), (co.uk.getmondo.overdraft.a.c)this.f.b(), (co.uk.getmondo.overdraft.a.a)this.g.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
