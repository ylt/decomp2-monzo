package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;

// $FF: synthetic class
final class a implements io.reactivex.c.a {
   private final SyncFeedAndBalanceJobService a;
   private final JobParameters b;

   private a(SyncFeedAndBalanceJobService var, JobParameters var) {
      this.a = var;
      this.b = var;
   }

   public static io.reactivex.c.a a(SyncFeedAndBalanceJobService var, JobParameters var) {
      return new a(var, var);
   }

   public void a() {
      SyncFeedAndBalanceJobService.a(this.a, this.b);
   }
}
