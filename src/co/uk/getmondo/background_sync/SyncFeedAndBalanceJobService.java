package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.app.job.JobInfo.Builder;
import android.content.ComponentName;
import android.content.Context;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.ApiException;
import io.reactivex.u;
import io.realm.internal.IOException;

public class SyncFeedAndBalanceJobService extends JobService {
   u a;
   d b;
   private final io.reactivex.b.a c = new io.reactivex.b.a();

   private void a(JobParameters var, Throwable var) {
      boolean var;
      label22: {
         if(var instanceof ApiException) {
            if(((ApiException)var).b() >= 500) {
               var = true;
               break label22;
            }
         } else if(!co.uk.getmondo.common.e.a.a(var) && var instanceof IOException) {
            d.a.a.a((Throwable)(new RuntimeException("Rescheduling background refresh", var)));
            var = true;
            break label22;
         }

         var = false;
      }

      if(var) {
         d.a.a.a("Error with background refresh, rescheduling", new Object[0]);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Giving up trying to perform background refresh", var)));
      }

      this.jobFinished(var, var);
   }

   public static void a(Context var) {
      ComponentName var = new ComponentName(var, SyncFeedAndBalanceJobService.class);
      d.a.a.a("Creating background refresher job", new Object[0]);
      ((JobScheduler)var.getSystemService("jobscheduler")).schedule((new Builder(100, var)).setRequiredNetworkType(1).build());
   }

   // $FF: synthetic method
   static void a(SyncFeedAndBalanceJobService var, JobParameters var) throws Exception {
      d.a.a.b("Feed and balance synced successfully", new Object[0]);
      var.jobFinished(var, false);
   }

   // $FF: synthetic method
   static void a(SyncFeedAndBalanceJobService var, JobParameters var, Throwable var) throws Exception {
      var.a(var, var);
   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
   }

   public void onDestroy() {
      this.c.a();
      super.onDestroy();
   }

   public boolean onStartJob(JobParameters var) {
      d.a.a.a("Background sync job triggered", new Object[0]);
      this.c.a(this.b.a().b(this.a).a(a.a(this, var), b.a(this, var)));
      return true;
   }

   public boolean onStopJob(JobParameters var) {
      this.c.a();
      return true;
   }
}
