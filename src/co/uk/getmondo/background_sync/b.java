package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;
import io.reactivex.c.g;

// $FF: synthetic class
final class b implements g {
   private final SyncFeedAndBalanceJobService a;
   private final JobParameters b;

   private b(SyncFeedAndBalanceJobService var, JobParameters var) {
      this.a = var;
      this.b = var;
   }

   public static g a(SyncFeedAndBalanceJobService var, JobParameters var) {
      return new b(var, var);
   }

   public void a(Object var) {
      SyncFeedAndBalanceJobService.a(this.a, this.b, (Throwable)var);
   }
}
