package co.uk.getmondo.background_sync;

import co.uk.getmondo.d.w;
import io.reactivex.u;
import java.util.ArrayList;
import java.util.List;

public class d {
   private final co.uk.getmondo.feed.a.d a;
   private final co.uk.getmondo.a.a b;
   private final co.uk.getmondo.common.accounts.b c;
   private final co.uk.getmondo.overdraft.a.c d;
   private final co.uk.getmondo.overdraft.a.a e;
   private final u f;

   public d(u var, co.uk.getmondo.feed.a.d var, co.uk.getmondo.a.a var, co.uk.getmondo.common.accounts.b var, co.uk.getmondo.overdraft.a.c var, co.uk.getmondo.overdraft.a.a var) {
      this.a = var;
      this.b = var;
      this.c = var;
      this.d = var;
      this.e = var;
      this.f = var;
   }

   private io.reactivex.b a(String var) {
      return this.d.b(var).firstElement().b(e.a(this)).b(this.f);
   }

   // $FF: synthetic method
   static io.reactivex.d a(d var, w var) throws Exception {
      io.reactivex.b var;
      if(var.d().b()) {
         var = var.a.c("overdraft_charges_item_id");
      } else {
         var = var.a.a(var.e.a(var));
      }

      return var;
   }

   public io.reactivex.b a() {
      ArrayList var = new ArrayList();
      String var = this.c.c();
      var.add(var);
      if(this.c.d() != null) {
         var.add(this.c.d());
      }

      io.reactivex.b var = this.a.a((List)var).d(this.b.b(var));
      io.reactivex.b var = var;
      if(this.c.b()) {
         var = var.d(this.d.a(var)).b((io.reactivex.d)this.a(var));
      }

      return var;
   }
}
